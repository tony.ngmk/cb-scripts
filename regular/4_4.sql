WITH
o AS ( 
SELECT DISTINCT
o2.orderid
, o1.shop_id shopid
, o1.create_time
, o1.cancel_time
, o2.cancel_reason
, o1.status_ext
, o1.complete_time
, o1.grass_date
FROM
((
SELECT DISTINCT
order_id
, shop_id
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) create_time
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) cancel_time
, order_be_status_id status_ext
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) complete_time
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) grass_date
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ((is_cb_shop = 0) AND (tz_type = 'local') AND date(from_unixtime(create_timestamp)) >= current_date - interval '120' day AND grass_region = 'TH' AND grass_date >= current_date - interval '120' day)
) o1
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'TH')) AND date(from_unixtime(create_time)) >= current_date - interval '120' day
) o2 ON (o1.order_id = o2.orderid))
) 
, r AS (
SELECT DISTINCT
orderid
, shopid
, reason
, status
, "from_unixtime"(ctime) ctime
, "from_unixtime"(mtime) mtime
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'TH') AND date("from_unixtime"(ctime)) >= current_date - interval '90' day)
) 
, wms AS (
SELECT shop_id
FROM sbs_mart.shopee_scbs_db__shop_tab__reg_daily_s0_live
WHERE country IN ('TH')
AND ((fbs_tag = 0 AND vacation_mode = 0 AND shop_status = 1)
OR (fbs_tag = 1 AND vacation_mode = 0 AND cb_option = 1)
OR (fbs_tag = 1 AND vacation_mode = 0 AND cb_option = 0))
)
, channel AS (
SELECT DISTINCT
shopid 
, userid
, extinfo
, grass_region
FROM 
marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE grass_region = 'TH' AND COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.7000.enabled') AS bigint), 0) = 1 -- enable masked channel
)
, user AS (
SELECT DISTINCT
shop_id 
, shop_level1_global_be_category main_category
, grass_region
, is_official_shop
, active_item_with_stock_cnt live_sku
, user_name username 
, user_id userid 
, CASE WHEN date(from_unixtime(shop_first_listing_timestamp)) <= current_date - INTERVAL '30' DAY THEN 'Y' ELSE 'N' END live_more_than_30_days
, CASE WHEN k.keyword IS NULL THEN 'Y' ELSE 'N' END no_vape_keyword
FROM 
regcbbi_others.shop_profile s
LEFT JOIN regcbbi_others.sip__thsip_vape_keyword__tmp__reg__s0 k ON regexp_like(s.shop_name,k.keyword) OR regexp_like(s.description,k.keyword)
WHERE grass_date = current_date - INTERVAL '1' DAY AND is_cb_shop = 0 AND grass_region = 'TH' AND status = 1 AND user_status = 1 AND is_holiday_mode = 0
AND date(from_unixtime(shop_first_listing_timestamp)) <= current_date - INTERVAL '30' DAY -- live shop for >= 30 days
AND shop_level1_global_be_category <> 'Tickets, Vouchers & Services' -- seller main category is not Voucher & Services
AND k.keyword IS NULL -- shop name & description do not contain vape keyword
)
, ops_check AS (
SELECT DISTINCT
shop_id
, "count"(DISTINCT (CASE WHEN ("date"(o.create_time) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) THEN o.orderid ELSE null END)) gross_orders_30d
, "count"(DISTINCT (CASE WHEN (((grass_region = 'TW') AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302))) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid WHEN (((grass_region <> 'TW') AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302))) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) NFR_included_cancellations_30d
, "count"(DISTINCT (CASE WHEN (((grass_region = 'TW') AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302))) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid WHEN (((grass_region <> 'TW') AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302))) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) NFR_included_cancellations_90d
, "count"(DISTINCT (CASE WHEN (((((grass_region = 'TW') AND (r.reason IN (1, 2, 3, 103, 105))) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid WHEN (((((grass_region <> 'TW') AND (r.reason IN (1, 2, 3, 103, 105, 107))) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END)) NFR_included_rr_90d
, "count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid ELSE null END)) net_orders_90d
FROM 
user 
LEFT JOIN o ON user.shop_id = o.shopid
LEFT JOIN r ON o.orderid = r.orderid
GROUP BY 1
)

, ex AS (
SELECT DISTINCT CAST(shopid AS bigint) shopid 
FROM regcbbi_others.shopee_regional_cb_team__thsip_migration_maintain_shop_list2
)

SELECT *
, sum(live_sku) OVER (ORDER BY gross_orders_30d DESC, cr_l90d ASC, shop_id ASC ROWS BETWEEN unbounded preceding AND current row) acc_sku
FROM
(SELECT 
u.grass_region
, u.shop_id
, is_official_shop
, main_category
, live_sku
, u.userid
, u.username
, NFR_included_cancellations_30d
, gross_orders_30d
, TRY(((NFR_included_cancellations_90d * DECIMAL '1.000') / ((NFR_included_cancellations_90d + NFR_included_rr_90d) + net_orders_90d))) cr_l90d
FROM user u 
INNER JOIN ops_check oc ON u.shop_id = oc.shop_id 
INNER JOIN channel ch ON ch.shopid = u.shop_id 
LEFT JOIN wms ON wms.shop_id = u.shop_id 
LEFT JOIN ex ON ex.shopid = u.shop_id
WHERE wms.shop_id IS NULL 
AND ex.shopid IS NULL
AND gross_orders_30d / DECIMAL '30.00' >= DECIMAL '0.1' -- L30D ADO >= 0.1
AND (TRY(((NFR_included_cancellations_90d * DECIMAL '1.000') / ((NFR_included_cancellations_90d + NFR_included_rr_90d) + net_orders_90d))) <= DECIMAL '0.03') -- L90D CR <= 0.03
)
