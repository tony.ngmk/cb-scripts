WITH dim AS (
SELECT
DISTINCT min(date_id) - interval '14' day w_2_begin,
min(date_id) - interval '8' day w_2_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim2 AS (
SELECT
DISTINCT min(date_id) - interval '21' day w_3_begin,
min(date_id) - interval '15' day w_3_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim3 AS (
SELECT
DISTINCT min(date_id) - interval '28' day w_4_begin,
min(date_id) - interval '22' day w_4_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim4 AS (
SELECT
DISTINCT min(date_id) - interval '35' day w_5_begin,
min(date_id) - interval '29' day w_5_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim5 AS (
SELECT
DISTINCT min(date_id) - interval '42' day w_6_begin,
min(date_id) - interval '36' day w_6_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim6 AS (
SELECT
DISTINCT min(date_id) - interval '49' day w_7_begin,
min(date_id) - interval '43' day w_7_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim1 AS (
SELECT
DISTINCT min(date_id) - interval '7' day w_1_begin,
min(date_id) - interval '1' day w_1_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
sip AS (
SELECT
DISTINCT affi_shopid,
mst_shopid,
a.country affi_country,
v.country mst_country,
cb_option
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT
DISTINCT shopid,
country,
cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live -- WHERE (cb_option = 1)
) v ON v.shopid = a.mst_shopid
WHERE
a.grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL'
)
AND a.sip_shop_status != 3
)
SELECT
DISTINCT a.grass_region,
CASE
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_2_begin AND w_2_end THEN 'W-2*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_3_begin AND w_3_end THEN 'W-3*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_4_begin AND w_4_end THEN 'W-4*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_5_begin AND w_5_end THEN 'W-5*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_6_begin AND w_6_end THEN 'W-6*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_7_begin AND w_7_end THEN 'W-7*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_1_begin AND w_1_end THEN 'W-1*'
ELSE null
END time_break,
CASE
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'MY'
and cb_option = 1
) THEN 'CB SIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'TW'
and cb_option = 1
) THEN 'TB SIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'BR'
and cb_option = 1
) THEN 'BR SIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'SG'
and cb_option = 1
) THEN 'KRCB SIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'MY'
and cb_option = 0
) THEN 'Local MYSIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'ID'
and cb_option = 0
) THEN 'Local IDSIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'TW'
and cb_option = 0
) THEN 'Local TWSIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'TH'
and cb_option = 0
) THEN 'Local THSIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'VN'
and cb_option = 0
) THEN 'Local VNSIP'
WHEN NOT (
a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
)
) THEN 'CB'
END shop_type,
count(
DISTINCT (
CASE
WHEN grass_region = 'TW'
AND cancel_reason_id IN (1, 2, 3, 200, 201, 204, 302, 303) THEN order_id
WHEN grass_region NOT IN ('TW')
AND cancel_reason_id IN (1, 2, 3, 4, 204, 301, 302, 303) THEN order_id
ELSE null
END
)
) cancel_order,
count(DISTINCT order_id) gross_order,
count(
DISTINCT CASE
WHEN is_net_order = 1 THEN order_id
ELSE NULL
END
) net_order
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live a
INNER JOIN dim ON 1 = 1
INNER JOIN dim2 ON 1 = 1
INNER JOIN DIM3 ON 1 = 1
INNER JOIN DIM4 ON 1 = 1
INNER JOIN dim5 on 1 = 1
INNER JOIN dim6 on 1 = 1
INNER JOIN dim1 on 1 = 1
WHERE
date(split(create_datetime, ' ') [ 1 ]) >= (current_date - INTERVAL '49' DAY)
AND grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL'
)
AND is_cb_shop = 1
AND grass_date >= (current_date - INTERVAL '50' DAY)
GROUP BY
1,
2,
WITH dim AS (
SELECT
DISTINCT min(date_id) - interval '14' day w_2_begin,
min(date_id) - interval '8' day w_2_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim2 AS (
SELECT
DISTINCT min(date_id) - interval '21' day w_3_begin,
min(date_id) - interval '15' day w_3_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim3 AS (
SELECT
DISTINCT min(date_id) - interval '28' day w_4_begin,
min(date_id) - interval '22' day w_4_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim4 AS (
SELECT
DISTINCT min(date_id) - interval '35' day w_5_begin,
min(date_id) - interval '29' day w_5_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim5 AS (
SELECT
DISTINCT min(date_id) - interval '42' day w_6_begin,
min(date_id) - interval '36' day w_6_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim6 AS (
SELECT
DISTINCT min(date_id) - interval '49' day w_7_begin,
min(date_id) - interval '43' day w_7_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
dim1 AS (
SELECT
DISTINCT min(date_id) - interval '7' day w_1_begin,
min(date_id) - interval '1' day w_1_end
FROM
regbida_keyreports.dim_date
WHERE
week(date_id) = week(current_date)
AND year(date_id) = year(current_date)
),
sip AS (
SELECT
DISTINCT affi_shopid,
mst_shopid,
a.country affi_country,
v.country mst_country,
cb_option
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT
DISTINCT shopid,
country,
cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live -- WHERE (cb_option = 1)
) v ON v.shopid = a.mst_shopid
WHERE
a.grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL'
)
AND a.sip_shop_status != 3
)
SELECT
DISTINCT a.grass_region,
CASE
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_2_begin AND w_2_end THEN 'W-2*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_3_begin AND w_3_end THEN 'W-3*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_4_begin AND w_4_end THEN 'W-4*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_5_begin AND w_5_end THEN 'W-5*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_6_begin AND w_6_end THEN 'W-6*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_7_begin AND w_7_end THEN 'W-7*'
WHEN date(split(create_datetime, ' ') [ 1 ]) BETWEEN w_1_begin AND w_1_end THEN 'W-1*'
ELSE null
END time_break,
CASE
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'MY'
and cb_option = 1
) THEN 'CB SIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'TW'
and cb_option = 1
) THEN 'TB SIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'BR'
and cb_option = 1
) THEN 'BR SIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'SG'
and cb_option = 1
) THEN 'KRCB SIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'MY'
and cb_option = 0
) THEN 'Local MYSIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'ID'
and cb_option = 0
) THEN 'Local IDSIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'TW'
and cb_option = 0
) THEN 'Local TWSIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'TH'
and cb_option = 0
) THEN 'Local THSIP'
WHEN a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
WHERE
mst_country = 'VN'
and cb_option = 0
) THEN 'Local VNSIP'
WHEN NOT (
a.shop_id IN (
SELECT
DISTINCT affi_shopid
FROM
sip
)
) THEN 'CB'
END shop_type,
count(
DISTINCT (
CASE
WHEN grass_region = 'TW'
AND cancel_reason_id IN (1, 2, 3, 200, 201, 204, 302, 303) THEN order_id
WHEN grass_region NOT IN ('TW')
AND cancel_reason_id IN (1, 2, 3, 4, 204, 301, 302, 303) THEN order_id
ELSE null
END
)
) cancel_order,
count(DISTINCT order_id) gross_order,
count(
DISTINCT CASE
WHEN is_net_order = 1 THEN order_id
ELSE NULL
END
) net_order
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live a
INNER JOIN dim ON 1 = 1
INNER JOIN dim2 ON 1 = 1
INNER JOIN DIM3 ON 1 = 1
INNER JOIN DIM4 ON 1 = 1
INNER JOIN dim5 on 1 = 1
INNER JOIN dim6 on 1 = 1
INNER JOIN dim1 on 1 = 1
WHERE
date(split(create_datetime, ' ') [ 1 ]) >= (current_date - INTERVAL '49' DAY)
AND grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL'
)
AND is_cb_shop = 1
AND grass_date >= (current_date - INTERVAL '50' DAY)
GROUP BY
1,
2,
3