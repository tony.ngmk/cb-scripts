INSERT INTO ${SCHEMA}.bi__cbsip_negative_balance_shop__reg__daily

WITH new_sts_info AS (
SELECT distinct
a.grass_region
,a.seller_userid
,a.account_id
,a.amount_local_currency
,1.00 * a.amount_local_currency / b.exchange_rate AS amount_local_usd
,a.order_id
,a.item_type
,a.payment_type
,a.payment_status
,a.sts_create_date
,a.grass_date
,a.remark
FROM (
SELECT
grass_region
,seller_userid
,account_id
,amount_local_currency
,order_id
,item_type
,payment_type
,payment_status
,sts_create_date
,grass_date
,remark
FROM regcbbi_general.bi__sts_billing_item_tab__reg__daily
WHERE
-- grass_date = (
-- select max(grass_date) as col1
-- from regcbbi_general.bi__sts_billing_item_tab__reg__daily
-- )
grass_date = CAST('${BIZ_TIME}' as date) - interval '1' day
AND payment_status = 2
) AS a
LEFT JOIN (
SELECT distinct
grass_date
,grass_region
,currency
,exchange_rate -- local currency / USD
FROM mp_order.dim_exchange_rate__reg_s0_live
WHERE
grass_date = CAST('${BIZ_TIME}' as date) - interval '1' day
) AS b
ON a.grass_region = b.grass_region

)

,nagative_balance_shop AS (
SELECT
a.grass_region
,a.seller_userid
,a.shop_balance__local_currency
,a.shop_balance__usd
FROM (
SELECT
grass_region
,seller_userid
,SUM(amount_local_currency) AS shop_balance__local_currency
,SUM(amount_local_usd) AS shop_balance__usd
FROM new_sts_info
WHERE
sts_create_date <= CAST('${BIZ_TIME}' as date) - interval '1' day
GROUP BY 1, 2
) AS a
WHERE
a.shop_balance__usd < 0
)

,shop_info AS (
SELECT distinct
a.shop_id
,a.shop_status
,a.shop_name
,a.rating_star
,a.sold_total
,a.user_id
,a.user_name
FROM (
SELECT distinct
shop_id
,status AS shop_status
,shop_name
,round(rating_star, 2) AS rating_star
,sold_total
,user_id
,user_name
FROM mp_user.dim_shop__reg_s0_live
WHERE
tz_type = 'local'
AND is_cb_shop = 1
AND status = 1
AND grass_date = CAST('${BIZ_TIME}' as date) - interval '1' day
AND grass_region IN (
'SG', 'MY', 'ID', 'TW', 'TH', 'PH', 'VN',
'BR', 'MX', 'CO', 'CL',
'ES', 'PL'
)
) AS a
JOIN (
SELECT distinct
user_id
FROM mp_user.dim_user__reg_s0_live
WHERE
tz_type = 'local'
AND grass_date = CAST('${BIZ_TIME}' as date) - interval '1' day
AND status = 1
AND grass_region IN (
'SG', 'MY', 'ID', 'TW', 'TH', 'PH', 'VN',
'BR', 'MX', 'CO', 'CL',
'ES', 'PL'
)
) AS b
ON a.user_id = b.user_id
)

,sip_info AS (
SELECT distinct
a.affi_shopid
,a.mst_shopid
FROM (
SELECT
CAST(affi_shopid as bigint) AS affi_shopid
,CAST(mst_shopid as bigint) AS mst_shopid
--,IF(sip_shop_status=3, 1, 0) AS is_offboarded
--,offboard_time -- unixtime BIGINT
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE
grass_region IN (
'SG', 'MY', 'ID', 'TW', 'TH', 'PH', 'VN',
'BR', 'MX', 'CO', 'CL',
'ES', 'PL'
)
AND sip_shop_status != 3
) AS a
JOIN (
SELECT
CAST(shopid as bigint) AS mst_shopid
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE
cb_option = 1 -- cb sip
) AS b
ON a.mst_shopid = b.mst_shopid
)

,order_info AS (
SELECT
grass_region
,shop_id
,seller_id AS seller_userid
,COUNT(distinct order_id) AS l30d_order_cnt
,SUM(gmv_usd) AS l30d_gmv__usd
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE
tz_type = 'local'
AND is_cb_shop = 1
AND grass_region IN (
'SG', 'MY', 'ID', 'TW', 'TH', 'PH', 'VN',
'BR', 'MX', 'CO', 'CL',
'ES', 'PL'
)
AND grass_date >= CAST('${BIZ_TIME}' as date) - interval '32' day
AND date(from_unixtime(create_timestamp)) >= CAST('${BIZ_TIME}' as date) - interval '30' day
AND date(from_unixtime(create_timestamp)) <= CAST('${BIZ_TIME}' as date) - interval '1' day
GROUP BY 1, 2, 3
)

SELECT distinct
a.seller_userid
,b.user_name
,b.shop_id
,b.shop_status
,a.shop_balance__local_currency
,a.shop_balance__usd
,COALESCE(d.l30d_order_cnt, 0) AS l30d_order_cnt
,COALESCE(d.l30d_gmv__usd, 0) AS l30d_gmv__usd

,CAST('${BIZ_TIME}' as date) - interval '1' day AS grass_date
,a.grass_region
FROM (
SELECT
grass_region
,seller_userid
,shop_balance__local_currency
,shop_balance__usd
FROM nagative_balance_shop
) AS a
JOIN (
SELECT distinct
shop_id
,shop_status
,shop_name
,rating_star
,sold_total
,user_id
,user_name
FROM shop_info
) AS b
ON a.seller_userid = b.user_id
JOIN sip_info AS c
ON b.shop_id = c.affi_shopid
LEFT JOIN (
SELECT
grass_region
,shop_id
,seller_userid
,l30d_order_cnt
,l30d_gmv__usd
FROM order_info
) AS d
ON b.shop_id = d.shop_id