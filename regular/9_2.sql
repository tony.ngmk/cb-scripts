WITH
sip_shop AS (
SELECT distinct b.affi_shopid
, b.mst_shopid
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
left join marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b on b.mst_shopid = a.shopid 
where a.cb_option = 0
AND a.country in ('ID')
and b.grass_region != ''
)

SELECT DISTINCT
a.grass_region
, a.shop_id
, u.shop_name
, DATE(CAST(a.topup_create_datetime AS TIMESTAMP)) as topup_date
, SUM(CASE WHEN credit_topup_main_type != 3 THEN topup_amt_usd ELSE 0 END) AS paid_topup_usd
, SUM(CASE WHEN credit_topup_main_type = 3 THEN topup_amt_usd ELSE 0 END) AS manual_free_credit_topup_usd
, SUM(CASE WHEN credit_topup_main_type != 3 THEN topup_amt ELSE 0 END) AS paid_topup
, SUM(CASE WHEN credit_topup_main_type = 3 THEN topup_amt ELSE 0 END) AS manual_free_credit_topup
, CASE WHEN (credit_topup_sub_type_name = 'sip ads drive' AND credit_topup_main_type_name = 'paid credit') THEN 'sip ads drive' ELSE 'normal' END AS credit_topUp
FROM mp_paidads.dwd_advertiser_credit_topup_df__reg_s0_live a
join sip_shop on a.shop_id = sip_shop.affi_shopid
join mp_user.dim_shop__reg_s0_live u on u.shop_id = a.shop_id
WHERE a.grass_date = current_date - interval '1' day
AND DATE(CAST(a.topup_create_datetime AS TIMESTAMP)) >= CURRENT_DATE - INTERVAL '90' DAY 
AND a.tz_type = 'local'
and u.tz_type = 'local'
and u.grass_date = current_date - interval '1' day
AND a.credit_topup_status in (1,2)
GROUP BY 1,2,3,4,9
ORDER BY 1,2,4