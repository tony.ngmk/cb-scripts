--- weekly

WITH dim AS ( 
SELECT DISTINCT
grass_date 
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE grass_date >= current_date-interval'100'day
) 
, buyer_account AS (
SELECT *
FROM (
VALUES 
ROW (32352918, 'TW')
, ROW (174331327, 'MY')
, ROW (216124110, 'ID')
, ROW (413283389,'VN')
, ROW (502225957,'TH')
) t (buyer_id, grass_region)
) 
, sip AS (
SELECT DISTINCT
affi_shopid
, a.country affi_country
, b.country mst_country
, mst_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE cb_option = 0
AND country IN ('TW', 'ID', 'MY','VN','TH')
) b 
ON a.mst_shopid = b.shopid
WHERE a.country IN ('MY', 'SG', 'BR', 'PH','TH','VN','MX','CL','CO','TW')
AND a.grass_region IN ('MY', 'SG', 'BR', 'PH','TH','VN','MX','CL','CO','TW') 
AND sip_shop_status!=3
) 
, raw AS (
SELECT DISTINCT
oversea_orderid
, max(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE date(from_unixtime(ctime)) >= current_date-interval'100'day 
AND local_country IN ('tw','my','id','vn','th') 
AND oversea_country in ('my','sg','vn','ph','th','br','mx','co','cl','tw')
GROUP BY 1
) 

, o as (
SELECT shop_id 
, order_id 
, order_be_status_id
, create_datetime
, grass_region
, cancel_timestamp as cancel_time 
, date(split(create_datetime,' ')[1]) create_time 
, create_timestamp
, cancel_reason_id cancel_reason
, is_cb_shop 
, cancel_user_id
, order_sn 
, is_net_order
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE date(from_unixtime(create_timestamp)) >= current_date-interval'100'day
AND tz_type = 'local'
AND grass_region IN ('MY', 'SG', 'BR', 'PH','TH','VN','MX','ID','TW','CL','CO','TW')
AND grass_date >= current_date-interval'100'day 
)

, net AS (
SELECT DISTINCT
shop_id
, date(split(create_datetime,' ')[1]) create_time
, count(DISTINCT order_id) gross_order
, count(DISTINCT (CASE WHEN is_net_order = 1 THEN order_id ELSE null END)) net_order
FROM o 
GROUP BY 1, 2
) 
, cancel AS (
SELECT DISTINCT
o1.shop_id shopid
, CASE WHEN o1.grass_region IN ('SG', 'MY', 'PH', 'TW') THEN date(from_unixtime(o1.cancel_time)) 
WHEN o1.grass_region IN ('TH', 'ID', 'VN') THEN date((from_unixtime(o1.cancel_time) - INTERVAL '1' HOUR))
WHEN o1.grass_region = 'BR' THEN date((from_unixtime(o1.cancel_time) - INTERVAL '11' HOUR)) 
WHEN o1.grass_region = 'MX' THEN date((from_unixtime(o1.cancel_time) - INTERVAL '14' HOUR)) 
WHEN o1.grass_region = 'CL' THEN date(CAST(format_datetime(from_unixtime(o1.cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN o1.grass_region = 'CO' THEN date(CAST(format_datetime(from_unixtime(o1.cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
END AS create_time
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302) THEN o1.orderid 
WHEN (NOT (o1.grass_region IN ('TW'))) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302) THEN o1.orderid 
ELSE null END)) cancel_order
, count(DISTINCT (CASE WHEN o1.grass_region <> 'TW' AND o2.grass_region = 'TW' AND o2.cancel_reason IN (1, 2, 3, 200, 201, 204, 302) AND local_orderid > 0 AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302) THEN o1.orderid 
WHEN (NOT (o1.grass_region IN ('TW'))) AND o2.grass_region <>'TW' AND o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302) AND local_orderid > 0 AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302) THEN o1.orderid 
ELSE null END)) cancel_order_v2
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302) AND local_orderid > 0 THEN o1.orderid 
WHEN (NOT (o1.grass_region IN ('TW'))) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302) AND local_orderid > 0 THEN o1.orderid 
ELSE null END)) cancel_order_sync_suc
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND ((NOT (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302))) OR o2.cancel_reason IS NULL) AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302) AND local_orderid > 0 THEN o1.orderid
WHEN (NOT (o1.grass_region IN ('TW'))) AND o2.grass_region IN ('TW') AND ((NOT (o2.cancel_reason IN (1, 2, 3, 200, 201, 204, 302))) OR o2.cancel_reason IS NULL) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302) AND local_orderid > 0 THEN o1.orderid 
WHEN (NOT (o1.grass_region IN ('TW'))) AND (NOT (o2.grass_region IN ('TW'))) AND ((NOT (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302))) OR o2.cancel_reason IS NULL) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302) AND local_orderid > 0 THEN o1.orderid 
ELSE null END)) sync_failed_acl1_acl2
, count(DISTINCT (CASE WHEN o1.cancel_reason = 204 AND o2.cancel_reason not in (1, 2, 3, 4, 204, 301, 302) AND l.ar_time IS NOT NULL AND local_orderid > 0 THEN o1.orderid
WHEN o1.cancel_reason = 302 AND l.pu_time IS NOT NULL AND o2.cancel_reason not in (1, 2, 3, 4, 204, 301, 302) AND local_orderid > 0 THEN o1.orderid 
ELSE null END)) acl_order
, count(DISTINCT (CASE WHEN o1.cancel_reason IN (204, 302) AND ba.buyer_id IS NOT NULL AND local_orderid > 0 THEN o1.orderid ELSE null END)) seller_fault_v2
FROM
raw
LEFT JOIN (
SELECT DISTINCT
order_id orderid 
, cancel_reason 
, grass_region 
, shop_id 
, create_time 
, cancel_time 
FROM o
WHERE is_cb_shop = 1 
) o1 
ON o1.orderid = raw.oversea_orderid
LEFT JOIN (
SELECT DISTINCT
order_id orderid
, cancel_time
, cancel_reason
, grass_region
FROM o 
WHERE
is_cb_shop = 0 
AND grass_region IN ('TW', 'MY', 'ID','VN','TH') 
) o2
ON o2.orderid = raw.local_orderid
LEFT JOIN (
SELECT DISTINCT
order_id
, order_sn
, cancel_user_id
FROM
o
WHERE grass_region in ('MY','TW','ID','VN','TH') 
AND is_cb_shop=0 
) o3
ON o3.order_id = o2.orderid
LEFT JOIN buyer_account ba
ON ba.buyer_id = cancel_user_id
AND ba.grass_region = o2.grass_region
LEFT JOIN (
SELECT DISTINCT
orderid
, min(CASE WHEN new_status = 1 THEN ctime ELSE null END) ar_time
, min(CASE WHEN new_status = 2 THEN ctime ELSE null END) pu_time
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live
WHERE grass_region IN ('MY', 'SG', 'BR', 'PH','TH','VN','MX','CL','CO','TW')
AND date(from_unixtime(ctime))>= current_date-interval'100'day 
GROUP BY 1
) l
ON l.orderid = raw.local_orderid
WHERE date(from_unixtime(o1.cancel_time)) >= current_date-interval'100'day
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
shopid
, CASE WHEN grass_region IN ('SG', 'MY', 'PH', 'TW') THEN date(from_unixtime(a.mtime)) 
WHEN grass_region IN ('TH', 'ID', 'VN') THEN date((from_unixtime(a.mtime) - INTERVAL '1' HOUR)) 
WHEN grass_region = 'BR' THEN date((from_unixtime(a.mtime) - INTERVAL '11' HOUR))
WHEN grass_region = 'MX' THEN date((from_unixtime(a.mtime ) - INTERVAL '14' HOUR)) 
WHEN a.grass_region = 'CL' THEN date(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN a.grass_region = 'CO' THEN date(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
END AS mtime
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND reason IN (1, 2, 3, 103, 105) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
WHEN grass_region <> 'TW' AND reason IN (1, 2, 3, 103, 105, 107) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
ELSE null END)) RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
order_id orderid
, cancel_reason
, create_time
FROM o 
WHERE is_cb_shop = 1
AND grass_region IN ('MY', 'SG', 'BR', 'PH','TH','VN','MX','ID','TW','CL','CO')
) b 
ON a.orderid = b.orderid
WHERE date(from_unixtime(mtime)) >= current_date-interval'100'day 
AND a.cb_option = 1 
AND grass_region IN ('MY', 'SG', 'BR', 'PH','TH','VN','MX','ID','TW','CL','CO')
GROUP BY 1, 2
) 


SELECT DISTINCT
-- , sip.affi_country
sip.mst_country
, min(dim.grass_date) min_grass_date
, max(dim.grass_date) max_grass_date 
, week(dim.grass_date) week 
, sum(gross_order) gross_order
, sum(net_order) net_order
, sum(cancel_order) cancel_order
, sum(cancel_order_sync_suc) c