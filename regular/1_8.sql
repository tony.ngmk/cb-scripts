WITH sip as
(
select distinct cast(affi.affi_shopid as bigint) as affi_shopid
-- , affi.mst_shopid
-- , mst.country as mst_country
-- , affi.country as affi_country
-- , mst.cb_option
from
(
select distinct cast(shopid as bigint) as shopid
, country
, cb_option
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
where cb_option = 0
and country = 'TW'
) mst
join 
(
select distinct cast(affi_shopid as bigint) as affi_shopid
, affi_username
, cast(mst_shopid as bigint) as mst_shopid
, country
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status != 3
and grass_region = 'MY'
) affi
on mst.shopid = affi.mst_shopid
),

ord_sync as
(
select distinct local_orderid, oversea_orderid
from marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
where local_orderid > 0
and upper(local_country) = 'TW'
and upper(oversea_country) = 'MY'
)

select distinct a_ord.create_date
, p_ord.shop_id as p_shopid
, p_ord.order_sn as p_ordersn
, p_ord.order_id as p_orderid
, p_ord.logistics_status as p_logistics_status
, p_ord.order_be_status as p_be_status
, a_ord.shop_id as a_shopid
, a_ord.order_sn as a_ordersn
, a_ord.order_id as a_orderid
, a_ord.logistics_status as a_logistics_status
, a_ord.order_be_status as a_be_status
, a_ord.gmv as gmv_local_currency
, a_ord.gmv_usd
, a_ord.buyer_id
, from_unixtime(sls2.lm_inbound_time) as lm_inbound_time
, sls1.consignment_no
, sls1.lm_tracking_number
from
(
select distinct order_id
, order_sn
, DATE(date_parse(create_datetime, '%Y-%m-%d %T')) as create_date
, shop_id
, logistics_status
, order_be_status
, gmv
, gmv_usd
, buyer_id
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_region = 'MY'
and grass_date >= DATE '2021-12-01'
and tz_type = 'local'
and DATE(date_parse(create_datetime, '%Y-%m-%d %T')) >= DATE '2021-12-01'
and payment_method_id = 6
and is_cb_shop = 1
) a_ord
join sip
on a_ord.shop_id = sip.affi_shopid
left join ord_sync
on a_ord.order_id = ord_sync.oversea_orderid
left join
(
select distinct order_id
, order_sn
, DATE(date_parse(create_datetime, '%Y-%m-%d %T')) as create_date
, shop_id
, logistics_status
, order_be_status
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_region = 'TW'
and grass_date >= DATE '2021-11-30'
and tz_type = 'local'
and DATE(date_parse(create_datetime, '%Y-%m-%d %T')) >= DATE '2021-11-30'
and is_cb_shop = 0
) p_ord
on ord_sync.local_orderid = p_ord.order_id
left join
(
select distinct ordersn, consignment_no, lm_tracking_number, log_id
from sls_mart.shopee_sls_logistic_my_db__logistic_request_tab_lfs_union_tmp
where grass_date = DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
and tz_type = 'local'
) sls1
on a_ord.order_sn = sls1.ordersn
left join
(
select distinct log_id
, min(IF(status in (8, 9, 10, 13) AND substr(tracking_code, 1, 2) NOT in ('F0', 'F1', 'F2'), update_time, NULL)) AS lm_inbound_time
from sls_mart.shopee_sls_logistic_my_db__logistic_tracking_tab_lfs_union_tmp
where grass_date = DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
and tz_type = 'local'
group by 1
) sls2
on sls1.log_id = sls2.log_id