WITH dim AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE grass_date >= date'2021-03-01'
AND grass_date <= CURRENT_DATE-INTERVAL'1'DAY 
AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
) 
, cb AS (
SELECT DISTINCT 
CAST(child_shopid AS INTEGER) AS child_shopid
, grass_region
, gp_account_name
, gp_account_owner
, gp_account_owner_userrole_name
, child_account_name 
, CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%css%' THEN 'Long Tail'
ELSE 'Others' END seller_type
FROM regbd_sf.cb__seller_index_tab
WHERE grass_region in ('TW','MY','BR','SG') 
) 
, o as (
SELECT DISTINCT 
shop_id shopid 
, date(split(create_datetime,' ')[1]) create_time
, order_id orderid 
, order_be_status_id
, cancel_reason_id
, grass_region
, cancel_timestamp cancel_time
, is_net_order
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE date(from_unixtime(create_timestamp) ) >= date'2021-03-01'
AND tz_type='local'
AND is_cb_shop=1 
AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
AND grass_date >= date'2021-03-01' 
) 

, sip AS (
SELECT DISTINCT
affi_shopid
, a.country as affi_country 
, mst_shopid
, b.country as mst_country 
, cb_option
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
, cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) b 
ON a.mst_shopid = b.shopid
WHERE grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
AND sip_shop_status!=3
AND b.country <>'SG'
) 
, seller AS (
SELECT DISTINCT
child_shopid
, CASE WHEN gp_account_owner in ('Boey Peng','Jun Li','Crystal Yu','Christine Tang','Cindy Jin','Shelly Zhuo','Zheng Kai','Jolene Wang','Johnny Chen','Echo Zeng') THEN '1.KAM' 
WHEN gp_account_owner_userrole_name LIKE '%PSM%' THEN '2.PSM' 
WHEN gp_account_owner_userrole_name LIKE '%ICB%' THEN '4.ICB' 
WHEN gp_account_owner_userrole_name LIKE '%CS%' THEN '3.CS' 
ELSE '5.Others' END seller_type
FROM regbd_sf.cb__seller_index_tab
WHERE grass_region in ('TW','MY','VN','SG','BR','ID')
) 
, net AS (
SELECT DISTINCT
shopid shop_id
, create_time
, count(DISTINCT orderid) gross_order
, count(DISTINCT (CASE WHEN is_net_order = 1 THEN orderid ELSE null END)) net_order
FROM o 
GROUP BY 1,2 
) 
, cancel AS (
SELECT DISTINCT
shopid
, CASE WHEN grass_region in ('SG','MY','PH','TW') THEN date(from_unixtime(cancel_time)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(cancel_time) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(cancel_time)-interval'11'hour)
WHEN grass_region = 'MX' THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Mexico_City','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'CL' THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'CO' THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'PL' THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP))
WHEN grass_region = 'ES' THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP))
WHEN grass_region = 'FR' THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP))
END AS cancel_time
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND cancel_reason_id IN (1, 2, 3, 200, 201, 204, 302,303) THEN orderid 
WHEN grass_region NOT IN ('TW') AND cancel_reason_id IN (1, 2, 3, 4, 204, 301, 302,303) THEN orderid 
ELSE null END)) cancel_order
FROM o 
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
a.shopid
, CASE WHEN grass_region in ('SG','MY','PH','TW') THEN date(from_unixtime(mtime)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(mtime) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime)-interval'11'hour)
WHEN grass_region = 'MX' THEN DATE(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Mexico_City','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'CL' THEN DATE(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'CO' THEN DATE(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'PL' THEN DATE(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP))
WHEN grass_region = 'ES' THEN DATE(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP))
WHEN grass_region = 'FR' THEN DATE(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP))
END AS mtime
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND reason IN (1, 2, 3, 103, 105) AND (cancel_reason_id = 0 OR cancel_reason_id IS NULL) AND status IN (2, 5) THEN a.orderid 
WHEN grass_region <> 'TW' AND reason IN (1, 2, 3, 103, 105, 107) AND (cancel_reason_id = 0 OR cancel_reason_id IS NULL) AND status IN (2, 5) THEN a.orderid 
ELSE null END)) RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT 
orderid 
, cancel_reason_id
FROM o
) b 
ON a.orderid = b.orderid
WHERE date(from_unixtime(mtime)) >= date'2021-04-01' 
AND cb_option = 1 
AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
GROUP BY 1, 2
) 
SELECT DISTINCT
dim.grass_date
, year(dim.grass_date) year 
, month(dim.grass_date) month
, CASE WHEN cb_option = 0 THEN 'LOCAL SIP' ELSE 'CB SIP' END AS SHOP_TYPE 
, sum(gross_order) gross_order
, sum(net_order) net_order
, sum(cancel_order) cancel_order
, sum(RR_order) RR_order
FROM dim
INNER JOIN sip 
ON dim.grass_region = sip.affi_country
LEFT JOIN net 
ON sip.affi_shopid = net.shop_id
AND dim.grass_date = net.create_time
LEFT JOIN cancel 
ON sip.affi_shopid = cancel.shopid 
AND dim.grass_date = cancel.cancel_time
LEFT JOIN rr 
ON sip.affi_shopid = rr.shopid
AND dim.grass_date = rr.mtime
LEFT JOIN seller 
ON sip.mst_shopid = CAST(seller.child_shopid AS int)
LEFT JOIN cb 
ON cb.child_shopid = sip.mst_shopid 
GROUP BY 1,2,3,4