WITH
sip AS (
SELECT DISTINCT
a.affi_shopid
, a.affi_itemid
, a.affi_country
, a.mst_shopid
, a.mst_itemid
, b.country mst_country
, b.cb_option
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON (a.mst_shopid = b.shopid)
inner join marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live affi on (a.affi_shopid = affi.affi_shopid)
where b.cb_option=0 
and b.country ='ID'
AND A.AFFI_COUNTRY IN ('SG','MY','PH','TH','VN','BR','MX','CO','CL','TW') 
AND affi.COUNTRY IN ('SG','MY','PH','TH','VN','BR','MX','CO','CL','TW')
and affi.sip_shop_status<>3
) ,
sku AS (
SELECT DISTINCT grass_date
, a.shop_id shopid 
, item_id itemid
, status item_status
, stock item_stock
, is_cb_shop
-- , grass_date
FROM
mp_item.dim_item__reg_s0_live a inner JOIN 
( SELECT distinct affi_shopid shop_id from sip
union all
SELECT distinct mst_shopid shop_id from sip
) b on a.shop_id=b.shop_id
WHERE grass_date = CURRENT_DATE - INTERVAL '1' DAY
and grass_region IN ('SG','MY','PH','TH','ID','VN','BR','MX','CO','CL','TW') 
and tz_type='local'
) 

, affi AS (
SELECT DISTINCT
affi_country
, affi_itemid
, item_status
, item_stock
, mst_itemid
,a.grass_date
, cb_option
, shop_status
FROM
(sip

INNER JOIN (
SELECT DISTINCT shop_id AS shopid 
, grass_date
, case when (((status = 1) AND (user_status = 1)) AND ((is_holiday_mode = 0) OR (is_holiday_mode IS NULL))) then 'Y'
else 'N' end as shop_status 
FROM
regcbbi_others.shop_profile
WHERE grass_date = CURRENT_DATE - INTERVAL '1' DAY AND is_cb_shop=1 AND grass_region IN ('SG','MY','PH','TH','ID','VN','BR','MX','CO','CL','TW')
) a ON (sip.affi_shopid = a.shopid))
LEFT JOIN sku ON (sip.affi_itemid = sku.itemid) and sku.grass_date=a.grass_date
WHERE SKU.is_cb_shop=1 
) 

, mst AS (
SELECT DISTINCT
mst_itemid
, item_status
, item_stock
, mst_country
, u.grass_date
FROM
(sip
join (select distinct shop_id
, grass_date
from regcbbi_others.shop_profile 
where grass_date= CURRENT_DATE - INTERVAL '1' DAY and grass_region ='ID' AND is_cb_shop=0 and user_status=1 and status=1 
) U ON U.shop_id=SIP.MST_SHOPID 
INNER JOIN sku ON (sip.mst_itemid = sku.itemid) and u.grass_date=sku.grass_date )
where sku.is_cb_shop=0 
) 
SELECT 
mst_country
, affi.affi_country
, affi.grass_date AS grass_date
, count(DISTINCT mst.mst_itemid ) overall_psku
, count(DISTINCT case when shop_status = 'Y' then mst.mst_itemid else null end ) ashop_live_psku
, count(DISTINCT case when shop_status = 'Y' then affi_itemid else null end ) asku
, COUNT(DISTINCT CASE WHEN SHOP_STATUS='Y' AND AFFI.AFFI_ITEMID = 0 THEN MST.MST_ITEMID ELSE NULL END ) SYNC_failed
, count(DISTINCT case when shop_status = 'N' then mst.mst_itemid else null end ) unlive_psku
, count(DISTINCT case when shop_status = 'Y' and affi.item_status = 1 and affi.item_stock > 0 then affi_itemid else null end ) live_sku
FROM
(mst
left JOIN affi ON (affi.mst_itemid = mst.mst_itemid) and affi.grass_date=mst.grass_date ) 
WHERE ((mst.item_status = 1) AND (mst.item_stock > 0)) and affi.affi_country in ('SG','MY','PH','TH','ID','VN','BR','MX','CO','CL','TW')
GROUP BY 1, 2, 3


