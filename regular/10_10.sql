WITH
o AS (
SELECT DISTINCT
o2.orderid
, o1.shop_id SHOPID
, o1.create_time
, o1.cancel_time
, o2.cancel_reason
-- , o1.status_ext
, o1.is_net_order
, o1.complete_time
, o1.grass_date
FROM
((
SELECT DISTINCT
order_id
, shop_id
, (CASE WHEN grass_region = 'CL' then from_unixtime(create_timestamp, 'America/Santiago') when grass_region = 'MX' then from_unixtime(create_timestamp, 'America/Mexico_City') when grass_region = 'CO' then from_unixtime(create_timestamp, 'America/Bogota') when (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) create_time
, (CASE WHEN grass_region = 'CL' then from_unixtime(cancel_timestamp, 'America/Santiago') when grass_region = 'MX' then from_unixtime(cancel_timestamp, 'America/Mexico_City') when grass_region = 'CO' then from_unixtime(cancel_timestamp, 'America/Bogota') WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) cancel_time
-- , order_be_status_id status_ext
, is_net_order
, (CASE WHEN grass_region = 'CL' then from_unixtime(cancel_timestamp, 'America/Santiago') when grass_region = 'MX' then from_unixtime(cancel_timestamp, 'America/Mexico_City') when grass_region = 'CO' then from_unixtime(cancel_timestamp, 'America/Bogota') WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) complete_time
, (CASE WHEN grass_region = 'CL' then from_unixtime(create_timestamp, 'America/Santiago') when grass_region = 'MX' then from_unixtime(create_timestamp, 'America/Mexico_City') when grass_region = 'CO' then from_unixtime(create_timestamp, 'America/Bogota') WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) grass_date FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ((is_cb_shop = 0) AND (tz_type = 'local'))
) o1
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'MY'))
) o2 ON (o1.order_id = o2.orderid))
) 
, r AS (
SELECT DISTINCT
orderid
, shopid
, reason
, status
, "from_unixtime"(ctime) ctime
, "from_unixtime"(mtime) mtime
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'MY'))
) 
SELECT
(current_date - INTERVAL '1' DAY) update_time
, grass_region
, "count"(DISTINCT shopid) shop_number
, "sum"(live_sku) sku_number
FROM
(
SELECT DISTINCT
grass_region
, a.shopid
, live_sku
, a.username
, COALESCE("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)), 0) NFR_included_cancellations_30d
, COALESCE("count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END)), 0) NFR_included_rr_30d
, COALESCE("count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND o.is_net_order = 1) THEN o.orderid ELSE null END)), 0) net_orders_30d
, (COALESCE("count"(DISTINCT (CASE WHEN (o.create_time BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) THEN o.orderid ELSE null END)), 0) / DECIMAL '30.00') gross_orders_30d
, COALESCE(TRY((("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) * DECIMAL '1.000000') / (("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) + "count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END))) + "count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND o.is_net_order = 1) THEN o.orderid ELSE null END))))), 0) cr_l90d
FROM
(((
SELECT DISTINCT
u.shopid
, u.username
, u.main_category
, u.sub_category
, u.is_preferred_shop
, u.rating_star
, u.response_rate
, COALESCE(live_sku, 0) live_sku
, COALESCE(supportive_sku, 0) supportive_sku
, COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20088.enabled') AS bigint), 0) have_channel_spxmarketplace
, COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20007.enabled') AS bigint), 0) have_channel_pos
, COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20011.enabled') AS bigint), 0) have_channel_jnt
, COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20010.enabled') AS bigint), 0) have_channel_edhl
, COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20021.enabled') AS bigint), 0) have_channel_njv
, COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20023.enabled') AS bigint), 0) have_channel_citylink
, COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.2000.enabled') AS bigint), 0) have_channel_mask
, e.state address_state
, (CAST(IF((e_sku IS NULL), DECIMAL '0.0', e_sku) AS double) / live_sku) e_sku_per
, IF(((tt.l2_cat IS NULL) AND (NOT (main_category IN ('Tickets & Vouchers')))), 'Y', 'N') check_cat
, u.is_official_shop
, (CASE WHEN (e.state IN ('Johor', 'Kedah', 'Kelantan', 'Kuala Lumpur', 'Melaka', 'Negeri Sembilan', 'Pahang', 'Penang', 'Perak', 'Perlis', 'Putrajaya', 'Selangor', 'Terengganu')) THEN 'Y' ELSE 'N' END) address_meet
, (CASE WHEN (((((COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20088.enabled') AS bigint), 0) + COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20007.enabled') AS bigint), 0)) + COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20011.enabled') AS bigint), 0)) + COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20010.enabled') AS bigint), 0)) + COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20021.enabled') AS bigint), 0) + COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20023.enabled') AS bigint), 0)) > 0) THEN 'Y' ELSE 'N' END) shop_support_channel
, (CASE WHEN (COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.2000.enabled') AS bigint), 0)) = 1 THEN 'Y' ELSE 'N' END) mask_channel
, IF((warehouse_id IS NULL), 'Y', 'N') NOT_WH_SHOP
, (CASE WHEN (smt.shopid IS NULL) THEN 'Y' ELSE 'N' END) not_smt_shop
, (CASE WHEN (sip.shopid IS NOT NULL) THEN 'Y' ELSE 'N' END) SIP_SHOP
, u.grass_region
FROM
(((((((((
SELECT DISTINCT
shop_id shopid
, shop_level1_category main_category
, shop_level2_category sub_category
, is_official_shop
, active_item_with_stock_cnt
, user_name username
, is_preferred_shop
, rating_star
, response_rate
, grass_region
FROM
regcbbi_others.shop_profile
WHERE ((((((status = 1) AND (user_status = 1)) AND (is_holiday_mode = 0)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (grass_region = 'MY')) AND (is_cb_shop = 0)) 
--and date(from_unixtime(last_login_timestamp)) >= current_date-interval'14'day 
) u
INNER JOIN (
SELECT DISTINCT
shop_id
--, address_state
FROM
mp_user.dim_user__reg_s0_live
WHERE ((grass_date = (current_date - INTERVAL '1' DAY)) AND (grass_region = 'MY'))
) u1 ON (u1.shop_id = u.shopid))
LEFT JOIN (
SELECT DISTINCT
shop_id
, warehouse_id
FROM
sls_mart.shopee_logistic_my_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
) wms ON (wms.shop_id = u.shopid))
JOIN marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live d ON (u.shopid = d.shopid)
LEFT JOIN (SELECT id, userid, state FROM marketplace.shopee_account_db__buyer_address_tab__reg_daily_s0_live WHERE country = 'MY' AND status <> 0) e ON e.id = d.extinfo.pickup_address_id AND e.userid = d.userid)
LEFT JOIN (
SELECT
shopid
, "count"(DISTINCT itemid) live_sku
, "count"(DISTINCT (CASE WHEN (((((have_channel_pos = 1) OR (have_channel_jnt = 1)) OR (have_channel_edhl = 1)) OR (have_channel_spxmarketplace = 1)) OR (have_channel_njv = 1) OR (have_channel_citylink = 1) OR (have_channel_mask = 1)) THEN itemid ELSE null END)) supportive_sku
FROM
(
SELECT DISTINCT
shopid
, itemid
, "sum"(DISTINCT (CASE WHEN (((CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20007.enabled') AS varchar) = 'true') OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20007.enabled') AS boolean) = true)) OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20007.enabled') AS int) = 1)) THEN 1 ELSE 0 END)) have_channel_pos
, "sum"(DISTINCT (CASE WHEN (((CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20011.enabled') AS varchar) = 'true') OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20011.enabled') AS boolean) = true)) OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20011.enabled') AS int) = 1)) THEN 1 ELSE 0 END)) have_channel_jnt
, "sum"(DISTINCT (CASE WHEN (((CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20010.enabled') AS varchar) = 'true') OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20010.enabled') AS boolean) = true)) OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20010.enabled') AS int) = 1)) THEN 1 ELSE 0 END)) have_channel_edhl
, "sum"(DISTINCT (CASE WHEN (((CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20088.enabled') AS varchar) = 'true') OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20088.enabled') AS boolean) = true)) OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20088.enabled') AS int) = 1)) THEN 1 ELSE 0 END)) have_channel_spxmarketplace
, "sum"(DISTINCT (CASE WHEN (((CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20021.enabled') AS varchar) = 'true') OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20021.enabled') AS boolean) = true)) OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20021.enabled') AS int) = 1)) THEN 1 ELSE 0 END)) have_channel_njv
, "sum"(DISTINCT (CASE WHEN (((CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20023.enabled') AS varchar) = 'true') OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20023.enabled') AS boolean) = true)) OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.20023.enabled') AS int) = 1)) THEN 1 ELSE 0 END)) have_channel_citylink
, "sum"(DISTINCT (CASE WHEN (((CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.2000.enabled') AS varchar) = 'true') OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.2000.enabled') AS boolean) = true)) OR (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.2000.enabled') AS int) = 1)) THEN 1 ELSE 0 END)) have_channel_mask
FROM
marketplace.shopee_item_v4_db__item_v4_tab__reg_daily_s0_live
WHERE ((((grass_region = 'MY') AND (cb_option = 0)) AND (status = 1)) AND (stock > 0))
GROUP BY 1, 2
) 
GROUP BY 1
) l ON (l.shopid = u.shopid))
LEFT JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live SIP ON (sip.shopid = u.shopid))
LEFT JOIN ((
SELECT "split"('Electronic Cigarettes', ',') bar

) 
CROSS JOIN UNNEST(bar) t (l2_cat)) tt ON (u.sub_category = TRY_CAST(tt.l2_cat AS varchar)))
LEFT JOIN (
SELECT DISTINCT shopid
FROM
regcbbi_others.shopee_regional_cb_team__mysip_migration_smt_exclude
WHERE (ingestion_timestamp = (SELECT "max"(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__mysip_migration_smt_exclude
))
) smt ON (CAST(smt.shopid AS bigint) = u.shopid))
LEFT JOIN (
SELECT
"count"(DISTINCT item_id) e_sku
, shop_id shopid
FROM
mp_item.dim_item__reg_s0_live
WHERE ((((((grass_region = 'MY') and tz_type='local' AND (is_cb_shop = 0)) AND (level2_category_id = 18911)) AND (status = 1)) AND (stock > 0)) AND (grass_date = (current_date - INTERVAL '1' DAY)))
GROUP BY 2
) check_shop ON (check_shop.shopid = u.shopid))
WHERE (d.grass_region != 'O')
) a
LEFT JOIN o ON (o.shopid = a.shopid))
LEFT JOIN r ON (o.orderid = r.orderid))
WHERE ((((((SIP_SHOP = 'N') AND (shop_support_channel = 'Y')) AND (address_meet = 'Y')) AND (NOT_WH_SHOP = 'Y')) AND (check_cat = 'Y')) AND (supportive_sku > 0)) and not_smt_shop='Y' and e_sku_per < 0.5 AND mask_channel = 'Y'

GROUP BY 1, 2, 3, 4
) 
WHERE 
(gross_orders_30d >= DECIMAL '0.1') AND
(cr_l90d <= DECIMAL '0.03')
GROUP BY 1, 2