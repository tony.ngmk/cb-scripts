WITH new_sts_info AS (
SELECT
UPPER('sg') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_sg_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_sg_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('my') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_my_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_my_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('id') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_id_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_id_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('tw') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_tw_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_tw_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('th') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_th_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_th_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('ph') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_ph_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_ph_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('vn') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_vn_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_vn_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('br') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_br_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_br_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('mx') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_mx_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_mx_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('co') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_co_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_co_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('cl') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_cl_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_cl_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('pl') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_pl_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_pl_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id

UNION
SELECT
UPPER('es') AS grass_region
,a.item_type
,COALESCE(a.reason_code_id, 404) AS reason_code_id
,a.amount_local_currency
,b.seller_userid
,a.payment_status
,a.order_id
,date(from_unixtime(a.ctime)) AS sts_create_date
,a.remark
,a.is_cb
,a.account_id
,a.source_entity_id
FROM (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(source_entity_id as bigint) AS source_entity_id
,CAST(json_extract(ext_info, '$.order_id') as bigint) AS order_id
,item_type 
,COALESCE(CAST(json_extract(ext_info, '$.reason_code_id') as bigint), 404) AS reason_code_id
,round(CAST(amount as double) / 100000, 2) AS amount_local_currency
,item_status AS payment_status
,ctime
,CAST(json_extract(ext_info, '$.remark') as varchar) AS remark
,is_cb
FROM marketplace.shopee_order_accounting_settlement_es_db__billing_item_tab__reg_continuous_s0_live
) AS a
JOIN (
SELECT distinct
CAST(account_id as bigint) AS account_id
,CAST(rule_id as bigint) AS rule_id
,CAST(external_account_id as bigint) AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_es_db__account_tab__reg_continuous_s0_live
) AS b
ON a.account_id = b.account_id
)

SELECT distinct
account_id
,source_entity_id
,item_type
,(CASE
when item_type = 1 then 'escrow'
when source_entity_id = order_id then 'adjustment_order'
else 'adjustment_shop'
END) AS payment_type
,reason_code_id
,amount_local_currency
,seller_userid
,is_cb
,order_id
,sts_create_date
,payment_status
,remark

,current_date - interval '1' day AS grass_date
,grass_region
FROM new_sts_info
WHERE
sts_create_date <= current_date - interval '1' day