with price_config AS (SELECT grass_region
, currency
, CAST(sip_exchange AS DOUBLE) sip_exchange --> USD/grass_region currency
FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2
WHERE cb_option = '1'
AND ingestion_timestamp = (SELECT MAX(ingestion_timestamp) max FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2)
)

-- all settlement related amount converted to USD using SIP system exchange rates
, o AS (SELECT DISTINCT 
o.wh_outbound_date
, o.grass_date AS create_date
, o.mst_country
, o.grass_region
, o.order_id
, o.order_sn
, o.payment_method_id
, o.logistics_status_id
, o.asf_sls
, o.actual_buyer_paid_shipping_fee
, o.actual_shipping_rebate_by_shopee_amt
, o.pv_rebate_by_shopee_amt
, o.sv_rebate_by_shopee_amt
, o.card_rebate_by_bank_amt
, o.card_rebate_by_shopee_amt
, o.coin_used_cash_amt
, o.pv_coin_earn_by_shopee_amt
, o.sv_coin_earn_by_shopee_amt
, o.actual_shipping_rebate_by_seller_amt
, o.seller_snapshot_discount_shipping_fee
, o.escrow_to_seller_amt
, o.gmv
, o.gmv_usd
, o.pv_coin_earn_by_seller_amt
, o.sv_coin_earn_by_seller_amt
, o.pv_rebate_by_seller_amt
, o.sv_rebate_by_seller_amt
, o.seller_txn_fee --platform charge SIP
, o.seller_txn_fee_shipping_fee --platform charge SIP
, o.buyer_txn_fee 
, o.service_fee --platform charge SIP
, o.shop_margin
, o.refund_amount
, o.tax
, o.tax_exemption_amt
, o.actual_weight

-- stmt related info
, order_adj_type
, order_adj_amount
, o.sip_settlement --order level stmt 
, sip_seller_comm_fee -- SIP charge seller
, sip_seller_service_fee -- SIP charge seller
, pc1.sip_exchange AS stmt_exchange
, pc1.currency AS stmt_currency
, pc2.sip_exchange AS mst_exchange
, pc2.currency AS mst_currency
, pc3.sip_exchange AS affi_exchange
, pc3.currency AS affi_currency
, o.offline_exchange AS offline_ord_exchange
, o.offline_currency AS offline_ord_currency
, CASE WHEN pc4.currency IS NULL THEN pc5.sip_exchange 
ELSE pc4.sip_exchange
END AS offline_sku_exchange
, CASE WHEN pc4.currency IS NULL THEN pc5.currency 
ELSE pc4.currency
END AS offline_sku_currency 
, o.sip_seller_comm_rate
, o.sip_seller_service_rate
, o.order_fe_status
, SUM(item_rebate_by_seller_amt) AS item_rebate_by_seller_amt
, SUM(item_rebate_by_shopee_amt) AS item_rebate_by_shopee_amt
, SUM(item_amount) AS item_amount
, SUM(commission_fee) AS commission_fee --platform charge SIP
, SUM(i.service_fee) AS service_fee_item --platform charge SIP
, SUM(item_tax_amt) AS item_tax_amt
, SUM(item_tax_exemption_amt) AS item_tax_exemption_amt
, SUM(item_weight_pp*item_amount) AS item_weight_pp
, SUM(item_price_pp*item_amount) AS item_price_pp
, SUM(order_price_pp*item_amount) AS order_price_pp
, SUM(IF(mst_promo_price = 0,mst_orig_price,mst_promo_price)*item_amount) AS mst_dp 
, SUM(mst_hpfs*item_amount) AS mst_hpfs
, SUM(affi_hpfn*item_amount) AS affi_hpfn
, SUM(IF(i.kind>0,i.sip_settlement_pp*item_amount,0)) AS settlement_campaign -- campaign sku stmt
, SUM(campaign_price*item_amount) AS campaign_price
, MAX(i.kind) AS sku_off_kind
, SUM(IF(item_promotion_source = 'flash_sale',i.gmv,0)) AS cfs_gmv
, SUM(IF(item_promotion_source = 'flash_sale',item_price_pp*item_amount)) AS cfs_item_price
, SUM(IF(item_promotion_source = 'flash_sale',order_price_pp*item_amount)) AS cfs_order_price
, SUM(IF(item_promotion_source = 'flash_sale',item_rebate_by_shopee_amt)) AS cfs_shopee_rebate
, SUM(IF(item_promotion_source = 'flash_sale',affi_hpfn*item_amount)) AS cfs_hpfn
, SUM(IF(item_promotion_source = 'flash_sale',i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS cfs_system_settlement
, SUM(IF(item_promotion_source = 'flash_sale',IF(campaign_price > 0,campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00,sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS cfs_offline_settlement -- offline or stmt curr
, SUM(IF(item_promotion_source = 'shopee',i.gmv,0)) AS campaign_gmv
, SUM(IF(item_promotion_source = 'shopee',item_price_pp*item_amount)) AS campaign_item_price
, SUM(IF(item_promotion_source = 'shopee',order_price_pp*item_amount)) AS campaign_order_price
, SUM(IF(item_promotion_source = 'shopee',item_rebate_by_shopee_amt)) AS campaign_shopee_rebate
, SUM(IF(item_promotion_source = 'shopee',affi_hpfn*item_amount)) AS campaign_hpfn
, SUM(IF(item_promotion_source = 'shopee',i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS campaign_system_settlement
, SUM(IF(item_promotion_source = 'shopee',IF(campaign_price > 0,campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00,i.sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS campaign_offline_settlement
, SUM(IF(is_bundle_deal = 1,i.gmv,0)) AS bundle_gmv
, SUM(IF(is_bundle_deal = 1,item_price_pp*item_amount)) AS bundle_item_price
, SUM(IF(is_bundle_deal = 1,order_price_pp*item_amount)) AS bundle_order_price
, SUM(IF(is_bundle_deal = 1,item_rebate_by_shopee_amt)) AS bundle_shopee_rebate
, SUM(IF(is_bundle_deal = 1,affi_hpfn*item_amount)) AS bundle_hpfn
, SUM(IF(is_bundle_deal = 1,i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS bundle_system_settlement
, SUM(IF(is_bundle_deal = 1,IF(campaign_price > 0,campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00,i.sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS bundle_offline_settlement
FROM regcbbi_others.sip_cn_pnl_order_tab_v2 o 
LEFT JOIN regcbbi_others.sip_cn_pnl_item_tab i ON o.order_id = i.order_id
LEFT JOIN (SELECT *
FROM price_config) pc1 
ON o.stmt_currency = pc1.currency -- USD / stmt curr
LEFT JOIN (SELECT *
FROM price_config) pc2 
ON o.mst_country = pc2.grass_region -- USD / mst curr
LEFT JOIN (SELECT *
FROM price_config) pc3 
ON o.grass_region = pc3.grass_region -- USD / affi curr
LEFT JOIN price_config pc4
ON i.mst_curr = pc4.currency
LEFT JOIN price_config pc5
ON i.mst_country = pc5.grass_region
WHERE (o.wh_inbound_date + interval '1' day < o.cancel_timestamp OR o.cancel_user_id IS NULL)
-- and o.grass_region in ('PH','TH','VN','ID','TW', 'MY')
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54)

-- convert all stmt amount to p currency 
SELECT DISTINCT
grass_region affi_country 
, mst_country 
, wh_outbound_date
, COUNT(DISTINCT order_id) AS orders 
, SUM(gmv_usd) AS gmv_usd 
, count(distinct case when payment_method_id = 6 then order_id else null end ) cod_orders 
, sum(case when payment_method_id = 6 then gmv_usd else null end ) cod_gmv_usd 
, count(distinct case when logistics_status_id = 6 AND payment_method_id = 6 and sls.sls_ops_parcel_confiscated_time is null and sls.sls_ops_terminal_damage_time is null then order_id else null end) cod_failed_orders 
, SUM(IF(logistics_status_id = 6 AND payment_method_id = 6 AND sls.sls_ops_parcel_confiscated_time is null and sls.sls_ops_terminal_damage_time is null
, gmv + pv_rebate_by_shopee_amt + sv_rebate_by_shopee_amt 
- pv_coin_earn_by_seller_amt - sv_coin_earn_by_seller_amt + coin_used_cash_amt 
+ card_rebate_by_bank_amt + card_rebate_by_shopee_amt + item_rebate_by_shopee_amt 
+ actual_shipping_rebate_by_shopee_amt - seller_txn_fee - commission_fee 
- service_fee_item - IF(COALESCE(item_tax_amt,0) = 0,tax,item_tax_amt),0)) AS cod_fail

, SUM(IF(logistics_status_id = 6 AND payment_method_id = 6 AND gmv_usd >= 20 AND sls.sls_ops_parcel_confiscated_time is null and sls.sls_ops_terminal_damage_time is null
, sip_settlement/stmt_exchange*1.00*mst_exchange,0
)) AS cod_offset_mst
, count(distinct case when order_fe_status in ('COMPLETED','CANCELLED') or logistics_status_id in (5,6,11) then order_id else null end) complete_order 
FROM o
LEFT JOIN
(
select ordersn
-- , date(from_unixtime(min(wh_outbound_time))) as wh_outbound_date
, min(sls_ops_parcel_lost_time) as sls_ops_parcel_lost_time
, min(sls_ops_parcel_confiscated_time) as sls_ops_parcel_confiscated_time
, min(sls_ops_terminal_damage_time) as sls_ops_terminal_damage_time
from regcbbi_others.cb_logistics_mart_test
where integrated_channel_origin is not null
and integrated_channel_origin NOT in ('CN', 'KR', 'JP')
group by 1
) sls
on o.order_sn = sls.ordersn
WHERE o.wh_outbound_date between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '69' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
AND mst_country <> 'SG'
GROUP BY 1,2,3