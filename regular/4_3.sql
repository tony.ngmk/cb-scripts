with
o as 
(
select DISTINCT o2.orderid
,o1.shop_id SHOPID
,o1.create_time
,o1.cancel_time
,o2.cancel_reason
,o1.status_ext
,o1.complete_time
,o1.grass_date
from (
select DISTINCT order_id
,shop_id
,(case when (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN date(from_unixtime(create_timestamp))
when (grass_region IN ('TH', 'ID', 'VN')) THEN date((from_unixtime(create_timestamp) - INTERVAL '1' HOUR))
when (grass_region = 'BR') THEN date((from_unixtime(create_timestamp) - INTERVAL '11' HOUR))
end) create_time
,(case when (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN from_unixtime(cancel_timestamp)
when (grass_region IN ('TH', 'ID', 'VN')) THEN (from_unixtime(cancel_timestamp) - INTERVAL '1' HOUR)
when (grass_region = 'BR') THEN (from_unixtime(cancel_timestamp) - INTERVAL '11' HOUR)
end) cancel_time
,order_be_status_id status_ext
,(case when (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN from_unixtime(cancel_timestamp)
when (grass_region IN ('TH', 'ID', 'VN')) THEN (from_unixtime(cancel_timestamp) - INTERVAL '1' HOUR)
when (grass_region = 'BR') THEN (from_unixtime(cancel_timestamp) - INTERVAL '11' HOUR)
end) complete_time
,(case when (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN date(from_unixtime(create_timestamp))
when (grass_region IN ('TH', 'ID', 'VN')) THEN date((from_unixtime(create_timestamp) - INTERVAL '1' HOUR))
when (grass_region = 'BR') THEN date((from_unixtime(create_timestamp) - INTERVAL '11' HOUR))
end) grass_date
from mp_order.dwd_order_all_ent_df__reg_s0_live
where is_cb_shop = 0
and tz_type = 'local'
and grass_region = 'VN'
) o1
inner join (
select DISTINCT orderid
,extinfo.cancel_reason
from marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
where cb_option = 0
and grass_region = 'VN'
) o2
on o1.order_id = o2.orderid
) --, fraud AS (
-- SELECT DISTINCT shopid
-- FROM
-- shopee.shopee_vn_op_team__potential_fraud_vnsip
--)
,r as 
(
select DISTINCT orderid
,shopid
,reason
,status
,from_unixtime(ctime) ctime
,from_unixtime(mtime) mtime
from marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
where cb_option = 0
and grass_region = 'VN'
)
,channel as 
(
select DISTINCT SHOPID
,USERID -- , COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50011.enabled') AS bigint), 0) GHN
-- , COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50021.enabled') AS bigint), 0) SPX
,COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.5001.enabled') AS bigint), 0) mask
from (
select DISTINCT SHOPID
,extinfo
,userid
,grass_region
from marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
where grass_region = 'VN'
)
)
,user as 
(
select DISTINCT shop_id
,(case when (is_official_shop = 1) THEN 'Official Shop'
when (is_preferred_shop = 1) THEN 'Preferred Shop'
ELSE 'Normal'
end) seller_type
,grass_region
,user_name
,user_id
,active_item_cnt
,active_item_with_stock_cnt
,status shop_status
,user_status
,shop_name
,description
,shop_level1_category
,shop_level2_category
,shop_level1_global_be_category
,shop_level2_global_be_category
,is_holiday_mode
from regcbbi_others.shop_profile
where grass_date = (current_date - INTERVAL '1' DAY)
and is_cb_shop = 0
and grass_region = 'VN'
and status = 1
and user_status = 1
and is_holiday_mode = 0
)
,vape as 
(
select DISTINCT shop_id
from user
inner join (
select DISTINCT keyword
from regcbbi_others.sip__vnsip_vape_keyword__reg__s0
where ingestion_timestamp = (SELECT
max(ingestion_timestamp) col_1
FROM
regcbbi_others.sip__vnsip_vape_keyword__reg__s0)
) k
on regexp_like(lower(shop_name), lower(keyword))
or regexp_like(lower(description), lower(keyword))
)
,L30D_LIVE as 
(
select DISTINCT shop_id
from regcbbi_others.shop_profile
where grass_date BETWEEN (current_date - INTERVAL '30' DAY)
and current_date - INTERVAL '1' DAY
and status = 1
and user_status = 1
and is_holiday_mode = 0
)
,ops_check as 
(
select DISTINCT shop_id
,grass_region
,seller_type
,user_name
,user_status
,shop_status
,shop_name
,user_id
,description
-- ,shop_level1_category
-- ,shop_level2_category
,shop_level1_global_be_category
,shop_level2_global_be_category
,active_item_cnt
,active_item_with_stock_cnt
,is_holiday_mode
,count(DISTINCT (CASE
WHEN (date(o.create_time) BETWEEN (current_date - INTERVAL '30' DAY)
AND (current_date - INTERVAL '1' DAY)) THEN o.orderid
ELSE null
END)) gross_orders_30d
,count(DISTINCT (CASE
WHEN (((grass_region = 'TW')
AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302)))
AND (date(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY)
AND (current_date - INTERVAL '1' DAY))) THEN o.orderid
WHEN (((grass_region <> 'TW')
AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)))
AND (date(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY)
AND (current_date - INTERVAL '1' DAY))) THEN o.orderid
ELSE null
END)) NFR_included_cancellations_90d
,count(DISTINCT (CASE
WHEN (((((grass_region = 'TW')
AND (r.reason IN (1, 2, 3, 103, 105)))
AND ((o.cancel_reason = 0)
OR (o.cancel_reason IS NULL)))
AND (r.status IN (2, 5)))
AND (date(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY)
AND (current_date - INTERVAL '1' DAY))) THEN r.orderid
WHEN (((((grass_region <> 'TW')
AND (r.reason IN (1, 2, 3, 103, 105, 107)))
AND ((o.cancel_reason = 0)
OR (o.cancel_reason IS NULL)))
AND (r.status IN (2, 5)))
AND (date(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY)
AND (current_date - INTERVAL '1' DAY))) THEN r.orderid
ELSE null
END)) NFR_included_rr_90d
,count(DISTINCT CASE
WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY)
AND (current_date - INTERVAL '1' DAY))
AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid
ELSE null
END) net_orders_90d
from user
left join o
on (
user.shop_id = o.shopid
)
left join r
on (
o.orderid = r.orderid
)
group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
)
,wms as 
(
select shop_id
from sbs_mart.shopee_scbs_db__shop_tab__reg_daily_s0_live
where country IN ('VN')
and (
(
fbs_tag = 0
and vacation_mode = 0
and shop_status = 1
)
or (
fbs_tag = 1
and vacation_mode = 0
and cb_option = 1
)
or (
fbs_tag = 1
and vacation_mode = 0
and cb_option = 0
)
)
)
,ex as 
(
select DISTINCT CAST(shopid AS bigint) shopid
from regcbbi_others.shopee_regional_cb_team__vnsip_migration_maintain_shop_list
where ingestion_timestamp = (SELECT
max(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__vnsip_migration_maintain_shop_list)
union all 
select DISTINCT CAST(mst_shopid AS bigint) shopid
from regcbbi_others.shopee_regional_cb_team__vn_localsip_tos_shoplist
where ingestion_timestamp = (SELECT
max(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__vn_localsip_tos_shoplist)
union all 
select DISTINCT CAST(shopid AS bigint) shopid
from regcbbi_others.shopee_regional_cb_team__vnsip_migration_maintain_shop_list2
where ingestion_timestamp = (SELECT
max(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__vnsip_migration_maintain_shop_list2)
)
select distinct user_name
,oc.shop_id
,oc.user_id
,active_item_cnt
,active_item_with_stock_cnt
,is_holiday_mode
,SHOP_STATUS
,USER_STATUS
,SELLER_TYPE
,case when (ll.shop_id is not null) then 'Y'
else 'N'
end l30d_live
,(gross_orders_30d / decimal '30.00') l30d_ado
,TRY(((NFR_included_cancellations_90d * decimal '1.000') / ((NFR_included_cancellations_90d + NFR_included_rr_90d) + net_orders_90d))) l90d_cr -- , ghn
-- , spx
,mask mask_channel -- , (CASE WHEN (f.shopid IS NOT NULL) THEN 'N' ELSE 'Y' END) no_fraud_tag
,'skipped' no_fraud_tag
,shop_name
,description
-- ,shop_level1_category
-- ,shop_level2_category
,shop_level1_global_be_category
,shop_level2_global_be_category
from OPS_CHECK OC
inner join CHANNEL CL
on CL.SHOPID = OC.shop_id
inner join L30D_LIVE ll
on ll.shop_id = oc.shop_id -- LEFT JOIN FRAUD F ON (CAST(F.shopid AS bigint) = OC.SHOP_iD)
left join wms
on wms.shop_id = oc.shop_id
left join vape v
on v.shop_id = oc.shop_id
left join ex
on ex.shopid = oc.shop_id
where wms.shop_id is null
and (gross_orders_30d / decimal '30.00') >= decimal '0.03' -- AND (f.shopid IS NULL)
and v.shop_id is null
and TRY(((NFR_included_cancellations_90d * decimal '1.000') / ((NFR_included_cancellations_90d + NFR_included_rr_90d) + net_orders_90d))) <= decimal '0.03'
and mask = 1
and shop_level1_global_be_category != 'Tickets, Vouchers & Services'
and ex.shopid is null
-- limit 100

