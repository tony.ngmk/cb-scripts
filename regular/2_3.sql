WITH base_data AS (
SELECT
grass_date
,grass_region
,SUM(shop_balance__usd) AS total_negative_balance__usd
,COUNT(distinct shop_id) AS negative_balance_shop_cnt
FROM (
SELECT
seller_userid
,user_name
,shop_id
,shop_status
,shop_balance__local_currency
,shop_balance__usd
,l30d_order_cnt
,l30d_gmv__usd
,grass_date
,grass_region
FROM ${SCHEMA}.bi__cbsip_negative_balance_shop__reg__daily
WHERE
grass_date = CAST('${BIZ_TIME}' as date) - interval '1' day
AND shop_status = 1
)
GROUP BY 1, 2
)

SELECT
grass_date
,grass_region
,total_negative_balance__usd
,negative_balance_shop_cnt
FROM base_data

UNION
SELECT
grass_date
,'0-Total' AS grass_region
,SUM(total_negative_balance__usd) AS total_negative_balance__usd
,SUM(negative_balance_shop_cnt) AS negative_balance_shop_cnt
FROM base_data
GROUP BY 1, 2

ORDER BY grass_date, grass_region