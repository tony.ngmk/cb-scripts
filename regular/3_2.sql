WITH dim AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE grass_date >= current_date-interval'90'day
) 
, frozen AS (
SELECT DISTINCT
affi_shopid
, mst_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT shopid
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE cb_option = 1
AND country = 'MY'
) b 
ON b.shopid = a.mst_shopid
INNER JOIN (
SELECT DISTINCT
shop_id shopid
, user_id userid
FROM
regcbbi_others.shop_profile
WHERE user_status IN (2, 3)
AND status = 1 
AND grass_date=current_date-interval'1'day 
AND grass_region IN ('SG','ID','TH','PH','VN','TW','MX','CO','BR','CL')
AND is_cb_shop=1 
) u 
ON u.shopid = a.affi_shopid
INNER JOIN (
SELECT DISTINCT userid 
FROM
regcbbi_others.ops_shop_violation_reason -- checked ingestion table no issue in new schema
WHERE date(action_time) <= (current_date - INTERVAL '60' DAY)
) t1 
ON t1.userid = u.userid
WHERE a.grass_region='MY'
) 
, sip AS (
SELECT DISTINCT
shopid
, country
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
LEFT JOIN frozen f 
ON f.mst_shopid = a.shopid
WHERE cb_option = 1
AND country = 'MY' 
AND f.mst_shopid IS NULL
) 
, seller AS (
SELECT DISTINCT
child_shopid
, CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%lt%' THEN 'Long Tail'
ELSE 'Others' END seller_type
FROM
regbd_sf.cb__seller_index_tab
WHERE grass_region IN ('MY')
) 
, net AS (
SELECT DISTINCT
shop_id
, date(split(create_datetime,' ')[1]) create_time
, count(DISTINCT order_id) gross_order
, count(DISTINCT (CASE WHEN is_net_order = 1 THEN order_id ELSE null END)) net_order
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE date(from_unixtime(create_timestamp)) >= current_date-interval'90'day 
AND tz_type='local'
AND grass_date >= current_date-interval'90'day
AND is_cb_shop=1
AND grass_region='MY'
GROUP BY 1, 2
) 
, cancel AS (
SELECT DISTINCT
shopid
, date(CASE WHEN grass_region IN ('SG', 'MY', 'PH', 'TW') THEN from_unixtime(cancel_time) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN (from_unixtime(cancel_time) - INTERVAL '1' HOUR) 
WHEN grass_region = 'BR' THEN (from_unixtime(cancel_time) - INTERVAL '11' HOUR) 
WHEN grass_region = 'CL' THEN CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN grass_region = 'CO' THEN CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN grass_region = 'MX' THEN CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Mexico_City','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
END) cancel_time
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND extinfo.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) THEN orderid 
WHEN grass_region <> 'TW' AND extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) THEN orderid 
ELSE null END)) cancel_order
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE date(from_unixtime(cancel_time)) >= current_date-interval'90'day
AND grass_region='MY'
AND cb_option=1
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
shopid
, date(CASE WHEN grass_region IN ('SG', 'MY', 'PH', 'TW') THEN from_unixtime(mtime) 
WHEN grass_region IN ('TH', 'ID', 'VN') THEN (from_unixtime(mtime) - INTERVAL '1' HOUR) 
WHEN grass_region = 'BR' THEN (from_unixtime(mtime) - INTERVAL '11' HOUR) 
WHEN grass_region = 'CL' THEN CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN grass_region = 'CO' THEN CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN grass_region = 'MX' THEN CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Mexico_City','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
END) mtime
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND reason IN (1, 2, 3, 103, 105) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
WHEN grass_region <> 'TW' AND reason IN (1, 2, 3, 103, 105, 107) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
ELSE null END)) RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE grass_region='MY' 
AND cb_option=1 
AND date(from_unixtime(create_time)) >= current_date-interval'150'day
) b 
ON a.orderid = b.orderid
WHERE date(from_unixtime(mtime)) >= current_date-interval'90'day
AND grass_region='MY' 
AND cb_option=1 
GROUP BY 1, 2
) 
SELECT DISTINCT
dim.grass_date
, COALESCE(seller_type, '5.Others') seller_type
, sum(gross_order) gross_order
, sum(net_order) net_order
, sum(cancel_order) cancel_order
, sum(RR_order) RR_order
FROM dim
INNER JOIN sip 
ON dim.grass_region = sip.country
LEFT JOIN net 
ON sip.shopid = net.shop_id
AND dim.grass_date = net.create_time
LEFT JOIN cancel 
ON sip.shopid = cancel.shopid
AND dim.grass_date = cancel.cancel_time
LEFT JOIN rr 
ON sip.shopid = rr.shopid
AND dim.grass_date = rr.mtime
LEFT JOIN seller 
ON sip.shopid = CAST(seller.child_shopid AS int)
GROUP BY 1, 2