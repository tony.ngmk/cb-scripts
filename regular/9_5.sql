with
s as 
(
select DISTINCT shop_id
,source
,grass_region
from regcbbi_others.shop_profile
where grass_date = CURRENT_DATE - INTERVAL '1' DAY
and source
like '%SIP%'
)
select distinct grass_date
,s.source
,s.grass_region
,if(topup_expiry_end_timestamp = event_timestamp, 'Y', 'N') as expired
,credit_topup_type_name
,SUM(deduction_amt) as deduction_amt
,SUM(deduction_amt_usd) as deduction_amt_usd
from (
select *
from mp_paidads.dwd_advertiser_deduction_di__reg_s0_live
where grass_date >= CURRENT_DATE - INTERVAL '90' DAY
and credit_topup_type_name
like '%paid%' AND tz_type = 'local'
) ads
join s
on ads.shop_id = s.shop_id
group by 1, 2, 3, 4, 5
