WITH
sip AS (
SELECT DISTINCT 
grass_region
, affi_shopid
, mst_shopid
, b.country mst_country
FROM
(marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON (a.mst_shopid = b.shopid))
WHERE ((b.cb_option = 0) AND (a.grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL','TW') )) 
and sip_shop_status!=3 
) 

, o AS (
SELECT DISTINCT
"date"("from_unixtime"(create_timestamp)) create_timestamp
, order_id
, order_sn
, shop_id
, gmv
, gmv_usd 
, logistics_status_id
, payment_method_id
, order_be_status_id
, order_fe_status_id
, order_fe_status
, order_be_status
, logistics_status
, is_cb_shop
, grass_region

FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL','TW','ID') AND "date"("from_unixtime"(create_timestamp)) >= current_date-interval'100'day 
and tz_type='local' and grass_date >= current_date-interval'100'day 
) 
, wh AS (SELECT DISTINCT * FROM regcbbi_others.logistics_basic_info
WHERE wh_outbound_date between CURRENT_DATE - INTERVAL '70' DAY and CURRENT_DATE - INTERVAL '1' DAY
)

, raw AS (
SELECT DISTINCT
oversea_orderid
, "max"(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
where date(from_unixtime(ctime)) >= current_date-interval'100'day 
and local_country IN ('tw','my','id','vn','th') 
and oversea_country in ('my','sg','vn','ph','th','br','mx','co','cl','tw')
GROUP BY 1
) 

SELECT DISTINCT
sip.mst_country 
, a.grass_region affi_country
, wh_outbound_date
, a.order_fe_status a_order_fe_status
, a.order_be_status a_order_be_status
, a.logistics_status a_logistics_status
, case when a.order_fe_status_id in (3,4) or a.logistics_status_id in (5,6,11) then 'Y' else 'N' end as complete 
, a.order_sn
, a.shop_id
, a.gmv_usd 
, a.create_timestamp
, p.gmv_usd p_gmv_usd 
, p.order_fe_status p_order_fe_status 
, p.order_be_status p_order_be_status
, p.logistics_status p_logistics_status
FROM
(select * 
from o
where is_cb_shop=1 and grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL','TW')
) a 
join wh on wh.ordersn = a.order_sn 
join sip on sip.affi_shopid = a.shop_id 
join raw on raw.oversea_orderid=a.order_id 
join (select * 
from o
where is_cb_shop=0 and grass_region in ('ID','TH','VN','TW','MY')
) p on p.order_id = raw.local_orderid
where a.order_fe_status_id not in (3,4) and a.logistics_status_id not in (5,6,11)