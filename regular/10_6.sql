WITH
o AS (
SELECT DISTINCT
o2.orderid
, o1.shop_id SHOPID
, o1.create_time
, o1.cancel_time
, o2.cancel_reason
-- , o1.status_ext
, o1.is_net_order
, o1.complete_time
, o1.grass_date
FROM
((
SELECT DISTINCT
order_id
, shop_id
, (CASE WHEN grass_region = 'CL' then from_unixtime(create_timestamp, 'America/Santiago') when grass_region = 'MX' then from_unixtime(create_timestamp, 'America/Mexico_City') when grass_region = 'CO' then from_unixtime(create_timestamp, 'America/Bogota') when (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) create_time
, (CASE WHEN grass_region = 'CL' then from_unixtime(cancel_timestamp, 'America/Santiago') when grass_region = 'MX' then from_unixtime(cancel_timestamp, 'America/Mexico_City') when grass_region = 'CO' then from_unixtime(cancel_timestamp, 'America/Bogota') WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) cancel_time
-- , order_be_status_id status_ext
, is_net_order
, (CASE WHEN grass_region = 'CL' then from_unixtime(cancel_timestamp, 'America/Santiago') when grass_region = 'MX' then from_unixtime(cancel_timestamp, 'America/Mexico_City') when grass_region = 'CO' then from_unixtime(cancel_timestamp, 'America/Bogota') WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) complete_time
, (CASE WHEN grass_region = 'CL' then from_unixtime(create_timestamp, 'America/Santiago') when grass_region = 'MX' then from_unixtime(create_timestamp, 'America/Mexico_City') when grass_region = 'CO' then from_unixtime(create_timestamp, 'America/Bogota') WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) grass_date 

FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE (((is_cb_shop = 0) AND (tz_type = 'local')) AND (grass_region = 'VN'))
) o1
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'VN'))
) o2 ON (o1.order_id = o2.orderid))
) 
/*, fraud AS (
SELECT DISTINCT shopid
FROM
shopee.shopee_vn_op_team__potential_fraud_vnsip
) */
, r AS (
SELECT DISTINCT
orderid
, shopid
, reason
, status
, "from_unixtime"(ctime) ctime
, "from_unixtime"(mtime) mtime
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'VN'))
) 
, channel AS (
SELECT DISTINCT
SHOPID
, USERID
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.50011.enabled') AS bigint), 0) GHN
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.50021.enabled') AS bigint), 0) SPX
FROM
(
SELECT DISTINCT
SHOPID
, extinfo
, userid
, grass_region
FROM
marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE (grass_region = 'VN')
) 
) 
, user AS (
SELECT DISTINCT
shop_id
, (CASE WHEN (is_official_shop = 1) THEN 'Official Shop' WHEN (is_preferred_shop = 1) THEN 'Preferred Shop' ELSE 'Normal' END) seller_type
, grass_region
, user_name
, user_id
, active_item_cnt
, active_item_with_stock_cnt
, status shop_status
, user_status
, shop_name
, description
, shop_level1_category
, shop_level2_category
, is_holiday_mode
FROM
regcbbi_others.shop_profile
WHERE ((((((grass_date = (current_date - INTERVAL '1' DAY)) AND (is_cb_shop = 0)) AND (grass_region = 'VN')) AND (status = 1)) AND (user_status = 1)) AND (is_holiday_mode = 0))
) 
, vape AS (
SELECT DISTINCT shop_id
FROM
(user
INNER JOIN (
SELECT DISTINCT keyword
FROM
regcbbi_others.shopee_regional_cb_team__vnsip_vape_keyword
WHERE (ingestion_timestamp = (SELECT "max"(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__vnsip_vape_keyword
))
) k ON ("regexp_like"("lower"(shop_name), "lower"(keyword)) OR "regexp_like"("lower"(description), "lower"(keyword))))
) 
, L30D_LIVE AS (
SELECT DISTINCT shop_id
FROM
regcbbi_others.shop_profile
WHERE ((((grass_date BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) AND (status = 1)) AND (user_status = 1)) AND (is_holiday_mode = 0))
) 
, ops_check AS (
SELECT DISTINCT
shop_id
, grass_region
, seller_type
, user_name
, user_status
, shop_status
, shop_name
, user_id
, description
, shop_level1_category
, shop_level2_category
, active_item_cnt
, active_item_with_stock_cnt
, is_holiday_mode
, "count"(DISTINCT (CASE WHEN ("date"(o.create_time) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) THEN o.orderid ELSE null END)) gross_orders_30d
, "count"(DISTINCT (CASE WHEN (((grass_region = 'TW') AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302))) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid WHEN (((grass_region <> 'TW') AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302))) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) NFR_included_cancellations_90d
, "count"(DISTINCT (CASE WHEN (((((grass_region = 'TW') AND (r.reason IN (1, 2, 3, 103, 105))) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid WHEN (((((grass_region <> 'TW') AND (r.reason IN (1, 2, 3, 103, 105, 107))) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END)) NFR_included_rr_90d
, "count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND o.is_net_order = 1) THEN o.orderid ELSE null END)) net_orders_90d
FROM
((user
LEFT JOIN o ON (user.shop_id = o.shopid))
LEFT JOIN r ON (o.orderid = r.orderid))
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
) 
, wms AS (
SELECT
shop_id
, "max_by"(is_b2c, grass_date) col_1
FROM
mp_user.dim_shop__reg_s0_live
WHERE (1 = 1) and grass_region='VN'
GROUP BY 1
HAVING ("max_by"(is_b2c, grass_date) = 1)
) 
, sip AS (
SELECT DISTINCT
mst_shopid
, t1.country mst_country
--, t2.country affi_country
, t1.cb_option
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (sip_shop_status <> 3) AND grass_region <>'O'
) t2 ON (t1.shopid = t2.mst_shopid))
) 
SELECT DISTINCT
(current_date - INTERVAL '1' DAY) update_time
, OC.GRASS_REGION
, "count"(DISTINCT oc.shop_id) shop_number
, "sum"(active_item_with_stock_cnt) live_sku
FROM
(((((OPS_CHECK OC
INNER JOIN CHANNEL CL ON (CL.SHOPID = OC.shop_id))
INNER JOIN L30D_LIVE ll ON (ll.shop_id = oc.shop_id))
LEFT JOIN wms ON (wms.shop_id = oc.shop_id))
LEFT JOIN vape v ON (v.shop_id = oc.shop_id))
LEFT JOIN sip ON (sip.mst_shopid = oc.shop_id))
WHERE ((((((((wms.shop_id IS NULL) AND ((gross_orders_30d / DECIMAL '30.00') >= DECIMAL '0.03'))) AND (v.shop_id IS NULL)) AND (TRY(((NFR_included_cancellations_90d * DECIMAL '1.000') / ((NFR_included_cancellations_90d + NFR_included_rr_90d) + net_orders_90d))) <= DECIMAL '0.03')) AND ((ghn = 1) OR (spx = 1))) AND (shop_level1_category <> 'Voucher & Services')) AND (sip.mst_shopid IS NULL))
GROUP BY 1, 2