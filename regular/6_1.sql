WITH dimdate
AS (
SELECT DISTINCT DATE (date_trunc('year', CURRENT_DATE - interval '1' day)) ytd,
date_add('year', - 1, date_trunc('year', CURRENT_DATE - interval '1' day)) y_1,
DATE (date_trunc('month', CURRENT_DATE - interval '1' day)) mtd
),
sip
AS (
SELECT DISTINCT affi_shopid,
mst_shopid,
t1.country mst_country,
t2.country affi_country
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT affi_shopid,
affi_username,
mst_shopid,
country
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE grass_region IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX', 'CO', 'CL', 'PL', 'FR', 'ES')
AND sip_shop_status != 3 ) t2 ON (t1.shopid = t2.mst_shopid)
),
refresh_day
AS (
SELECT min(mon_current_q) first_mon_current_q
FROM (
SELECT DISTINCT DATE ((date_id - INTERVAL '1' DAY)) mon_current_q,
day_of_week(date_id) weekday,
quarter(date_id) q
FROM regbida_keyreports.dim_date
WHERE (quarter(date_id) = quarter(CURRENT_DATE))
AND (day_of_week(date_id) = 1)
AND (year(date_id) = year(CURRENT_DATE))
)
),
penalty
AS (
WITH seller_penalty_points AS (
SELECT DISTINCT --a.log_id refid,
a.id refid,
a.shop_id,
a.user_name username,
a.point,
row_number() OVER (
PARTITION BY id ORDER BY update_time DESC
) AS entry_no,
a.manual_status,
a.auto_status,
a.violation_reason
FROM marketplace.shopee_seller_account_health_db__penalty_history_log_tab__reg_continuous_s0_live a
WHERE date_parse(a.execute_dt, '%Y%m%d') >= (
SELECT first_mon_current_q
FROM refresh_day
)
AND (metrics_name = 'point')
AND grass_region <> ''
-- AND environment <> 'o'
)
--AND REGION IN ('SG','MY','ID','PH','TH','VN','TW','BR','MX','CO','CL','PL','FR','ES')
SELECT DISTINCT a.shop_id,
sum(point) penalty_point,
COALESCE(sum(CASE
WHEN (violation_reason IN (5, 6, 7, 8))
THEN point
ELSE NULL
END), 0) NFR_LSR_POINT,
COALESCE(sum(CASE
WHEN (violation_reason IN (9, 10, 11, 13, 23))
THEN point
ELSE NULL
END), 0) LISTING_CS_POINT,
COALESCE(sum(CASE
WHEN (violation_reason = 20)
THEN point
ELSE NULL
END), 0) SHIPPING_FRAUD_POINT,
COALESCE(sum(CASE
WHEN (NOT (violation_reason IN (5, 6, 7, 8, 9, 10, 11, 13, 20, 23)))
THEN point
ELSE NULL
END), 0) OTHER_POINT
FROM (
SELECT DISTINCT shop_id,
violation_reason,
sum(point) point
FROM seller_penalty_points
WHERE entry_no = 1
AND manual_status = 1
AND auto_status = 1
GROUP BY 1,
2
) a
JOIN sip ON sip.affi_shopid = a.shop_id
GROUP BY 1
),
o
AS (
SELECT DISTINCT o1.order_id orderid,
o1.shop_id,
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW'))
THEN from_unixtime(o1.create_timestamp)
WHEN (grass_region = 'ID')
THEN CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'Asia/Jakarta', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'TH')
THEN CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'Asia/Bangkok', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'VN')
THEN CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'Asia/Ho_Chi_Minh', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'BR')
THEN CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'America/Sao_Paulo', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CL')
THEN CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'America/Santiago', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CO')
THEN CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'America/Bogota', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'MX')
THEN CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'America/Mexico_City', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region IN ('PL', 'ES', 'FR'))
THEN CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'CET', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
END create_time,
CAST(format_datetime(from_unixtime(o1.create_timestamp) AT TIME ZONE 'America/Mexico_City', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) create_timestamp,
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW'))
THEN from_unixtime(o1.cancel_timestamp)
WHEN (grass_region = 'ID')
THEN CAST(format_datetime(from_unixtime(o1.cancel_timestamp) AT TIME ZONE 'Asia/Jakarta', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'TH')
THEN CAST(format_datetime(from_unixtime(o1.cancel_timestamp) AT TIME ZONE 'Asia/Bangkok', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'VN')
THEN CAST(format_datetime(from_unixtime(o1.cancel_timestamp) AT TIME ZONE 'Asia/Ho_Chi_Minh', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'BR')
THEN CAST(format_datetime(from_unixtime(o1.cancel_timestamp) AT TIME ZONE 'America/Sao_Paulo', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CL')
THEN CAST(format_datetime(from_unixtime(o1.cancel_timestamp) AT TIME ZONE 'America/Santiago', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CO')
THEN CAST(format_datetime(from_unixtime(o1.cancel_timestamp) AT TIME ZONE 'America/Bogota', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'MX')
THEN CAST(format_datetime(from_unixtime(o1.cancel_timestamp) AT TIME ZONE 'America/Mexico_City', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region IN ('PL', 'ES', 'FR'))
THEN CAST(format_datetime(from_unixtime(o1.cancel_timestamp) AT TIME ZONE 'CET', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
END cancel_time,
o1.cancel_reason_id AS cancel_reason,
o1.order_be_status_id status_ext,
is_net_order,
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW'))
THEN from_unixtime(o1.complete_timestamp)
WHEN (grass_region = 'ID')
THEN CAST(format_datetime(from_unixtime(o1.complete_timestamp) AT TIME ZONE 'Asia/Jakarta', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'TH')
THEN CAST(format_datetime(from_unixtime(o1.complete_timestamp) AT TIME ZONE 'Asia/Bangkok', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'VN')
THEN CAST(format_datetime(from_unixtime(o1.complete_timestamp) AT TIME ZONE 'Asia/Ho_Chi_Minh', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'BR')
THEN CAST(format_datetime(from_unixtime(o1.complete_timestamp) AT TIME ZONE 'America/Sao_Paulo', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CL')
THEN CAST(format_datetime(from_unixtime(o1.complete_timestamp) AT TIME ZONE 'America/Santiago', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CO')
THEN CAST(format_datetime(from_unixtime(o1.complete_timestamp) AT TIME ZONE 'America/Bogota', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'MX')
THEN CAST(format_datetime(from_unixtime(o1.complete_timestamp) AT TIME ZONE 'America/Mexico_City', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region IN ('PL', 'ES', 'FR'))
THEN CAST(format_datetime(from_unixtime(o1.complete_timestamp) AT TIME ZONE 'CET', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
END complete_time,
gmv_usd
FROM mp_order.dwd_order_all_ent_df__reg_s0_live o1
WHERE (is_cb_shop = 1)
AND o1.tz_type = 'local'
AND o1.grass_region IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX', 'CO', 'CL', 'PL', 'FR', 'ES')
AND grass_date >= date_add('year', - 1, date_trunc('year', CURRENT_DATE - interval '1' day))
),
r
AS (
SELECT DISTINCT orderid,
shopid,
reason,
STATUS,
from_unixtime(ctime) ctime,
from_unixtime(mtime) mtime
FROM marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE (cb_option = 1)
AND grass_region IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX', 'CO', 'CL', 'PL', 'FR', 'ES')
AND date(from_unixtime(mtime)) >= date_add('year', - 1, date_trunc('year', CURRENT_DATE - interval '1' day))
),
list
AS (
SELECT DISTINCT a.country,
a.shop_id,
a.user_id,
a.shop_name,
a.user_name,
max(a.penalised_tier) AS penalised_tier,
a.start_date AS start_date,
a.end_date AS end_date,
a.update_time,
first_mon_current_q
FROM (
SELECT -- country,
grass_region AS country,
shop_id,
user_id,
shop_name,
user_name,
rule_id,
CASE
WHEN rule_id = 101
THEN 1
WHEN rule_id = 102
THEN 2
WHEN rule_id = 103
THEN 3
WHEN rule_id = 104
THEN 4
WHEN rule_id > 104
AND rule_id < 1000
THEN 5
WHEN rule_id >= 1000
THEN 3
ELSE NULL
END AS penalised_tier,
DATE (date_parse(execute_dt, '%Y%m%d') + interval '1' day) AS start_date,
DATE (date_parse(effective_dt, '%Y%m%d')) AS end_date,
from_unixtime(update_time / 1000) AS update_time
FROM marketplace.shopee_seller_account_health_db__penalty_history_log_tab__reg_continuous_s0_live
WHERE auto_status = 1
AND grass_region <> ''
AND manual_status = 1
-- AND environment <> 'o'
AND metrics_all_name = 'operation'
AND shop_id NOT IN (8399855, 45809904, 28599224, 1549918, 25569272, 25567936, 9184325, 289810498, 194488879, 74261370, 8763232)
) a
JOIN refresh_day ON a.end_date >= refresh_day.first_mon_current_q
JOIN sip ON sip.affi_shopid = a.shop_id
INNER JOIN (
SELECT country,
shop_id,
max(from_unixtime(update_time / 1000)) AS update_time
FROM (
SELECT -- country,
grass_region AS country,
shop_id,
update_time,
max(auto_status) auto_status,
max(manual_status) manual_status
FROM marketplace.shopee_seller_account_health_db__penalty_history_log_tab__reg_continuous_s0_live
WHERE metrics_all_name = 'operation'
AND grass_region <> ''
-- AND environment <> 'o'
GROUP BY 1,
2,
3
)
WHERE auto_status = 1
AND manual_status = 1
GROUP BY 1,
2
) b ON a.country = b.country
AND a.shop_id = b.shop_id
AND a.update_time = b.update_time
GROUP BY 1,
2,
3,
4,
5,
7,
8,
9,
10
)
, nfr as (
select distinct shop_id 
, nfr_7d
, cancel_orders_7d 
, return_orders_7d
, denominator_7d
from mkpldp_shop_health.aggr_sh_shop_nfr_res_da
where country in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and date(date_parse(dt, '%Y%m%d')) = current_date-interval'1'day 
)
, lsr as (
select distinct shop_id
, lsr_7d
from mkpldp_shop_health.aggr_sh_shop_lsr_res_da
where country in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and date(date_parse(dt, '%Y%m%d')) = current_date-interval'1'day 
)
SELECT DISTINCT a.affi_shopid,
a.mst_shopid,
a.affi_country,
a.mst_country,
NFR_included_cancellations_7d,
NFR_included_rr_7d,
net_orders_7d,
COALESCE(TRY(((NFR_included_cancellations_7d * DECIMAL '1.000000') / ((NFR_included_cancellations_7d + NFR_included_rr_7d) + net_orders_7d))), 0) l7d_cr,
NFR_included_cancellations_30d,
NFR_included_rr_30d,
net_orders_30d,
COALESCE(TRY(((NFR_included_cancellations_30d * DECIMAL '1.000000') / ((NFR_included_cancellations_30d + NFR_included_rr_30d) + net_orders_30d))), 0) l30d_cr,
NFR_included_cancellations_mtd,
NFR_included_rr_mtd,
net_orders_mtd,
COALESCE(TRY(((NFR_included_cancellations_mtd * DECIMAL '1.000000') / ((NFR_included_cancellations_mtd + NFR_included_rr_mtd) + net_orders_mtd))), 0) mtd_cr,
NFR_included_cancellations_ytd,
NFR_included_rr_ytd,
net_orders_ytd,
COALESCE(TRY(((NFR_included_cancellations_ytd * DECIMAL '1.000000') / ((NFR_included_cancellations_ytd + NFR_included_rr_ytd) + net_orders_ytd))), 0) ytd_cr,
COALESCE(penalty_point, 0) penalty_point,
COALESCE(LISTING_CS_POINT, 0) LISTING_CS_POINT,
COALESCE(NFR_LSR_POINT, 0) NFR_LSR_POINT,
COALESCE(SHIPPING_FRAUD_POINT, 0) SHIPPING_FRAUD_POINT,
COALESCE(OTHER_POINT, 0) OTHER_POINT,
COALESCE(cast(affi_apt as decimal(10,2)), 0) l30D_apt,
list.start_date,
list.end_date, 
coalesce(nfr_7d,0) nfr_7d, 
coalesce(lsr_7d,0) lsr_7d 

FROM (
SELECT DISTINCT affi_shopid,
sip.mst_country,
sip.affi_country,
sip.mst_shopid,
count(DISTINCT CASe WHEN (affi_country = 'TW')
AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302, 303))
AND DATE (o.cancel_time) BETWEEN (CURRENT_DATE - INTERVAL '7' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN o.orderid
WHEN (affi_country <> 'TW')
AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302, 303))
AND DATE (o.cancel_time) BETWEEN (CURRENT_DATE - INTERVAL '7' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN o.orderid ELSE NULL END) NFR_included_cancellations_7d,
count(DISTINCT CASE
WHEN (affi_country = 'TW')
AND (r.reason IN ( 2, 3, 103, 105))
AND ( (o.cancel_reason = 0)
OR (o.cancel_reason IS NULL) )
AND (r.STATUS IN (2, 5))
AND DATE (r.mtime) BETWEEN (CURRENT_DATE - INTERVAL '7' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN r.orderid
WHEN (affi_country <> 'TW')
AND (r.reason IN ( 2, 3, 103, 105, 107))
AND ((o.cancel_reason = 0)
OR (o.cancel_reason IS NULL) )
AND (r.STATUS IN (2, 5))
AND DATE (r.mtime) BETWEEN (CURRENT_DATE - INTERVAL '7' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN r.orderid ELSE NULL
END) NFR_included_rr_7d,
count(DISTINCT CASE
WHEN DATE (o.create_time) BETWEEN (CURRENT_DATE - INTERVAL '7' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
AND o.is_net_order = 1
-- AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))
THEN o.orderid ELSE NULL END) net_orders_7d,
count(DISTINCT CASE
WHEN (affi_country = 'TW')
AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302, 303))
AND DATE (o.cancel_time) BETWEEN (CURRENT_DATE - INTERVAL '30' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN o.orderid
WHEN (affi_country <> 'TW')
AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302, 303))
AND DATE (o.cancel_time) BETWEEN (CURRENT_DATE - INTERVAL '30' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN o.orderid ELSE NULL END) NFR_included_cancellations_30d,
count(DISTINCT CASE WHEN (affi_country = 'TW')
AND (r.reason IN ( 2, 3, 103, 105))
AND ( (o.cancel_reason = 0)
OR (o.cancel_reason IS NULL) )
AND (r.STATUS IN (2, 5))
AND DATE (r.mtime) BETWEEN (CURRENT_DATE - INTERVAL '30' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN r.orderid
WHEN (affi_country <> 'TW')
AND (r.reason IN ( 2, 3, 103, 105, 107))
AND ( (o.cancel_reason = 0)
OR (o.cancel_reason IS NULL) )
AND (r.STATUS IN (2, 5))
AND DATE (r.mtime) BETWEEN (CURRENT_DATE - INTERVAL '30' DAY) AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN r.orderid
ELSE NULL END) NFR_included_rr_30d,
count(DISTINCT CASE WHEN DATE (o.create_time) BETWEEN (CURRENT_DATE - INTERVAL '30' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
AND o.is_net_order = 1
-- AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))
THEN o.orderid ELSE NULL END) net_orders_30d,
count(DISTINCT CASE WHEN (affi_country = 'TW')
AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302, 303))
AND DATE (o.cancel_time) BETWEEN ytd
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN o.orderid
WHEN (affi_country <> 'TW')
AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302, 303))
AND DATE (o.cancel_time) BETWEEN ytd
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN o.orderid
ELSE NULL END) NFR_included_cancellations_ytd,
count(DISTINCT CASE WHEN (affi_country = 'TW')
AND (r.reason IN ( 2, 3, 103, 105))
AND ( (o.cancel_reason = 0)
OR (o.cancel_reason IS NULL) )
AND (r.STATUS IN (2, 5))
AND DATE (r.mtime) BETWEEN ytd
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN r.orderid
WHEN (affi_country <> 'TW')
AND (r.reason IN ( 2, 3, 103, 105, 107))
AND ((o.cancel_reason = 0)OR (o.cancel_reason IS NULL))
AND (r.STATUS IN (2, 5))
AND DATE (r.mtime) BETWEEN ytd
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN r.orderid ELSE NULL
END) NFR_included_rr_ytd,
count(DISTINCT CASE
WHEN DATE (o.create_time) BETWEEN ytd
AND (CURRENT_DATE - INTERVAL '1' DAY)
AND o.is_net_order = 1
-- AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))
THEN o.orderid
ELSE NULL
END) net_orders_ytd,
count(DISTINCT CASE
WHEN (affi_country = 'TW')
AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302, 303))
AND DATE (o.cancel_time) BETWEEN mtd
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN o.orderid
WHEN (affi_country <> 'TW')
AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302, 303))
AND DATE (o.cancel_time) BETWEEN mtd
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN o.orderid
ELSE NULL
END) NFR_included_cancellations_mtd,
count(DISTINCT CASE
WHEN (affi_country = 'TW')
AND (r.reason IN ( 2, 3, 103, 105))
AND ( (o.cancel_reason = 0)
OR (o.cancel_reason IS NULL) )
AND (r.STATUS IN (2, 5))
AND DATE (r.mtime) BETWEEN mtd
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN r.orderid
WHEN (affi_country <> 'TW')
AND (r.reason IN ( 2, 3, 103, 105, 107))
AND ( (o.cancel_reason = 0)
OR (o.cancel_reason IS NULL) )
AND (r.STATUS IN (2, 5))
AND DATE (r.mtime) BETWEEN mtd
AND (CURRENT_DATE - INTERVAL '1' DAY)
THEN r.orderid
ELSE NULL
END) NFR_included_rr_mtd,
count(DISTINCT CASE
WHEN DATE (o.create_time) BETWEEN mtd
AND (CURRENT_DATE - INTERVAL '1' DAY)
AND o.is_net_order = 1
-- AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))
THEN o.orderid
ELSE NULL
END) net_orders_mtd
FROM sip
INNER JOIN dimdate ON (1 = 1)
LEFT JOIN o ON (sip.affi_shopid = o.shop_id)
LEFT JOIN r ON (o.orderid = r.orderid)
GROUP BY 1,
2,
3,
4
) a
LEFT JOIN list ON list.shop_id = a.affi_shopid
LEFT JOIN penalty p ON (p.shop_id = a.affi_shopid)
LEFT JOIN regcbbi_others.shopee_regional_cb_team__sip_apt_map apt ON (CAST(apt.affi_shopid AS BIGINT) = a.affi_shopid)
--where a.affi_shopid = 396752931
left join nfr on nfr.shop_id = a.affi_shopid 
left join lsr on lsr.shop_id = a.affi_shopid