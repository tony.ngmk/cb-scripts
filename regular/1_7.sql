WITH sip as
(
select distinct cast(affi.affi_shopid as bigint) as affi_shopid
, affi.mst_shopid
, mst.country as mst_country
, affi.country as affi_country
, mst.cb_option
from
(
select distinct cast(shopid as bigint) as shopid
, country
, cb_option
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
where cb_option = 0
) mst
join 
(
select distinct cast(affi_shopid as bigint) as affi_shopid
, affi_username
, cast(mst_shopid as bigint) as mst_shopid
, country
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status != 3
and grass_region = 'MY'
) affi
on mst.shopid = affi.mst_shopid
),

trial as
(
select distinct cast(trim(shop_id) as bigint) as shop_id
from regcbbi_others.sip_cod_trial_temp_my_s0
where cast(ingestion_timestamp as bigint) = (select max(cast(ingestion_timestamp as bigint)) from regcbbi_others.sip_cod_trial_temp_my_s0)
)

select sls.grass_date
, count(distinct sls.orderid) as orders
, count(distinct if(sls.payment_method = 6, sls.orderid, null)) as cod_orders
, sum(o.gmv_usd) as gmv_usd
, sum(if(sls.payment_method = 6, o.gmv_usd, 0)) as cod_gmv_usd
, count(distinct if(sls.payment_method = 6 and sls.delivery_failed_time > sls.pickup_done_time, sls.orderid, null)) as cod_failed_orders
, sum(if(sls.payment_method = 6 and sls.delivery_failed_time > sls.pickup_done_time, o.gmv_usd, 0)) as cod_failed_gmv_usd
, count(distinct if(rr.orderid is not null, sls.orderid, null)) as rr_orders
, if(trial.shop_id is not null, 'Y', 'N') as trial_shop
from
(
select distinct orderid
, shopid
, payment_method
, order_placed_local_date as grass_date
, pickup_done_time
, delivery_failed_time
from regcbbi_others.cb_logistics_mart_test
where grass_region = 'MY'
and order_placed_local_date between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '89' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
) sls
join
(
select distinct order_id
, gmv_usd
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_date >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '90' day
and tz_type = 'local'
and grass_region = 'MY'
and is_cb_shop = 1
) o 
on sls.orderid = o.order_id
join sip
on sls.shopid = sip.affi_shopid
left join trial
on sls.shopid = trial.shop_id
left join
(
select distinct orderid
from marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
where cb_option = 1
and grass_region = 'MY'
and status in (2, 5)
and ctime >= to_unixtime(DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '90' day)
) rr
on sls.orderid = rr.orderid
group by 1, 9