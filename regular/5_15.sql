WITH
dim AS 
(
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE (grass_date between current_date-interval'63'day and CURRENT_DATE-INTERVAL'1'DAY )
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
) 
, cb AS 
(
SELECT DISTINCT CAST(child_shopid AS INTEGER) AS child_shopid
, grass_region
, gp_account_name
, gp_account_owner
, gp_account_owner_userrole_name
, child_account_name 
, (CASE WHEN (gp_account_owner_userrole_name LIKE '%KAM%') THEN 'KAM' WHEN (gp_account_owner_userrole_name LIKE '%PSM%') THEN 'PSM' 
ELSE 'CSS' END) seller_type
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
WHERE grass_region in ('TW','MY','BR','SG') 
) 
, sip AS (
SELECT DISTINCT
affi_shopid
, a.country as affi_country 
, mst_shopid
, b.country as mst_country 
, cb_option
FROM
(marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT shopid
, country
, cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) b ON (a.mst_shopid = b.shopid))
WHERE grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') and sip_shop_status!=3
) 

, o as 
(
SELECT DISTINCT
shop_id
, date(split(create_datetime,' ')[1]) as create_time
, order_id 
, order_sn 
, order_be_status_id
, payment_method_id
from mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ( date(split(create_datetime,' ')[1]) >= current_date-interval'150'day ) and tz_type='local' and is_cb_shop=1
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and grass_date >= current_date-interval'100'day 
)
, l as 
( 
SELECT DISTINCT
o.shop_id shopid
, date(from_unixtime(ctime)) ctime
, count(distinct case when o.payment_method_id=6 and l.orderid is not null then o.order_id else null end ) cod_fail_order 
from o 
left join (select distinct orderid 
, ctime 
from marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_continuous_s0_live
WHERE new_status IN (6) and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and date(from_unixtime(ctime)) >= current_date-interval'100'day 
) l on l.orderid = o.order_id 

group by 1,2 
) 

, seller AS 
(
SELECT DISTINCT
child_shopid
, CASE WHEN gp_account_owner in 
('Boey Peng',
'Jun Li',
'Crystal Yu',
'Christine Tang',
'Cindy Jin',
'Shelly Zhuo',
'Zheng Kai',
'Jolene Wang',
'Johnny Chen',
'Echo Zeng') THEN '1.KAM' 
WHEN (gp_account_owner_userrole_name LIKE '%PSM%') THEN '2.PSM' 
WHEN (gp_account_owner_userrole_name LIKE '%ICB%') THEN '4.ICB' 
WHEN (gp_account_owner_userrole_name LIKE '%CS%') THEN '3.CS' ELSE '5.Others' END seller_type
from cncbbi_general.shopee_cb_seller_profile_with_old_column
WHERE grass_region in ('TW','MY','VN','SG','BR','ID')
) 
, net AS (
SELECT DISTINCT
shop_id
, create_time
, "count"(DISTINCT order_id) gross_order
, "count"(DISTINCT (CASE WHEN (order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15)) THEN order_id ELSE null END)) net_order
, count(distinct case when payment_method_id=6 then order_id else null end ) cod_order 
--, count(distinct case when payment_method_id=6 and l.orderid is not null then order_id else null end ) cod_fail_order 
FROM
o 

GROUP BY 1, 2
) 
, cancel AS (
SELECT DISTINCT
shopid
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(cancel_time)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(cancel_time) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(cancel_time)-interval'11'hour)
WHEN (grass_region = 'CL') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'CO') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'MX') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Mexico_City','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'PL') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'ES') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'FR') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
end cancel_time
, "count"(DISTINCT (CASE WHEN ((grass_region = 'TW') AND (extinfo.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303))) THEN orderid 

WHEN ((grass_region NOT IN ('TW')) AND (extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) THEN orderid 
ELSE null END)) cancel_order
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ( ("date"("from_unixtime"(cancel_time)) >=current_date-interval'150'day )) AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and cb_option=1 
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
shopid
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(mtime)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(mtime) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime)-interval'11'hour)
WHEN (grass_region = 'CL') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'CO') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'MX') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'America/Mexico_City','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'PL') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'ES') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'FR') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
end mtime
, count(distinct case when status in (2,5) then a.orderid else null end ) rr_order 
, "count"(DISTINCT (CASE WHEN ((((grass_region = 'TW') AND (reason IN (1, 2, 3, 103, 105))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5))) THEN a.orderid 
WHEN ((((grass_region <> 'TW') AND (reason IN (1, 2, 3, 103, 105, 107))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5))) THEN a.orderid ELSE null END)) nfr_RR_order
FROM
(marketplace.shopee_return_v2_db__return_v2_tab__reg_continuous_s0_live a
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') and date(from_unixtime(create_time))>=current_date-interval'100'day and cb_option=1 ) b ON (a.orderid = b.orderid))
where ( ("date"("from_unixtime"(mtime)) >= current_date-interval'100'day )) AND CB_OPTION= 1 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
GROUP BY 1, 2
) 
SELECT DISTINCT
case when dim.grass_date between current_date-interval'7'day and current_date-interval'1'day then 1 
when dim.grass_date between current_date-interval'14'day and current_date-interval'8'day then 2 
when dim.grass_date between current_date-interval'21'day and current_date-interval'15'day then 3 
when dim.grass_date between current_date-interval'28'day and current_date-interval'22'day then 4 
when dim.grass_date between current_date-interval'35'day and current_date-interval'29'day then 5 
when dim.grass_date between current_date-interval'42'day and current_date-interval'36'day then 6 
when dim.grass_date between current_date-interval'49'day and current_date-interval'43'day then 7 
when dim.grass_date between current_date-interval'56'day and current_date-interval'50'day then 8 
when dim.grass_date between current_date-interval'63'day and current_date-interval'57'day then 9 
else null end as week
, case when dim.grass_date between current_date-interval'30'day and current_date-interval'1'day then 1 
when dim.grass_date between current_date-interval'60'day and current_date-interval'31'day then 2 
else 3 end as month 
, cb_option 
, sip.mst_country
, sip.affi_country 

, "sum"(gross_order) gross_order
, "sum"(net_order) net_order
, "sum"(cancel_order) cancel_order
, "sum"(RR_order) RR_order
, sum(nfr_RR_order) nfr_RR_order
, sum(cod_order) cod_orders 
, sum(cod_fail_order) cod_fail_order
FROM
(
((((dim
INNER JOIN sip ON (dim.grass_region = sip.affi_country ))
LEFT JOIN net ON ((sip.affi_shopid = net.shop_id) AND (dim.grass_date = net.create_time)))
LEFT JOIN cancel ON ((sip.affi_shopid = cancel.shopid) AND (dim.grass_date = cancel.cancel_time)))
LEFT JOIN rr ON ((sip.affi_shopid = rr.shopid) AND (dim.grass_date = rr.mtime)))
left join l on l.shopid= sip.affi_shopid and dim.grass_date=l.ctime 
--LEFT JOIN seller ON (sip.mst_shopid = CAST(seller.child_shopid AS int))
)
--left join cb on cb.child_shopid = sip.mst_shopid 
GROUP BY 1, 2,3,4 , 5