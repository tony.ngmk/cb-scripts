WITH sip as 
(
select distinct cast(affi.affi_shopid as bigint) as affi_shopid
, affi.mst_shopid
, mst.country as mst_country
, affi.country as affi_country
, mst.cb_option
from
(
select distinct cast(shopid as bigint) as shopid
, country
, cb_option
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) mst
join 
(
select distinct cast(affi_shopid as bigint) as affi_shopid
, affi_username
, cast(mst_shopid as bigint) as mst_shopid
, country
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status != 3 
) affi
on mst.shopid = affi.mst_shopid
)

select distinct sls.grass_region
, case when sls.grass_date >= date_trunc('week', current_date) then 'WTD*'
when sls.grass_date >= date_trunc('week', current_date) - interval '7' day then 'W-1*'
when sls.grass_date >= date_trunc('week', current_date) - interval '14' day then 'W-2*'
when sls.grass_date >= date_trunc('week', current_date) - interval '21' day then 'W-3*'
when sls.grass_date >= date_trunc('week', current_date) - interval '28' day then 'W-4*'
when sls.grass_date >= date_trunc('week', current_date) - interval '35' day then 'W-5*'
when sls.grass_date >= date_trunc('week', current_date) - interval '42' day then 'W-6*'
when sls.grass_date >= date_trunc('week', current_date) - interval '49' day then 'W-7*'
end as time_break
, case when sip.cb_option = 1 and sip.mst_country = 'MY' then 'CB SIP'
when sip.cb_option = 1 and sip.mst_country = 'TW' then 'TB SIP'
when sip.cb_option = 1 and sip.mst_country = 'BR' then 'BR SIP'
when sip.cb_option = 1 and sip.mst_country = 'SG' then 'KRCB SIP'
when sip.cb_option = 0 then concat('Local', ' ', sip.mst_country, 'SIP')
else 'CB'
end as shop_type
, count(distinct sls.orderid) as gross_order
, count(distinct if(sls.payment_method = 6, sls.orderid, null)) as cod_order
, count(distinct if(sls.payment_method = 6 and sls.delivery_failed_time > sls.pickup_done_time, sls.orderid, null)) as cod_failed_order
from
(
select distinct shopid
, orderid
, order_placed_local_date as grass_date
, payment_method
, grass_region
, delivery_failed_time
, pickup_done_time -- Added by Chen Chen to align with SLS definition of COD failed delivery
from regcbbi_others.cb_logistics_mart_test
where order_placed_local_date between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '48' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
) sls
left join sip
on sls.shopid = sip.affi_shopid
group by 1, 2, 3