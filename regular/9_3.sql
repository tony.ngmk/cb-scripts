with
price_config as 
(
select grass_region
,currency
,CAST(sip_exchange AS DOUBLE) sip_exchange --> USD/grass_region currency
from regcbbi_others.sip__sip_exchange__tmp__reg__s2
where cb_option = '1'
and ingestion_timestamp = (SELECT MAX(ingestion_timestamp) max FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2)
)
,margin as 
(
select DISTINCT grass_region
,mst_country
,CAST(sip_margin AS DOUBLE)/100.00 AS sip_margin
,CAST(valid_from AS DATE) valid_from
,CAST(valid_to AS DATE) valid_to
from regcbbi_others.sip__country_margin__tmp__reg__s2
where ingestion_timestamp = (SELECT MAX(ingestion_timestamp) max_ingestion FROM regcbbi_others.sip__country_margin__tmp__reg__s2)
and cb_option = '1'
)
-- all settlement related amount converted to USD using SIP system exchange rates
,o as 
(
select DISTINCT o.wh_outbound_date
,o.grass_date AS create_date
,o.mst_country
,o.grass_region
,o.order_id
,o.order_sn
,o.payment_method_id
,o.logistics_status_id
-- , o.order_be_status
,asf_sls
,o.actual_buyer_paid_shipping_fee
,o.actual_shipping_rebate_by_shopee_amt
,o.pv_rebate_by_shopee_amt
,o.sv_rebate_by_shopee_amt
,o.card_rebate_by_bank_amt
,o.card_rebate_by_shopee_amt
,o.coin_used_cash_amt
,o.pv_coin_earn_by_shopee_amt
,o.sv_coin_earn_by_shopee_amt
,o.actual_shipping_rebate_by_seller_amt
,o.seller_snapshot_discount_shipping_fee
,o.escrow_to_seller_amt
,o.gmv
,o.gmv_usd
,o.pv_coin_earn_by_seller_amt
,o.sv_coin_earn_by_seller_amt
,o.pv_rebate_by_seller_amt
,o.sv_rebate_by_seller_amt
,o.seller_txn_fee --platform charge SIP
,o.seller_txn_fee_shipping_fee --platform charge SIP
,o.buyer_txn_fee
,o.service_fee --platform charge SIP
,refund_amount
,o.tax
,o.tax_exemption_amt
,actual_weight
,reason
,o.shop_id
,mst_shopid
,sip_rate
,shop_margin
,sip_margin
-- stmt related info
,order_adj_type
,order_adj_amount
,o.sip_settlement --order level stmt 
,sip_seller_comm_fee -- SIP charge seller
,sip_seller_service_fee -- SIP charge seller
,pc1.sip_exchange AS stmt_exchange
,pc1.currency AS stmt_currency
,pc2.sip_exchange AS mst_exchange
,pc2.currency AS mst_currency
,pc3.sip_exchange AS affi_exchange
,pc3.currency AS affi_currency
,o.offline_exchange AS offline_ord_exchange
,o.offline_currency AS offline_ord_currency
,o.sip_seller_comm_rate
,o.sip_seller_service_rate
,o.sip_seller_txn_rate
,o.sip_seller_management_rate
,o.sip_seller_spp_rate
,TRY(COALESCE(seller_txn_fee/gmv *1.00, 0)) AS txn_rate
from regcbbi_others.sip_cn_pnl_order_tab_v2 o
left join (
select *
from price_config
) pc1
on o.stmt_currency = pc1.currency -- USD / stmt curr
left join (
select *
from price_config
) pc2
on o.mst_country = pc2.grass_region -- USD / mst curr
left join (
select *
from price_config
) pc3
on o.grass_region = pc3.grass_region -- USD / affi curr
left join margin
on o.grass_region = margin.grass_region
and o.mst_country = margin.mst_country
and o.grass_date BETWEEN margin.valid_from
and margin.valid_to
where (
o.wh_inbound_date + interval '1' day < o.cancel_timestamp
or o.cancel_user_id IS NULL
)
)
,pnl_itm_distinct as 
(
select distinct *
from regcbbi_others.sip_cn_pnl_item_tab
-- added a temporary fix as there are duplicate orders in the intermediate table
-- impact: affi_hpfn double counted
)
,oi as 
(
select DISTINCT order_id
,order_sn
,SUM(commission_base_amt) commission_base_amt
,SUM(item_rebate_by_seller_amt) AS item_rebate_by_seller_amt
,SUM(item_rebate_by_shopee_amt) AS item_rebate_by_shopee_amt
,SUM(item_amount) AS item_amount
,SUM(commission_fee) AS commission_fee --platform charge SIP
,SUM(service_fee_item) AS service_fee_item --platform charge SIP
,SUM(item_tax_amt) AS item_tax_amt
,SUM(item_tax_exemption_amt) AS item_tax_exemption_amt
,SUM(item_weight_pp) AS item_weight_pp
,SUM(item_price_pp) AS item_price_pp
,SUM(order_price_pp) AS order_price_pp
,SUM(mst_dp) AS mst_dp
,SUM(mst_hpfs) AS mst_hpfs
,SUM(affi_hpfn) AS affi_hpfn
,SUM(settlement_campaign) AS settlement_campaign -- offline sku system stmt amt
,SUM(campaign_price/offline_sku_exchange) AS campaign_price_usd -- offline sku offline stmt amt
,MAX(sku_off_kind) AS sku_off_kind
,SUM(cfs_gmv) AS cfs_gmv
,SUM(cfs_item_price) AS cfs_item_price
,SUM(cfs_order_price) AS cfs_order_price
,SUM(cfs_shopee_rebate) AS cfs_shopee_rebate
,SUM(cfs_hpfn) AS cfs_hpfn
,SUM(cfs_system_settlement) AS cfs_system_settlement
,SUM(cfs_offline_settlement) AS cfs_offline_settlement -- offline or stmt curr
,SUM(campaign_gmv) AS campaign_gmv
,SUM(campaign_item_price) AS campaign_item_price
,SUM(campaign_order_price) AS campaign_order_price
,SUM(campaign_shopee_rebate) AS campaign_shopee_rebate
,SUM(campaign_hpfn) AS campaign_hpfn
,SUM(campaign_system_settlement) AS campaign_system_settlement
,SUM(campaign_offline_settlement) AS campaign_offline_settlement
,SUM(bundle_gmv) AS bundle_gmv
,SUM(bundle_item_price) AS bundle_item_price
,SUM(bundle_order_price) AS bundle_order_price
,SUM(bundle_shopee_rebate) AS bundle_shopee_rebate
,SUM(bundle_hpfn) AS bundle_hpfn
,SUM(bundle_system_settlement) AS bundle_system_settlement
,SUM(bundle_offline_settlement) AS bundle_offline_settlement
,TRY(COALESCE(SUM(commission_fee)/SUM(commission_base_amt)*1.00, 0)) AS comm_rate
,TRY(COALESCE(SUM(service_fee_item)/SUM(commission_base_amt)*1.00, 0)) AS serv_rate
,SUM(TRY(item_margin * (sku_settlement/sku_stmt_exchange*sku_affi_exchange+affi_hpfn) * (1+(commission_fee*1.00/commission_base_amt)+(service_fee_item*1.00/commission_base_amt)+0.02))) AS item_margin
from (
select DISTINCT i.order_id
,i.order_sn
,item_id
,case when pc4.currency IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange
end as offline_sku_exchange
,case when pc4.currency IS NULL THEN pc5.currency ELSE pc4.currency
end as offline_sku_currency
,pc1.sip_exchange AS sku_stmt_exchange
,pc3.sip_exchange AS sku_affi_exchange
,item_margin
,SUM(commission_base_amt) commission_base_amt
,SUM(item_rebate_by_seller_amt) AS item_rebate_by_seller_amt
,SUM(item_rebate_by_shopee_amt) AS item_rebate_by_shopee_amt
,SUM(item_amount) AS item_amount
,SUM(commission_fee) AS commission_fee --platform charge SIP
,SUM(i.service_fee) AS service_fee_item --platform charge SIP
,SUM(item_tax_amt) AS item_tax_amt
,SUM(item_tax_exemption_amt) AS item_tax_exemption_amt
,SUM(item_weight_pp*item_amount) AS item_weight_pp
,SUM(item_price_pp*item_amount) AS item_price_pp
,SUM(order_price_pp*item_amount) AS order_price_pp
,SUM(IF(mst_promo_price = 0, mst_orig_price, mst_promo_price)*item_amount) AS mst_dp
,SUM(mst_hpfs*item_amount) AS mst_hpfs
,SUM(affi_hpfn*item_amount) AS affi_hpfn
,SUM(IF(i.kind > 0, i.sip_settlement_pp*item_amount, 0)) AS settlement_campaign -- offline sku system stmt amt
,SUM(campaign_price*item_amount) AS campaign_price -- offline sku offline stmt amt
,MAX(i.kind) AS sku_off_kind
,SUM(IF(item_promotion_source = 'flash_sale', i.gmv, 0)) AS cfs_gmv
,SUM(IF(item_promotion_source = 'flash_sale', item_price_pp*item_amount)) AS cfs_item_price
,SUM(IF(item_promotion_source = 'flash_sale', order_price_pp*item_amount)) AS cfs_order_price
,SUM(IF(item_promotion_source = 'flash_sale', item_rebate_by_shopee_amt)) AS cfs_shopee_rebate
,SUM(IF(item_promotion_source = 'flash_sale', affi_hpfn*item_amount)) AS cfs_hpfn
,SUM(IF(item_promotion_source = 'flash_sale', i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS cfs_system_settlement
,SUM(IF(item_promotion_source = 'flash_sale', IF(campaign_price > 0, campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00, sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS cfs_offline_settlement -- offline or stmt curr
,SUM(IF(item_promotion_source = 'shopee', i.gmv, 0)) AS campaign_gmv
,SUM(IF(item_promotion_source = 'shopee', item_price_pp*item_amount)) AS campaign_item_price
,SUM(IF(item_promotion_source = 'shopee', order_price_pp*item_amount)) AS campaign_order_price
,SUM(IF(item_promotion_source = 'shopee', item_rebate_by_shopee_amt)) AS campaign_shopee_rebate
,SUM(IF(item_promotion_source = 'shopee', affi_hpfn*item_amount)) AS campaign_hpfn
,SUM(IF(item_promotion_source = 'shopee', i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS campaign_system_settlement
,SUM(IF(item_promotion_source = 'shopee', IF(campaign_price > 0, campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00, i.sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS campaign_offline_settlement
,SUM(IF(is_bundle_deal = 1, i.gmv, 0)) AS bundle_gmv
,SUM(IF(is_bundle_deal = 1, item_price_pp*item_amount)) AS bundle_item_price
,SUM(IF(is_bundle_deal = 1, order_price_pp*item_amount)) AS bundle_order_price
,SUM(IF(is_bundle_deal = 1, item_rebate_by_shopee_amt)) AS bundle_shopee_rebate
,SUM(IF(is_bundle_deal = 1, affi_hpfn*item_amount)) AS bundle_hpfn
,SUM(IF(is_bundle_deal = 1, i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS bundle_system_settlement
,SUM(IF(is_bundle_deal = 1, IF(campaign_price > 0, campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00, i.sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS bundle_offline_settlement
,SUM(i.sip_settlement_pp*item_amount) AS sku_settlement
from pnl_itm_distinct i
left join (
select *
from price_config
) pc1
on i.stmt_currency = pc1.currency -- USD / stmt curr 
left join (
select *
from price_config
) pc3
on i.grass_region = pc3.grass_region -- USD / affi curr
left join price_config pc4
on i.mst_curr = pc4.currency
left join price_config pc5
on i.mst_country = pc5.grass_region
group by 1, 2, 3, 4, 5, 6, 7, 8
)
group by 1, 2
)
-- convert all stmt amount to p currency 
,consol as 
(
select DISTINCT wh_outbound_date
,mst_country
,grass_region
,concat(stmt_currency, mst_currency) stmt_mst_currency_pair
,o.sip_seller_comm_rate
,o.sip_seller_service_rate
,o.sip_seller_txn_rate
,o.sip_seller_management_rate
,o.sip_seller_spp_rate
,COUNT(DISTINCT o.order_id) AS orders
,SUM(escrow_to_seller_amt) escrow_to_seller_amt
,SUM(gmv) AS gmv
,SUM(pv_rebate_by_shopee_amt) AS pv_rebate_by_shopee_amt
,SUM(sv_rebate_by_shopee_amt) AS sv_rebate_by_shopee_amt
,SUM(pv_coin_earn_by_seller_amt) AS pv_coin_earn_by_seller_amt
,SUM(sv_coin_earn_by_seller_amt) AS sv_coin_earn_by_seller_amt
,SUM(coin_used_cash_amt) AS coin_used_cash_amt
,SUM(card_rebate_by_bank_amt) AS card_rebate_by_bank_amt
,SUM(card_rebate_by_shopee_amt) AS card_rebate_by_shopee_amt
,SUM(item_rebate_by_shopee_amt) AS item_rebate_by_shopee_amt
,SUM(asf_sls) AS actual_shipping_fee
,SUM(actual_buyer_paid_shipping_fee) AS actual_buyer_paid_shipping_fee
,SUM(actual_shipping_rebate_by_shopee_amt) AS actual_shipping_rebate_by_shopee_amt
,SUM(actual_shipping_rebate_by_seller_amt) actual_shipping_rebate_by_seller_amt
-- , SUM(seller_txn_fee) AS shopee_txn_fee
,SUM(seller_txn_fee) AS shopee_txn_fee
,SUM(commission_fee) AS shopee_commission_fee
,SUM(service_fee_item) AS shopee_service_fee
,SUM(IF(COALESCE(item_tax_amt, 0) = 0, tax, item_tax_amt)) AS tax
,SUM(IF(COALESCE(item_tax_exemption_amt, 0) = 0, tax_exemption_amt, item_tax_exemption_amt)) AS tax_exemption_amt
,SUM(IF(COALESCE(order_adj_type, 0) > 0, (order_adj_amount/offline_ord_exchange*1.00- settlement_campaign/stmt_exchange*1.00 + campaign_price_usd*1.00)*mst_exchange, IF(COALESCE(sku_off_kind, 0) > 0, (sip_settlement/stmt_exchange*1.00 - settlement_campaign/stmt_exchange*1.00 + campaign_price_usd*1.00)*mst_exchange, sip_settlement/stmt_exchange*1.00*mst_exchange))) AS final_settlement_mst
,SUM(IF(COALESCE(refund_amount, 0) > 0, refund_amount, 0)) AS refund_amount
,SUM(IF(logistics_status_id = 6 AND payment_method_id = 6, gmv + pv_rebate_by_shopee_amt + sv_rebate_by_shopee_amt - pv_coin_earn_by_seller_amt - sv_coin_earn_by_seller_amt + coin_used_cash_amt + card_rebate_by_bank_amt + card_rebate_by_shopee_amt + item_rebate_by_shopee_amt + actual_shipping_rebate_by_shopee_amt - seller_txn_fee - commission_fee - service_fee_item - IF(COALESCE(item_tax_amt, 0) = 0, tax, item_tax_amt), 0)) AS cod_fail
,SUM(sip_settlement/stmt_exchange*1.00*mst_exchange) AS settlement_amount_system_mst
,SUM(IF(order_adj_type = 6, (sip_settlement/stmt_exchange*1.00 - order_adj_amount/offline_ord_exchange*1.00)*mst_exchange, 0)) AS offline_adjustment_mst
,SUM(CASE WHEN order_adj_type = 7 THEN ((sip_settlement/stmt_exchange*1.00 - order_adj_amount/offline_ord_exchange*1.00)*mst_exchange)
WHEN (order_adj_type <> 7 OR order_adj_type IS NULL) AND 
((grass_region = 'TH' AND refund_amount >= 150) OR
(grass_region = 'PH' AND refund_amount >= 250) OR
(grass_region = 'BR' AND refund_amount >= 26) OR
(grass_region = 'VN' AND refund_amount >= 113550) OR
(grass_region = 'ID' AND refund_amount >= 71280) OR
(grass_region = 'SG' AND refund_amount >= 6.8) OR
(grass_region = 'MY' AND refund_amount >= 21) OR
(grass_region = 'PL' AND refund_amount >= 20) OR 
(grass_region = 'MX' AND refund_amount >= 103) OR 
(grass_region = 'TW' AND refund_amount > 0)) 
THEN IF(sip_settlement/stmt_exchange*1.00 - TRY(refund_amount/affi_exchange*1.00) < 0, sip_settlement/stmt_exchange*1.00*mst_exchange, TRY(refund_amount/affi_exchange*1.00)*mst_exchange)
ELSE 0 END) AS rr_offset_mst
,SUM(IF(logistics_status_id = 6 AND payment_method_id = 6 AND gmv_usd >= 20, sip_settlement/stmt_exchange*1.00*mst_exchange, 0)) AS cod_offset_mst
,SUM(pv_rebate_by_seller_amt) AS pv_rebate_by_seller_amt
,SUM(sv_rebate_by_seller_amt) AS sv_rebate_by_seller_amt
,SUM(affi_hpfn) AS affi_hpfn
,SUM(seller_snapshot_discount_shipping_fee) seller_snapshot_discount_shipping_fee
,SUM(item_price_pp) AS item_price
,SUM(order_price_pp) AS order_price
,SUM(cfs_gmv) AS cfs_gmv
,SUM(cfs_item_price) AS cfs_item_price
,SUM(cfs_shopee_rebate) AS cfs_shopee_rebate
,SUM(cfs_hpfn) AS cfs_hpfn
,SUM(cfs_system_settlement*mst_exchange) AS cfs_system_settlement_mst
,SUM(cfs_offline_settlement*mst_exchange) AS cfs_offline_settlement_mst
,SUM(campaign_gmv) AS campaign_gmv
,SUM(campaign_item_price) AS campaign_item_price
,SUM(campaign_shopee_rebate) AS campaign_shopee_rebate
,SUM(campaign_hpfn) AS campaign_hpfn
,SUM(campaign_system_settlement*mst_exchange) AS campaign_system_settlement_mst
,SUM(campaign_offline_settlement*mst_exchange) AS campaign_offline_settlement_mst
,SUM(bundle_gmv) AS bundle_gmv
,SUM(bundle_item_price) AS bundle_item_price
,SUM(bundle_order_price) AS bundle_order_price
,SUM(bundle_hpfn) AS bundle_hpfn
,SUM(bundle_system_settlement*mst_exchange) AS bundle_system_settlement_mst
,SUM(bundle_offline_settlement*mst_exchange) AS bundle_offline_settlement_mst
,SUM(item_weight_pp) AS item_weight
,SUM(actual_weight) AS actual_weight
,SUM(IF(order_adj_type = 8, (sip_settlement/stmt_exchange*1.00 - order_adj_amount/offline_ord_exchange*1.00)*mst_exchange, 0)) AS seller_voucher_subsidy_mst
,SUM(IF(order_adj_type = 4, (sip_settlement/stmt_exchange*1.00 - order_adj_amount/offline_ord_exchange*1.00)*mst_exchange, 0)) AS bundle_subsidy_mst
,SUM(pv_coin_earn_by_shopee_amt) pv_coin_earn_by_shopee_amt
,SUM(sv_coin_earn_by_shopee_amt) sv_coin_earn_by_shopee_amt
,SUM(CASE WHEN stmt_currency IN ('CNY', 'USD') THEN TRY((commission_fee*1.00/commission_base_amt)*(sip_settlement/stmt_exchange*affi_exchange+affi_hpfn))
ELSE 0 END) AS cnsc_comm_fee
,SUM(CASE WHEN stmt_currency IN ('CNY', 'USD') THEN TRY((service_fee_item*1.00/commission_base_amt)*(sip_settlement/stmt_exchange*affi_exchange+affi_hpfn))
ELSE 0 END) AS cnsc_service_fee
,SUM(CASE WHEN stmt_currency IN ('CNY', 'USD') THEN TRY((seller_txn_fee*1.00/gmv)*(sip_settlement/stmt_exchange*affi_exchange+affi_hpfn))
ELSE 0 END) AS cnsc_txn_fee
,SUM(TRY(shop_margin * (sip_settlement/stmt_exchange*affi_exchange+affi_hpfn) * (1+(commission_fee*1.00/commission_base_amt)+(service_fee_item*1.00/commission_base_amt)+(seller_txn_fee*1.00/gmv)))) AS shop_margin
,SUM(cfs_hpfn*(1+comm_rate+serv_rate+txn_rate)*(sip_margin+shop_margin))AS cfs_hpfn_w_fee
,SUM(cfs_offline_settlement*mst_exchange*(1+comm_rate+serv_rate+txn_rate)*(sip_margin+shop_margin)) AS cfs_offline_settlement_w_fee_mst
,SUM(campaign_hpfn*(1+comm_rate+serv_rate+txn_rate)*(sip_margin+shop_margin)) AS campaign_hpfn_w_fee
,SUM(campaign_offline_settlement*mst_exchange*(1+comm_rate+serv_rate+txn_rate)*(sip_margin+shop_margin)) AS campaign_offline_settlement_w_fee_mst
,SUM(item_amount) item_amount
,SUM(TRY((sip_margin-1) * (sip_settlement/stmt_exchange*affi_exchange+affi_hpfn) * (1+(commission_fee*1.00/commission_base_amt)+(service_fee_item*1.00/commission_base_amt)+(seller_txn_fee*1.00/gmv)))) AS country_margin
,SUM(commission_base_amt) commission_base_amt
,SUM(item_margin) item_margin
from o
join oi
on o.order_id = oi.order_id
where wh_outbound_date >= CURRENT_DATE - INTERVAL '90' DAY
and mst_country <> 'SG'
group by 1, 2, 3, 4, 5, 6, 7, 8, 9
)
,consol_2 as 
(
select wh_outbound_date
,mst_country
,grass_region
,stmt_mst_currency_pair
,orders
,escrow_to_seller_amt
,gmv
,pv_rebate_by_shopee_amt
,sv_rebate_by_shopee_amt
,pv_coin_earn_by_seller_amt
,sv_coin_earn_by_seller_amt
,coin_used_cash_amt
,card_rebate_by_bank_amt
,card_rebate_by_shopee_amt
,item_rebate_by_shopee_amt
,actual_shipping_fee
,actual_buyer_paid_shipping_fee
,actual_shipping_rebate_by_shopee_amt
,actual_shipping_rebate_by_seller_amt
,shopee_txn_fee
,shopee_commission_fee
,shopee_service_fee
,tax
,tax_exemption_amt
,final_settlement_mst
,final_settlement_mst * sip_seller_comm_rate AS sip_commission_fee_mst
,final_settlement_mst * sip_seller_service_rate AS sip_svc_fee_mst
,final_settlement_mst * sip_seller_spp_rate AS spp_revenue_mst
,refund_amount
,cod_fail
,settlement_amount_system_mst
,offline_adjustment_mst
,rr_offset_mst
,cod_offset_mst
,pv_rebate_by_seller_amt
,sv_rebate_by_seller_amt
,affi_hpfn
,seller_snapshot_discount_shipping_fee
,item_price
,order_price
,cfs_gmv
,cfs_item_price
,cfs_shopee_rebate
,cfs_hpfn
,cfs_system_settlement_mst
,cfs_offline_settlement_mst
,campaign_gmv
,campaign_item_price
,campaign_shopee_rebate
,campaign_hpfn
,campaign_system_settlement_mst
,campaign_offline_settlement_mst
,bundle_gmv
,bundle_item_price
,bundle_order_price
,bundle_hpfn
,bundle_system_settlement_mst
,bundle_offline_settlement_mst
,item_weight
,actual_weight
,seller_voucher_subsidy_mst
,bundle_subsidy_mst
,pv_coin_earn_by_shopee_amt
,sv_coin_earn_by_shopee_amt
,cnsc_comm_fee
,cnsc_service_fee
,cnsc_txn_fee
,shop_margin
,cfs_hpfn_w_fee
,cfs_offline_settlement_w_fee_mst
,campaign_hpfn_w_fee
,campaign_offline_settlement_w_fee_mst
,item_amount
,country_margin
,final_settlement_mst * sip_seller_txn_rate AS txn_revenue_mst
,final_settlement_mst * sip_seller_management_rate AS management_revenue_mst
,item_margin
from consol
)
select distinct wh_outbound_date
,mst_country
,grass_region
,stmt_mst_currency_pair
,SUM(orders) as orders
,SUM(escrow_to_seller_amt) as escrow_to_seller_amt
,SUM(gmv) as gmv
,SUM(pv_rebate_by_shopee_amt) as pv_rebate_by_shopee_amt
,SUM(sv_rebate_by_shopee_amt) as sv_rebate_by_shopee_amt
,SUM(pv_coin_earn_by_seller_amt) as pv_coin_earn_by_seller_amt
,SUM(sv_coin_earn_by_seller_amt) as sv_coin_earn_by_seller_amt
,SUM(coin_used_cash_amt) as coin_used_cash_amt
,SUM(card_rebate_by_bank_amt) as card_rebate_by_bank_amt
,SUM(card_rebate_by_shopee_amt) as card_rebate_by_shopee_amt
,SUM(item_rebate_by_shopee_amt) as item_rebate_by_shopee_amt
,SUM(actual_shipping_fee) as actual_shipping_fee
,SUM(actual_buyer_paid_shipping_fee) as actual_buyer_paid_shipping_fee
,SUM(actual_shipping_rebate_by_shopee_amt) as actual_shipping_rebate_by_shopee_amt
,SUM(actual_shipping_rebate_by_seller_amt) as actual_shipping_rebate_by_seller_amt
,SUM(shopee_txn_fee) as shopee_txn_fee
,SUM(shopee_commission_fee) as shopee_commission_fee
,SUM(shopee_service_fee) as shopee_service_fee
,SUM(tax) as tax
,SUM(tax_exemption_amt) as tax_exemption_amt
,SUM(final_settlement_mst) as final_settlement_mst
,SUM(sip_commission_fee_mst) as sip_commission_fee_mst
,SUM(management_revenue_mst) as management_revenue_mst
,SUM(spp_revenue_mst) as spp_revenue_mst
,SUM(refund_amount) as refund_amount
,SUM(cod_fail) as cod_fail
,SUM(settlement_amount_system_mst) as settlement_amount_system_mst
,SUM(offline_adjustment_mst) as offline_adjustment_mst
,SUM(rr_offset_mst) as rr_offset_mst
,SUM(cod_offset_mst) as cod_offset_mst
,SUM(pv_rebate_by_seller_amt) as pv_rebate_by_seller_amt
,SUM(sv_rebate_by_seller_amt) as sv_rebate_by_seller_amt
,SUM(affi_hpfn) as affi_hpfn
,SUM(seller_snapshot_discount_shipping_fee) as seller_snapshot_discount_shipping_fee
,SUM(item_price) as item_price
,SUM(order_price) as order_price
,SUM(cfs_gmv) as cfs_gmv
,SUM(cfs_item_price) as cfs_item_price
,SUM(cfs_shopee_rebate) as cfs_shopee_rebate
,SUM(cfs_hpfn) as cfs_hpfn
,SUM(cfs_system_settlement_mst) as cfs_system_settlement_mst
,SUM(cfs_offline_settlement_mst) as cfs_offline_settlement_mst
,SUM(campaign_gmv) as campaign_gmv
,SUM(campaign_item_price) as campaign_item_price
,SUM(campaign_shopee_rebate) as campaign_shopee_rebate
,SUM(campaign_hpfn) as campaign_hpfn
,SUM(campaign_system_settlement_mst) as campaign_system_settlement_mst
,SUM(campaign_offline_settlement_mst) as campaign_offline_settlement_mst
,SUM(bundle_gmv) as bundle_gmv
,SUM(bundle_item_price) as bundle_item_price
,SUM(bundle_order_price) as bundle_order_price
,SUM(bundle_hpfn) as bundle_hpfn
,SUM(bundle_system_settlement_mst) as bundle_system_settlement_mst
,SUM(bundle_offline_settlement_mst) as bundle_offline_settlement_mst
,SUM(item_weight) as item_weight
,SUM(actual_weight) as actual_weight
,SUM(seller_voucher_subsidy_mst) as seller_voucher_subsidy_mst
,SUM(bundle_subsidy_mst) as bundle_subsidy_mst
,SUM(pv_coin_earn_by_shopee_amt) as pv_coin_earn_by_shopee_amt
,SUM(sv_coin_earn_by_shopee_amt) as sv_coin_earn_by_shopee_amt
,SUM(cnsc_comm_fee) as cnsc_comm_fee
,SUM(cnsc_service_fee) as cnsc_service_fee
,SUM(cnsc_txn_fee) as cnsc_txn_fee
,SUM(shop_margin) as shop_margin
,SUM(cfs_hpfn_w_fee) as cfs_hpfn_w_fee
,SUM(cfs_offline_settlement_w_fee_mst) as cfs_offline_settlement_w_fee_mst
,SUM(campaign_hpfn_w_fee) as campaign_hpfn_w_fee
,SUM(campaign_offline_settlement_w_fee_mst) as campaign_offline_settlement_w_fee_mst
,SUM(item_amount) as item_amount
,SUM(country_margin) as country_margin
,SUM(txn_revenue_mst) as txn_revenue_mst
,SUM(sip_svc_fee_mst) as sip_svc_fee_mst
,SUM(item_margin) item_margin
from consol_2
group by 1, 2, 3, 4
