WITH sip AS 
(
select distinct cast(affi.affi_shopid as bigint) as affi_shopid
, affi.mst_shopid
, mst.country as mst_country
, affi.country as affi_country
, mst.cb_option
from
(
select distinct cast(shopid as bigint) as shopid
, country
, cb_option
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
where cb_option = 1
) mst
join 
(
select distinct cast(affi_shopid as bigint) as affi_shopid
, affi_username
, cast(mst_shopid as bigint) as mst_shopid
, country
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where grass_region in ('SG','MY','PH','TH','ID','VN','TW')
and sip_shop_status != 3 
) affi
on mst.shopid = affi.mst_shopid
),

by_order_date as
(
select sls.grass_region
, sls.grass_date
, if(sip.affi_shopid is not null, 'SIP', 'CB(NS)') as shop_type
, count(distinct sls.orderid) as gross_orders
, count(distinct if(sls.payment_method = 6, sls.orderid, null)) as cod_orders
from
(
select distinct grass_region
, order_placed_local_date as grass_date
, shopid
, orderid
, payment_method
from regcbbi_others.cb_logistics_mart_test
where grass_region in ('SG','MY','PH','TH','ID','VN','TW')
and order_placed_local_date between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
) sls
left join sip
on sls.shopid = sip.affi_shopid
group by 1, 2, 3
),

by_cod_failed_date as
(
select sls.grass_region
, delivery_failed_date
, if(sip.affi_shopid is not null, 'SIP', 'CB(NS)') as shop_type
, count(distinct sls.orderid) as cod_failed_orders
from
(
select distinct grass_region
, orderid
, shopid
, DATE(from_unixtime(delivery_failed_time)) as delivery_failed_date
from regcbbi_others.cb_logistics_mart_test
where grass_region in ('SG','MY','PH','TH','ID','VN','TW')
and DATE(from_unixtime(delivery_failed_time)) between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
and delivery_failed_time > pickup_done_time -- Added by Chen Chen to align logic with SLS
and payment_method = 6
--and order_placed_local_date >= between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day -- Removed by Chen Chen
) sls
left join sip
on sls.shopid = sip.affi_shopid
group by 1, 2, 3
)

select distinct by_order_date.grass_region
, by_order_date.grass_date
, by_order_date.shop_type
, by_order_date.gross_orders
, by_order_date.cod_orders
, by_cod_failed_date.cod_failed_orders
from by_order_date
left join by_cod_failed_date
on by_order_date.grass_region = by_cod_failed_date.grass_region
and by_order_date.shop_type = by_cod_failed_date.shop_type
and by_order_date.grass_date = by_cod_failed_date.delivery_failed_date