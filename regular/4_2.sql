WITH
wms AS (
SELECT shop_id
FROM sbs_mart.shopee_scbs_db__shop_tab__reg_daily_s0_live
WHERE country IN ('ID')
AND ((fbs_tag = 0 AND vacation_mode = 0 AND shop_status = 1)
OR (fbs_tag = 1 AND vacation_mode = 0 AND cb_option = 1)
OR (fbs_tag = 1 AND vacation_mode = 0 AND cb_option = 0))
) 
, shop AS (
SELECT DISTINCT
mu.shopid
, mu.main_category
, mu.sku live_sku
, is_official_shop
, is_preferred_shop
, mu.USERNAME
, IF((wms.shop_id IS NULL), 'Y', 'N') NOT_WH_SHOP
, (CASE WHEN (SIP.SHOPID IS NOT NULL) THEN 'Y' ELSE 'N' END) SIP_SHOP
, (CASE WHEN (HB_SHOP = 'N') THEN 'Y' ELSE 'N' END) HB_ALLOW
FROM
(((
SELECT DISTINCT
shop_id shopid
, shop_level1_global_be_category main_category
, active_item_with_stock_cnt sku
, is_official_shop
, is_preferred_shop
, USER_NAME USERNAME
, (CASE WHEN (shop_level1_global_be_category IN ('Health', 'Beauty', 'Food & Beverages')) THEN 'Y' ELSE 'N' END) HB_SHOP
FROM
regcbbi_others.shop_profile
WHERE ((((((((status = 1) AND (user_status = 1)) AND (is_holiday_mode = 0)) AND (is_cb_shop = 0)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (grass_region = 'ID')) AND (tz_type = 'local')) AND ("date"("from_unixtime"(shop_first_listing_timestamp)) <= (current_date - INTERVAL '90' DAY)) AND is_official_shop = 0)
) mu
-- INNER JOIN shopee_regional_cb_team__idsip_jawa_barat_shop j ON CAST(j.shop_id AS BIGINT) = mu.shopid
LEFT JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live sip ON (sip.shopid = mu.shopid))
LEFT JOIN wms ON (wms.shop_id = mu.shopid))
) 
, o AS (
SELECT DISTINCT
o2.orderid
, o1.shop_id SHOPID
, o1.create_time
, o1.cancel_time
, o2.cancel_reason
, o1.status_ext
, o1.complete_time
, o1.grass_date
FROM
((
SELECT DISTINCT
order_id
, shop_id
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) create_time
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) cancel_time
, order_be_status_id status_ext
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) complete_time
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) grass_date
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ((is_cb_shop = 0) AND (tz_type = 'local') AND "date"("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR) >= current_date - INTERVAL '120' DAY AND grass_region = 'ID')
) o1
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'ID') AND "date"("from_unixtime"(create_time) - INTERVAL '1' HOUR) >= current_date - INTERVAL '120' DAY)
) o2 ON (o1.order_id = o2.orderid))
) 
, r AS (
SELECT DISTINCT
orderid
, shopid
, reason
, status
, "from_unixtime"(ctime) ctime
, "from_unixtime"(mtime) mtime
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'ID') AND date("from_unixtime"(mtime)) >= current_date - INTERVAL '90' DAY)
) 
, sku AS (
SELECT DISTINCT
shopid
, "count"(DISTINCT itemid) skus
FROM
(
SELECT DISTINCT
b.item_id itemid
, b.shop_id shopid
-- , (CASE WHEN (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.80014.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNT_80014_CHANNEL
-- , (CASE WHEN (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.80005.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNE_80005_CHANNEL
-- , (CASE WHEN (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.80015.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNT_80015_CHANNEL
-- , (CASE WHEN (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.80023.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNE_80023_CHANNEL
-- , (CASE WHEN (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.80020.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNE_80020_CHANNEL
-- , (CASE WHEN (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.80025.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNE_80025_CHANNEL
-- , (CASE WHEN (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.80088.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNE_80088_CHANNEL
, (CASE WHEN (CAST("json_extract"(shipping_info, '$.8003.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNE_8003_CHANNEL
FROM
(mp_item.dim_item__reg_s0_live b
INNER JOIN (SELECT shopid FROM shop) u ON (u.shopid = b.shop_id))
WHERE (((((b.grass_region = 'ID') AND (status = 1)) AND (stock > 0)) AND (is_cb_shop = 0)) AND (CAST("json_extract"(shipping_info, '$.8003.enabled') AS bigint) = 1) AND grass_date = current_date - interval '1' day AND tz_type = 'local')
) 
WHERE (JNE_8003_CHANNEL = 1)
GROUP BY 1
) 
, normal_sku_amount AS (
SELECT
"count"(DISTINCT itemid) normal_sku
, "count"(DISTINCT (CASE WHEN (stock > 0) THEN itemid ELSE null END)) live_item
, shopid
FROM
(
SELECT DISTINCT
item_id itemid
, shop_id shopid
, stock
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((((grass_region = 'ID') AND (status = 1)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (is_cb_shop = 0)) AND (seller_status = 1)) AND (shop_status = 1)) AND (is_holiday_mode = 0))
) 
GROUP BY 3
) 
, prohibited_item AS (
SELECT DISTINCT
shopid
, "count"(DISTINCT (CASE WHEN c.leaf_node_category_id IS NOT NULL THEN itemid ELSE null END)) prohibited_item_cat
, "count"(DISTINCT (CASE WHEN (k.keyword IS NOT NULL) THEN itemid ELSE null END)) prohibited_item_keyword
FROM
((
SELECT DISTINCT
item_id itemid
, shop_id shopid
, name
, global_be_category_id
FROM
mp_item.dim_item__reg_s0_live
WHERE ((((((((grass_region = 'ID') AND (is_cb_shop = 0)) AND (status = 1)) AND (stock > 0)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (seller_status = 1)) AND (shop_status = 1)) AND (is_holiday_mode = 0))
) i
LEFT JOIN (
SELECT DISTINCT keyword
FROM
regcbbi_others.sip__idsip_prohibited_keyword__tmp__reg__s0
) k ON "regexp_like"(name, keyword)
LEFT JOIN (SELECT DISTINCT CAST(leaf_node_category_id AS bigint) leaf_node_category_id FROM regcbbi_others.sip__idsip_prohibited_global_cate__reg__s0) c ON c.leaf_node_category_id = i.global_be_category_id)
GROUP BY 1
) 
, opt_check_for_prohibited AS (
SELECT DISTINCT
shopid
, status_prohibited_cat
, status_prohibited_keyword
FROM
(
SELECT
s.shopid
, live_item
, (CASE WHEN ((CAST(COALESCE(prohibited_item_cat, 0) AS double) / live_item) >= DECIMAL '0.7') THEN 'Fail' ELSE 'Pass' END) status_prohibited_cat
, (CASE WHEN ((CAST(COALESCE(prohibited_item_keyword, 0) AS double) / live_item) >= DECIMAL '0.7') THEN 'Fail' ELSE 'Pass' END) status_prohibited_keyword
FROM
(normal_sku_amount s
LEFT JOIN prohibited_item l ON (s.shopid = l.shopid))
) 
) 
SELECT *
FROM
(
SELECT DISTINCT
grass_region
, shopid
, is_official_shop
, main_category
, live_sku
, userid
, USERNAME
, NFR_included_cancellations_30d
, NFR_included_rr_30d
, gross_orders_30d
, cr_l90d
, rr_l30d
, "sum"(live_sku) OVER (ORDER BY gross_orders_30d DESC, cr_l90d ASC, shopid ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) acc_sku
FROM
(
SELECT DISTINCT
grass_region
, a.shopid
, a.userid
, main_category
, live_sku
, USERNAME
, is_official_shop
, COALESCE("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)), 0) NFR_included_cancellations_30d
, COALESCE("count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END)), 0) NFR_included_rr_30d
-- , COALESCE("count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid ELSE null END)), 0) net_orders_30d
, (COALESCE("count"(DISTINCT (CASE WHEN (o.create_time BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) THEN o.orderid ELSE null END)), 0) / DECIMAL '30.00') gross_orders_30d
, COALESCE(TRY((("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) * DECIMAL '1.000000') / (("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) + "count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END))) + "count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid ELSE null END))))), 0) cr_l90d
, COALESCE(TRY((("count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END)) * DECIMAL '1.000000') / (("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) + "count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END))) + "count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid ELSE null END))))), 0) rr_l30d
FROM
(((((((((
SELECT DISTINCT
a.shopid
, a.userid
, normal_sku
, live_sku
, is_official_shop
, is_preferred_shop
, NOT_WH_SHOP
, COALESCE(skus, 0) supportive_channel_sku
, shop.main_category
, SIP_SHOP
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80014.enabled') AS bigint), 0) J_T_Express
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80005.enabled') AS bigint), 0) JNE_Regular__Cashless
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80015.enabled') AS bigint), 0) SICEPAT
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80023.enabled') AS bigint), 0) IDE
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80020.enabled') AS bigint), 0) njv
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80025.enabled') AS bigint), 0) Anteraja
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80088.enabled') AS bigint), 0) SPX
, (COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80023.enabled') AS bigint), 0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80015.enabled') AS bigint), 0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80005.enabled') AS bigint), 0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80014.enabled') AS bigint),0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80020.enabled') AS bigint),0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80025.enabled') AS bigint),0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80088.enabled') AS bigint),0)) overall
, status_prohibited_cat
, status_prohibited_keyword
, a.grass_region
, USERNAME
, HB_ALLOW
FROM
(((((
SELECT DISTINCT
SHOPID
, extinfo
, userid
, grass_region
FROM
marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE ((grass_region = 'ID') AND (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.8003.enabled') AS bigint) = 1))
) a
INNER JOIN shop ON (shop.shopid = a.shopid))
LEFT JOIN sku ON (a.shopid = sku.shopid))
LEFT JOIN normal_sku_amount ON (normal_sku_amount.shopid = a.shopid))
LEFT JOIN opt_check_for_prohibited oc ON (oc.shopid = a.shopid))
) a
LEFT JOIN o ON (o.shopid = a.shopid))
LEFT JOIN (
SELECT DISTINCT CAST(SHOPID AS bigint) SHOPID
FROM
regcbbi_others.sip__idsip_migration_maintain_shop_list__tmp__reg__s0
UNION 
SELECT DISTINCT CAST(mst_shopid AS bigint) shopid
FROM regcbbi_others.shopee_regional_cb_team__id_localsip_tos_shoplist
UNION 
SELECT DISTINCT CAST(shopid AS bigint) shopid
FROM regcbbi_others.shopee_regional_cb_team__idsip_migration_maintain_shop_list2
UNION 
SELECT DISTINCT CAST(shop_id AS bigint) shopid
FROM regcbbi_others.shopee_regional_cb_team__idsip_jawa_barat_shop
) EX ON (EX.SHOPID = A.SHOPID))
LEFT JOIN (
SELECT DISTINCT CAST(shop_id AS bigint) shop_id
FROM
regcbbi_others.idsip_book
) bk ON (bk.shop_id = a.shopid))
LEFT JOIN (
SELECT DISTINCT CAST(shop_id AS bigint) shop_id
FROM
regcbbi_others.idsip_drop_shipper
) ds ON (ds.shop_id = a.shopid))
LEFT JOIN (
SELECT DISTINCT CAST(shop_id AS bigint) shop_id
FROM
regcbbi_others.idsip_fnb
) fnb ON (fnb.shop_id = a.shopid))
LEFT JOIN (
SELECT DISTINCT shop_id
FROM
regcbbi_others.sip__idsip_new_onboarding_criteria_simulation_1__df__id__s0
) bp ON (bp.shop_id = a.shopid))
LEFT JOIN r ON (o.orderid = r.orderid))
-- LEFT JOIN (
-- SELECT DISTINCT shopid
-- FROM
-- shopee.shopee_logistic_id_db__logistic_shop_tab
-- WHERE (channelid = 8003)
-- ) c ON (c.shopid = a.shopid)
LEFT JOIN (
SELECT DISTINCT shop_id
FROM
regcbbi_others.shopee_regional_cb_team__dimension_issue_sku
) cig ON (CAST(cig.shop_id AS bigint) = a.shopid))
WHERE ((((((((((((SIP_SHOP = 'N') AND (NOT_WH_SHOP = 'Y')) AND (status_prohibited_keyword = 'Pass')) AND (status_prohibited_cat = 'Pass')) AND (overall >= 1)) 
-- AND (c.shopid IS NOT NULL)
) AND (supportive_channel_sku > 0)) AND (EX.SHOPID IS NULL)) AND (fnb.shop_id IS NULL)) AND (ds.shop_id IS NULL)) AND (HB_ALLOW = 'Y')) AND (bp.shop_id IS NULL) AND cig.shop_id IS NULL)
GROUP BY 1, 2, 3, 4, 5, 6, 7
) 
WHERE ((gross_orders_30d > DECIMAL '0.1') AND (cr_l90d <= DECIMAL '0.03') AND (rr_l30d < DECIMAL '0.01'))
)
WHERE (acc_sku < 7200000)