WITH
dim AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE (grass_date between current_date-interval'63'day and CURRENT_DATE-INTERVAL'1'DAY )
and grass_region in ('SG','MY','PH','TH','ID','VN','TW') 
) 
, cb AS (SELECT DISTINCT CAST(child_shopid AS INTEGER) AS child_shopid
, grass_region
, gp_account_name
, gp_account_owner
, gp_account_owner_userrole_name
, child_account_name 
, (CASE WHEN (gp_account_owner_userrole_name LIKE '%KAM%') THEN 'KAM' WHEN (gp_account_owner_userrole_name LIKE '%PSM%') THEN 'PSM' 
ELSE 'CSS' END) seller_type
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
WHERE grass_region in ('TW','MY','BR','SG') ) 

, sip AS (
SELECT DISTINCT
affi_shopid
, a.country as affi_country 
, mst_shopid
, b.country as mst_country 
-- ,case when cb_option=1 then 'CB SIP' else 'Local SIP' end as shop_type 
, user_status
, is_holiday_mode
, source
FROM
(marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT shopid
, country
, cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) b ON (a.mst_shopid = b.shopid)) 
join (select distinct shop_id 
, user_status
, is_holiday_mode
, source 
from regcbbi_others.shop_profile
where grass_date = current_date-interval'1'day and is_cb_shop=1 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW')) u on u.shop_id = a.affi_shopid 
WHERE grass_region in ('SG','MY','PH','TH','ID','VN','TW') and sip_shop_status!=3
) 
, orders as (
select distinct order_id 
, order_be_status 
, logistics_status
, is_cb_shop
, grass_region
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_region in ('SG','MY','PH','TH','ID','VN','TW') 
and grass_date >= current_date-interval'60'day 
and date(split(create_datetime,' ')[1]) >= current_date-interval'60'day
)


, o as (
SELECT DISTINCT
shop_id
, date(split(order_create_datetime,' ')[1]) as create_time
, order_id 
, order_sn 
-- , order_be_status_id
, buyer_id 
-- , payment_method_id
, is_cod_order
, lm_first_attempt_delivery_datetime
, lm_first_failed_delivery_attempt_message
, lm_second_failed_delivery_attempt_message
from mp_ofm.dwd_forder_all_ent_df__reg_s0_live
WHERE ( date(split(order_create_datetime,' ')[1]) >= current_date-interval'60'day ) and tz_type='local' and is_cb_shop=1
and grass_region in ('SG','MY','PH','TH','ID','VN','TW') 
and grass_date >= current_date-interval'60'day and is_cod_order=1 
)

, raw AS (
SELECT DISTINCT
oversea_orderid
, "max"(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
where date(from_unixtime(ctime)) >= current_date-interval'90'day and local_country IN ('tw','my','id','vn') and oversea_country in ('my','sg','vn','ph','th','br','mx')
GROUP BY 1
) 

, l as (SELECT DISTINCT
o.shop_id shopid
, o.order_id 
, o.order_sn 
, o.create_time 
, o.buyer_id
, lm_first_failed_delivery_attempt_message
, lm_second_failed_delivery_attempt_message
, ctime 
, lm_first_attempt_delivery_datetime
from o 
join (select distinct orderid 
, date(from_unixtime(min(ctime))) ctime
from marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live
WHERE new_status IN (6) and grass_region in ('SG','MY','PH','TH','ID','VN','TW') 
group by 1 
having date(from_unixtime(min(ctime)))>= current_date-interval'30'day 
) l on l.orderid = o.order_id 

-- group by 1, 2 ,3,4 ,5 , 6 ,7
) 


select sip.* 
, l.buyer_id
, l.order_id 
, l.order_sn 
, l.create_time
, lm_first_failed_delivery_attempt_message
, lm_second_failed_delivery_attempt_message
, l.ctime as cod_failed_time 
, l.lm_first_attempt_delivery_datetime
, o1.order_be_status affi_order_be_status
, o1.logistics_status affi_logistics_status
, o2.order_be_status mst_order_be_status
, o2.logistics_status mst_logistics_status
from l 
join sip on sip.affi_shopid =l.shopid 
join (select * 
from orders
where is_cb_shop=1 
) o1 on o1.order_id = l.order_id 
left join raw on raw.oversea_orderid=o1.order_id 
left join (select * 
from orders
where grass_region in ('TW','MY','ID','VN')
) o2 on o2.order_id = raw.local_orderid

where l.ctime between current_date-interval'14'day and current_date-interval'1'day