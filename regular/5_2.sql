WITH
sip AS (
SELECT DISTINCT
affi_shopid
, mst_shopid
, a.country affi_country
, v.country mst_country
FROM
(marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE (cb_option = 1) and country ='TW'
) v ON (v.shopid = a.mst_shopid))
where a.country in ('SG','MY','ID','PH','TH','VN','BR') AND grass_region IN ('SG','MY','ID','PH','TH','VN','BR') and sip_shop_status !=3
) 
, cb AS (
SELECT DISTINCT CAST(child_shopid AS bigint) child_shopid
FROM
cncbbi_general.shopee_cb_seller_profile_with_old_column
where grass_region IN ('SG','MY','ID','PH','TH','VN','BR','TW')
) 

, R AS (
SELECT DISTINCT ORDERID 
, REASON 
, status
, CTIME 
, SHOPID 
FROM marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
where grass_region IN ('SG','MY','ID','PH','TH','VN','BR','TW')
and cb_option=1 
)


, break AS (
SELECT DISTINCT
(CASE WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '7' DAY) AND (current_date - INTERVAL '1' DAY)) THEN 'AW-1' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '14' DAY) AND (current_date - INTERVAL '8' DAY)) THEN 'BW-2' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '21' DAY) AND (current_date - INTERVAL '15' DAY)) THEN 'CW-3' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '28' DAY) AND (current_date - INTERVAL '22' DAY)) THEN 'DW-4' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '35' DAY) AND (current_date - INTERVAL '29' DAY)) THEN 'EW-5' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '42' DAY) AND (current_date - INTERVAL '36' DAY)) THEN 'FW-6' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '49' DAY) AND (current_date - INTERVAL '43' DAY)) THEN 'GW-7' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '56' DAY) AND (current_date - INTERVAL '50' DAY)) THEN 'HW-8' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '63' DAY) AND (current_date - INTERVAL '57' DAY)) THEN 'IW-9' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '70' DAY) AND (current_date - INTERVAL '64' DAY)) THEN 'JW-10' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '77' DAY) AND (current_date - INTERVAL '71' DAY)) THEN 'KW-11' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '84' DAY) AND (current_date - INTERVAL '78' DAY)) THEN 'LW-12' WHEN ("from_unixtime"(CTIME) BETWEEN (current_date - INTERVAL '91' DAY) AND (current_date - INTERVAL '85' DAY)) THEN 'MW-13' ELSE null END) date_week
, "count"(DISTINCT (CASE WHEN (("date_diff"('hour', create_time, "from_unixtime"(CTIME)) / DECIMAL '24.00') <= 5) THEN A.ORDERID ELSE null END)) Less_5
, "count"(DISTINCT (CASE WHEN ((("date_diff"('hour', create_time, "from_unixtime"(CTIME)) / DECIMAL '24.00') > 5) AND (("date_diff"('hour', create_time, "from_unixtime"(CTIME)) / DECIMAL '24.00') <= 10)) THEN A.ORDERID ELSE null END)) Between_5_10
, "count"(DISTINCT (CASE WHEN ((("date_diff"('hour', create_time, "from_unixtime"(CTIME)) / DECIMAL '24.00') > 10) AND (("date_diff"('hour', create_time, "from_unixtime"(CTIME)) / DECIMAL '24.00') < 15)) THEN A.ORDERID ELSE null END)) Between_10_15
, "count"(DISTINCT (CASE WHEN ((("date_diff"('hour', create_time, "from_unixtime"(CTIME)) / DECIMAL '24.00') > 15) AND (("date_diff"('hour', create_time, "from_unixtime"(CTIME)) / DECIMAL '24.00') <= 20)) THEN A.ORDERID ELSE null END)) Between_15_20
, "count"(DISTINCT (CASE WHEN (("date_diff"('hour', create_time, "from_unixtime"(CTIME)) / DECIMAL '24.00') > 20) THEN A.ORDERID ELSE null END)) Large_than_20
, "count"(A.ORDERID) all_non_receipt_rr
FROM
r A
inner join sip ON A.SHOPID= SIP.AFFI_SHOPID 
INNER JOIN (
SELECT DISTINCT
ORDER_ID orderid
, date(split(create_datetime,' ')[1]) grass_date
, from_unixtime(create_timestamp) create_time
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE is_cb_shop=1 and grass_region in ('SG','MY','ID','PH','TH','VN','BR') and date(split(create_datetime,' ')[1]) >= current_date-interval'120'day 
and grass_date>= current_date-interval'120'day 
) o ON (o.orderid = a.orderid)
WHERE (("date"("from_unixtime"(A.CTIME)) BETWEEN (current_date - INTERVAL '91' DAY) AND (current_date - INTERVAL '1' DAY)) AND (a.reason = 1)) AND A.reason in (1,2,3,103,105,107) and A.status in (2,5)
GROUP BY 1
ORDER BY 1 ASC

) 
SELECT
a.*
, TRY(((bk.Less_5 * DECIMAL '1.000000') / CAST(bk.all_non_receipt_rr AS bigint ))) Less_5
, TRY(((bk.Between_5_10 * DECIMAL '1.000000') / CAST(bk.all_non_receipt_rr AS bigint ))) Between_5_10
, TRY(((bk.Between_10_15 * DECIMAL '1.000000') / CAST(bk.all_non_receipt_rr AS bigint ))) Between_10_15
, TRY(((bk.Between_15_20 * DECIMAL '1.000000') / CAST(bk.all_non_receipt_rr AS bigint ))) Between_15_20
, TRY(((bk.Large_than_20 * DECIMAL '1.000000') / CAST(bk.all_non_receipt_rr AS bigint ))) Large_than_20


FROM
((
SELECT DISTINCT
(CASE WHEN (grass_date BETWEEN (current_date - INTERVAL '7' DAY) AND (current_date - INTERVAL '1' DAY)) THEN 'AW-1' WHEN (grass_date BETWEEN (current_date - INTERVAL '14' DAY) AND (current_date - INTERVAL '8' DAY)) THEN 'BW-2' WHEN (grass_date BETWEEN (current_date - INTERVAL '21' DAY) AND (current_date - INTERVAL '15' DAY)) THEN 'CW-3' WHEN (grass_date BETWEEN (current_date - INTERVAL '28' DAY) AND (current_date - INTERVAL '22' DAY)) THEN 'DW-4' WHEN (grass_date BETWEEN (current_date - INTERVAL '35' DAY) AND (current_date - INTERVAL '29' DAY)) THEN 'EW-5' WHEN (grass_date BETWEEN (current_date - INTERVAL '42' DAY) AND (current_date - INTERVAL '36' DAY)) THEN 'FW-6' WHEN (grass_date BETWEEN (current_date - INTERVAL '49' DAY) AND (current_date - INTERVAL '43' DAY)) THEN 'GW-7' WHEN (grass_date BETWEEN (current_date - INTERVAL '56' DAY) AND (current_date - INTERVAL '50' DAY)) THEN 'HW-8' WHEN (grass_date BETWEEN (current_date - INTERVAL '63' DAY) AND (current_date - INTERVAL '57' DAY)) THEN 'IW-9' WHEN (grass_date BETWEEN (current_date - INTERVAL '70' DAY) AND (current_date - INTERVAL '64' DAY)) THEN 'JW-10' WHEN (grass_date BETWEEN (current_date - INTERVAL '77' DAY) AND (current_date - INTERVAL '71' DAY)) THEN 'KW-11' WHEN (grass_date BETWEEN (current_date - INTERVAL '84' DAY) AND (current_date - INTERVAL '78' DAY)) THEN 'LW-12' WHEN (grass_date BETWEEN (current_date - INTERVAL '91' DAY) AND (current_date - INTERVAL '85' DAY)) THEN 'MW-13' ELSE null END) date_week
, "sum"((CASE WHEN (shop_type = 'SIP') THEN gross_order ELSE null END)) SIP_Gross_ORDER
, "sum"((CASE WHEN (shop_type = 'SIP') THEN gross_deliver_order ELSE null END)) SIP_GROSS_DELIVER_ORDER
, "sum"((CASE WHEN (shop_type = 'SIP') THEN non_receipt_rr ELSE null END)) "__SIP_Non-Recipt"
, "sum"((CASE WHEN (shop_type = 'SIP') THEN rr_request_excl_receipt ELSE null END)) __SIP_Request_RR
, TRY((("sum"((CASE WHEN (SHOP_TYPE = 'SIP') THEN non_receipt_rr ELSE null END)) * DECIMAL '1.000000') / "sum"((CASE WHEN (shop_type = 'SIP') THEN CAST(gross_order AS bigint) ELSE null END)))) SIP_Non_Receipt
, TRY((("sum"((CASE WHEN (SHOP_TYPE = 'SIP') THEN rr_request_excl_receipt ELSE null END)) * DECIMAL '1.000000') / "sum"((CASE WHEN (shop_type = 'SIP') THEN CAST(gross_deliver_order AS bigint ) ELSE null END)))) SIP_RR_Request
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (wrong_item_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN rr_request_excl_receipt ELSE null END)) AS bigint ))) SIP_Wrong_Item
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (item_damage_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN rr_request_excl_receipt ELSE null END)) AS bigint ))) SIP_Item_Damage
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (item_missing_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN rr_request_excl_receipt ELSE null END)) AS bigint ))) SIP_Item_Missing
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (fake_item_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN rr_request_excl_receipt ELSE null END)) AS bigint ))) SIP_Fake_Item
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (functional_damage_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN rr_request_excl_receipt ELSE null END)) AS bigint ))) SIP_Functional_Damage
, "sum"((CASE WHEN (shop_type = 'CB (NS)') THEN gross_order ELSE null END)) CB_Gross_ORDER
, "sum"((CASE WHEN (shop_type = 'CB (NS)') THEN gross_deliver_order ELSE null END)) CB_GROSS_DELIVER_ORDER
, "sum"((CASE WHEN (shop_type = 'CB (NS)') THEN non_receipt_rr ELSE null END)) "__CB_Non-Recipt"
, "sum"((CASE WHEN (shop_type = 'CB (NS)') THEN rr_request_excl_receipt ELSE null END)) __CB_Request_RR
, TRY((("sum"((CASE WHEN (SHOP_TYPE = 'CB (NS)') THEN non_receipt_rr ELSE null END)) * DECIMAL '1.000000') / "sum"((CASE WHEN (shop_type = 'CB (NS)') THEN CAST(gross_order AS bigint) ELSE null END)))) CB_Non_Receipt
, TRY((("sum"((CASE WHEN (SHOP_TYPE = 'CB (NS)') THEN rr_request_excl_receipt ELSE null END)) * DECIMAL '1.000000') / "sum"((CASE WHEN (shop_type = 'CB (NS)') THEN CAST(gross_deliver_order AS bigint) ELSE null END)))) CB_RR_Request
, TRY(("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (wrong_item_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN rr_request_excl_receipt ELSE null END)) AS bigint))) CB_Wrong_Item
, TRY(("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (item_damage_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN rr_request_excl_receipt ELSE null END)) AS bigint))) CB_Item_Damage
, TRY(("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (item_missing_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN rr_request_excl_receipt ELSE null END)) AS bigint))) CB_Item_Missing
, TRY(("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (fake_item_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN rr_request_excl_receipt ELSE null END)) AS bigint))) CB_Fake_Item
, TRY(("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (functional_damage_rr * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN rr_request_excl_receipt ELSE null END)) AS bigint))) CB_Functional_Damage
, "sum"((CASE WHEN (shop_type = 'SIP') THEN receipt_rr_app ELSE null END)) __SIP_EXCL_NON_RECEIPT_APPROVE
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN rr_request_excl_receipt ELSE null END)) AS bigint))) SIP_EXCL_NON_RECEIPT_APPROVE_
, "sum"((CASE WHEN (shop_type = 'SIP') THEN non_receipt_rr_app ELSE null END)) __SIP_NON_RECEIPT_APPROVE
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (non_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN non_receipt_rr ELSE null END)) AS bigint))) SIP_NON_RECEIPT_APPROVE_
, "sum"((CASE WHEN (shop_type = 'CB (NS)') THEN receipt_rr_app ELSE null END)) __CB_EXCL_NON_RECEIPT_APPROVE
, TRY(("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN rr_request_excl_receipt ELSE null END)) AS bigint))) CB_EXCL_NON_RECEIPT_APPROVE_
, "sum"((CASE WHEN (shop_type = 'CB (NS)') THEN non_receipt_rr_app ELSE null END)) __CB_NON_RECEIPT_APPROVE
, TRY(("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (non_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN non_receipt_rr ELSE null END)) AS bigint))) CB_NON_RECEIPT_APPROVE_
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (item_missing_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN gross_order ELSE null END)) AS bigint))) SIP_Item_Missing_app
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (wrong_item_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN gross_order ELSE null END)) AS bigint))) SIP_Wrong_Item_app
, TRY(("sum"((CASE WHEN (shop_type = 'SIP') THEN (functional_damage_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP') THEN gross_order ELSE null END)) AS bigint))) SIP_Functional_Damage_app
, ("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (item_missing_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN gross_order ELSE null END)) AS decimal(10,2))) CB_Item_Missing_app
, ("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (wrong_item_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN gross_order ELSE null END)) AS decimal(10,2))) CB_Wrong_Item_app
, ("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN (functional_damage_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'CB (NS)') THEN gross_order ELSE null END)) AS decimal(10,2))) CB_Functional_Damage_app
, ("sum"((CASE WHEN (shop_type = 'SIP_P') THEN (item_missing_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP_P') THEN gross_order ELSE null END)) AS decimal(10,2))) SIP_P_Item_Missing_app
, ("sum"((CASE WHEN (shop_type = 'SIP_P') THEN (wrong_item_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP_P') THEN gross_order ELSE null END)) AS decimal(10,2))) SIP_P_Wrong_Item_app
, ("sum"((CASE WHEN (shop_type = 'SIP_P') THEN (functional_damage_receipt_rr_app * DECIMAL '1.000000') ELSE null END)) / CAST("sum"((CASE WHEN (shop_type = 'SIP_P') THEN gross_order ELSE null END)) AS decimal(10,2))) SIP_P_Functional_Damage_app

, TRY((("sum"((CASE WHEN (SHOP_TYPE = 'SIP_P') THEN non_receipt_rr ELSE null END)) * DECIMAL '1.000000') / "sum"((CASE WHEN (shop_type = 'SIP_P') THEN CAST(gross_order AS bigint) ELSE null END)))) SIP_P_Non_Receipt
, TRY((("sum"((CASE WHEN (SHOP_TYPE = 'SIP_P') THEN rr_request_excl_receipt ELSE null END)) * DECIMAL '1.000000') / "sum"((CASE WHEN (shop_type = 'SIP_P') THEN CAST(gross_deliver_order AS bigint) ELSE null END)))) SIP_P_RR_Request 

FROM
(
SELECT DISTINCT
a.grass_region
, (CASE WHEN (a.shopid IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (MST_COUNTRY = 'TW')
))

THEN 'SIP'
when a.shopid in (select distinct mst_shopid 
from sip )
then 'SIP_P'




WHEN (NOT (A.SHOPID IN (SELECT DISTINCT AFFI_SHOPID
FROM
SIP
))) THEN 'CB (NS)'

END) shop_type
, a.grass_date
, "count"(DISTINCT (CASE WHEN ((payment_method_id <> 6) AND (pay_time IS NOT NULL)) THEN a.orderid WHEN ((payment_method_id = 6) AND ((shipping_confirm_time IS NOT NULL) )) THEN a.orderid ELSE null END)) gross_order
, "count"(DISTINCT (CASE WHEN (logistics_status IN (5, 6, 10)) THEN a.orderid ELSE null END)) gross_deliver_order
, "count"(DISTINCT (CASE WHEN (status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15)) THEN a.orderid ELSE null END)) net_order
, "count"(DISTINCT (CASE WHEN (r.reason IN (2, 3, 103, 105, 107)) THEN r.orderid ELSE null END)) rr_request_excl_receipt
, "count"(DISTINCT (CASE WHEN ((status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15)) AND (logistics_status IN (5, 6, 10))) THEN a.orderid ELSE null END)) net_deliver_orders
, "count"(DISTINCT (CASE WHEN (r.orderid IS NOT NULL) THEN r.orderid ELSE null END)) rr_orders
, "count"(DISTINCT (CASE WHEN (r.status IN (2, 5)) THEN r.orderid ELSE null END)) rr_approve_orders
, "count"(DISTINCT (CASE WHEN (r.reason = 1) THEN r.orderid ELSE null END)) non_receipt_rr
, "count"(DISTINCT (CASE WHEN (r.reason = 2) THEN r.orderid ELSE null END)) wrong_item_rr
, "count"(DISTINCT (CASE WHEN (r.reason = 3) THEN r.orderid ELSE null END)) item_damage_rr
, "count"(DISTINCT (CASE WHEN (r.reason = 103) THEN r.orderid ELSE null END)) item_missing_rr
, "count"(DISTINCT (CASE WHEN (r.reason = 105) THEN r.orderid ELSE null END)) fake_item_rr
, "count"(DISTINCT (CASE WHEN (r.reason = 107) THEN r.orderid ELSE null END)) functional_damage_rr
, "count"(DISTINCT (CASE WHEN ((r.reason = 1) AND (r.status IN (2, 5))) THEN r.orderid ELSE null END)) non_receipt_rr_app
, "count"(DISTINCT (CASE WHEN ((r.reason =2) AND (r.status IN (2, 5))) THEN r.orderid ELSE null END)) wrong_item_receipt_rr_app
, "count"(DISTINCT (CASE WHEN ((r.reason =103) AND (r.status IN (2, 5))) THEN r.orderid ELSE null END)) item_missing_receipt_rr_app
, "count"(DISTINCT (CASE WHEN ((r.reason =107) AND (r.status IN (2, 5))) THEN r.orderid ELSE null END)) functional_damage_receipt_rr_app
, "count"(DISTINCT (CASE WHEN ((r.reason IN (2, 3, 103, 105, 107)) AND (r.status IN (2, 5))) THEN r.orderid ELSE null END)) receipt_rr_app


FROM
(((
SELECT DISTINCT ORDER_ID ORDERID 
, shop_id SHOPID 
,date(split(create_datetime,' ')[1])as grass_date
, grass_region
, payment_method_id
, shipping_confirm_timestamp shipping_confirm_time
, pay_timestamp pay_time
, logistics_status_id logistics_status
, order_be_status_id STATUS_EXT
FROM 

mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE is_cb_shop=1 and grass_region in ('SG','MY','ID','PH','TH','VN','BR') and date(split(create_datetime,' ')[1]) >= current_date-interval'120'day 
and grass_date>= current_date-interval'120'day ) a
INNER JOIN (
SELECT DISTINCT child_shopid
FROM
cb
) cb ON (cb.child_shopid = a.shopid))
LEFT JOIN (
SELECT DISTINCT
reason
, status
, orderid
, "date"("from_unixtime"(ctime)) ctime
FROM
r 
) r ON (r.orderid = a.orderid))
WHERE ( (a.grass_date BETWEEN (current_date - INTERVAL '91' DAY) AND (current_date - INTERVAL '1' DAY)))
GROUP BY 1, 2, 3
) 
GROUP BY 1
) a
LEFT JOIN break bk ON (bk.date_week = a.date_week))
ORDER BY 1 ASC