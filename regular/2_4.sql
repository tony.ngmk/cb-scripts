WITH shop_balance_info AS (
SELECT
seller_userid
,user_name
,shop_id
,shop_status
,round(shop_balance__local_currency, 2) AS shop_balance__local_currency
,round(shop_balance__usd, 2) AS shop_balance__usd
,CAST(l30d_order_cnt as integer) AS l30d_order_cnt
,round(l30d_gmv__usd, 2) AS l30d_gmv__usd
,grass_date
,grass_region
FROM ${SCHEMA}.bi__cbsip_negative_balance_shop__reg__daily
WHERE
grass_date = CAST('${BIZ_TIME}' as date) - interval '1' day
--AND shop_status = 1
AND grass_region = 'SG'
)

SELECT distinct
a.grass_region
,a.shop_id
,a.shop_status
,a.shop_balance__local_currency
,a.shop_balance__usd
,a.l30d_order_cnt
,a.l30d_gmv__usd
,a.seller_userid
,a.user_name
,b.merchant_name
,b.merchant_owner_email

FROM shop_balance_info AS a
LEFT JOIN (
SELECT distinct
shop_id
,merchant_name
,merchant_owner_email
FROM cncbbi_general.shopee_cb_seller_profile
WHERE
grass_region = 'SG'
) AS b
ON a.shop_id = b.shop_id
ORDER BY a.shop_balance__usd, b.merchant_owner_email, b.merchant_name