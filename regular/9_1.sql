with
s as 
(
select DISTINCT affi_shopid shop_id
,cb_option
,b.country AS mst_country
,grass_region
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
join marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b
on a.mst_shopid = b.shopid
where grass_region <> ''
and cb_option = 0
)
select distinct 
grass_date
,cb_option
,mst_country
,s.grass_region
,SUM(deduction_amt) as deduction_amt
,SUM(deduction_amt_usd) as deduction_amt_usd
from (
select *
from mp_paidads.dwd_advertiser_deduction_di__reg_s0_live
where grass_date >= CURRENT_DATE - INTERVAL '91' DAY
and credit_topup_type_name
like '%paid%' AND tz_type = 'local'
) ads
join s
on ads.shop_id = s.shop_id
group by 1, 2, 3, 4
