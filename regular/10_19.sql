WITH
sip AS (
SELECT DISTINCT
a.affi_shopid
, affi_itemid
, affi_country
, mst_shopid
, mst_itemid
, b.country mst_country
, b.cb_option
FROM
(select distinct 
affi_itemid 
, affi_shopid 
, mst_shopid 
, affi_country
, mst_itemid
from 
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
where AFFI_COUNTRY IN 
('BR','MX','CO','CL','PL','ES')
) a
INNER JOIN(select distinct shopid 
, cb_option
, country 
from
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
where cb_option=1 and country in ('TW','MY','BR')
) b ON (a.mst_shopid = b.shopid)
join (select distinct affi_shopid 

from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where grass_region in ('BR','MX','CO','CL','PL','ES') and sip_shop_status<>3

) c on c.affi_shopid = a.affi_shopid 


) ,

sku AS (
SELECT DISTINCT grass_date
, a.shop_id shopid 
, item_id itemid
, status item_status
, stock item_stock
, is_cb_shop
, seller_status
, shop_status
, is_holiday_mode
, grass_region
FROM
mp_item.dim_item__reg_s0_live a inner JOIN 
( SELECT distinct affi_shopid shop_id from sip
union all
SELECT distinct mst_shopid shop_id from sip
) b on a.shop_id=b.shop_id
WHERE grass_date = CURRENT_DATE - INTERVAL '1' DAY AND is_cb_shop=1 AND grass_region IN ('BR','MX','CO','CL','PL','ES','TW','MY')
and shop_status= 1 
and seller_status= 1
and tz_type='local'
) 
, cb as ( 
select distinct shop_id as child_shopid
, (CASE WHEN LOWER(seller_type) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(seller_type) LIKE '%st%' THEN 'Short Tail'
WHEN LOWER(seller_type) LIKE '%mt%' THEN 'Mid Tail'
WHEN LOWER(seller_type) LIKE '%lt%' THEN 'Long Tail'
ELSE 'Others' END) AS seller_type
, merchant_owner_email as gp_account_owner
, merchant_name as gp_account_name
from dev_regcbbi_general.cb_seller_profile_reg
where (gp_account_billing_country = 'China' OR gp_account_billing_country is NULL)
and (grass_region IN ('TW','MY','BR'))
)
, affi AS (
SELECT DISTINCT
affi_country
, affi_itemid
, item_status
, item_stock
, mst_itemid
, sku.grass_date
, cb_option
, case when (((seller_status = 1) AND (shop_status = 1)) AND ((is_holiday_mode = 0) OR (is_holiday_mode IS NULL))) then 'Y'
else 'N' end as shop_status 
FROM
sip
JOIN sku ON (sip.affi_itemid = sku.itemid) 
) 

, mst AS (
SELECT DISTINCT
itemid mst_itemid
, item_status
, item_stock
, grass_region mst_country
, sku.grass_date
, shopid 
FROM
sku 
where shop_status=1 and ((item_status = 1) AND (item_stock > 0)) AND grass_region IN ('TW','MY','BR')
) 

SELECT DISTINCT mst_country
, affi.affi_country
, affi.grass_date AS grass_date
, cb.seller_type 
, count (DISTINCT mst.mst_itemid ) overall_psku
, count (DISTINCT case when shop_status = 'Y' then mst.mst_itemid else null end ) ashop_live_psku
, count (DISTINCT case when shop_status = 'Y' then affi_itemid else null end ) asku
, COUNT(DISTINCT CASE WHEN SHOP_STATUS='Y' AND AFFI.AFFI_ITEMID = 0 THEN MST.MST_ITEMID ELSE NULL END ) SYNC_failed
, count (DISTINCT case when shop_status = 'N' then mst.mst_itemid else null end ) unlive_psku
, count (DISTINCT case when shop_status = 'Y' and affi.item_status = 1 and affi.item_stock > 0 then affi_itemid else null end ) live_sku
FROM
mst
JOIN affi ON (affi.mst_itemid = mst.mst_itemid) 
join cb on cb.child_shopid =mst.shopid 

GROUP BY 1, 2, 3, 4