with
sip as 
(
select grass_region
,affi_shopid
,mst_shopid
,b.country mst_country
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
inner join marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b
on a.mst_shopid = b.shopid
where b.cb_option = 0
and a.grass_region <> ''
)
,fees as 
(
select DISTINCT orderid
,cc_fee
,commission_fee
,service_fee
from regcbbi_others.shopee_bi_keyreports_local_sip_revenue_v3
where grass_date >= current_date - INTERVAL '100' DAY
)
,wh as 
(
select DISTINCT *
from regcbbi_others.logistics_basic_info
where wh_outbound_date >= CURRENT_DATE - INTERVAL '90' DAY
)
,o as 
(
select date(from_unixtime(create_timestamp)) create_timestamp
,DATE(CAST(create_datetime AS TIMESTAMP)) AS create_date
,order_id
,order_sn
,shop_id
,gmv
,buyer_paid_shipping_fee
,pv_rebate_by_shopee_amt
,sv_rebate_by_shopee_amt
,pv_coin_earn_by_shopee_amt
,sv_coin_earn_by_shopee_amt
,sv_rebate_by_seller_amt
,pv_rebate_by_seller_amt
,sv_coin_earn_by_seller_amt
,pv_coin_earn_by_seller_amt
,coin_used_cash_amt
,card_rebate_by_bank_amt
,IF((actual_shipping_rebate_by_shopee_amt = 0), estimate_shipping_rebate_by_shopee_amt, actual_shipping_rebate_by_shopee_amt) shopee_shipping_rebate
,actual_shipping_fee
,logistics_status_id
,payment_method_id
,order_be_status_id
,sv_promotion_id
,card_rebate_by_shopee_amt
,is_cb_shop
,grass_region
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_date >= current_date - INTERVAL '100' DAY
and tz_type = 'local'
and grass_region NOT IN ('IN', 'AR', 'FR')
and date(from_unixtime(create_timestamp)) >= DATE('2021-12-03')
)
,ai as 
(
select DISTINCT order_id
,order_sn
,is_cb_shop
,o.grass_region
,sum(item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
,sum(item_tax_amt) item_tax_amt
,sum(commission_fee) commission_fee
,sum(buyer_txn_fee) buyer_txn_fee
,sum(seller_txn_fee) seller_txn_fee
,sum(service_fee) service_fee
,SUM(affi_real_weight*item_amount) AS affi_real_weight
,SUM(CASE WHEN affi_real_weight > 0 THEN item_amount END) AS item_amount_w_real_weight
,SUM(item_weight_pp*1000*item_amount) AS seller_input_weight
,sum(item_amount*COALESCE(sum_hpfn, 0)) AS hpfn -- a currency
,sum(item_amount*COALESCE(initial_hp, 0)) AS initial_price -- a currency
,SUM(item_amount) item_amount
from mp_order.dwd_order_item_all_ent_df__reg_s0_live o
join sip
on o.shop_id = sip.affi_shopid
left join (
select DISTINCT item_id affi_itemid
,primary_item_id mst_itemid
,shop_id affi_shopid
,primary_shop_id mst_shopid
,COALESCE(item_real_weight / 100.00, 0) AS affi_real_weight
from mp_item.ads_sip_affi_item_profile__reg_s0_live
where primary_shop_type = 0
and tz_type = 'local'
and grass_date = current_date - INTERVAL '2' DAY
) si
on o.item_id = si.affi_itemid
left join (
select DISTINCT mst_country
,affi_country
,TRY_CAST(weight AS DOUBLE) weight
,TRY_CAST(hpfn AS DOUBLE) hpfn
,TRY_CAST(initial_hp AS DOUBLE) initial_hp
,TRY_CAST(sum_hpfn AS DOUBLE) sum_hpfn
,TRY_CAST(fx AS DOUBLE) fx
,TRY_CAST(country_margin AS DOUBLE) country_margin
from regcbbi_others.local_sip_price_config a
join (
select MAX(ingestion_timestamp) ingestion_timestamp
from regcbbi_others.local_sip_price_config
) b
on a.ingestion_timestamp = b.ingestion_timestamp
) cfg
on sip.grass_region = cfg.affi_country
and sip.mst_country = cfg.mst_country
and CASE WHEN (affi_real_weight = 0 OR affi_real_weight IS NULL) THEN IF(item_weight_pp*1000 BETWEEN 1 AND 10, 1, FLOOR(IF(item_weight_pp*1000 >= 20000, 20000, item_weight_pp*1000)/10.0)*10) = cfg.weight
ELSE IF(affi_real_weight BETWEEN 1 AND 10, 1, FLOOR(IF(affi_real_weight >= 20000, 20000, affi_real_weight)/10.0)*10) = cfg.weight END
where o.grass_region NOT IN ('IN', 'AR', 'FR')
and grass_date >= current_date - INTERVAL '100' DAY
and tz_type = 'local'
and is_cb_shop = 1
and date(from_unixtime(create_timestamp)) >= DATE('2021-12-03')
group by 1, 2, 3, 4
)
,pi as 
(
select DISTINCT order_id
,order_sn
,is_cb_shop
,o.grass_region
,sum(item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
,sum(item_tax_amt) item_tax_amt
,sum(commission_fee) commission_fee
,sum(buyer_txn_fee) buyer_txn_fee
,sum(seller_txn_fee) seller_txn_fee
,sum(service_fee) service_fee
,SUM(item_amount) item_amount
from mp_order.dwd_order_item_all_ent_df__reg_s0_live o
join (
select DISTINCT mst_shopid
from sip
) sip
on o.shop_id = sip.mst_shopid
where o.grass_region IN ('ID', 'MY', 'TW', 'VN', 'TH')
and grass_date >= current_date - INTERVAL '100' DAY
and tz_type = 'local'
and is_cb_shop = 0
and date(from_unixtime(create_timestamp)) >= DATE('2021-12-03')
group by 1, 2, 3, 4
)
,rr as 
(
select orderid
,(refund_amount / DECIMAL '100000.00') refund_amount
from marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
where grass_region NOT IN ('IN', 'AR', 'FR')
and date(from_unixtime(ctime)) >= current_date - INTERVAL '100' DAY
and refund_amount > 0
and status IN (2, 5)
)
,ssv as 
(
select distinct perc_ssv/100.00 AS perc_ssv
,promo_id
from regcbbi_others.sip_local_pnl_ssv
)
,rbt as 
(
select DISTINCT overseas_itemid
,overseas_modelid
,seller_negotiated_price_local_currency
,start_date_time
,end_date_time
from regcbbi_others.sip_local_pnl_item_rebate
where overseas_itemid > 0
and seller_negotiated_price_local_currency > 0
)
,rbt2 as 
(
select DISTINCT orderid
,sum(rebates) rebate
from (
select orderid
,itemid
,modelid
,((seller_negotiated_price_local_currency * amount) - price * amount) rebates
from (
select order_id orderid
,item_id itemid
,model_id modelid
,item_amount amount
,item_price_pp price
,(from_unixtime(create_timestamp)) create_time
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where grass_region IN ('SG', 'MY')
and grass_date >= current_date - INTERVAL '100' DAY
and is_cb_shop = 1
and date(from_unixtime(create_timestamp)) >= DATE('2021-12-03')
and tz_type = 'local'
) p_oi
inner join rbt
on rbt.overseas_modelid = p_oi.modelid
and rbt.overseas_itemid = p_oi.itemid
and p_oi.create_time BETWEEN start_date_time
and end_date_time
)
group by 1
)
,offline_seller as 
(
select DISTINCT TRY_CAST(mst_order_id AS BIGINT) mst_order_id
,TRY_CAST(mst_escrow_amt AS DOUBLE) mst_escrow_amt
,TRY_CAST(mst_deduction_amt AS DOUBLE) mst_deduction_amt
from regcbbi_others.sip__localsip_idsip_offline_adj_order_lvl__wf__reg__s0
where ingestion_timestamp = (SELECT MAX(ingestion_timestamp) it
FROM regcbbi_others.sip__localsip_idsip_offline_adj_order_lvl__wf__reg__s0)
)
select distinct mst_country
,sip.grass_region
,wh_outbound_date
,COUNT(distinct a.order_id) as orders
,sum(a.gmv) gmv
,sum(a.buyer_paid_shipping_fee) buyer_paid_shipping_fee
,sum(ai.item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
,sum(a.pv_rebate_by_shopee_amt) pv_rebate_by_shopee_amt
,sum(a.coin_used_cash_amt) coin_used_cash_amt
,sum(a.card_rebate_by_bank_amt) card_rebate_by_bank_amt
,sum(a.card_rebate_by_shopee_amt) card_rebate_by_shopee_amt
,sum(a.sv_rebate_by_seller_amt) sv_rebate_by_seller_amt
,sum(a.sv_coin_earn_by_seller_amt) sv_coin_earn_by_seller_amt
,sum(ai.item_tax_amt) item_tax_amt
,sum(a.shopee_shipping_rebate) shopee_shipping_rebate
,sum(wh.actual_shipping_fee) asf_sls
,sum(fees.commission_fee) commission_fee
,sum(fees.cc_fee) seller_txn_fee
,sum(fees.service_fee) service_fee
,sum(if(rr.refund_amount > 0, rr.refund_amount, 0)) refund_amount
,sum((case when ((a.logistics_status_id = 6) and (a.payment_method_id = 6)) then a.gmv + ai.item_rebate_by_shopee_amt + a.pv_rebate_by_shopee_amt + a.coin_used_cash_amt + a.card_rebate_by_bank_amt + a.card_rebate_by_bank_amt - a.sv_coin_earn_by_seller_amt - a.pv_coin_earn_by_seller_amt - ai.item_tax_amt + a.shopee_shipping_rebate + fees.commission_fee + fees.cc_fee + fees.service_fee else 0 end)) cod_fail
,sum((case when (ssv.promo_id is not null) then ((perc_ssv) * a.sv_rebate_by_seller_amt) else 0 end)) ssv_rebate
,sum(p.gmv) gmv_p
,sum(p.buyer_paid_shipping_fee) buyer_paid_shipping_fee_p
,sum(pi.commission_fee) commission_fee_p
,sum(pi.buyer_txn_fee) buyer_txn_fee_p
,sum(pi.seller_txn_fee) seller_txn_fee_p
,sum(pi.service_fee) service_fee_p
,sum(pi.item_rebate_by_shopee_amt) item_rebate_by_shopee_amt_p
,sum(p.pv_rebate_by_shopee_amt) pv_rebate_by_shopee_amt_p
,sum(p.shopee_shipping_rebate) shopee_shipping_rebate_p
,sum(p.sv_coin_earn_by_seller_amt) sv_coin_earn_by_seller_p
,sum(p.coin_used_cash_amt) coin_used_cash_amt_p
,sum(COALESCE(ai.hpfn, 0)) hpfn
,sum(rebate) rebate
,SUM(ai.initial_price) as initial_price
,SUM(a.pv_coin_earn_by_seller_amt) as pv_coin_earn_by_seller_amt
,SUM(a.pv_rebate_by_seller_amt) as pv_rebate_by_seller_amt
,SUM(a.sv_rebate_by_shopee_amt) as sv_rebate_by_shopee_amt
,SUM(a.pv_coin_earn_by_shopee_amt) as pv_coin_earn_by_shopee_amt
,SUM(a.sv_coin_earn_by_shopee_amt) as sv_coin_earn_by_shopee_amt
,SUM(ai.item_amount) item_amount
,SUM(offline_seller.mst_deduction_amt) as rebate_seller
from (
select *
from o
where is_cb_shop = 1
and grass_region IN ('SG', 'MY', 'TW', 'ID', 'PH', 'TH', 'VN', 'BR', 'MX', 'CO', 'CL')
) a
inner join ai
on a.order_id = ai.order_id
inner join sip
on a.shop_id = sip.affi_shopid
inner join (
select DISTINCT a_site
,p_site
,TRY_CAST(live_date AS DATE) live_date
,remark
from regcbbi_others.sip__localsip_lane_live_date__df__reg__s0
) live_date
on a.grass_region = live_date.a_site
and sip.mst_country = live_date.p_site
and a.create_date >= live_date.live_date -- filter out test orders
inner join (
select oversea_orderid
,oversea_shopid
,oversea_country
,local_orderid
,local_shopid
,local_country
from marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
where DATE(from_unixtime(ctime)) >= current_date - INTERVAL '100' DAY
and oversea_country <> 'id'
) m
on a.order_id = m.oversea_orderid
inner join (
select *
from o
where order_be_status_id NOT IN (1, 6, 8, 5, 7, 8, 16)
and is_cb_shop = 0
and grass_region IN ('ID', 'MY', 'TW', 'VN', 'TH')
) p
on m.local_orderid = p.order_id
inner join wh
on a.order_sn = wh.ordersn
left join pi
on p.order_id = pi.order_id
left join fees
on a.order_id = fees.orderid
left join rr
on a.order_id = rr.orderid
left join ssv
on a.sv_promotion_id = ssv.promo_id
left join rbt2
on a.order_id = rbt2.orderid
left join offline_seller
on p.order_id = offline_seller.mst_order_id
-- WHERE mst_country in ('ID')
-- AND sip.grass_region IN ('BR','CO')
-- AND wh_outbound_date between DATE('2022-03-31') AND DATE ('2022-04-05')
group by 1, 2, 3

