WITH shop as
(
SELECT shop_id, merchant_id, merchant_region
FROM marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live
)

, user as 
(
SELECT grass_region 
, shop_id 
, user_id
, user_name
, date(from_unixtime(registration_timestamp)) as registration_date 
FROM mp_user.dim_user__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
and is_cb_shop = 1

UNION ALL 

SELECT grass_region --FR is not in user mart after 16 Jun, taking last day's snapshot for payment's negative balance calculation
, shop_id 
, user_id
, user_name
, date(from_unixtime(registration_timestamp)) as registration_date 
FROM mp_user.dim_user__reg_s0_live 
WHERE grass_date = DATE'2022-06-16' 
and tz_type = 'local'
and is_cb_shop = 1
and grass_region = 'FR'
) 

, merchant as 
(
SELECT merchant_id
, merchant_name
, salesforce_id
, merchant_type
FROM marketplace.shopee_seller_management_db__merchant_tab__reg_daily_s0_live 
)

, seller_ggp as 
(
SELECT a.merchant_id 
, a.seller_id 
, b.seller_name 
, b.item_owner as seller_owner_email
FROM marketplace.shopee_seller_management_db__ggp_merchant_tab__reg_daily_s0_live a
LEFT JOIN marketplace.shopee_seller_management_db__seller_ggp_tab__reg_daily_s0_live b on a.seller_id = b.seller_id
)

, owner as 
(
SELECT distinct user_id as owner_id, user_email as owner_email
FROM marketplace.shopee_seller_bd_center_db__user_tab__reg_daily_s0_live
) 


, shop_owner as 
(
SELECT distinct a.shop_id 
, a.organization_id 
, a.owner_id
, o.owner_email as shop_owner_email
FROM
(
SELECT distinct item_id as shop_id 
, organization_id 
, owner_id
, mtime 
, rank() over (partition by item_id order by mtime desc) as owner_rank
FROM marketplace.shopee_seller_bd_center_db__organization_shop_tab__reg_daily_s0_live
WHERE owner_type = 1 
AND state = 1
) a
LEFT JOIN owner o on a.owner_id = o.owner_id
WHERE owner_rank = 1
)

, gp_owner as 
(
SELECT distinct a.item_id as merchant_id 
, a.organization_id 
, a.owner_id
, b.region as merchant_department_region
, o.owner_email as merchant_owner_email
FROM 
(SELECT item_id 
, organization_id 
, owner_id 
, mtime 
, rank() over (partition by item_id order by mtime desc) as owner_rank 
FROM marketplace.shopee_seller_bd_center_db__organization_gp_tab__reg_daily_s0_live
WHERE owner_type = 1
AND state = 1
) a
LEFT JOIN marketplace.shopee_seller_bd_center_db__organization_tab__reg_daily_s0_live b on a.organization_id = b.organization_id
LEFT JOIN owner o on a.owner_id = o.owner_id
WHERE owner_rank = 1
)

, sip as
(
SELECT a.affi_shopid
, a.mst_shopid 
, m.mst_country
, m.cb_option 
FROM 
(
SELECT cast(affi_shopid AS BIGINT) AS affi_shopid
, cast(mst_shopid AS BIGINT) AS mst_shopid
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status <> 3
) a
LEFT JOIN
(
SELECT cast(shopid AS BIGINT) AS mst_shopid
, country AS mst_country
, cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) m
ON a.mst_shopid = m.mst_shopid
)

SELECT distinct 
user.user_id
, shop.shop_id
, user.user_name

, shop_owner.shop_owner_email
, cn.shop_organization_name --child_account_owner_userrole_name 


, shop.merchant_id -- gp_account_id 
, merchant.merchant_name -- gp account name
, shop.merchant_region --gp account billing country
, gp_owner.merchant_owner_email
, cn.merchant_organization_name -- gp_account_owner_userrole_name
, merchant.merchant_type -- gp_account_seller_classification
, gp_owner.merchant_department_region

, seller_ggp.seller_id -- ggp_account_id 
, seller_ggp.seller_name -- ggp account name 
, seller_ggp.seller_owner_email
, cn.seller_organization_name -- ggp_account_userrole_name

, coalesce(cn.cb_group, kr.gp_smt) as cb_group 
, cn.rm_tl as rm_tl 

, CASE WHEN sip.affi_shopid is NOT NULL AND sip.cb_option = 0 then 'local SIP'
WHEN sip.affi_shopid is NOT NULL AND shop.merchant_region = 'CN' then 'CN SIP'
WHEN sip.affi_shopid is NOT NULL AND shop.merchant_region = 'KR' then 'KR SIP'
WHEN shop.merchant_region in ('HK', 'CN') AND coalesce(gp_owner.merchant_department_region, shop.merchant_region) = 'HK' then 'HKCB'
WHEN shop.merchant_region = 'KR' then 'KRCB'
WHEN shop.merchant_region = 'JP' then 'JPCB'
WHEN shop.merchant_region in ('HK', 'CN') AND (cn.shop_organization_name like '%BD_%' or cn.shop_organization_name like '%ICB_%') then 'CNCB BD'
WHEN shop.merchant_region in ('HK', 'CN') AND cn.shop_organization_name like '%UST%' then 'CNCB UST'
WHEN shop.merchant_region in ('HK', 'CN') AND cn.shop_organization_name like '%ST_%' then 'CNCB ST'
WHEN shop.merchant_region in ('HK', 'CN') AND cn.shop_organization_name like '%MT_%' then 'CNCB MT'
WHEN shop.merchant_region in ('HK', 'CN') AND cn.shop_organization_name like '%LT%' then 'CNCB LT'
WHEN shop.merchant_region in ('HK', 'CN') then 'CNCB Others'
ELSE 'Others'
END AS seller_type 

, user.grass_region

FROM shop 
JOIN user on shop.shop_id = user.shop_id 
LEFT JOIN merchant on shop.merchant_id = merchant.merchant_id 
LEFT JOIN seller_ggp on shop.merchant_id = seller_ggp.merchant_id 
LEFT JOIN shop_owner on shop.shop_id = shop_owner.shop_id 
LEFT JOIN gp_owner on gp_owner.merchant_id = shop.merchant_id 
LEFT JOIN 
(SELECT distinct shop_id, seller_organization_name, merchant_organization_name, shop_organization_name, cb_group, rm_tl
FROM cncbbi_general.shopee_cb_seller_profile
) cn on shop.shop_id = cn.shop_id
LEFT JOIN 
(SELECT distinct shop_id, gp_smt FROM dev_regcbbi_kr.krcb_shop_profile 
WHERE grass_date = (select max(grass_date) from dev_regcbbi_kr.krcb_shop_profile)
) kr on shop.shop_id = kr.shop_id 
LEFT JOIN sip on shop.shop_id = sip.affi_shopid