WITH sip as 
(
select distinct cast(affi.affi_shopid as bigint) as affi_shopid
, affi.mst_shopid
, mst.country as mst_country
, affi.country as affi_country
, mst.cb_option
from
(
select distinct cast(shopid as bigint) as shopid
, country
, cb_option
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) mst
join 
(
select distinct cast(affi_shopid as bigint) as affi_shopid
, affi_username
, cast(mst_shopid as bigint) as mst_shopid
, country
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status != 3 
) affi
on mst.shopid = affi.mst_shopid
)

select cb_option
, mst_country
, affi_country
, wh_outbound_date
, case when lead_time <= 4 then '0_4'
when lead_time <= 20 then cast(cast(ceiling(lead_time) as int) as varchar)
when lead_time <= 25 then '20_25'
when lead_time <= 30 then '25_30'
else '30_'
end as lead_time
, count(distinct orderid) as orders
, sum(gmv_usd) as gmv_usd
, count(distinct if(is_cod_failed = 1, orderid, null)) as cod_failed_order
, sum(if(is_cod_failed = 1, gmv_usd, 0)) as cod_failed_gmv_usd
from 
(
select distinct sip.cb_option
, sip.mst_country
, sip.affi_country
, DATE(from_unixtime(sls.wh_outbound_time)) as wh_outbound_date
, sls.orderid
, if(sls.first_delivery_attempt_time > sls.create_time, cast(sls.first_delivery_attempt_time - sls.create_time as double)/86400, null) as lead_time 
, o.gmv_usd
, if(sls.delivery_failed_time > sls.pickup_done_time, 1, 0) as is_cod_failed
from
(
select orderid
, shopid
, create_time
, pickup_done_time
, delivery_failed_time
, min(first_delivery_attempt_time) as first_delivery_attempt_time
, min(wh_outbound_time) as wh_outbound_time
from regcbbi_others.cb_logistics_mart_test
where payment_method = 6
and order_placed_local_date between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '139' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '29' day
group by 1, 2, 3, 4, 5
) sls
join
(
select distinct order_id
, gmv_usd
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_date >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '139' day
and tz_type = 'local'
and is_cb_shop = 1
) o 
on sls.orderid = o.order_id
join sip
on sls.shopid = sip.affi_shopid
where DATE(from_unixtime(sls.wh_outbound_time)) between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '89' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '29' day
and sls.first_delivery_attempt_time is not null
and sls.pickup_done_time is not null
)
group by 1, 2, 3, 4, 5