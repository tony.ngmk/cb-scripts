WITH
o AS ( 
SELECT DISTINCT
o2.orderid
, o1.shop_id SHOPID
, o1.create_time
, o1.cancel_time
, o2.cancel_reason
, o1.status_ext
, o1.complete_time
, o1.grass_date
FROM
((
SELECT DISTINCT
order_id
, shop_id
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) create_time
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) cancel_time
, order_be_status_id status_ext
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) complete_time
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) grass_date
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ((is_cb_shop = 0) AND (tz_type = 'local') AND date(from_unixtime(create_timestamp)) >= current_date - interval '120' day AND grass_region = 'MY' AND grass_date >= current_date - interval '120' day)
) o1
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'MY') AND date(from_unixtime(create_time)) >= current_date - interval '120' day)
) o2 ON (o1.order_id = o2.orderid))
) 

, r AS (
SELECT DISTINCT
orderid
, shopid
, reason
, status
, "from_unixtime"(ctime) ctime
, "from_unixtime"(mtime) mtime
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'MY') AND date("from_unixtime"(ctime)) >= current_date - interval '90' day)
)

, shop AS (
SELECT DISTINCT
shop_id shopid 
, shop_level1_global_be_category main_category
, shop_level2_global_be_category sub_category
, active_item_with_stock_cnt
, user_name username
, is_preferred_shop
, is_official_shop
, response_rate
, grass_region
, user_id
FROM 
regcbbi_others.shop_profile
WHERE status = 1 AND user_status = 1 AND is_holiday_mode = 0 AND grass_date = current_date - INTERVAL '1' DAY AND grass_region = 'MY' AND is_cb_shop = 0 AND tz_type = 'local'
)

, ex AS (
SELECT DISTINCT CAST(mst_shopid AS bigint) shopid 
FROM regcbbi_others.shopee_regional_cb_team__my_localsip_tos_shoplist
UNION ALL 
SELECT DISTINCT CAST(shopid AS bigint) shopid 
FROM regcbbi_others.sip__mysip_migration_maintain_shop_list__tmp__reg_s0
UNION ALL 
SELECT DISTINCT CAST(shopid AS bigint) shopid 
FROM regcbbi_others.shopee_regional_cb_team__mysip_migration_maintain_shop_list2
UNION ALL 
SELECT DISTINCT CAST(shopid AS bigint) shopid 
FROM regcbbi_others.sip__mysip_migration_smt_exclude__tmp__reg__s0
)

, wms AS (
SELECT shop_id
FROM sbs_mart.shopee_scbs_db__shop_tab__reg_daily_s0_live
WHERE country IN ('MY')
AND ((fbs_tag = 0 AND vacation_mode = 0 AND shop_status = 1)
OR (fbs_tag = 1 AND vacation_mode = 0 AND cb_option = 1)
OR (fbs_tag = 1 AND vacation_mode = 0 AND cb_option = 0))
)

, channel AS (
SELECT DISTINCT
shopid
-- , COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20088.enabled') AS bigint), 0) have_channel_spxmarketplace
-- , COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20007.enabled') AS bigint), 0) have_channel_pos
-- , COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20011.enabled') AS bigint), 0) have_channel_jnt
-- , COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20010.enabled') AS bigint), 0) have_channel_edhl
-- , COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20021.enabled') AS bigint), 0) have_channel_njv
-- , COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.20023.enabled') AS bigint), 0) have_channel_citylink
, COALESCE(CAST("json_extract"("from_utf8"(d.extinfo.logistics_info), '$.channels.2000.enabled') AS bigint), 0) have_channel_mask
, e.state address_state
FROM marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live d 
LEFT JOIN (
SELECT 
id
, userid
, state 
FROM marketplace.shopee_account_db__buyer_address_tab__reg_daily_s0_live 
WHERE country = 'MY' AND status <> 0) e ON e.id = d.extinfo.pickup_address_id AND e.userid = d.userid
WHERE d.grass_region = 'MY'
)

, supportive_sku AS (
SELECT DISTINCT
shopid
, count(DISTINCT itemid) live_sku
, count(DISTINCT CASE WHEN have_channel_mask = 1 THEN itemid ELSE NULL END) supportive_sku
, count(DISTINCT CASE WHEN level2_global_be_category = 'E-Cigarettes' THEN itemid ELSE NULL END) e_sku
FROM (
SELECT DISTINCT
shop_id shopid
, item_id itemid
, level2_global_be_category
, "sum"(DISTINCT (CASE WHEN (((CAST("json_extract"(shipping_info, '$.2000.enabled') AS varchar) = 'true') OR (CAST("json_extract"(shipping_info, '$.2000.enabled') AS boolean) = true)) OR (CAST("json_extract"(shipping_info, '$.2000.enabled') AS int) = 1)) THEN 1 ELSE 0 END)) have_channel_mask
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region = 'MY' AND is_cb_shop = 0 AND status = 1 AND stock > 0 AND grass_date = current_date - INTERVAL '1' DAY AND tz_type = 'local' AND seller_status = 1 AND shop_status = 1 AND is_holiday_mode = 0
GROUP BY 1,2,3
)
GROUP BY 1
)

SELECT * FROM 
(SELECT DISTINCT
grass_region
, shopid
, is_official_shop
, user_id
, live_sku
, username
, main_category
, NFR_included_cancellations_30d
, NFR_included_rr_30d
, gross_orders_30d
, cr_l90d
, "sum"(live_sku) OVER (ORDER BY gross_orders_30d DESC, cr_l90d ASC, shopid ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) acc_sku
FROM
(SELECT DISTINCT 
grass_region
, s.shopid
, is_official_shop
, user_id
, live_sku
, s.username
, main_category
, COALESCE("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)), 0) NFR_included_cancellations_30d
, COALESCE("count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END)), 0) NFR_included_rr_30d
, COALESCE("count"(DISTINCT (CASE WHEN (o.create_time BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) THEN o.orderid ELSE null END)), 0) / DECIMAL '30.00' gross_orders_30d
, COALESCE(TRY((("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) * DECIMAL '1.000000') / (("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) + "count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END))) + "count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid ELSE null END))))), 0) cr_l90d
FROM shop s
LEFT JOIN ex ON s.shopid = ex.shopid
LEFT JOIN wms ON s.shopid = wms.shop_id
LEFT JOIN channel c ON s.shopid = c.shopid 
LEFT JOIN supportive_sku ss ON s.shopid = ss.shopid
LEFT JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live sip ON s.shopid = sip.shopid
LEFT JOIN o ON s.shopid = o.shopid 
LEFT JOIN r ON s.shopid = r.shopid
WHERE sip.shopid IS NULL -- not SIP P shop
-- AND (have_channel_spxmarketplace + have_channel_pos + have_channel_jnt + have_channel_edhl + have_channel_njv + have_channel_citylink) > 0 -- shop with supportive channel
AND have_channel_mask = 1 -- have mask channel
AND address_state IN ('Johor', 'Kedah', 'Kelantan', 'Kuala Lumpur', 'Melaka', 'Negeri Sembilan', 'Pahang', 'Penang', 'Perak', 'Perlis', 'Putrajaya', 'Selangor', 'Terengganu') -- shop in west malaysia
AND wms.shop_id IS NULL -- not wh shop
AND s.sub_category NOT IN ('E-Cigarettes') AND s.main_category NOT IN ('Tickets, Vouchers & Services') 
AND supportive_sku > 0 -- at least one live sku with supportive channel
AND ex.shopid IS NULL -- exclude shops sent TOS before and in smt provided list
AND e_sku * DECIMAL '1.000000' / live_sku < 0.5 -- less than 50% e-cig sku
GROUP BY 1,2,3,4,5,6,7)
WHERE gross_orders_30d > DECIMAL '0.1'
AND cr_l90d <= DECIMAL '0.03'
ORDER BY gross_orders_30d DESC, cr_l90d ASC, shopid ASC
)
-- WHERE acc_sku < 2000000