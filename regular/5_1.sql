WITH
dim AS (
SELECT DISTINCT
"min"(date_id) - interval'14'day w_2_begin
, "min"(date_id) - interval'8'day w_2_end
FROM
regbida_keyreports.dim_date
WHERE (("week"(date_id) = ("week"(current_date))) AND ("year"(date_id) = "year"(current_date)))
) 
, dim2 AS (
SELECT DISTINCT
"min"(date_id) - interval'21'day w_3_begin
, "min"(date_id) - interval'15'day w_3_end
FROM
regbida_keyreports.dim_date
WHERE (("week"(date_id) = ("week"(current_date))) AND ("year"(date_id) = "year"(current_date)))
) 
, dim3 AS (
SELECT DISTINCT
"min"(date_id) - interval'28'day w_4_begin
, "min"(date_id) - interval'22'day w_4_end
FROM
regbida_keyreports.dim_date
WHERE (("week"(date_id) = ("week"(current_date))) AND ("year"(date_id) = "year"(current_date)))
) 
, dim4 AS (
SELECT DISTINCT
"min"(date_id) - interval'35'day w_5_begin
, "min"(date_id) - interval'29'day w_5_end
FROM
regbida_keyreports.dim_date
WHERE (("week"(date_id) = ("week"(current_date))) AND ("year"(date_id) = "year"(current_date)))
) 

, dim5 AS (
SELECT DISTINCT
"min"(date_id) - interval'42'day w_6_begin
, "min"(date_id) - interval'36'day w_6_end
FROM
regbida_keyreports.dim_date
WHERE (("week"(date_id) = ("week"(current_date))) AND ("year"(date_id) = "year"(current_date)))
) 

, dim6 AS (
SELECT DISTINCT
"min"(date_id) - interval'49'day w_7_begin
, "min"(date_id) - interval'43'day w_7_end
FROM
regbida_keyreports.dim_date
WHERE (("week"(date_id) = ("week"(current_date))) AND ("year"(date_id) = "year"(current_date)))
) 

, dim1 AS (
SELECT DISTINCT
"min"(date_id) - interval'7'day w_1_begin
, "min"(date_id) - interval'1'day w_1_end
FROM
regbida_keyreports.dim_date
WHERE (("week"(date_id) = ("week"(current_date))) AND ("year"(date_id) = "year"(current_date)))
) 

, sip AS (
SELECT DISTINCT
affi_shopid
, mst_shopid
, a.country affi_country
, v.country mst_country
, cb_option
FROM
(marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
, cb_option 
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
-- WHERE (cb_option = 1)
) v ON (v.shopid = a.mst_shopid))
where 
a.grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') and
a.sip_shop_status!=3 
) 
, cb AS (
SELECT DISTINCT CAST(child_shopid AS bigint) child_shopid
FROM
cncbbi_general.shopee_cb_seller_profile_with_old_column
where grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) 
, nmv AS (
SELECT DISTINCT
grass_region
, (CASE WHEN (grass_date BETWEEN w_2_begin AND w_2_end) THEN 'W-2*' 
WHEN (grass_date BETWEEN w_3_begin AND w_3_end) THEN 'W-3*' 
WHEN (grass_date BETWEEN w_4_begin AND w_4_end) THEN 'W-4*'
WHEN (grass_date BETWEEN w_5_begin AND w_5_end) THEN 'W-5*' 
WHEN (grass_date BETWEEN w_6_begin AND w_6_end) THEN 'W-6*' 
WHEN (grass_date BETWEEN w_7_begin AND w_7_end) THEN 'W-7*' 
WHEN (grass_date BETWEEN w_1_begin AND w_1_end) THEN 'W-1*' 
ELSE null END) time_break
, CASE WHEN CB_OPTION=1 and mst_country='TW' THEN 'TB SIP' 
WHEN CB_OPTION=1 and mst_country='MY' THEN 'CB SIP' 
when CB_OPTION=1 and mst_country='BR' THEN 'BR SIP' 
when CB_OPTION=1 and mst_country='SG' THEN 'KRCB SIP' 

when CB_OPTION=0 and mst_country='TW' THEN 'Local TWSIP' 
when CB_OPTION=0 and mst_country='MY' THEN 'Local MYSIP' 
when CB_OPTION=0 and mst_country='ID' THEN 'Local IDSIP' 
when CB_OPTION=0 and mst_country='VN' THEN 'Local VNSIP' 
ELSE 
'Local SIP' end as shop_type
, ("sum"(total_price) ) total_price
FROM
((((((
SELECT DISTINCT
order_id orderid
,order_sn ordersn
-- , total_price_usd
,gmv total_price
-- , buyer_shipping_fee
,shop_id shopid
, grass_region
, date(split(create_datetime,' ')[1]) grass_date
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ( date(split(create_datetime,' ')[1]) >= (current_date - INTERVAL '60' DAY)) and is_cb_shop=1 AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and grass_date >= (current_date - INTERVAL '60' DAY)
) a
INNER JOIN dim ON (1 = 1))
INNER JOIN dim2 ON (1 = 1))
INNER JOIN DIM3 ON (1 = 1))
INNER JOIN DIM4 ON (1 = 1))
inner join dim5 on 1=1 
inner join dim6 on 1=1 
inner join dim1 on 1=1 
INNER JOIN SIP ON SIP.AFFI_SHOPID = A.SHOPID 
INNER JOIN (
select distinct ordersn 
from regcbbi_others.logistics_basic_info
where cast(wh_outbound_date as date )>= (current_date - INTERVAL '60' DAY)
) e ON (a.ordersn = e.ordersn))
-- where sip.mst_country ='MY'
GROUP BY 1, 2, 3
) 
, rr AS (
SELECT DISTINCT
a.grass_region
, (CASE WHEN ( date(split(create_datetime,' ')[1]) BETWEEN w_2_begin AND w_2_end) THEN 'W-2*'

WHEN ( date(split(create_datetime,' ')[1]) BETWEEN w_3_begin AND w_3_end) THEN 'W-3*' 

WHEN ( date(split(create_datetime,' ')[1]) BETWEEN w_4_begin AND w_4_end) THEN 'W-4*' 

WHEN ( date(split(create_datetime,' ')[1]) BETWEEN w_5_begin AND w_5_end) THEN 'W-5*' 

WHEN ( date(split(create_datetime,' ')[1]) BETWEEN w_6_begin AND w_6_end) THEN 'W-6*' 

WHEN ( date(split(create_datetime,' ')[1]) BETWEEN w_7_begin AND w_7_end) THEN 'W-7*' 

WHEN ( date(split(create_datetime,' ')[1]) BETWEEN w_1_begin AND w_1_end) THEN 'W-1*' 

ELSE null END) time_break
, (CASE WHEN (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (mst_country = 'MY') and cb_option =1)) then 'CB SIP'
WHEN (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (mst_country = 'TW') and cb_option =1)) then 'TB SIP'

WHEN (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (mst_country = 'BR') and cb_option =1)) then 'BR SIP'

WHEN (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (mst_country = 'SG') and cb_option =1)) then 'KRCB SIP'


WHEN (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (mst_country = 'MY') and cb_option =0)) THEN 'Local MYSIP'

WHEN (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (mst_country = 'ID') and cb_option =0)) THEN 'Local IDSIP'

WHEN (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (mst_country = 'TW') and cb_option =0)) THEN 'Local TWSIP'

WHEN (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
WHERE (mst_country = 'VN') and cb_option =0)) THEN 'Local VNSIP'

WHEN (NOT (a.shop_id IN (SELECT DISTINCT affi_shopid
FROM
sip
))) THEN 'CB' END) shop_type

, "sum"((CASE WHEN ((r.reason IN (1, 2, 3, 103, 105, 107)) AND (r.status IN (2, 5))) THEN refund_amount ELSE null END)) refund_amount
, "sum"((CASE WHEN ((r.reason IN (1)) AND (r.status IN (2, 5))) THEN refund_amount ELSE null END)) NON_RECEIPT_refund_amount
, "sum"((CASE WHEN ((r.reason IN (2, 3, 103, 105, 107)) AND (r.status IN (2, 5))) THEN refund_amount ELSE null END)) RECEIPT_refund_amount
, "count"(DISTINCT (CASE WHEN (r.reason IN (1, 2, 3, 103, 105, 107)) THEN r.orderid ELSE null END)) rr_orders
, "count"(DISTINCT (CASE WHEN ((r.reason IN (1, 2, 3, 103, 105, 107)) AND (r.status IN (2, 5))) THEN r.orderid ELSE null END)) rr_approve_orders
, "count"(DISTINCT (CASE WHEN (r.reason IN (1)) THEN r.orderid ELSE null END)) non_receipt_rr_orders
, "count"(DISTINCT (CASE WHEN ((r.reason IN (1)) AND (r.status IN (2, 5))) THEN r.orderid ELSE null END)) non_receipt_rr_approve_orders
, "count"(DISTINCT (CASE WHEN (r.reason IN (2, 3, 103, 105, 107)) THEN r.orderid ELSE null END)) receipt_rr_orders
, "count"(DISTINCT (CASE WHEN ((r.reason IN (2, 3, 103, 105, 107)) AND (r.status IN (2, 5))) THEN r.orderid ELSE null END)) receipt_rr_approve_orders
, "count"(DISTINCT (CASE WHEN ((payment_method_id <> 6) AND (pay_timestamp IS NOT NULL)) THEN a.order_id WHEN ((payment_method_id = 6) AND ((shipping_confirm_timestamp IS NOT NULL))) THEN a.order_id ELSE null END)) gross_order
, "count"(DISTINCT (CASE WHEN (logistics_status_id IN (5, 6, 10)) THEN a.order_id ELSE null END)) gross_deliver_order
FROM
((((((mp_order.dwd_order_all_ent_df__reg_s0_live a
INNER JOIN dim ON (1 = 1))
INNER JOIN dim2 ON (1 = 1))
INNER JOIN DIM3 ON (1 = 1))
INNER JOIN DIM4 ON (1 = 1))
inner join dim5 on 1=1 
inner join dim6 on 1=1 
inner join dim1 on 1=1 


INNER JOIN (
SELECT DISTINCT child_shopid
FROM
cb
) cb ON (cb.child_shopid = a.shop_id))
LEFT JOIN (
SELECT DISTINCT
orderid
, reason
, status
, (refund_amount / DECIMAL '100000.00') refund_amount
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
where cb_option=1 and grass_region<>'o'
) r ON (r.orderid = a.order_id))
WHERE ( date(split(create_datetime,' ')[1]) >= (current_date - INTERVAL '60' DAY)) AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') AND is_cb_shop=1 and grass_date >= (current_date - INTERVAL '60' DAY)
GROUP BY 1, 2, 3
) 
SELECT *
FROM
(
SELECT DISTINCT
rr.grass_region
, rr.time_break
, rr.shop_type
, TRY(((rr_approve_orders * DECIMAL '1.000000') / CAST(gross_order AS decimal(10,2)))) overall_rr_approve
, TRY(((non_receipt_rr_approve_orders * DECIMAL '1.000000') / CAST(gross_order AS decimal(10,2)))) non_receipt_rr_approve
, TRY(((receipt_rr_approve_orders * DECIMAL '1.000000') / CAST(gross_deliver_order AS decimal(10,2)))) receipt_rr_approve
, rr.refund_amount
, NON_RECEIPT_refund_amount
, RECEIPT_refund_amount
, COALESCE(nmv.total_price, 0) total_price
, rr_orders
, rr_approve_orders
, gross_order
FROM
(rr
LEFT JOIN nmv ON (((rr.grass_region = nmv.grass_region) 
AND (rr.time_break = nmv.time_break))
AND (rr.shop_type = nmv.shop_type)))
) 
WHERE (time_break IS NOT NULL) 
--AND SHOP_TYPE IS NOT NULL