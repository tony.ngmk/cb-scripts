WITH
sip AS (
SELECT DISTINCT 
grass_region
, affi_shopid
, mst_shopid
, b.country mst_country
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b
ON a.mst_shopid = b.shopid
WHERE b.cb_option = 0
and sip_shop_status!=3 
) 
, fees AS (
SELECT DISTINCT
orderid
, cc_fee
, commission_fee
, service_fee
, cc_fee_usd
, commission_fee_usd
, service_fee_usd
, lane 
FROM
regcbbi_others.shopee_bi_keyreports_local_sip_revenue_v3
WHERE grass_date >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day
)
, sls as
(
select ordersn
, date(from_unixtime(min(wh_outbound_time))) as wh_outbound_date
, min(sls_ops_parcel_lost_time) as sls_ops_parcel_lost_time
, min(sls_ops_parcel_confiscated_time) as sls_ops_parcel_confiscated_time
, min(sls_ops_terminal_damage_time) as sls_ops_terminal_damage_time
from regcbbi_others.cb_logistics_mart_test
where integrated_channel_origin is not null
and integrated_channel_origin NOT in ('CN', 'KR', 'JP')
group by 1
)

, o AS (
SELECT DISTINCT
"date"("from_unixtime"(create_timestamp)) create_timestamp
, order_id
, order_sn
, shop_id
, gmv
, gmv_usd 
, pv_rebate_by_shopee_amt_usd
, card_rebate_by_bank_amt_usd 
, coin_used_cash_amt_usd 
, sv_coin_earn_by_seller_amt_usd 
, pv_coin_earn_by_seller_amt_usd 
, IF((actual_shipping_rebate_by_shopee_amt_usd = 0), estimate_shipping_rebate_by_shopee_amt_usd, actual_shipping_rebate_by_shopee_amt_usd) shopee_shipping_rebate_usd
, buyer_paid_shipping_fee
, pv_rebate_by_shopee_amt
, sv_rebate_by_shopee_amt
, coin_used_cash_amt
, sv_coin_earn_by_seller_amt
, card_rebate_by_bank_amt
, sv_rebate_by_seller_amt
, pv_rebate_by_seller_amt
, IF((actual_shipping_rebate_by_shopee_amt = 0), estimate_shipping_rebate_by_shopee_amt, actual_shipping_rebate_by_shopee_amt) shopee_shipping_rebate
, actual_shipping_fee
, logistics_status_id
, payment_method_id
, order_be_status_id
, order_fe_status_id
, sv_promotion_id 
--, logistics_status_id 
, card_rebate_by_shopee_amt
, is_cb_shop
, grass_region
, pv_coin_earn_by_seller_amt
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_region not in ('FR', 'AR', 'IN')
and "date"("from_unixtime"(create_timestamp)) >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day -- need wh outbound orders from D-70 to D-1; should be safe to limit gross order to D-120
and tz_type = 'local'
and grass_date >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '120' day
) 
, oi AS (
SELECT DISTINCT
order_id
, order_sn
, is_cb_shop
, o.grass_region
, "sum"(item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
, "sum"(item_tax_amt) item_tax_amt
, sum(item_tax_amt_usd) item_tax_amt_usd
, sum(item_rebate_by_shopee_amt_usd) item_rebate_by_shopee_amt_usd
, "sum"(commission_fee) commission_fee
, "sum"(buyer_txn_fee) buyer_txn_fee
, "sum"(seller_txn_fee) seller_txn_fee
, "sum"(service_fee) service_fee
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
LEFT JOIN sip ON o.shop_id = sip.affi_shopid
WHERE o.grass_region not in ('FR', 'AR', 'IN')
AND "date"("from_unixtime"(create_timestamp)) >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day
and tz_type='local' and grass_date >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '120' day
and is_cb_shop=1 
GROUP BY 1, 2, 3, 4
) 

SELECT DISTINCT sip.grass_region as affi_country 
, mst_country
, sls.wh_outbound_date
, count(distinct a.order_id) as orders
, sum(a.gmv) as gmv
, sum(a.gmv_usd) as gmv_usd
, count(distinct if(a.payment_method_id = 6, a.order_id, null)) as cod_orders 
, sum(if(a.payment_method_id = 6, a.gmv, 0)) as cod_order_gmv 
, sum(if(a.payment_method_id = 6, a.gmv_usd, 0)) as cod_order_gmv_usd 
, count(distinct if(a.logistics_status_id = 6 and a.payment_method_id = 6 and sls.sls_ops_parcel_confiscated_time is null and sls.sls_ops_terminal_damage_time is null, a.order_id, null)) as cod_failed_orders 

, sum(if(a.logistics_status_id = 6 and a.payment_method_id = 6 and sls.sls_ops_parcel_confiscated_time is null and sls.sls_ops_terminal_damage_time is null
, a.gmv + ai.item_rebate_by_shopee_amt + a.pv_rebate_by_shopee_amt + a.coin_used_cash_amt 
+ a.card_rebate_by_bank_amt + a.card_rebate_by_bank_amt - a.sv_coin_earn_by_seller_amt 
- a.pv_coin_earn_by_seller_amt - ai.item_tax_amt + a.shopee_shipping_rebate + fees.commission_fee 
+ fees.cc_fee + fees.service_fee
, 0)) as cod_fail_gmv 

, sum(if(a.logistics_status_id = 6 AND a.payment_method_id = 6 and sls.sls_ops_parcel_confiscated_time is null and sls.sls_ops_terminal_damage_time is null
, a.gmv_usd + ai.item_rebate_by_shopee_amt_usd + a.pv_rebate_by_shopee_amt_usd + a.coin_used_cash_amt_usd 
+ a.card_rebate_by_bank_amt_usd + a.card_rebate_by_bank_amt_usd - a.sv_coin_earn_by_seller_amt_usd 
- a.pv_coin_earn_by_seller_amt_usd - ai.item_tax_amt_usd + a.shopee_shipping_rebate_usd + fees.commission_fee_usd 
+ fees.cc_fee_usd + fees.service_fee_usd
, 0)) as cod_fail_gmv_usd 
, count(distinct if(a.order_fe_status_id in (3,4) or a.logistics_status_id in (5,6,11), a.order_id, null)) as complete_orders
FROM
(
SELECT *
FROM o
WHERE is_cb_shop = 1
) a
JOIN 
(
SELECT *
FROM oi
WHERE is_cb_shop = 1
) ai
ON a.order_id = ai.order_id
JOIN sip
ON a.shop_id = sip.affi_shopid
JOIN marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live m
ON a.order_id = m.oversea_orderid
JOIN 
(
SELECT *
FROM o
WHERE order_be_status_id NOT IN (1, 6, 8, 5, 7, 8, 16) AND is_cb_shop = 0
) p 
ON m.local_orderid = p.order_id
JOIN sls
ON a.order_sn = sls.ordersn
LEFT JOIN fees
ON a.order_id = fees.orderid
WHERE sls.wh_outbound_date between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '69' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
GROUP BY 1, 2, 3