WITH
sip AS (
SELECT
cb_option
, b.country mst_country
, a.grass_region
, a.affi_shopid
, a.mst_shopid 
, a.sip_shop_status
, b.country AS sip_primary_shop_region
, CASE WHEN a.sip_shop_status = 3 THEN DATE(FROM_UNIXTIME(offboard_time)) 
ELSE DATE '9999-01-01' END AS sip_shop_offboard_date -- ! TO CHECK LOGIC
, (CASE WHEN ((cb_option = 0) AND (b.country = 'ID')) THEN 'Local SIP ID' 
WHEN ((cb_option = 0) AND (b.country = 'TW')) THEN 'Local SIP TW' 
WHEN ((cb_option = 0) AND (b.country = 'MY')) THEN 'Local SIP MY' 
WHEN ((cb_option = 1) AND (b.country = 'TW')) THEN 'CNCB SIP TW' 
WHEN ((cb_option = 1) AND (b.country = 'MY')) THEN 'CNCB SIP MY' 
WHEN ((cb_option = 1) AND (b.country = 'SG')) THEN 'KRCB SIP SG' 
WHEN ((cb_option = 1) AND (b.country = 'BR')) THEN 'CNCB SIP BR' 
WHEN ((cb_option = 0) AND (b.country = 'TH')) THEN 'Local SIP TH'
WHEN ((cb_option = 0) AND (b.country = 'VN')) THEN 'Local SIP VN'
ELSE 'Other SIP' END) source
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON a.mst_shopid = b.shopid
WHERE a.grass_region <> '' 
-- AND sip_shop_status <> 3
) 
, cb AS (
SELECT
TRY_CAST(child_shopid AS bigint) child_shopid
, (CASE WHEN ((gp_account_billing_country = 'China') OR (gp_account_billing_country IS NULL)) THEN 'CNCB' 
-- WHEN (gp_account_billing_country = 'Korea') THEN 'KRCB' 
-- WHEN (gp_account_billing_country = 'Japan') THEN 'JPCB' 
ELSE 'Other CB' END) source
FROM
cncbbi_general.shopee_cb_seller_profile_with_old_column
WHERE ((gp_account_billing_country IN ('China', 'Korea', 'Japan')) OR (gp_account_billing_country IS NULL)) 
) 
, shop AS (
SELECT user_id
, shop_id
, shop_name
, user_name
, description
, status
, sold_total
, cb_shop_origin_country
, rating_good
, rating_normal
, rating_bad
, response_rate
, rating_star
, seller_rating
, is_official_shop
, is_preferred_shop
, is_cb_shop
, is_auto_reply
, is_holiday_mode
, is_b2c
, is_seller_active
, tz_type
, grass_region
, grass_date
FROM
mp_user.dim_shop__reg_s0_live 
WHERE grass_date = (CAST('${BIZ_TIME}' AS DATE) - INTERVAL '1' DAY)
AND tz_type = 'local'
) 
, user AS (
SELECT
grass_date
, shop_id
, status
FROM
mp_user.dim_user__reg_s0_live
WHERE grass_date = (CAST('${BIZ_TIME}' AS DATE) - INTERVAL '1' DAY)
AND tz_type = 'local'
) 
, item AS (
SELECT 
shop_id
, grass_date
-- , shop_level1_category
-- , shop_level2_category
, shop_level1_global_be_category
, shop_level2_global_be_category
, active_item_cnt
, active_item_with_stock_cnt
, shop_first_listing_timestamp
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE grass_date = (CAST('${BIZ_TIME}' AS DATE) - INTERVAL '1' DAY)
AND tz_type = 'local'
) 
, last_login AS (

SELECT 
user_id,
last_login_timestamp_td
FROM mp_user.ads_activeness_user_activity_td__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = (CAST('${BIZ_TIME}' AS DATE) - INTERVAL '1' DAY)

)
, output AS (
SELECT
shop.user_id
, shop.shop_id
, shop.shop_name
, shop.user_name
-- , shop.description
, shop.status
, shop.sold_total
, shop.cb_shop_origin_country
, shop.rating_good
, shop.rating_normal
, shop.rating_bad
, shop.response_rate
, shop.rating_star
, shop.seller_rating
, shop.is_official_shop
, shop.is_preferred_shop
, shop.is_cb_shop
, shop.is_auto_reply
, shop.is_holiday_mode
, shop.is_b2c
, shop.is_seller_active
, shop.tz_type
, CASE WHEN shop.is_cb_shop = 0 AND mst.shop_id is NULL THEN 'Local' 
WHEN shop.is_cb_shop = 0 AND mst.shop_id is NOT NULL THEN 'Local primary'

-- 2022-04-18 Revised logic to get JPCB and KRCB shops 
WHEN shop.is_cb_shop = 1 AND shop.cb_shop_origin_country = 'JP' THEN 'JPCB'
WHEN shop.is_cb_shop = 1 AND shop.cb_shop_origin_country = 'KR' AND (sip.affi_shopid IS NULL OR sip.sip_shop_status = 3) THEN 'KRCB'

WHEN shop.is_cb_shop = 1 AND cb.child_shopid IS NOT NULL AND (sip.affi_shopid IS NULL OR sip.sip_shop_status = 3) THEN cb.source 
WHEN shop.is_cb_shop = 1 AND cb.child_shopid IS NULL AND (sip.affi_shopid IS NULL OR sip.sip_shop_status = 3) THEN 'Other CB' 
WHEN shop.is_cb_shop = 1 AND (sip.affi_shopid IS NOT NULL AND sip.sip_shop_status <> 3) THEN sip.source 
ELSE 'Others' END AS source_ds

, CASE WHEN shop.is_cb_shop = 0 THEN 'Local' 
-- 2022-04-18 Revised logic to get JPCB and KRCB shops 
WHEN shop.is_cb_shop = 1 AND shop.cb_shop_origin_country = 'JP' THEN 'JPCB'
WHEN shop.is_cb_shop = 1 AND shop.cb_shop_origin_country = 'KR' AND (sip.affi_shopid IS NULL OR sip.sip_shop_status = 3) THEN 'KRCB'

WHEN shop.is_cb_shop = 1 AND cb.child_shopid IS NOT NULL AND (sip.affi_shopid IS NULL OR sip.sip_shop_status = 3) THEN cb.source 
WHEN shop.is_cb_shop = 1 AND cb.child_shopid IS NULL AND (sip.affi_shopid IS NULL OR sip.sip_shop_status = 3) THEN 'Other CB' 
WHEN shop.is_cb_shop = 1 AND (sip.affi_shopid IS NOT NULL AND sip.sip_shop_status <> 3) THEN sip.source 
ELSE 'Others' END AS source

, CASE WHEN shop.is_cb_shop = 0 THEN 'Local' 
WHEN shop.is_cb_shop = 1 AND shop.cb_shop_origin_country = 'JP' THEN 'JPCB' 
WHEN shop.is_cb_shop = 1 AND cb.child_shopid IS NOT NULL AND (sip.affi_shopid IS NULL) THEN cb.source 
WHEN shop.is_cb_shop = 1 AND cb.child_shopid IS NULL AND sip.affi_shopid IS NULL THEN 'Other CB' 
WHEN shop.is_cb_shop = 1 AND sip.affi_shopid IS NOT NULL THEN sip.source 
ELSE 'Others' END AS source_before_shop_return
, user.status AS user_status
, NULL AS last_login_timestamp
, NULL AS shop_level1_category
, NULL AS shop_level2_category
, item.shop_level1_global_be_category
, item.shop_level2_global_be_category
, item.active_item_cnt
, item.active_item_with_stock_cnt
, item.shop_first_listing_timestamp
, sip.sip_shop_offboard_date
, CASE WHEN (sip.affi_shopid IS NOT NULL AND sip.sip_shop_status <> 3) THEN sip.mst_shopid ELSE NULL END AS sip_primary_shop_id
, CASE WHEN (sip.affi_shopid IS NOT NULL AND sip.sip_shop_status <> 3) THEN sip.sip_primary_shop_region ELSE NULL END AS sip_primary_shop_region
, shop.grass_region
, shop.grass_date
FROM
shop
INNER JOIN user ON shop.shop_id = user.shop_id AND shop.grass_date = user.grass_date
LEFT JOIN item ON shop.shop_id = item.shop_id AND shop.grass_date = item.grass_date
LEFT JOIN cb ON shop.shop_id = cb.child_shopid
LEFT JOIN sip ON shop.shop_id = sip.affi_shopid
-- LEFT JOIN last_login ON last_login.user_id = shop.user_id
LEFT JOIN (
SELECT shop_id 
FROM mp_user.dim_shop_ext__reg_s0_live 
WHERE grass_date = (CAST('${BIZ_TIME}' AS DATE) - INTERVAL '1' DAY)
AND tz_type = 'local'
AND is_sip_primary = 1 
) mst ON shop.shop_id = mst.shop_id
)

SELECT * FROM output
WHERE shop_id > 0;

