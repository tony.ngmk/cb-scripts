WITH
sip AS ( 
SELECT DISTINCT
affi_shopid
, mst_shopid
, t1.country mst_country
, t2.country affi_country
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (grass_region IN ('SG', 'MY', 'PH'))
) t2 ON (t1.shopid = t2.mst_shopid))
WHERE (((cb_option = 0) AND (t1.country IN ('TH'))) AND (t2.country IN ('SG', 'MY', 'VN')))
) 
, buyer_account AS (
SELECT *
FROM
(
VALUES 
ROW (32352918, 'TW')
, ROW (174331327, 'MY')
, ROW (216124110, 'ID')
, row (502225957,'TH')

) t (buyer_id, grass_region)
) 
, raw AS (
SELECT DISTINCT
oversea_orderid
, "max"(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE (((local_country = 'th') AND (oversea_country IN ('sg', 'my', 'ph'))) AND ("date"("from_unixtime"(ctime)) >= (current_date - INTERVAL '90' DAY)))
GROUP BY 1
) 
, log AS (
SELECT DISTINCT
orderid
, shopid
, "min"((CASE WHEN (new_status = 1) THEN ctime ELSE null END)) ar_time
, "min"((CASE WHEN (new_status = 2) THEN ctime ELSE null END)) pu_time
FROM
(
SELECT DISTINCT
a.orderid
, A.SHOPID
, CTIME
, new_status
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_continuous_s0_live a
WHERE ((grass_region = 'th') AND ("date"("from_unixtime"(ctime)) >= (current_date - INTERVAL '90' DAY)))
) 
GROUP BY 1, 2
) 
SELECT DISTINCT
affi_cancel_time
, mst_cancel_time
, affi_shopid
, mst_shopid
, affi_country
, mst_country
, affi_cancel_reason
, mst_cancel_reason
, affi_cancel_reason_ext
, mst_cancel_reason_ext
, oversea_orderid
, local_orderid
, reason
, Operator
, BUYER_ID
, acl1_acl2
, case when regexp_like(lower(reason),'buyer') then 'Buyer Request'
when regexp_like(lower(reason),'prohibited') then 'Prohibited listing'
when affi_cancel_reason in (1,2,3,4) and buyer_id is not null then 'OverWeight'
else 'Other' end as remark
FROM
(
SELECT DISTINCT
o2.cancel_time affi_cancel_time
, o3.cancel_time mst_cancel_time
, affi_shopid
, mst_shopid
, affi_country
, mst_country
, o2.cancel_reason as affi_cancel_reason
, o3.cancel_Reason as mst_cancel_reason 

, (CASE WHEN (O2.cancel_reason = 1) THEN 'CANCEL_REASON_OUT_OF_STOCK' WHEN (O2.cancel_reason = 2) THEN 'CANCEL_REASON_CUSTOMER_REQUEST' WHEN (o2.cancel_reason = 3) THEN 'CANCEL_REASON_UNDELIVERABLE_AREA ' WHEN (o2.cancel_reason = 4) THEN 'CANCEL_REASON_CANNOT_SUPPORT_COD ' WHEN (o2.cancel_reason = 5) THEN 'CANCEL_REASON_LOST_PARCEL ' WHEN (o2.cancel_reason = 100) THEN 'CANCEL_REASON_SYSTEM_UNPAID ' WHEN (o2.cancel_reason = 200) THEN 'CANCEL_REASON_LOGISTICS_REQUEST_CANCELED ' WHEN (O2.cancel_reason = 201) THEN 'CANCEL_REASON_LOGISTICS_PICKUP_FAILED' WHEN (o2.cancel_reason = 202) THEN 'CANCEL_REASON_LOGISTICS_DELIVERY_FAILED ' WHEN (o2.cancel_reason = 203) THEN 'CANCEL_REASON_LOGISTICS_COD_REJECTED ' WHEN (o2.cancel_reason = 204) THEN 'CANCEL_REASON_SELLER_NOT_ARRANGE_PICKUP' WHEN (o2.cancel_reason = 300) THEN 'CANCEL_REASON_BACKEND_ESCROW_TERMINATED ' WHEN (o2.cancel_reason = 301) THEN 'CANCEL_REASON_BACKEND_INACTIVE_SELLER' WHEN (o2.cancel_reason = 302) THEN 'CANCEL_REASON_BACKEND_SELLER_DID_NOT_SHIP' ELSE 'Other' END) affi_cancel_reason_ext
, (CASE WHEN (O3.cancel_reason = 1) THEN 'CANCEL_REASON_OUT_OF_STOCK' WHEN (O3.cancel_reason = 2) THEN 'CANCEL_REASON_CUSTOMER_REQUEST' WHEN (O3.cancel_reason = 3) THEN 'CANCEL_REASON_UNDELIVERABLE_AREA ' WHEN (O3.cancel_reason = 4) THEN 'CANCEL_REASON_CANNOT_SUPPORT_COD ' WHEN (O3.cancel_reason = 5) THEN 'CANCEL_REASON_LOST_PARCEL ' WHEN (O3.cancel_reason = 100) THEN 'CANCEL_REASON_SYSTEM_UNPAID ' WHEN (O3.cancel_reason = 200) THEN 'CANCEL_REASON_LOGISTICS_REQUEST_CANCELED ' WHEN (O3.cancel_reason = 201) THEN 'CANCEL_REASON_LOGISTICS_PICKUP_FAILED' WHEN (O3.cancel_reason = 202) THEN 'CANCEL_REASON_LOGISTICS_DELIVERY_FAILED ' WHEN (O3.cancel_reason = 203) THEN 'CANCEL_REASON_LOGISTICS_COD_REJECTED ' WHEN (O3.cancel_reason = 204) THEN 'CANCEL_REASON_SELLER_NOT_ARRANGE_PICKUP' WHEN (O3.cancel_reason = 300) THEN 'CANCEL_REASON_BACKEND_ESCROW_TERMINATED ' WHEN (O3.cancel_reason = 301) THEN 'CANCEL_REASON_BACKEND_INACTIVE_SELLER' WHEN (O3.cancel_reason = 302) THEN 'CANCEL_REASON_BACKEND_SELLER_DID_NOT_SHIP' ELSE 'Other' END) mst_cancel_reason_ext
, oversea_orderid
, local_orderid
, reason
, Operator
, (CASE WHEN ((o2.cancel_reason = 204) AND (ar_time IS NOT NULL)) THEN 'Y'
WHEN ((o2.cancel_reason in (302,303) ) AND (pu_time IS NOT NULL)) THEN 'Y' ELSE 'N' END) acl1_acl2
, case when o2.cancel_reason in (204,302,303) and ba.buyer_id is not null then 'Y' else 'N' end as seller_fault
, BA.BUYER_ID 
FROM
(((((((sip
INNER JOIN (
SELECT DISTINCT
orderid
, shopid
, extinfo.cancel_reason
, "from_unixtime"(cancel_time) cancel_time
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE (((("date"("from_unixtime"(create_time)) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) AND (cb_option = 1)) AND (extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) AND (grass_region IN ('SG', 'MY', 'ID')))
) o2 ON (o2.shopid = sip.affi_shopid))
INNER JOIN raw o ON (o.oversea_orderid = o2.orderid))
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
, "from_unixtime"(cancel_time) cancel_time
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE (((("date"("from_unixtime"(create_time)) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND (cb_option = 0)) 
AND ((NOT (extinfo.cancel_reason IN (1,2,3,200,201,204,302,303))) OR (extinfo.cancel_reason IS NULL)) ) AND (grass_region IN ('TH')))
) o3 ON (o3.orderid = o.local_orderid))
INNER JOIN (
SELECT DISTINCT
order_id
, order_sn
, grass_region
, cancel_user_id
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE (((( date(split(create_datetime,' ')[1]) ) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND (is_cb_shop = 0)) AND (grass_region = 'TH'))
AND grass_date>=(current_date - INTERVAL '90' DAY)
) o4 ON (o4.order_id = o3.orderid))
LEFT JOIN buyer_account ba ON ((ba.buyer_id = o4.cancel_user_id) AND (ba.grass_region = o4.grass_region)))
LEFT JOIN (
SELECT DISTINCT
orderid
, "json_extract_scalar"("from_utf8"(data), '$.Reason') reason
, "json_extract_scalar"("from_utf8"(data), '$.Operator') Operator
FROM
marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_daily_s0_live
WHERE ((grass_region = 'TH') AND (new_status = 7))
) oa ON (oa.orderid = o.local_orderid))
LEFT JOIN log l ON (l.orderid = o3.orderid))
-- WHERE (ba.buyer_id IS NULL)
) 
where seller_fault='N' and acl1_acl2='N'
-- WHERE (acl1_acl2 = 'N')