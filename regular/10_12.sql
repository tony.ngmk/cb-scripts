WITH
sip AS ( 
SELECT DISTINCT 
affi_shopid
, mst_shopid
, t1.country mst_country
, t2.country affi_country
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username 
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where grass_region IN ('SG','MY','PH','TH','VN','BR','MX','CO','CL','TW') AND sip_shop_status!=3 
) t2 ON (t1.shopid = t2.mst_shopid))
WHERE ((cb_option = 0) AND (T1.COUNTRY IN ('TW', 'MY', 'ID','VN','TH')))
) 

, OVERALL AS (

SELECT DISTINCT
grass_date
, grass_region
-- , SIP.AFFI_COUNTRY 
, "count"(DISTINCT SHOP_ID) shop_number
, "count"(DISTINCT item_id) sku_number
-- , CASE WHEN SIP.MST_SHOPID IS NULL THEN 'LOCAL' ELSE 'SIP' END AS SHOP_TYPE
FROM
(
SELECT DISTINCT
item_id
, shop_id
, grass_date
, grass_region
FROM
mp_item.dim_item__reg_s0_live
WHERE ((((grass_date = (current_date - INTERVAL '1' DAY)) AND (grass_region IN ('TW', 'MY', 'ID','VN','TH'))) AND (status = 1)) AND (stock > 0))
and seller_status=1 and shop_status=1 and (is_holiday_mode = 0 or is_holiday_mode is null) 
AND is_cb_shop=0 
and tz_type='local'
) I 

GROUP BY 1, 2
)
select distinct grass_date
, grass_region as mst_country 
, affi_country 
, shop_number 
, overall_sku_number-sku_number unmigrated_sku
, sku_number 
, overall_sku_number

from 
(SELECT DISTINCT
i.grass_date
, i.grass_region
, SIP.AFFI_COUNTRY 
, "count"(DISTINCT i.SHOP_ID) shop_number
, "sum"(sku ) sku_number
, CASE WHEN SIP.MST_SHOPID IS NULL THEN 'LOCAL' ELSE 'SIP' END AS SHOP_TYPE
, ov.sku_number as overall_sku_number 
FROM


(
SELECT DISTINCT
shop_id
, grass_date 
, grass_region 
, count(distinct item_id) sku 
FROM
mp_item.dim_item__reg_s0_live
WHERE ((((grass_date = (current_date - INTERVAL '1' DAY) ) AND (grass_region IN ('TW', 'MY', 'ID','VN','TH'))) AND (status = 1)) AND (stock > 0))
AND is_cb_shop=0 and shop_status=1 and seller_status= 1 and (is_holiday_mode = 0) and tz_type='local'
group by 1,2,3 
) I 
JOIN sip ON (sip.mst_shopid = i.shop_id)
JOIN OVERALL OV ON OV.grass_region=i.grass_region

GROUP BY 1, 2,3,6, 7 )