WITH
sip AS 
(
SELECT DISTINCT
grass_region
, affi_shopid
, mst_shopid
, b.country mst_country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON a.mst_shopid = b.shopid
WHERE b.cb_option = 0 AND a.grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL','ID','TW')
) 
, fees AS
(
SELECT DISTINCT
orderid
, cc_fee
, commission_fee
, service_fee
FROM regcbbi_others.shopee_bi_keyreports_local_sip_revenue_v3
WHERE lane IN ('IDPH', 'IDSG', 'IDMY','IDTH','IDVN','MYSG', 'TWSG', 'TWMY', 'TWID','IDBR','VNMY','IDMX','VNSG','VNPH','MYTW','IDTW','VNTW' )
)
, wh AS 
(
SELECT DISTINCT ordersn, wh_outbound_date 
FROM regcbbi_others.logistics_basic_info
WHERE wh_outbound_date >= CURRENT_DATE - INTERVAL '70' DAY
)
, o AS 
(
SELECT DISTINCT
"date"("from_unixtime"(create_timestamp)) create_timestamp
, order_id
, order_sn
, shop_id
--, gmv
--, gmv_usd
--, buyer_paid_shipping_fee
--, pv_rebate_by_shopee_amt
--, sv_rebate_by_shopee_amt
--, coin_used_cash_amt
--, sv_coin_earn_by_seller_amt
--, card_rebate_by_bank_amt
--, sv_rebate_by_seller_amt
--, pv_rebate_by_seller_amt
--, IF((actual_shipping_rebate_by_shopee_amt = 0), estimate_shipping_rebate_by_shopee_amt, actual_shipping_rebate_by_shopee_amt) shopee_shipping_rebate
--, actual_shipping_fee
, logistics_status_id
--, payment_method_id
, order_be_status_id
, order_fe_status_id
--, sv_promotion_id
--, card_rebate_by_shopee_amt
, is_cb_shop
, grass_region
--, pv_coin_earn_by_seller_amt
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE (((grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL','ID','TW')
) AND ("date"("from_unixtime"(create_timestamp)) >= DATE('2021-11-03'))))
and tz_type='local'
and grass_date>=date'2021-11-03'
) 
, oi AS (
SELECT DISTINCT
order_id
, order_sn
, is_cb_shop
, o.grass_region
--, "sum"(item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
--, "sum"(item_tax_amt) item_tax_amt
--, "sum"(commission_fee) commission_fee
--, "sum"(buyer_txn_fee) buyer_txn_fee
--, "sum"(seller_txn_fee) seller_txn_fee
--, "sum"(service_fee) service_fee
--, sum(item_amount*COALESCE(sum_hpfn,0)) AS hpfn
--, sum(item_amount*COALESCE(initial_hp,0)) AS initial_price
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
LEFT JOIN sip ON o.shop_id = sip.affi_shopid
LEFT JOIN (SELECT DISTINCT mst_country
, affi_country
, TRY_CAST(weight AS DOUBLE) weight
, TRY_CAST(hpfn AS DOUBLE) hpfn 
, TRY_CAST(initial_hp AS DOUBLE) initial_hp
, TRY_CAST(sum_hpfn AS DOUBLE) sum_hpfn
, TRY_CAST(fx AS DOUBLE) fx 
, TRY_CAST(country_margin AS DOUBLE) country_margin
FROM regcbbi_others.local_sip_price_config a 
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp FROM regcbbi_others.local_sip_price_config) b
ON a.ingestion_timestamp = b.ingestion_timestamp) cfg 
ON sip.grass_region = cfg.affi_country AND sip.mst_country = cfg.mst_country AND FLOOR(IF(item_weight_pp*1000 >= 20000,20000,item_weight_pp*1000)/10.0)*10 = cfg.weight
WHERE ((o.grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL','TW')
) AND ("date"("from_unixtime"(create_timestamp)) >= DATE('2021-11-03')))
and tz_type='local'
and grass_date>= date'2021-11-03'
and is_cb_shop=1 
GROUP BY 1, 2, 3, 4
) 
, rr AS (
SELECT DISTINCT
orderid
--, (refund_amount / DECIMAL '100000.00') refund_amount
, case when grass_region='TW' and reason in (2,3,103,105) then 'Y' 
when grass_region !='TW' and reason in (2,3,103,105,107) then 'Y' else 'N' end as is_nfr 

FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_continuous_s0_live
WHERE (((grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL','TW')

) AND ("date"("from_unixtime"(ctime)) >= DATE('2021-11-03'))) AND (refund_amount > 0)) AND status IN (2,5) and cb_option = 1 
and reason !=1 
) 
, ssv AS (SELECT distinct perc_ssv/100.00 AS perc_ssv,promo_id FROM regcbbi_others.sip_local_pnl_ssv)

, rbt AS (
SELECT DISTINCT
overseas_itemid
, overseas_modelid
, seller_negotiated_price_local_currency
, start_date_time
, end_date_time
FROM
regcbbi_others.sip_local_pnl_item_rebate
WHERE ((overseas_itemid > 0) AND (seller_negotiated_price_local_currency > 0))
) 
, rbt2 AS (
SELECT DISTINCT
orderid
, "sum"(rebates) rebate
FROM
(
SELECT DISTINCT
orderid
, itemid
, modelid
, ((seller_negotiated_price_local_currency * amount) - price) rebates
FROM
((
SELECT
order_id orderid
, item_id itemid
, model_id modelid
, item_amount amount
, item_price_pp price
, ("from_unixtime"(create_timestamp)) create_time
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (((grass_region IN ('SG', 'MY')) AND (is_cb_shop = 1)) AND ("date"("from_unixtime"(create_timestamp)) >= DATE('2021-11-03'))) and tz_type='local'
) p_oi
INNER JOIN rbt ON (((rbt.overseas_modelid = p_oi.modelid) AND (rbt.overseas_itemid = p_oi.itemid)) AND (p_oi.create_time BETWEEN start_date_time AND end_date_time)))
) 
GROUP BY 1
) 

SELECT DISTINCT
sip.grass_region as affi_country 
, mst_country
, wh_outbound_date
, COUNT(DISTINCT a.order_id) AS orders
, count(distinct case when rr.orderid is not null then a.order_id else null end) rr_orders 
, count(distinct case when rr.orderid is not null and rr.is_nfr='Y' then a.order_id else null end) nfr_rr_orders 
--, "sum"(a.gmv) gmv
--, sum(a.gmv_usd) gmv_usd
--, count(distinct CASE WHEN ((a.logistics_status_id = 6) AND (a.payment_method_id = 6)) then a.order_id else null end ) cod_failed_orders 

--, "sum"(IF(rr.refund_amount > 0,rr.refund_amount,0)) refund_amount
, COUNT(DISTINCT case when a.order_fe_status_id in (3,4) or a.logistics_status_id in (5,6,11) then a.order_id else null end ) AS complete_orders
--, "sum"((CASE WHEN ((a.logistics_status_id = 6) AND (a.payment_method_id = 6)) THEN a.gmv + ai.item_rebate_by_shopee_amt + a.pv_rebate_by_shopee_amt + a.coin_used_cash_amt + a.card_rebate_by_bank_amt + a.card_rebate_by_bank_amt - a.sv_coin_earn_by_seller_amt - a.pv_coin_earn_by_seller_amt - ai.item_tax_amt + a.shopee_shipping_rebate + fees.commission_fee + fees.cc_fee + fees.service_fee ELSE 0 END)) cod_fail

FROM
(

(
( 
(
(

(

(

SELECT *
FROM
o
WHERE is_cb_shop = 1 AND grass_region IN ('SG', 'MY', 'ID', 'PH','TH','VN','BR','MX','CO','CL','TW')
) a
INNER JOIN (
SELECT *
FROM
oi
WHERE is_cb_shop = 1 AND grass_region IN ('SG', 'MY', 'ID', 'PH','TH','VN','BR','MX','CO','CL','TW')
) ai ON a.order_id = ai.order_id

)
INNER JOIN sip ON a.shop_id = sip.affi_shopid

)
INNER JOIN (
select distinct oversea_orderid, local_orderid
from marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live 
) m ON a.order_id = m.oversea_orderid
) 
INNER JOIN (
SELECT *
FROM
o
WHERE (NOT order_be_status_id IN (1, 6, 8, 5, 7, 8, 16)) AND is_cb_shop = 0 AND grass_region IN ('ID', 'MY', 'TW','VN','TH')
) p ON m.local_orderid = p.order_id
) 
INNER JOIN wh ON a.order_sn = wh.ordersn
)

LEFT JOIN rr ON a.order_id = rr.orderid
)
GROUP BY 1, 2, 3


