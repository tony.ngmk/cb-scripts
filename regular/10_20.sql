with gp AS (SELECT DISTINCT 
(CASE WHEN LOWER(seller_type) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(seller_type) LIKE '%st%' THEN 'Short Tail'
WHEN LOWER(seller_type) LIKE '%mt%' THEN 'Mid Tail'
WHEN LOWER(seller_type) LIKE '%lt%' THEN 'Long Tail'
ELSE 'Others' END) AS team
, seller_name as ggp_account_name
, merchant_name as gp_account_name
, merchant_organization_name as gp_account_owner_userrole_name
, merchant_type as gp_account_seller_classification
, merchant_owner_email as gp_account_owner
, grass_region
, user_name as child_account_name
, shop_owner_email as child_account_owner
, shop_id child_shopid
FROM dev_regcbbi_general.cb_seller_profile_reg
WHERE merchant_name NOT LIKE '%SIP%' 
and grass_region in ('MY','BR','TW','SG')
-- AND gp_account_seller_classification <> 'Taobao Seller'
)


, s2 AS (SELECT DISTINCT shop_id shopid
,status shop_status
,user_status status
,is_holiday_mode holiday_mode_on
,shop_level1_category main_category 
,user_id
,user_name
from regcbbi_others.shop_profile
where grass_date= current_date-interval'1'day and is_cb_shop=1 and tz_type='local'
)

, shop AS (SELECT DISTINCT shopid
,main_category 
,user_id
,user_name
FROM s2 WHERE shop_status = 1 AND status = 1 AND holiday_mode_on = 0 
)

, sku AS (SELECT DISTINCT a.shop_id shopid
,a.item_id itemid
,concat('http://f.shopee.vn/file/',split(images,',')[1]) as image_link
,a.status item_status
,a.stock item_stock
,a.grass_region
,a.weight/100.00 AS item_weight 
FROM mp_item.dim_item__reg_s0_live a 
left join mp_item.dim_item_ext__reg_s0_live b on a.grass_date=b.grass_date and a.item_id=b.item_id
WHERE status NOT IN (0,4,5) 
and a.grass_date = current_date-interval'1'day 
and b.grass_date = current_date-interval'1'day 
and a.tz_type='local'
and b.tz_type='local'
)

, sku2 AS (SELECT DISTINCT shopid
, itemid
, image_link
,item_weight 
FROM sku 
WHERE item_status = 1 
AND item_stock > 0 
AND grass_region in ('TW','MY','BR'))

, orders AS (SELECT DISTINCT shop_id shopid 
,COUNT(DISTINCT order_id)/30.00 AS orders
,COUNT(DISTINCT CASE WHEN order_be_status_id in (1,2,3,4,11,12,13,14,15) THEN order_id ELSE NULL END)/30.00 AS net_order
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE 
grass_date>=current_date - interval '32' day
and date(from_unixtime(create_timestamp)) BETWEEN current_date - interval '30' day AND current_date - interval '1' day 
AND is_cb_shop = 1 
AND grass_region in ('TW','MY','BR') 
and tz_type='local'
GROUP BY 1)

, sip AS (SELECT DISTINCT mst_shopid
, COUNT(CASE WHEN grass_region = 'SG' THEN affi_shopid ELSE NULL END) AS sg_open
, COUNT(CASE WHEN grass_region = 'MY' THEN affi_shopid ELSE NULL END) AS my_open
, COUNT(CASE WHEN grass_region = 'ID' THEN affi_shopid ELSE NULL END) AS id_open
, COUNT(CASE WHEN grass_region = 'TH' THEN affi_shopid ELSE NULL END) AS th_open
, COUNT(CASE WHEN grass_region = 'PH' THEN affi_shopid ELSE NULL END) AS ph_open
, COUNT(CASE WHEN grass_region = 'VN' THEN affi_shopid ELSE NULL END) AS vn_open
, COUNT(CASE WHEN grass_region = 'TW' THEN affi_shopid ELSE NULL END) AS tw_open
, COUNT(CASE WHEN grass_region = 'BR' THEN affi_shopid ELSE NULL END) AS br_open
, COUNT(CASE WHEN grass_region = 'MX' THEN affi_shopid ELSE NULL END) AS MX_open 
, COUNT(CASE WHEN grass_region = 'CO' THEN affi_shopid ELSE NULL END) AS CO_open 
, COUNT(CASE WHEN grass_region = 'CL' THEN affi_shopid ELSE NULL END) AS CL_open 
, COUNT(CASE WHEN grass_region = 'PL' THEN affi_shopid ELSE NULL END) AS PL_open 
, COUNT(CASE WHEN grass_region = 'ES' THEN affi_shopid ELSE NULL END) AS ES_open 
, COUNT(CASE WHEN grass_region = 'FR' THEN affi_shopid ELSE NULL END) AS FR_open 


FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status!=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
GROUP BY 1)

, a_hm AS (SELECT DISTINCT a.mst_shopid
FROM (SELECT DISTINCT b.affi_shopid
, b.mst_shopid
, b.country AS affi_country
, a.country AS mst_country
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a 
JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b on b.mst_shopid = a.shopid 
WHERE a.cb_option = 1 AND A.country IN ('TW','MY','BR') 
and sip_shop_status!=3
and b.grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')

) a
-- ashop turned on HM
JOIN (SELECT DISTINCT u1.shop_id shopid
from (select distinct shop_id 
from mp_user.dim_user__reg_s0_live 
where status=1 and grass_date=current_date-interval'1'day ) u1 
join (select distinct shop_id 
from mp_user.dim_shop__reg_s0_live 
where status=1 and is_holiday_mode=1 and is_cb_shop=1 and grass_date=current_date-interval'1'day ) u2 on u2.shop_id = u1.shop_id 
) b
ON a.affi_shopid = b.shopid)

, cancel AS (SELECT DISTINCT shopid
, COUNT(DISTINCT CASE WHEN (grass_region = 'TW' AND extinfo.cancel_reason IN (1,2,3,200,201,204,302,303)) THEN orderid 
WHEN (grass_region != 'TW' AND extinfo.cancel_reason IN (1,2,3,4,204,301,302,303)) THEN orderid ELSE NULL END)/30.00 AS cancel_order
FROM marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE grass_region IN ('TW','MY','BR') AND date(from_unixtime(cancel_time)) BETWEEN current_date - interval '30' day AND current_date - interval '1' day
and cb_option=1 
GROUP BY 1)

, rr AS (SELECT DISTINCT shopid
, COUNT(DISTINCT CASE WHEN (grass_region = 'TW' AND reason IN (1,2,3,103,105) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2,5) ) THEN a.orderid
WHEN (grass_region != 'TW' AND reason IN (1,2,3,103,105,107) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2,5)) THEN a.orderid
ELSE NULL END)/30.00 AS RR_order
FROM (SELECT DISTINCT shopid,grass_region,reason,status,orderid 
FROM marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE grass_region IN ('TW','MY','BR') AND DATE(FROM_UNIXTIME(mtime)) BETWEEN current_date - interval '30' day AND current_date - interval '1' day
and cb_option=1 
) a
JOIN (SELECT DISTINCT orderid,extinfo.cancel_reason 
FROM marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live 
WHERE grass_region IN ('TW','MY','BR') and cb_option=1 ) b ON a.orderid = b.orderid
GROUP BY 1)

, whs AS (SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_id_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_th_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_my_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_ph_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_tw_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
marketplace.shopee_logistic_br_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_mx_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1)) 
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_co_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_cl_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_sg_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1))
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_pl_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1)) 
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_es_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1)) 
UNION ALL SELECT DISTINCT shop_id
FROM
sls_mart.shopee_logistic_fr_db__shop_warehouse_mapping_tab__reg_daily_s0_live
WHERE ((service = 'WMS') AND (status = 1)) 


-- after the table is ready
-- UNION ALL SELECT DISTINCT shop_id
-- FROM
-- Shopee.shopee_logistic_PL_db__shop_warehouse_mapping_tab
-- WHERE ((service = 'WMS') AND (status = 1)) 

)
/*
, heavy AS (SELECT DISTINCT CAST(shopid AS BIGINT) shopid FROM shopee.shopee_regional_cb_team__heavy_rate_shops
where ingestion_timestamp = (select max(ingestion_timestamp) max_time
from shopee.shopee_regional_cb_team__heavy_rate_shops)
)*/
, sub AS (SELECT DISTINCT shop_id,main_account_id 
FROM marketplace.shopee_subaccount_db__account_shop_tab__reg_daily_s0_live s 
JOIN(SELECT DISTINCT account_id, account_type,status 
FROM marketplace.shopee_subaccount_db__account_tab__reg_daily_s0_live WHERE status=1 AND account_type=1) s2 on s2.account_id=s.main_account_id
WHERE s.status = 1 AND country IN ('TW','MY','BR'))

, target AS (SELECT DISTINCT 

a.*
, main_category
, COALESCE(orders,0) orders 
, COALESCE(net_order,0) net_order
, COALESCE(cancel_order,0) cancel_order
, COALESCE(RR_order,0) RR_order
, COALESCE(cancel_order,0)/CAST((COALESCE(net_order,0)+COALESCE(cancel_order,0)+COALESCE(RR_order,0)) AS DOUBLE) AS cancel_rate
,shop.user_name
,shop.user_id
FROM (SELECT DISTINCT grass_region m_country, gp.* FROM gp WHERE grass_region IN ('TW','MY','BR') ) a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN sub ON a.child_shopid = sub.shop_id 
LEFT JOIN orders ON a.child_shopid = orders.shopid
LEFT JOIN cancel ON a.child_shopid = cancel.shopid
LEFT JOIN rr ON a.child_shopid = rr.shopid
LEFT JOIN whs ON a.child_shopid = whs.shop_id 
-- LEFT JOIN heavy ON a.child_shopid = heavy.shopid
LEFT JOIN a_hm ON a.child_shopid = a_hm.mst_shopid
-- cnsc check 1 
left join (select distinct shop_id 
from marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live 
where shop_type=3) cnsc1 on cnsc1.shop_id = a.child_shopid 
-- cnsc check 2 shopee_regional_cb_team__cnsc_pre_rollout_shop 
left join (Select distinct cast(shopid as BIGINT) shopid 
from regcbbi_others.shopee_regional_cb_team__cnsc_pre_rollout_shop) cnsc2 on cnsc2.shopid= a.child_shopid 
WHERE orders.orders >= 0.5 AND whs.shop_id IS NULL --AND heavy.shopid IS NULL
AND a_hm.mst_shopid IS NULL
AND COALESCE(cancel_order,0)/CAST((COALESCE(net_order,0)+COALESCE(cancel_order,0)+COALESCE(RR_order,0)) AS DOUBLE) <= 0.05
and cnsc1.shop_id is null 
and cnsc2.shopid is null 
)

, sg AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'SG') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'SG') b ON a.child_shopid = b.shopid)

, id AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'ID') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'ID') b ON a.child_shopid = b.shopid)

, th AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'TH') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'TH') b ON a.child_shopid = b.shopid)

, ph AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'PH') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'PH') b ON a.child_shopid = b.shopid)

, vn AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'VN') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'VN') b ON a.child_shopid = b.shopid)

, tw AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'TW') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'TW') b ON a.child_shopid = b.shopid)

, br AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'BR') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'BR') b ON a.child_shopid = b.shopid) 

, MX AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'MX') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'MX') b ON a.child_shopid = b.shopid) 

, CO AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'CO') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'CO') b ON a.child_shopid = b.shopid) 

, CL AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'CL') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'CL') b ON a.child_shopid = b.shopid) 

, PL AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'PL') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'PL') b ON a.child_shopid = b.shopid) 
, my AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'MY') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'MY') b ON a.child_shopid = b.shopid)

, ES AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'ES') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'ES') b ON a.child_shopid = b.shopid)
, FR AS (SELECT DISTINCT gp_account_name
, image_link
FROM (SELECT DISTINCT gp_account_name,child_shopid FROM gp WHERE grass_region = 'FR') a 
JOIN shop ON a.child_shopid = shop.shopid
JOIN (SELECT DISTINCT * FROM sku WHERE grass_region = 'FR') b ON a.child_shopid = b.shopid) 



, r1 AS (SELECT DISTINCT target.*
, sku2.itemid
, sku2.item_weight
, sku2.image_link
, sg.image_link AS sg_image_link
, id.image_link AS id_image_link
, th.image_link AS th_image_link
, ph.image_link AS ph_image_link
, vn.image_link AS vn_image_link
, tw.image_link AS tw_image_link
, br.image_link AS br_image_link
, MX.image_link AS MX_image_link
, CO.image_link AS CO_image_link
, CL.image_link AS CL_image_link
, PL.image_link AS PL_image_link
, my.image_link AS my_image_link 

, ES.image_link AS ES_image_link
, FR.image_link AS FR_image_link 

FROM target
JOIN sku2 ON target.child_shopid = sku2.shopid
LEFT JOIN sg ON target.gp_account_name = sg.gp_account_name AND sku2.image_link = sg.image_link
LEFT JOIN id ON target.gp_account_name = id.gp_account_name AND sku2.image_link = id.image_link
LEFT JOIN th ON target.gp_account_name = th.gp_account_name AND sku2.image_link = th.image_link
LEFT JOIN ph ON target.gp_account_name = ph.gp_account_name AND sku2.image_link = ph.image_link
LEFT JOIN vn ON target.gp_account_name = vn.gp_account_name AND sku2.image_link = vn.image_link
LEFT JOIN tw ON target.gp_account_name = tw.gp_account_name AND sku2.image_link = tw.image_link
LEFT JOIN br ON target.gp_account_name = br.gp_account_name AND sku2.image_link = br.image_link
LEFT JOIN MX ON target.gp_account_name = MX.gp_account_name AND sku2.image_link = MX.image_link 
LEFT JOIN CO ON target.gp_account_name = CO.gp_account_name AND sku2.image_link = CO.image_link 
LEFT JOIN CL ON target.gp_account_name = CL.gp_account_name AND sku2.image_link = CL.image_link 
LEFT JOIN PL ON target.gp_account_name = PL.gp_account_name AND sku2.image_link = PL.image_link
LEFT JOIN MY ON target.gp_account_name = MY.gp_account_name AND sku2.image_link = MY.image_link 
LEFT JOIN ES ON target.gp_account_name = ES.gp_account_name AND sku2.image_link = ES.image_link 
LEFT JOIN FR ON target.gp_account_name = FR.gp_account_name AND sku2.image_link = FR.image_link 
)

, r2 AS (SELECT DISTINCT 
r1.m_country
,team
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, child_account_name
, child_account_owner
, child_shopid 
, main_category
, COALESCE(orders,0) orders 
, COALESCE(net_order,0) net_order
, COALESCE(cancel_order,0) cancel_order
, COALESCE(RR_order,0) RR_order
, cancel_rate 
, COUNT(DISTINCT itemid) AS total_sku
, COUNT(DISTINCT CASE WHEN sg_image_link IS NULL THEN itemid ELSE NULL END) AS sg_sku
, COUNT(DISTINCT CASE WHEN id_image_link IS NULL THEN itemid ELSE NULL END) AS id_sku
, COUNT(DISTINCT CASE WHEN th_image_link IS NULL THEN itemid ELSE NULL END) AS th_sku
, COUNT(DISTINCT CASE WHEN ph_image_link IS NULL THEN itemid ELSE NULL END) AS ph_sku
, COUNT(DISTINCT CASE WHEN vn_image_link IS NULL THEN itemid ELSE NULL END) AS vn_sku
, COUNT(DISTINCT CASE WHEN tw_image_link IS NULL THEN itemid ELSE NULL END) AS tw_sku
, COUNT(DISTINCT CASE WHEN br_image_link IS NULL THEN itemid ELSE NULL END) AS br_sku
, COUNT(DISTINCT CASE WHEN MX_image_link IS NULL THEN itemid ELSE NULL END) AS MX_sku 
, COUNT(DISTINCT CASE WHEN CO_image_link IS NULL THEN itemid ELSE NULL END) AS CO_sku 
, COUNT(DISTINCT CASE WHEN CL_image_link IS NULL THEN itemid ELSE NULL END) AS CL_sku 
, COUNT(DISTINCT CASE WHEN PL_image_link IS NULL THEN itemid ELSE NULL END) AS PL_sku
, COUNT(DISTINCT CASE WHEN MY_image_link IS NULL THEN itemid ELSE NULL END) AS MY_sku 
, COUNT(DISTINCT CASE WHEN ES_image_link IS NULL THEN itemid ELSE NULL END) AS ES_sku
, COUNT(DISTINCT CASE WHEN FR_image_link IS NULL THEN itemid ELSE NULL END) AS FR_sku 

, AVG(CASE WHEN tw_image_link IS NULL THEN item_weight ELSE NULL END) AS tw_weight
,user_name
,user_id
, sg_open
, MY_open
, id_open
, th_open
, ph_open
, vn_open
, tw_open
, br_open
, MX_open
, CO_open
, CL_open
, PL_open 
, ES_open 
, FR_open 

FROM r1
LEFT JOIN sip ON r1.child_shopid = sip.mst_shopid

GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48)


SELECT current_date update_time 
, m_country
,team
, sum(sg_sku) sg_sku 
, sum(my_sku) my_sku 
, sum(id_sku) id_sku 
, sum(ph_sku) ph_sku 
, sum(th_sku) th_sku 
, sum(vn_sku) vn_sku 
, sum(tw_sku) tw_sku 
, sum(br_sku) br_sku 
, sum(mx_sku) mx_sku 
, sum(co_sku) co_sku 
, sum(cl_sku) cl_sku 
, sum(pl_sku) pl_sku 
, sum(es_sku) es_sku 
, sum(fr_sku) fr_sku 

FROM
(SELECT DISTINCT 
m_country
,team
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, child_account_name
, child_account_owner
, r2.child_shopid 
, main_category
, orders 
, net_order
, cancel_order
, RR_order
, cancel_rate 
, total_sku
, sg_sku
, my_sku 
, id_sku
, th_sku
, ph_sku
, vn_sku
, tw_sku
, br_sku
, MX_sku 
, CO_sku 
, CL_sku 
, PL_sku
, ES_SKU 
, FR_SKU 
, tw_weight
,user_name
,user_id
, case when sg_sku >=50 and (sg_open = 0 OR sg_open IS NULL) then 1 else 0 end SG 
, case when id_sku >=50 and (id_open = 0 OR id_open IS NULL) then 1 else 0 end ID 
, case when th_sku >=50 and (th_open = 0 OR th_open IS NULL) then 1 else 0 end TH 
, case when ph_sku >=50 and (ph_open = 0 OR ph_open IS NULL) 
then 1 else 0 end PH 
, case when vn_sku >=50 and (vn_open = 0 OR vn_open IS NULL)
--AND m_country = 'MY' 
then 1 else 0 end VN 
, case when tw_sku >=50 and (tw_open = 0 OR tw_open IS NULL) 
--AND m_country = 'MY' 
then 1 else 0 end TW 
, case when br_sku >=50 and (br_open = 0 OR br_open IS NULL) 
--AND m_country = 'MY' 
AND main_category NOT IN ('Women Clothes','Women Shoes','Muslim Fashion') then 1 else 0 end BR 
, case when mx_sku >=50 and (mx_open = 0 OR mx_open IS NULL) AND main_category NOT IN ('Women Clothes','Women Shoes','Muslim Fashion') then 1 else 0 end MX 
, case when co_sku >=50 and (co_open = 0 OR co_open IS NULL) AND main_category NOT IN ('Women Clothes','Women Shoes','Muslim Fashion') then 1 else 0 end CO 
, case when cl_sku >=50 and (cl_open = 0 OR cl_open IS NULL) and main_category NOT IN ('Women Clothes','Women Shoes','Muslim Fashion') then 1 else 0 end CL 
, case when PL_sku >=50 and (PL_open = 0 OR PL_open IS NULL) then 1 else 0 end PL 
, case when my_sku >=50 and (my_open = 0 OR my_open IS NULL) then 1 else 0 end MY
, case when ES_sku >=50 and (PL_open = 0 OR ES_open IS NULL) then 1 else 0 end ES 
, case when FR_sku >=50 and (my_open = 0 OR FR_open IS NULL) then 1 else 0 end FR 

FROM r2 

WHERE ((sg_sku >= 50 AND (sg_open = 0 OR sg_open IS NULL)) 
OR (id_sku >= 50 AND (id_open = 0 OR id_open IS NULL))
OR (th_sku >= 50 AND (th_open = 0 OR th_open IS NULL))
OR (ph_sku >= 50 AND (ph_open = 0 OR ph_open IS NULL))
OR (vn_sku >= 50 AND (vn_open = 0 OR vn_open IS NULL))
OR (tw_sku >= 50 AND (tw_open = 0 OR tw_open IS NULL))
OR (br_sku >= 50 AND (br_open = 0 OR br_open IS NULL))
OR (MX_sku >= 50 AND (MX_open = 0 OR MX_open IS NULL))
OR (CO_sku >= 50 AND (CO_open = 0 OR CO_open IS NULL))
OR (CL_sku >= 50 AND (CL_open = 0 OR CL_open IS NULL))
OR (PL_sku >= 50 AND (PL_open = 0 OR PL_open IS NULL))
OR (MY_sku >= 50 AND (MY_open = 0 OR MY_open IS NULL)) 
OR (ES_sku >= 50 AND (PL_open = 0 OR ES_open IS NULL))
OR (FR_sku >= 50 AND (MY_open = 0 OR FR_open IS NULL)) 


) and team <> 'Others')
WHERE (case when m_country='TW' and SG+ID+TH+PH+VN+BR+MY>0 then true 
when m_country='MY' and SG+ID+TH+PH+VN+TW+BR+MX+CO+CL+PL+MY+ES+FR >0 then true 
when m_country='BR' and MX+CO+CL+PL+ES+FR >0 then true else false end) = true 

group by 1,2,3