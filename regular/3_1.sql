WITH dim AS (
SELECT DISTINCT 
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE (NOT (grass_region IN ('TW'))) 
AND grass_date >= current_date-interval'90'day
) 
, sip AS (
SELECT DISTINCT
affi_shopid
, country
, mst_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT shopid
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE cb_option = 1
AND country = 'TW'
) b 
ON a.mst_shopid = b.shopid
WHERE grass_region in ('SG','MY','ID','TH','PH','VN','BR','MX')
AND sip_shop_status!=3 
) 
, o AS (
SELECT 
shop_id 
, order_id 
, order_be_status_id
, create_datetime
, grass_region
, cancel_timestamp as cancel_time 
, date(split(create_datetime,' ')[1]) create_time 
, create_timestamp
, cancel_reason_id cancel_reason
, is_cb_shop 
, cancel_user_id
, order_sn 
, is_net_order
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE date(from_unixtime(create_timestamp)) >= current_date-interval'100'day
AND tz_type = 'local' 
AND grass_region IN ('MY', 'SG', 'BR', 'PH','TH','VN','MX','ID','TW')
AND grass_date >= current_date-interval'100'day 
AND is_cb_shop=1 
)

, seller AS (
SELECT DISTINCT
child_shopid
, CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%lt%' THEN 'Long Tail'
ELSE 'Others' END seller_type
FROM regbd_sf.cb__seller_index_tab
WHERE grass_region IN ('TW')
) 
, net AS (
SELECT DISTINCT
shop_id
, create_time
, count(DISTINCT order_id) gross_order
, count(DISTINCT (CASE WHEN is_net_order = 1 THEN order_id ELSE null END)) net_order
FROM o 
WHERE grass_region in ('SG','MY','ID','TH','PH','VN','BR','MX') 
GROUP BY 1, 2
) 
, cancel AS (
SELECT DISTINCT
shop_id shopid
, CASE WHEN grass_region in ('SG','MY','PH','TW') THEN date(from_unixtime(cancel_time)) 
WHEN grass_region in ('TH','ID','VN') THEN date(from_unixtime(cancel_time) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(cancel_time)-interval'11'hour) 
END AS cancel_time
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND cancel_reason IN (1, 2, 3, 200, 201, 204, 302 ,303) THEN order_id 
WHEN grass_region NOT IN ('TW') AND cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) THEN order_id 
ELSE null END)) cancel_order
FROM o 
WHERE date(from_unixtime(cancel_time)) >= current_date-interval'90'day
AND grass_region in ('SG','MY','ID','TH','PH','VN','BR','MX') 
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
shopid
, CASE WHEN grass_region in ('SG','MY','PH','TW') THEN date(from_unixtime(mtime)) 
WHEN grass_region in ('TH','ID','VN') THEN date(from_unixtime(mtime) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime)-interval'11'hour) 
END AS mtime
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND reason IN (1, 2, 3, 103, 105) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
WHEN grass_region <> 'TW' AND reason IN (1, 2, 3, 103, 105, 107) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
ELSE null END)) RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
order_id orderid
, cancel_reason
FROM o
WHERE grass_region in ('SG','MY','ID','TH','PH','VN','BR','MX')
) b 
ON a.orderid = b.orderid
WHERE date(from_unixtime(mtime)) >= current_date-interval'90'day
AND grass_region in ('SG','MY','ID','TH','PH','VN','BR','MX')
AND cb_option=1 
AND date(from_unixtime(ctime)) >= current_date-interval'90'day
GROUP BY 1, 2
) 
SELECT DISTINCT
dim.grass_date
, COALESCE(seller_type, '5.Others') seller_type
-- , sip.country
, sum(gross_order) gross_order
, sum(net_order) net_order
, sum(cancel_order) cancel_order
, sum(RR_order) RR_order
FROM dim
INNER JOIN sip 
ON dim.grass_region = sip.country
LEFT JOIN net 
ON sip.affi_shopid = net.shop_id
AND dim.grass_date = net.create_time
LEFT JOIN cancel 
ON sip.affi_shopid = cancel.shopid
AND dim.grass_date = cancel.cancel_time
LEFT JOIN rr 
ON sip.affi_shopid = rr.shopid
AND dim.grass_date = rr.mtime
LEFT JOIN seller 
ON sip.mst_shopid = CAST(seller.child_shopid AS int)
GROUP BY 1, 2