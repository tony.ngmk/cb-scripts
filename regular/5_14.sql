WITH raw AS (SELECT DISTINCT

a.ordersn

, a.orderid

, total_price

, shopee_voucher_rebate

, shopee_card_rebate

, shopee_coin_rebate

, shopee_item_rebate

, seller_voucher_rebate

, bank_card_rebate

, shopee_actual_shipping_rebate

, actual_shipping_fee

, buyer_shipping_fee

, affi_hpfn

, mst_hpfs

, mst_hpfn

, mst_price

, price_ratio

, affi_exch_rate

, mst_exch_rate

, (CASE WHEN (grass_region = 'ID') THEN 1680 WHEN (grass_region = 'MY') THEN DECIMAL '0.3' WHEN (grass_region = 'PH') THEN DECIMAL '8.4' WHEN (grass_region = 'SG') THEN DECIMAL '0.16' WHEN (grass_region = 'TH') THEN DECIMAL '2.8'
WHEN (grass_region = 'VN') THEN 1260 WHEN grass_region = 'BR' THEN 2.8 ELSE 0 END) bias

, ((((((total_price + shopee_voucher_rebate) + shopee_card_rebate) + shopee_coin_rebate) + shopee_item_rebate) + bank_card_rebate) + shopee_actual_shipping_rebate) NMV

, grass_region

, COALESCE(system_settlement, 0) system_settlement
, settlement_amount
, actual_amount
, IF(((order_remark = 0) OR (order_remark IS NULL)), order_remark) remark
, c.country AS mst_country
FROM

((((((SELECT DISTINCT

order_sn ordersn

, order_id orderid

, gmv total_price

, pv_rebate_by_shopee_amt shopee_voucher_rebate

, card_rebate_by_shopee_amt shopee_card_rebate

, coin_used_cash_amt shopee_coin_rebate

, sv_rebate_by_seller_amt seller_voucher_rebate

, card_rebate_by_bank_amt bank_card_rebate

, COALESCE(actual_shipping_rebate_by_shopee_amt, 0) shopee_actual_shipping_rebate

, shop_id shopid

, actual_shipping_fee

, buyer_paid_shipping_fee buyer_shipping_fee

, grass_region

FROM

mp_order.dwd_order_all_ent_df__reg_s0_live

WHERE (("date"(FROM_UNIXTIME(create_timestamp)) >= current_date-interval'150'day )) AND is_cb_shop = 1
and grass_date >=current_date-interval'150'day 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')

) a
JOIN (SELECT DISTINCT order_id orderid 
,SUM(item_rebate_by_seller_amt) AS seller_item_rebate
,SUM(item_rebate_by_shopee_amt) shopee_item_rebate
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE is_cb_shop = 1 AND DATE(FROM_UNIXTIME(create_timestamp)) >= current_date-interval'150'day
and tz_type='local' and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')
GROUP BY 1) oi ON a.orderid = oi.orderid

INNER JOIN (

SELECT DISTINCT

affi_shopid

, mst_shopid

FROM

marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')
) b ON (a.shopid = b.affi_shopid))

INNER JOIN (

SELECT DISTINCT shopid,country

FROM

marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live

WHERE (cb_option = 1) and country in ('BR','TW','MY')
) c ON (b.mst_shopid = c.shopid))

INNER JOIN (

SELECT
a.orderid
, COALESCE(try_cast(actual_amount AS double), 0) actual_amount
, "sum"(((affi_hpfn / DECIMAL '100000.00') * quantity)) affi_hpfn
, "sum"((IF((mst_promo_price = -1), (mst_orig_price / DECIMAL '100000.00'), (mst_promo_price / DECIMAL '100000.00')) * quantity)) mst_price
, "sum"(((mst_hpfn / 100000) * quantity)) mst_hpfn
, "sum"(((mst_hpfs / 100000) * quantity)) mst_hpfs
, "sum"(0) system_settlement
, "max"(c.kind) order_remark
FROM

(marketplace.shopee_sip_db__sip_order_item_snapshot_tab__reg_daily_s0_live a
LEFT JOIN (
SELECT DISTINCT
orderid
, MAX(kind) kind
, MIN(actual_amount) AS actual_amount
FROM (
SELECT *, (CASE WHEN (remark = 'SIP Bundle Deal Order') THEN 4 WHEN (remark = 'SIP Campaign Order') THEN 5 WHEN (remark = 'SIP offline Adjustment') THEN 6
WHEN (remark = 'SIP RR Confirmed') THEN 7 WHEN (remark = 'SIP Voucher Adjustment') THEN 8 ELSE 0 END) kind

FROM regcbbi_others.shopee_regional_cb_team__sip_offline_adj_order_level
WHERE REMARK='SIP RR Confirmed'
) GROUP BY 1
) c ON (try_cast(a.orderid AS VARCHAR )= c.orderid))

GROUP BY 1, 2

) d ON (a.orderid = d.orderid))

INNER JOIN (

SELECT DISTINCT

orderid

, (try_cast("json_extract_scalar"(extinfo, '$.price_ratio') AS int) / DECIMAL '100000.00') price_ratio

, try_cast("json_extract_scalar"(extinfo, '$.affi_exch_rate') AS double) affi_exch_rate

, try_cast("json_extract_scalar"(extinfo, '$.mst_exch_rate') AS double) mst_exch_rate

, (settlement_amount / DECIMAL '100000.00') settlement_amount

FROM

marketplace.shopee_sip_db__sip_order_tab__reg_daily_s0_live
where affi_country in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')
) e ON (d.orderid = e.orderid))))



, t3 AS (SELECT DISTINCT raw.ordersn

, (settlement_amount) order_system_settlement

, (actual_amount) order_adjust_settlement

, grass_region

, mst_country

, wh_outbound_date

FROM

raw

JOIN (
select ordersn 
, wh_outbound_date 
from regcbbi_others.logistics_basic_info
where grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES') and date(wh_outbound_date) >= current_date-interval'150'day 
) b ON raw.ordersn = b.ordersn

)



SELECT DISTINCT COUNT(DISTINCT ordersn) AS orders

, wh_outbound_date


, SUM(order_system_settlement) AS order_system_settlement

-- , SUM(order_adjust_settlement) AS order_adjust_settlement



, grass_region

, mst_country

FROM t3

GROUP BY 2,4,5