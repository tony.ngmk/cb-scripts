WITH dim AS ( 
SELECT DISTINCT 
grass_date 
, grass_region 
FROM 
mp_order.dim_exchange_rate__reg_s0_live
WHERE grass_date >= current_date-interval'100'day
AND grass_region in ('MY', 'SG', 'ID', 'PH', 'TH', 'VN', 'BR','MX','CO','CL','TW')
) 
, buyer_account AS (
SELECT buyer_id , grass_region 
FROM (
VALUES 
ROW (32352918, 'TW')
, ROW (174331327, 'MY')
, ROW (216124110, 'ID')
, ROW (413283389,'VN')
, ROW (502225957,'TH')
) t (buyer_id, grass_region)
) 
, sip AS (
SELECT DISTINCT
a.affi_shopid
, a.country affi_country
, b.country mst_country
, mst_shopid
--, CASE WHEN f.affi_shopid IS NOT NULL THEN 'Y' ELSE 'N' END focus
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE cb_option = 0
AND country IN ('TW', 'ID', 'MY','VN','TH')
) b
ON a.mst_shopid = b.shopid
-- LEFT JOIN (
-- SELECT DISTINCT CAST(affi_shopid AS bigint) affi_shopid
-- FROM
-- regcbbi_others.shopee_regional_cb_team__localsip_focus_shop
-- WHERE ingestion_timestamp = (SELECT max(ingestion_timestamp) col FROM regcbbi_others.shopee_regional_cb_team__localsip_focus_shop)
-- ) f
-- ON f.affi_shopid = a.affi_shopid
WHERE a.country IN ('MY', 'SG', 'ID', 'PH', 'TH', 'VN', 'BR','MX','CO','CL','TW')
AND a.grass_region IN ('MY', 'SG', 'ID', 'PH', 'TH', 'VN', 'BR','MX','CO','CL','TW') 
AND sip_shop_status!= 3 
) 
, raw AS (
SELECT DISTINCT
oversea_orderid
, max(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE date(from_unixtime(ctime)) >= current_date-interval'100'day 
AND local_country IN ('tw','my','id','vn','th') 
AND oversea_country in ('my','sg','vn','ph','th','br','mx','co','cl','tw')
GROUP BY 1
) 
, net AS (
SELECT DISTINCT
shop_id
, date(split(create_datetime,' ')[1]) create_time
, count(DISTINCT order_id) gross_order
, count(DISTINCT (CASE WHEN is_net_order = 1 THEN order_id ELSE null END)) net_order
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE date(from_unixtime(create_timestamp)) >= current_date-interval'100'day
AND tz_type = 'local' 
AND grass_region IN ('MY', 'SG', 'ID', 'PH', 'TH', 'VN', 'BR','MX','CO','CL','TW')
AND is_cb_shop=1 
AND grass_date >= current_date-interval'100'day 
GROUP BY 1, 2
) 
, o as (
SELECT DISTINCT
shop_id shopid
, create_datetime
, order_id orderid 
, cancel_reason_id cancel_reason 
, cancel_timestamp cancel_time 
, cancel_datetime
, order_be_status_id 
, grass_region
, is_cb_shop 
, cancel_user_id
-- , date(split(create_datetime,' ')[1]) as create_time
-- , count(DISTINCT order_id) gross_order
-- , count(DISTINCT (CASE WHEN (order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15)) THEN order_id ELSE null END)) net_order

FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE date(split(create_datetime,' ')[1]) >= current_date-interval'150'day
AND tz_type='local' 
--and is_cb_shop=1
AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL') 
AND grass_date >= current_date-interval'150'day 
-- GROUP BY 1, 2
)
, cancel AS (
SELECT DISTINCT
o1.shopid
, CASE WHEN o1.grass_region IN ('SG', 'MY', 'PH', 'TW') THEN date(from_unixtime(o1.cancel_time)) 
WHEN o1.grass_region IN ('TH', 'ID', 'VN') THEN date((from_unixtime(o1.cancel_time) - INTERVAL '1' HOUR))
WHEN o1.grass_region = 'BR' THEN date((from_unixtime(o1.cancel_time) - INTERVAL '11' HOUR)) 
WHEN o1.grass_region = 'MX' THEN date((from_unixtime(o1.cancel_time) - INTERVAL '14' HOUR)) 
WHEN o1.grass_region = 'CL' THEN date(CAST(format_datetime(from_unixtime(o1.cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN o1.grass_region = 'CO' THEN date(CAST(format_datetime(from_unixtime(o1.cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
END cancel_time
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) THEN o1.orderid
WHEN (NOT (o1.grass_region IN ('TW'))) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) THEN o1.orderid 
ELSE null END)) cancel_order
, count(DISTINCT (CASE WHEN o1.grass_region <> 'TW' AND o2.grass_region = 'TW' AND o2.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid 
WHEN o1.grass_region = 'TW'AND o2.grass_region <> 'TW' AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) AND o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid
WHEN (NOT (o1.grass_region IN ('TW'))) AND o2.grass_region <> 'TW' AND o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid
ELSE null END)) cancel_order_v2 --- both p and a are nfr cancellation and sync success
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) AND local_orderid > 0 THEN o1.orderid 
WHEN (NOT (o1.grass_region IN ('TW'))) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid 
ELSE null END)) cancel_order_sync_suc
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND ((NOT (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) OR (o2.cancel_reason IS NULL)) AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) AND local_orderid > 0 THEN o1.orderid 
WHEN (NOT (o1.grass_region IN ('TW'))) AND o2.grass_region IN ('TW') AND ((NOT (o2.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303))) OR o2.cancel_reason IS NULL) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid 
WHEN (NOT (o1.grass_region IN ('TW'))) AND (NOT (o2.grass_region IN ('TW'))) AND ((NOT (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) OR o2.cancel_reason IS NULL) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid 
ELSE null END)) sync_failed_acl1_acl2
---- a order is nfr cancellation p order is not 
, count(DISTINCT (CASE WHEN o1.cancel_reason = 204 AND l.ar_time IS NOT NULL THEN o1.orderid 
WHEN o1.cancel_reason IN (302,303) AND l.pu_time IS NOT NULL THEN o1.orderid 
ELSE null END)) acl_order
----- a is nfr but p has arrange ship 
, count(DISTINCT (CASE WHEN o1.cancel_reason IN (204, 302,303) AND ba.buyer_id IS NOT NULL AND local_orderid > 0 THEN o1.orderid ELSE null END)) seller_fault_v2 
----- a order is auto cancellation but by shopee account -- sync successful
FROM raw 
LEFT JOIN (
SELECT DISTINCT 
orderid
, cancel_time
, cancel_reason
, shopid
, grass_region
FROM o
WHERE is_cb_shop = 1
AND grass_region IN ('MY', 'SG', 'PH', 'TH', 'VN', 'BR','MX','CO','CL','TW')
) o1
ON o1.orderid = raw.oversea_orderid
LEFT JOIN (
SELECT * 
FROM o
WHERE is_cb_shop=0 
AND grass_region IN ('MY', 'VN','ID','TW','TH')
) o2 
ON o2.orderid = raw.local_orderid
LEFT JOIN buyer_account ba
ON ba.buyer_id = o2.cancel_user_id
AND ba.grass_region = o2.grass_region
LEFT JOIN (
SELECT DISTINCT
orderid
, min(CASE WHEN new_status = 1 THEN ctime ELSE null END) ar_time
, min(CASE WHEN new_status = 2 THEN ctime ELSE null END) pu_time
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live
WHERE grass_region IN ('MY', 'ID', 'VN','TW','TH')
AND DATE(FROM_UNIXTIME(CTIME)) >= current_date-interval'100'day 
GROUP BY 1
) l 
ON l.orderid = raw.local_orderid
WHERE date(from_unixtime(o1.cancel_time)) >= current_date-interval'100'day
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
a.shopid
, CASE WHEN a.grass_region IN ('SG', 'MY', 'PH', 'TW') THEN date(from_unixtime(mtime)) 
WHEN a.grass_region IN ('TH', 'ID', 'VN') THEN date((from_unixtime(mtime) - INTERVAL '1' HOUR)) 
WHEN a.grass_region = 'BR' THEN date((from_unixtime(mtime) - INTERVAL '11' HOUR)) 
WHEN a.grass_region = 'MX' THEN date((from_unixtime(mtime) - INTERVAL '14' HOUR))
WHEN a.grass_region = 'CL' THEN date(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN a.grass_region = 'CO' THEN date(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
END AS mtime
, count(DISTINCT (CASE WHEN a.grass_region = 'TW' AND reason IN (1, 2, 3, 103, 105) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
WHEN a.grass_region <> 'TW' AND reason IN (1, 2, 3, 103, 105, 107) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
ELSE null END)) RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT orderid 
, cancel_reason 
FROM o
WHERE is_cb_shop=1 
AND grass_region IN ('MY', 'SG', 'PH', 'TH', 'VN', 'BR','MX','CO','CL','TW')
) b 
ON a.orderid = b.orderid
WHERE date(from_unixtime(mtime)) >= current_date-interval'100'day
AND a.cb_option = 1 
AND a.grass_region IN ('MY', 'SG', 'PH', 'TH', 'VN', 'BR','MX','CO','CL','TW')
GROUP BY 1, 2
) 
SELECT DISTINCT
dim.grass_date
, sip.affi_country
, sip.mst_country
, sum(gross_order) gross_order
, sum(net_order) net_order
, sum(cancel_order) cancel_order
-- , sum(cancel_order_sync_suc) cancel_order_sync_suc
, sum(cancel_order_sync_suc) - sum(seller_fault_v2) - sum(cancel_order_v2) sync_failed_acl1_acl2
, sum(RR_order) RR_order
, sum(cancel_order) - sum(cancel_order_sync_suc) a_p_synced_failed
, sum(cancel_order_v2) + sum(seller_fault_v2) seller_fault --- seller V2 ---- both p and a are nfr cancellation and sync success
, sum(acl_order) acl_order
-- , sum(seller_fault_v2) seller_fault_v2 ----- a order is auto cancellation but by shopee account
-- , sum(cancel_order_v2) cancel_order_v2
-- , focus
FROM dim
INNER JOIN sip 
ON dim.grass_region = sip.affi_country
LEFT JOIN net 
ON sip.affi_shopid = net.shop_id
AND dim.grass_date = net.create_time
LEFT JOIN cancel 
ON sip.affi_shopid = cancel.shopid
AND dim.grass_date = cancel.cancel_time
LEFT JOIN rr 
ON sip.affi_shopid = rr.shopid
AND dim.grass_date = rr.mtime
--WHERE SIP.MST_COUNTRY ='TH'
GROUP BY 1, 2, 3
HAVING sum(gross_order) + COALESCE(sum(cancel_order), 0) + COALESCE(sum(RR_order), 0) > 0