WITH dim AS (
SELECT
DISTINCT grass_date,
grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE
grass_date >= current_date - interval '150' day
AND grass_region IN (
'SG',
'MY',
'TH',
'PH',
'ID',
'VN',
'BR',
'MX',
'CO',
'CL',
'PL'
)
),
sip AS (
SELECT
DISTINCT affi_shopid,
country,
mst_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT
DISTINCT shopid
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE
cb_option = 1
) b ON a.mst_shopid = b.shopid
WHERE
a.grass_region in (
'SG',
'MY',
'TH',
'PH',
'ID',
'VN',
'BR',
'MX',
'CO',
'CL',
'PL'
)
AND sip_shop_status != 3
),
seller AS (
SELECT
DISTINCT child_shopid,
grass_region,
CASE
WHEN gp_account_owner_userrole_name LIKE '%KAM_TB_Emerging%' THEN '1.KAM Emerging'
WHEN gp_account_owner_userrole_name LIKE '%KAM_TB_Mature%' THEN '1.KAM Mature'
WHEN gp_account_owner_userrole_name LIKE '%PSM_SH%' THEN '2.SH MSM'
WHEN gp_account_owner_userrole_name LIKE '%PSM_SZ%' THEN '2.SZ MSM'
WHEN gp_account_owner_userrole_name LIKE '%ICB_SH%' THEN '4.SH ICB'
WHEN gp_account_owner_userrole_name LIKE '%ICB_SZ%' THEN '4.SZ ICB'
WHEN gp_account_owner_userrole_name LIKE '%CS%' THEN '3.CS'
ELSE '5.Others'
END seller_type
FROM
cncbbi_general.shopee_cb_seller_profile_with_old_column
WHERE
grass_region IN (
'SG',
'MY',
'TH',
'PH',
'ID',
'VN',
'BR',
'MX',
'CO',
'CL',
'PL'
)
),
orders as (
SELECT
DISTINCT shop_id,
create_datetime,
order_id,
order_be_status_id,
cancel_timestamp,
cancel_datetime,
cancel_reason_id,
grass_region,
is_net_order
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE
date(split(create_datetime, ' ') [ 1 ]) >= current_date - interval '150' day
AND tz_type = 'local'
AND is_cb_shop = 1
AND grass_region IN (
'SG',
'MY',
'TH',
'PH',
'ID',
'VN',
'BR',
'MX',
'CO',
'CL',
'PL'
)
AND grass_date >= current_date - interval '150' day
),
net AS (
SELECT
DISTINCT shop_id,
date(split(create_datetime, ' ') [ 1 ]) as create_time,
count(DISTINCT order_id) gross_order,
count(
DISTINCT (
CASE
WHEN is_net_order = 1 THEN order_id
ELSE null
END
)
) net_order
FROM
orders
GROUP BY
1,
2
),
cancel AS (
SELECT
DISTINCT shop_id shopid,
date(cancel_datetime) cancel_time,
count(
DISTINCT (
CASE
WHEN grass_region = 'TW'
AND cancel_reason_id IN (1, 2, 3, 200, 201, 204, 302, 303) THEN order_id
WHEN grass_region <> 'TW'
AND cancel_reason_id IN (1, 2, 3, 4, 204, 301, 302, 303) THEN order_id
ELSE null
END
)
) cancel_order
FROM
orders
GROUP BY
1,
2
),
rr AS (
SELECT
DISTINCT shopid,
CASE
WHEN grass_region in ('SG', 'MY', 'PH', 'TW') then date(from_unixtime(mtime))
WHEN grass_region IN ('TH', 'ID', 'VN') THEN date(from_unixtime(mtime) - INTERVAL '1' HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime) - interval '11' hour)
WHEN grass_region = 'CL' THEN CAST(
format_datetime(
from_unixtime(mtime) AT TIME ZONE 'America/Santiago',
'yyyy-MM-dd HH:mm:ss'
) AS date
)
WHEN grass_region = 'CO' THEN CAST(
format_datetime(
from_unixtime(mtime) AT TIME ZONE 'America/Bogota',
'yyyy-MM-dd HH:mm:ss'
) AS date
)
WHEN grass_region = 'MX' THEN CAST(
format_datetime(
from_unixtime(mtime) AT TIME ZONE 'America/Mexico_City',
'yyyy-MM-dd HH:mm:ss'
) AS date
)
WHEN grass_region IN ('PL', 'ES', 'FR') THEN CAST(
format_datetime(
from_unixtime(mtime) AT TIME ZONE 'CET',
'yyyy-MM-dd HH:mm:ss'
) AS date
)
END AS mtime,
count(
DISTINCT (
CASE
WHEN grass_region = 'TW'
AND reason IN (1, 2, 3, 103, 105)
AND (
cancel_reason = 0
OR cancel_reason IS NULL
)
AND status IN (2, 5) THEN a.orderid
WHEN grass_region <> 'TW'
AND reason IN (1, 2, 3, 103, 105, 107)
AND (
cancel_reason = 0
OR cancel_reason IS NULL
)
AND status IN (2, 5) THEN a.orderid
ELSE null
END
)
) RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT
DISTINCT order_id orderid,
cancel_reason_id cancel_reason
FROM
orders
) b ON a.orderid = b.orderid
WHERE
date(from_unixtime(mtime)) >= current_date - interval '150' day
AND cb_option = 1
AND grass_region IN (
'SG',
'MY',
'TH',
'PH',
'ID',
'VN',
'BR',
'MX',
'CO',
'CL',
'PL'
)
GROUP BY
1,
2
)
SELECT
DISTINCT dim.grass_date,
COALESCE(seller_type, '5.Others') seller_type,
seller.grass_region,
sum(gross_order) gross_order,
sum(net_order) net_order,
sum(cancel_order) cancel_order,
sum(RR_order) RR_order
FROM
dim
INNER JOIN seller ON dim.grass_region = seller.grass_region
LEFT JOIN net ON CAST(seller.child_shopid AS int) = net.shop_id
AND dim.grass_date = net.create_time
LEFT JOIN cancel ON CAST(seller.child_shopid AS int) = cancel.shopid
AND dim.grass_date = cancel.cancel_time
LEFT JOIN rr ON CAST(seller.child_shopid AS int) = rr.shopid
AND dim.grass_date = rr.mtime
LEFT JOIN sip ON CAST(seller.child_shopid AS int) = sip.affi_shopid
WHERE
sip.affi_shopid IS NULL
GROUP BY
1,
2,
3