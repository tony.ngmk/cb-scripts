WITH
dim AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE (grass_date between current_date-interval'63'day and CURRENT_DATE-INTERVAL'1'DAY )
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
) 
, cb AS (SELECT DISTINCT CAST(child_shopid AS INTEGER) AS child_shopid
, grass_region
, gp_account_name
, gp_account_owner
, gp_account_owner_userrole_name
, child_account_name 
, (CASE WHEN (gp_account_owner_userrole_name LIKE '%KAM%') THEN 'KAM' WHEN (gp_account_owner_userrole_name LIKE '%PSM%') THEN 'PSM' 
ELSE 'CSS' END) seller_type
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
WHERE grass_region in ('TW','MY','BR','SG') ) 

, sip AS (
SELECT DISTINCT
affi_shopid
, a.country as affi_country 
, mst_shopid
, b.country as mst_country 
-- ,case when cb_option=1 then 'CB SIP' else 'Local SIP' end as shop_type 
, user_status
, is_holiday_mode
, source
FROM
(marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT shopid
, country
, cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) b ON (a.mst_shopid = b.shopid)) 
join (select distinct shop_id 
, user_status
, is_holiday_mode
, source 
from regcbbi_others.shop_profile
where grass_date = current_date-interval'1'day and is_cb_shop=1 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')) u on u.shop_id = a.affi_shopid 
WHERE grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') and sip_shop_status!=3
) 

, o as (
SELECT DISTINCT
shop_id
, date(split(create_datetime,' ')[1]) as create_time
, order_id 
, order_sn 
, order_be_status_id
, payment_method_id
from mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ( date(split(create_datetime,' ')[1]) >= current_date-interval'150'day ) and tz_type='local' and is_cb_shop=1
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and grass_date >= current_date-interval'150'day 
)
, l as (SELECT DISTINCT
o.shop_id shopid
, date(from_unixtime(min(ctime))) ctime
, count(distinct case when o.payment_method_id=6 and l.orderid is not null then o.order_id else null end ) cod_fail_order 
from o 
left join (select distinct orderid 
, ctime 
from marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live
WHERE new_status IN (6) and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and date(from_unixtime(ctime)) >= current_date-interval'150'day 
) l on l.orderid = o.order_id 

group by 1
) 

, net AS (
SELECT DISTINCT
shop_id
, create_time
, "count"(DISTINCT order_id) gross_order
, "count"(DISTINCT (CASE WHEN (order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15)) THEN order_id ELSE null END)) net_order
, count(distinct case when payment_method_id=6 then order_id else null end ) cod_order 
--, count(distinct case when payment_method_id=6 and l.orderid is not null then order_id else null end ) cod_fail_order 
FROM
o 

GROUP BY 1, 2
) 
, cancel AS (
SELECT DISTINCT
shopid
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(cancel_time)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(cancel_time) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(cancel_time)-interval'11'hour)
WHEN (grass_region = 'CL') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'CO') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'MX') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Mexico_City','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'PL') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'ES') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'FR') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 

end cancel_time
, "count"(DISTINCT (CASE WHEN ((grass_region = 'TW') AND (extinfo.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303))) THEN orderid 

WHEN ((grass_region NOT IN ('TW')) AND (extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) THEN orderid 
ELSE null END)) cancel_order
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ( ("date"("from_unixtime"(cancel_time)) >=current_date-interval'150'day )) AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and cb_option=1 
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
shopid
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(mtime)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(mtime) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime)-interval'11'hour)
WHEN (grass_region = 'CL') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'CO') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'MX') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'America/Mexico_City','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'PL') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'ES') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'FR') THEN DATE(CAST(format_datetime(from_unixtime(MTIME) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
end mtime
, a.orderid 
, order_sn 
, grass_date
, (CASE WHEN ((((grass_region = 'TW') AND (reason IN (1, 2, 3, 103, 105))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5))) THEN 'Y' 
WHEN ((((grass_region <> 'TW') AND (reason IN (1, 2, 3, 103, 105, 107))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5))) THEN 'Y' ELSE 'N' END) nfr_RR_order
, case when status =1 then 'requested'
when status =2 then 'accepted'
when status =3 then 'cancelled'
when status =4 then 'judging'
when status =5 then 'refund paid'
when status =6 then 'closed'
when status =7 then 'processing'
when status =8 then 'seller dispute'
ELSE NULL END rr_status 
, case when reason = 1 then 'NONRECEIPT' 
WHEN REASON = 2 THEN 'WRONG_ITEM' 
WHEN REASON =3 THEN 'ITEM_DAMAGED' 
WHEN REASON = 103 THEN 'ITEM_MISSING' 
WHEN REASON = 105 THEN 'FAKE_ITEM' 
WHEN REASON =107 THEN 'FUNCTIONAL_DAMAGED_ITEM' 
WHEN REASON= 102 THEN 'CHANGE OF MIND' 
WHEN REASON= 4 THEN 'DIFFERENT DESCRIPTION'
WHEN REASON=7 THEN 'Product is used/refurbished/rebuilt'
WHEN REASON = 104 THEN 'Product does not meet expectations'
WHEN REASON=101 THEN 'Received damaged/wrong product(s)'
WHEN REASON= 5 THEN 'Mutual agreement with the seller'
when reason =106 then 'Physical Damage'
when reason = 6 then 'Others'
ELSE 'Others' END reason_exp 
FROM
(marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
order_id
, cancel_reason_id cancel_reason 
, order_sn 
, date(split(create_datetime,' ')[1]) grass_date
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') 
and date(from_unixtime(create_timestamp))>=current_date-interval'90'day and is_cb_shop=1 
and grass_date >= current_date-interval'90'day 
) b ON (a.orderid = b.order_id))
where ( ("date"("from_unixtime"(mtime)) >= current_date-interval'90'day )) 
AND CB_OPTION= 1 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and status in (2,5)
) 


select sip.* 
, rr.orderid 
, rr.order_sn 
, rr.grass_date
, rr.mtime rr_modify_time 
, reason_exp
, nfr_RR_order
, rr.rr_status 
from rr 
join sip on sip.affi_shopid =rr.shopid 
where rr.mtime between current_date-interval'14'day and current_date-interval'1'day