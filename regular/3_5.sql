WITH dim AS ( 
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE grass_date >= CURRENT_DATE-INTERVAL'90'DAY 
) 

, raw AS (
SELECT DISTINCT
oversea_orderid
, max(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE date(from_unixtime(ctime)) >= CURRENT_DATE-INTERVAL'90'DAY
GROUP BY 1
) 
, sip AS (
SELECT DISTINCT
shopid mst_shopid 
, country AS mst_country 
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE cb_option=0 
AND country IN ('TW','ID','MY','VN','TH')
) 
, net AS ( 
SELECT DISTINCT
shop_id
, CASE WHEN raw.local_orderid is null THEN 'LOCAL(NS)' ELSE 'LOCAL SIP' END AS shop_type
, CASE WHEN grass_region in ('SG','MY','PH','TW') THEN date(from_unixtime(create_timestamp) )
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(create_timestamp) - INTERVAL'1'HOUR )
WHEN grass_region = 'BR' THEN date(from_unixtime(create_timestamp)-interval'11'hour)
WHEN grass_region = 'CL' THEN date(CAST(format_datetime(from_unixtime(create_timestamp) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'CO' THEN date(CAST(format_datetime(from_unixtime(create_timestamp) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
END AS create_time
, count(DISTINCT order_id) gross_order
, count(DISTINCT (CASE WHEN is_net_order = 1 THEN order_id ELSE null END)) net_order 
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live o 
JOIN sip 
ON sip.mst_shopid=o.shop_id 
LEFT JOIN raw 
ON raw.local_orderid= o.order_id
WHERE date(from_unixtime(create_timestamp)) >= CURRENT_DATE-INTERVAL'90'DAY
AND tz_type='local' 
AND o.is_cb_shop=0 
AND o.grass_region in ('TW','ID','MY','VN','TH')
AND grass_date>= (current_date - INTERVAL '90' DAY)
-- and raw.local_orderid is null
GROUP BY 1, 2,3 
) 
, cancel AS (
SELECT DISTINCT
shopid
, CASE WHEN raw.local_orderid is null THEN 'LOCAL(NS)' ELSE 'LOCAL SIP' END AS shop_type
, CASE WHEN grass_region in ('SG','MY','PH','TW') THEN date(from_unixtime(cancel_time)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(cancel_time) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(cancel_time)-interval'11'hour)
WHEN grass_region = 'CL' THEN date(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'CO' THEN date(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
END AS cancel_time
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND extinfo.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) THEN orderid 
WHEN grass_region = 'BR' AND extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,801,303) THEN orderid 
WHEN grass_region NOT IN ('TW','BR') AND extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) THEN orderid 
ELSE null END)) cancel_order
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live o 
JOIN sip 
ON sip.mst_shopid=o.shopid 
LEFT JOIN raw 
ON raw.local_orderid= o.orderid
WHERE date(from_unixtime(cancel_time)) >= current_date - INTERVAL '90' DAY
AND o.cb_option=0 
AND o.grass_region in ('TW','ID','MY','VN','TH')
-- and raw.local_orderid is null 
GROUP BY 1,2,3 
) 
, rr AS (
SELECT DISTINCT
shopid
, CASE WHEN raw.local_orderid is null THEN 'LOCAL(NS)' ELSE 'LOCAL SIP' END AS shop_type
, CASE WHEN grass_region in ('SG','MY','PH','TW') THEN date(from_unixtime(mtime)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(mtime) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime)-interval'11'hour)
WHEN grass_region = 'CL' THEN date(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'CO' THEN date(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
END AS mtime
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND reason IN (1, 2, 3, 103, 105) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
WHEN grass_region <> 'TW' AND reason IN (1, 2, 3, 103, 105, 107) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
ELSE null END)) RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE cb_option=0 
AND grass_region in ('TW','ID','MY','VN','TH') 
AND date(from_unixtime(CREATE_TIME)) >=(current_date - INTERVAL '90' DAY)
) b 
ON a.orderid = b.orderid
JOIN sip 
ON sip.mst_shopid=a.shopid 
LEFT JOIN raw 
ON raw.local_orderid= a.orderid
WHERE date(from_unixtime(mtime)) >= current_date - INTERVAL '90' DAY
AND a.cb_option=0 
AND a.grass_region in ('TW','ID','MY','VN','TH')
-- and raw.local_orderid is null 
GROUP BY 1, 2,3 
) 
SELECT DISTINCT
dim.grass_date
, sip.mst_country 
, sum(gross_order) gross_order
, sum(net_order) net_order
, sum(cancel_order) cancel_order
, sum(RR_order) RR_order
, net.shop_type 
FROM
dim
INNER JOIN sip 
ON dim.grass_region = sip.mst_country 
LEFT JOIN net 
ON sip.mst_shopid = net.shop_id
AND dim.grass_date = net.create_time
LEFT JOIN cancel 
ON sip.mst_shopid = cancel.shopid
AND dim.grass_date = cancel.cancel_time
AND cancel.shop_type = net.shop_type 
LEFT JOIN rr 
ON sip.mst_shopid = rr.shopid
AND dim.grass_date = rr.mtime
AND net.shop_type= rr.shop_type 
GROUP BY 1, 2,7
HAVING sum(gross_order) > 0