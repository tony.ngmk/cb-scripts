WITH sip as 
(
select distinct cast(affi.affi_shopid as bigint) as affi_shopid
, affi.mst_shopid
, mst.country as mst_country
, affi.country as affi_country
, mst.cb_option
from
(
select distinct cast(shopid as bigint) as shopid
, country
, cb_option
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) mst
join 
(
select distinct cast(affi_shopid as bigint) as affi_shopid
, affi_username
, cast(mst_shopid as bigint) as mst_shopid
, country
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where grass_region in ('SG','MY','PH','TH','ID','VN','TW')
and sip_shop_status != 3 
) affi
on mst.shopid = affi.mst_shopid
)

select distinct sls.grass_region
, sls.grass_date
, case when sip.affi_shopid is not null and sip.cb_option = 1 then 'CB SIP'
when sip.affi_shopid is not null and sip.cb_option = 0 then 'Local SIP'
else 'CB(NS)'
end as shop_type
, count(distinct sls.orderid) as gross_orders
, count(distinct if(sls.payment_method = 6, sls.orderid, null)) as cod_orders
, count(distinct if(sls.payment_method = 6 and sls.delivery_failed_time > sls.pickup_done_time, sls.orderid, null)) as logistic_failed_orders
-- Question: why logistics new_status = 6 time is null? if failed delivery is more than 10%, there'll never be 90% of completed orders?
, count(distinct if(sls.delivery_failed_time is null and o.order_fe_status_id in (3, 4), sls.orderid, null)) as complete_orders -- 3:COMPLETED, 4:CANCELLED 
from
(
select distinct shopid
, orderid
, order_placed_local_date as grass_date
, payment_method
, grass_region
, delivery_failed_time
, pickup_done_time -- Added by Chen Chen to align with SLS definition of COD failed delivery
from regcbbi_others.cb_logistics_mart_test
where grass_region in ('SG','MY','PH','TH','ID','VN','TW')
and order_placed_local_date between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
) sls
left join
(
select distinct order_id
, order_fe_status_id
, order_fe_status
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_date >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day
and tz_type = 'local'
and grass_region in ('SG','MY','PH','TH','ID','VN','TW')
and DATE(date_parse(create_datetime, '%Y-%m-%d %T')) >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '119' day
and is_cb_shop = 1
) o
on sls.orderid = o.order_id
left join sip
on sls.shopid = sip.affi_shopid
group by 1, 2, 3