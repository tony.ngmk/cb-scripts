WITH sip as 
(
select distinct affi.affi_shopid
, affi.country as affi_country
, affi.mst_shopid
, mst.country as mst_country
, u.user_status
, u.is_holiday_mode
, u.source
from
(
select distinct cast(shopid as bigint) as shopid
, country
, cb_option
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) mst
join 
(
select distinct cast(affi_shopid as bigint) as affi_shopid
, affi_username
, cast(mst_shopid as bigint) as mst_shopid
, country
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status != 3
and grass_region in ('SG','MY','PH','TH','ID','VN','TW')
) affi
on mst.shopid = affi.mst_shopid
join
(
select distinct shop_id
, user_status
, is_holiday_mode
, source 
from regcbbi_others.shop_profile
where grass_date = DATE(date_parse('${PRE_DAY}', '%Y%m%d'))
and is_cb_shop=1 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW')
) u
on affi.affi_shopid = u.shop_id
),

ord_sync as
(
select oversea_orderid
, max(local_orderid) as local_orderid
from marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
where ctime >= to_unixtime(DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '89' day)
group by 1
)

select sip.*
, sls.buyer_id
, sls.orderid as order_id
, sls.ordersn as order_sn
, sls.create_time --order placed date
, ofm.lm_first_failed_delivery_attempt_message
, ofm.lm_second_failed_delivery_attempt_message
, sls.cod_failed_time
, ofm.lm_first_attempt_delivery_datetime
, db.status as affi_order_be_status
, dl.status as affi_logistics_status
, p_ord.mst_order_be_status
, p_ord.mst_logistics_status
, sls.wh_outbound_date
from
(
select distinct grass_region
, orderid
, ordersn
, shopid
, buyer_userid as buyer_id
, order_placed_local_date as create_time
, status_ext
, logistics_status
, consignment_no
, DATE(from_unixtime(wh_outbound_time)) as wh_outbound_date
, DATE(from_unixtime(delivery_failed_time)) as cod_failed_time
from regcbbi_others.cb_logistics_mart_test
where payment_method = 6
and delivery_failed_time > pickup_done_time
and DATE(from_unixtime(wh_outbound_time)) between DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '34' day and DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '13' day
and sls_ops_parcel_confiscated_time is null
and sls_ops_terminal_damage_time is null
) sls
join sip
on sls.shopid = sip.affi_shopid
left join ord_sync
on sls.orderid = ord_sync.oversea_orderid
left join
(
select distinct order_id 
, order_be_status as mst_order_be_status
, logistics_status as mst_logistics_status
from mp_order.dwd_order_all_ent_df__reg_s0_live
where grass_date >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '89' day
and tz_type = 'local'
and is_cb_shop = 0
) p_ord
on ord_sync.local_orderid = p_ord.order_id
left join
(
select distinct consignment_no
, lm_first_attempt_delivery_datetime
, lm_first_failed_delivery_attempt_message
, lm_second_failed_delivery_attempt_message
from mp_ofm.dwd_forder_all_ent_df__reg_s0_live
where grass_date >= DATE(date_parse('${PRE_DAY}', '%Y%m%d')) - interval '89' day
and tz_type = 'local'
and is_cb_shop = 1
) ofm
on sls.consignment_no = ofm.consignment_no
left join mp_order.dim_order_backend_status__reg_s0_live db
on sls.status_ext = db.status_id
left join mp_order.dim_logistics_status__reg_s0_live dl
on sls.logistics_status = dl.status_id