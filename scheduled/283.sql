insert into nexus.test_5bf51331cc888bca8e6c9bf37e6c507b8dc52ac8fdda8e5d4cb38548d214b70d_370f5e1d0ad41531e542ba6c9d852e1b with kr_gov_subsidy_voucher as (
select 
try(cast(promotion_id as bigint)) as promotion_id,
try(date(start_date)) as start_date,
try(date(end_date)) as end_date 
from 
regcbbi_kr.krcb_gov_voucher_list
where
ingestion_timestamp in (
select
max(ingestion_timestamp) as latest_ingest_time
from
regcbbi_kr.krcb_gov_voucher_list
)
group by
1,2,3
)
select
date(split(o.create_datetime,' ')[1]) as order_create_date, --Order Create Date
o.grass_region,
v.promotion_id,
o.item_id,
o.shop_id,
sum(coalesce(pv_rebate_by_shopee_amt_usd,0) + coalesce(sv_rebate_by_shopee_amt_usd,0)) as voucher_rebate_usd, --USD
sum(coalesce(pv_rebate_by_shopee_amt,0) + coalesce(sv_rebate_by_shopee_amt,0)) as voucher_rebate --Local Currency
from 
mp_order.dwd_order_item_all_ent_df__reg_s0_live as o
join 
kr_gov_subsidy_voucher as v
on 
date(split(o.create_datetime,' ')[1]) between v.start_date and v.end_date
and (
v.promotion_id = o.sv_promotion_id 
or v.promotion_id = o.pv_promotion_id
)
where 
date(split(o.create_datetime,' ')[1]) between date('2022-01-01') and date_add('day',-1,current_date)
and o.grass_date >= date('2022-01-01')
and is_bi_excluded = 0
and is_net_order = 1
and is_cb_shop = 1
and tz_type = 'local'
group by 
1,2,3,4,5
order by
1,2,3,4