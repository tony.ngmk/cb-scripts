WITH shop_raw as 
(
SELECT a.*
, coalesce(b.gp_first_shop_date, c.registration_date) as first_shop_date
, date_trunc('month', start_date) as start_month 
, date_trunc('month', end_date) as end_month
FROM
(
SELECT trim(grass_region) AS grass_region
, cast(trim(shopid) AS BIGINT) AS shop_id 
, trim(gp_account_name) as gp_account_name
, cast(trim(is_eligible) AS INT) AS is_eligible
, trim(tiering) AS tiering
, round(cast(trim(rate) AS DOUBLE), 3) AS rate
, date'2021-10-16' as start_date 
, date'2021-11-15' as end_date
, 'Oct' as cycle
, 1 as is_old_shop_exemption
FROM regcb_bi.bi__cncb_commission_full_2021_oct__tmp__reg__s0
where ingestion_timestamp = (select max(ingestion_timestamp) as max_time from regcb_bi.bi__cncb_commission_full_2021_oct__tmp__reg__s0)


UNION


SELECT trim(grass_region) AS grass_region
, cast(trim(shopid) AS BIGINT) AS shop_id 
, trim(gp_account_name) as gp_account_name
, cast(trim(is_eligible) AS INT) AS is_eligible
, trim(tiering) AS tiering
, round(cast(trim(rate) AS DOUBLE), 3) AS rate
, date'2021-11-16' as start_date 
, date'2021-12-15' as end_date
, 'Nov' as cycle
, 1 as is_old_shop_exemption
FROM regcbbi_others.comm__cncb_commission_full_2021_nov__tmp__reg__s0
where ingestion_timestamp = (select max(ingestion_timestamp) as max_time from regcbbi_others.comm__cncb_commission_full_2021_nov__tmp__reg__s0)


UNION


SELECT trim(grass_region) AS grass_region
, cast(trim(shopid) AS BIGINT) AS shop_id 
, trim(gp_account_name) as gp_account_name
, cast(trim(is_eligible) AS INT) AS is_eligible
, trim(tiering) AS tiering
, round(cast(trim(rate) AS DOUBLE), 3) AS rate
, date'2021-12-16' as start_date 
, date'2021-12-31' as end_date
, 'Dec' as cycle
, cast(trim(is_old_shop_exemption) as int) as is_old_shop_exemption
FROM regcbbi_others.comm__cncb_commission_1st_half_cycle_2021_dec__tmp__reg__s0
where ingestion_timestamp = (select max(ingestion_timestamp) as max_time from regcbbi_others.comm__cncb_commission_1st_half_cycle_2021_dec__tmp__reg__s0)


UNION


SELECT trim(grass_region) AS grass_region
, cast(trim(shopid) AS BIGINT) AS shop_id 
, trim(gp_account_name) as gp_account_name
, cast(trim(is_eligible) AS INT) AS is_eligible
, trim(tiering) AS tiering
, round(cast(trim(rate) AS DOUBLE), 3) AS rate
, date'2022-01-01' as start_date 
, date'2022-01-15' as end_date
, 'Dec' as cycle
, cast(trim(is_old_shop_exemption) as int) as is_old_shop_exemption
FROM regcbbi_others.comm__cncb_commission_2nd_half_cycle_2021_dec__tmp__reg__s0
where ingestion_timestamp = (select max(ingestion_timestamp) as max_time from regcbbi_others.comm__cncb_commission_2nd_half_cycle_2021_dec__tmp__reg__s0)


UNION


SELECT trim(grass_region) AS grass_region
, cast(trim(shopid) AS BIGINT) AS shop_id 
, trim(gp_account_name) as gp_account_name
, cast(trim(is_eligible) AS INT) AS is_eligible
, trim(tiering) AS tiering
, round(cast(trim(rate) AS DOUBLE), 3) AS rate
, date'2022-01-16' as start_date 
, date'2022-02-15' as end_date
, 'Jan' as cycle
, cast(trim(is_old_shop_exemption) as int) as is_old_shop_exemption
FROM regcbbi_others.comm__cncb_commission_full_2022_jan__tmp__reg__s0
where ingestion_timestamp = (select max(ingestion_timestamp) as max_time from regcbbi_others.comm__cncb_commission_full_2022_jan__tmp__reg__s0)


UNION


SELECT trim(grass_region) AS grass_region
, cast(trim(shopid) AS BIGINT) AS shop_id 
, trim(gp_account_name) as gp_account_name
, cast(trim(is_eligible) AS INT) AS is_eligible
, trim(tiering) AS tiering
, round(cast(trim(rate) AS DOUBLE), 3) AS rate
, date'2022-02-16' as start_date 
, date'2022-03-15' as end_date
, 'Feb' as cycle
, cast(trim(is_old_shop_exemption) as int) as is_old_shop_exemption
FROM regcbbi_others.comm__cncb_commission_full_2022_feb__tmp__reg__s0
where ingestion_timestamp = (select max(ingestion_timestamp) as max_time from regcbbi_others.comm__cncb_commission_full_2022_feb__tmp__reg__s0)


UNION


SELECT trim(grass_region) AS grass_region
, cast(trim(shopid) AS BIGINT) AS shop_id 
, trim(gp_account_name) as gp_account_name
, cast(trim(is_eligible) AS INT) AS is_eligible
, trim(tiering) AS tiering
, round(cast(trim(rate) AS DOUBLE), 3) AS rate
, date'2022-03-16' as start_date 
, date'2022-04-15' as end_date
, 'Mar' as cycle
, cast(trim(is_old_shop_exemption) as int) as is_old_shop_exemption
FROM regcbbi_others.comm__cncb_commission_full_2022_mar__tmp__reg__s0
where ingestion_timestamp = (select max(ingestion_timestamp) as max_time from regcbbi_others.comm__cncb_commission_full_2022_mar__tmp__reg__s0)
) a 
left join 
(
SELECT shopid, cast(gp_all_site_first_shop_date_new_logic as date) as gp_first_shop_date
FROM regcbbi_general.commission_shop_gp_name
) b on a.shop_id = b.shopid 
left join
(
select grass_region, shop_id, date(from_unixtime(registration_timestamp)) as registration_date
from mp_user.dim_user__reg_s0_live
where tz_type = 'local'
and grass_date = current_date - interval '1' day
and is_cb_shop = 1
) c on a.shop_id = c.shop_id
WHERE ((a.is_eligible=0 and a.is_old_shop_exemption = 1) or a.tiering in ('Tier 1','Tier 2'))
)


, order_raw as 
(
SELECT grass_region 
, DATE(date_parse(create_datetime, '%Y-%m-%d %T')) as create_date
, shop_id
, sum(order_fraction) as total_order 
, sum(gmv_usd) as sum_nmv_usd 
--, sum(CASE WHEN order_be_status_id in (1, 2, 3, 4, 11, 12, 13, 14, 15) then gmv_usd else 0 end) as sum_nmv_usd
, sum(commission_fee_usd) as comm_usd 
, sum(service_fee_usd) as service_usd
, sum(seller_txn_fee_usd) as seller_txn_usd
, sum(buyer_txn_fee_usd) as buyer_txn_usd
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live 
WHERE tz_type = 'local'
AND grass_date >= DATE '2021-10-16'
AND DATE(date_parse(create_datetime, '%Y-%m-%d %T')) >= DATE '2021-10-16'
AND is_cb_shop = 1 
AND order_be_status_id in (1, 2, 3, 4, 11, 12, 13, 14, 15)
GROUP BY 1,2,3
)




, ads as 
(
SELECT grass_date
, shop_id 
, sum(ads_expenditure_amt_usd_1d) as total_exp 
, sum(free_expenditure_w_expiry_amt_usd_1d) + sum(free_expenditure_wo_expiry_amt_usd_1d) as free_exp
from mp_paidads.ads_advertiser_mkt_1d__reg_s0_live
where grass_date >= date '2021-10-16'
and tz_type = 'local'
and is_cb_seller = 1
group by 1,2
)






SELECT cycle 
, start_date 
, end_date 
, tiering 
, 1.0000*(m0_comm + m0_service + m0_stxn + m0_btxn + m0_total_exp) / m0_nmv as m0_total_take_rate 
, try(1.0000*(m1_comm + m1_service + m1_stxn + m1_btxn + + m1_total_exp) / m1_nmv) as m1_total_take_rate 
, try(1.0000*(m2_comm + m2_service + m2_stxn + m2_btxn + + m2_total_exp) / m2_nmv) as m2_total_take_rate 
, try(1.0000*(m3_comm + m3_service + m3_stxn + m3_btxn + + m3_total_exp) / m3_nmv) as m3_total_take_rate 
, try(1.0000*(m4_comm + m4_service + m4_stxn + m4_btxn + + m4_total_exp) / m4_nmv) as m4_total_take_rate
, try(1.0000*(m5_comm + m5_service + m5_stxn + m5_btxn + + m5_total_exp) / m5_nmv) as m5_total_take_rate


, 1.0000*(m0_comm + m0_service + m0_stxn + m0_btxn + m0_total_exp - m0_free_exp) / m0_nmv as m0_net_take_rate 
, try(1.0000*(m1_comm + m1_service + m1_stxn + m1_btxn + m1_total_exp - m1_free_exp) / m1_nmv) as m1_net_take_rate 
, try(1.0000*(m2_comm + m2_service + m2_stxn + m2_btxn + m2_total_exp - m2_free_exp) / m2_nmv) as m2_net_take_rate 
, try(1.0000*(m3_comm + m3_service + m3_stxn + m3_btxn + m3_total_exp - m3_free_exp) / m3_nmv) as m3_net_take_rate 
, try(1.0000*(m4_comm + m4_service + m4_stxn + m4_btxn + m4_total_exp - m4_free_exp) / m4_nmv) as m4_net_take_rate 
, try(1.0000*(m5_comm + m5_service + m5_stxn + m5_btxn + m5_total_exp - m5_free_exp) / m5_nmv) as m5_net_take_rate 
FROM
(
SELECT cycle
, start_date
, end_date
, CASE WHEN is_eligible = 0 THEN 'Exemption' ELSE tiering END as tiering


, sum(CASE WHEN o.create_date between start_date and end_date THEN sum_nmv_usd ELSE 0 END) as m0_nmv
, sum(CASE WHEN o.create_date between start_date + interval '1' month and end_date + interval '1' month THEN sum_nmv_usd ELSE 0 END) as m1_nmv 
, sum(CASE WHEN o.create_date between start_date + interval '2' month and end_date + interval '2' month THEN sum_nmv_usd ELSE 0 END) as m2_nmv
, sum(CASE WHEN o.create_date between start_date + interval '3' month and end_date + interval '3' month THEN sum_nmv_usd ELSE 0 END) as m3_nmv
, sum(CASE WHEN o.create_date between start_date + interval '4' month and end_date + interval '4' month THEN sum_nmv_usd ELSE 0 END) as m4_nmv
, sum(CASE WHEN o.create_date between start_date + interval '5' month and end_date + interval '5' month THEN sum_nmv_usd ELSE 0 END) as m5_nmv


, sum(CASE WHEN o.create_date between start_date and end_date THEN comm_usd ELSE 0 END) as m0_comm
, sum(CASE WHEN o.create_date between start_date + interval '1' month and end_date + interval '1' month THEN comm_usd ELSE 0 END) as m1_comm 
, sum(CASE WHEN o.create_date between start_date + interval '2' month and end_date + interval '2' month THEN comm_usd ELSE 0 END) as m2_comm
, sum(CASE WHEN o.create_date between start_date + interval '3' month and end_date + interval '3' month THEN comm_usd ELSE 0 END) as m3_comm
, sum(CASE WHEN o.create_date between start_date + interval '4' month and end_date + interval '4' month THEN comm_usd ELSE 0 END) as m4_comm
, sum(CASE WHEN o.create_date between start_date + interval '5' month and end_date + interval '5' month THEN comm_usd ELSE 0 END) as m5_comm


, sum(CASE WHEN o.create_date between start_date and end_date THEN service_usd ELSE 0 END) as m0_service
, sum(CASE WHEN o.create_date between start_date + interval '1' month and end_date + interval '1' month THEN service_usd ELSE 0 END) as m1_service
, sum(CASE WHEN o.create_date between start_date + interval '2' month and end_date + interval '2' month THEN service_usd ELSE 0 END) as m2_service
, sum(CASE WHEN o.create_date between start_date + interval '3' month and end_date + interval '3' month THEN service_usd ELSE 0 END) as m3_service
, sum(CASE WHEN o.create_date between start_date + interval '4' month and end_date + interval '4' month THEN service_usd ELSE 0 END) as m4_service
, sum(CASE WHEN o.create_date between start_date + interval '5' month and end_date + interval '5' month THEN service_usd ELSE 0 END) as m5_service


, sum(CASE WHEN o.create_date between start_date and end_date THEN seller_txn_usd ELSE 0 END) as m0_stxn
, sum(CASE WHEN o.create_date between start_date + interval '1' month and end_date + interval '1' month THEN seller_txn_usd ELSE 0 END) as m1_stxn
, sum(CASE WHEN o.create_date between start_date + interval '2' month and end_date + interval '2' month THEN seller_txn_usd ELSE 0 END) as m2_stxn
, sum(CASE WHEN o.create_date between start_date + interval '3' month and end_date + interval '3' month THEN seller_txn_usd ELSE 0 END) as m3_stxn
, sum(CASE WHEN o.create_date between start_date + interval '4' month and end_date + interval '4' month THEN seller_txn_usd ELSE 0 END) as m4_stxn
, sum(CASE WHEN o.create_date between start_date + interval '5' month and end_date + interval '5' month THEN seller_txn_usd ELSE 0 END) as m5_stxn


, sum(CASE WHEN o.create_date between start_date and end_date THEN buyer_txn_usd ELSE 0 END) as m0_btxn
, sum(CASE WHEN o.create_date between start_date + interval '1' month and end_date + interval '1' month THEN buyer_txn_usd ELSE 0 END) as m1_btxn 
, sum(CASE WHEN o.create_date between start_date + interval '2' month and end_date + interval '2' month THEN buyer_txn_usd ELSE 0 END) as m2_btxn
, sum(CASE WHEN o.create_date between start_date + interval '3' month and end_date + interval '3' month THEN buyer_txn_usd ELSE 0 END) as m3_btxn
, sum(CASE WHEN o.create_date between start_date + interval '4' month and end_date + interval '4' month THEN buyer_txn_usd ELSE 0 END) as m4_btxn
, sum(CASE WHEN o.create_date between start_date + interval '5' month and end_date + interval '5' month THEN buyer_txn_usd ELSE 0 END) as m5_btxn


, sum(CASE WHEN a.grass_date between start_date and end_date THEN total_exp ELSE 0 END) as m0_total_exp
, sum(CASE WHEN a.grass_date between start_date + interval '1' month and end_date + interval '1' month THEN total_exp ELSE 0 END) as m1_total_exp 
, sum(CASE WHEN a.grass_date between start_date + interval '2' month and end_date + interval '2' month THEN total_exp ELSE 0 END) as m2_total_exp
, sum(CASE WHEN a.grass_date between start_date + interval '3' month and end_date + interval '3' month THEN total_exp ELSE 0 END) as m3_total_exp
, sum(CASE WHEN a.grass_date between start_date + interval '4' month and end_date + interval '4' month THEN total_exp ELSE 0 END) as m4_total_exp
, sum(CASE WHEN a.grass_date between start_date + interval '5' month and end_date + interval '5' month THEN total_exp ELSE 0 END) as m5_total_exp


, sum(CASE WHEN a.grass_date between start_date and end_date THEN free_exp ELSE 0 END) as m0_free_exp
, sum(CASE WHEN a.grass_date between start_date + interval '1' month and end_date + interval '1' month THEN free_exp ELSE 0 END) as m1_free_exp 
, sum(CASE WHEN a.grass_date between start_date + interval '2' month and end_date + interval '2' month THEN free_exp ELSE 0 END) as m2_free_exp
, sum(CASE WHEN a.grass_date between start_date + interval '3' month and end_date + interval '3' month THEN free_exp ELSE 0 END) as m3_free_exp
, sum(CASE WHEN a.grass_date between start_date + interval '4' month and end_date + interval '4' month THEN free_exp ELSE 0 END) as m4_free_exp
, sum(CASE WHEN a.grass_date between start_date + interval '5' month and end_date + interval '5' month THEN free_exp ELSE 0 END) as m5_free_exp


, sum(CASE WHEN a.grass_date between start_date and end_date THEN total_exp-free_exp ELSE 0 END) as m0_net_exp
, sum(CASE WHEN a.grass_date between start_date + interval '1' month and end_date + interval '1' month THEN total_exp-free_exp ELSE 0 END) as m1_net_exp 
, sum(CASE WHEN a.grass_date between start_date + interval '2' month and end_date + interval '2' month THEN total_exp-free_exp ELSE 0 END) as m2_net_exp
, sum(CASE WHEN a.grass_date between start_date + interval '3' month and end_date + interval '3' month THEN total_exp-free_exp ELSE 0 END) as m3_net_exp
, sum(CASE WHEN a.grass_date between start_date + interval '4' month and end_date + interval '4' month THEN total_exp-free_exp ELSE 0 END) as m4_net_exp
, sum(CASE WHEN a.grass_date between start_date + interval '5' month and end_date + interval '5' month THEN total_exp-free_exp ELSE 0 END) as m5_net_exp


, sum(CASE WHEN o.create_date between start_date and end_date THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date and end_date THEN total_exp-free_exp ELSE 0 END) as m0_net_revenue
, sum(CASE WHEN o.create_date between start_date + interval '1' month and end_date + interval '1' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '1' month and end_date + interval '1' month THEN total_exp-free_exp ELSE 0 END) as m1_net_revenue 
, sum(CASE WHEN o.create_date between start_date + interval '2' month and end_date + interval '2' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '2' month and end_date + interval '2' month THEN total_exp-free_exp ELSE 0 END) as m2_net_revenue
, sum(CASE WHEN o.create_date between start_date + interval '3' month and end_date + interval '3' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '3' month and end_date + interval '3' month THEN total_exp-free_exp ELSE 0 END) as m3_net_revenue
, sum(CASE WHEN o.create_date between start_date + interval '4' month and end_date + interval '4' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '4' month and end_date + interval '4' month THEN total_exp-free_exp ELSE 0 END) as m4_net_revenue
, sum(CASE WHEN o.create_date between start_date + interval '5' month and end_date + interval '5' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '5' month and end_date + interval '5' month THEN total_exp-free_exp ELSE 0 END) as m5_net_revenue


, sum(CASE WHEN o.create_date between start_date and end_date THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date and end_date THEN total_exp ELSE 0 END) as m0_gross_revenue
, sum(CASE WHEN o.create_date between start_date + interval '1' month and end_date + interval '1' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '1' month and end_date + interval '1' month THEN total_exp ELSE 0 END) as m1_gross_revenue
, sum(CASE WHEN o.create_date between start_date + interval '2' month and end_date + interval '2' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '2' month and end_date + interval '2' month THEN total_exp ELSE 0 END) as m2_gross_revenue
, sum(CASE WHEN o.create_date between start_date + interval '3' month and end_date + interval '3' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '3' month and end_date + interval '3' month THEN total_exp ELSE 0 END) as m3_gross_revenue
, sum(CASE WHEN o.create_date between start_date + interval '4' month and end_date + interval '4' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '4' month and end_date + interval '4' month THEN total_exp ELSE 0 END) as m4_gross_revenue
, sum(CASE WHEN o.create_date between start_date + interval '5' month and end_date + interval '5' month THEN comm_usd+service_usd+seller_txn_usd+buyer_txn_usd ELSE 0 END) + sum(CASE WHEN a.grass_date between start_date + interval '5' month and end_date + interval '5' month THEN total_exp ELSE 0 END) as m5_gross_revenue




from shop_raw s 
LEFT JOIN order_raw o on s.shop_id = o.shop_id 
LEFT JOIN ads a on s.shop_id = a.shop_id 
GROUP BY 1,2,3,4
ORDER BY 4,2
)
ORDER BY 4,2