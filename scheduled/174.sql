insert into nexus.test_3156a4d767472b4d4772bed9e63971ae98b44a851736245add0a51bb3dbc770e_c0a148604f582e1bae1230e0a8a02966 WITH audit AS (
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_sg_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_id_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_my_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_tw_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_th_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_ph_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_vn_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_br_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_mx_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_co_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_cl_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_pl_db__shopee_audit_log_tab__reg_daily_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
target_pk,
"json_extract_scalar"(changes, '$.status[1]') audit_status,
"json_extract_scalar"(changes, '$.reason') reason,
action_time_msec
FROM
marketplace.shopee_backend_log_es_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE
"json_extract_scalar"(changes, '$.status[1]') IN ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION
SELECT
user_id AS target_pk,
case
when new_status = 1 then 'normal'
when new_status = 2 then 'banned'
when new_status = 3 then 'frozen'
else null
end as audit_status,
remarks as reason,
ctime * 1000 as action_time_msec
FROM
marketplace.shopee_fraud_treatment_center_db__treatmentcenter_user_account_oplog_tab__reg_daily_s0_live
WHERE
grass_region != ''
AND new_status IN (1, 2, 3)
),
user AS (
SELECT
grass_region,
user_id,
user_name,
shop_id,
account_status,
account_status_reason,
account_status_date
FROM
(
SELECT
grass_region,
user_id,
user_name,
shop_id,
account_status,
audit_status,
"lower"(reason) account_status_reason,
"date"("from_unixtime"((action_time_msec / 1000))) account_status_date,
"rank"() OVER (
PARTITION BY target_pk,
audit_status
ORDER BY
action_time_msec DESC
) action_time_rank
FROM
audit a
INNER JOIN (
SELECT
DISTINCT grass_region,
user_id,
user_name,
shop_id,
IF(
status = 0,
'deleted',
IF(status = 2, 'banned', 'frozen')
) account_status
FROM
mp_user.dim_user__reg_s0_live
WHERE
is_cb_shop = 1
AND grass_date = "date_add"('day', -1, current_date)
AND tz_type = 'local'
AND status IN (0, 2, 3)
) u ON target_pk = u.user_id
)
WHERE
audit_status = account_status
AND action_time_rank = 1
),
sip AS (
SELECT
affi_shopid,
t1.cb_option,
t1.country AS psite
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (
SELECT
distinct affi_shopid,
affi_userid,
affi_username,
mst_shopid,
country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE
sip_shop_status != 3
AND grass_region != ''
) AS t2 on t1.shopid = t2.mst_shopid
),
cb_seller AS (
SELECT
*,
COALESCE(gp.primary_email__c, sip_gp.primary_email__c) seller_email
FROM
(
SELECT
CAST(cb.child_shopid AS varchar) shop_id,
CAST(cb.child_userid AS varchar) user_id,
cb.gp_account_billing_country,
cb.seller_type,
cb.gp_account_name,
(
CASE
WHEN (cb.gp_account_billing_country = 'Korea') THEN krgp.gp_smt_pic_email
ELSE cb.gp_account_owner_email
END
) gp_account_owner_email,
cb.child_account_name shop_name,
(
CASE
WHEN (cbsip.affi_userid IS NOT NULL) THEN "split"(cb.gp_account_name, '-') [1]
END
) sip_gp_account_name,
(
CASE
WHEN (cbsip.affi_userid IS NOT NULL) THEN 1
ELSE 0
END
) is_cbsip,
cb.grass_region region
FROM
(
SELECT
si.grass_region,
si.child_shopid,
si.child_userid,
si.gp_account_name,
si.gp_account_id,
si.gp_account_owner_email,
si.child_account_owner,
si.child_account_name,
si.gp_account_billing_country,
CASE
WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
WHEN sip.affi_shopid IS NOT NULL
AND sip.cb_option = 0 THEN 'Local SIP'
WHEN sip.affi_shopid IS NOT NULL
AND sip.psite IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL
AND si.gp_account_billing_country = 'Korea'
AND sip.psite IN ('SG') THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL THEN 'CB SIP'
when si.gp_account_billing_country = 'Korea' then 'KRCB'
when si.gp_account_billing_country = 'Japan' then 'JPCB'
when si.gp_account_billing_country is NULL
or si.gp_account_billing_country != 'China' then 'Others'
when si.child_account_owner_userrole_name like '%HKCB%' then 'HKCB'
when si.child_account_owner_userrole_name like '%BD_%' then 'CNCB BD'
when si.child_account_owner_userrole_name like '%UST_%' then 'CNCB UST'
when si.child_account_owner_userrole_name like '%ST_%' THEN 'CNCB ST'
when si.child_account_owner_userrole_name like '%MT_%' THEN 'CNCB MT'
when si.child_account_owner_userrole_name like '%LT%' THEN 'CNCB LT'
WHEN si.gp_account_billing_country = 'China' THEN COALESCE(CONCAT('CNCB ', si.cb_group), 'CNCB Others')
else 'CNCB Others'
END as seller_type
FROM
regbd_sf.cb__seller_index_tab AS si
LEFT JOIN sip ON CAST(si.child_shopid AS BIGINT) = sip.affi_shopid
LEFT JOIN (
SELECT
DISTINCT shop_id
FROM
regcbbi_general.cbwh_shop_history_v2
WHERE
grass_date = DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = CAST(si.child_shopid as BIGINT)
WHERE
si.grass_region <> 'XX'
) AS cb
LEFT JOIN (
SELECT
DISTINCT affi_userid,
mst_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live ON mst_shopid = shopid
WHERE
cb_option = 1
AND grass_region <> 'XX'
) AS cbsip ON CAST(cbsip.affi_userid AS varchar) = CAST(cb.child_userid AS varchar)
LEFT JOIN (
SELECT
child_shopid,
gp_smt_pic,
gp_smt_pic_email
FROM
regcbbi_others.shopee_regional_cb_team__krcb_gp_allocation
WHERE
ingestion_timestamp = (
SELECT
"max"(ingestion_timestamp) max_ing
FROM
regcbbi_others.shopee_regional_cb_team__krcb_gp_allocation
)
) AS krgp ON CAST(krgp.child_shopid AS varchar) = CAST(cb.child_shopid AS varchar)
) AS tab
LEFT JOIN (
SELECT
name,
max(primary_email__c) as primary_email__c
FROM
cncbbi_general.shopee_sz_bd_team__gp_contact_info
WHERE
ingestion_timestamp = (
SELECT
"max"(ingestion_timestamp) maxing
FROM
cncbbi_general.shopee_sz_bd_team__gp_contact_info
)
GROUP BY
1
) AS gp ON gp.name = tab.gp_account_name
LEFT JOIN (
SELECT
name,
max(primary_email__c) as primary_email__c
FROM
cncbbi_general.shopee_sz_bd_team__gp_contact_info
WHERE
ingestion_timestamp = (
SELECT
"max"(ingestion_timestamp) maxing
FROM
cncbbi_general.shopee_sz_bd_team__gp_contact_info
)
GROUP BY
1
) AS sip_gp ON sip_gp.name = tab.sip_gp_account_name
),
user_agg AS (
SELECT
user.shop_id,
user.user_id,
IF(
(user.account_status = 'deleted'),
'Deleted Account',
cb_seller.seller_type
) seller_type,
IF(
(user.account_status = 'deleted'),
'Deleted Account',
cb_seller.gp_account_name
) gp_account_name,
IF(
(user.account_status = 'deleted'),
'Deleted Account',
cb_seller.gp_account_owner_email
) gp_account_owner_email,
user.user_name shop_name,
IF(
(user.account_status = 'deleted'),
'Deleted Account',
cb_seller.seller_email
) GP_email,
IF(
(user.account_status = 'deleted'),
'Deleted Account',
CAST(cb_seller.is_cbsip AS varchar)
) is_cbsip,
user.grass_region region,
user.account_status,
user.account_status_reason,
(
CASE
WHEN "regexp_like"(
user.account_status_reason,
'prohibit|counterfeit|fake'
) THEN 'Counterfeit / Fake / Prohibited'
WHEN "regexp_like"(user.account_status_reason, 'Inactive') THEN 'Inactive'
WHEN "regexp_like"(user.account_status_reason, 'scam|fraud') THEN 'Scam / Fraud'
WHEN "regexp_like"(user.account_status_reason, 'low price|brush') THEN 'Low Price / Order Brushing'
WHEN "regexp_like"(
user.account_status_reason,
'gp.*?limit|limit.*?gp'
) THEN 'GP Limit'
WHEN "regexp_like"(user.account_status_reason, 'shadow') THEN 'Shadow Account'
WHEN "regexp_like"(user.account_status_reason, 'kyc') THEN 'KYC'
WHEN "regexp_like"(user.account_status_reason, 'seller request') THEN 'Seller Request'
ELSE 'Others'
END
) account_status_reason_processed,
user.account_status_date account_status_changed_date,
"date_diff"(
'day',
user.account_status_date,
"date_add"('day', -1, current_date)
) account_status_days,
cb_seller.gp_account_billing_country
FROM
user
LEFT JOIN cb_seller ON CAST(user.user_id AS varchar) = CAST(cb_seller.user_id AS varchar)
),
adj_reason AS (
SELECT
*,
CASE
WHEN ("lower"(scenario) LIKE '%paid ads%')
OR (scenario LIKE '%Ads%') THEN 'Ads Related'
WHEN "lower"(scenario) LIKE '%refund%' THEN 'RR Related'
WHEN (
("lower"(scenario) LIKE '%shipping fee%')
OR ("lower"(scenario) LIKE '%logistic%')
)
OR ("lower"(scenario) LIKE '%asf%') THEN 'Logistics / Shipping Related'
WHEN (
("lower"(scenario) LIKE '%transaction fee%')
OR ("lower"(scenario) LIKE '%service fee%')
)
OR ("lower"(scenario) LIKE '%commission fee%') THEN 'Seller Fee Related'
WHEN "lower"(scenario) LIKE '%warehouse%' THEN 'Warehouse Related'
ELSE 'Others'
END AS adjustment_reason_group
FROM
(
SELECT
'SG' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_sg_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'ID' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_id_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'MY' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_my_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'TW' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_tw_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'TH' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_th_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'PH' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_ph_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'VN' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_vn_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'BR' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_br_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'MX' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_mx_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'CO' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_co_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'CL' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_cl_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'PL' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_pl_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION
SELECT
'ES' region,
reason_code_id,
scenario
FROM
marketplace.shopee_settlement_system_es_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
)
),
escrow_verified_date_before_2019_aug AS (
SELECT
order_id,
"date"("from_unixtime"(escrow_verified_timestamp)) ev_date
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE
tz_type = 'local'
AND is_cb_shop = 1
AND escrow_verified_timestamp <= "to_unixtime"(date '2019-08-31')
),
fx AS (
SELECT
DISTINCT currency,
local_to_usd,
usd_to_cny,
usd_to_krw,
usd_to_jpy
FROM
(
SELECT
currency,
grass_date,
exchange_rate local_to_usd
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE
(grass_date = "date_add"('day', -1, current_date))
) fx_usd
INNER JOIN (
SELECT
(1 / exchange_rate) usd_to_cny,
grass_date
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE
(
(grass_date = "date_add"('day', -1, current_date))
AND (currency = 'CNY')
)
) fx_cny ON (fx_usd.grass_date = fx_cny.grass_date)
INNER JOIN (
SELECT
(1 / exchange_rate) usd_to_krw,
grass_date
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE
(
(grass_date = "date_add"('day', -1, current_date))
AND (currency = 'KRW')
)
) fx_krw ON (fx_usd.grass_date = fx_krw.grass_date)
INNER JOIN (
SELECT
(1 / exchange_rate) usd_to_jpy,
grass_date
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE
(
(grass_date = "date_add"('day', -1, current_date))
AND (currency = 'JPY')
)
) fx_jpy ON (fx_usd.grass_date = fx_jpy.grass_date)
)
SELECT
user_agg.shop_id,
user_agg.seller_type,
user_agg.gp_account_name,
user_agg.gp_account_owner_email,
user_agg.shop_name,
user_agg.GP_email,
user_agg.is_cbsip,
user_agg.region,
esc_agg.total_escrow_USD_amt,
adj_agg.Ads_adjustment_USD_amt,
adj_agg.Logistics_Shipping_adjustment_USD_amt,
adj_agg.Order_ASF_adjustment_USD_amt,
adj_agg.RR_adjustment_USD_amt,
adj_agg.Seller_Fee_adjustment_USD_amt,
adj_agg.Warehouse_adjustment_USD_amt,
adj_agg.Other_adjustment_USD_amt,
(
COALESCE(esc_agg.total_escrow_USD_amt, 0) + COALESCE(adj_agg.total_adjustment_USD_amt, 0)
) negative_balance_USD_amt,
COALESCE(esc_agg.currency, adj_agg.currency) local_currency,
(
COALESCE(esc_agg.total_escrow_local_amt, 0) + COALESCE(adj_agg.total_adjustment_local_amt, 0)
) negative_balance_local_amt,
IF(
(user_agg.gp_account_billing_country = 'Korea'),
'KRW',
IF(
(user_agg.gp_account_billing_country = 'Japan'),
'JPY',
IF(
(user_agg.gp_account_billing_country = 'China'),
'RMB',
'USD'
)
)
) seller_currency,
(
(
COALESCE(esc_agg.total_escrow_USD_amt, 0) + COALESCE(adj_agg.total_adjustment_USD_amt, 0)
) / IF(
(user_agg.gp_account_billing_country = 'Korea'),
usd_to_krw,
IF(
(user_agg.gp_account_billing_country = 'Japan'),
usd_to_jpy,
IF(
(user_agg.gp_account_billing_country = 'China'),
usd_to_cny,
DECIMAL '1.00'
)
)
)
) negative_balance_seller_currency_amt,
"least"(
COALESCE(
esc_agg.earliest_billing_item_created_date,
date '9999-01-01'
),
COALESCE(
adj_agg.earliest_billing_item_created_date,
date '9999-01-01'
)
) earliest_billing_item_created_date,
"greatest"(
COALESCE(
esc_agg.earliest_billing_item_created_date,
date '1900-01-01'
),
COALESCE(
adj_agg.earliest_billing_item_created_date,
date '1900-01-01'
)
) last_billing_item_created_date,
"date_diff"(
'day',
"least"(
COALESCE(
esc_agg.earliest_billing_item_created_date,
date '9999-01-01'
),
COALESCE(
adj_agg.earliest_billing_item_created_date,
date '9999-01-01'
)
),
"date_add"('day', -1, current_date)
) negative_balance_cumulated_days,
user_agg.account_status,
user_agg.account_status_reason,
user_agg.account_status_reason_processed,
user_agg.account_status_changed_date,
user_agg.account_status_days
FROM
user_agg
LEFT JOIN (
SELECT
target_account_id,
esc.currency,
"max"(billing_item_created_date) last_billing_item_created_date,
"min"(
(
CASE
WHEN (
esc.billing_item_created_date > date '2019-08-31'
) THEN esc.billing_item_created_date
ELSE ev.ev_date
END
)
) earliest_billing_item_created_date,
"sum"((CAST(escrow_amount_inc AS double) / 100000)) total_escrow_local_amt,
"sum"(
(
(CAST(escrow_amount_inc AS double) / 100000) / IF((esc.currency = 'USD'), 1, local_to_usd)
)
) total_escrow_USD_amt
FROM
regcbbi_others.weekly_clawback_tracker_bfd_esc_reg_s0_tmp esc
INNER JOIN fx ON (esc.currency = fx.currency)
LEFT JOIN escrow_verified_date_before_2019_aug ev ON CAST(esc.escrow_entity_id AS varchar) = CAST(ev.order_id AS varchar)
GROUP BY
1,
2
) esc_agg ON (user_agg.user_id = esc_agg.target_account_id)
LEFT JOIN (
SELECT
adj.target_account_id,
adj.currency,
"max"(billing_item_created_date) last_billing_item_created_date,
"min"(
(
CASE
WHEN (
(
adj.billing_item_created_date > date '2019-08-31'
)
OR (ev.ev_date IS NULL)
) THEN adj.billing_item_created_date
ELSE ev.ev_date
END
)
) earliest_billing_item_created_date,
"sum"((CAST(adjustment_amount AS double) / 100000)) total_adjustment_local_amt,
"sum"(
(
(CAST(adjustment_amount AS double) / 100000) / IF((adj.currency = 'USD'), 1, local_to_usd)
)
) total_adjustment_USD_amt,
"sum"(
(
CASE
WHEN (
adj_reason.adjustment_reason_group = 'Ads Related'
) THEN (
(CAST(adjustment_amount AS double) / 100000) / IF((adj.currency = 'USD'), 1, local_to_usd)
)
ELSE 0
END
)
) Ads_adjustment_USD_amt,
"sum"(
(
CASE
WHEN (
adj_reason.adjustment_reason_group = 'Logistics / Shipping Related'
) THEN (
(CAST(adjustment_amount AS double) / 100000) / IF((adj.currency = 'USD'), 1, local_to_usd)
)
ELSE 0
END
)
) Logistics_Shipping_adjustment_USD_amt,
"sum"(
(
CASE
WHEN (
adj_reason.adjustment_reason_group = 'Order ASF Related'
) THEN (
(CAST(adjustment_amount AS double) / 100000) / IF((adj.currency = 'USD'), 1, local_to_usd)
)
ELSE 0
END
)
) Order_ASF_adjustment_USD_amt,
"sum"(
(
CASE
WHEN (
adj_reason.adjustment_reason_group = 'RR Related'
) THEN (
(CAST(adjustment_amount AS double) / 100000) / IF((adj.currency = 'USD'), 1, local_to_usd)
)
ELSE 0
END
)
) RR_adjustment_USD_amt,
"sum"(
(
CASE
WHEN (
adj_reason.adjustment_reason_group = 'Seller Fee Related'
) THEN (
(CAST(adjustment_amount AS double) / 100000) / IF((adj.currency = 'USD'), 1, local_to_usd)
)
ELSE 0
END
)
) Seller_Fee_adjustment_USD_amt,
"sum"(
(
CASE
WHEN (
adj_reason.adjustment_reason_group = 'Warehouse Related'
) THEN (
(CAST(adjustment_amount AS double) / 100000) / IF((adj.currency = 'USD'), 1, local_to_usd)
)
ELSE 0
END
)
) Warehouse_adjustment_USD_amt,
"sum"(
(
CASE
WHEN (
(adj_reason.adjustment_reason_group = 'Others')
OR (adj_reason.adjustment_reason_group IS NULL)
) THEN (
(CAST(adjustment_amount AS double) / 100000) / IF((adj.currency = 'USD'), 1, local_to_usd)
)
ELSE 0
END
)
) Other_adjustment_USD_amt
FROM
regcbbi_others.weekly_clawback_tracker_bfd_adj_reg_s0_tmp adj
INNER JOIN fx ON adj.currency = fx.currency
LEFT JOIN escrow_verified_date_before_2019_aug ev ON CAST(adj.escrow_entity_id AS varchar) = CAST(ev.order_id AS varchar)
LEFT JOIN adj_reason ON adj.reason_code_id = adj_reason.reason_code_id
AND adj.region = adj_reason.region
GROUP BY
1,
2
) adj_agg ON user_agg.user_id = adj_agg.target_account_id
LEFT JOIN fx ON COALESCE(esc_agg.currency, adj_agg.currency) = fx.currency
WHERE
COALESCE(esc_agg.total_escrow_local_amt, 0) + COALESCE(adj_agg.total_adjustment_local_amt, 0) < 0
ORDER BY
1 ASC,
2 ASC,
3 ASC,
4 ASC,
5 ASC