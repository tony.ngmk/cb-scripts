SELECT orders.country
, orders.Purchase_date


, orders.cb_orders
, try((CB_Orders + 0.0000)/Country_Orders) as cb_order_percentage 
, try((CB_Orders + 0.0000)/Country_Orders_excl) as cb_order_percentage_excl_special_shops 
, orders.cb_gmv
, try((DECIMAL '1.0000' * orders.CB_GMV) / orders.Country_GMV) as cb_gmv_percentage
, try((DECIMAL '1.0000' * orders.CB_GMV) / orders.Country_GMV_excl) as cb_gmv_percentage_excl_special_shops 
, orders.cb_item_sold 

, orders.Local_Orders
, orders.Local_GMV
, orders.Local_Item_Sold
, orders.Country_Orders
, orders.Country_GMV
, orders.Country_Item_Sold
, orders.Country_Orders_excl
, orders.Country_GMV_excl 


, sku.cb_live AS CB_New_SKU
, sku.country_live AS Country_New_SKU
, sku.cb_total_sku AS CB_Total_SKU
, sku.country_total_sku AS Country_Total_SKU
, try(DECIMAL '1.0000' * sku.cb_total_sku / sku.country_total_sku) AS CB_SKU_Percentage


FROM
(SELECT o.grass_region as country
, o.Purchase_date
, count(case when o.cb_option = 1 AND gp_account_billing_country = 'China' AND is_bi_excluded = 0 THEN o.orderid ELSE null END) CNCB_Orders
, sum (case when o.cb_option = 1 AND gp_account_billing_country = 'China' AND is_bi_excluded = 0 THEN gmv_usd ELSE null END) CNCB_GMV
, sum (case when o.cb_option = 1 AND gp_account_billing_country = 'China' AND is_bi_excluded = 0 THEN amount ELSE null END) CNCB_Item_Sold
, count(case when o.cb_option = 1 AND is_bi_excluded = 0 THEN o.orderid ELSE null END) CB_Orders
, sum (case when o.cb_option = 1 AND is_bi_excluded = 0 THEN gmv_usd ELSE null END) CB_GMV
, sum (case when o.cb_option = 1 AND is_bi_excluded = 0 THEN amount ELSE null END) CB_Item_Sold
, count(CASE WHEN o.cb_option = 0 AND is_bi_excluded = 0 THEN o.orderid ELSE null END) Local_Orders
, sum (CASE WHEN o.cb_option = 0 AND is_bi_excluded = 0 THEN o.gmv_usd ELSE null END) Local_GMV
, sum (CASE WHEN o.cb_option = 0 AND is_bi_excluded = 0 THEN amount ELSE null END) Local_Item_Sold
, count(o.orderid) Country_Orders
, sum (o.gmv_usd) Country_GMV
, sum (amount) Country_Item_Sold
, count(CASE WHEN is_bi_excluded = 0 THEN o.orderid ELSE NULL END) Country_Orders_excl
, sum (CASE WHEN is_bi_excluded = 0 THEN o.gmv_usd ELSE NULL END) Country_GMV_excl
-- , sum (CASE WHEN is_bi_excluded = 0 THEN amount ELSE NULL END) Country_Item_Sold 
FROM
(
select order_id as orderid
, grass_region
, shop_id as shopid 
, date(from_unixtime(create_timestamp)) as Purchase_date
, is_cb_shop as cb_option
, is_bi_excluded
, sum(gmv_usd) as gmv_usd
, sum(item_amount) as amount 
from mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
where tz_type = 'local' 
and grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL','PL', 'FR', 'ES')
and is_placed = 1 
and date(from_unixtime(create_timestamp)) BETWEEN date_add('day', -30, current_date) AND date_add('day', -1, current_date)
--and is_bi_excluded = 0
group by 1, 2, 3, 4, 5, 6
) o
LEFT JOIN 
(select * from regbd_sf.cb__seller_index_tab where grass_region != '') x ON o.shopid = cast(x.child_shopid AS bigint)
group by 1, 2 
) orders
LEFT JOIN 
(
SELECT
country
, create_time
, cb_live
, cncb_live
, country_live
, sum(cb_live) OVER (PARTITION BY country ORDER BY create_time ASC) cb_total_sku
, sum(cncb_live) OVER (PARTITION BY country ORDER BY create_time ASC) cncb_total_sku 
, sum(country_live) OVER (PARTITION BY country ORDER BY create_time ASC) country_total_sku
FROM
(
SELECT
i.grass_region country
, date(from_unixtime(i.ctime)) create_time
, count(DISTINCT (CASE WHEN u.is_cb_shop = 1 THEN itemid ELSE null END)) cb_live 
, count(DISTINCT (CASE WHEN u.is_cb_shop = 1 AND x.gp_account_billing_country = 'China' THEN itemid ELSE null END)) cncb_live
, count(DISTINCT itemid) country_live
FROM
(
SELECT grass_region
, create_timestamp as ctime
, shop_id as shopid
, item_id as itemid
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL','PL', 'FR', 'ES')
and status = 1
and stock > 0
and tz_type = 'local'
and grass_date = current_date - interval '1' day
) i
JOIN 
(
SELECT grass_region, shop_id, is_cb_shop
FROM mp_user.dim_shop__reg_s0_live 
WHERE grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL','PL', 'FR', 'ES')
and grass_date = date_add('day', -1, current_date) 
and tz_type = 'local'
and status = 1
and is_holiday_mode != 1
) u ON i.shopid = u.shop_id
/**
JOIN 
(
SELECT grass_region, shop_id, user_id
FROM mp_user.dim_user__reg_s0_live 
WHERE grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL','PL', 'FR', 'ES')
and grass_date = date_add('day', -1, current_date) 
and tz_type = 'local'
and status = 1
--and date(from_unixtime(last_login_timestamp)) >= date_add('day', -7, current_date)
) u1 ON u.shop_id = u1.shop_id
JOIN 
(
SELECT user_id as userid
, DATE(from_unixtime(last_login_timestamp_td)) AS last_login
FROM mp_user.dws_user_login_td_account_info__reg_s0_live 
WHERE
tz_type = 'local'
AND grass_date = current_date - interval '1' day 
AND date(from_unixtime(last_login_timestamp_td)) >= date_add('day', -7, current_date)
)l on u1.user_id = l.userid
**/
LEFT JOIN 
(
SELECT child_shopid, gp_account_billing_country
FROM regbd_sf.cb__seller_index_tab 
WHERE grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL','PL', 'FR', 'ES')
) x ON (u.shop_id = CAST(x.child_shopid AS bigint))
GROUP BY 1, 2
) 
) sku ON orders.purchase_date = sku.create_time AND orders.country = sku.country 
ORDER BY orders.purchase_date DESC, orders.country DESC