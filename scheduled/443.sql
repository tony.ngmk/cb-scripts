insert into nexus.test_d3d7c8415572e5b4cc04487e23954c50e842a67fd38d916bda0209eb544c7c9a_f0b257518e0a947faeec42d00e4c4e1e /** 
Name: Seller level Ops performance Monitor 
Function: CB Benchmark M-1 
Notes: 

Date Developer Activity 
2022-05-25 Sharmaine Create 


**********************************/ 
--1) Get Orders placed in M-1 
WITH M1_orders AS(
SELECT
o.grass_region,
o.order_id,
o.shop_id,
DATE(FROM_UNIXTIME(o.create_timestamp)) AS order_place_date,
o.order_be_status_id,
o.cancel_reason_id,
o.cancel_reason,
o.cancel_user_id,
o.buyer_id,
o.seller_id,
CASE
WHEN o.cancel_user_id = o.buyer_id THEN 'buyer'
WHEN o.cancel_user_id = o.seller_id THEN 'seller'
WHEN o.cancel_user_id = -1 THEN 'system'
END AS cancel_by
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live o
WHERE
o.is_bi_excluded = 0
AND DATE(FROM_UNIXTIME(o.create_timestamp)) BETWEEN date_trunc('month', current_date) - interval '1' month
AND DATE_TRUNC('month', current_date) - interval '1' day -- filter M-1 placed orders
AND o.grass_region IN ('TH', 'MY', 'ID', 'PH', 'VN') -- grass_regions requested by Biz 
AND tz_type = 'local'
AND o.is_cb_shop = 1
),
gross_net_orders_summary AS(
SELECT
grass_region,
COUNT(DISTINCT order_id) AS M1_gross_orders,
COUNT (
DISTINCT (
CASE
WHEN order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN order_id
ELSE NULL
END
)
) AS M1_net_orders
FROM
M1_orders
GROUP BY
1
),
--2) Get RR orders corresponding to orders placed in M-1 
return_refund_summary AS (
SELECT
r.grass_region,
COUNT (DISTINCT orderid) AS M1_RR_accepted_orders,
COUNT (
DISTINCT (
CASE
WHEN reason IN (1, 2, 103, 3, 106, 107) then orderid
ELSE NULL
END
)
) AS M1_RR_accepted_and_not_buyer_fault_orders
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live r
INNER JOIN M1_orders ON M1_orders.order_id = r.orderid
WHERE
-- Only include status 2 and 5 as the statuses which are accepted. Other statuses still have chance for RR status to terminate at closed or cancelled 
status IN (2, 5) -- 0: deleted, 1: requested, 2: accepted, 3: cancelled, 4: judging, 5: refund_paid, 6: closed, 7: return_processing (start reverse logistics), 8: return_seller_dispute (seller dispute after item is returned)
AND r.grass_region IN ('TH', 'MY', 'ID', 'PH', 'VN') -- grass_regions requested by Biz
GROUP BY
1
),
--3) Cancelled Orders 
cancelled_orders_summary AS(
SELECT
grass_region,
COUNT (
DISTINCT CASE
WHEN cancel_by = 'seller' THEN order_id
ELSE NULL
END
) AS M1_seller_initiated_cancelled_orders,
COUNT (
DISTINCT CASE
WHEN cancel_by = 'buyer' THEN order_id
ELSE NULL
END
) AS M1_buyer_initiated_cancelled_orders,
COUNT (
DISTINCT CASE
WHEN cancel_by = 'system' THEN order_id
ELSE NULL
END
) AS M1_system_initiated_cancelled_orders
FROM
M1_orders
GROUP BY
1
)
SELECT
o.grass_region,
o.M1_gross_orders,
o.M1_net_orders,
rr.M1_RR_accepted_orders,
rr.M1_RR_accepted_and_not_buyer_fault_orders,
c.M1_seller_initiated_cancelled_orders,
c.M1_buyer_initiated_cancelled_orders,
c.M1_system_initiated_cancelled_orders
FROM
gross_net_orders_summary o
LEFT JOIN return_refund_summary rr ON rr.grass_region = o.grass_region
LEFT JOIN cancelled_orders_summary c ON c.grass_region = o.grass_region
ORDER BY
o.grass_region ASC