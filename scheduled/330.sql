/********************************** 
Ingested Table Name: 
regcbbi_general.negative_escrow_project_tracker_bfd_breakdown 
**********************************/ 
--Get action_time_msec
WITH audit as
(SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_sg_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_id_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_my_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_tw_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_th_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_ph_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_vn_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_br_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_mx_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_co_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_cl_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_pl_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_es_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_fr_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT user_id AS target_pk, case when new_status = 1 then 'normal' when new_status = 2 then 'banned' when new_status = 3 then 'frozen' else null end as audit_status, remarks as reason, ctime * 1000 as action_time_msec FROM marketplace.shopee_fraud_treatment_center_db__treatmentcenter_user_account_oplog_tab__reg_daily_s0_live WHERE grass_region != '' AND new_status IN (1, 2, 3)
)


--Get users which status are in B/F/D
, user as
(SELECT grass_region, user_id, user_name, shop_id, account_status, account_status_reason, account_status_date
FROM
(SELECT grass_region, user_id, user_name, shop_id, account_status, audit_status, lower(reason) as account_status_reason, date(from_unixtime(action_time_msec/1000)) as account_status_date,
rank() over (partition by target_pk, audit_status order by action_time_msec desc) as action_time_rank
FROM audit a
JOIN
(SELECT distinct grass_region, user_id, user_name, shop_id, IF(status = 0, 'deleted', IF(status = 2, 'banned', 'frozen')) as account_status
FROM mp_user.dim_user__reg_s0_live
WHERE is_cb_shop = 1 AND grass_date = date_add('day', -1, current_date)
AND tz_type = 'local' AND status in (0,2,3)
AND grass_region != ''
) u
ON target_pk = u.user_id
)
WHERE audit_status = account_status
AND action_time_rank = 1
)


, sip AS (
SELECT affi_shopid
, t1.cb_option
, t1.country AS psite
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1 
JOIN (
SELECT distinct affi_shopid, affi_userid, affi_username, mst_shopid, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status != 3
AND grass_region != ''
) AS t2 on t1.shopid = t2.mst_shopid
)
, seller_index AS (
SELECT CAST(si.child_shopid as BIGINT) as shop_id
, si.gp_account_name
, si.gp_account_id
, si.gp_account_owner
, si.child_account_owner
, si.gp_account_owner_userrole_name
, si.child_account_owner_userrole_name
, si.gp_account_owner_email
, si.child_userid
, CASE WHEN sip.affi_shopid is not NULL THEN 1 ELSE 0 END as is_sip
, CASE WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
--WHEN cb_option = 0 THEN 'Local'
WHEN sip.affi_shopid IS NOT NULL AND sip.cb_option = 0 THEN 'Local SIP'
--WHEN sip.affi_shopid IS NOT NULL AND sip.psite IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
--WHEN sip.affi_shopid IS NOT NULL AND si.gp_account_billing_country = 'Korea' AND sip.psite IN ('SG') THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL THEN 'CB SIP'
when si.gp_account_billing_country = 'Korea' then 'KRCB'
when si.gp_account_billing_country = 'Japan' then 'JPCB'
when si.gp_account_billing_country is NULL or si.gp_account_billing_country != 'China' then 'Others'
when si.child_account_owner_userrole_name like '%HKCB%' then 'HKCB'
when si.child_account_owner_userrole_name like '%BD_%' then 'CNCB BD'
when si.child_account_owner_userrole_name like '%UST_%' then 'CNCB UST'
when si.child_account_owner_userrole_name like '%ST_%' THEN 'CNCB ST'
when si.child_account_owner_userrole_name like '%MT_%' THEN 'CNCB MT'
when si.child_account_owner_userrole_name like '%LT%' THEN 'CNCB LT'
WHEN si.gp_account_billing_country = 'China' THEN COALESCE(CONCAT('CNCB ', si.cb_group), 'CNCB Others')
else 'CNCB Others' END as seller_type
, gp_account_billing_country
FROM regbd_sf.cb__seller_index_tab AS si 
LEFT JOIN sip ON CAST(si.child_shopid AS BIGINT) = sip.affi_shopid
LEFT JOIN (
SELECT DISTINCT shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date BETWEEN DATE_ADD('day', -15, current_date) AND DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = CAST(si.child_shopid as BIGINT)
WHERE si.grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
)


--Further fine-tuning the user profile
, user_agg AS
(select
user.shop_id,
user.user_id,
IF(user.account_status = 'deleted', 'Deleted Account', seller_index.seller_type) as seller_type,
IF(user.account_status = 'deleted', 'Deleted Account', seller_index.gp_account_name) as gp_account_name,
IF(user.account_status = 'deleted', 'Deleted Account', seller_index.gp_account_owner_email) as gp_account_owner_email,
user.user_name as shop_name,
user.grass_region as region,
user.account_status,
user.account_status_reason,
case when regexp_like(user.account_status_reason,'prohibit|counterfeit|fake') then 'Counterfeit / Fake / Prohibited'
when regexp_like(user.account_status_reason,'Inactive') then 'Inactive'
when regexp_like(user.account_status_reason,'scam|fraud') then 'Scam / Fraud'
when regexp_like(user.account_status_reason,'low price|brush') then 'Low Price / Order Brushing'
when regexp_like(user.account_status_reason,'gp.*?limit|limit.*?gp') then 'GP Limit'
when regexp_like(user.account_status_reason,'shadow') then 'Shadow Account'
when regexp_like(user.account_status_reason,'kyc') then 'KYC'
when regexp_like(user.account_status_reason,'seller request') then 'Seller Request'
else 'Others' end
as account_status_reason_processed,
user.account_status_date as account_status_changed_date,
date_diff('day', user.account_status_date, date_add('day', -1, current_date)) as account_status_days,
seller_index.gp_account_billing_country
from user join seller_index on user.user_id = CAST(seller_index.child_userid as BIGINT)
)


--Assign adjustment reasons
, adj_reason AS
(SELECT *,
CASE WHEN lower(scenario) like '%paid ads%' or scenario like '%Ads%' THEN 'Ads Related'
WHEN lower(scenario) like '%refund%' THEN 'RR Related'
WHEN lower(scenario) like '%shipping fee%' or lower(scenario) like '%logistic%' or lower(scenario) like '%asf%' THEN 'Logistics / Shipping Related'
WHEN lower(scenario) like '%transaction fee%' or lower(scenario) like '%service fee%' or lower(scenario) like '%commission fee%' THEN 'Seller Fee Related'
WHEN lower(scenario) like '%warehouse%' THEN 'Warehouse Related'
WHEN reason_code_id IN (63,64) THEN 'CB SIP Reset Adjustment'
ELSE 'Others' END AS adjustment_reason_group
FROM
(SELECT 'SG' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_sg_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'ID' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_id_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'MY' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_my_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'TW' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_tw_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'TH' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_th_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'PH' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_ph_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'VN' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_vn_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'BR' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_br_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'MX' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_mx_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'CO' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_co_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'CL' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_cl_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'PL' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_pl_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'ES' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_es_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
UNION SELECT 'FR' as region, reason_code_id, scenario FROM marketplace.shopee_settlement_system_fr_db__settlement_adjustment_reason_code_tab__reg_daily_s0_live
)
),


--Filter date
escrow_verified_date_before_2019_aug as
(SELECT order_id, date(from_unixtime(escrow_verified_timestamp)) as ev_date
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE tz_type = 'local'
AND grass_region = ''
AND is_cb_shop = 1
AND escrow_verified_timestamp <= to_unixtime(date'2019-08-31')
)


--Exchange rates
, fx as
(SELECT DISTINCT currency, local_to_usd, usd_to_cny, usd_to_krw, usd_to_jpy
FROM
(SELECT currency, grass_date, exchange_rate as local_to_usd FROM mp_order.dim_exchange_rate__reg_s0_live WHERE grass_date = date_add('day',-1,current_date)) fx_usd
JOIN
(SELECT 1 / exchange_rate as usd_to_cny, grass_date FROM mp_order.dim_exchange_rate__reg_s0_live WHERE grass_date = date_add('day',-1,current_date) AND currency = 'CNY') fx_cny
ON fx_usd.grass_date = fx_cny.grass_date
JOIN
(SELECT 1 / exchange_rate as usd_to_krw, grass_date FROM mp_order.dim_exchange_rate__reg_s0_live WHERE grass_date = date_add('day',-1,current_date) AND currency = 'KRW') fx_krw
ON fx_usd.grass_date = fx_krw.grass_date
JOIN
(SELECT 1 / exchange_rate as usd_to_jpy, grass_date FROM mp_order.dim_exchange_rate__reg_s0_live WHERE grass_date = date_add('day',-1,current_date) AND currency = 'JPY') fx_jpy
ON fx_usd.grass_date = fx_jpy.grass_date
)


--Shop level aggregation
, user_agg_shop AS(
SELECT *
FROM user_agg
left join
(select target_account_id,
esc.currency,
sum(cast(escrow_amount_inc as double) / 100000) as total_escrow_local_amt,
sum(cast(escrow_amount_inc as double) / 100000 / IF(esc.currency = 'USD', 1, local_to_usd)) as total_escrow_USD_amt
from regcbbi_others.weekly_clawback_tracker_bfd_esc_reg_s0_tmp esc
join fx
on esc.currency = fx.currency
left join escrow_verified_date_before_2019_aug ev
on cast(esc.escrow_entity_id as bigint) = ev.order_id
group by 1,2
) esc_agg
on user_agg.user_id = esc_agg.target_account_id
left join
(select adj.target_account_id,
adj.currency,
sum(cast(adjustment_amount as double) / 100000) as total_adjustment_local_amt,
sum(cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) as total_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Ads Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Ads_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Logistics / Shipping Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Logistics_Shipping_adjustment_USD_amt,
--sum(case when adj_reason.adjustment_reason_group = 'Order ASF Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Order_ASF_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'RR Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as RR_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Seller Fee Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Seller_Fee_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Warehouse Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Warehouse_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'CB SIP Reset Adjustment' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as SIP_reset_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Others' OR adj_reason.adjustment_reason_group is null then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Other_adjustment_USD_amt
from regcbbi_others.weekly_clawback_tracker_bfd_adj_reg_s0_tmp adj
join fx
on adj.currency = fx.currency
left join escrow_verified_date_before_2019_aug ev
on cast(adj.escrow_entity_id as bigint) = ev.order_id
left join adj_reason
on (adj.reason_code_id = adj_reason.reason_code_id and adj.region = adj_reason.region)
group by 1,2
) adj_agg
on user_agg.user_id = adj_agg.target_account_id
--left join fx
--on coalesce(esc_agg.currency, adj_agg.currency) = fx.currency


where coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0) < 0)


--Regular Biiling item level aggregation
, bill_item_agg AS(
SELECT coalesce(esc_target_account_id, adj_target_account_id) target_account_id, 
coalesce(total_escrow_local_amt,0) total_escrow_local_amt, 
coalesce(total_escrow_USD_amt,0) total_escrow_USD_amt,
coalesce(total_adjustment_local_amt,0) total_adjustment_local_amt,
coalesce(total_adjustment_USD_amt,0) total_adjustment_USD_amt,
coalesce(Ads_adjustment_USD_amt,0) Ads_adjustment_USD_amt,
coalesce(Logistics_Shipping_adjustment_USD_amt,0) Logistics_Shipping_adjustment_USD_amt,
coalesce(Warehouse_adjustment_USD_amt,0) Warehouse_adjustment_USD_amt,
coalesce(RR_adjustment_USD_amt,0) RR_adjustment_USD_amt,
coalesce(Seller_Fee_adjustment_USD_amt,0) Seller_Fee_adjustment_USD_amt,
coalesce(SIP_reset_adjustment_USD_amt,0) SIP_reset_adjustment_USD_amt,
coalesce(Other_adjustment_USD_amt,0) Other_adjustment_USD_amt
FROM (select esc.target_account_id AS esc_target_account_id,
esc.currency,
sum(cast(escrow_amount_inc as double) / 100000) as total_escrow_local_amt,
sum(cast(escrow_amount_inc as double) / 100000 / IF(esc.currency = 'USD', 1, local_to_usd)) as total_escrow_USD_amt
from regcbbi_others.weekly_clawback_tracker_bfd_esc_reg_s0_tmp esc
join fx
on esc.currency = fx.currency
left join escrow_verified_date_before_2019_aug ev
on cast(esc.escrow_entity_id as bigint) = ev.order_id
--WHERE escrow_amount < 0
group by 1,2
) esc_agg
full outer join
(select adj.target_account_id AS adj_target_account_id,
adj.currency,
sum(cast(adjustment_amount as double) / 100000) as total_adjustment_local_amt,
sum(cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) as total_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Ads Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Ads_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Logistics / Shipping Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Logistics_Shipping_adjustment_USD_amt,
--sum(case when adj_reason.adjustment_reason_group = 'Order ASF Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Order_ASF_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'RR Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as RR_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Seller Fee Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Seller_Fee_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Warehouse Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Warehouse_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'CB SIP Reset Adjustment' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as SIP_reset_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Others' OR adj_reason.adjustment_reason_group is null then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Other_adjustment_USD_amt
from regcbbi_others.weekly_clawback_tracker_bfd_adj_reg_s0_tmp adj
join fx
on adj.currency = fx.currency
left join escrow_verified_date_before_2019_aug ev
on cast(adj.escrow_entity_id as bigint) = ev.order_id
left join adj_reason
on (adj.reason_code_id = adj_reason.reason_code_id and adj.region = adj_reason.region)
--WHERE adjustment_amount < 0
group by 1,2
) adj_agg
on esc_agg.esc_target_account_id = adj_agg.adj_target_account_id
--left join fx
--on coalesce(esc_agg.currency, adj_agg.currency) = fx.currency
--where esc_agg.esc_target_account_id IN (SELECT user_id FROM user_agg_shop)
)


--Negative Biiling item level aggregation
, neg_bill_item_agg AS(
SELECT coalesce(esc_target_account_id, adj_target_account_id) target_account_id, 
coalesce(total_escrow_local_amt,0) total_escrow_local_amt, 
coalesce(total_escrow_USD_amt,0) total_escrow_USD_amt,
coalesce(total_adjustment_local_amt,0) total_adjustment_local_amt,
coalesce(total_adjustment_USD_amt,0) total_adjustment_USD_amt,
coalesce(Ads_adjustment_USD_amt,0) Ads_adjustment_USD_amt,
coalesce(Logistics_Shipping_adjustment_USD_amt,0) Logistics_Shipping_adjustment_USD_amt,
coalesce(Warehouse_adjustment_USD_amt,0) Warehouse_adjustment_USD_amt,
coalesce(RR_adjustment_USD_amt,0) RR_adjustment_USD_amt,
coalesce(Seller_Fee_adjustment_USD_amt,0) Seller_Fee_adjustment_USD_amt,
coalesce(SIP_reset_adjustment_USD_amt,0) SIP_reset_adjustment_USD_amt,
coalesce(Other_adjustment_USD_amt,0) Other_adjustment_USD_amt
FROM (select esc.target_account_id AS esc_target_account_id,
esc.currency,
sum(cast(escrow_amount_inc as double) / 100000) as total_escrow_local_amt,
sum(cast(escrow_amount_inc as double) / 100000 / IF(esc.currency = 'USD', 1, local_to_usd)) as total_escrow_USD_amt
from regcbbi_others.weekly_clawback_tracker_bfd_esc_reg_s0_tmp esc
join fx
on esc.currency = fx.currency
left join escrow_verified_date_before_2019_aug ev
on cast(esc.escrow_entity_id as bigint) = ev.order_id
WHERE escrow_amount < 0
group by 1,2
) esc_agg
full outer join
(select adj.target_account_id AS adj_target_account_id,
adj.currency,
sum(cast(adjustment_amount as double) / 100000) as total_adjustment_local_amt,
sum(cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) as total_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Ads Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Ads_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Logistics / Shipping Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Logistics_Shipping_adjustment_USD_amt,
--sum(case when adj_reason.adjustment_reason_group = 'Order ASF Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Order_ASF_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'RR Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as RR_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Seller Fee Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Seller_Fee_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Warehouse Related' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Warehouse_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'CB SIP Reset Adjustment' then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as SIP_reset_adjustment_USD_amt,
sum(case when adj_reason.adjustment_reason_group = 'Others' OR adj_reason.adjustment_reason_group is null then (cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) else 0 end) as Other_adjustment_USD_amt
from regcbbi_others.weekly_clawback_tracker_bfd_adj_reg_s0_tmp adj
join fx
on adj.currency = fx.currency
left join escrow_verified_date_before_2019_aug ev
on cast(adj.escrow_entity_id as bigint) = ev.order_id
left join adj_reason
on (adj.reason_code_id = adj_reason.reason_code_id and adj.region = adj_reason.region)
WHERE adjustment_amount < 0
group by 1,2
) adj_agg
on esc_agg.esc_target_account_id = adj_agg.adj_target_account_id)


--Main Query
(SELECT
current_date AS report_extraction,
user_agg_shop.region AS region,
COUNT(DISTINCT user_agg_shop.shop_id) num_neg_bal_shops,
CAST(COUNT(DISTINCT user_agg_shop.gp_account_name) AS VARCHAR) AS num_bfd_neg_bal_gp,
SUM(coalesce(user_agg_shop.total_escrow_USD_amt, 0) + coalesce(user_agg_shop.total_adjustment_USD_amt, 0)) neg_bal_usd,
COUNT(DISTINCT CASE WHEN account_status_days > 182 THEN user_agg_shop.shop_id ELSE NULL END) num_neg_bal_shops_threshold,
CAST(COUNT(DISTINCT CASE WHEN account_status_days > 182 THEN user_agg_shop.shop_id ELSE NULL END) AS DOUBLE)/CAST(COUNT(DISTINCT user_agg_shop.shop_id) AS DOUBLE) threshold_vs_neg_bal_shops,
CAST(COUNT(DISTINCT CASE WHEN account_status_days > 182 THEN user_agg_shop.gp_account_name ELSE NULL END) AS VARCHAR) AS num_neg_bal_gp_threshold,
CAST(CAST(COUNT(DISTINCT CASE WHEN account_status_days > 182 THEN user_agg_shop.gp_account_name ELSE NULL END) AS DOUBLE)/CAST(COUNT(DISTINCT user_agg_shop.gp_account_name) AS DOUBLE) AS VARCHAR) AS threshold_vs_neg_bal_gp,
SUM(CASE WHEN account_status_days > 182 THEN coalesce(user_agg_shop.total_escrow_USD_amt, 0) + coalesce(user_agg_shop.total_adjustment_USD_amt, 0) ELSE 0 END) neg_bal_usd_threshold,
CAST(SUM(CASE WHEN account_status_days > 182 THEN coalesce(user_agg_shop.total_escrow_USD_amt, 0) + coalesce(user_agg_shop.total_adjustment_USD_amt, 0) ELSE 0 END) AS DOUBLE)/CAST(SUM(coalesce(user_agg_shop.total_escrow_USD_amt, 0) + coalesce(user_agg_shop.total_adjustment_USD_amt, 0))AS DOUBLE) threshold_vs_neg_bal_amt,


SUM(bill_item_agg.total_escrow_USD_amt) escrow,
CAST(SUM(bill_item_agg.total_escrow_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + coalesce(bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) escrow_pctg, 
SUM(bill_item_agg.Ads_adjustment_USD_amt) ads_fees,
CAST(SUM(bill_item_agg.Ads_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + coalesce(bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) ads_pctg, 
SUM(bill_item_agg.Warehouse_adjustment_USD_amt) wh_fees,
CAST(SUM(bill_item_agg.Warehouse_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + coalesce(bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) wh_pctg, 
SUM(bill_item_agg.Logistics_Shipping_adjustment_USD_amt) log_fees,
CAST(SUM(bill_item_agg.Logistics_Shipping_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + coalesce(bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) log_pctg, 
SUM(bill_item_agg.RR_adjustment_USD_amt) rr_fees,
CAST(SUM(bill_item_agg.RR_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + coalesce(bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) rr_pctg, 
SUM(bill_item_agg.Seller_Fee_adjustment_USD_amt) seller_fees,
CAST(SUM(bill_item_agg.Seller_Fee_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + coalesce(bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) seller_fee_pctg, 
SUM(bill_item_agg.SIP_reset_adjustment_USD_amt) sip_reset_fees,
CAST(SUM(bill_item_agg.SIP_reset_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + coalesce(bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) sip_reset_fee_pctg, 
SUM(bill_item_agg.Other_adjustment_USD_amt) other_fees,
CAST(SUM(bill_item_agg.Other_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + coalesce(bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) other_pctg, 


SUM(coalesce(bill_item_agg.total_escrow_USD_amt, 0) + 
coalesce(bill_item_agg.Ads_adjustment_USD_amt, 0) +
coalesce(bill_item_agg.Warehouse_adjustment_USD_amt, 0) +
coalesce(bill_item_agg.Logistics_Shipping_adjustment_USD_amt, 0) +
coalesce(bill_item_agg.RR_adjustment_USD_amt, 0) +
coalesce(bill_item_agg.Seller_Fee_adjustment_USD_amt, 0) +
coalesce(bill_item_agg.SIP_reset_adjustment_USD_amt, 0) +
coalesce(bill_item_agg.Other_adjustment_USD_amt, 0)) total,


SUM(neg_bill_item_agg.total_escrow_USD_amt) neg_escrow,
CAST(SUM(neg_bill_item_agg.total_escrow_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + coalesce(neg_bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) neg_escrow_pctg, 
SUM(neg_bill_item_agg.Ads_adjustment_USD_amt) neg_ads_fees,
CAST(SUM(neg_bill_item_agg.Ads_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + coalesce(neg_bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) neg_ads_pctg, 
SUM(neg_bill_item_agg.Warehouse_adjustment_USD_amt) neg_wh_fees,
CAST(SUM(neg_bill_item_agg.Warehouse_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + coalesce(neg_bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) neg_wh_pctg, 
SUM(neg_bill_item_agg.Logistics_Shipping_adjustment_USD_amt) neg_log_fees,
CAST(SUM(neg_bill_item_agg.Logistics_Shipping_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + coalesce(neg_bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) neg_log_pctg, 
SUM(neg_bill_item_agg.RR_adjustment_USD_amt) neg_rr_fees,
CAST(SUM(neg_bill_item_agg.RR_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + coalesce(neg_bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) neg_rr_pctg, 
SUM(neg_bill_item_agg.Seller_Fee_adjustment_USD_amt) neg_seller_fees,
CAST(SUM(neg_bill_item_agg.Seller_Fee_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + coalesce(neg_bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) neg_seller_fee_pctg, 
SUM(neg_bill_item_agg.SIP_reset_adjustment_USD_amt) neg_sip_reset_fees,
CAST(SUM(neg_bill_item_agg.SIP_reset_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + coalesce(neg_bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) neg_sip_reset_fee_pctg, 
SUM(neg_bill_item_agg.Other_adjustment_USD_amt) neg_other_fees,
CAST(SUM(neg_bill_item_agg.Other_adjustment_USD_amt) AS DOUBLE)/CAST(SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + coalesce(neg_bill_item_agg.total_adjustment_USD_amt, 0)) AS DOUBLE) neg_other_pctg, 


SUM(coalesce(neg_bill_item_agg.total_escrow_USD_amt, 0) + 
coalesce(neg_bill_item_agg.Ads_adjustment_USD_amt, 0) +
coalesce(neg_bill_item_agg.Warehouse_adjustment_USD_amt, 0) +
coalesce(neg_bill_item_agg.Logistics_Shipping_adjustment_USD_amt, 0) +
coalesce(neg_bill_item_agg.RR_adjustment_USD_amt, 0) +
coalesce(neg_bill_item_agg.Seller_Fee_adjustment_USD_amt, 0) +
coalesce(neg_bill_item_agg.SIP_reset_adjustment_USD_amt, 0) +
coalesce(neg_bill_item_agg.Other_adjustment_USD_amt, 0)) neg_total


FROM user_agg_shop 
LEFT JOIN bill_item_agg ON user_agg_shop.user_id = bill_item_agg.target_account_id
LEFT JOIN neg_bill_item_agg ON user_agg_shop.user_id = neg_bill_item_agg.target_account_id
where coalesce(user_agg_shop.total_escrow_local_amt, 0) + coalesce(user_agg_shop.total_adjustment_local_amt, 0) < 0
GROUP BY 1,2)


UNION


(SELECT
current_date AS report_extraction,
'CB Total' AS region,
COUNT(DISTINCT user_agg_shop.shop_id) num_neg_bal_shops,
CAST(COUNT(DISTINCT user_agg_shop.gp_account_name) AS VARCHAR) AS num_bfd_neg_bal_gp,
SUM(coalesce(user_agg_shop.total_escrow_USD_amt, 0) + coalesce(user_agg_shop.total_adjustment_USD_amt, 0)) neg_bal_u