insert into nexus.test_aab352f7b4fdd1bccbcb992b0d5dc3e971fb37e9ba32ed3f6cda07a400879907_bdfec781fa04f13b28a9455009e276f3 SELECT DISTINCT
grass_region
, mst_counrty
, "year"(order_c_date) year
, "month"(order_c_date) month
, "week"(order_c_date) week
, order_c_date grass_date
, tt.cb_option is_cb_sip
, t3.main_category
, t3.sub_category
, t3.level3_category
, t3.level1_global_be_category
, t3.level2_global_be_category
, t3.level3_global_be_category
, t3.level4_global_be_category
, t3.level5_global_be_category
, shop_name
, item_name
, t1.affi_shopid
, t1.affi_itemid
, promo_source
, item_status
, item_price_usd
, status
, seller_status
, is_pre_order
, holiday_mode_on
, (CASE WHEN (grass_region = 'TW') THEN "concat"('http://shopee.tw/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'MY') THEN "concat"('http://shopee.com.my/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'BR') THEN "concat"('http://shopee.com.br/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'ID') THEN "concat"('http://shopee.co.id/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'TH') THEN "concat"('http://shopee.co.th/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'PH') THEN "concat"('http://shopee.ph/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'VN') THEN "concat"('http://shopee.vn/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar))
WHEN (grass_region = 'SG') THEN "concat"('http://shopee.sg/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar))
WHEN (grass_region = 'PL') THEN "concat"('http://shopee.pl/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'CL') THEN "concat"('http://shopee.cl/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'CO') THEN "concat"('http://shopee.com.co/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'MX') THEN "concat"('http://shopee.com.mx/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'FR') THEN "concat"('http://shopee.fr/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
WHEN (grass_region = 'ES') THEN "concat"('http://shopee.es/product/', CAST(t1.affi_shopid AS varchar), '/', CAST(t1.affi_itemid AS varchar)) 
ELSE 'NA' END) product_link
, d_1_stock
, "round"("sum"(orders), 3) orders
, "sum"(quantity) quantity
, "round"("sum"(gmv_usd),3) gmv_usd
, "round"(TRY(("sum"(gmv_usd) / "sum"(orders))), 3) abs_usd
, "max"(current_date) last_updated_on
FROM
(
SELECT DISTINCT
item_id itemid
, name item_name
, level1_category main_category
, level2_category sub_category
, level3_category
, level1_global_be_category
, level2_global_be_category
, level3_global_be_category
, level4_global_be_category
, level5_global_be_category
, status item_status
, price_usd item_price_usd
, seller_status
, is_pre_order
, stock as d_1_stock


FROM
mp_item.dim_item__reg_s0_live
WHERE grass_region ='MY' AND tz_type = 'local' AND is_cb_shop = 1 and grass_date=current_date - INTERVAL '1' DAY )
t3
INNER JOIN 


(
SELECT DISTINCT
affi_itemid
, affi_shopid
, mst_itemid
FROM
marketplace.shopee_sip_v2_db__msku_map_tab__reg_daily_s0_live
WHERE affi_country = 'MY'
) t1 ON (t1.affi_itemid = t3.itemid)
INNER JOIN (
SELECT DISTINCT
affi_shopid
, mst_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE grass_region <>'' and sip_shop_status<>3
) ttt ON (t1.affi_shopid = ttt.affi_shopid)
INNER JOIN (
SELECT DISTINCT
shopid
, cb_option
, country mst_counrty
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live


) tt ON (ttt.mst_shopid = tt.shopid)
INNER JOIN (
SELECT DISTINCT
shop_id shopid
, shop_name shop_name
, grass_region
, status
, is_holiday_mode holiday_mode_on
FROM
regcbbi_others.shop_profile 
WHERE grass_region = 'MY' AND tz_type = 'local' AND is_cb_shop = 1 and grass_date=current_date - INTERVAL '1' DAY )
t2 ON (t1.affi_shopid = t2.shopid)
INNER JOIN (
SELECT DISTINCT
item_id itemid
, item_promotion_source promo_source
, case when grass_region in ('ID') then date(from_unixtime(create_timestamp, 'Asia/Jakarta'))
when grass_region in ('TH') then date(from_unixtime(create_timestamp, 'Asia/Bangkok'))
when grass_region in ('VN') then date(from_unixtime(create_timestamp, 'Asia/Ho_Chi_Minh'))
when grass_region = 'BR' then date(from_unixtime(create_timestamp, 'America/Sao_Paulo'))
when grass_region = 'CL' then date(from_unixtime(create_timestamp, 'America/Santiago')) --Chile
when grass_region = 'CO' then date(from_unixtime(create_timestamp, 'America/Bogota')) --Colombia
when grass_region in ('PL', 'ES', 'FR') then date(from_unixtime(create_timestamp, 'CET')) --EU
when grass_region in ('IN') then date(from_unixtime(create_timestamp, 'Asia/Kolkata')) --India
else date(from_unixtime(create_timestamp)) end as order_c_date --SG timezone for SG/MY/TW/PH and unknown countries
, "sum"(order_fraction) orders
, "sum"(gmv_usd) gmv_usd
, "sum"(item_amount) quantity
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE tz_type='local' 
and grass_region = 'MY' 
and is_placed=1 
and grass_date >= current_date - INTERVAL '31' DAY 
and from_unixtime(create_timestamp) >= current_date - INTERVAL '31' DAY
and is_cb_shop = 1 
GROUP BY 1, 2, 3) t6 ON t3.itemid = t6.itemid
where quantity>0 and order_c_date >= current_date - INTERVAL '30' DAY
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,20,21,22,23,24,25,26,27,28