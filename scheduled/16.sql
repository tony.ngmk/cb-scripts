insert into nexus.test_f465f41d5a8985493c50f20cfe6564056f1d466dda1c2d7e31b29f982efada64_0dc9839792d508e8b23deb214cf2320e WITH
sip AS (
SELECT affi_shopid
, t1.cb_option
, t1.country AS psite
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1 
INNER JOIN (
SELECT distinct affi_shopid, affi_userid, affi_username, mst_shopid, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status != 3
AND grass_region != ''
) AS t2 on t1.shopid = t2.mst_shopid
)
, seller_index AS (
SELECT CAST(si.child_shopid as BIGINT) as shop_id
, si.gp_account_billing_country
, CASE WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
WHEN sip.affi_shopid IS NOT NULL AND sip.cb_option = 0 THEN 'Local SIP'
WHEN sip.affi_shopid IS NOT NULL AND sip.psite IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL AND si.gp_account_billing_country = 'Korea' AND sip.psite IN ('SG') THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL THEN 'CB SIP'
when si.gp_account_billing_country = 'Korea' then 'KRCB'
when si.gp_account_billing_country = 'Japan' then 'JPCB'
when si.gp_account_billing_country is NULL or si.gp_account_billing_country != 'China' then 'Others'
when si.child_account_owner_userrole_name like '%HKCB%' then 'HKCB'
when si.child_account_owner_userrole_name like '%BD_%' then 'CNCB BD'
when si.child_account_owner_userrole_name like '%UST_%' then 'CNCB UST'
when si.child_account_owner_userrole_name like '%ST_%' THEN 'CNCB ST'
when si.child_account_owner_userrole_name like '%MT_%' THEN 'CNCB MT'
when si.child_account_owner_userrole_name like '%LT%' THEN 'CNCB LT'
WHEN si.gp_account_billing_country = 'China' THEN COALESCE(CONCAT('CNCB ', si.cb_group), 'CNCB Others')
else 'CNCB Others' END as seller_type
FROM regbd_sf.cb__seller_index_tab AS si
LEFT JOIN sip ON CAST(si.child_shopid AS BIGINT) = sip.affi_shopid
LEFT JOIN (
SELECT DISTINCT shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date BETWEEN DATE_ADD('day', -14, current_date) AND DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = CAST(si.child_shopid as BIGINT)
WHERE si.grass_region IN ('ID') -- ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
)


SELECT i.date AS grass_date
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'CNCB UST' THEN i.item_id ELSE NULL END) AS item_count_cncbust
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'CNCB ST' THEN i.item_id ELSE NULL END) AS item_count_cncbst
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'CNCB MT' THEN i.item_id ELSE NULL END) AS item_count_cncbmt
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'CNCB LT' THEN i.item_id ELSE NULL END) AS item_count_cncblt
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'CNCB BD' THEN i.item_id ELSE NULL END) AS item_count_cncbbd
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'CNCB Others' THEN i.item_id ELSE NULL END) AS item_count_cncbothers
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'KRCB' THEN i.item_id ELSE NULL END) AS item_count_krcb
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'JPCB' THEN i.item_id ELSE NULL END) AS item_count_jpcb
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'HKCB' THEN i.item_id ELSE NULL END) AS item_count_hkcb
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'Local SIP' THEN i.item_id ELSE NULL END) AS item_count_localsip
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'TB SIP' THEN i.item_id ELSE NULL END) AS item_count_tbsip
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'KR SIP' THEN i.item_id ELSE NULL END) AS item_count_krsip
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'CB SIP' THEN i.item_id ELSE NULL END) AS item_count_cbsip
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'CBWH' THEN i.item_id ELSE NULL END) AS item_count_cbwh
, COUNT(DISTINCT CASE WHEN cb.seller_type = 'Others' THEN i.item_id ELSE NULL END) AS item_count_others
FROM marketplace.shopee_item_audit_log_db__item_audit_tab__reg_daily_s0_live AS i
LEFT JOIN seller_index AS cb ON cb.shop_id = i.shop_id


WHERE date BETWEEN DATE('2021-07-20') AND DATE_ADD('day', -1, current_date)
--AND date >= DATE_ADD('week', -4, DATE_ADD('day', -1, current_date))
AND try(i.data.fields[1].name) = 'status' and try(i.data.fields[1].new) in ('3', '4')
AND i.country IN ('ID')
AND lower(try(i.data.reason)) LIKE '%prohibited and restricted goods%'


GROUP BY 1
ORDER BY 1 DESC