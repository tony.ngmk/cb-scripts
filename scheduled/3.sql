WITH sip
AS (SELECT DISTINCT affi_shopid,
mst_shopid,
t1.country mst_country,
t2.country affi_country,
t1.cb_option
FROM (marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
inner join (SELECT DISTINCT affi_shopid,
affi_username,
mst_shopid,
country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE ( sip_shop_status <> 3 )
AND grass_region IN ( 'PL', 'ES', 'FR' )) t2
ON ( t1.shopid = t2.mst_shopid ))
WHERE ( t2.country IN ( 'PL', 'ES', 'FR' ) )),
track
AS (SELECT DISTINCT keyword,
Cast(min_price AS BIGINT) min_price
FROM regcbbi_others.shopee_regional_cb_team__twtopskukeywords
WHERE ingestion_timestamp = (SELECT Max(ingestion_timestamp) col
FROM
regcbbi_others.shopee_regional_cb_team__twtopskukeywords)),
un
AS (SELECT DISTINCT mst_itemid,
unlist_region
FROM
marketplace.shopee_sip_db__item_unlist_config_tab__reg_daily_s0_live)
SELECT DISTINCT map.affi_shopid,
map.mst_shopid,
map.mst_itemid,
sip.mst_country,
map.affi_country,
map.affi_itemid,
name,
status,
'mobile keywords' keywords,
unlist_region
FROM (SELECT DISTINCT item_id,
shop_id,
name,
price,
status
FROM mp_item.dim_item__reg_s0_live
WHERE grass_date = current_date - interval'1'day
AND grass_region IN ( 'FR', 'ES' )
AND level2_global_be_category = 'Mobile Phones'
AND is_cb_shop = 1
AND tz_type = 'local'
AND shop_status = 1
AND seller_status = 1
AND is_holiday_mode = 0
AND status NOT IN ( 4, 0, 5, 3, 8 )
AND Regexp_like(name,
'Carcasa para móvil|Funda para móvil|Estuche para móvil|Carcasa|Funda|Estuche|Funda protectora|Carcasa protectora|Estuche proctector|Protector|Protector de voltaje|Línea de transmisión|Cable de transmisión|Cable|Cargar|Recargar|Batería|Pila|Célula|Enchufe|Clavija|Plug|Enchufe de conversión|Adaptador de enchufe|Adaptador para enchufe|Móvil demo|Móvil de exhibición|Prototipo móvil|Modelo|Auriculares|Audífonos|Auriculares con cable|Audífonos con cable|Audífonos inalámbricos|Auriculares inalámbricos|Auriculares sin cable|Auriculares Bluetooth|Audífonos Bluetooth|Palo de selfie|Stick de selfie|Barra de selfie|Espejo para selfies|Espejo para autofotos|Membrana de pantalla|Protector de pantalla|Cristal templado|Protector|Membrana|Soporte|Base|Stand|Soporte para móvil|Base para móvil|Stand para móvil|Tarjeta de almacenamiento|Tarjeta de memoria|Tarjeta de datos|Reloj inteligente / Pulsera inteligente|Smartwatch / Pulsera inteligente'
) = FALSE) i
join (SELECT DISTINCT affi_itemid,
mst_itemid,
affi_shopid,
mst_shopid,
affi_country
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE affi_country IN ( 'FR', 'ES' )) map
ON map.affi_itemid = i.item_id
join track t
ON Regexp_like(Lower(name), t.keyword)
AND i.price <= t.min_price
join(SELECT *
FROM sip
WHERE affi_country IN ( 'FR', 'ES' )) sip
ON sip.affi_shopid = i.shop_id
left join un
ON un.mst_itemid = map.mst_itemid
UNION
SELECT DISTINCT affi_shopid,
mst_shopid,
map.mst_itemid,
mst_country,
map.affi_country,
i.itemid,
name,
status,
( CASE
WHEN ( k.keyword IS NOT NULL ) THEN k.keyword
ELSE 'N'
END ) keyword,
unlist_region
FROM ((sip
inner join (SELECT DISTINCT item_id itemid,
shop_id shopid,
Replace(Lower(name), '_', ' ') name,
status
FROM mp_item.dim_item__reg_s0_live
WHERE ( ( NOT ( status IN ( 0, 4, 5, 8 ) ) )
AND ( grass_region IN( 'PL', 'ES', 'FR' ) ) )
AND is_cb_shop = 1
AND grass_date = current_date - interval'1'day
AND seller_status = 1
AND shop_status = 1
AND is_holiday_mode = 0) i
ON ( i.shopid = sip.affi_shopid ))
inner join (SELECT DISTINCT affi_itemid,
mst_itemid,
affi_country
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE ( affi_country IN ( 'PL', 'ES', 'FR' ) )) map
ON ( map.affi_itemid = i.itemid ))
inner join (SELECT DISTINCT keyword,
country
FROM regcbbi_others.prohibited_keyword_tmp_new_market_s0) k
ON Regexp_like(name, "Concat"('\b', Cast(keyword AS VARCHAR),
'\b'))
AND k.country = map.affi_country
left join un
ON un.mst_itemid = map.mst_itemid