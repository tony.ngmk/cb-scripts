insert into nexus.test_94b73068c538c029bc0e94688e64a659e15923c53ae18bef473177fdfee0b562_cc64643f3ca150ce5bf8e84269498cad WITH
sip AS (
SELECT DISTINCT
grass_region
, affi_shopid
, mst_shopid
, b.country mst_country
FROM
(marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON (a.mst_shopid = b.shopid))
WHERE ((b.cb_option = 0) AND (a.grass_region in ('SG','MY','PH','TH','VN','MX','BR', 'CO', 'CL') ))
) 


,sip_weight_upward as (select distinct order_sn 
, sum(affi_real_weight*item_amount) sip_weight 
, sum(seller_input_order_weight) seller_input_order_weight 
-- , count(distinct affi_itemid) sku 
-- , sum(item_amount) item_amount
from dev_regcbbi_others.sip_local_sip_order_sip_weight_df_reg_s0 
where date(create_datetime)>=date'2022-04-01'
group by 1 
having sum(affi_real_weight*item_amount) >= sum(seller_input_order_weight) 
)


, sku as (select 
distinct item_id affi_itemid 
, primary_item_id mst_itemid 
, item_real_weight/100.00 item_real_weight
, status 
, seller_status
from mp_item.ads_sip_affi_item_profile__reg_s0_live
where grass_date = if(grass_region in ('SG','MY','PH','TH','ID','VN','TW'), current_date- interval'1'day ,current_date- interval'2'day )
and grass_date >= current_date- interval'2'day
and primary_shop_type=0 and shop_status=1 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL')
and primary_seller_region in ('ID','MY','TW','VN','TH') and tz_type='local' 
and shop_status=1 
-- and seller_status=1 and status = 1
)


, fees AS (
SELECT DISTINCT
orderid
, cc_fee
, commission_fee
, service_fee
FROM
regcbbi_others.shopee_bi_keyreports_local_sip_revenue_v3
WHERE ((( (lane IN ('IDPH', 'IDSG', 'IDMY','IDTH','IDVN','IDCO','IDCL','MYSG', 'TWSG', 'TWMY', 'TWID','IDBR','VNMY','IDMX','VNSG','VNPH','THMY'))))
))
, wh_single as (select distinct 
-- ordersn 
-- , wh_outbound_date
-- , shop_id 
-- , item_id 
-- , order_id 
-- , actual_weight
-- , item_amount
-- , weight_per_item 
-- , 
a.mst_itemid mst_itemid 
from dev_regcbbi_others.sip__item_weight_adj_order__df__reg__s0 o 
join sku a on a.affi_itemid = o.item_id 
where actual_weight >0 
)
, wh AS (SELECT DISTINCT * FROM regcbbi_others.logistics_basic_info
WHERE wh_outbound_date = CURRENT_DATE - INTERVAL '1' DAY)
, o AS (
SELECT DISTINCT
"date"("from_unixtime"(create_timestamp)) create_timestamp
, order_id
, order_sn
, shop_id
, gmv
, gmv_usd
, buyer_paid_shipping_fee
, pv_rebate_by_shopee_amt
, sv_rebate_by_shopee_amt
, coin_used_cash_amt
, sv_coin_earn_by_seller_amt
, card_rebate_by_bank_amt
, sv_rebate_by_seller_amt
, pv_rebate_by_seller_amt
, IF((actual_shipping_rebate_by_shopee_amt = 0), estimate_shipping_rebate_by_shopee_amt, actual_shipping_rebate_by_shopee_amt) shopee_shipping_rebate
, actual_shipping_fee
, logistics_status_id
, payment_method_id
, order_be_status_id
, sv_promotion_id
, card_rebate_by_shopee_amt
, is_cb_shop
, grass_region
, pv_coin_earn_by_seller_amt
, buyer_id
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE (((grass_region in ('SG','MY','PH','TH','VN','MX','BR','TW','ID','CO','CL') ) AND ("date"("from_unixtime"(create_timestamp)) >= CURRENT_DATE - INTERVAL '30' DAY)))
and tz_type='local' and grass_date>= current_date-interval'30'day 
) 




, oi AS (
SELECT DISTINCT
order_id
, o.order_sn
, is_cb_shop
, o.grass_region
, max(seller_status+status) overall_status 
, max(case when wh.mst_itemid is not null and item_real_weight>0 then 1 else 0 end) sku_level_update
, min(affi_real_weight) item_real_weight
, "sum"(item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
, "sum"(item_tax_amt) item_tax_amt
, "sum"(commission_fee) commission_fee
, "sum"(buyer_txn_fee) buyer_txn_fee
, "sum"(seller_txn_fee) seller_txn_fee
, "sum"(service_fee) service_fee
, SUM(item_amount) item_amount
, SUM(affi_real_weight*item_amount) AS affi_real_weight
, SUM(CASE WHEN affi_real_weight > 0 THEN item_amount END) AS item_amount_w_real_weight
, SUM(item_weight_pp*1000*item_amount) AS seller_input_weight
, sum(item_amount*COALESCE(sum_hpfn,0)) AS hpfn
, sum(item_amount*COALESCE(initial_hp,0)) AS initial_price
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
join sku on sku.affi_itemid = o.item_id 
left join (select mst_itemid 
from wh_single) wh on wh.mst_itemid = sku.mst_itemid 


LEFT JOIN sip ON o.shop_id = sip.affi_shopid
LEFT JOIN (
-- SELECT DISTINCT item_id affi_itemid
-- , primary_item_id mst_itemid
-- , shop_id affi_shopid
-- , primary_shop_id mst_shopid
-- , COALESCE(item_real_weight / 100.00, 0) AS affi_real_weight
-- FROM mp_item.ads_sip_affi_item_profile__reg_s0_live
-- WHERE primary_shop_type = 0
-- AND tz_type = 'local'
-- AND grass_date = current_date - INTERVAL '2' DAY
select distinct affi_itemid 
, mst_itemid 
, affi_shopid 
, mst_shopid 
, affi_real_weight
, order_sn 
from dev_regcbbi_others.sip_local_sip_order_sip_weight_df_reg_s0 
where date(create_datetime)>=date'2022-04-01'
) si
ON o.item_id = si.affi_itemid and si.order_sn = o.order_sn 

LEFT JOIN (SELECT DISTINCT mst_country
, affi_country
, TRY_CAST(weight AS DOUBLE) weight
, TRY_CAST(hpfn AS DOUBLE) hpfn 
, TRY_CAST(initial_hp AS DOUBLE) initial_hp
, TRY_CAST(sum_hpfn AS DOUBLE) sum_hpfn
, TRY_CAST(fx AS DOUBLE) fx 
, TRY_CAST(country_margin AS DOUBLE) country_margin
FROM regcbbi_others.local_sip_price_config a 
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp FROM regcbbi_others.local_sip_price_config) b
ON a.ingestion_timestamp = b.ingestion_timestamp) cfg 
ON sip.grass_region = cfg.affi_country 
AND sip.mst_country = cfg.mst_country 
AND CASE WHEN (affi_real_weight IS NULL OR affi_real_weight = 0) THEN IF(item_weight_pp*1000 BETWEEN 1 AND 10,1,FLOOR(IF(item_weight_pp*1000 >= 20000,20000,item_weight_pp*1000)/10.0)*10) = cfg.weight
ELSE IF(affi_real_weight BETWEEN 1 AND 10,1,FLOOR(IF(affi_real_weight >= 20000,20000,affi_real_weight)/10.0)*10) = cfg.weight END
WHERE ((o.grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL') ) AND ("date"("from_unixtime"(create_timestamp)) >= CURRENT_DATE - INTERVAL '30' DAY))
and tz_type='local' 
and grass_date >= current_date-interval'30'day 
GROUP BY 1, 2, 3, 4
) 
, rr AS (
SELECT DISTINCT
orderid
, (refund_amount / DECIMAL '100000.00') refund_amount
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE (((grass_region in ('SG','MY','PH','TH','VN','MX','BR','CO','CL') ) AND ("date"("from_unixtime"(ctime)) >= CURRENT_DATE - INTERVAL '30' DAY)) AND (refund_amount > 0)) AND status IN (2,5)
) 
, ssv AS (SELECT distinct perc_ssv/100.00 AS perc_ssv,promo_id FROM regcbbi_others.sip_local_pnl_ssv)


, rbt AS (
SELECT DISTINCT
overseas_itemid
, overseas_modelid
, seller_negotiated_price_local_currency
, start_date_time
, end_date_time
FROM
regcbbi_others.sip_local_pnl_item_rebate
WHERE ((overseas_itemid > 0) AND (seller_negotiated_price_local_currency > 0))
) 
, rbt2 AS (
SELECT DISTINCT
orderid
, "sum"(rebates) rebate
FROM
(
SELECT DISTINCT
orderid
, itemid
, modelid
, ((seller_negotiated_price_local_currency * amount) - price) rebates
FROM
((
SELECT
order_id orderid
, item_id itemid
, model_id modelid
, item_amount amount
, item_price_pp price
, ("from_unixtime"(create_timestamp)) create_time
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (((grass_region IN ('SG', 'MY')) AND (is_cb_shop = 1)) AND ("date"("from_unixtime"(create_timestamp)) >= CURRENT_DATE - INTERVAL '30' DAY)) and tz_type='local' and grass_date >= current_date-interval'30'day 
) p_oi
INNER JOIN rbt ON (((rbt.overseas_modelid = p_oi.modelid) AND (rbt.overseas_itemid = p_oi.itemid)) AND (p_oi.create_time BETWEEN start_date_time AND end_date_time)))
) 
GROUP BY 1
)


, r AS (SELECT DISTINCT
mst_country
, sip.grass_region
, wh_outbound_date
, a.create_timestamp
, sip.affi_shopid
, sip.mst_shopid
, a.buyer_id
, a.order_id
, fx_usd.fx_usd AS fx_affi_usd
, fx_usd_mst.fx_usd AS fx_mst_usd
, case when ai.item_real_weight >0 then 'Y' else 'N' end as has_sip_weight 
, case when ai.overall_status=2 then 'Y' else 'N' end as live_sku
, case when ai.item_real_weight > 0 and ai.sku_level_update =1 then 'Y' else 'N' end as sku_level_update 
, case when swu.order_sn is not null then 'Y' else 'N' end as upward_update 
, "sum"(a.gmv) gmv
, sum(a.gmv_usd) gmv_usd 
, "sum"(a.buyer_paid_shipping_fee) buyer_paid_shipping_fee
, "sum"(ai.item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
, "sum"(a.pv_rebate_by_shopee_amt) pv_rebate_by_shopee_amt
, "sum"(a.coin_used_cash_amt) coin_used_cash_amt
, "sum"(a.card_rebate_by_bank_amt) card_rebate_by_bank_amt
, "sum"(a.card_rebate_by_shopee_amt) card_rebate_by_shopee_amt
, "sum"(a.sv_rebate_by_seller_amt) sv_rebate_by_seller_amt
, "sum"(a.sv_coin_earn_by_seller_amt) sv_coin_earn_by_seller_amt
, "sum"(ai.item_tax_amt) item_tax_amt
, "sum"(a.shopee_shipping_rebate) shopee_shipping_rebate
, "sum"(wh.actual_shipping_fee) actual_shipping_fee
, "sum"(fees.commission_fee) commission_fee
, "sum"(fees.cc_fee) seller_txn_fee
, "sum"(fees.service_fee) service_fee
, "sum"(rr.refund_amount) refund_amount
, "sum"((CASE WHEN ((a.logistics_status_id = 6) AND (a.payment_method_id = 6)) THEN a.gmv + ai.item_rebate_by_shopee_amt + a.pv_rebate_by_shopee_amt + a.coin_used_cash_amt + a.card_rebate_by_bank_amt + a.card_rebate_by_bank_amt - a.sv_coin_earn_by_seller_amt - a.pv_coin_earn_by_seller_amt - ai.item_tax_amt + a.shopee_shipping_rebate + fees.commission_fee + fees.cc_fee + fees.service_fee ELSE 0 END)) cod_fail
, "sum"((CASE WHEN (ssv.promo_id IS NOT NULL) THEN ((perc_ssv ) * a.sv_rebate_by_seller_amt) ELSE 0 END)) ssv_rebate
, "sum"(p.gmv) gmv_p
, "sum"(p.buyer_paid_shipping_fee) buyer_paid_shipping_fee_p
, "sum"(pi.commission_fee) commission_fee_p
, "sum"(pi.buyer_txn_fee) buyer_txn_fee_p
, "sum"(pi.seller_txn_fee) seller_txn_fee_p
, "sum"(pi.service_fee) service_fee_p
, "sum"(pi.item_rebate_by_shopee_amt) item_rebate_by_shopee_amt_p
, "sum"(p.pv_rebate_by_shopee_amt) pv_rebate_by_shopee_amt_p
, "sum"(p.shopee_shipping_rebate) shopee_shipping_rebate_p
, "sum"(p.sv_coin_earn_by_seller_amt) sv_coin_earn_by_seller_p
, "sum"(p.coin_used_cash_amt) coin_used_cash_amt_p
, "sum"(COALESCE(ai.hpfn, 0)) hpfn
, "sum"(rebate) rebate
, SUM(ai.initial_price) AS initial_price
, SUM(a.pv_coin_earn_by_seller_amt) AS pv_coin_earn_by_seller_amt
, SUM(a.pv_rebate_by_seller_amt) AS pv_rebate_by_seller_amt
, SUM(ai.item_amount) item_amount
, SUM(wh.actual_weight) wh_actual_weight
, SUM(ai.seller_input_weight) seller_input_weight
, SUM(ai.affi_real_weight) affi_real_weight
, SUM(ai.item_amount_w_real_weight) item_amount_w_real_weight 
FROM
((((((((((((
SELECT *
FROM
o
WHERE ((is_cb_shop = 1) AND (grass_region IN ('SG', 'MY', 'ID', 'PH','TH','VN','BR','MX','CO','CL')))
) a
JOIN (SELECT DISTINCT affi_country,TRY_CAST(fx AS DOUBLE) fx_usd
FROM regcbbi_others.shopee_regional_cb_team__cnsip_price_config a
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp
FROM regcbbi_others.shopee_regional_cb_team__cnsip_price_config) b ON a.ingestion_timestamp = b.ingestion_timestamp
WHERE mst_country = 'USD') fx_usd ON a.grass_region = fx_usd.affi_country
INNER JOIN (
SELECT *
FROM
oi
WHERE ((is_cb_shop = 1) AND (grass_region IN ('SG', 'MY', 'ID', 'PH','TH','VN','BR','MX','CO','CL')))
) ai ON (a.order_id = ai.order_id))
INNER JOIN sip ON (a.shop_id = sip.affi_shopid)
JOIN (SELECT DISTINCT affi_country,TRY_CAST(fx AS DOUBLE) fx_usd
FROM regcbbi_others.shopee_regional_cb_team__cnsip_price_config a
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp
FROM regcbbi_others.shopee_regional_cb_team__cnsip_price_config) b ON a.ingestion_timestamp = b.ingestion_timestamp
WHERE mst_country = 'USD') fx_usd_mst ON sip.mst_country = fx_usd_mst.affi_country)
INNER JOIN (SELECT *
FROM marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE from_unixtime(ctime) >= current_date - INTERVAL '180' DAY
) m ON (a.order_id = m.oversea_orderid))
INNER JOIN (
SELECT *
FROM
o
WHERE (((NOT (order_be_status_id IN (1, 6, 8, 5, 7, 8, 16))) AND (is_cb_shop = 0)) AND (grass_region IN ('ID', 'MY', 'TW','VN','TH')))
) p ON (m.local_orderid = p.order_id))
INNER JOIN wh ON (a.order_sn = wh.ordersn)))
left join sip_weight_upward swu on swu.order_sn = a.order_sn 
LEFT JOIN (
SELECT *
FROM
oi
WHERE ((is_cb_shop = 0) AND (grass_region IN ('ID', 'MY', 'TW','VN','TH')))
) pi ON (p.order_id = pi.order_id))
LEFT JOIN fees ON (a.order_id = fees.orderid))
LEFT JOIN rr ON (a.order_id = rr.orderid))
LEFT JOIN ssv ON (a.sv_promotion_id = ssv.promo_id))
LEFT JOIN rbt2 ON (a.order_id = rbt2.orderid))
WHERE wh_outbound_date = CURRENT_DATE - INTERVAL '1' DAY
GROUP BY 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14)


SELECT DISTINCT r.mst_country 
, grass_region affi_country
, wh_outbound_date 
, has_sip_weight
, case when has_sip_weight='Y' and sku_level_update='Y' then 'Y' else 'N' end as sku_level_update
, case when has_sip_weight='Y' and sku_level_update='N' then 'Y' else 'N' end as cat_level_update
, live_sku 
, case when sku_level_update = 'Y' and has_sip_weight='Y' and upward_update ='Y' then 'Y' else 'N' end as upward_update
, case when sku_level_update = 'Y' and has_sip_weight='Y' and upward_update ='N' then 'Y' else 'N' end as downward_update 
-- , actual_buyer_paid_shipping_fee + actual_shipping_rebate_by_shopee_amt + affi_hpfn - actual_shipping_fee AS shipping_loss_local_currency
, sum((buyer_paid_shipping_fee + shopee_shipping_rebate + hpfn)/fx_affi_usd - buyer_paid_shipping_fee_p/fx_mst_usd - actual_shipping_fee/fx_affi_usd) AS shipping_loss_usd
, count(distinct order_id) orders 
, count(distinct case when (buyer_paid_shipping_fee + shopee_shipping_rebate + hpfn)/fx_affi_usd - buyer_paid_shipping_fee_p/fx_mst_usd - actual_shipping_fee/fx_affi_usd <0 then order_id 
else null end
) loss_order 
, sum(gmv_usd) gmv_usd
, avg(wh_actual_weight) avg_actual_weight
, sum(wh_actual_weight) sum_actual_weight
, avg(actual_shipping_fee*decimal'1.000'/fx_affi_usd) avg_shipping_fee_usd
, sum(actual_shipping_fee*decimal'1.000'/fx_affi_usd) sum_shipping_fee_usd 


FROM r 
group by 1,2 , 3 ,4 , 5 ,6 , 7 , 8 , 9