insert into nexus.test_bea92849c6d0ec3d63b112461dd2f170a4372d9f1dc21d1ba8704d5ad6fb87c8_c10f2219c339925d9eb7a9e2e1fa7fbf WITH user as 
(
SELECT grass_region 
, u.shop_id 
, user_status 
, shop_registration_date 
, shop_status
, cdate
FROM
(
SELECT grass_region 
, shop_id 
, status as user_status
, date(from_unixtime(registration_timestamp)) as shop_registration_date 
FROM mp_user.dim_user__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
and is_cb_shop = 1
) u
LEFT JOIN 
(
SELECT shop_id 
, status as shop_status 
FROM mp_user.dim_shop__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
) s on u.shop_id = s.shop_id
LEFT JOIN 
(SELECT shop_id
, date(from_unixtime(ctime)) as cdate -- to exclude shops created after dt in the raw table
FROM marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live
) c on u.shop_id = c.shop_id
)


, tag_type as 
(
SELECT cast(a.tag_id as int) as tag_id 
, type as wh_type
, tag_name 
, market as grass_region 
, cast(commission_rate as double) as tag_commission_rate 
, update_date as tag_update_date
, max(date(from_unixtime(metrics_cycle_time/1000))) - interval '1' day as tag_max_metrics_time
FROM regcbbi_others.mkt__commission_tag_id_mapping__reg_s0 a
LEFT JOIN marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live b on cast(a.tag_id as int) = b.tag_id
WHERE type = 'nonWH'
AND b.grass_region != ''
GROUP BY 1,2,3,4,5,6
)




, raw_db as 
(
select tag_id
, tag_name
, tag_max_metrics_time
, tag_commission_rate
, db.* 
FROM tag_type t 
JOIN
(
SELECT 'CN_all' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.managed_team_region in ('CN','blank') --- or a.managed_team_region is null: should add managed-team_region is null
and a.is_cb_shop = 1
and b.merchant_region = 'CN'



UNION 


SELECT 'CN_incubation' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.managed_team_region in ('CN','blank') --- or a.managed_team_region is null: should add managed-team_region is null
and a.is_cb_shop = 1
and b.merchant_region = 'CN'
AND a.country in ('CO', 'CL', 'ES', 'PL')
and merchant_level_first_open_days < 121

UNION 

SELECT 'CN_all_HK' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.managed_team_region in ('CN') --- or a.managed_team_region is null: should add managed-team_region is null
and a.is_cb_shop = 1
and b.merchant_region = 'HK'



UNION 


SELECT 'CN_incubation_HK' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.managed_team_region in ('CN') --- or a.managed_team_region is null: should add managed-team_region is null
and a.is_cb_shop = 1
and b.merchant_region = 'HK'
AND a.country in ('CO', 'CL', 'ES', 'PL')
and merchant_level_first_open_days < 121


UNION 



SELECT 'KR_t3' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.is_cb_shop = 1
and b.merchant_region = 'KR'
and (last_month_gmv is null or last_month_gmv< 100000)


UNION 


SELECT 'KR_t2' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.is_cb_shop = 1
and b.merchant_region = 'KR'
and last_month_gmv > 99999.99 
and last_month_gmv < 300000


UNION 


SELECT 'KR_t1' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.is_cb_shop = 1
and b.merchant_region = 'KR'
and last_month_gmv > 300000


UNION 


SELECT 'KR_incubation' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.is_cb_shop = 1
and b.merchant_region = 'KR'
and region_level_first_open_months < 3


UNION 


SELECT 'JP_t3' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.is_cb_shop = 1
and b.merchant_region = 'JP'
and (last_month_gmv is null or last_month_gmv< 150000)


UNION 


SELECT 'JP_t2' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.is_cb_shop = 1
and b.merchant_region = 'JP'
and last_month_gmv > 149999.99 
and last_month_gmv < 200000


UNION 


SELECT 'JP_t1' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.is_cb_shop = 1
and b.merchant_region = 'JP'
and last_month_gmv > 200000


UNION 


SELECT 'JP_incubation' as rule_type 
, a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
and a.is_cb_shop = 1
and b.merchant_region = 'JP'
and region_level_first_open_months < 3
) db 
ON t.grass_region = db.country and t.tag_name = db.rule_type and t.tag_max_metrics_time = db.dt
)


, tag_shop_list as 
(
SELECT a.tag_id 
, a.wh_type
, a.tag_name 
, a.grass_region 
, a.tag_commission_rate 
, a.tag_update_date
, a.tag_max_metrics_time
, b.shop_id 
, date(from_unixtime(b.mtime/1000)) - interval '1' day as tag_shop_mtime
, date(from_unixtime(b.metrics_cycle_time/1000)) - interval '1' day as tag_shop_metrics_time
FROM tag_type a 
LEFT JOIN marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live b on a.tag_id = b.tag_id
WHERE b.status= 1
AND b.grass_region != ''
)


SELECT a.* 
, b.merchant_region as db_merchant_region
, b.managed_team_region as db_managed_team_region 
, b.is_cb_shop as db_is_cb_shop 
, b.merchant_level_first_open_days as db_merchant_level_first_open_days
, b.last_month_gmv as db_last_month_gmv
, b.region_level_first_open_months as db_region_level_first_open_months
, db_data_date 
FROM 
(
select 
tag.tag_name
, tag.grass_region
, tag.tag_id 
, tag.tag_max_metrics_time
, tag.tag_commission_rate
, tag.shop_id as tag_shop_id 
, db.shop_id 
FROM tag_shop_list tag 
LEFT JOIN raw_db db ON db.tag_id = tag.tag_id and db.shop_id = tag.shop_id 
LEFT JOIN user as user_tag on tag.shop_id = user_tag.shop_id 
where db.shop_id is null
and (user_tag.user_status is not null or user_tag.shop_status is not null)
and user_tag.cdate <= db.dt --to exclude shops created after UTC+8 dt
)a 
left join 
(SELECT a.country 
, a.shop_id 
, b.merchant_region
, a.managed_team_region 
, a.is_cb_shop
, merchant_level_first_open_days
, last_month_gmv
, region_level_first_open_months
, date(dt) as db_data_date 
, country
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
left join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date - interval '2' day)-interval '1' month
) b on a.tag_shop_id = b.shop_id and a.tag_max_metrics_time = b.db_data_date
order by a.tag_name, a.grass_region, a.tag_id, a.tag_shop_id