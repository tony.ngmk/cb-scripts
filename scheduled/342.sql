insert into nexus.test_94a71dd3e0ee908b6a13b5385330d7d94f1fe061732c32660077a9bf88f5c164_554ed44dd3a8c9e928f758cfabb05b15 WITH cb as 
(
SELECT
'CB' as seller_origin 
, a.grass_region


, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_ADG 


, cast(count(distinct case when grass_date between date_trunc('week', date_add('day', -1, current_date)) and date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_week(date_add('day', -1, current_date)) as WTD_CB_ADO
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)) ) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then order_id else null end) as double)
/ 7 as W1_CB_ADO 
, sum(case when grass_date between date_trunc('week', date_add('day', -1, current_date)) and date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_week(date_add('day', -1, current_date)) as WTD_CB_ADG
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)) ) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then gmv_usd else null end)
/ 7 as W1_CB_ADG 
FROM 
(
SELECT 
grass_region
, if(grass_region IN ('BR', 'MX', 'CO','CL','PL','ES', 'FR'), date(from_unixtime(create_timestamp)), grass_date) grass_date
, order_id 
, gmv_usd
, is_cb_shop
, shop_id
FROM mp_order.dwd_order_place_pay_complete_di__reg_s0_live
WHERE if(grass_region IN ('BR', 'MX', 'CO','CL','PL','ES', 'FR'), date(from_unixtime(create_timestamp)), grass_date) between date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and date_add('day', -1, current_date)
AND tz_type = 'local'
AND grass_region IN ('SG','ID', 'TH', 'TW','MY','PH','VN','BR','MX','CO','CL','PL','ES', 'FR')
AND is_cb_shop = 1
AND is_placed = 1
AND (is_bi_excluded = 0 OR is_bi_excluded IS NULL)
) a 
LEFT JOIN (SELECT * FROM regbd_sf.cb__seller_index_tab WHERE grass_region != '') s on a.shop_id = cast(child_shopid as bigint)
GROUP BY 1, 2 
)
, country as 
(
SELECT
'Country' as seller_origin 
, a.grass_region
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_country_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_country_ADO
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_country_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_country_ADG

, cast(count(distinct case when grass_date between date_trunc('week', date_add('day', -1, current_date)) and date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_week(date_add('day', -1, current_date)) as WTD_country_ADO
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)) ) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then order_id else null end) as double)
/ 7 as W1_country_ADO
, sum(case when grass_date between date_trunc('week', date_add('day', -1, current_date)) and date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_week(date_add('day', -1, current_date)) as WTD_country_ADG
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)) ) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then gmv_usd else null end)
/ 7 as W1_country_ADG
FROM 
(
SELECT 
grass_region
, if(grass_region IN ('BR', 'MX', 'CO','CL','PL','ES', 'FR'), date(from_unixtime(create_timestamp)), grass_date) grass_date
, order_id 
, gmv_usd
, is_cb_shop
, shop_id
FROM mp_order.dwd_order_place_pay_complete_di__reg_s0_live
WHERE if(grass_region IN ('BR', 'MX', 'CO','CL','PL'), date(from_unixtime(create_timestamp)), grass_date) between date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and date_add('day', -1, current_date)
AND tz_type = 'local'
AND grass_region IN ('SG','ID', 'TH', 'TW','MY','PH','VN','BR','MX','CO','CL','PL','ES', 'FR')
AND is_placed = 1
AND (is_bi_excluded = 0 OR is_bi_excluded IS NULL)
) a 
--LEFT JOIN cb__seller_index_tab s on a.shop_id = cast(child_shopid as bigint)
GROUP BY 1, 2 
)


, origin as 
(
SELECT
(case when gp_account_billing_country = 'China' then 'CNCB'
when gp_account_billing_country = 'Korea' then 'KRCB'
when gp_account_billing_country = 'Japan' then 'JPCB'
else null end) as seller_origin
, a.grass_region
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_ADG 

, cast(count(distinct case when grass_date between date_trunc('week', date_add('day', -1, current_date)) and date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_week(date_add('day', -1, current_date)) as WTD_CB_ADO
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)) ) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then order_id else null end) as double)
/ 7 as W1_CB_ADO 
, sum(case when grass_date between date_trunc('week', date_add('day', -1, current_date)) and date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_week(date_add('day', -1, current_date)) as WTD_CB_ADG
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)) ) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then gmv_usd else null end)
/ 7 as W1_CB_ADG 
FROM 
(
SELECT 
grass_region
, if(grass_region IN ('BR', 'MX', 'CO','CL','PL','ES', 'FR'), date(from_unixtime(create_timestamp)), grass_date) grass_date
, order_id 
, gmv_usd
, is_cb_shop
, shop_id
FROM mp_order.dwd_order_place_pay_complete_di__reg_s0_live
WHERE if(grass_region IN ('BR', 'MX', 'CO','CL','PL','ES', 'FR'), date(from_unixtime(create_timestamp)), grass_date) between date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and date_add('day', -1, current_date)
AND tz_type = 'local'
AND grass_region IN ('SG','ID', 'TH', 'TW','MY','PH','VN','BR','MX','CO','CL','PL','ES', 'FR')
AND is_cb_shop = 1
AND is_placed = 1
AND (is_bi_excluded = 0 OR is_bi_excluded IS NULL)
) a 
LEFT JOIN (SELECT * FROM regbd_sf.cb__seller_index_tab WHERE grass_region != '') s on a.shop_id = cast(child_shopid as bigint)
LEFT JOIN 
(
SELECT * 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live 
WHERE grass_region != ''
) sip on a.shop_id = cast(sip.affi_shopid as bigint)
WHERE s.gp_account_billing_country IN ('China', 'Korea', 'Japan')
GROUP BY 1, 2
)




, sip_type as 
(
SELECT
(case when gp_account_billing_country = 'China' then 'TB SIP'
when gp_account_billing_country IN ('Taiwan', 'Malaysia', 'Indonesia', 'Vietnam') then 'Local SIP'
else null end) as seller_origin
, a.grass_region
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_ADG 

, cast(count(distinct case when grass_date between date_trunc('week', date_add('day', -1, current_date)) and date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_week(date_add('day', -1, current_date)) as WTD_CB_ADO
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)) ) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then order_id else null end) as double)
/ 7 as W1_CB_ADO 
, sum(case when grass_date between date_trunc('week', date_add('day', -1, current_date)) and date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_week(date_add('day', -1, current_date)) as WTD_CB_ADG
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)) ) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then gmv_usd else null end)
/ 7 as W1_CB_ADG 
FROM 
(
SELECT 
grass_region
, if(grass_region IN ('BR', 'MX', 'CO','CL','PL','ES', 'FR'), date(from_unixtime(create_timestamp)), grass_date) grass_date
, order_id 
, gmv_usd
, is_cb_shop
, shop_id
FROM mp_order.dwd_order_place_pay_complete_di__reg_s0_live
WHERE if(grass_region IN ('BR', 'MX', 'CO','CL','PL','ES', 'FR'), date(from_unixtime(create_timestamp)), grass_date) between date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and date_add('day', -1, current_date)
AND tz_type = 'local'
AND grass_region IN ('SG','ID', 'TH', 'TW','MY','PH','VN','BR','MX','CO','CL','PL','ES', 'FR')
AND is_cb_shop = 1
AND is_placed = 1
AND (is_bi_excluded = 0 OR is_bi_excluded IS NULL)
) a 
LEFT JOIN (SELECT * FROM regbd_sf.cb__seller_index_tab WHERE grass_region != '') s on a.shop_id = cast(child_shopid as bigint)
LEFT JOIN 
(
SELECT * 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live 
WHERE grass_region != ''
) sip on a.shop_id = cast(sip.affi_shopid as bigint)
WHERE s.gp_account_billing_country IN ('China', 'Taiwan', 'Malaysia', 'Indonesia', 'Vietnam')
and sip.affi_shopid is not null
GROUP BY 1, 2
)




, mkt_each as 
(
SELECT
a.seller_origin
, a.grass_region
, MTD_CB_ADO
, MTD_CB_ADO / M1_CB_ADO - 1 AS MOM_CB_ADO 
, MTD_CB_ADO / MTD_country_ADO AS MTD_CB_ADO_pct
, MTD_CB_ADO / MTD_country_ADO - M1_CB_ADO / M1_country_ADO AS MOM_CB_ADO_pct


, MTD_CB_ADG
, MTD_CB_ADG / M1_CB_ADG - 1 AS MOM_CB_ADG 
, MTD_CB_ADG / MTD_country_ADG AS MTD_CB_ADG_pct
, MTD_CB_ADG / MTD_country_ADG - M1_CB_ADG / M1_country_ADG AS MOM_CB_ADG_pct


, MTD_CB_ADG / MTD_CB_ADO AS MTD_CB_ABS
, (MTD_CB_ADG / MTD_CB_ADO) / (M1_CB_ADG / M1_CB_ADO) - 1 AS MOM_CB_ABS


, WTD_CB_ADO
, WTD_CB_ADO / W1_CB_ADO - 1 AS WOW_CB_ADO
, WTD_CB_ADO / WTD_country_ADO AS WTD_CB_ADO_pct
, WTD_CB_ADO / WTD_country_ADO - W1_CB_ADO / W1_country_ADO AS WOW_CB_ADO_pct 


, WTD_CB_ADG
, WTD_CB_ADG / W1_CB_ADG - 1 AS WOW_CB_ADG 
, WTD_CB_ADG / WTD_country_ADG AS WTD_CB_ADG_pct
, WTD_CB_ADG / WTD_country_ADG - W1_CB_ADG / W1_country_ADG AS WOW_CB_ADG_pct


, WTD_CB_ADG / WTD_CB_ADO AS WTD_CB_ABS
, (WTD_CB_ADG / WTD_CB_ADO) / (W1_CB_ADG / W1_CB_ADO) - 1 AS WOW_CB_ABS
FROM 
(
SELECT * FROM cb
UNION ALL 
SELECT * FROM origin
UNION ALL 
SELECT * FROM sip_type 
) a
LEFT JOIN country c on a.grass_region = c.grass_region 
)


, mkt_all as 
(
SELECT
a.seller_origin
, a.grass_region
, MTD_CB_ADO
, MTD_CB_ADO / M1_CB_ADO - 1 AS MOM_CB_ADO 
, MTD_CB_ADO / MTD_country_ADO AS MTD_CB_ADO_pct
, MTD_CB_ADO / MTD_country_ADO - M1_CB_ADO / M1_country_ADO AS MOM_CB_ADO_pct


, MTD_CB_ADG
, MTD_CB_ADG / M1_CB_ADG - 1 AS MOM_CB_ADG 
, MTD_CB_ADG / MTD_country_ADG AS MTD_CB_ADG_pct
, MTD_CB_ADG / MTD_country_ADG - M1_CB_ADG / M1_country_ADG AS MOM_CB_ADG_pct


, MTD_CB_ADG / MTD_CB_ADO AS MTD_CB_ABS
, (MTD_CB_ADG / MTD_CB_ADO) / (M1_CB_ADG / M1_CB_ADO) - 1 AS MOM_CB_ABS


, WTD_CB_ADO
, WTD_CB_ADO / W1_CB_ADO - 1 AS WOW_CB_ADO 
, WTD_CB_ADO / WTD_country_ADO AS WTD_CB_ADO_pct
, WTD_CB_ADO / WTD_country_ADO - W1_CB_ADO / W1_country_ADO AS WOW_CB_ADO_pct


, WTD_CB_ADG
, WTD_CB_ADG / W1_CB_ADG - 1 AS WOW_CB_ADG 
, WTD_CB_ADG / WTD_country_ADG AS WTD_CB_ADG_pct
, WTD_CB_ADG / WTD_country_ADG - W1_CB_ADG / W1_country_ADG AS WOW_CB_ADG_pct


, WTD_CB_ADG / WTD_CB_ADO AS WTD_CB_ABS
, (WTD_CB_ADG / WTD_CB_ADO) / (W1_CB_ADG / W1_CB_ADO) - 1 AS WOW_CB_ABS


FROM 
(
(
SELECT 
seller_origin
, 'Total' as grass_region
, sum(MTD_CB_ADO) as MTD_CB_ADO
, sum(M1_CB_ADO) as M1_CB_ADO
, sum(MTD_CB_ADG) as MTD_CB_ADG
, sum(M1_CB_ADG) as M1_CB_ADG 
, sum(WTD_CB_ADO) as WTD_CB_ADO
, sum(W1_CB_ADO) as W1_CB_ADO
, sum(WTD_CB_ADG) as WTD_CB_ADG
, sum(W1_CB_ADG) as W1_CB_ADG 
FROM cb
GROUP BY 1, 2
)
UNION ALL 
(
SELECT 
seller_origin
, 'Total' as grass_region
, sum(MTD_CB_ADO) as MTD_CB_ADO
, sum(M1_CB_ADO) as M1_CB_ADO
, sum(MTD_CB_ADG) as MTD_CB_ADG
, sum(M1_CB_ADG) as M1_CB_ADG 
, sum(WTD_CB_ADO) as WTD_CB_ADO
, sum(W1_CB_ADO) as W1_CB_ADO
, sum(WTD_CB_ADG) as WTD_CB_ADG
, sum(W1_CB_ADG) as W1_CB_ADG 
FROM origin
GROUP BY 1, 2
)
UNION ALL 
(
SELECT 
seller_origin
, 'Total' as grass_region
, sum(MTD_CB_ADO) as MTD_CB_ADO
, sum(M1_CB_ADO) as M1_CB_ADO
, sum(MTD_CB_ADG) as MTD_CB_ADG
, sum(M1_CB_ADG) as M1_CB_ADG 
, sum(WTD_CB_ADO) as WTD_CB_ADO
, sum(W1_CB_ADO) as W1_CB_ADO
, sum(WTD_CB_ADG) as WTD_CB_ADG
, sum(W1_CB_ADG) as W1_CB_ADG 
FROM sip_type
GROUP BY 1, 2
) 
) a
LEFT JOIN 
(
SELECT 
seller_origin
, 'Total' as grass_region
, sum(MTD_country_ADO) as MTD_country_ADO
, sum(M1_country_ADO) as M1_country_ADO
, sum(MTD_country_ADG) as MTD_country_ADG
, sum(M1_country_ADG) as M1_country_ADG 
, sum(WTD_country_ADO) as WTD_country_ADO
, sum(W1_country_ADO) as W1_country_ADO
, sum(WTD_country_ADG) as WTD_country_ADG
, sum(W1_country_ADG) as W1_country_ADG 
FROM country
GROUP BY 1, 2
) c on a.grass_region = c.grass_region
) 


SELECT * FROM mkt_each
UNION ALL 
SELECT * FROM mkt_all