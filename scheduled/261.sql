insert into nexus.test_c2e7673c4656661b6b05b3b84e99d75c7fed9d182a0a7b29bd8cb29a7a9210c5_735d5377cc0658c3c57029f95518c2fe -- 2022/03/09 updates:
-- reviewed & changed RR threshold:
-- TH: 300 >>> 150
--2022/03/17 updates:
-- added shop margin section using latest shop margin
--2022/03/24 update:
-- fixed multi offline stmt sku level sku currency issue
--2022/03/31 update:
-- updated rebate calculation method (add in comm, service, txn, country margin & shop margin markups)
-- updated country margin calculation logic: base changed from NMV --> (system settlement + hpfn)*(1 + comm% + serv% + txn%)
-- 2022/04/12 update:
-- order tax valid for TW, MX, CL (https://confluence.shopee.io/display/SPCT/%5BMaster+RD%5D+Order+tax+feature+list)
-- 2022/04/26 updates:
-- include buyer_txn_fee as SIP cost
-- 2022/04/28 updates:
-- break down total service revenue from seller into: spp, txn and management
--2022/05/05 updates:
-- add item margin section, item margin is calculated using latest item margin, historical values are not accurate


with price_config AS (SELECT grass_region
, currency
, CAST(sip_exchange AS DOUBLE) sip_exchange --> USD/grass_region currency
FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2
WHERE cb_option = '1'
AND ingestion_timestamp = (SELECT MAX(ingestion_timestamp) max FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2)
)


, margin AS (SELECT DISTINCT grass_region
, mst_country
, CAST(sip_margin AS DOUBLE)/100.00 AS sip_margin
, CAST(valid_from AS DATE) valid_from
, CAST(valid_to AS DATE) valid_to
FROM regcbbi_others.sip__country_margin__tmp__reg__s2
WHERE ingestion_timestamp = (SELECT MAX(ingestion_timestamp) max_ingestion FROM regcbbi_others.sip__country_margin__tmp__reg__s2)
AND cb_option = '1'
)
-- all settlement related amount converted to USD using SIP system exchange rates
, o AS (
SELECT DISTINCT 
o.wh_outbound_date
, o.grass_date AS create_date
, o.mst_country
, o.grass_region
, o.order_id
, o.order_sn
, o.payment_method_id
, o.logistics_status_id
-- , o.order_be_status
, asf_sls
, o.actual_buyer_paid_shipping_fee
, o.actual_shipping_rebate_by_shopee_amt
, o.pv_rebate_by_shopee_amt
, o.sv_rebate_by_shopee_amt
, o.card_rebate_by_bank_amt
, o.card_rebate_by_shopee_amt
, o.coin_used_cash_amt
, o.pv_coin_earn_by_shopee_amt
, o.sv_coin_earn_by_shopee_amt
, o.actual_shipping_rebate_by_seller_amt
, o.seller_snapshot_discount_shipping_fee
, o.escrow_to_seller_amt
, o.gmv
, o.gmv_usd
, o.pv_coin_earn_by_seller_amt
, o.sv_coin_earn_by_seller_amt
, o.pv_rebate_by_seller_amt
, o.sv_rebate_by_seller_amt
, o.seller_txn_fee --platform charge SIP
, o.seller_txn_fee_shipping_fee --platform charge SIP
, o.buyer_txn_fee 
, o.service_fee --platform charge SIP
, refund_amount
, o.tax
, o.tax_exemption_amt
, actual_weight
, reason
, o.shop_id
, mst_shopid
, sip_rate
, shop_margin
, sip_margin


-- stmt related info
, order_adj_type
, order_adj_amount
, o.sip_settlement --order level stmt 
, sip_seller_comm_fee -- SIP charge seller
, sip_seller_service_fee -- SIP charge seller
, pc1.sip_exchange AS stmt_exchange
, pc1.currency AS stmt_currency
, pc2.sip_exchange AS mst_exchange
, pc2.currency AS mst_currency
, pc3.sip_exchange AS affi_exchange
, pc3.currency AS affi_currency
, o.offline_exchange AS offline_ord_exchange
, o.offline_currency AS offline_ord_currency
, o.sip_seller_comm_rate
, o.sip_seller_service_rate
, o.sip_seller_txn_rate
, o.sip_seller_management_rate
, o.sip_seller_spp_rate
, TRY(COALESCE(seller_txn_fee/gmv *1.00, 0)) AS txn_rate


FROM regcbbi_others.sip_cn_pnl_order_tab_v2 o 
LEFT JOIN (SELECT *
FROM price_config) pc1 
ON o.stmt_currency = pc1.currency -- USD / stmt curr
LEFT JOIN (SELECT *
FROM price_config) pc2 
ON o.mst_country = pc2.grass_region -- USD / mst curr
LEFT JOIN (SELECT *
FROM price_config) pc3 
ON o.grass_region = pc3.grass_region -- USD / affi curr
LEFT JOIN margin
ON o.grass_region = margin.grass_region
AND o.mst_country = margin.mst_country
AND o.grass_date BETWEEN margin.valid_from AND margin.valid_to 
WHERE (o.wh_inbound_date + interval '1' day < o.cancel_timestamp OR o.cancel_user_id IS NULL)
)


, oi AS (
SELECT DISTINCT 
order_id
, order_sn
, SUM(commission_base_amt) commission_base_amt
, SUM(item_rebate_by_seller_amt) AS item_rebate_by_seller_amt
, SUM(item_rebate_by_shopee_amt) AS item_rebate_by_shopee_amt
, SUM(item_amount) AS item_amount
, SUM(commission_fee) AS commission_fee --platform charge SIP
, SUM(service_fee_item) AS service_fee_item --platform charge SIP
, SUM(item_tax_amt) AS item_tax_amt
, SUM(item_tax_exemption_amt) AS item_tax_exemption_amt
, SUM(item_weight_pp) AS item_weight_pp
, SUM(item_price_pp) AS item_price_pp
, SUM(order_price_pp) AS order_price_pp
, SUM(mst_dp) AS mst_dp 
, SUM(mst_hpfs) AS mst_hpfs
, SUM(affi_hpfn) AS affi_hpfn
, SUM(settlement_campaign) AS settlement_campaign -- offline sku system stmt amt
, SUM(campaign_price/offline_sku_exchange) AS campaign_price_usd -- offline sku offline stmt amt
, MAX(sku_off_kind) AS sku_off_kind
, SUM(cfs_gmv) AS cfs_gmv
, SUM(cfs_item_price) AS cfs_item_price
, SUM(cfs_order_price) AS cfs_order_price
, SUM(cfs_shopee_rebate) AS cfs_shopee_rebate
, SUM(cfs_hpfn) AS cfs_hpfn
, SUM(cfs_system_settlement) AS cfs_system_settlement
, SUM(cfs_offline_settlement) AS cfs_offline_settlement -- offline or stmt curr
, SUM(campaign_gmv) AS campaign_gmv
, SUM(campaign_item_price) AS campaign_item_price
, SUM(campaign_order_price) AS campaign_order_price
, SUM(campaign_shopee_rebate) AS campaign_shopee_rebate
, SUM(campaign_hpfn) AS campaign_hpfn
, SUM(campaign_system_settlement) AS campaign_system_settlement
, SUM(campaign_offline_settlement) AS campaign_offline_settlement
, SUM(bundle_gmv) AS bundle_gmv
, SUM(bundle_item_price) AS bundle_item_price
, SUM(bundle_order_price) AS bundle_order_price
, SUM(bundle_shopee_rebate) AS bundle_shopee_rebate
, SUM(bundle_hpfn) AS bundle_hpfn
, SUM(bundle_system_settlement) AS bundle_system_settlement
, SUM(bundle_offline_settlement) AS bundle_offline_settlement 
, TRY(COALESCE(SUM(commission_fee)/SUM(commission_base_amt)*1.00,0)) AS comm_rate
, TRY(COALESCE(SUM(service_fee_item)/SUM(commission_base_amt)*1.00,0)) AS serv_rate 
, SUM(TRY(item_margin * (sku_settlement/sku_stmt_exchange*sku_affi_exchange+affi_hpfn) * (1+(commission_fee*1.00/commission_base_amt)+(service_fee_item*1.00/commission_base_amt)+0.02))) AS item_margin

FROM (SELECT DISTINCT 
i.order_id
, i.order_sn
, item_id
, CASE WHEN pc4.currency IS NULL THEN pc5.sip_exchange 
ELSE pc4.sip_exchange
END AS offline_sku_exchange
, CASE WHEN pc4.currency IS NULL THEN pc5.currency 
ELSE pc4.currency
END AS offline_sku_currency 
, pc1.sip_exchange AS sku_stmt_exchange 
, pc3.sip_exchange AS sku_affi_exchange
, item_margin 
, SUM(commission_base_amt) commission_base_amt
, SUM(item_rebate_by_seller_amt) AS item_rebate_by_seller_amt
, SUM(item_rebate_by_shopee_amt) AS item_rebate_by_shopee_amt
, SUM(item_amount) AS item_amount
, SUM(commission_fee) AS commission_fee --platform charge SIP
, SUM(i.service_fee) AS service_fee_item --platform charge SIP
, SUM(item_tax_amt) AS item_tax_amt
, SUM(item_tax_exemption_amt) AS item_tax_exemption_amt
, SUM(item_weight_pp*item_amount) AS item_weight_pp
, SUM(item_price_pp*item_amount) AS item_price_pp
, SUM(order_price_pp*item_amount) AS order_price_pp
, SUM(IF(mst_promo_price = 0,mst_orig_price,mst_promo_price)*item_amount) AS mst_dp 
, SUM(mst_hpfs*item_amount) AS mst_hpfs
, SUM(affi_hpfn*item_amount) AS affi_hpfn
, SUM(IF(i.kind>0,i.sip_settlement_pp*item_amount,0)) AS settlement_campaign -- offline sku system stmt amt
, SUM(campaign_price*item_amount) AS campaign_price -- offline sku offline stmt amt
, MAX(i.kind) AS sku_off_kind
, SUM(IF(item_promotion_source = 'flash_sale',i.gmv,0)) AS cfs_gmv
, SUM(IF(item_promotion_source = 'flash_sale',item_price_pp*item_amount)) AS cfs_item_price
, SUM(IF(item_promotion_source = 'flash_sale',order_price_pp*item_amount)) AS cfs_order_price
, SUM(IF(item_promotion_source = 'flash_sale',item_rebate_by_shopee_amt)) AS cfs_shopee_rebate
, SUM(IF(item_promotion_source = 'flash_sale',affi_hpfn*item_amount)) AS cfs_hpfn
, SUM(IF(item_promotion_source = 'flash_sale',i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS cfs_system_settlement
, SUM(IF(item_promotion_source = 'flash_sale',IF(campaign_price > 0,campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00,sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS cfs_offline_settlement -- offline or stmt curr
, SUM(IF(item_promotion_source = 'shopee',i.gmv,0)) AS campaign_gmv
, SUM(IF(item_promotion_source = 'shopee',item_price_pp*item_amount)) AS campaign_item_price
, SUM(IF(item_promotion_source = 'shopee',order_price_pp*item_amount)) AS campaign_order_price
, SUM(IF(item_promotion_source = 'shopee',item_rebate_by_shopee_amt)) AS campaign_shopee_rebate
, SUM(IF(item_promotion_source = 'shopee',affi_hpfn*item_amount)) AS campaign_hpfn
, SUM(IF(item_promotion_source = 'shopee',i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS campaign_system_settlement
, SUM(IF(item_promotion_source = 'shopee',IF(campaign_price > 0,campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00,i.sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS campaign_offline_settlement
, SUM(IF(is_bundle_deal = 1,i.gmv,0)) AS bundle_gmv
, SUM(IF(is_bundle_deal = 1,item_price_pp*item_amount)) AS bundle_item_price
, SUM(IF(is_bundle_deal = 1,order_price_pp*item_amount)) AS bundle_order_price
, SUM(IF(is_bundle_deal = 1,item_rebate_by_shopee_amt)) AS bundle_shopee_rebate
, SUM(IF(is_bundle_deal = 1,affi_hpfn*item_amount)) AS bundle_hpfn
, SUM(IF(is_bundle_deal = 1,i.sip_settlement_pp*item_amount/pc1.sip_exchange*1.00)) AS bundle_system_settlement
, SUM(IF(is_bundle_deal = 1,IF(campaign_price > 0,campaign_price/(CASE WHEN i.mst_curr IS NULL THEN pc5.sip_exchange ELSE pc4.sip_exchange END)*1.00,i.sip_settlement_pp/pc1.sip_exchange*1.00)*item_amount)) AS bundle_offline_settlement 
, SUM(i.sip_settlement_pp*item_amount) AS sku_settlement 
FROM regcbbi_others.sip_cn_pnl_item_tab i
LEFT JOIN (SELECT *
FROM price_config) pc1 
ON i.stmt_currency = pc1.currency -- USD / stmt curr 
LEFT JOIN (SELECT *
FROM price_config) pc3 
ON i.grass_region = pc3.grass_region -- USD / affi curr
LEFT JOIN price_config pc4
ON i.mst_curr = pc4.currency 
LEFT JOIN price_config pc5
ON i.mst_country = pc5.grass_region
GROUP BY 1,2,3,4,5,6,7,8 
)
GROUP BY 1, 2
) 


-- convert all stmt amount to p currency 
, consol AS (
SELECT DISTINCT wh_outbound_date
, mst_country
, grass_region
, concat(stmt_currency, mst_currency) stmt_mst_currency_pair
, o.sip_seller_comm_rate
, o.sip_seller_service_rate
, o.sip_seller_txn_rate
, o.sip_seller_management_rate
, o.sip_seller_spp_rate
, COUNT(DISTINCT o.order_id) AS orders 
, SUM(escrow_to_seller_amt) escrow_to_seller_amt
, SUM(gmv) AS gmv
, SUM(pv_rebate_by_shopee_amt) AS pv_rebate_by_shopee_amt
, SUM(sv_rebate_by_shopee_amt) AS sv_rebate_by_shopee_amt
, SUM(pv_coin_earn_by_seller_amt) AS pv_coin_earn_by_seller_amt
, SUM(sv_coin_earn_by_seller_amt) AS sv_coin_earn_by_seller_amt
, SUM(coin_used_cash_amt) AS coin_used_cash_amt
, SUM(card_rebate_by_bank_amt) AS card_rebate_by_bank_amt
, SUM(card_rebate_by_shopee_amt) AS card_rebate_by_shopee_amt
, SUM(item_rebate_by_shopee_amt) AS item_rebate_by_shopee_amt
, SUM(asf_sls) AS actual_shipping_fee
, SUM(actual_buyer_paid_shipping_fee) AS actual_buyer_paid_shipping_fee
, SUM(actual_shipping_rebate_by_shopee_amt) AS actual_shipping_rebate_by_shopee_amt 
, SUM(actual_shipping_rebate_by_seller_amt) actual_shipping_rebate_by_seller_amt
-- , SUM(seller_txn_fee) AS shopee_txn_fee
, SUM(seller_txn_fee) AS shopee_txn_fee
, SUM(commission_fee) AS shopee_commission_fee
, SUM(service_fee_item) AS shopee_service_fee
, SUM(IF(COALESCE(item_tax_amt,0) = 0,tax,item_tax_amt)) AS tax
, SUM(IF(COALESCE(item_tax_exemption_amt,0) = 0,tax_exemption_amt,item_tax_exemption_amt)) AS tax_exemption_amt
, SUM(IF(COALESCE(order_adj_type,0) > 0
, (order_adj_amount/offline_ord_exchange*1.00- settlement_campaign/stmt_exchange*1.00 + campaign_price_usd*1.00)*mst_exchange
, IF(COALESCE(sku_off_kind,0) > 0
, (sip_settlement/stmt_exchange*1.00 - settlement_campaign/stmt_exchange*1.00 + campaign_price_usd*1.00)*mst_exchange
, sip_settlement/stmt_exchange*1.00*mst_exchange
)
)
) AS final_settlement_mst
, SUM(IF(COALESCE(refund_amount,0)>0
, refund_amount 
, 0)) AS refund_amount
, SUM(IF(logistics_status_id = 6 AND payment_method_id = 6
, gmv + pv_rebate_by_shopee_amt + sv_rebate_by_shopee_amt - pv_coin_earn_by_seller_amt - sv_coin_earn_by_seller_amt + coin_used_cash_amt + card_rebate_by_bank_amt + card_rebate_by_shopee_amt + item_rebate_by_shopee_amt + actual_shipping_rebate_by_shopee_amt - seller_txn_fee - commission_fee - service_fee_item - IF(COALESCE(item_tax_amt,0) = 0,tax,item_tax_amt),0)) AS cod_fail
, SUM(sip_settlement/stmt_exchange*1.00*mst_exchange) AS settlement_amount_system_mst
, SUM(IF(order_adj_type = 6
, (sip_settlement/stmt_exchange*1.00 - order_adj_amount/offline_ord_exchange*1.00)*mst_exchange
, 0)
) AS offline_adjustment_mst
, SUM(CASE WHEN order_adj_type = 7 THEN (
(sip_settlement/stmt_exchange*1.00 - order_adj_amount/offline_ord_exchange*1.00)*mst_exchange
)
WHEN (order_adj_type <> 7 OR order_adj_type IS NULL) AND 
((grass_region = 'TH' AND refund_amount >= 150) OR
(grass_region = 'PH' AND refund_amount >= 250) OR
(grass_region = 'BR' AND refund_amount >= 26) OR
(grass_region = 'VN' AND refund_amount >= 113550) OR
(grass_region = 'ID' AND refund_amount >= 71280) OR
(grass_region = 'SG' AND refund_amount >= 6.8) OR
(grass_region = 'MY' AND refund_amount >= 21) OR
(grass_region = 'PL' AND refund_amount >= 20) OR 
(grass_region = 'MX' AND refund_amount >= 103) OR 
(grass_region = 'TW' AND refund_amount > 0)) 
THEN IF(sip_settlement/stmt_exchange*1.00 - TRY(refund_amount/affi_exchange*1.00) < 0
, sip_settlement/stmt_exchange*1.00*mst_exchange
, TRY(refund_amount/affi_exchange*1.00)*mst_exchange
)
ELSE 0 END) AS rr_offset_mst
, SUM(IF(logistics_status_id = 6 AND payment_method_id = 6 AND gmv_usd >= 20
, sip_settlement/stmt_exchange*1.00*mst_exchange,0
)) AS cod_offset_mst
, SUM(pv_rebate_by_seller_amt) AS pv_rebate_by_seller_amt
, SUM(sv_rebate_by_seller_amt) AS sv_rebate_by_seller_amt
, SUM(affi_hpfn) AS affi_hpfn
, SUM(seller_snapshot_discount_shipping_fee) seller_snapshot_discount_shipping_fee
, SUM(item_price_pp) AS item_price
, SUM(order_price_pp) AS order_price
, SUM(cfs_gmv) AS cfs_gmv
, SUM(cfs_item_price) AS cfs_item_price
, SUM(cfs_shopee_rebate) AS cfs_shopee_rebate
, SUM(cfs_hpfn) AS cfs_hpfn
, SUM(cfs_system_settlement*mst_exchange) AS cfs_system_settlement_mst
, SUM(cfs_offline_settlement*mst_exchange) AS cfs_offline_settlement_mst
, SUM(campaign_gmv) AS campaign_gmv
, SUM(campaign_item_price) AS campaign_item_price
, SUM(campaign_shopee_rebate) AS campaign_shopee_rebate
, SUM(campaign_hpfn) AS campaign_hpfn
, SUM(campaign_system_settlement*mst_exchange) AS campaign_system_settlement_mst
, SUM(campaign_offline_settlement*mst_exchange) AS campaign_offline_settlement_mst
, SUM(bundle_gmv) AS bundle_gmv
, SUM(bundle_item_price) AS bundle_item_price
, SUM(bundle_order_price) AS bundle_order_price
, SUM(bundle_hpfn) AS bundle_hpfn
, SUM(bundle_system_settlement*mst_exchange) AS bundle_system_settlement_mst
, SUM(bundle_offline_settlement*mst_exchange) AS bundle_offline_settlement_mst
, SUM(item_weight_pp) AS item_weight
, SUM(actual_weight) AS actual_weight
, SUM(IF(order_adj_type = 8
, (sip_settlement/stmt_exchange*1.00 - order_adj_amount/offline_ord_exchange*1.00)*mst_exchange
,0)
) AS seller_voucher_subsidy_mst
, SUM(IF(order_adj_type = 4
, (sip_settlement/stmt_exchange*1.00 - order_adj_amount/offline_ord_exchange*1.00)*mst_exchange
, 0)
) AS bundle_subsidy_mst
, SUM(pv_coin_earn_by_shopee_amt) pv_coin_earn_by_shopee_amt
, SUM(sv_coin_earn_by_shopee_amt) sv_coin_earn_by_shopee_amt 
, SUM(CASE WHEN stmt_currency IN ('CNY', 'USD') THEN TRY((commission_fee*1.00/commission_base_amt)*(sip_settlement/stmt_exchange*affi_exchange+affi_hpfn))
ELSE 0 END) AS cnsc_comm_fee 
, SUM(CASE WHEN stmt_currency IN ('CNY', 'USD') THEN TRY((service_fee_item*1.00/commission_base_amt)*(sip_settlement/stmt_exchange*affi_exchange+affi_hpfn))
ELSE 0 END) AS cnsc_service_fee
, SUM(CASE WHEN stmt_currency IN ('CNY', 'USD') THEN TRY((seller_txn_fee*1.00/gmv)*(sip_settlement/stmt_exchange*affi_exchange+affi_hpfn))
ELSE 0 END) AS cnsc_txn_fee
, SUM(TRY(shop_margin * (sip_settlement/stmt_exchange*affi_exchange+affi_hpfn) * (1+(commission_fee*1.00/commission_base_amt)+(service_fee_item*1.00/commission_base_amt)+(seller_txn_fee*1.00/gmv)))
) AS shop_margin 
, SUM(cfs_hpfn*(1+comm_rate+serv_rate+txn_rate)*(sip_margin+shop_margin))AS cfs_hpfn_w_fee
, SUM(cfs_offline_settlement*mst_exchange*(1+comm_rate+serv_rate+txn_rate)*(sip_margin+shop_margin)) AS cfs_offline_settlement_w_fee_mst
, SUM(campaign_hpfn*(1+comm_rate+serv_rate+txn_rate)*(sip_margin+shop_margin)) AS campaign_hpfn_w_fee
, SUM(campaign_offline_settlement*mst_exchange*(1+comm_rate+serv_rate+txn_rate)*(sip_margin+shop_margin)) AS campaign_offline_settlement_w_fee_mst 
, SUM(item_amount) item_amount
, SUM(TRY((sip_margin-1) * (sip_settlement/stmt_exchange*affi_exchange+affi_hpfn) * (1+(commission_fee*1.00/commission_base_amt)+(service_fee_item*1.00/commission_base_amt)+(seller_txn_fee*1.00/gmv)))
) AS country_margin
, SUM(commission_base_amt) commission_base_amt 
, SUM(item_margin) item_margin 
FROM o 
JOIN oi ON o.order_id = oi.order_id
WHERE wh_outbound_date >= CURRENT_DATE - INTERVAL '90' DAY AND mst_country <> 'SG'
GROUP BY 1,2,3,4,5,6,7,8,9
)


, consol_2 AS (
SELECT wh_outbound_date
, mst_country
, grass_region
, stmt_mst_currency_pair
, orders
, escrow_to_seller_amt
, gmv
, pv_rebate_by_shopee_amt
, sv_rebate_by_shopee_amt
, pv_coin_earn_by_seller_amt
, sv_coin_earn_by_seller_amt
, coin_used_cash_amt
, card_rebate_by_bank_amt
, card_rebate_by_shopee_amt
, item_rebate_by_shopee_amt
, actual_shipping_fee
, actual_buyer_paid_shipping_fee
, actual_shipping_rebate_by_shopee_amt
, actual_shipping_rebate_by_seller_amt
, shopee_txn_fee
, shopee_commission_fee
, shopee_service_fee
, tax
, tax_exemption_amt
, final_settlement_mst
, final_settlement_mst * sip_seller_comm_rate AS sip_commission_fee_mst
, final_settlement_mst * sip_seller_service_rate AS sip_svc_fee_mst
, final_settlement_mst * sip_seller_spp_rate AS spp_revenue_mst
, refund_amount
, cod_fail
, settlement_amount_system_mst
, offline_adjustment_mst
, rr_offset_mst
, cod_offset_mst
, pv_rebate_by_seller_amt
, sv_rebate_by_seller_amt
, affi_hpfn
, seller_snapshot_discount_shipping_fee
, item_price
, order_price
, cfs_gmv
, cfs_item_price
, cfs_shopee_rebate
, cfs_hpfn
, cfs_system_settlement_mst
, cfs_offline_settlement_mst
, campaign_gmv
, campaign_item_price
, campaign_shopee_rebate
, campaign_hpfn
, campaign_system_settlement_mst
, campaign_offline_settlement_mst
, bundle_gmv
, bundle_item_price
, bundle_order_price
, bundle_hpfn
, bundle_system_settlement_mst
, bundle_offline_settlement_mst
, item_weight
, actual_weight
, seller_voucher_subsidy_mst
, bundle_subsidy_mst
, pv_coin_earn_by_shopee_amt
, sv_coin_earn_by_shopee_amt
, cnsc_comm_fee
, cnsc_service_fee
, cnsc_txn_fee
, shop_margin
, cfs_hpfn_w_fee
, cfs_offline_settlement_w_fee_mst
, campaign_hpfn_w_fee
, campaign_offline_settlement_w_fee_mst
, item_amount
, country_margin
, final_settlement_mst * sip_seller_txn_rate AS txn_revenue_mst
, final_settlement_mst * sip_seller_management_rate AS management_revenue_mst 
, item_margin
FROM consol
)


SELECT DISTINCT 
wh_outbound_date
, mst_country
, grass_region
, stmt_mst_currency_pair
, SUM(orders) AS orders
, SUM(escrow_to_seller_amt) AS escrow_to_seller_amt
, SUM(gmv) AS gmv
, SUM(pv_rebate_by_shopee_amt) AS pv_rebate_by_shopee_amt
, SUM(sv_rebate_by_shopee_amt) AS sv_rebate_by_shopee_amt
, SUM(pv_coin_earn_by_seller_amt) AS pv_coin_earn_by_seller_amt
, SUM(sv_coin_earn_by_seller_amt) AS sv_coin_earn_by_seller_amt
, SUM(coin_used_cash_amt) AS coin_used_cash_amt
, SUM(card_rebate_by_bank_amt) AS card_rebate_by_bank_amt
, SUM(card_rebate_by_shopee_amt) AS card_rebate_by_shopee_amt
, SUM(item_rebate_by_shopee_amt) AS item_rebate_by_shopee_amt
, SUM(actual_shipping_fee) AS actual_shipping_fee
, SUM(actual_buyer_paid_shipping_fee) AS actual_buyer_paid_shipping_fee
, SUM(actual_shipping_rebate_by_shopee_amt) AS actual_shipping_rebate_by_shopee_amt
, SUM(actual_shipping_rebate_by_seller_amt) AS actual_shipping_rebate_by_seller_amt
, SUM(shopee_txn_fee) AS shopee_txn_fee
, SUM(shopee_commission_fee) AS shopee_commission_fee
, SUM(shopee_service_fee) AS shopee_service_fee
, SUM(tax) AS tax
, SUM(tax_exemption_amt) AS tax_exemption_amt
, SUM(final_settlement_mst) AS final_settlement_mst
, SUM(sip_commission_fee_mst) AS sip_commission_fee_mst
, SUM(management_revenue_mst) AS management_revenue_mst
, SUM(spp_revenue_mst) AS spp_revenue_mst
, SUM(refund_amount) AS refund_amount
, SUM(cod_fail) AS cod_fail
, SUM(settlement_amount_system_mst) AS settlement_amount_system_mst
, SUM(offline_adjustment_mst) AS offline_adjustment_mst
, SUM(rr_offset_mst) AS rr_offset_mst
, SUM(cod_offset_mst) AS cod_offset_mst
, SUM(pv_rebate_by_seller_amt) AS pv_rebate_by_seller_amt
, SUM(sv_rebate_by_seller_amt) AS sv_rebate_by_seller_amt
, SUM(affi_hpfn) AS affi_hpfn
, SUM(seller_snapshot_discount_shipping_fee) AS seller_snapshot_discount_shipping_fee
, SUM(item_price) AS item_price
, SUM(order_price) AS order_price
, SUM(cfs_gmv) AS cfs_gmv
, SUM(cfs_item_price) AS cfs_item_price
, SUM(cfs_shopee_rebate) AS cfs_shopee_rebate
, SUM(cfs_hpfn) AS cfs_hpfn
, SUM(cfs_system_settlement_mst) AS cfs_system_settlement_mst
, SUM(cfs_offline_settlement_mst) AS cfs_offline_settlement_mst
, SUM(campaign_gmv) AS campaign_gmv
, SUM(campaign_item_price) AS campaign_item_price
, SUM(campaign_shopee_rebate) AS campaign_shopee_rebate
, SUM(campaign_hpfn) AS campaign_hpfn
, SUM(campaign_system_settlement_mst) AS campaign_system_settlement_mst
, SUM(campaign_offline_settlement_mst) AS campaign_offline_settlement_mst
, SUM(bundle_gmv) AS bundle_gmv
, SUM(bundle_item_price) AS bundle_item_price
, SUM(bundle_order_price) AS bundle_order_price
, SUM(bundle_hpfn) AS bundle_hpfn
, SUM(bundle_system_settlement_mst) AS bundle_system_settlement_mst
, SUM(bundle_offline_settlement_mst) AS bundle_offline_settlement_mst
, SUM(item_weight) AS item_weight
, SUM(actual_weight) AS actual_weight
, SUM(seller_voucher_subsidy_mst) AS seller_voucher_subsidy_mst
, SUM(bundle_subsidy_mst) AS bundle_subsidy_mst
, SUM(pv_coin_earn_by_shopee_amt) AS pv_coin_earn_by_shopee_amt
, SUM(sv_coin_earn_by_shopee_amt) AS sv_coin_earn_by_shopee_amt
, SUM(cnsc_comm_fee) AS cnsc_comm_fee
, SUM(cnsc_service_fee) AS cnsc_service_fee
, SUM(cnsc_txn_fee) AS cnsc_txn_fee
, SUM(shop_margin) AS shop_margin
, SUM(cfs_hpfn_w_fee) AS cfs_hpfn_w_fee
, SUM(cfs_offline_settlement_w_fee_mst) AS cfs_offline_settlement_w_fee_mst
, SUM(campaign_hpfn_w_fee) AS campaign_hpfn_w_fee
, SUM(campaign_offline_settlement_w_fee_mst) AS campaign_offline_settlement_w_fee_mst
, SUM(item_amount) AS item_amount
, SUM(country_margin) AS country_margin
, SUM(txn_revenue_mst) AS txn_revenue_mst
, SUM(sip_svc_fee_mst) AS sip_svc_fee_mst
, SUM(item_margin) item_margin
FROM consol_2 
GROUP BY 1,2,3,4