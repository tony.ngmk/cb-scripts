insert into nexus.test_5e0d1524ab4cbe39ba0a4601e2061ff3aea48bd427e48ee27bce9072eccb0c68_f5fe3e214ce09f4ab03da7986f2747d5 WITH


seller_index AS (
SELECT s.shop_id
FROM mp_user.dim_shop__reg_s0_live AS s
LEFT JOIN (
SELECT COALESCE(sp.grass_region, si.grass_region) AS grass_region
, COALESCE(sp.gp_account_billing_country, si.gp_account_billing_country) AS gp_account_billing_country
, COALESCE(sp.gp_account_owner_userrole_name, si.gp_account_owner_userrole_name) AS gp_account_owner_userrole_name
, COALESCE(sp.child_account_owner_userrole_name, si.child_account_owner_userrole_name) AS child_account_owner_userrole_name
, COALESCE(CAST(sp.child_shopid AS BIGINT), CAST(si.child_shopid AS BIGINT)) AS shop_id
, COALESCE(sp.cb_group, si.cb_group) AS cb_group
FROM regbd_sf.cb__seller_index_tab AS si
FULL OUTER JOIN cncbbi_general.shopee_cb_seller_profile_with_old_column AS sp ON CAST(sp.child_shopid AS BIGINT) = CAST(si.child_shopid AS BIGINT)
WHERE COALESCE(si.grass_region, sp.grass_region) IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
) AS si ON si.shop_id = s.shop_id
WHERE s.tz_type = 'local' AND s.is_cb_shop = 1
AND s.grass_date = DATE_ADD('day', -1, current_date)
AND s.grass_region IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
)






SELECT i1.grass_region
, i1.grass_date
, i1.listing_count
, i2.listing_count_title_change
, i3.listing_count_qc
, i3.listing_count_qc_banned
, i3.listing_count_qc_banned_keyword_spam
, CAST(i3.listing_count_qc_banned_keyword_spam AS DOUBLE) / i3.listing_count_qc_banned AS qc_banned_keyword_spam_perc
, CAST(i3.listing_count_qc_banned AS DOUBLE) / i3.listing_count_qc AS overall_ban_rate
, CAST(i3.listing_count_qc_banned_keyword_spam AS DOUBLE) / i3.listing_count_qc AS keyword_spam_ban_rate
FROM (
SELECT DISTINCT grass_date, grass_region, COUNT(DISTINCT item_id) AS listing_count
FROM mp_item.dim_item__reg_s0_live
WHERE tz_type = 'local' AND is_cb_shop = 1
AND grass_region IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
AND ( grass_date BETWEEN DATE_ADD('day', -14, current_date) AND DATE_ADD('day', -1, current_date)
OR grass_date BETWEEN DATE('2021-12-07') AND DATE('2021-12-16') )
AND status = 1 AND shop_status = 1 AND seller_status = 1 AND stock > 0 AND is_holiday_mode = 0
GROUP BY 1,2
) AS i1
LEFT JOIN (
SELECT DATE(FROM_UNIXTIME(i.ctime)) AS grass_date
, i.country AS grass_region
, COUNT(DISTINCT CASE WHEN f.name = 'name' THEN i.itemid ELSE NULL END) AS listing_count_title_change
FROM marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live AS i
CROSS JOIN UNNEST(cast(JSON_EXTRACT(json_parse(from_utf8(i.data)),'$.Fields') as Array(Row(name varchar, new varchar, old varchar)))) as f
INNER JOIN seller_index AS cb ON cb.shop_id = i.shopid
WHERE ( DATE(FROM_UNIXTIME(i.ctime)) BETWEEN DATE_ADD('day', -14, current_date) AND DATE_ADD('day', -1, current_date)
OR DATE(FROM_UNIXTIME(i.ctime)) BETWEEN DATE('2021-12-07') AND DATE('2021-12-16') ) 
AND date(i.grass_date) >= DATE('2021-12-07')
--AND f.name = 'name'
AND i.country IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
GROUP BY 1,2
) AS i2 ON (i2.grass_date = i1.grass_date AND i2.grass_region = i1.grass_region)
LEFT JOIN (
SELECT DATE(FROM_UNIXTIME(i.update_time)) AS grass_date
, i.grass_region
, COUNT(DISTINCT i.item_id) AS listing_count_qc
, COUNT(DISTINCT CASE WHEN i.action = 2 THEN i.item_id ELSE NULL END) AS listing_count_qc_banned
, COUNT(DISTINCT CASE WHEN i.action = 2 AND (
lower(JSON_EXTRACT_SCALAR(i.fail_reason, '$[0].description')) LIKE '%irrelevant keywords in product title%'
OR lower(JSON_EXTRACT_SCALAR(i.fail_reason, '$[0].description')) LIKE '%kata kunci yang tidak%'
OR lower(JSON_EXTRACT_SCALAR(i.fail_reason, '$[0].description')) LIKE '%kata kunci yang tidak sesuai%'
OR lower(JSON_EXTRACT_SCALAR(i.fail_reason, '$[0].description')) LIKE '%keyword spam%'
OR lower(JSON_EXTRACT_SCALAR(i.fail_reason, '$[0].description')) LIKE '%濫用文字誤導搜尋%'
OR lower(JSON_EXTRACT_SCALAR(i.fail_reason, '$[0].description')) LIKE '%thông tin của quá nhiều sản phẩm%' )
THEN i.item_id ELSE NULL END) AS listing_count_qc_banned_keyword_spam
FROM marketplace.shopee_quality_control_platform_v2_db__item_review_history_tab__reg_daily_s0_live AS i
INNER JOIN seller_index AS cb ON cb.shop_id = i.shop_id
WHERE i.action IN (1,2,3) -- 1 Pass 2 Ban 3 Delete
AND ( DATE(FROM_UNIXTIME(i.update_time)) BETWEEN DATE_ADD('day', -14, current_date) AND DATE_ADD('day', -1, current_date)
OR DATE(FROM_UNIXTIME(i.update_time)) BETWEEN DATE('2021-12-07') AND DATE('2021-12-16') )
--AND qc_level IN (1,2)
--AND qc_reason NOT IN ('MassDelete', 'MassBan', 'MassRevertDelete', 'MassRevertBan')
--AND remark != 'Mass Pass Action'
AND i.grass_region IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
GROUP BY 1,2
) AS i3 ON (i3.grass_date = i1.grass_date AND i3.grass_region = i1.grass_region)
ORDER BY 1, 2 DESC