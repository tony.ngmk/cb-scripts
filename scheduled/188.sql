insert into nexus.test_857f009da6f48ce7f123de71720c493ef1e13d4cfd53b524ac06a488c528dd2e_1c2dca3f5b07f87ca40730ae4a977363 WITH dim AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE grass_date >= current_date - interval '150' day
) 
, buyer_account AS (
SELECT *
FROM (
VALUES 
ROW (32352918, 'TW')
, ROW (174331327, 'MY')
, ROW (216124110, 'ID')
) t(buyer_id, grass_region)
) 
, sip AS (
SELECT DISTINCT
affi_shopid
, a.country affi_country
, b.country mst_country
, mst_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE cb_option = 0 AND country = 'ID'
) b
ON a.mst_shopid = b.shopid
WHERE a.grass_region IN ('MY','SG','ID','PH','VN','TH','BR','MX','CO','CL')
) 
, raw AS (
SELECT DISTINCT
oversea_orderid
, max(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE oversea_country in ('my','sg','id','ph','vn','th','br','mx','co','cl')
GROUP BY 1
) 
, net AS (
SELECT DISTINCT
shop_id
, date(split(create_datetime,' ')[1]) create_time
, count(DISTINCT order_id) gross_order
, count(DISTINCT CASE WHEN is_net_order = 1 THEN order_id ELSE null END) net_order
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE
date(split(create_datetime,' ')[1]) >= current_date - interval '150' day 
AND tz_type = 'local'
AND grass_region IN ('MY','SG','ID','PH','VN','TH','BR','MX','CO','CL') 
AND is_cb_shop = 1 
AND grass_date >= current_date - interval '150' day
GROUP BY 1, 2
) 
, cancel AS (
SELECT DISTINCT
o1.shopid
, CASE WHEN o1.grass_region IN ('SG', 'MY', 'PH', 'TW') THEN date(from_unixtime(o1.cancel_time)) 
WHEN o1.grass_region IN ('TH', 'ID', 'VN') THEN date(from_unixtime(o1.cancel_time) - INTERVAL '1' HOUR) 
WHEN o1.grass_region = 'BR' THEN date(from_unixtime(o1.cancel_time) - INTERVAL '11' HOUR) 
WHEN o1.grass_region = 'MX' THEN date(from_unixtime(o1.cancel_time) - INTERVAL '14' HOUR) 
WHEN o1.grass_region = 'CL' THEN CAST(format_datetime(from_unixtime(o1.cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN o1.grass_region = 'CO' THEN CAST(format_datetime(from_unixtime(o1.cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
END AS cancel_time
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) THEN o1.orderid 
WHEN (NOT (o1.grass_region = 'TW')) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) THEN o1.orderid
ELSE null END)) cancel_order
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) AND local_orderid > 0 THEN o1.orderid
WHEN (NOT (o1.grass_region = 'TW')) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid
ELSE null END)) cancel_order_sync_suc
, count(DISTINCT (CASE WHEN o1.grass_region = 'TW' AND ((NOT (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) OR o2.cancel_reason IS NULL) AND o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303) AND local_orderid > 0 THEN o1.orderid 
WHEN (NOT (o1.grass_region = 'TW')) AND o2.grass_region = 'TW' AND ((NOT (o2.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303 ))) OR o2.cancel_reason IS NULL) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid
WHEN (NOT (o1.grass_region = 'TW')) AND (NOT (o2.grass_region = 'TW')) AND ((NOT (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303 ))) OR o2.cancel_reason IS NULL) AND o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303) AND local_orderid > 0 THEN o1.orderid 
ELSE null END)) sync_not_match
, count(DISTINCT (CASE WHEN o1.cancel_reason = 204 AND l.ar_time IS NOT NULL THEN o1.orderid
WHEN o1.cancel_reason IN (302,303) AND l.pu_time IS NOT NULL THEN o1.orderid
ELSE null END)) acl_order
, count(DISTINCT (CASE WHEN o1.cancel_reason IN (204, 302,303) AND ba.buyer_id IS NOT NULL THEN o1.orderid ELSE null END)) seller_fault_v2
FROM 
raw
LEFT JOIN (
SELECT DISTINCT
orderid
, cancel_time
, extinfo.cancel_reason
, shopid
, grass_region
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE
cb_option = 1
AND grass_region IN ('MY','SG','PH','VN','TH','BR','MX','CO','CL')
AND date(from_unixtime(create_time)) >= current_date - interval '150' day 
) o1
ON o1.orderid = raw.oversea_orderid 
LEFT JOIN (
SELECT DISTINCT
order_id orderid
, cancel_timestamp cancel_time
, cancel_reason_id cancel_reason
, grass_region
, cancel_user_id
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE
is_cb_shop = 0 
AND grass_region IN ('ID') 
AND date(from_unixtime(create_timestamp)) >= current_date - interval '150' day 
AND grass_date >= current_date - interval '150' day 
) o2
ON o2.orderid = raw.local_orderid
-- LEFT JOIN (
-- SELECT DISTINCT
-- order_id
-- , order_sn
-- , cancel_user_id
-- , grass_region
-- FROM
-- shopee.order_mart_dwd_order_all_event_final_status_df
-- WHERE ((is_cb_shop = 0) AND (grass_region IN ( 'ID'))) and date(from_unixtime(create_timestamp)) >= current_date-interval'150'day
-- and grass_date>= current_date-interval'150'day
-- ) o3 ON (o3.order_id = o2.orderid))
LEFT JOIN buyer_account ba
ON ba.buyer_id = o2.cancel_user_id 
AND ba.grass_region = o2.grass_region
LEFT JOIN (
SELECT DISTINCT
orderid
, min(CASE WHEN new_status = 1 THEN ctime ELSE null END) ar_time
, min(CASE WHEN new_status = 2 THEN ctime ELSE null END) pu_time
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_continuous_s0_live
WHERE grass_region IN ('MY','SG','ID','PH','VN','TH','BR','MX')
AND date(from_unixtime(ctime)) >= current_date - interval '150' day
GROUP BY 1
) l
ON l.orderid = raw.local_orderid
WHERE 
date(from_unixtime(o1.cancel_time)) >= current_date - interval '120' day
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
shopid
, CASE WHEN grass_region IN ('SG', 'MY', 'PH', 'TW') THEN date(from_unixtime(mtime))
WHEN grass_region IN ('TH', 'ID', 'VN') THEN date(from_unixtime(mtime) - INTERVAL '1' HOUR) 
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime) - INTERVAL '11' HOUR)
WHEN grass_region = 'MX' THEN date(from_unixtime(mtime) - INTERVAL '14' HOUR)
WHEN grass_region = 'CL' THEN CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN grass_region = 'CO' THEN CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
END mtime
, count(DISTINCT (CASE WHEN grass_region = 'TW' AND reason IN (1, 2, 3, 103, 105) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid 
WHEN grass_region <> 'TW' AND reason IN (1, 2, 3, 103, 105, 107) AND (cancel_reason = 0 OR cancel_reason IS NULL) AND status IN (2, 5) THEN a.orderid
ELSE null END)) RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE cb_option = 1 
AND grass_region IN ('MY', 'SG', 'ID', 'PH','VN','TH','BR','MX') 
AND date(from_unixtime(create_time)) >= current_date - interval '150' day
) b 
ON a.orderid = b.orderid
WHERE
date(from_unixtime(ctime)) >= current_date - interval '150' day
AND a.cb_option = 1
AND grass_region IN ('MY', 'SG', 'ID', 'PH','VN','TH','BR','MX')
GROUP BY 1, 2
) 
SELECT DISTINCT
sip.affi_country
, sip.mst_country
, sip.affi_shopid
, sip.mst_shopid
, nfr_included_cancellations_7d
, nfr_included_rr_7d
, net_orders_7d
, l7d_cr l7d_cancellation_date
, l30d_cr l30d_cancellation_date
, COALESCE(sum(CASE WHEN net.create_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN gross_order ELSE null END), 0) gross_order_7d
, COALESCE(sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN cancel_order_sync_suc ELSE null END), 0) cancel_order_sync_suc_7d
, COALESCE((sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN cancel_order_sync_suc ELSE null END)
- sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN sync_not_match ELSE null END)
+ sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN seller_fault_v2 ELSE null END)), 0) seller_fault_7d
, COALESCE((sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN cancel_order ELSE null END) 
- sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN cancel_order_sync_suc ELSE null END)), 0) a_p_synced_failed_7d
, COALESCE((sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN sync_not_match ELSE null END) 
- sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN seller_fault_v2 ELSE null END)), 0) p_a_sync_failed_7d
, COALESCE(sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN acl_order ELSE null END), 0) p_a_order_synced_failed_acl1_acl2_7d
, COALESCE((sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN sync_not_match ELSE null END)
- sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN acl_order ELSE null END)
- sum(CASE WHEN cancel.cancel_time BETWEEN current_date - INTERVAL '7' DAY AND current_date - INTERVAL '1' DAY THEN seller_fault_v2 ELSE null END)), 0) p_a_other_sync_failed_7d
FROM
dim
INNER JOIN sip
ON dim.grass_region = sip.affi_country
INNER JOIN (
SELECT DISTINCT
affi_shopid
, l7d_cr
, net_orders_7d
, nfr_included_cancellations_7d
, l30d_cr
, nfr_included_rr_7d
FROM
regcbbi_others.sip_ops_performance
) ops 
ON ops.affi_shopid = sip.affi_shopid
INNER JOIN (
SELECT DISTINCT shop_id shopid
FROM
regcbbi_others.shop_profile
WHERE
status = 1
AND user_status = 1
AND is_holiday_mode = 0
AND grass_date = current_date - INTERVAL '1' DAY 
AND is_cb_shop = 1
) u
ON u.shopid = sip.affi_shopid
LEFT JOIN net
ON sip.affi_shopid = net.shop_id
AND dim.grass_date = net.create_time
LEFT JOIN cancel
ON sip.affi_shopid = cancel.shopid
AND dim.grass_date = cancel.cancel_time
WHERE
mst_country = 'ID'
AND net_orders_7d + nfr_included_cancellations_7d + nfr_included_rr_7d > 0
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9