insert into nexus.test_7e155944298e045766d193b12434211bb834a85f27a723058bf6573464a4f70c_15370cb851ee1b8f24e260f5374add50 WITH t AS
(
SELECT ord.*, rr.return_time
FROM
(SELECT orderid
, ordersn
, service_code
, pay_time
, coalesce(cancel_completed_time, invalid_time) AS cancel_time
, cdt_with_ext
, DATE(from_unixtime(expected_receive_time)) AS EDD
, seller_shipout_time AS shipout_time
, pickup_done_time AS pickup_time
, wh_inbound_time AS wh_inbound_date
, wh_outbound_time AS wh_outbound_date
, delivery_time
, dest_country_arrival_time AS br_arrival_time
, lm_cc_done_time AS cc_done_time
, coalesce(lm_cc_done_time, lm_inbound_time_actual) AS lm_inbound_time
, first_delivery_attempt_time
FROM regcbbi_others.cb_logistics_mart_test
WHERE grass_region = 'BR'
AND channel_id in (90001, 90002)
) ord
LEFT JOIN
(SELECT orderid, min(ctime) AS return_time
FROM marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE grass_region = 'BR'
AND status in (2, 5) --'RETURN_ACCEPTED', 'RETURN_REFUND_PAID'
AND ctime >= to_unixtime(date_add('day', -210, date(from_unixtime(1655614800))))
GROUP BY 1
) rr 
ON ord.orderid = rr.orderid
),


daily_summary AS
(SELECT CASE WHEN t.service_code in ('B01', 'B02') then '2. China Post'
WHEN t.service_code in ('B05', 'B06') then '3. Asendia'
WHEN t.service_code in ('B09', 'B10', 'B11', 'B12','B13','B14','B15') then '4. Correios'
WHEN t.service_code in ('B07', 'B08') then '1. Sweden Post'
END AS vendor
----yesterday----
, 0 AS D_1_paid_ord
, count(distinct IF(DATE(from_unixtime(shipout_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_shipout_ord
, count(distinct IF(DATE(from_unixtime(cancel_time)) = date(from_unixtime(1655614800)) AND shipout_time > 0, orderid, NULL)) AS D_1_cancelled_ord
, count(distinct IF(DATE(from_unixtime(return_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_returned_ord
, count(distinct IF(DATE(from_unixtime(wh_inbound_date)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_wh_inbound_ord
, count(distinct IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_wh_outbound_ord
, count(distinct IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_br_arrival_ord
, count(distinct IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_cc_done_ord 
, count(distinct IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_delivered_ord
, try(cast(count(distinct IF(DATE(from_unixtime(first_delivery_attempt_time)) = date(from_unixtime(1655614800)) AND DATE(from_unixtime(first_delivery_attempt_time)) > EDD, orderid, NULL)) AS DOUBLE)/cast(count(distinct IF(DATE(from_unixtime(first_delivery_attempt_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)) AS D_1_aft_EDD_perc


, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL)) AS D_1_total_lt_avg
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.8) AS D_1_total_lt_80p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.9) AS D_1_total_lt_90p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.95) AS D_1_total_lt_95p


, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL)) AS D_1_lm_lt_avg --lm inbound time is earlier of cc done time and lm inbound time
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.8) AS D_1_lm_lt_80p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.9) AS D_1_lm_lt_90p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.95) AS D_1_lm_lt_95p


, avg(IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL)) AS D_1_cc_lt_avg
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.8) AS D_1_cc_lt_80p
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.9) AS D_1_cc_lt_90p
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.95) AS D_1_cc_lt_95p


, avg(IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL)) AS D_1_lh_lt_avg
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.8) AS D_1_lh_lt_80p
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.9) AS D_1_lh_lt_90p
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.95) AS D_1_lh_lt_95p

, avg(IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL)) AS D_1_wh_process_time_avg
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.8) AS D_1_wh_process_time_80p
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.9) AS D_1_wh_process_time_90p
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.95) AS D_1_wh_process_time_95p


, avg(IF(DATE(from_unixtime(pickup_time)) = date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL)) AS D_1_seller_apt_avg
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) = date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.8) AS D_1_seller_apt_80p
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) = date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.9) AS D_1_seller_apt_90p
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) = date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.95) AS D_1_seller_apt_95p


----past 7 day avg----
, 0 AS L7D_paid_ord
, cast(count(distinct IF(DATE(from_unixtime(shipout_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_shipout_ord
, cast(count(distinct IF(DATE(from_unixtime(cancel_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pay_time > 0, orderid, NULL)) AS DOUBLE)/7 AS L7D_cancelled_ord
, cast(count(distinct IF(DATE(from_unixtime(return_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_returned_ord
, cast(count(distinct IF(DATE(from_unixtime(wh_inbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_wh_inbound_ord
, cast(count(distinct IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_wh_outbound_ord
, cast(count(distinct IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_br_arrival_ord
, cast(count(distinct IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_cc_done_ord 
, cast(count(distinct IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_delivered_ord
, try(cast(count(distinct IF(DATE(from_unixtime(first_delivery_attempt_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND DATE(from_unixtime(first_delivery_attempt_time)) > EDD, orderid, NULL)) AS DOUBLE)/cast(count(distinct IF(DATE(from_unixtime(first_delivery_attempt_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)) AS L7D_aft_EDD_perc

, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL)) AS L7D_total_lt_avg
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.8) AS L7D_total_lt_80p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.9) AS L7D_total_lt_90p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.95) AS L7D_total_lt_95p


, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL)) AS L7D_lm_lt_avg --lm inbound time is earlier of cc done time and lm inbound time
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.8) AS L7D_lm_lt_80p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.9) AS L7D_lm_lt_90p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.95) AS L7D_lm_lt_95p


, avg(IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL)) AS L7D_cc_lt_avg
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.8) AS L7D_cc_lt_80p
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.9) AS L7D_cc_lt_90p
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.95) AS L7D_cc_lt_95p


, avg(IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL)) AS L7D_lh_lt_avg
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.8) AS L7D_lh_lt_80p
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.9) AS L7D_lh_lt_90p
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.95) AS L7D_lh_lt_95p


, avg(IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL)) AS L7D_wh_process_time_avg
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.8) AS L7D_wh_process_time_80p
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.9) AS L7D_wh_process_time_90p
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.95) AS L7D_wh_process_time_95p


, avg(IF(DATE(from_unixtime(pickup_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL)) AS L7D_seller_apt_avg
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.8) AS L7D_seller_apt_80p
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.9) AS L7D_seller_apt_90p
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.95) AS L7D_seller_apt_95p


FROM t 
GROUP BY 1
),


daily_summary_all AS
(SELECT '0. Total' AS vendor
----yesterday----
, count(distinct IF(DATE(from_unixtime(pay_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_paid_ord
, count(distinct IF(DATE(from_unixtime(shipout_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_shipout_ord
, count(distinct IF(DATE(from_unixtime(cancel_time)) = date(from_unixtime(1655614800)) AND shipout_time > 0, orderid, NULL)) AS D_1_cancelled_ord
, count(distinct IF(DATE(from_unixtime(return_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_returned_ord
, count(distinct IF(DATE(from_unixtime(wh_inbound_date)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_wh_inbound_ord
, count(distinct IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_wh_outbound_ord
, count(distinct IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_br_arrival_ord
, count(distinct IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_cc_done_ord 
, count(distinct IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS D_1_delivered_ord
, try(cast(count(distinct IF(DATE(from_unixtime(first_delivery_attempt_time)) = date(from_unixtime(1655614800)) AND DATE(from_unixtime(first_delivery_attempt_time)) > EDD, orderid, NULL)) AS DOUBLE)/cast(count(distinct IF(DATE(from_unixtime(first_delivery_attempt_time)) = date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)) AS D_1_aft_EDD_perc


, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL)) AS D_1_total_lt_avg
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.8) AS D_1_total_lt_80p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.9) AS D_1_total_lt_90p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.95) AS D_1_total_lt_95p


, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL)) AS D_1_lm_lt_avg --lm inbound time is earlier of cc done time and lm inbound time
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.8) AS D_1_lm_lt_80p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.9) AS D_1_lm_lt_90p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.95) AS D_1_lm_lt_95p


, avg(IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL)) AS D_1_cc_lt_avg
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.8) AS D_1_cc_lt_80p
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.9) AS D_1_cc_lt_90p
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) = date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.95) AS D_1_cc_lt_95p


, avg(IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL)) AS D_1_lh_lt_avg
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.8) AS D_1_lh_lt_80p
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.9) AS D_1_lh_lt_90p
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) = date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.95) AS D_1_lh_lt_95p


, avg(IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL)) AS D_1_wh_process_time_avg
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.8) AS D_1_wh_process_time_80p
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.9) AS D_1_wh_process_time_90p
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) = date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.95) AS D_1_wh_process_time_95p


, avg(IF(DATE(from_unixtime(pickup_time)) = date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL)) AS D_1_seller_apt_avg
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) = date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.8) AS D_1_seller_apt_80p
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) = date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.9) AS D_1_seller_apt_90p
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) = date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.95) AS D_1_seller_apt_95p


----past 7 day avg----
, cast(count(distinct IF(DATE(from_unixtime(pay_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_paid_ord
, cast(count(distinct IF(DATE(from_unixtime(shipout_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_shipout_ord
, cast(count(distinct IF(DATE(from_unixtime(cancel_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pay_time > 0, orderid, NULL)) AS DOUBLE)/7 AS L7D_cancelled_ord
, cast(count(distinct IF(DATE(from_unixtime(return_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_returned_ord
, cast(count(distinct IF(DATE(from_unixtime(wh_inbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_wh_inbound_ord
, cast(count(distinct IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_wh_outbound_ord
, cast(count(distinct IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_br_arrival_ord
, cast(count(distinct IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_cc_done_ord
, cast(count(distinct IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)/7 AS L7D_delivered_ord
, try(cast(count(distinct IF(DATE(from_unixtime(first_delivery_attempt_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND DATE(from_unixtime(first_delivery_attempt_time)) > EDD, orderid, NULL)) AS DOUBLE)/cast(count(distinct IF(DATE(from_unixtime(first_delivery_attempt_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)), orderid, NULL)) AS DOUBLE)) AS L7D_aft_EDD_perc

, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL)) AS L7D_total_lt_avg
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.8) AS L7D_total_lt_80p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.9) AS L7D_total_lt_90p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > pay_time, cast((delivery_time - pay_time) AS DOUBLE)/86400, NULL), 0.95) AS L7D_total_lt_95p


, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL)) AS L7D_lm_lt_avg --lm inbound time is earlier of cc done time and lm inbound time
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.8) AS L7D_lm_lt_80p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.9) AS L7D_lm_lt_90p
, approx_percentile(IF(DATE(from_unixtime(delivery_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL), 0.95) AS L7D_lm_lt_95p


, avg(IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL)) AS L7D_cc_lt_avg
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.8) AS L7D_cc_lt_80p
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.9) AS L7D_cc_lt_90p
, approx_percentile(IF(DATE(from_unixtime(cc_done_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND cc_done_time > br_arrival_time, cast((cc_done_time - br_arrival_time) AS DOUBLE)/86400, NULL), 0.95) AS L7D_cc_lt_95p


, avg(IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL)) AS L7D_lh_lt_avg
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.8) AS L7D_lh_lt_80p
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.9) AS L7D_lh_lt_90p
, approx_percentile(IF(DATE(from_unixtime(br_arrival_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND br_arrival_time > wh_outbound_date, cast((br_arrival_time - wh_outbound_date) AS DOUBLE)/86400, NULL), 0.95) AS L7D_lh_lt_95p


, avg(IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL)) AS L7D_wh_process_time_avg
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.8) AS L7D_wh_process_time_80p
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.9) AS L7D_wh_process_time_90p
, approx_percentile(IF(DATE(from_unixtime(wh_outbound_date)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND wh_outbound_date > wh_inbound_date, cast((wh_outbound_date - wh_inbound_date) AS DOUBLE)/86400, NULL), 0.95) AS L7D_wh_process_time_95p


, avg(IF(DATE(from_unixtime(pickup_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL)) AS L7D_seller_apt_avg
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.8) AS L7D_seller_apt_80p
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.9) AS L7D_seller_apt_90p
, approx_percentile(IF(DATE(from_unixtime(pickup_time)) between date_add('day', -6, date(from_unixtime(1655614800))) AND date(from_unixtime(1655614800)) AND pickup_time > pay_time, cast((pickup_time - pay_time) AS DOUBLE)/86400, NULL), 0.95) AS L7D_seller_apt_95p


FROM t 
GROUP BY 1
)


SELECT vendor
, D_1_paid_ord
, D_1_shipout_ord
, D_1_cancelled_ord
, D_1_returned_ord
, D_1_wh_inbound_ord
, D_1_wh_outbound_ord
, D_1_br_arrival_ord
, D_1_cc