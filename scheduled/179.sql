insert into nexus.test_2bc56a6dc2318313a44a6d6e4398e85ecea1dab61d5c82f7949e5f1918de0015_32e0e2280cdc7dc0b191239111978db1 WITH
focus AS (
SELECT
map.affi_country
, map.affi_shopid
, map.affi_itemid
, a_i.item_name affi_item_name
, map.mst_itemid
, map.mst_shopid
, sub_cat
, l30d_ado
, p_i.grass_region mst_country
FROM
((((((
SELECT
item_id
, name item_name
, shop_id
, level2_global_be_category_id sub_cat
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((((((status = 1) AND (shop_status = 1)) AND (seller_status = 1)) AND (stock > 0)) AND (is_holiday_mode = 0)) AND (is_cb_shop = 1)) AND (grass_region IN ('TH'))) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (tz_type = 'local'))
) a_i
INNER JOIN (
SELECT CAST(a_shopid AS bigint) affi_shopid
FROM
regcbbi_others.sip_hps_shop_th__df__reg__s0
) a_s ON (a_s.affi_shopid = a_i.shop_id))
INNER JOIN (
SELECT
affi_itemid
, affi_shopid
, mst_itemid
, mst_shopid
, affi_country
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE (affi_country = 'TH')
) map ON (a_i.item_id = map.affi_itemid))
INNER JOIN (
SELECT
item_id
, name
, grass_region
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((((((status = 1) AND (shop_status = 1)) AND (seller_status = 1)) AND (stock > 0)) AND (is_holiday_mode = 0)) AND (grass_region IN ('MY', 'TW'))) AND (is_cb_shop = 1)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (tz_type = 'local'))
) p_i ON (p_i.item_id = map.mst_itemid))
LEFT JOIN (
SELECT itemid
FROM
marketplace.shopee_listing_qc_backend_th_db__screening_qclog__reg_daily_s0_live
WHERE ((status IN (2, 4, 5)) AND (classification = 0))
) ex_i ON (ex_i.itemid = a_i.item_id))
LEFT JOIN (
SELECT
item_id
, ("sum"(order_fraction) / DECIMAL '30.00') l30d_ado
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE (((((is_placed = 1) AND (tz_type = 'local')) AND (is_cb_shop = 1)) AND (grass_region IN ('TH'))) AND (grass_date >= (current_date - INTERVAL '30' DAY)))
GROUP BY 1
) o ON (o.item_id = a_i.item_id))
WHERE (ex_i.itemid IS NULL)
) 
, ex AS (
SELECT DISTINCT
auditid audit_id
, itemid
, shopid
, country
, name upload_option
, new upload_content
, TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Operator')) operator
, TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Source')) source
, CAST(grass_date AS date) ctime
FROM
(marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
CROSS JOIN UNNEST(CAST(TRY("json_extract"(TRY("from_utf8"(data)), '$.Fields')) AS array(row(name varchar,old varchar,new varchar)))))
WHERE (((((TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Operator')) IN ('zhixian.toh@shopee.com')) OR (TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Source')) = 'Seller Center Single')) AND (name IN ('name'))) AND (country = 'TH')) AND (CAST(grass_date AS date) BETWEEN (current_date - INTERVAL '120' DAY) AND (current_date - INTERVAL '1' DAY)))
) 
, ph AS (
SELECT
affi_itemid
, mst_itemid
, item_name
, "concat"('http://shopee.ph/product/', CAST(m.affi_shopid AS varchar), '/', CAST(i.item_id AS varchar)) eng_reference_link
FROM
((
SELECT
item_id
, name item_name
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((grass_date = (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local')) AND (is_cb_shop = 1)) AND (grass_region = 'PH')) AND (status = 1))
) i
INNER JOIN marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live m ON (i.item_id = m.affi_itemid))
WHERE (affi_country = 'PH')
) 
, sg AS (
SELECT
affi_itemid
, mst_itemid
, item_name
, "concat"('http://shopee.sg/product/', CAST(m.affi_shopid AS varchar), '/', CAST(i.item_id AS varchar)) eng_reference_link
FROM
((
SELECT
item_id
, name item_name
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((grass_date = (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local')) AND (is_cb_shop = 1)) AND (grass_region = 'SG')) AND (status = 1))
) i
INNER JOIN marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live m ON (i.item_id = m.affi_itemid))
WHERE (affi_country = 'SG')
) 
, my AS (
SELECT
affi_itemid
, mst_itemid
, item_name
, "concat"('http://shopee.com.my/product/', CAST(m.affi_shopid AS varchar), '/', CAST(i.item_id AS varchar)) eng_reference_link
FROM
((
SELECT
item_id
, name item_name
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((grass_date = (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local')) AND (is_cb_shop = 1)) AND (grass_region = 'MY')) AND (status = 1))
) i
INNER JOIN marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live m ON (i.item_id = m.affi_itemid))
WHERE (affi_country = 'MY')
) 
, summary AS (
SELECT
f.affi_country
, f.affi_shopid
, f.affi_itemid
, "concat"('https://shopee.co.th/product/', CAST(f.affi_shopid AS varchar), '/', CAST(f.affi_itemid AS varchar)) affi_product_link
, f.affi_item_name
, f.sub_cat
, f.l30d_ado affi_l30d_ado
, (CASE WHEN (ph.affi_itemid IS NOT NULL) THEN ph.item_name WHEN ((ph.affi_itemid IS NULL) AND (sg.affi_itemid IS NOT NULL)) THEN sg.item_name WHEN (((ph.affi_itemid IS NULL) AND (sg.affi_itemid IS NULL)) AND (my.affi_itemid IS NOT NULL)) THEN my.item_name ELSE null END) english_name
, (CASE WHEN (ph.affi_itemid IS NOT NULL) THEN ph.eng_reference_link WHEN ((ph.affi_itemid IS NULL) AND (sg.affi_itemid IS NOT NULL)) THEN sg.eng_reference_link WHEN (((ph.affi_itemid IS NULL) AND (sg.affi_itemid IS NULL)) AND (my.affi_itemid IS NOT NULL)) THEN my.eng_reference_link ELSE null END) eng_reference_link
, (CASE WHEN (f.mst_country = 'MY') THEN "concat"('http://shopee.com.my/product/', CAST(f.mst_shopid AS varchar), '/', CAST(f.mst_itemid AS varchar)) WHEN (f.mst_country = 'TW') THEN "concat"('http://shopee.tw/product/', CAST(f.mst_shopid AS varchar), '/', CAST(f.mst_itemid AS varchar)) END) mst_product_link
FROM
(((((focus f
LEFT JOIN ex ON (ex.itemid = f.affi_itemid))
LEFT JOIN regcbbi_others.sip__listing_optimization_bau_exclude_list__wf__reg__s0 ex1 ON (CAST(ex1.affi_itemid AS bigint) = f.affi_itemid))
LEFT JOIN ph ON (ph.mst_itemid = f.mst_itemid))
LEFT JOIN sg ON (sg.mst_itemid = f.mst_itemid))
LEFT JOIN my ON (my.mst_itemid = f.mst_itemid))
WHERE ((ex.itemid IS NULL) AND (ex1.affi_itemid IS NULL))
) 
SELECT
affi_country
, affi_shopid
, affi_itemid
, affi_product_link
, affi_item_name
, '' affi_description
, english_name
, eng_reference_link
, '' Priority
, '' Human_translated_title_local_language
, '' Keyword
, '' Human_translated_description_EN
, '' Machine_translation_score_Title
, '' Machine_translation_score_Description
, '' Title_length_Y_N
, '' Description_length_Y_N
, '' Confirm_to_upload_Y_N
, '' Remark_for_N_reason
, '' Other_remarks
, sub_cat
, mst_product_link
, affi_l30d_ado
, "rank"() OVER (PARTITION BY affi_country ORDER BY affi_l30d_ado DESC) ado_rank
FROM
summary
WHERE (english_name IS NOT NULL)
ORDER BY affi_country ASC, affi_l30d_ado DESC
LIMIT 1000