/********************************** 
Name: Traffic Monitoring - CFS
Function: 
Notes: workflow name = CBWH - Traffic Monitoring CFS

Date Developer Activity
2022-03-31 Mingyu add 3PF seller type 
2022-03-30 Mingyu exclude SLS Orders 
2022-05-16 Hannah exclude lovito 
2022-06-08 Hannah cfs slot too high issue

**********************************/ 
WITH rate as (
SELECT
grass_region,
exchange_rate
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE
grass_date = current_date - interval '1' day
),






-- nnwh_shops AS (
-- -- SELECT
-- -- DISTINCT s1.shopee_entity,
-- -- s2.shop_id
-- -- FROM
-- -- sbs_mart.shopee_pms_db__fbs_seller_tab__reg_daily_s0_live s1
-- -- LEFT JOIN sbs_mart.shopee_pms_db__fbs_supplier_shop_tab__reg_daily_s0_live s2 ON s1.seller_id = s2.sbs_vendor_id
-- -- WHERE
-- -- s1.shopee_entity = 'SPENN'
-- -- AND s1.seller_status = 1
-- -- AND s1.cb_option = 1
-- select distinct shop_id from regcbbi_general.cbwh_shop_history_v2
-- where 1=1
-- AND grass_date = current_date - INTERVAL '1' DAY
-- and warehouse_id = 'CNN'
-- and grass_region IN ('VN''TH')
-- -- and is_live_shop = 1
-- -- and fbs_tag =1
-- -- and pff_tag =0 -- pure whs
-- ),


cfs as (
select
grass_region,
date(from_unixtime(start_timestamp)) as grass_date,
promotion_id AS promotionid,
item_id AS itemid,
shop_id AS shopid,
start_timestamp AS start_time,
end_timestamp AS end_time,
1.00 * item_promotion_price / 100000 as cfs_price,
1.00 * item_rebate_amt / 100000 as rebate_price,
is_cb_shop, 
is_sbs
--round((1.00 * item_promotion_price / 100000), 2) cfs_price,
--round((1.00 * item_rebate_amt / 100000), 2) rebate_price
from
mp_promo.dim_flash_sale_sku__reg_s0_live
WHERE
grass_region IN ('ID', 'MY', 'PH', 'TH', 'VN', 'MX', 'SG')
AND grass_date = current_date - INTERVAL '1' DAY --latest snapshot
AND tz_type = 'local'
AND model_status_id = 1 --confirmed item
AND item_promotion_type_id = 3 --only flash sale
AND date(from_unixtime(start_timestamp)) >= current_date - interval '65' day
-- exclude nnwh-fbs and nnwh-pff 
AND shop_id NOT IN (
426379311,
445279067,
426377685,
445275287,
446091597,
446089250
) --lovito
),
cb_cbwh_3pf as (
SELECT
'CBWH - 3PF' as seller_origin,
ss.grass_region,
cast(
count(
distinct case
when cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO,
cast(
count(
distinct case
when cfs.rebate_price > 0
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot_wo_rebate,
cast(
count(
distinct case
when cfs.rebate_price > 0
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO_wo_rebate,
cast(
count(
distinct case
when round(cfs.cfs_price * 1.0000 / exchange_rate, 0) <= 0.3
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_LPP_slot,
cast(
count(
distinct case
when round(order_price_pp_usd, 1) <= 0.3
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_LPP_ADO,
cast(
count(
distinct case
when cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO,
cast(
count(
distinct case
when cfs.rebate_price > 0
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot_wo_rebate,
cast(
count(
distinct case
when cfs.rebate_price > 0
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO_wo_rebate,
cast(
count(
distinct case
when round(cfs.cfs_price * 1.0000 / exchange_rate, 0) <= 0.3
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_LPP_slot,
cast(
count(
distinct case
when round(order_price_pp_usd, 1) <= 0.3
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_LPP_ADO,
sum(
case
when a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then gmv_usd
else null
end
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADG,
sum(
case
when a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then gmv_usd
else null
end
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADG,
cast(
count(
distinct case
when cfs.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_slot,
cast(
count(
distinct case
when a.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_ADO,
cast(
count(
distinct case
when cfs.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('week', date_add('day', -1, current_date)) then itemid
else null
end
) as double
) / 7 as W1_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('week', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / 7 as W1_CFS_ADO,
sum(
case
when a.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then gmv_usd
else null
end
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_ADG,
sum(
case
when a.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('week', date_add('day', -1, current_date)) then gmv_usd
else null
end
) / 7 as W1_CFS_ADG
FROM
(
SELECT
distinct grass_region,
shop_id
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_date >= current_date - interval '1' day
AND tz_type = 'local'
AND is_cb_shop = 1
) ss
JOIN (
SELECT
DISTINCT region AS grass_region,
cast(shop_id as bigint) AS shop_id
FROM
regcb_bi.cbwh_3pf_shop_list
) w on cast (ss.shop_id as bigint) = w.shop_id
and ss.grass_region = w.grass_region
JOIN cfs on ss.shop_id = cfs.shopid
LEFT JOIN (
SELECT
grass_region,
if(
grass_region IN ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR'),
date(from_unixtime(create_timestamp)),
grass_date
) grass_date,
order_id,
order_fraction,
gmv_usd,
is_cb_shop,
shop_id,
item_id,
create_timestamp,
item_rebate_by_shopee_amt,
item_price_pp,
order_price_pp_usd
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
if(
grass_region IN ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR'),
date(from_unixtime(create_timestamp)),
grass_date
) between date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and date_add('day', -1, current_date)
AND tz_type = 'local'
AND grass_region != ''
AND is_placed = 1
AND (
is_bi_excluded = 0
OR is_bi_excluded IS NULL
)
AND item_promotion_source = 'flash_sale'
AND coalesce(flash_sale_type_id, 0) = 0
AND is_cb_shop = 1
AND fulfilment_source IN (
'FULFILLED_BY_LOCAL_SELLER'
)
) a on cfs.itemid = a.item_id
and a.create_timestamp between cfs.start_time
and cfs.end_time
LEFT JOIN rate r on cfs.grass_region = r.grass_region
GROUP BY
1,
2
),
cb_cbwh as (
SELECT
'CBWH' as seller_origin,
ss.grass_region,
cast(
count(
distinct case
when cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO,
cast(
count(
distinct case
when cfs.rebate_price > 0
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot_wo_rebate,
cast(
count(
distinct case
when cfs.rebate_price > 0
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO_wo_rebate,
cast(
count(
distinct case
when round(cfs.cfs_price * 1.0000 / exchange_rate, 0) <= 0.3
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_LPP_slot,
cast(
count(
distinct case
when round(order_price_pp_usd, 1) <= 0.3
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_LPP_ADO,
cast(
count(
distinct case
when cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO,
cast(
count(
distinct case
when cfs.rebate_price > 0
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot_wo_rebate,
cast(
count(
distinct case
when cfs.rebate_price > 0
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO_wo_rebate,
cast(
count(
distinct case
when round(cfs.cfs_price * 1.0000 / exchange_rate, 0) <= 0.3
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_LPP_slot,
cast(
count(
distinct case
when round(order_price_pp_usd, 1) <= 0.3
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_LPP_ADO,
sum(
case
when a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then gmv_usd
else null
end
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADG,
sum(
case
when a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then gmv_usd
else null
end
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADG,
cast(
count(
distinct case
when cfs.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_slot,
cast(
count(
distinct case
when a.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_ADO,
cast(
count(
distinct case
when cfs.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('week', date_add('day', -1, current_date)) then itemid
else null
end
) as double
) / 7 as W1_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('week', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / 7 as W1_CFS_ADO,
sum(
case
when a.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then gmv_usd
else null
end
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_ADG,
sum(
case
when a.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('week', date_add('day', -1, current_date)) then gmv_usd
else null
end
) / 7 as W1_CFS_ADG
FROM
(
SELECT
distinct a.grass_region,
a.shop_id
FROM
mp_user.dim_shop__reg_s0_live a
WHERE
a.grass_date >= current_date - interval '1' day
AND tz_type = 'local'
AND is_cb_shop = 1
) ss
JOIN (
SELECT
DISTINCT grass_region,
shop_id
FROM
regcbbi_general.cbwh_shop_history_v2
WHERE
grass_date >= current_date - INTERVAL '1' DAY
AND active_item_with_stock_cnt > 0
AND holiday_mode_on = False
AND fbs_tag = 1
AND fbs_status = 1
) w on cast (ss.shop_id as bigint) = w.shop_id
and ss.grass_region = w.grass_region
JOIN cfs on ss.shop_id = cfs.shopid
--hannah : LEFT JOIN 
JOIN (
SELECT
grass_region,
if(
grass_region IN ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR'),
date(from_unixtime(create_timestamp)),
grass_date
) grass_date,
order_id,
order_fraction,
gmv_usd,
is_cb_shop,
shop_id,
item_id,
create_timestamp,
item_rebate_by_shopee_amt,
item_price_pp,
order_price_pp_usd
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
if(
grass_region IN ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR'),
date(from_unixtime(create_timestamp)),
grass_date
) between date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and date_add('day', -1, current_date)
AND tz_type = 'local'
AND grass_region != ''
AND is_placed = 1
AND (
is_bi_excluded = 0
OR is_bi_excluded IS NULL
)
AND item_promotion_source = 'flash_sale'
AND coalesce(flash_sale_type_id, 0) = 0
AND is_cb_shop = 1
AND fulfilment_source IN (
'FULFILLED_BY_SHOPEE'
--,'FULFILLED_BY_LOCAL_SELLER'
)
) a on cfs.itemid = a.item_id
and a.create_timestamp between cfs.start_time
and cfs.end_time
LEFT JOIN rate r on cfs.grass_region = r.grass_region
GROUP BY
1,
2
),
CB as (
SELECT
'CB' as seller_origin,
ss.grass_region,
cast(
count(
distinct case
when cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO,
cast(
count(
distinct case
when cfs.rebate_price > 0
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot_wo_rebate,
cast(
count(
distinct case
when cfs.rebate_price > 0
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO_wo_rebate,
cast(
count(
distinct case
when round(cfs.cfs_price * 1.0000 / exchange_rate, 0) <= 0.3
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_LPP_slot,
cast(
count(
distinct case
when round(order_price_pp_usd, 1) <= 0.3
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_LPP_ADO,
cast(
count(
distinct case
when cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO,
cast(
count(
distinct case
when cfs.rebate_price > 0
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_slot_wo_rebate,
cast(
count(
distinct case
when cfs.rebate_price > 0
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADO_wo_rebate,
cast(
count(
distinct case
when round(cfs.cfs_price * 1.0000 / exchange_rate, 0) <= 0.3
and cfs.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('month', date_add('day', -1, current_date)) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_LPP_slot,
cast(
count(
distinct case
when round(order_price_pp_usd, 1) <= 0.3
and a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_LPP_ADO,
sum(
case
when a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then gmv_usd
else null
end
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADG,
sum(
case
when a.grass_date >= date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('month', date_add('day', -1, current_date)) then gmv_usd
else null
end
) / day_of_month(
date_add(
'day',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
) as M1_CFS_ADG,
cast(
count(
distinct case
when cfs.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_slot,
cast(
count(
distinct case
when a.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_ADO,
cast(
count(
distinct case
when cfs.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and cfs.grass_date < date_trunc('week', date_add('day', -1, current_date)) then itemid
else null
end
) as double
) / 7 as W1_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('week', date_add('day', -1, current_date)) then order_id
else null
end
) as double
) / 7 as W1_CFS_ADO,
sum(
case
when a.grass_date between date_trunc('week', date_add('day', -1, current_date))
and date_add('day', -1, current_date) then gmv_usd
else null
end
) / day_of_week(date_add('day', -1, current_date)) as WTD_CFS_ADG,
sum(
case
when a.grass_date >= date_add(
'week',
-1,
date_trunc('week', date_add('day', -1, current_date))
)
and a.grass_date < date_trunc('week', date_add('day', -1, current_date)) then gmv_usd
else null
end
) / 7 as W1_CFS_ADG
FROM
(
SELECT
distinct grass_region,
shop_id
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_date >= current_date - interval '1' day
AND tz_type = 'local'
AND is_cb_shop = 1
) ss
JOIN cfs on ss.shop_id = cfs.shopid
LEFT JOIN (
SELECT
grass_region,
if(
grass_region IN ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR'),
date(from_unixtime(create_timestamp)),
grass_date
) grass_date,
order_id,
order_fraction,
gmv_usd,
is_cb_shop,
shop_id,
item_id,
create_timestamp,
item_rebate_by_shopee_amt,
item_price_pp,
order_price_pp_usd
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
if(
grass_region IN ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR'),
date(from_unixtime(create_timestamp)),
grass_date
) between date_add(
'month',
-1,
date_trunc('month', date_add('day', -1, current_date))
)
and date_add('day', -1, current_date)
AND tz_type = 'local'
AND grass_region != ''
AND is_placed = 1
AND (
is_bi_excluded = 0
OR is_bi_excluded IS NULL
)
AND item_promotion_source = 'flash_sale'
AND coalesce(flash_sale_type_id, 0) = 0
) a on cfs.itemid = a.item_id
and a.create_timestamp between cfs.start_time
and cfs.end_time
LEFT JOIN rate r on cfs.grass_region = r.grass_region
GROUP BY
1,
2
),
country as (
SELECT
'Country' as seller_origin,
ss.grass_region,
cast(
count(
distinct case
when cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot,
cast(
count(
distinct case
when a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO,
cast(
count(
distinct case
when cfs.rebate_price > 0
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_slot_wo_rebate,
cast(
count(
distinct case
when cfs.rebate_price > 0
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO_w_rebate,
cast(
count(
distinct case
when (
cfs.rebate_price = 0
or cfs.rebate_price is NULL
)
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_ADO_wo_rebate,
cast(
count(
distinct case
when round(cfs.cfs_price * 1.0000 / exchange_rate, 0) <= 0.3
and cfs.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and cfs.grass_date <= date_add('day', -1, current_date) then (cfs.itemid, cfs.start_time)
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_LPP_slot,
cast(
count(
distinct case
when round(order_price_pp_usd, 1) <= 0.3
and a.grass_date >= date_trunc('month', date_add('day', -1, current_date))
and a.grass_date <= date_add('day', -1, current_date) then order_id
else null
end
) as double
) / day_of_month(date_add('day', -1, current_date)) as MTD_CFS_LPP_ADO,
cast(
count(
distinct case
when cfs.grass_d