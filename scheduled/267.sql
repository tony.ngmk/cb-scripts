insert into nexus.test_e81c4f44ce9f080b1191e9f7720e66e756a6f57ed252b50c417b15fcd2768cab_b4dc06e1527b0730eee093066444fcab with seller as (
select 
shop_id as shopid,
user_name as shop_name
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
)
),
return_info1 as (
select
date(split(e.return_created_datetime,' ')[1]) as return_request_date,
date(split(e.return_seller_accept_due_datetime,' ')[1]) as return_seller_accept_due_datetime,
e.return_reason,
e.return_status,
e.return_logistics_status,
e.is_refund_amount_adjustable,
date(split(e.return_refund_paid_datetime,' ')[1]) as return_refund_paid_datetime,
e.return_id,
e.order_id,
e.dispute_reason,
e.dispute_reason_text,
sum(e.refund_amount) as refund_amount,
sum(e.refund_amount_usd) as refund_amount_usd,
sum(e.return_actual_shipping_fee) as return_actual_shipping_fee,
sum(e.return_actual_shipping_fee_usd) as return_actual_shipping_fee_usd,
sum(e.order_price_pp)*sum(e.item_amount) as subtotal_per_return
from
mp_order.dwd_return_item_all_ent_df__reg_s0_live as e
join
seller as s
on
e.shop_id = s.shopid
and e.grass_date >= date_add('day',-90,current_date)
and date(split(e.order_create_datetime,' ')[1]) >= date_add('day',-90,current_date)
and e.tz_type = 'local'
where
e.grass_region <> ''
group by
1,2,3,4,5,6,7,8,9,10,11
),
return_info2 as (
select
r.return_id,
r.orderid,
r.return_info.text_reason as return_reason_remark,
r.return_info.seller_compensation.compensation_amount as seller_compensation_amount,
r.return_info.seller_compensation.maximum_compensation_amount as seller_maximum_compensation_amount,
r.return_info.seller_compensation_status,
from_unixtime(r.return_info.seller_due_Date) as seller_respond_to_rr_request_date,
from_unixtime(r.return_info.seller_reply_due_date) as seller_provide_evidence_date,
from_unixtime(r.return_info.return_ship_due_date) as buyer_ship_out_date,
from_unixtime(r.return_info.return_seller_due_date) as seller_confirm_items_received_date,
from_unixtime(r.return_info.seller_compensation.return_seller_compensation_due_date) as compensation_due_date
from --https://confluence.shopee.io/display/SPCT/%5BAdmin%5D+Return+List+and+RDP+Migration
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live as r
join
seller as s
on
r.shopid = s.shopid --return request는 항상 오더 생성 이후에 발생
and date(from_unixtime(r.ctime)) >= date_add('day',-91,current_date)
where
r.grass_region <> ''
group by
1,2,3,4,5,6,7,8,9,10,11
),
return_orders as (
select
return_request_date,
return_seller_accept_due_datetime,
return_reason,
return_status,
return_logistics_status,
is_refund_amount_adjustable,
return_reason_remark,
refund_amount,
refund_amount_usd,
return_refund_paid_datetime,
e.return_id,
e.order_id,
seller_compensation_amount,
seller_maximum_compensation_amount,
return_actual_shipping_fee,
return_actual_shipping_fee_usd,
dispute_reason,
dispute_reason_text,
subtotal_per_return,
seller_compensation_status,
seller_respond_to_rr_request_date,
seller_provide_evidence_date,
buyer_ship_out_date,
seller_confirm_items_received_date,
compensation_due_date
from
return_info1 as e
join
return_info2 as r
on
r.return_id = e.return_id
and r.orderid = e.order_id
),
sls_info as (
select
f.order_id,
f.consignment_no as sls_tracking_number,
f.lm_tracking_no
from
mp_ofm.dwd_forder_all_ent_di__reg_s0_live as f
join
seller as s
on
f.shop_id = s.shopid
and date(split(f.order_create_datetime,' ')[1]) >= date_add('day',-90,current_date)
and f.tz_type = 'local'
where
f.grass_date >= date_add('day',-90,current_date)
group by
1,2,3
),
order_info as (
select
date(split(o.create_datetime,' ')[1]) as grass_date,
o.grass_region,
o.order_sn,
o.order_id,
s.shop_name,
s.shopid,
o.order_be_status,
o.logistics_status,
sum(o.cogs) as cogs
from
mp_order.dwd_order_item_all_ent_df__reg_s0_live as o
join
seller as s
on 
o.shop_id = s.shopid
and date(split(o.create_datetime,' ')[1]) >= date_add('day',-90,current_date)
and o.grass_date >= date_add('day',-90,current_date)
and is_bi_excluded = 0
and o.tz_type = 'local'
where
o.grass_region <> ''
group by
1,2,3,4,5,6,7,8
),
exchange_rate as (
select
grass_region,
exchange_rate,
grass_date
from
mp_order.dim_exchange_rate__reg_s0_live
where
grass_date >= date_add('day',-90,current_date)
)
select
r.return_request_date,
o.grass_date,
o.grass_region,
o.order_sn,
o.order_id,
l.sls_tracking_number,
l.lm_tracking_no,
o.cogs as cogs_per_orderid,
r.subtotal_per_return/e.exchange_rate as subtotal_usd_per_return,


r.return_id,
o.order_be_status,
o.logistics_status,
r.return_status,
r.return_logistics_status,
r.return_seller_accept_due_datetime,
r.return_reason,
if(r.is_refund_amount_adjustable=1,'Y','N') as is_refund_amount_adjustable,
r.return_reason_remark,
r.dispute_reason,
r.dispute_reason_text,
r.return_actual_shipping_fee,
r.return_actual_shipping_fee_usd,
r.refund_amount,
r.refund_amount_usd,
r.return_refund_paid_datetime as payout_date,


case when r.seller_compensation_amount>0 then seller_compensation_amount/cast(100000 as double) else seller_compensation_amount end as seller_compensation_amount,
case when r.seller_maximum_compensation_amount>0 then seller_maximum_compensation_amount/cast(100000 as double) else seller_maximum_compensation_amount end as seller_maximum_compensation_amount,


case
when r.seller_compensation_status=0 then 'COMPENSATION_NOT_APPLICABLE' 
when r.seller_compensation_status=1 then 'COMPENSATION_INITIAL_STAGE'
when r.seller_compensation_status=2 then 'COMPENSATION_PENDING_REQUEST'
when r.seller_compensation_status=3 then 'COMPENSATION_NOT_REQUIRED'
when r.seller_compensation_status=4 then 'COMPENSATION_REQUESTED'
when r.seller_compensation_status=5 then 'COMPENSATION_APPROVED'
when r.seller_compensation_status=6 then 'COMPENSATION_REJECTED'
when r.seller_compensation_status=7 then 'COMPENSATION_CANCELLED'
when r.seller_compensation_status=8 then 'COMPENSATION_NOT_ELIGIBLE' 
end as seller_compensation_status,
concat('https://cs.uat.shopee.',lower(o.grass_region),'/dispute/detail/',cast(r.return_id as varchar)) as seller_disput_link,


r.seller_respond_to_rr_request_date,
r.seller_provide_evidence_date,
r.buyer_ship_out_date,
r.seller_confirm_items_received_date,
r.compensation_due_date
from
order_info as o
join
return_orders as r
on
o.order_id = r.order_id
left join
sls_info as l
on
o.order_id = l.order_id
join
exchange_rate as e
on
o.grass_region = e.grass_region
and o.grass_date = e.grass_date
order by
2 desc,4,9