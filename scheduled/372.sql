insert into nexus.test_8077ab13450ba5a8f45874d6c2f3916076b0ebffd71f16fe71cc9015e6c960a3_335d248125550132e3ea83c92163ecc8 WITH
t1 AS (
SELECT 
cfs.grass_region country
, cfs.shopid
, mst_shopid
, mst_country
, cfs.itemid
, cfs.modelid
, user_item_limit
, flash_date
, st
, et
, promotion_stock stock
, cfs_price
FROM (SELECT 
grass_region,
promotion_id,
DATE(CASE WHEN grass_region = 'SG' THEN FROM_UNIXTIME(start_time,'Asia/Singapore')
WHEN grass_region = 'MY' THEN FROM_UNIXTIME(start_time,'Asia/Kuala_Lumpur') 
WHEN grass_region = 'PH' THEN FROM_UNIXTIME(start_time,'Asia/Manila')
WHEN grass_region = 'TH' THEN FROM_UNIXTIME(start_time,'Asia/Bangkok')
WHEN grass_region = 'ID' THEN FROM_UNIXTIME(start_time,'Asia/Jakarta')
WHEN grass_region = 'VN' THEN FROM_UNIXTIME(start_time,'Asia/Ho_Chi_Minh')
WHEN grass_region = 'IN' THEN FROM_UNIXTIME(start_time,'Asia/Kolkata')
WHEN grass_region = 'BR' THEN FROM_UNIXTIME(start_time,'America/Sao_Paulo')
WHEN grass_region = 'CL' THEN FROM_UNIXTIME(start_time,'America/Santiago')
WHEN grass_region = 'CO' THEN FROM_UNIXTIME(start_time,'America/Bogota')
WHEN grass_region = 'MX' THEN FROM_UNIXTIME(start_time,'America/Mexico_City')
WHEN grass_region = 'PL' THEN FROM_UNIXTIME(start_time,'Europe/Warsaw')
WHEN grass_region = 'ES' THEN FROM_UNIXTIME(start_time,'Europe/Madrid')
WHEN grass_region = 'FR' THEN FROM_UNIXTIME(start_time,'Europe/Paris')

ELSE FROM_UNIXTIME(start_time) END) AS flash_date,

(CASE WHEN grass_region = 'SG' THEN FROM_UNIXTIME(start_time,'Asia/Singapore')
WHEN grass_region = 'MY' THEN FROM_UNIXTIME(start_time,'Asia/Kuala_Lumpur') 
WHEN grass_region = 'PH' THEN FROM_UNIXTIME(start_time,'Asia/Manila')
WHEN grass_region = 'TH' THEN FROM_UNIXTIME(start_time,'Asia/Bangkok')
WHEN grass_region = 'ID' THEN FROM_UNIXTIME(start_time,'Asia/Jakarta')
WHEN grass_region = 'VN' THEN FROM_UNIXTIME(start_time,'Asia/Ho_Chi_Minh')
WHEN grass_region = 'IN' THEN FROM_UNIXTIME(start_time,'Asia/Kolkata')
WHEN grass_region = 'BR' THEN FROM_UNIXTIME(start_time,'America/Sao_Paulo')
WHEN grass_region = 'CL' THEN FROM_UNIXTIME(start_time,'America/Santiago')
WHEN grass_region = 'CO' THEN FROM_UNIXTIME(start_time,'America/Bogota')
WHEN grass_region = 'MX' THEN FROM_UNIXTIME(start_time,'America/Mexico_City')
WHEN grass_region = 'PL' THEN FROM_UNIXTIME(start_time,'Europe/Warsaw')
WHEN grass_region = 'ES' THEN FROM_UNIXTIME(start_time,'Europe/Madrid')
WHEN grass_region = 'FR' THEN FROM_UNIXTIME(start_time,'Europe/Paris')
ELSE FROM_UNIXTIME(start_time) END) AS start_time,

(CASE WHEN grass_region = 'SG' THEN FROM_UNIXTIME(end_time,'Asia/Singapore')
WHEN grass_region = 'MY' THEN FROM_UNIXTIME(end_time,'Asia/Kuala_Lumpur') 
WHEN grass_region = 'PH' THEN FROM_UNIXTIME(end_time,'Asia/Manila')
WHEN grass_region = 'TH' THEN FROM_UNIXTIME(end_time,'Asia/Bangkok')
WHEN grass_region = 'ID' THEN FROM_UNIXTIME(end_time,'Asia/Jakarta')
WHEN grass_region = 'VN' THEN FROM_UNIXTIME(end_time,'Asia/Ho_Chi_Minh')
WHEN grass_region = 'IN' THEN FROM_UNIXTIME(end_time,'Asia/Kolkata')
WHEN grass_region = 'BR' THEN FROM_UNIXTIME(end_time,'America/Sao_Paulo')
WHEN grass_region = 'CL' THEN FROM_UNIXTIME(end_time,'America/Santiago')
WHEN grass_region = 'CO' THEN FROM_UNIXTIME(end_time,'America/Bogota')
WHEN grass_region = 'MX' THEN FROM_UNIXTIME(end_time,'America/Mexico_City')
WHEN grass_region = 'PL' THEN FROM_UNIXTIME(end_time,'Europe/Warsaw')
WHEN grass_region = 'ES' THEN FROM_UNIXTIME(end_time,'Europe/Madrid')
WHEN grass_region = 'FR' THEN FROM_UNIXTIME(end_time,'Europe/Paris')
ELSE FROM_UNIXTIME(end_time) END) AS end_time,
shop_id shopid,
item_id itemid,
model_id modelid,
CASE WHEN promotion_type = 1 THEN 'PRODUCT_PROMOTION'
WHEN promotion_type = 2 THEN 'SELLER_DISCOUNT' 
WHEN promotion_type = 3 THEN 'FLASH_SALE' 
WHEN promotion_type = 4 THEN 'BRAND_SALE' 
WHEN promotion_type = 5 THEN 'SHOP_FLASH_SALE' 
ELSE 'UNKNOWN' END AS promotion_type,
start_time st,
end_time et,
promotion_price / 100000.00 AS cfs_price,
promotion_stock AS promotion_stock,
user_item_limit AS user_item_limit
FROM marketplace.shopee_promotion_item_db__promotion_model_tab__reg_daily_s0_live
WHERE grass_region <> ''
AND promotion_type IN (3) --cfs
AND start_time >= TO_UNIXTIME(current_date - INTERVAL '92' DAY)
AND model_status = 1


) cfs
JOIN (SELECT 
DISTINCT b.affi_shopid
, b.mst_shopid
, b.country AS affi_country
, a.country AS mst_country
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a 
JOIN (SELECT DISTINCT * FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE grass_region <> '') b 
on b.mst_shopid = a.shopid 
WHERE a.cb_option = 0 AND b.sip_shop_status <> 3
) sip
ON cfs.shopid = sip.affi_shopid 

) 


, sip AS (
SELECT DISTINCT
grass_region
, affi_shopid
, mst_shopid
, b.country mst_country
FROM
((
SELECT DISTINCT
grass_region
, affi_shopid
, mst_shopid
, sip_shop_status
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (grass_region <> '' AND sip_shop_status <> 3)
) a
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON (a.mst_shopid = b.shopid))
WHERE (b.cb_option = 0 AND sip_shop_status <> 3)
) 
, map_sku AS (
SELECT DISTINCT
a.*
, mst_country
FROM
((
SELECT DISTINCT
affi_shopid
, affi_itemid
, affi_country
, mst_itemid
, mst_shopid
, affi_real_weight/100/10*10 AS sip_weight
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE (((affi_country NOT IN ('MY', 'SG', 'PH', 'TH', 'VN')) AND (schema <> '')) 
)
) a
INNER JOIN sip ON (a.affi_shopid = sip.affi_shopid))
WHERE mst_country NOT IN ('BR', 'SG') --not CNSIP BR/KRSIP

-- SELECT DISTINCT 
-- shop_id AS affi_shopid
-- , item_id AS affi_itemid
-- , grass_region AS affi_country
-- , primary_item_id AS mst_itemid
-- , primary_shop_id AS mst_shopid
-- , primary_seller_region AS mst_country
-- , item_real_weight/100000.00 AS sip_weight
-- FROM mp_item.ads_sip_affi_item_profile__reg_s0_live
-- WHERE grass_date = current_date - INTERVAL '2' DAY
-- AND tz_type = 'local'
-- AND primary_shop_type = 0 -- local sip
-- AND grass_region IN ('MY', 'SG', 'PH', 'TH', 'VN')
) 
, o AS (
SELECT 
-- DISTINCT
CAST(create_datetime AS timestamp) create_datetime
, create_timestamp 
, DATE(CAST(create_datetime AS TIMESTAMP)) grass_date
, order_id
, order_sn
, shop_id
, item_id
, model_id
, gmv
, gmv_usd
, buyer_paid_shipping_fee
, item_amount
, actual_shipping_fee
, is_cb_shop
, grass_region
, item_rebate_by_shopee_amt
, is_flash_sale
, flash_sale_type_id
, order_fraction
, IF((("floor"(((item_weight_pp * 100000.00 / 100) / DECIMAL '10.0')) * 10) > 30000), 30000, ("floor"(((item_weight_pp* 100000.00 / 100) / DECIMAL '10.0')) * 10)) weight
, item_price_pp item_price
, cogs
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (
(CAST(create_datetime AS timestamp) BETWEEN (current_date - INTERVAL '70' DAY) AND (current_date - INTERVAL '1' DAY)) 
AND (tz_type = 'local')
AND (item_amount > 0)
AND grass_region NOT IN ('PL', 'ES', 'FR') 
)
)


, tgt1 AS (
SELECT 
DISTINCT
create_datetime
, grass_date
, grass_region
, order_id
, shop_id
, item_id
, CASE WHEN (sip_weight IS NOT NULL AND sip_weight > 0) THEN sip_weight 
ELSE weight END AS weight
, t1.mst_country 
, mst_itemid
FROM
t1
INNER JOIN (
SELECT *
FROM
o
WHERE ((((is_cb_shop = 1)) AND (is_flash_sale = 1)) AND (flash_sale_type_id <> 2) AND grass_region NOT IN ('MY', 'SG', 'PH', 'TH', 'VN')) 
) a 
ON (t1.itemid = a.item_id) AND (a.create_timestamp BETWEEN t1.st AND t1.et)
AND t1.modelid = a.model_id AND a.item_amount <= t1.user_item_limit
JOIN map_sku ON t1.itemid = map_sku.affi_itemid
) -- cfs a order


, tgt AS (SELECT DISTINCT


a.create_datetime
, a.grass_date
, a.grass_region
, a.order_id
, a.shop_id
, a.item_id
, mst_itemid
, "sum"(a.order_fraction) order_fraction
, "sum"(a.cogs) cogs
, SUM(a.item_amount*a.item_price) AS selling_price 
, "sum"(a.item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
, "sum"(a.item_amount) item_amount
, "sum"(a.gmv) gmv
, sum(a.gmv_usd) gmv_usd
, SUM(a.item_amount*CAST(sum_hpfn AS DOUBLE)) hpfn
FROM
tgt1
INNER JOIN (
SELECT *
FROM
o
WHERE is_cb_shop = 1 
AND grass_region NOT IN ('MY', 'SG', 'PH', 'TH', 'VN') 
) a
ON tgt1.item_id = a.item_id AND tgt1.order_id = a.order_id 
LEFT JOIN (
SELECT DISTINCT a.*
FROM
(regcbbi_others.local_sip_price_config a
INNER JOIN (
SELECT "max"(ingestion_timestamp) ingestion_timestamp
FROM
regcbbi_others.local_sip_price_config
) b ON (a.ingestion_timestamp = b.ingestion_timestamp))
) b 
ON tgt1.grass_region = b.affi_country
AND tgt1.mst_country = b.mst_country
AND tgt1.weight = CAST(b.weight AS DOUBLE)
GROUP BY 1,2,3,4,5,6,7 
)


, tgt2 AS (
SELECT DISTINCT
tgt.grass_date
, tgt.grass_region
, tgt.shop_id
, m.local_shopid AS mst_shopid
, UPPER(m.local_country) AS mst_country
, tgt.item_id
-- , t1.cfs_price
, CAST(("sum"(tgt.selling_price) / "sum"(tgt.item_amount)) AS double) ASP
, "sum"(tgt.item_amount) a_amount
, "sum"(tgt.order_fraction) a_order_fraction
, "sum"(tgt.gmv) a_gmv
, "sum"(tgt.item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
, "sum"(p.cogs) p_settlement
, "sum"(hpfn) a_hpfn
, sum(tgt.gmv_usd) gmv_usd
FROM tgt
INNER JOIN (
SELECT DISTINCT
local_country
, oversea_country
, local_orderid
, local_shopid
, oversea_orderid
, oversea_shopid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE (((schema <> '')) AND (local_orderid <> 0) 
AND UPPER(oversea_country) NOT IN ('MY', 'SG', 'PH', 'TH', 'VN'))
) m ON (tgt.order_id = m.oversea_orderid)
-- INNER JOIN map_sku ON ((tgt.shop_id = map_sku.affi_shopid) AND (tgt.item_id = map_sku.affi_itemid))
INNER JOIN (
SELECT DISTINCT
create_datetime
, order_id
, item_id
, "sum"(gmv) gmv
, "sum"(buyer_paid_shipping_fee) buyer_paid_shipping_fee
, "sum"(cogs) cogs
FROM
o
WHERE ((is_cb_shop = 0) AND (grass_region IN ('ID', 'MY','VN', 'TW', 'TH')))
GROUP BY 1, 2, 3
) p ON ((m.local_orderid = p.order_id) AND (p.item_id = tgt.mst_itemid))
GROUP BY 1, 2, 3, 4, 5, 6
)


SELECT DISTINCT grass_date
, grass_region
, shop_id
, mst_shopid
, mst_country
, item_id
, ASP
, a_amount
, a_order_fraction
, a_gmv
, item_rebate_by_shopee_amt
, p_settlement
, a_hpfn
, gmv_usd
FROM tgt2