insert into nexus.test_93130bd8eb1ac3a5d48e6bb76b5c7803ce1a4691b62966ba9bc3cd9946512adc_06a623bd6513e5a44d3b1dc3d12ff1e1 WITH I AS (
SELECT shop_id, date_trunc('week', grass_date) week, po_pctg
FROM
(
SELECT shop_id, grass_date
, count(distinct item_id) total_live_listing
, sum(is_pre_order) total_po_listing
, 1.0000*sum(is_pre_order)/count(distinct item_id) as po_pctg
FROM mp_item.dim_item__reg_s0_live
WHERE is_cb_shop = 1
AND seller_status = 1
AND shop_status = 1
AND is_holiday_mode = 0
AND status = 1
AND stock > 0
AND tz_type = 'local'
AND grass_region != 'FR'
AND grass_date IN (
date_add('week', -8, date_trunc('week', CURRENT_DATE)),
date_add('week', -7, date_trunc('week', CURRENT_DATE)),
date_add('week', -6, date_trunc('week', CURRENT_DATE)),
date_add('week', -5, date_trunc('week', CURRENT_DATE)),
date_add('week', -4, date_trunc('week', CURRENT_DATE)),
date_add('week', -3, date_trunc('week', CURRENT_DATE)),
date_add('week', -2, date_trunc('week', CURRENT_DATE)),
date_add('week', -1, date_trunc('week', CURRENT_DATE)),
date_trunc('week', CURRENT_DATE)
)
GROUP BY 1,2
)
)
, sip AS (
SELECT t1.country as P_country
, t2.country as A_country
, affi_shopid AS shopid
, t1.cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1 
JOIN
(SELECT distinct affi_shopid, affi_userid, affi_username, mst_shopid, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE grass_region != '' AND sip_shop_status != 3 ) t2 on t1.shopid=t2.mst_shopid
WHERE t1.country != 'FR'
AND t2.country != 'FR'
)
, seller_index AS (
SELECT CAST(si.child_shopid as BIGINT) as shop_id
, si.gp_account_name
, si.gp_account_id
, si.gp_account_owner
, si.child_account_owner
, si.gp_account_owner_userrole_name
, si.child_account_owner_userrole_name
, CASE WHEN sip.shopid is not NULL THEN 1 ELSE 0 END as is_sip
, CASE WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
WHEN sip.shopid IS NOT NULL AND sip.cb_option = 0 THEN 'Local SIP'
WHEN sip.shopid IS NOT NULL AND sip.p_country IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
WHEN sip.shopid IS NOT NULL AND si.gp_account_billing_country = 'Korea' AND sip.p_country IN ('SG') THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.shopid IS NOT NULL THEN 'CB SIP'
when si.gp_account_billing_country = 'Korea' then 'KRCB'
when si.gp_account_billing_country = 'Japan' then 'JPCB'
when si.gp_account_billing_country is NULL or si.gp_account_billing_country != 'China' then 'Others'
when si.child_account_owner_userrole_name like '%HKCB%' then 'HKCB'
when si.child_account_owner_userrole_name like '%BD_%' then 'CNCB BD'
when si.child_account_owner_userrole_name like '%UST_%' then 'CNCB UST'
when si.child_account_owner_userrole_name like '%ST_%' THEN 'CNCB ST'
when si.child_account_owner_userrole_name like '%MT_%' THEN 'CNCB MT'
when si.child_account_owner_userrole_name like '%LT%' THEN 'CNCB LT'
WHEN si.gp_account_billing_country = 'China' THEN COALESCE(CONCAT('CNCB ', si.cb_group), 'CNCB Others')
else 'CNCB Others' END as seller_type
, gp_account_billing_country
FROM regbd_sf.cb__seller_index_tab AS si 
LEFT JOIN sip ON CAST(si.child_shopid AS BIGINT) = sip.shopid
LEFT JOIN (
SELECT DISTINCT shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date BETWEEN DATE_ADD('day', -14, current_date) AND DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = CAST(si.child_shopid as BIGINT)
WHERE si.grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
)


----------------------PO---------------------
SELECT week, 'Total' seller_type, avg(po_pctg) avg_po_pctg
FROM I
GROUP BY 1,2


UNION ALL


SELECT week, cb.seller_type
, avg(po_pctg) avg_po_pctg
FROM I
LEFT JOIN sip on I.shop_id = sip.shopid
LEFT JOIN seller_index AS cb on cb.shop_id = I.shop_id
GROUP BY 1,2