WITH sip AS
(
SELECT DISTINCT affi_shopid ,
mst_shopid ,
t1.country mst_country ,
t2.country affi_country ,
t1.cb_option
FROM (marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN
(
SELECT DISTINCT affi_shopid ,
affi_username ,
mst_shopid ,
country
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (
sip_shop_status <> 3)
AND grass_region IN ('PL',
'ES',
'FR') ) t2
ON (
t1.shopid = t2.mst_shopid))
WHERE (
t2.country IN ('PL',
'ES',
'FR') )
)SELECT DISTINCT CURRENT_DATE update_time ,
affi_country ,
"Count"(DISTINCT affi_itemid) sku ,
"Sum"(l30d_ado) l30d_ado
FROM (
SELECT DISTINCT affi_shopid ,
mst_shopid ,
itemid affi_itemid ,
mst_itemid ,
NAME ,
status ,
l30d_ado ,
keyword ,
affi_country
FROM (
SELECT DISTINCT affi_shopid ,
mst_shopid ,
mst_itemid ,
i.itemid ,
NAME ,
status ,
map.affi_country , (
CASE
WHEN (
k.keyword IS NOT NULL) THEN k.keyword
ELSE 'N'
END) keyword ,
l30d_ado
FROM (((sip
INNER JOIN
(
SELECT DISTINCT item_id itemid ,
shop_id shopid ,
Replace(Lower(NAME),'_',' ') NAME ,
status
FROM mp_item.dim_item__reg_s0_live
WHERE ((
NOT (
status IN (0,
4,
5,8)))
AND (
grass_region IN('PL',
'ES',
'FR') ))
AND is_cb_shop=1
AND grass_date=CURRENT_DATE-interval'1'day
AND seller_status=1
AND shop_status=1
AND is_holiday_mode=0 ) i
ON (
i.shopid = sip.affi_shopid))
INNER JOIN
(
SELECT DISTINCT affi_itemid ,
mst_itemid ,
affi_country
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE (
affi_country IN ('PL',
'ES',
'FR') ) ) map
ON (
map.affi_itemid = i.itemid))
INNER JOIN
(
SELECT DISTINCT keyword ,
country
FROM regcbbi_others.prohibited_keyword_tmp_new_market_s0 ) k
ON regexp_like(NAME , "concat"('\b', cast(keyword AS varchar), '\b'))
AND k.country = map.affi_country
LEFT JOIN
(
SELECT DISTINCT item_id ,
("sum"(order_fraction) / decimal '30.00') l30d_ado
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (((
is_cb_shop = 1)
AND (
grass_region IN ('MY',
'BR')))
AND (
"date"("split"(create_datetime, ' ')[1]) BETWEEN (CURRENT_DATE - interval '30' day) AND (
CURRENT_DATE - interval '1' day)))
AND grass_date >= (CURRENT_DATE - interval '30' day)
GROUP BY 1 ) o
ON (
o.item_id = map.mst_itemid)) )
-- WHERE (keyword <> 'N')
)
GROUP BY 1,
2