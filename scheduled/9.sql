WITH sf_raw
AS (SELECT a.grass_region,
a.grass_date,
a.shopid,
a.orderid,
a.rule_name,
CASE
WHEN ( ( grass_region = 'SG'
AND CAST(rule_id AS INT) IN ( 12426 ) )
OR ( grass_region = 'MY'
AND rule_name LIKE '%FSS CCB%' )
OR ( grass_region NOT IN ( 'SG', 'MY', 'PH' )
AND ( rule_name LIKE '%FSC%'
OR rule_name LIKE '%535%'
OR rule_name LIKE '%321%'
OR rule_name LIKE '%CCB and FSS%' ) )
--or (sip.shopid is not null and sip.grass_region = 'SG')
) THEN 1
ELSE 0
END AS if_FSC,
CASE
WHEN ( ( grass_region = 'SG'
AND CAST(rule_id AS INT) IN ( 12429 ) )
OR ( grass_region = 'MY'
AND ( rule_name LIKE '%FSS%'
OR rule_name LIKE '%Dday%' )
AND rule_name NOT LIKE '%CCB%' )
OR ( grass_region = 'PH'
AND ( rule_name LIKE '%FSS%'
OR rule_name LIKE '%PSP%' )
AND rule_name NOT LIKE '%CCB%' )
OR ( grass_region NOT IN ( 'SG', 'MY', 'PH' )
AND ( rule_name LIKE '%FSS%'
OR Lower(rule_name) LIKE
'%cn exclusivity%'
OR Lower(rule_name) LIKE
'%free shipping campaign%' )
AND rule_name NOT LIKE '%CCB%' ) ) THEN 1
ELSE 0
END AS if_FSS,
CASE
WHEN ( ( grass_region = 'SG'
AND CAST(rule_id AS INT) IN ( 583, 721, 1696, 10237,
16151, 16157 ) )
OR ( grass_region = 'MY'
AND rule_name LIKE '%CCB%'
AND rule_name NOT LIKE '%FSS%' )
OR ( grass_region = 'PH'
AND rule_name LIKE '%CCB%'
AND rule_name NOT LIKE '%FSS%' )
OR ( grass_region NOT IN ( 'SG', 'MY', 'PH' )
AND ( rule_name LIKE '%CCB%'
OR rule_name LIKE '%CBXTRA%' )
AND rule_name NOT LIKE '%FSS%' ) ) THEN 1
ELSE 0
END AS if_CCB,
CASE
WHEN ( rule_name NOT LIKE '%FSS%'
AND Lower(rule_name) NOT LIKE '%cn exclusivity%'
AND Lower(rule_name) NOT LIKE
'%free shipping campaign%'
AND rule_name NOT LIKE '%PSP%'
AND rule_name NOT LIKE '%Dday%'
AND rule_name NOT LIKE '%CCB and FSS%'
AND rule_name NOT LIKE '%CCB%'
AND rule_name NOT LIKE '%CBXTRA%'
AND rule_name NOT LIKE '%FSC%'
AND rule_name NOT LIKE '%535%'
AND rule_name NOT LIKE '%321%'
AND ( grass_region = 'SG'
AND CAST(rule_id AS INT) NOT IN (
12426, 12429, 583, 721,
1696, 10237, 16151, 16157 ) ) )
--and (sip.shopid is null or sip.grass_region != 'SG')
THEN 1
ELSE 0
END AS if_others
FROM (SELECT DISTINCT o.grass_region,
o.grass_date,
o.shopid,
o.orderid,
r1.rule_name,
r1.rule_id
--, sum(fee_amount) service_fee 
FROM ((SELECT grass_region,
DATE(From_unixtime(create_timestamp))
AS
grass_date
,
shop_id
AS shopid,
order_id
AS
orderid,
Json_extract_scalar(service_fee_info_list,
'$[0].rule_id') AS
rule_id
--, sum(cast(json_extract_scalar(service_fee_info_list,'$[0].fee_amt') as double)) as fee_amount 
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE tz_type = 'local'
AND is_cb_shop = 1
AND grass_date >= Date_add('day', -90,
CURRENT_DATE)
AND DATE(From_unixtime(create_timestamp))
BETWEEN
Date_add(
'month', -1,
Date_trunc('month', CURRENT_DATE)) AND
Date_add('day', -1, CURRENT_DATE))
UNION ALL
(SELECT grass_region,
DATE(From_unixtime(create_timestamp))
AS
grass_date
,
shop_id
AS
shopid,
order_id
AS
orderid,
Json_extract_scalar(service_fee_info_list,
'$[1].rule_id') AS
rule_id
--, sum(cast(json_extract_scalar(service_fee_info_list,'$[1].fee_amt') as double)) as fee_amount 
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE tz_type = 'local'
AND is_cb_shop = 1
AND grass_date >= Date_add('day', -90,
CURRENT_DATE)
AND DATE(From_unixtime(create_timestamp))
BETWEEN
Date_add(
'month', -1,
Date_trunc('month', CURRENT_DATE)) AND
Date_add('day', -1, CURRENT_DATE))
UNION ALL
(SELECT grass_region,
DATE(From_unixtime(create_timestamp))
AS
grass_date
,
shop_id
AS
shopid,
order_id
AS
orderid,
Json_extract_scalar(service_fee_info_list,
'$[2].rule_id') AS
rule_id
--, sum(cast(json_extract_scalar(service_fee_info_list,'$[2].fee_amt') as double)) as fee_amount 
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE tz_type = 'local'
AND is_cb_shop = 1
AND grass_date >= Date_add('day', -90,
CURRENT_DATE)
AND DATE(From_unixtime(create_timestamp))
BETWEEN
Date_add(
'month', -1,
Date_trunc('month', CURRENT_DATE)) AND
Date_add('day', -1, CURRENT_DATE))
UNION ALL
(SELECT grass_region,
DATE(From_unixtime(create_timestamp))
AS
grass_date
,
shop_id
AS
shopid,
order_id
AS
orderid,
Json_extract_scalar(service_fee_info_list,
'$[3].rule_id') AS
rule_id
--, sum(cast(json_extract_scalar(service_fee_info_list,'$[3].fee_amt') as double)) as fee_amount 
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE tz_type = 'local'
AND is_cb_shop = 1
AND grass_date >= Date_add('day', -90,
CURRENT_DATE)
AND DATE(From_unixtime(create_timestamp))
BETWEEN
Date_add(
'month', -1,
Date_trunc('month', CURRENT_DATE)) AND
Date_add('day', -1, CURRENT_DATE))) o
LEFT JOIN (SELECT DISTINCT rule_id,
rule_name
--, cast(fee_rate as double)/10000000 as fee_rate 
FROM
marketplace.shopee_service_fee_rule_db__service_fee_rule_tab__reg_daily_s0_live)
r1
ON CAST(o.rule_id AS INT) = r1.rule_id) a
LEFT JOIN (SELECT DISTINCT b.affi_shopid AS shopid,
b.mst_shopid,
a.country AS mst_country
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
LEFT JOIN
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b
ON b.mst_shopid = a.shopid
WHERE a.cb_option = 1
AND b.grass_region != '') sip
ON a.shopid = sip.shopid
WHERE a.grass_region IN ( 'SG', 'MY', 'TW', 'ID',
'TH', 'PH', 'VN', 'BR', 'MX' )),
order_data
AS (SELECT a.grass_region,
CAST(COUNT(DISTINCT CASE
WHEN grass_date >= Date_trunc('month',
Date_add
(
'day', -1,
CURRENT_DATE))
AND grass_date <= Date_add('day', -1,
CURRENT_DATE)
AND if_fsc = 1 THEN a.orderid
ELSE NULL
END) AS DOUBLE) /
Day_of_month(Date_add('day', -1
,
CURRENT_DATE))
AS MTD_FSC_ADO,
Sum(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
CURRENT_DATE))
AND grass_date <= Date_add('day', -1, CURRENT_DATE)
AND if_fsc = 1 THEN gmv_usd
ELSE NULL
END) / Day_of_month(Date_add('day', -1, CURRENT_DATE))
AS MTD_FSC_ADG,
CAST(COUNT(DISTINCT CASE
WHEN grass_date >= Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND grass_date < Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))
AND if_fsc = 1 THEN a.orderid
ELSE NULL
END) AS DOUBLE) /
Day_of_month(Date_add('day', -1
,
Date_trunc('month'
,
Date_add(
'day', -1,
CURRENT_DATE
))))
AS M1_FSC_ADO,
Sum(CASE
WHEN grass_date >= Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND grass_date < Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE
))
AND if_fsc = 1 THEN gmv_usd
ELSE NULL
END) / Day_of_month(Date_add('day', -1, Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))
))
AS
M1_FSC_ADG,
CAST(COUNT(DISTINCT CASE
WHEN grass_date >= Date_trunc('month',
Date_add
(
'day', -1,
CURRENT_DATE))
AND grass_date <= Date_add('day', -1,
CURRENT_DATE)
AND if_fss = 1 THEN a.orderid
ELSE NULL
END) AS DOUBLE) /
Day_of_month(Date_add('day', -1
,
CURRENT_DATE))
AS MTD_FSS_ADO,
Sum(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
CURRENT_DATE))
AND grass_date <= Date_add('day', -1, CURRENT_DATE)
AND if_fss = 1 THEN gmv_usd
ELSE NULL
END) / Day_of_month(Date_add('day', -1, CURRENT_DATE))
AS MTD_FSS_ADG,
CAST(COUNT(DISTINCT CASE
WHEN grass_date >= Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND grass_date < Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))
AND if_fss = 1 THEN a.orderid
ELSE NULL
END) AS DOUBLE) /
Day_of_month(Date_add('day', -1
,
Date_trunc('month'
,
Date_add(
'day', -1,
CURRENT_DATE
))))
AS M1_FSS_ADO,
Sum(CASE
WHEN grass_date >= Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND grass_date < Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE
))
AND if_fss = 1 THEN gmv_usd
ELSE NULL
END) / Day_of_month(Date_add('day', -1, Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))
))
AS
M1_FSS_ADG,
CAST(COUNT(DISTINCT CASE
WHEN grass_date >= Date_trunc('month',
Date_add
(
'day', -1,
CURRENT_DATE))
AND grass_date <= Date_add('day', -1,
CURRENT_DATE)
AND if_ccb = 1 THEN a.orderid
ELSE NULL
END) AS DOUBLE) /
Day_of_month(Date_add('day', -1
,
CURRENT_DATE))
AS MTD_CCB_ADO,
Sum(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
CURRENT_DATE))
AND grass_date <= Date_add('day', -1, CURRENT_DATE)
AND if_ccb = 1 THEN gmv_usd
ELSE NULL
END) / Day_of_month(Date_add('day', -1, CURRENT_DATE))
AS MTD_CCB_ADG,
CAST(COUNT(DISTINCT CASE
WHEN grass_date >= Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND grass_date < Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))
AND if_ccb = 1 THEN a.orderid
ELSE NULL
END) AS DOUBLE) /
Day_of_month(Date_add('day', -1
,
Date_trunc('month'
,
Date_add(
'day', -1,
CURRENT_DATE
))))
AS M1_CCB_ADO,
Sum(CASE
WHEN grass_date >= Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND grass_date < Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE
))
AND if_ccb = 1 THEN gmv_usd
ELSE NULL
END) / Day_of_month(Date_add('day', -1, Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))
))
AS
M1_CCB_ADG
FROM sf_raw r
LEFT JOIN
(SELECT grass_region
--, date(from_unixtime(create_timestamp)) as grass_date
,
shop_id AS shopid,
order_id AS orderid,
gmv_usd,
order_fraction
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE tz_type = 'local'
AND is_cb_shop = 1
AND grass_date >= Date_add('day', -90,
CURRENT_DATE
)
AND DATE(From_unixtime(create_timestamp))
BETWEEN
Date_add(
'month', -1,
Date_trunc('month', CURRENT_DATE)) AND
Date_add('day', -1, CURRENT_DATE)) a
ON r.orderid = a.orderid
GROUP BY 1)
----------------------------------------------shop count-----------------------------------------
,
shop_raw
AS (SELECT grass_region,
shopid,
Sum(CASE
WHEN start_date <= Date_add('day', -1, Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND end_date >= Date_add('day', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))) THEN
if_fsc
ELSE 0
END) AS M1_fsc,
Sum(CASE
WHEN start_date <= Date_add('day', -1, Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND end_date >= Date_add('day', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))) THEN
if_ccb
ELSE 0
END) AS M1_ccb,
Sum(CASE
WHEN start_date <= Date_add('day', -1, Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE)))
AND end_date >= Date_add('day', -1,
Date_trunc('month',
Date_add('day', -1,
CURRENT_DATE))) THEN
if_fss
ELSE 0
END) AS M1_fss,
Sum(CASE
WHEN start_date <= Date_add('day', -1, CURRENT_DATE)
AND end_date >= Date_add('day', -1, CURRENT_DATE)
THEN
if_fsc
ELSE 0
END) AS MTD_fsc,
Sum(CASE
WHEN start_date <= Date_add('day', -1, CURRENT_DATE)
AND end_date >= Date_add('day', -1, CURRENT_DATE)
THEN
if_ccb
ELSE 0
END) AS MTD_ccb,
Sum(CASE
WHEN start_date <= Date_add('day', -1, CURRENT_DATE)
AND end_date >= Date_add('day', -1, CURRENT_DATE)
THEN
if_fss
ELSE 0
END) AS MTD_fss
FROM (SELECT DISTINCT r1.region AS
grass_region,
r1.rule_id,
shop_id AS shopid,
CASE
WHEN ( ( r1.region = 'SG'
AND CAST(r1.rule_id AS INT) IN (
12426 )
)
OR ( r1.region = 'MY'
AND rule_name LIKE '%FSS CCB%' )
OR ( r1.region NOT IN (
'SG', 'MY', 'PH' )
AND ( r1.rule_name LIKE '%FSC%'
OR r1.rule_name LIKE
'%535%'
OR r1.rule_name LIKE
'%321%'
OR r1.rule_name LIKE
'%CCB and FSS%' ) )
--or (sip.shopid is not null and sip.grass_region = 'SG')
) THEN 1
ELSE 0
END AS if_FSC,
CASE
WHEN ( ( r1.region = 'SG'
AND CAST(r1.rule_id AS INT) IN (
12429 )
)
OR ( r1.region = 'MY'
AND ( r1.rule_name LIKE '%FSS%'
OR r1.rule_name LIKE
'%Dday%'
)
AND r1.rule_name NOT LIKE
'%CCB%' )
OR ( r1.region = 'PH'
AND ( r1.rule_name LIKE '%FSS%'
OR r1.rule_name LIKE
'%PSP%' )
AND r1.rule_name NOT LIKE
'%CCB%' )
OR (
r1.region NOT IN (
'SG', 'MY', 'PH' )
AND ( r1.rule_name LIKE '%FSS%'
OR Lower(r1.rule_name)
LIKE
'%cn exclusivity%'
OR Lower(r1.rule_name)
LIKE
'%free shipping campaign%'
)
AND r1.rule_name NOT LIKE '%CCB%' ) )
THEN 1
ELSE 0
END AS if_FSS,
CASE
WHEN ( ( r1.region = 'SG'
AND CAST(r1.rule_id AS INT) IN (
583, 721, 1696, 10237,
16151, 16157 ) )
OR ( r1.region = 'MY'
AND r1.rule_name LIKE '%CCB%'
AND r1.rule_name NOT LIKE
'%FSS%' )
OR ( r1.region = 'PH'
AND r1.rule_name LIKE '%CCB%'
AND r1.rule_name NOT LIKE
'%FSS%' )
OR ( r1.region NOT IN (
'SG', 'MY', 'PH' )
AND ( r1.rule_name LIKE '%CCB%'
OR r1.rule_name LIKE
'%CBXTRA%' )
AND r1.rule_name NOT LIKE
'%FSS%' ) )
THEN 1
ELSE 0
END AS if_CCB,
DATE(From_unixtime(r2.start_time)) AS
start_date,
DATE(From_unixtime(r2.end_time)) AS end_date
FROM
marketplace.shopee_service_fee_rule_db__service_fee_rule_tab__reg_daily_s0_live
r1
JOIN
marketplace.shopee_service_fee_rule_shop_db__service_fee_rule_shop_tab__reg_daily_s0_live r2
ON r1.rule_id = r2.rule_id
WHERE ( ( r1.rule_name LIKE '%FSS%'
OR r1.rule_name LIKE '%CCB%'
OR r1.rule_name LIKE '%FSC%'
OR r1.rule_name LIKE '%CCB and FSS%'
OR r1.rule_name LIKE '%CBXTRA%'
OR r1.rule_name LIKE '%535%'
OR r1.rule_name LIKE '%321%'
OR Lower(r1.rule_name) LIKE '%cn exclusivity%'
OR Lower(r1.rule_name) LIKE '%free shipping campaign%'
OR rule_name LIKE '%PSP%'
OR rule_name LIKE '%Dday%' )
OR ( r2.rule_id IN ( 12426, 12429, 583, 721,
1696, 10237, 16151, 16157 ) ) )
AND DATE(From_unixtime(r2.start_time)) <=
Date_add('day', -1, CURRENT_DATE)
AND DATE(From_unixtime(r2.end_time)) >= Date_add('month', -1,
Date_trunc('month', Date_add('day', -1,
CURRENT_DATE))) -- Start of M-1
AND r1.rule_status = 1)
GROUP BY 1,
2),
shop_data
AS (SELECT sr.grass_region,
COUNT(DISTINCT CASE
WHEN m1_fsc = 1 THEN sr.shopid
ELSE NULL
END) AS M1_FSC_shop,
COUNT(DISTINCT CASE
WHEN m1_fss = 1 THEN sr.shopid
ELSE NULL
END) AS M1_FSS_shop,
COUNT(DISTINCT CASE
WHEN m1_ccb = 1 THEN sr.shopid
ELSE NULL
END) AS M1_CCB_shop,
COUNT(DISTINCT CASE
WHEN mtd_fsc = 1 THEN sr.shopid
ELSE NULL
END) AS MTD_FSC_shop,
COUNT(DISTINCT CASE
WHEN mtd_fss = 1 THEN sr.shopid
ELSE NULL
END) AS MTD_FSS_shop,
COUNT(DISTINCT CASE
WHEN mtd_ccb = 1 THEN sr.shopid
ELSE NULL
END) AS MTD_CCB_shop
FROM shop_raw sr
JOIN (SELECT DISTINCT grass_region,
shop_id,
is_cb_shop
FROM mp_user.dim_shop__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = Date_add('day', -1, CURRENT_DATE)
AND status = 1) s
ON sr.shopid = s.shop_id
JOIN (SELECT DISTINCT grass_region,
shop_id
FROM mp_user.dim_user__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = Date_add('day', -1, CURRENT_DATE)
AND status = 1) u
ON sr.shopid = u.shop_id
WHERE is_cb_shop = 1
GROUP BY 1)
SELECT o.grass_region,
mtd_fsc_ado,
mtd_fsc_adg,
mtd_fsc_shop,
mtd_fss_ado,
mtd_fss_adg,
mtd_fss_shop,
mtd_ccb_ado,
mtd_ccb_adg,
mtd_ccb_shop,
m1_fsc_ado,
m1_fsc_adg,
m1_fsc_shop,
m1_fss_ado,
m1_fss_adg,
m1_fss_shop,
m1_ccb_ado,
m1_ccb_adg,
m1_ccb_shop
FROM order_data o
LEFT JOIN shop_data s
ON o.grass_region = s.grass_region