insert into nexus.test_64974b54b0f46e3008f6cf6b2c5449a2cc70041854978883d45d09a605b14d93_8a2672582c4fe8029779a78d6e55ac54 WITH 
seller_index AS (
SELECT DISTINCT cb.shop_id
, cb.user_id
, CASE WHEN white.whitelist_date IS NOT NULL THEN white.whitelist_date
WHEN cb.grass_region IN ('SG') THEN DATE('2022-03-07')
WHEN cb.grass_region IN ('ID') THEN DATE('2022-03-14')
WHEN cb.grass_region IN ('TH','TW') THEN DATE('2022-03-21')
WHEN cb.grass_region IN ('BR','CL','CO','ES','MX','MY','PH','PL','VN') THEN DATE('2022-04-11')
ELSE white.whitelist_date END AS whitelist_date
FROM mp_user.dim_shop__reg_s0_live AS cb
LEFT JOIN (
SELECT region, CAST(seller_userid AS BIGINT) AS seller_userid, DATE(whitelist_date) AS whitelist_date
FROM regcbbi_others.pay_sts_refactor_whitelist_v2__reg_s0_live
) AS white ON white.seller_userid = cb.user_id
WHERE (cb.grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MX', 'MY', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
OR white.seller_userid IS NOT NULL)
AND cb.tz_type = 'local' AND cb.is_cb_shop = 1
AND cb.grass_date = DATE_ADD('day', -2, current_date)
)


SELECT t2.grass_region
, aud.reason
, COUNT(DISTINCT e.orderid) AS order_count
FROM (
SELECT o.orderid
, "date"("from_unixtime"("min"(CASE WHEN o.new_status = 12 THEN o.ctime ELSE null END))) AS escrow_created_time
, "date"("from_unixtime"("min"(CASE WHEN o.new_status = 13 THEN o.ctime ELSE null END))) AS escrow_pending_time
, "date"("from_unixtime"("min"(CASE WHEN o.new_status = 14 THEN o.ctime ELSE null END))) AS escrow_verified_time
FROM marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_continuous_s0_live AS o
INNER JOIN seller_index AS cb ON cb.shop_id = o.shopid
WHERE "date"("from_unixtime"(o.ctime)) >= DATE_ADD('day', -120, date(from_unixtime(1655935200)))
AND o.grass_region in ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
GROUP BY 1
) AS e
LEFT JOIN (
SELECT aud1.orderid, JSON_EXTRACT_SCALAR(FROM_UTF8(aud1.data), '$.Operator') AS operator
, JSON_EXTRACT_SCALAR(FROM_UTF8(aud1.data), '$.Reason') AS reason 
FROM marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_continuous_s0_live AS aud1
INNER JOIN (
SELECT aud3.orderid, MAX(aud3.ctime) AS ctime
FROM marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_continuous_s0_live AS aud3
INNER JOIN seller_index AS cb ON cb.shop_id = CAST(aud3.shopid AS BIGINT)
WHERE aud3.grass_region IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
GROUP BY 1
) AS aud2 ON (aud2.orderid = aud1.orderid AND aud2.ctime = aud1.ctime)
WHERE aud1.grass_region IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
) AS aud ON aud.orderid = e.orderid
INNER JOIN (
SELECT o.order_id
, o.grass_region
, from_unixtime(o.create_timestamp) as create_time
, o.seller_id
, payeeid.userid
, o.order_be_status_id
, o.order_be_status
, payeeid.ctime AS payeeid_valid_ctime
FROM mp_order.dwd_order_all_ent_df__reg_s0_live AS o
INNER JOIN seller_index AS cb ON cb.shop_id = o.shop_id
INNER JOIN (
SELECT userid, ctime, mtime
FROM (
(SELECT userid, "max"("date"("from_unixtime"(ctime))) ctime, "max"("date"("from_unixtime"(mtime))) mtime
FROM marketplace.shopee_payment_backend_id_db__channel_payee_account_tab__reg_daily_s0_live
WHERE status = 4 GROUP BY 1)
UNION ALL
(SELECT userid, "max"("date"("from_unixtime"(ctime))) ctime, "max"("date"("from_unixtime"(mtime))) mtime
FROM marketplace.shopee_payment_backend_my_db__channel_payee_account_tab__reg_daily_s0_live
WHERE status = 4 GROUP BY 1)
UNION ALL
(SELECT userid, "max"("date"("from_unixtime"(ctime))) ctime, "max"("date"("from_unixtime"(mtime))) mtime
FROM marketplace.shopee_payment_backend_ph_db__channel_payee_account_tab__reg_daily_s0_live
WHERE status = 4 GROUP BY 1)
UNION ALL
(SELECT userid, "max"("date"("from_unixtime"(ctime))) ctime, "max"("date"("from_unixtime"(mtime))) mtime
FROM marketplace.shopee_payment_backend_sg_db__channel_payee_account_tab__reg_daily_s0_live
WHERE status = 4 GROUP BY 1)
UNION ALL
(SELECT userid, "max"("date"("from_unixtime"(ctime))) ctime, "max"("date"("from_unixtime"(mtime))) mtime
FROM marketplace.shopee_payment_backend_th_db__channel_payee_account_tab__reg_daily_s0_live
WHERE status = 4 GROUP BY 1)
UNION ALL
(SELECT userid, "max"("date"("from_unixtime"(ctime))) ctime, "max"("date"("from_unixtime"(mtime))) mtime
FROM marketplace.shopee_payment_backend_tw_db__channel_payee_account_tab__reg_daily_s0_live
WHERE status = 4 GROUP BY 1)
UNION ALL
(SELECT userid, "max"("date"("from_unixtime"(ctime))) ctime, "max"("date"("from_unixtime"(mtime))) mtime
FROM marketplace.shopee_payment_backend_vn_db__channel_payee_account_tab__reg_daily_s0_live
WHERE status = 4 GROUP BY 1)
)
WHERE ctime <= current_date
) AS payeeid ON o.seller_id = payeeid.userid
WHERE o.tz_type = 'local' AND o.is_cb_shop = 1
AND o.grass_date >= DATE_ADD('day', -120, current_date)
AND from_unixtime(o.create_timestamp) >= DATE_ADD('day', -120, current_date)
AND o.order_be_status_id IN (12)
AND o.grass_region IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
) AS t2 ON e.orderid = t2.order_id
INNER JOIN (
SELECT u.user_id AS userid, s.status AS shop_status, u.status AS user_status, s.is_official_shop, s.is_preferred_shop
FROM mp_user.dim_user__reg_s0_live AS u
JOIN mp_user.dim_shop__reg_s0_live AS s ON u.user_id = s.user_id
WHERE s.is_cb_shop = 1
AND u.grass_date = date_add('day',-1,current_date)
AND s.grass_date = date_add('day',-1,current_date)
AND u.status = 1
AND s.status = 1
AND u.grass_region IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
AND s.grass_region IN ('ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
) AS up ON t2.seller_id = up.userid
WHERE ( t2.order_be_status_id = 12 AND e.escrow_created_time <= current_date )
AND e.escrow_verified_time IS NULL
GROUP BY 1,2
ORDER BY 1,2