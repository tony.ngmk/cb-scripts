insert into nexus.test_e4423d4da18522caa796f5e6151aca38acd8fdf450dd5ec71c58d3a8c63ed47d_1850b3e41c3e88c5e518339dcd1f19c2 --2022/04/05 update:
-- fixed sip weight issue
-- 2022/04/26 updates:
-- added IDSIP offline adj amount --rebate_seller col


WITH
sip AS (
SELECT 
grass_region
, affi_shopid
, mst_shopid
, b.country mst_country
FROM
(marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON (a.mst_shopid = b.shopid))
WHERE ((b.cb_option = 0) AND (a.grass_region <> ''))
) 
, fees AS (
SELECT DISTINCT
orderid
, cc_fee
, commission_fee
, service_fee
FROM
regcbbi_others.shopee_bi_keyreports_local_sip_revenue_v3
WHERE ((( (lane IN ('IDPH', 'IDSG', 'IDMY','IDTH','IDVN','IDCO', 'IDCL','MYSG', 'MYPH','TWSG', 'TWMY', 'TWID','IDBR','VNMY','IDMX','VNSG','VNPH','THMY', 'THSG'))))
)
AND grass_date >= current_date - INTERVAL '100' DAY)
, wh AS (SELECT DISTINCT * FROM regcbbi_others.logistics_basic_info
WHERE wh_outbound_date >= CURRENT_DATE - INTERVAL '90' DAY)
, o AS (
SELECT 
"date"("from_unixtime"(create_timestamp)) create_timestamp
, DATE(CAST(create_datetime AS TIMESTAMP)) AS create_date
, order_id
, order_sn
, shop_id
, gmv
, buyer_paid_shipping_fee
, pv_rebate_by_shopee_amt
, sv_rebate_by_shopee_amt
, pv_coin_earn_by_shopee_amt
, sv_coin_earn_by_shopee_amt 
, sv_rebate_by_seller_amt
, pv_rebate_by_seller_amt
, sv_coin_earn_by_seller_amt
, pv_coin_earn_by_seller_amt
, coin_used_cash_amt 
, card_rebate_by_bank_amt
, IF((actual_shipping_rebate_by_shopee_amt = 0), estimate_shipping_rebate_by_shopee_amt, actual_shipping_rebate_by_shopee_amt) shopee_shipping_rebate
, actual_shipping_fee
, logistics_status_id
, payment_method_id
, order_be_status_id
, sv_promotion_id
, card_rebate_by_shopee_amt
, is_cb_shop
, grass_region

FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE 
grass_date >= current_date - INTERVAL '100' DAY
AND tz_type='local'
AND grass_region NOT IN ('IN', 'AR', 'FR')
AND "date"("from_unixtime"(create_timestamp)) >= DATE('2021-12-03')
)



, ai AS (
SELECT DISTINCT
order_id
, order_sn
, is_cb_shop
, o.grass_region
, "sum"(item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
, "sum"(item_tax_amt) item_tax_amt
, "sum"(commission_fee) commission_fee
, "sum"(buyer_txn_fee) buyer_txn_fee
, "sum"(seller_txn_fee) seller_txn_fee
, "sum"(service_fee) service_fee
, SUM(affi_real_weight*item_amount) AS affi_real_weight
, SUM(CASE WHEN affi_real_weight > 0 THEN item_amount END) AS item_amount_w_real_weight
, SUM(item_weight_pp*1000*item_amount) AS seller_input_weight
, sum(item_amount*COALESCE(sum_hpfn,0)) AS hpfn -- a currency
, sum(item_amount*COALESCE(initial_hp,0)) AS initial_price -- a currency
, SUM(item_amount) item_amount
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
JOIN sip ON o.shop_id = sip.affi_shopid
LEFT JOIN (SELECT DISTINCT item_id affi_itemid
, primary_item_id mst_itemid
, shop_id affi_shopid
, primary_shop_id mst_shopid
, COALESCE(item_real_weight / 100.00, 0) AS affi_real_weight
FROM mp_item.ads_sip_affi_item_profile__reg_s0_live
WHERE primary_shop_type = 0
AND tz_type = 'local'
AND grass_date = current_date - INTERVAL '2' DAY
) si
ON o.item_id = si.affi_itemid 
LEFT JOIN (SELECT DISTINCT mst_country
, affi_country
, TRY_CAST(weight AS DOUBLE) weight
, TRY_CAST(hpfn AS DOUBLE) hpfn 
, TRY_CAST(initial_hp AS DOUBLE) initial_hp
, TRY_CAST(sum_hpfn AS DOUBLE) sum_hpfn
, TRY_CAST(fx AS DOUBLE) fx 
, TRY_CAST(country_margin AS DOUBLE) country_margin
FROM regcbbi_others.local_sip_price_config a 
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp FROM regcbbi_others.local_sip_price_config) b
ON a.ingestion_timestamp = b.ingestion_timestamp) cfg 
ON sip.grass_region = cfg.affi_country 
AND sip.mst_country = cfg.mst_country 
AND CASE WHEN (affi_real_weight = 0 OR affi_real_weight IS NULL) THEN IF(item_weight_pp*1000 BETWEEN 1 AND 10,1,FLOOR(IF(item_weight_pp*1000 >= 20000,20000,item_weight_pp*1000)/10.0)*10) = cfg.weight
ELSE IF(affi_real_weight BETWEEN 1 AND 10,1,FLOOR(IF(affi_real_weight >= 20000,20000,affi_real_weight)/10.0)*10) = cfg.weight END

WHERE 
o.grass_region NOT IN ('IN', 'AR', 'FR')
AND grass_date >= current_date - INTERVAL '100' DAY
AND tz_type='local'
AND is_cb_shop = 1
AND "date"("from_unixtime"(create_timestamp)) >= DATE('2021-12-03')
GROUP BY 1, 2, 3, 4
)


, pi AS (
SELECT DISTINCT
order_id
, order_sn
, is_cb_shop
, o.grass_region
, "sum"(item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
, "sum"(item_tax_amt) item_tax_amt
, "sum"(commission_fee) commission_fee
, "sum"(buyer_txn_fee) buyer_txn_fee
, "sum"(seller_txn_fee) seller_txn_fee
, "sum"(service_fee) service_fee
, SUM(item_amount) item_amount
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
JOIN (SELECT DISTINCT mst_shopid FROM sip) sip ON o.shop_id = sip.mst_shopid
WHERE 
o.grass_region IN ('ID', 'MY', 'TW', 'VN', 'TH')
AND grass_date >= current_date - INTERVAL '100' DAY
AND tz_type='local'
AND is_cb_shop = 0
AND "date"("from_unixtime"(create_timestamp)) >= DATE('2021-12-03')
GROUP BY 1, 2, 3, 4
) 
, rr AS (
SELECT 
orderid
, (refund_amount / DECIMAL '100000.00') refund_amount
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE 
grass_region NOT IN ('IN', 'AR', 'FR')
AND "date"("from_unixtime"(ctime)) >= current_date - INTERVAL '100' DAY
AND refund_amount > 0
AND status IN (2,5)
) 




, ssv AS (SELECT distinct perc_ssv/100.00 AS perc_ssv
,promo_id FROM regcbbi_others.sip_local_pnl_ssv)


, rbt AS (
SELECT DISTINCT
overseas_itemid
, overseas_modelid
, seller_negotiated_price_local_currency
, start_date_time
, end_date_time
FROM
regcbbi_others.sip_local_pnl_item_rebate
WHERE ((overseas_itemid > 0) AND (seller_negotiated_price_local_currency > 0)
AND start_date_time IS NOT NULL
AND end_date_time IS NOT NULL)
) 
, rbt2 AS (
SELECT DISTINCT
orderid
, "sum"(rebates) rebate
FROM
(
SELECT 
orderid
, itemid
, modelid
, ((seller_negotiated_price_local_currency * amount) - price * amount) rebates
FROM
(
SELECT
order_id orderid
, item_id itemid
, model_id modelid
, item_amount amount
, item_price_pp price
, ("from_unixtime"(create_timestamp)) create_time
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE 
grass_region IN ('SG', 'MY')
AND grass_date >= current_date - INTERVAL '100' DAY
AND is_cb_shop = 1
AND "date"("from_unixtime"(create_timestamp)) >= DATE('2021-12-03')
AND tz_type='local'
) p_oi
INNER JOIN rbt 
ON rbt.overseas_modelid = p_oi.modelid 
AND rbt.overseas_itemid = p_oi.itemid
AND p_oi.create_time BETWEEN start_date_time AND end_date_time
) 
GROUP BY 1


)


, offline_seller AS (SELECT DISTINCT TRY_CAST(mst_order_id AS BIGINT) mst_order_id
, TRY_CAST(mst_escrow_amt AS DOUBLE) mst_escrow_amt
, TRY_CAST(mst_deduction_amt AS DOUBLE) mst_deduction_amt
FROM regcbbi_others.sip__localsip_idsip_offline_adj_order_lvl__wf__reg__s0
WHERE ingestion_timestamp = (SELECT MAX(ingestion_timestamp) it 
FROM regcbbi_others.sip__localsip_idsip_offline_adj_order_lvl__wf__reg__s0)
)


SELECT DISTINCT
mst_country
, sip.grass_region
, wh_outbound_date
, COUNT(DISTINCT a.order_id) AS orders
, "sum"(a.gmv) gmv
, "sum"(a.buyer_paid_shipping_fee) buyer_paid_shipping_fee
, "sum"(ai.item_rebate_by_shopee_amt) item_rebate_by_shopee_amt
, "sum"(a.pv_rebate_by_shopee_amt) pv_rebate_by_shopee_amt
, "sum"(a.coin_used_cash_amt) coin_used_cash_amt
, "sum"(a.card_rebate_by_bank_amt) card_rebate_by_bank_amt
, "sum"(a.card_rebate_by_shopee_amt) card_rebate_by_shopee_amt
, "sum"(a.sv_rebate_by_seller_amt) sv_rebate_by_seller_amt
, "sum"(a.sv_coin_earn_by_seller_amt) sv_coin_earn_by_seller_amt
, "sum"(ai.item_tax_amt) item_tax_amt
, "sum"(a.shopee_shipping_rebate) shopee_shipping_rebate
, "sum"(wh.actual_shipping_fee) asf_sls
, "sum"(fees.commission_fee) commission_fee
, "sum"(fees.cc_fee) seller_txn_fee
, "sum"(fees.service_fee) service_fee
, "sum"(IF(rr.refund_amount > 0,rr.refund_amount,0)) refund_amount
, "sum"((CASE WHEN ((a.logistics_status_id = 6) AND (a.payment_method_id = 6)) THEN a.gmv + ai.item_rebate_by_shopee_amt + a.pv_rebate_by_shopee_amt + a.coin_used_cash_amt + a.card_rebate_by_bank_amt + a.card_rebate_by_bank_amt - a.sv_coin_earn_by_seller_amt - a.pv_coin_earn_by_seller_amt - ai.item_tax_amt + a.shopee_shipping_rebate + fees.commission_fee + fees.cc_fee + fees.service_fee ELSE 0 END)) cod_fail
, "sum"((CASE WHEN (ssv.promo_id IS NOT NULL) THEN ((perc_ssv ) * a.sv_rebate_by_seller_amt) ELSE 0 END)) ssv_rebate
, "sum"(p.gmv) gmv_p
, "sum"(p.buyer_paid_shipping_fee) buyer_paid_shipping_fee_p
, "sum"(pi.commission_fee) commission_fee_p
, "sum"(pi.buyer_txn_fee) buyer_txn_fee_p
, "sum"(pi.seller_txn_fee) seller_txn_fee_p
, "sum"(pi.service_fee) service_fee_p
, "sum"(pi.item_rebate_by_shopee_amt) item_rebate_by_shopee_amt_p
, "sum"(p.pv_rebate_by_shopee_amt) pv_rebate_by_shopee_amt_p
, "sum"(p.shopee_shipping_rebate) shopee_shipping_rebate_p
, "sum"(p.sv_coin_earn_by_seller_amt) sv_coin_earn_by_seller_p
, "sum"(p.coin_used_cash_amt) coin_used_cash_amt_p
, "sum"(COALESCE(ai.hpfn, 0)) hpfn
, "sum"(rebate) rebate
, SUM(ai.initial_price) AS initial_price
, SUM(a.pv_coin_earn_by_seller_amt) AS pv_coin_earn_by_seller_amt
, SUM(a.pv_rebate_by_seller_amt) AS pv_rebate_by_seller_amt
, SUM(a.sv_rebate_by_shopee_amt) AS sv_rebate_by_shopee_amt
, SUM(a.pv_coin_earn_by_shopee_amt) AS pv_coin_earn_by_shopee_amt
, SUM(a.sv_coin_earn_by_shopee_amt) AS sv_coin_earn_by_shopee_amt
, SUM(ai.item_amount) item_amount
, SUM(offline_seller.mst_deduction_amt) AS rebate_seller
FROM
(
SELECT *
FROM
o
WHERE is_cb_shop = 1 
AND grass_region IN ('SG', 'MY', 'ID', 'PH','TH','VN','BR','MX', 'CO', 'CL')
) a
INNER JOIN ai 
ON a.order_id = ai.order_id
INNER JOIN sip 
ON a.shop_id = sip.affi_shopid
INNER JOIN (SELECT DISTINCT 
a_site
, p_site
, CAST(live_date AS DATE) live_date
, remark
FROM regcbbi_others.sip__localsip_lane_live_date__df__reg__s0
) live_date 
ON a.grass_region = live_date.a_site
AND sip.mst_country = live_date.p_site
AND a.create_date >= live_date.live_date -- filter out test orders
INNER JOIN (SELECT oversea_orderid
, oversea_shopid
, oversea_country
, local_orderid
, local_shopid 
, local_country
FROM marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE 
DATE(from_unixtime(ctime)) >= current_date - INTERVAL '100' DAY
AND oversea_country <> 'id'
) m 
ON a.order_id = m.oversea_orderid
INNER JOIN (
SELECT *
FROM
o


WHERE 
order_be_status_id NOT IN (1, 6, 8, 5, 7, 8, 16) 
AND is_cb_shop = 0 
AND grass_region IN ('ID', 'MY', 'TW','VN', 'TH')
) p 
ON m.local_orderid = p.order_id
INNER JOIN wh 


ON a.order_sn = wh.ordersn
LEFT JOIN pi 
ON p.order_id = pi.order_id
LEFT JOIN fees ON a.order_id = fees.orderid
LEFT JOIN rr ON a.order_id = rr.orderid
LEFT JOIN ssv ON a.sv_promotion_id = ssv.promo_id
LEFT JOIN rbt2 ON a.order_id = rbt2.orderid
LEFT JOIN offline_seller 
ON p.order_id = offline_seller.mst_order_id 
-- WHERE mst_country in ('ID') 
-- AND sip.grass_region IN ('BR','CO') 
-- AND wh_outbound_date between DATE('2022-03-31') AND DATE ('2022-04-05')
GROUP BY 1, 2, 3