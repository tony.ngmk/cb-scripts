insert into nexus.test_4eee6998398755ce1cf9ccd0c0e570c9c741fb11a46bf76cb9bc4bde274761d9_aae52ce2cef915bf696acdf0044b1b5a WITH
cb_and_mall_shops AS (
SELECT shop_id
FROM
mp_user.dim_shop__reg_s0_live
WHERE ((((grass_region = 'ID') AND (is_official_shop = 1)) AND (is_cb_shop = 1)) AND (grass_date = (current_date - INTERVAL '1' DAY)))
) 
, mtd_orders AS (
SELECT
s.shop_id
, item_id
, (CAST("count"(DISTINCT (CASE WHEN (grass_date BETWEEN "date_trunc"('month', (current_date - INTERVAL '1' DAY)) AND (current_date - INTERVAL '1' DAY)) THEN order_id ELSE null END)) AS double) / "day"((current_date - INTERVAL '1' DAY))) mtd_ado
, CAST("sum"((CASE WHEN (grass_date BETWEEN "date_trunc"('month', (current_date - INTERVAL '1' DAY)) AND (current_date - INTERVAL '1' DAY)) THEN gmv ELSE null END)) AS double) mtd_gmv
FROM
(mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
INNER JOIN cb_and_mall_shops s ON (o.shop_id = s.shop_id))
WHERE (((((IF((grass_region IN ('BR', 'MX')), "date"("from_unixtime"(create_timestamp)), grass_date) <= (current_date - INTERVAL '1' DAY)) AND (o.tz_type = 'local')) AND (o.is_placed = 1)) AND (o.is_bi_excluded = 0)) AND (bi_exclude_reason IS NULL))
GROUP BY 1, 2
) 
, total_orders_gmv AS (
SELECT
s.shop_id
, o.item_id
, CAST("count"(DISTINCT o.order_id) AS double) total_orders
, CAST("sum"(gmv) AS double) total_gmv
FROM
(mp_order.dwd_order_item_all_ent_df__reg_s0_live o
RIGHT JOIN cb_and_mall_shops s ON (o.shop_id = s.shop_id))
WHERE (((("date"("from_unixtime"(o.create_timestamp)) <= (current_date - INTERVAL '1' DAY)) AND (o.tz_type = 'local')) AND (o.is_bi_excluded = 0)) AND (o.bi_exclude_reason IS NULL))
GROUP BY 1, 2
) 
SELECT
m.shop_id
, m.item_id
, mtd_ado
, mtd_gmv
, total_orders
, total_gmv
FROM
(total_orders_gmv o
INNER JOIN mtd_orders m ON ((o.shop_id = m.shop_id) AND (o.item_id = m.item_id)))
ORDER BY 1 ASC, 2 ASC