insert into nexus.test_d860b945c7610792da87a13e900d0ab3c7d6d217b455f2fa4ef50474f92303c7_e499d6cf8ad67fa7950c86733ff6693f WITH
sip AS (
SELECT t2.affi_shopid
, t1.cb_option
, t1.country AS psite
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live AS t1
INNER JOIN (
SELECT DISTINCT affi_shopid
, affi_userid
, affi_username
, mst_shopid
, country
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status != 3
AND grass_region != ''
) AS t2 ON t1.shopid = t2.mst_shopid
)
, seller_index AS (
SELECT DISTINCT grass_region
, si.user_id
, si.shop_id
, merchant_name AS gp_account_name
, merchant_id AS gp_account_id
, merchant_owner_email AS gp_account_owner
, shop_organization_name AS child_account_owner_userrole_name
, merchant_region AS gp_account_billing_country
, merchant_organization_name AS gp_account_owner_userrole_name
, CASE WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
WHEN sip.affi_shopid IS NOT NULL AND sip.cb_option = 0 THEN 'Local SIP'
WHEN sip.affi_shopid IS NOT NULL AND sip.psite IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL AND merchant_region = 'KR' THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL THEN 'CB SIP'
else si.seller_type end AS seller_type
FROM dev_regcbbi_general.cb_seller_profile_reg AS si
LEFT JOIN sip ON si.shop_id = sip.affi_shopid
LEFT JOIN (
SELECT DISTINCT shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date BETWEEN DATE_ADD('day', -14, current_date) AND DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = si.shop_id
WHERE si.grass_region IN ('ID','MY','PH','SG','TH','VN','MX')
)
, fx AS (
SELECT DISTINCT currency, exchange_rate AS local_to_usd
FROM mp_order.dim_exchange_rate__reg_s0_live
WHERE grass_date = DATE_ADD('day', -1, current_date)
)
, sts AS (
SELECT sts.region AS grass_region
, sts.user_id
, SUM(sts.billing_item_amount / fx.local_to_usd) AS balance_usd
, SUM(CASE WHEN is_wh_handling_fee = 1 THEN sts.billing_item_amount / fx.local_to_usd ELSE 0 END) AS wh_handling_fee
, SUM(CASE WHEN is_wh_storage_fee = 1 THEN sts.billing_item_amount / fx.local_to_usd ELSE 0 END) AS wh_storage_fee
, SUM(CASE WHEN is_wh_fee = 1 THEN sts.billing_item_amount / fx.local_to_usd ELSE 0 END) AS wh_fee
, SUM(CASE WHEN is_offset = 1 THEN sts.billing_item_amount / fx.local_to_usd ELSE 0 END) AS gp_offset
FROM (
SELECT 'ID' AS region
, CAST(a.seller_userid AS BIGINT) AS user_id
, CAST(bi.amount AS DOUBLE) / 100000 AS billing_item_amount
, bi.ctime
, 'IDR' AS currency
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27) THEN 1 ELSE 0 END AS is_wh_handling_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (37) THEN 1 ELSE 0 END AS is_wh_storage_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27,37,40,51) THEN 1 ELSE 0 END AS is_wh_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (66) THEN 1 ELSE 0 END AS is_offset
FROM marketplace.shopee_order_accounting_settlement_id_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT DISTINCT CAST(account_id AS VARCHAR) AS account_id, CAST(rule_id AS VARCHAR) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_id_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS VARCHAR)
WHERE bi.is_cb = 1 AND bi.item_status IN (2) AND bi.item_type IN (1,2,3,4,5)


UNION ALL


SELECT 'MY' AS region
, CAST(a.seller_userid AS BIGINT) AS user_id
, CAST(bi.amount AS DOUBLE) / 100000 AS billing_item_amount
, bi.ctime
, 'MYR' AS currency
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27) THEN 1 ELSE 0 END AS is_wh_handling_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (37) THEN 1 ELSE 0 END AS is_wh_storage_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27,37,40,51) THEN 1 ELSE 0 END AS is_wh_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (66) THEN 1 ELSE 0 END AS is_offset
FROM marketplace.shopee_order_accounting_settlement_my_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT DISTINCT CAST(account_id AS VARCHAR) AS account_id, CAST(rule_id AS VARCHAR) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_my_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS VARCHAR)
WHERE bi.is_cb = 1 AND bi.item_status IN (2) AND bi.item_type IN (1,2,3,4,5)


UNION ALL


SELECT 'PH' AS region
, CAST(a.seller_userid AS BIGINT) AS user_id
, CAST(bi.amount AS DOUBLE) / 100000 AS billing_item_amount
, bi.ctime
, 'PHP' AS currency
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27) THEN 1 ELSE 0 END AS is_wh_handling_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (37) THEN 1 ELSE 0 END AS is_wh_storage_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27,37,40,51) THEN 1 ELSE 0 END AS is_wh_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (66) THEN 1 ELSE 0 END AS is_offset
FROM marketplace.shopee_order_accounting_settlement_ph_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT DISTINCT CAST(account_id AS VARCHAR) AS account_id, CAST(rule_id AS VARCHAR) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_ph_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS VARCHAR)
WHERE bi.is_cb = 1 AND bi.item_status IN (2) AND bi.item_type IN (1,2,3,4,5)


UNION ALL


SELECT 'SG' AS region
, CAST(a.seller_userid AS BIGINT) AS user_id
, CAST(bi.amount AS DOUBLE) / 100000 AS billing_item_amount
, bi.ctime
, 'SGD' AS currency
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27) THEN 1 ELSE 0 END AS is_wh_handling_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (37) THEN 1 ELSE 0 END AS is_wh_storage_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27,37,40,51) THEN 1 ELSE 0 END AS is_wh_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (66) THEN 1 ELSE 0 END AS is_offset
FROM marketplace.shopee_order_accounting_settlement_sg_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT DISTINCT CAST(account_id AS VARCHAR) AS account_id, CAST(rule_id AS VARCHAR) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_sg_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS VARCHAR)
WHERE bi.is_cb = 1 AND bi.item_status IN (2) AND bi.item_type IN (1,2,3,4,5)


UNION ALL


SELECT 'TH' AS region
, CAST(a.seller_userid AS BIGINT) AS user_id
, CAST(bi.amount AS DOUBLE) / 100000 AS billing_item_amount
, bi.ctime
, 'THB' AS currency
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27) THEN 1 ELSE 0 END AS is_wh_handling_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (37) THEN 1 ELSE 0 END AS is_wh_storage_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (27,37,40,51) THEN 1 ELSE 0 END AS is_wh_fee
, CASE WHEN bi.item_type IN (2,3,4,5) AND TRY(CAST(JSON_EXTRACT_SCALAR(bi.ext_info, '$.reason_code_id') AS INT)) IN (66) THEN 1 ELSE 0 END AS is_offset
FROM marketplace.shopee_order_accounting_settlement_th_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT DISTINCT CAST(account_id AS VARCHAR) AS account_id, CAST(rule_id AS VARCHAR) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_th_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS VARCHAR)
WHERE bi.is_cb = 1 AND bi.item_status IN (2) AND bi.item_type IN (1,2,3,4,5)
) AS sts
INNER JOIN fx ON fx.currency = sts.currency
GROUP BY 1,2
)
, audit AS (
SELECT *
FROM (
SELECT user_id
, CASE WHEN lower(audit_status) IN ('deleted', '0') THEN '0_DELETE'
WHEN lower(audit_status) IN ('banned', '2') THEN '2_BANNED'
WHEN lower(audit_status) IN ('frozen', '3') THEN '3_FROZEN'
ELSE NULL END AS audit_status
, reason
, FROM_UNIXTIME(action_timestamp) AS last_audit_datetime
, row_number() OVER (PARTITION BY user_id ORDER BY action_timestamp DESC) AS latest_rank
FROM (
SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_br_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_cl_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_co_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_es_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_id_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_mx_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_my_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_ph_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_pl_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_sg_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_th_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_tw_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT target_pk AS user_id
, JSON_EXTRACT_SCALAR(changes, '$.status[1]') AS audit_status
, JSON_EXTRACT_SCALAR(changes, '$.reason') AS reason
, CAST(action_time_msec AS DOUBLE) / 1000 AS action_timestamp
FROM marketplace.shopee_backend_log_vn_db__shopee_audit_log_tab__reg_continuous_s0_live
WHERE lower(JSON_EXTRACT_SCALAR(changes, '$.status[1]')) IN ('deleted', 'banned', 'frozen', '0', '2', '3')


UNION ALL


SELECT user_id
, CAST(new_status AS VARCHAR) AS audit_status
, CAST(tags AS VARCHAR) as reason
, ctime AS action_timestamp
FROM marketplace.shopee_fraud_treatment_center_db__treatmentcenter_user_account_oplog_tab__reg_daily_s0_live
WHERE grass_region != ''
AND new_status IN (1, 2, 3)
)
)
WHERE latest_rank = 1
)
SELECT wh.grass_region
, wh.shop_id
, s.user_id
, s.shop_name
, s.user_name
, s2.user_status
, cb.gp_account_name
, cb.gp_account_owner
, sts.balance_usd 
, sts.wh_handling_fee
, sts.wh_storage_fee
, sts.wh_fee
, sts.gp_offset
, o.L90D_ADO
, au.last_audit_datetime
, au.reason
FROM (
SELECT DISTINCT grass_region, shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date BETWEEN DATE_ADD('day', -15, current_date) AND DATE_ADD('day', -2, current_date)
) AS wh
INNER JOIN (
SELECT shop_id, user_id, shop_name, user_name
FROM mp_user.dim_shop__reg_s0_live
WHERE tz_type = 'local' AND is_cb_shop = 1
AND grass_region IN ('ID','MY','PH','SG','TH','VN','MX')
AND grass_date = DATE_ADD('day', -1, current_date)
) AS s ON s.shop_id = wh.shop_id
INNER JOIN (
SELECT DISTINCT shop_id
, CASE status WHEN 0 THEN '0_DELETE'
WHEN 1 THEN '1_NORMAL'
WHEN 2 THEN '2_BANNED'
WHEN 3 THEN '3_FROZEN'
ELSE NULL END AS user_status
FROM mp_user.dim_user__reg_s0_live
WHERE tz_type = 'local' AND is_cb_shop = 1
AND grass_region IN ('ID','MY','PH','SG','TH','VN','MX')
AND grass_date = DATE_ADD('day', -1, current_date)
) AS s2 ON s2.shop_id = wh.shop_id
LEFT JOIN seller_index AS cb ON cb.shop_id = wh.shop_id
LEFT JOIN sts ON sts.user_id = s.user_id
LEFT JOIN (
SELECT shop_id, CAST(COUNT(DISTINCT order_id) AS DOUBLE) / 90 AS L90D_ADO
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE tz_type = 'local' AND is_cb_shop = 1
AND grass_region IN ('ID','MY','PH','SG','TH','VN','MX')
AND grass_date >= DATE_ADD('day', -91, current_date)
AND DATE(split(create_datetime, ' ')[1]) BETWEEN DATE_ADD('day', -90, current_date) AND DATE_ADD('day', -1, current_date)
GROUP BY 1
) AS o ON o.shop_id = wh.shop_id
LEFT JOIN audit AS au ON (au.user_id = s.user_id AND au.audit_status = s2.user_status)