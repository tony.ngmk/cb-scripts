insert into nexus.test_c8997b2fcb28dd2753c6d7487327a209cf968158f8834d64c72cae19a5baaa8b_7aa2b7eafba1125e8bc0f794ac225b82 WITH
focus AS (
SELECT
map.affi_country
, map.affi_shopid
, map.affi_itemid
, a_i.item_name affi_item_name
, p_i.name mst_item_name
, map.mst_itemid
, map.mst_shopid
, sub_cat
, l30d_ado
FROM
((((((
SELECT
item_id
, name item_name
, shop_id
, level2_global_be_category_id sub_cat
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((((((status = 1) AND (shop_status = 1)) AND (seller_status = 1)) AND (stock > 0)) AND (is_holiday_mode = 0)) AND (is_cb_shop = 1)) AND (grass_region IN ('PH'))) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (tz_type = 'local'))
) a_i
INNER JOIN (
SELECT CAST(a_shopid AS bigint) affi_shopid
FROM
regcbbi_others.sip__hps_shop_id__df__reg__s0
) a_s ON (a_s.affi_shopid = a_i.shop_id))
INNER JOIN (
SELECT
affi_itemid
, affi_shopid
, mst_itemid
, mst_shopid
, affi_country
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE (affi_country = 'PH')
) map ON (a_i.item_id = map.affi_itemid))
INNER JOIN (
SELECT
item_id
, name
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((((((status = 1) AND (shop_status = 1)) AND (seller_status = 1)) AND (stock > 0)) AND (is_holiday_mode = 0)) AND (grass_region IN ('ID'))) AND (is_cb_shop = 0)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (tz_type = 'local'))
) p_i ON (p_i.item_id = map.mst_itemid))
LEFT JOIN (
SELECT itemid
FROM
marketplace.shopee_listing_qc_backend_ph_db__screening_qclog__reg_daily_s0_live
WHERE ((status IN (2, 4, 5)) AND (classification = 0))
) ex_i ON (ex_i.itemid = a_i.item_id))
LEFT JOIN (
SELECT
item_id
, ("sum"(order_fraction) / DECIMAL '30.00') l30d_ado
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE (((((is_placed = 1) AND (tz_type = 'local')) AND (is_cb_shop = 1)) AND (grass_region IN ('PH'))) AND (grass_date >= (current_date - INTERVAL '30' DAY)))
GROUP BY 1
) o ON (o.item_id = a_i.item_id))
WHERE (ex_i.itemid IS NULL)
) 
, ex AS (
SELECT DISTINCT
auditid audit_id
, itemid
, shopid
, country
, name upload_option
, new upload_content
, TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Operator')) operator
, TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Source')) source
, CAST(grass_date AS date) ctime
FROM
(marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
CROSS JOIN UNNEST(CAST(TRY("json_extract"(TRY("from_utf8"(data)), '$.Fields')) AS array(row(name varchar,old varchar,new varchar)))))
WHERE (((((TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Operator')) IN ('zhixian.toh@shopee.com')) OR (TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Source')) = 'Seller Center Single')) AND (name IN ('name'))) AND (country = 'PH')) AND (CAST(grass_date AS date) BETWEEN (current_date - INTERVAL '120' DAY) AND (current_date - INTERVAL '1' DAY)))
) 
, summary AS (
SELECT
f.affi_country
, f.affi_shopid
, f.affi_itemid
, "concat"('https://shopee.ph/product/', CAST(f.affi_shopid AS varchar), '/', CAST(f.affi_itemid AS varchar)) affi_product_link
, f.affi_item_name
, f.mst_item_name
, f.sub_cat
, f.l30d_ado affi_l30d_ado
, "concat"('http://shopee.co.id/product/', CAST(f.mst_shopid AS varchar), '/', CAST(f.mst_itemid AS varchar)) mst_product_link
FROM
((focus f
LEFT JOIN ex ON (ex.itemid = f.affi_itemid))
LEFT JOIN regcbbi_others.sip__listing_optimization_bau_exclude_list__wf__reg__s0 ex1 ON (CAST(ex1.affi_itemid AS bigint) = f.affi_itemid))
WHERE ((ex.itemid IS NULL) AND (ex1.affi_itemid IS NULL))
) 
SELECT
affi_country
, affi_shopid
, affi_itemid
, affi_product_link
, affi_item_name
, '' affi_description
, mst_product_link
, mst_item_name
, '' Priority
, '' Human_translated_title_local_language
, '' Keyword
, '' Human_translated_description_EN
, '' Machine_translation_score_Title
, '' Machine_translation_score_Description
, '' Title_length_Y_N
, '' Description_length_Y_N
, '' Confirm_to_upload_Y_N
, '' Remark_for_N_reason
, '' Other_remarks
, sub_cat
, affi_l30d_ado
, "rank"() OVER (PARTITION BY affi_country ORDER BY affi_l30d_ado DESC) ado_rank
FROM
summary
ORDER BY affi_country ASC, affi_l30d_ado DESC
LIMIT 1000