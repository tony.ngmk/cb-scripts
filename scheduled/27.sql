insert into nexus.test_a6df30db454640924f5a31e838cd1350f19934ad8bcf9e40a02e63c5c8740206_b8c955e7b18d702027599105d88e32e9 with seller as (
select
cb.user_name as shop_name,
shop_id as shopid,
cb.gp_name as gp_account_name,
cb.grass_region,
jp.gp_smt_pic,
jp.gp_smt_tier
from
dev_regcbbi_kr.jpcb_shop_profile cb
join 
regcbbi_others.shopee_regional_cb_team__jpcb_gp_allocation jp
on
cb.gp_name = jp.gp_name
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.jpcb_shop_profile
where
grass_region <> '')
and grass_region <> ''
and ingestion_timestamp in (select
max(ingestion_timestamp) as latest_timestamp
from 
regcbbi_others.shopee_regional_cb_team__jpcb_gp_allocation)
),
order_info as (
select 
distinct
o.shop_id
, o.seller_name
, o.item_id
, o.item_name
, o.model_id
, o.grass_region
, avg(o.item_price_pp) as item_price_local_currency
, sum(case when date(split(o.create_datetime, ' ')[1]) = date_add('day', -1, current_date) then item_amount end) as yesterday_item_sold
, count(distinct case when date(split(o.create_datetime, ' ')[1]) = date_add('day', -1, current_date) then order_id end) as yesterday_order_count
, sum(case when date(split(o.create_datetime, ' ')[1]) = date_add('day', -1, current_date) then gmv_usd end) as yesterday_gmv_usd
, sum(case when date(split(o.create_datetime, ' ')[1]) between date_add('day', -30, current_date) and date_add('day', -1, current_date) then item_amount end) as L30D_item_sold
, count(distinct case when date(split(o.create_datetime, ' ')[1]) between date_add('day', -30, current_date) and date_add('day', -1, current_date) then order_id / 30.00 end) as L30D_ado
, sum(case when date(split(o.create_datetime, ' ')[1]) between date_add('day', -30, current_date) and date_add('day', -1, current_date) then gmv_usd / 30.00 end) as L30D_adgmv
from
mp_order.dwd_order_item_all_ent_df__reg_s0_live o
join
seller s
on
o.shop_id = s.shopid
where
o.tz_type = 'local'
and o.grass_region in ('SG','MY','TW','TH','PH','ID')
and o.is_bi_excluded = 0
and o.grass_date >= date_add('day', -31, current_date)
and date(split(o.create_datetime, ' ')[1]) between date_add('day', -30, current_date) and date_add('day', -1, current_date)
group by 1,2,3,4,5,6
),
item as (
select
distinct
o.item_id
, o.shop_id
, i.stock
, case
when i.grass_region = 'MY' then concat('https://shopee.com.my/product/',cast(o.shop_id as varchar),'/',cast(o.item_id as varchar))
when i.grass_region = 'ID' then concat('https://shopee.co.id/product/',cast(o.shop_id as varchar),'/',cast(o.item_id as varchar))
when i.grass_region = 'PH' then concat('https://shopee.ph/product/',cast(o.shop_id as varchar),'/',cast(o.item_id as varchar))
when i.grass_region = 'SG' then concat('https://shopee.sg/product/',cast(o.shop_id as varchar),'/',cast(o.item_id as varchar))
when i.grass_region = 'TH' then concat('https://shopee.co.th/product/',cast(o.shop_id as varchar),'/',cast(o.item_id as varchar))
when i.grass_region = 'TW' then concat('https://shopee.tw/product/',cast(o.shop_id as varchar),'/',cast(o.item_id as varchar))
end as product_url
from
mp_item.dim_item__reg_s0_live i
join
order_info o
on
i.item_id = o.item_id
where
tz_type = 'local'
and i.grass_region in ('SG','MY','TW','TH','PH','ID')
and i.grass_date = date_add('day', -1, current_date)
and i.is_cb_shop = 1
),
model_name as (
select
o.item_id
, o.model_id
, m.model_name
from
mp_item.dim_model__reg_s0_live m
join
order_info o
on 
m.item_id = o.item_id
and m.model_id = o.model_id
where
m.tz_type = 'local'
and m.grass_region in ('SG','MY','TW','TH','PH','ID')
and m.grass_date = date_add('day', -1, current_date)
and m.is_cb_shop = 1
)


select
distinct
o.grass_region
, o.shop_id
, o.seller_name
, o.item_id
, o.item_name
, o.item_price_local_currency
, o.model_id
, m.model_name
, coalesce(i.stock, 0) item_stock
, case when i.stock = 0 then date_add('day', -1, current_date) end as oos_date
, L30D_item_sold 
, i.product_url
, gp_smt_tier
, gp_smt_pic
from
order_info o
join
item i
on
o.item_id = i.item_id
join
seller s
on
s.shopid = o.shop_id
join 
model_name m
on
o.model_id = m.model_id
where 
o.L30D_item_sold >=10
and o.L30D_ado >=1
and o.L30D_adgmv >=1
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14
order by 9,1,2