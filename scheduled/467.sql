-- RR order > =3 and RR compensation rate >= 10% and shop rr% > 10% OR Total COD failed >=3 and COD compensation rate >= 10%
WITH
o AS (


SELECT DISTINCT
order_id orderid 
, shop_id
, order_sn 
, payment_method_id
, seller_name
, is_cod_order
, "date"("split"(create_datetime, ' ')[1]) create_time
, "date"("split"(cancel_datetime, ' ')[1]) cancel_time
, order_be_status_id status_ext
, "date"("split"(complete_datetime, ' ')[1]) complete_time
, gmv_usd
, grass_region
, is_cb_shop
, buyer_name
, buyer_id 
, order_be_status
, logistics_status 
, order_fe_status_id
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ( (tz_type = 'local')) and date(create_datetime) between current_date-interval'70'day and current_date-interval'10'day 
and grass_date >= current_date-interval'70'day 
and grass_region in ('PH','TH','VN','ID','VN')
-- GROUP BY 1, 2, 3, 4, 5, 6, 7, 8


) 
, ofm as (select distinct order_id 
, lm_first_failed_delivery_attempt_message
, lm_second_failed_delivery_attempt_message
, date(lm_first_attempt_delivery_datetime) lm_first_attempt_delivery_datetime
from mp_ofm.dwd_forder_all_ent_df__reg_s0_live
where grass_region in ('PH','TH','VN') and is_cb_shop=1 and grass_date >= current_date-interval'70'day
)


, ex1 as (select order_id 
, lm_first_failed_delivery_attempt_message
, lm_second_failed_delivery_attempt_message
from ofm 
where (( lm_first_failed_delivery_attempt_message like '%J&T%') or ( lm_second_failed_delivery_attempt_message like '%J&T%'))
)
, sip AS (
SELECT DISTINCT
t2.affi_shopid
, mst_shopid
, t1.country mst_country
, t2.country affi_country
, t1.cb_option
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE ((sip_shop_status <> 3) AND (GRASS_REGION IN ('PH','TH','VN')))
) t2 ON (t1.shopid = t2.mst_shopid))
WHERE (t1.cb_option = 0) and t1.country in ('ID','VN')
) 
, cod_FAILED AS (
SELECT DISTINCT
orderid
, shopid
, date(from_unixtime(min(ctime))) cod_failed_date 
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_continuous_s0_live A
WHERE (new_status IN (6)) 


AND (GRASS_REGION IN ('PH','TH','VN') )


group by 1,2 
having date(from_unixtime(min(ctime))) >= (current_date - INTERVAL '70' DAY)


) 


--IDPH: 48006
--IDTH: 78020
--IDVN: 58010


--VNPH: 48007


--THPH: 48009


--MYPH: 48010




, cod_enable as (
select shopid 
, cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.48006.cod_enabled') AS boolean) idph
, cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.78020.cod_enabled') AS boolean) idth 
, cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.58010.cod_enabled') AS boolean) idvn
, cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.48007.cod_enabled') AS boolean) vnph
, cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.48009.cod_enabled') AS boolean) thph
, cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.48010.cod_enabled') AS boolean) myph
from marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live 
where grass_region in ('PH','TH','VN') and (cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.48006.cod_enabled') AS boolean)=True
or cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.78020.cod_enabled') AS boolean)=True
or cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.58010.cod_enabled') AS boolean)=True
or cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.48007.cod_enabled') AS boolean)=True 
or cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.48009.cod_enabled') AS boolean)=True 
or cast(json_extract(try(json_parse(from_utf8(extinfo.logistics_info))), '$.channels.48010.cod_enabled') AS boolean)=True 
)


)


, raw AS (
SELECT DISTINCT
oversea_orderid
, "max"(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
where date(from_unixtime(ctime)) >= current_date-interval'70'day and local_country IN ('id','vn')
and oversea_country in ('vn','ph','th')
GROUP BY 1
) 


, r as 
(SELECT distinct 

o.order_sn as affi_ordersn 
, o2.order_sn as mst_ordersn 
, sip.affi_shopid 
, sip.mst_shopid 
, o2.seller_name mst_username 
, o.seller_name as affi_username 
, sip.affi_country
, sip.mst_country 
, o.order_be_status as affi_order_be_status 
, o.logistics_status as affi_logistics_status 
, o2.order_be_status as mst_order_be_status 
, o2.logistics_status as mst_logistics_status 
, buyer_name
, buyer_id 
, o.create_time order_create_date 
, ofm.lm_first_attempt_delivery_datetime
, cod_failed_date
, ofm.lm_first_failed_delivery_attempt_message
, ofm.lm_second_failed_delivery_attempt_message
, "count"( (CASE WHEN (payment_method_id = 6) THEN o.orderid ELSE null END)) over (partition by sip.affi_shopid ) COD_ORDERS
, "count"( (CASE WHEN ((l.orderid IS NOT NULL) AND (payment_method_id = 6)) THEN o.orderid ELSE null END)) over (partition by sip.affi_shopid ) cod_failed_order
FROM
(select distinct 
shop_id 
, date(from_unixtime(shop_first_listing_timestamp)) shop_first_listing_timestamp
, shop_level1_category
, active_item_with_stock_cnt
, case when grass_region = 'SG' THEN concat('https://shopee.sg/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='PH' THEN concat('https://shopee.ph/shop/',CAST(shop_id as VARCHAR)) 
when grass_region ='TW' THEN concat('https://shopee.tw/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='ID' THEN concat('https://shopee.co.id/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='TH' THEN concat('https://shopee.co.th/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='VN' THEN concat('https://shopee.vn/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='MY' THEN concat('https://shopee.com.my/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='BR' THEN concat('https://shopee.com.br/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='MX' THEN concat('https://shopee.com.mx/shop/',CAST(shop_id as VARCHAR)) 
end as shop_link 
, user_status
, is_holiday_mode
, status
, grass_region
from regcbbi_others.shop_profile
where grass_date=current_date-interval'1'day and grass_region in ('PH','TH','VN') 
and user_status=1 and is_holiday_mode=0 and status = 1 and is_cb_shop=1 
) u 
join cod_enable ce on ce.shopid = u.shop_id 
INNER JOIN (select * 
from o
where is_cb_shop=1 and grass_region in ('PH','TH','VN') 
) o ON (o.shop_id = u.shop_id )
join sip on sip.affi_shopid = u.shop_id 


join raw on raw.oversea_orderid=o.orderid 
join(select orderid 
, gmv_usd
, seller_name
, order_be_status
, logistics_status
, order_sn 
from o
where is_cb_shop=0 and grass_region in ('ID','VN')and order_fe_status_id != 4 
) o2 on o2.orderid = raw.local_orderid
LEFT JOIN (
SELECT DISTINCT
shop_id
, (placed_order_cnt_30d / DECIMAL '30.00') l30d_ado
FROM
mp_order.dws_seller_gmv_nd__reg_s0_live
WHERE (((grass_date = (current_date - INTERVAL '2' DAY)) AND (tz_type = 'local'))
AND (grass_region IN ('PH','TH','VN') ))
) ld ON (ld.shop_id = sip.affi_shopid )
LEFT JOIN cod_FAILED l ON (l.orderid = o.orderid)
left join ex1 on ex1.order_id = raw.oversea_orderid 
left join ofm on ofm.order_id = o.orderid 
where ex1.order_id is null and o.payment_method_id=6
--and (case when sip.mst_country ='ID' and sip.affi_country ='PH' and idph = True then true
-- when sip.mst_country ='ID' and sip.affi_country ='VN' and idvn = True then true
-- when sip.mst_country ='ID' and sip.affi_country ='TH' and idTH = True then true 
--when sip.mst_country ='VN' and sip.affi_country ='PH' and vnph = True then true
--when sip.mst_country ='TH' and sip.affi_country ='PH' and thph = True then true 
--when sip.mst_country ='MY' and sip.affi_country ='PH' and myph = True then true 
-- else false end 
--)
)




select * 
from r 
where cod_failed_date is not null and COD_ORDERS>=2 and try(cod_failed_order*decimal'1.000'/cod_orders)>=0.15 
--and r.affi_shopid= 3969274