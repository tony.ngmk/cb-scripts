WITH sip AS (
SELECT affi_shopid
, mst_shopid
, t1.country as mst_country 
, t2.country as affi_country 
, t1.cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status!=3 AND grass_region IN ('TW')
) t2 ON t1.shopid = t2.mst_shopid
) 


, t1 AS (
SELECT DISTINCT 
auditid AS audit_id
, itemid
, shopid
, country 
, name upload_option
, new AS upload_content
, try(json_extract_scalar(try(from_utf8(data)),'$.Operator')) AS operator
, try(json_extract_scalar(try(from_utf8(data)),'$.Source')) AS source
, CAST(grass_date as date) ctime
FROM 
marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
CROSS JOIN UNNEST(
CAST(
try(json_extract(try(from_utf8(data)),'$.Fields')) AS ARRAY(
ROW(name VARCHAR, old VARCHAR, new VARCHAR)
)
)
)
WHERE (try(json_extract_scalar(try(from_utf8(data)),'$.Operator')) IN ('zhixian.toh@shopee.com'
----vn
-- ,'thuytien.nguyen@shopee.com','hongngoc.bui@shopee.com','linh.thai@shopee.com','nhi.nimtuyet@shopee.com', 'bichtruc.ly@shopee.com','kaiwen.zhang@shopee.com'
---- my
-- ,'jooyee.ong@shopee.com', 'nurulnadia.salim@shopee.com', 'elise.seak@shopee.com'
) 
OR try(json_extract_scalar(try(from_utf8(data)),'$.Source')) = 'Seller Center Single')
AND name IN ('name','modelid')
AND country = 'TW' --change countries
AND CAST(grass_date as date) BETWEEN current_date - interval '60' day AND current_date - interval '1' day 
)


, t2 AS (
SELECT t1.* 
FROM t1 
INNER JOIN (
SELECT itemid, upload_option, max(ctime) ctime, max_by(t1.audit_id,ctime) audit_id
FROM t1
LEFT JOIN (SELECT audit_id FROM t1 WHERE upload_option = 'modelid') t1_2 ON t1.audit_id = t1_2.audit_id
WHERE t1_2.audit_id IS NULL
GROUP BY 1,2
) b ON t1.itemid = b.itemid AND t1.upload_option = b.upload_option AND t1.ctime = b.ctime AND t1.audit_id = b.audit_id
)


SELECT current_date update_date
, t2.country affi_country
, t2.shopid 
, t2.itemid 
, t2.upload_option
, t2.upload_content
, i.name overwrite_content
, i.item_status
, sip.cb_option
FROM t2
INNER JOIN (
SELECT item_id
, shop_id
, name
, status item_status 
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region = 'TW'
AND grass_date = current_date - interval '1' day 
AND is_holiday_mode = 0 
AND shop_status = 1 
AND seller_status = 1
AND is_cb_shop = 1
AND tz_type = 'local'
AND status = 1 
-- AND status IN (0,3)
) i ON t2.itemid = i.item_id
INNER JOIN sip ON sip.affi_shopid = i.shop_id
WHERE t2.upload_content <> i.name