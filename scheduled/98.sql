WITH shops AS (
SELECT
a.shop_id
, COALESCE(mtd_sku_count, 0) AS mtd_sku_count
, COALESCE(m1_sku_count, 0) AS m1_sku_count
FROM
(SELECT
DISTINCT CAST(shop_id AS BIGINT) AS shop_id
FROM
regcbbi_others.commercial__ust_tracker__tmp__reg__s0
WHERE
ingestion_timestamp = (
SELECT 
MAX(ingestion_timestamp) AS max_ingest
FROM 
regcbbi_others.commercial__ust_tracker__tmp__reg__s0
)
) a
LEFT JOIN 
(SELECT
DISTINCT shop_id
, COUNT(DISTINCT CASE WHEN grass_date = current_date - interval '1' day THEN item_id END) AS mtd_sku_count
, COUNT(DISTINCT CASE WHEN grass_date = date_trunc('month' , current_date - interval '1' day) - interval '1' day THEN item_id END) AS m1_sku_count
FROM
mp_item.dim_item__reg_s0_live
WHERE
tz_type = 'local'
AND is_cb_shop IS NOT NULL
AND (grass_date = date_trunc('month' , current_date - interval '1' day) - interval '1' day OR grass_date = current_date - interval '1' day)
AND stock > 0 
AND status = 1
AND seller_status = 1 
AND shop_status = 1 
AND is_holiday_mode = 0
GROUP BY
1
) b
ON
a.shop_id = b.shop_id
),


live_shop_mtd AS (
SELECT
DISTINCT shop_id
, 'Y' AS mtd_record
FROM
mp_user.dws_user_login_td_account_info__reg_s0_live
WHERE
tz_type = 'local'
AND grass_date = current_date - interval '1' day
AND DATE(from_unixtime(last_login_timestamp_td)) >= current_date - interval '6' day
),


live_shop_m1 AS (
SELECT
DISTINCT shop_id
, 'Y' AS m1_record
FROM
mp_user.dws_user_login_td_account_info__reg_s0_live
WHERE
tz_type = 'local'
AND grass_date = date_trunc('month', current_date - interval '1' day) - interval '1' day
AND DATE(from_unixtime(last_login_timestamp_td)) >= (date_trunc('month', current_date - interval '1' day) - interval '1' day) - interval '6' day

),


shop_stats AS (
SELECT
a.shop_id
, mtd_sku_count
, m1_sku_count
, CASE WHEN mtd_sku_count > 0 AND mtd_record = 'Y' THEN 'Y' ELSE 'N' END AS mtd_live_shop
, CASE WHEN m1_sku_count > 0 AND m1_record = 'Y' THEN 'Y' ELSE 'N' END AS m1_live_shop
FROM
shops a
LEFT JOIN
live_shop_mtd b
ON
a.shop_id = b.shop_id
LEFT JOIN
live_shop_m1 c
ON
a.shop_id = c.shop_id
),


third_pf_shops AS (
SELECT 
DISTINCT CAST(shop_id AS BIGINT) AS shop_id 
FROM 
regcb_bi.cbwh_3pf_shop_list 
WHERE 
ingestion_timestamp = (SELECT 
MAX(ingestion_timestamp) AS max_ingest
FROM 
regcb_bi.cbwh_3pf_shop_list
)
AND shop_id != ''
),


non_3pf_shop AS (
SELECT 
DISTINCT CAST(shop_id AS BIGINT) AS shop_id 
FROM 
regcbbi_others.commercial__ust_tracker__tmp__reg__s0 a
WHERE 
ingestion_timestamp = (SELECT 
MAX(ingestion_timestamp) AS max_ingest 
FROM 
regcbbi_others.commercial__ust_tracker__tmp__reg__s0
)
AND NOT EXISTS (SELECT 
DISTINCT shop_id 
FROM 
regcb_bi.cbwh_3pf_shop_list b
WHERE 
ingestion_timestamp = (SELECT 
MAX(ingestion_timestamp) 
FROM 
regcb_bi.cbwh_3pf_shop_list
)
AND b.shop_id = a.shop_id
AND b.shop_id != ''
AND a.shop_id != ''
)
),


shop_orders_gmv AS (
SELECT
shop_id
--local ado
, CAST(COUNT(DISTINCT CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month AND date_trunc('month', current_date - interval '1' day) - interval '1' day THEN order_id END) AS DOUBLE) / DAY(date_trunc('month', current_date - interval '1' day) - interval '1' day) AS local_m1_ado
, CAST(COUNT(DISTINCT CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) AND current_date - interval '1' day THEN order_id END) AS DOUBLE) / DAY(current_date - interval '1' day) AS local_mtd_ado
, CAST(COUNT(DISTINCT CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '21' day AND date_trunc('week', date_trunc('week', date_trunc('week', current_date - interval '1' day) - interval '1' day) - interval '1' day) - interval '1' day THEN order_id END) AS DOUBLE) / 7 AS local_w3_ado
, CAST(COUNT(DISTINCT CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '14' day AND date_trunc('week', date_trunc('week', current_date - interval '1' day) - interval '1' day) - interval '1' day THEN order_id END) AS DOUBLE) / 7 AS local_w2_ado
, CAST(COUNT(DISTINCT CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '7' day AND date_trunc('week', current_date - interval '1' day) - interval '1' day THEN order_id END) AS DOUBLE) / 7 AS local_w1_ado
, CAST(COUNT(DISTINCT CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) AND current_date - interval '1' day THEN order_id END) AS DOUBLE) / DAY_OF_WEEK(current_date - interval '1' day) AS local_wtd_ado
, CAST(COUNT(DISTINCT CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) = current_date - interval '1' day THEN order_id END) AS DOUBLE) AS local_d1_order

--local gmv
, SUM(CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month AND date_trunc('month', current_date - interval '1' day) - interval '1' day THEN gmv_usd END) / DAY(date_trunc('month', current_date - interval '1' day) - interval '1' day) AS local_m1_adg_usd
, SUM(CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) AND current_date - interval '1' day THEN gmv_usd END) / DAY(current_date - interval '1' day) AS local_mtd_adg_usd
, SUM(CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '21' day AND date_trunc('week', date_trunc('week', date_trunc('week', current_date - interval '1' day) - interval '1' day) - interval '1' day) - interval '1' day THEN gmv_usd END) / 7 AS local_w3_adg_usd
, SUM(CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '14' day AND date_trunc('week', date_trunc('week', current_date - interval '1' day) - interval '1' day) - interval '1' day THEN gmv_usd END) / 7 AS local_w2_adg_usd
, SUM(CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '7' day AND date_trunc('week', current_date - interval '1' day) - interval '1' day THEN gmv_usd END) / 7 AS local_w1_adg_usd
, SUM(CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) AND current_date - interval '1' day THEN gmv_usd END) / DAY_OF_WEEK(current_date - interval '1' day) AS local_wtd_adg_usd
, SUM(CASE WHEN 
(fulfilment_source = 'FULFILLED_BY_SHOPEE' OR shop_id IN (SELECT DISTINCT shop_id FROM third_pf_shops))
AND DATE(from_unixtime(create_timestamp)) = current_date - interval '1' day THEN gmv_usd END) AS local_d1_adg_usd


--warehouse ado
, CAST(COUNT(DISTINCT CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month AND date_trunc('month', current_date - interval '1' day) - interval '1' day THEN order_id END) AS DOUBLE) / DAY(date_trunc('month', current_date - interval '1' day) - interval '1' day) AS warehouse_m1_ado
, CAST(COUNT(DISTINCT CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) AND current_date - interval '1' day THEN order_id END) AS DOUBLE) / DAY(current_date - interval '1' day) AS warehouse_mtd_ado
, CAST(COUNT(DISTINCT CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '21' day AND date_trunc('week', date_trunc('week', date_trunc('week', current_date - interval '1' day) - interval '1' day) - interval '1' day) - interval '1' day THEN order_id END) AS DOUBLE) / 7 AS warehouse_w3_ado
, CAST(COUNT(DISTINCT CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '14' day AND date_trunc('week', date_trunc('week', current_date - interval '1' day) - interval '1' day) - interval '1' day THEN order_id END) AS DOUBLE) / 7 AS warehouse_w2_ado
, CAST(COUNT(DISTINCT CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '7' day AND date_trunc('week', current_date - interval '1' day) - interval '1' day THEN order_id END) AS DOUBLE) / 7 AS warehouse_w1_ado
, CAST(COUNT(DISTINCT CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) AND current_date - interval '1' day THEN order_id END) AS DOUBLE) / DAY_OF_WEEK(current_date - interval '1' day) AS warehouse_wtd_ado
, CAST(COUNT(DISTINCT CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) = current_date - interval '1' day THEN order_id END) AS DOUBLE) AS warehouse_d1_order

--warehouse gmv
, SUM(CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month AND date_trunc('month', current_date - interval '1' day) - interval '1' day THEN gmv_usd END) / DAY(date_trunc('month', current_date - interval '1' day) - interval '1' day) AS warehouse_m1_adg_usd
, SUM(CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) AND current_date - interval '1' day THEN gmv_usd END) / DAY(current_date - interval '1' day) AS warehouse_mtd_adg_usd
, SUM(CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '21' day AND date_trunc('week', date_trunc('week', date_trunc('week', current_date - interval '1' day) - interval '1' day) - interval '1' day) - interval '1' day THEN gmv_usd END) / 7 AS warehouse_w3_adg_usd
, SUM(CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '14' day AND date_trunc('week', date_trunc('week', current_date - interval '1' day) - interval '1' day) - interval '1' day THEN gmv_usd END) / 7 AS warehouse_w2_adg_usd
, SUM(CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) - interval '7' day AND date_trunc('week', current_date - interval '1' day) - interval '1' day THEN gmv_usd END) / 7 AS warehouse_w1_adg_usd
, SUM(CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('week', current_date - interval '1' day) AND current_date - interval '1' day THEN gmv_usd END) / DAY_OF_WEEK(current_date - interval '1' day) AS warehouse_wtd_adg_usd
, SUM(CASE WHEN 
fulfilment_source = 'FULFILLED_BY_CB_SELLER' 
AND shop_id IN (SELECT DISTINCT shop_id FROM non_3pf_shop)
AND DATE(from_unixtime(create_timestamp)) = current_date - interval '1' day THEN gmv_usd END) AS warehouse_d1_adg_usd
FROM
(SELECT
s.shop_id
, order_id
, SUM(gmv_usd) AS gmv_usd
, create_timestamp
, fulfilment_source
FROM
mp_order.dwd_order_place_pay_complete_di__reg_s0_live o
RIGHT JOIN 
(SELECT
DISTINCT CAST(shop_id AS BIGINT) AS shop_id
FROM
regcbbi_others.commercial__ust_tracker__tmp__reg__s0
WHERE
ingestion_timestamp = (
SELECT 
MAX(ingestion_timestamp) AS max_ingest
FROM 
regcbbi_others.commercial__ust_tracker__tmp__reg__s0
)
) s 
ON 
o.shop_id = s.shop_id
WHERE
DATE(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month AND current_date - interval '1' day
AND tz_type = 'local'
AND is_placed = 1
AND (is_bi_excluded = 0 OR is_bi_excluded IS NULL)
GROUP BY
1, 2, 4, 5
)
GROUP BY
1
)


SELECT
a.shop_id
, mtd_live_shop
, mtd_sku_count
, m1_live_shop
, m1_sku_count
--local
, COALESCE(local_m1_ado, 0) local_m1_ado
, COALESCE(local_m1_adg_usd, 0) local_m1_adg_usd
, COALESCE(local_mtd_ado, 0) local_mtd_ado
, COALESCE(local_mtd_adg_usd, 0) local_mtd_adg_usd
, COALESCE(local_w3_ado, 0) local_w3_ado
, COALESCE(local_w3_adg_usd, 0) local_w3_adg_usd
, COALESCE(local_w2_ado, 0) local_w2_ado
, COALESCE(local_w2_adg_usd, 0) local_w2_adg_usd
, COALESCE(local_w1_ado, 0) local_w1_ado
, COALESCE(local_w1_adg_usd, 0) local_w1_adg_usd
, COALESCE(local_wtd_ado, 0) local_wtd_ado
, COALESCE(local_wtd_adg_usd, 0) local_wtd_adg_usd 
, COALESCE(local_d1_order, 0) local_d1_order
, COALESCE(local_d1_adg_usd, 0) local_d1_adg_usd
--warehouse
, COALESCE(warehouse_m1_ado, 0) warehouse_m1_ado
, COALESCE(warehouse_m1_adg_usd, 0) warehouse_m1_adg_usd
, COALESCE(warehouse_mtd_ado, 0) warehouse_mtd_ado
, COALESCE(warehouse_mtd_adg_usd, 0) warehouse_mtd_adg_usd
, COALESCE(warehouse_w3_ado, 0) warehouse_w3_ado
, COALESCE(warehouse_w3_adg_usd, 0) warehouse_w3_adg_usd
, COALESCE(warehouse_w2_ado, 0) warehouse_w2_ado
, COALESCE(warehouse_w2_adg_usd, 0) warehouse_w2_adg_usd
, COALESCE(warehouse_w1_ado, 0) warehouse_w1_ado
, COALESCE(warehouse_w1_adg_usd, 0) warehouse_w1_adg_usd
, COALESCE(warehouse_wtd_ado, 0) warehouse_wtd_ado
, COALESCE(warehouse_wtd_adg_usd, 0) warehouse_wtd_adg_usd
, COALESCE(warehouse_d1_order, 0) warehouse_d1_order
, COALESCE(warehouse_d1_adg_usd, 0) warehouse_d1_adg_usd
FROM 
shop_stats a
LEFT JOIN
shop_orders_gmv b
ON
a.shop_id = b.shop_id