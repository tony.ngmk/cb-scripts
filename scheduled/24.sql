insert into nexus.test_9561c619fde7fe708261d278969bf795aa16a2ecd580e174f017e3d3dc383453_ec95f29fe2b9b9320dc9d645b519422e with 
sip AS (SELECT distinct affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT DISTINCT affi_shopid
, affi_username
, mst_shopid 
, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status!=3 AND GRASS_REGION IN ('SG','MY','ID','PH','TH','VN','TW','BR','MX','CO','CL','PL','ES','FR')
) t2 ON t1.shopid = t2.mst_shopid
WHERE CB_OPTION = 1 
)

select distinct mst_country 
, affi_country 
, shop_level1_category
, affi_shopid 
, L30D_ADO
, L30D_CFS_ADO
, gmv_usd L30D_ADGMV_USD 
, abs
, live_sku
, U.user_status affi_status 
, u2.user_status mst_status 
, u.is_holiday_mode affi_hm 
, u2.is_holiday_mode mst_hm 
, shop_link
, V.REASON AS AFFI_REASON 
, V2.reason AS MST_REASON 
, v.username as affi_operator
from sip 
join (Select distinct 
shop_id 
, shop_level1_category
, user_status
, active_item_with_stock_cnt live_sku 
, case when grass_region = 'SG' THEN concat('https://shopee.sg/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='PH' THEN concat('https://shopee.ph/shop/',CAST(shop_id as VARCHAR)) 
when grass_region ='TW' THEN concat('https://shopee.tw/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='ID' THEN concat('https://shopee.co.id/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='TH' THEN concat('https://shopee.co.th/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='VN' THEN concat('https://shopee.vn/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='MY' THEN concat('https://shopee.com.my/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='BR' THEN concat('https://shopee.com.br/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='MX' THEN concat('https://shopee.com.mx/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='CO' THEN concat('https://shopee.com.CO/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='CL' THEN concat('https://shopee.com.CL/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='PL' THEN concat('https://shopee.PL/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='ES' THEN concat('https://shopee.ES/shop/',CAST(shop_id as VARCHAR)) 
WHEN grass_region='FR' THEN concat('https://shopee.FR/shop/',CAST(shop_id as VARCHAR)) 
end as shop_link 
, is_holiday_mode
from regcbbi_others.shop_profile
where grass_date = current_date-interval'1'day and is_cb_shop=1 and (user_status in (2,3) or is_holiday_mode=1 ) AND grass_region IN ('SG','MY','ID','PH','TH','VN','TW','BR','MX','CO','CL','PL','ES','FR') ) u 
ON U.SHOP_ID = SIP.AFFI_SHOPID 

join (select distinct shop_id 
, user_status
, is_holiday_mode
from regcbbi_others.shop_profile
where grass_date = current_date-interval'1'day and is_cb_shop=1 and grass_region in ('TW','MY','BR','SG')
) U2 ON U2.SHOP_ID = SIP.MST_SHOPID 

LEFT JOIN (SELECT DISTINCT SHOPID 
, REASON 
, username
FROM regcbbi_others.ops_shop_violation_reason

) V ON V.SHOPID = SIP.AFFI_SHOPID 
LEFT JOIN (SELECT DISTINCT SHOPID 
, REASON 
FROM regcbbi_others.ops_shop_violation_reason

) V2 ON V2.SHOPID = SIP.MST_SHOPID 
JOIN (SELECT SHOP_ID 
, count(distinct order_sn)/30.00 L30D_ADO 
, count(distinct case when flash_sale_type_id<2 and is_flash_sale=1 then order_sn else null end )/30.00 L30D_CFS_ADO 
, sum(gmv)/30.00 gmv 
, sum(gmv_usd)/30.00 gmv_usd
, TRY( sum(gmv_usd) * DECIMAL'1.000'/count(distinct order_sn) ) abs 
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where date(split(create_datetime,' ')[1]) between current_date-interval'30'day and current_date-interval'1'day and is_cb_shop=1 
and grass_region in ('SG','MY','ID','PH','TH','VN','TW','BR','MX','CO','CL','PL','ES','FR') 
GROUP BY 1 
) o on o.shop_id= sip.affi_shopid