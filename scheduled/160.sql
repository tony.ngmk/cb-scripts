INSERT INTO regcbbi_general.cbwh_stock_history_copy 
SELECT * FROM (
SELECT
grass_date,
grass_region,
cast(shop_id as bigint) as shop_id,
shop_whs_id,
username,
ggp_account_name,
ggp_account_owner,
sku_id,
whs_id,
itemid,
modelid,
cast(model_stock AS bigint) as model_stock,
cast(item_stock as bigint) as item_stock,
model_status,
item_status,
damaged_stock,
missing_stock,
normal_stock
FROM 
regcbbi_general.cbwh_stock_history_v2 
)