insert into nexus.test_3020ab65425f308209af8f4ffefd88a65881f609a0fc8c6793770bf0b78ea575_c6ec01d38c3f196e262cec31d44c4260 WITH
shop_details2 AS (
SELECT DISTINCT
shop_id shopid
, user_name
FROM
mp_user.dim_shop__my_s0_live
WHERE ((grass_date = "date_add"('day', -1, current_date)))
) 


, wh_sku_info as (
SELECT 
shop_id as shopid,
cast(mp_sku_item_id as bigint) as mp_sku_item_id, 
sum(b.stock_on_hand_qty_1d) as wh_stock, -- wh stock
-- sum(whs_available_qty) as whs__available_qty, -- total warehouse stock 
sum(sellable_qty) as whs__isc_sellable_qty, -- warehouse sellable stock 
-- sum(fe_stock_qty_1d) as fe_stock_qty_1d, -- same with item_mart (fe display stock) affected by promotion
-- count(distinct cast(mp_sku_item_id as bigint)) as fbs_sku_cnt,
max(mp_purchasable_days_30d) as mp_purchasable_days_30d,
sum(mp_gross_itemsold_qty_30d) as mp_gross_itemsold_qty_30d


FROM sbs_mart.ads_retail_and_fbs_summary_mpsku_1d_my as a
left join sbs_mart.ads_retail_and_fbs_summary_mtsku_1d_my as b on a.mt_sku_id = b.mt_sku_id and a.grass_date = b.grass_date
left join isc_mart.dwd_general_stock_df_my d on a.mt_sku_id = d.mt_sku_id and a.grass_date = d.grass_date
WHERE 1=1 -- shop_id IN (229907985)
and a.grass_date = current_date - interval '1' day
-- and is_sbs = 1
-- and is_pff_shop = 1
and is_virtual_sku = 0
and stock_on_hand_qty_1d > 0 
and fe_stock_qty_1d > 0 
and is_normal_status = 1 
-- and is_live = 1
group by 1,2
)


, item_details AS (
SELECT DISTINCT
a.item_id itemid
, a.shop_id shopid
, a.name
, "concat"('https://shopee.com.my/product/', CAST(a.shop_id AS varchar), '/', CAST(a.item_id AS varchar)) item_link
, case when coalesce(TRY(regexp_replace(fe_display_categories[1][1],'[0-9]+:','')),'Others') in ('Health & Beauty','Baby & Toys','Groceries & Pets') then 'FMCG'
when coalesce(TRY(regexp_replace(fe_display_categories[1][1],'[0-9]+:','')),'Others') in ('Mobile & Accessories' , 'Computer & Accessories' , 'Gaming & Consoles','Home Appliances','Cameras & Drones') then 'Electronics'
when coalesce(TRY(regexp_replace(fe_display_categories[1][1],'[0-9]+:','')),'Others') in ('Women Clothes','Women''s Bags','Women Shoes','Men Clothes','Fashion Accessories','Muslim Fashion','Watches','Men''s Bags & Wallets','Men Shoes','Travel & Luggage') then 'Fashion'
when coalesce(TRY(regexp_replace(fe_display_categories[1][1],'[0-9]+:','')),'Others') in ('Home & Living','Automotive','Sports & Outdoor','Games, Books & Hobbies') then 'Lifestyle'
else 'Others'
end as cluster
, coalesce(TRY(regexp_replace(fe_display_categories[1][1],'[0-9]+:','')),'Others') as level1_category
, a.rating_score
, a.stock
, a.price
, TRY("concat"('http://cf.shopee.com.my/file/', "split_part"(images, ',', 1))) image_link
FROM mp_item.dim_item__my_s0_live as a
left join mp_item.dim_item_ext__my_s0_live as b on a.item_id = b.item_id and a.grass_date = b.grass_date 
WHERE a.grass_date = "date_add"('day', -1, current_date)
) 


, traffic AS (
SELECT DISTINCT
item_id as itemid
, ("sum"(IF((grass_date BETWEEN "date_trunc"('month', current_date) AND "date_add"('day', -1, current_date)), CAST(pv_cnt_1d AS double), null)) / "day"("date_add"('day', -1, current_date))) mtd_item_views
, ("sum"(IF((grass_date BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))), CAST(pv_cnt_1d AS double), null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m1_item_views
, ("sum"(IF((grass_date BETWEEN "date_add"('month', -2, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_add"('month', -1, "date_trunc"('month', current_date)))), CAST(pv_cnt_1d AS double), null)) / "day"("date_add"('day', -1, "date_add"('month', -1, "date_trunc"('month', current_date))))) m2_item_views
FROM
mp_shopflow.dws_item_civ_1d__my_s0_live
WHERE ((grass_date >= "date_add"('month', -2, "date_trunc"('month', current_date))))
GROUP BY 1
) 
, orders AS (
SELECT DISTINCT
oi.item_id itemid
, oi.shop_id shopid
, ("sum"(IF(("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('week', -1, "date_trunc"('week', current_date)) AND "date_add"('day', -1, "date_trunc"('week', current_date))), order_fraction, null)) / 7) w_1_ado
, ("sum"(IF(("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('week', -2, "date_trunc"('week', current_date)) AND "date_add"('day', -1, "date_add"('week', -1, "date_trunc"('week', current_date)))), order_fraction, null)) / 7) w_2_ado
, ("sum"(IF(("date"("from_unixtime"(create_timestamp)) BETWEEN "date_trunc"('month', current_date) AND "date_add"('day', -1, current_date)), order_fraction, null)) / "day"("date_add"('day', -1, current_date))) mtd_ado
, ("sum"(IF(("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))), order_fraction, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_ado
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))) AND (is_flash_sale = 1)) AND (flash_sale_type_id in (0,1))), item_amount, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_item_sold_cfs
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))) AND (campaign IS NOT NULL)) AND (is_flash_sale = 0)), item_amount, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_item_sold_cam
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))) AND (campaign IS NULL)) AND (is_flash_sale = 0)), item_amount, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_item_sold_org
, ("sum"(IF(("date"("from_unixtime"(create_timestamp)) BETWEEN "date_trunc"('month', current_date) AND "date_add"('day', -1, current_date)), item_amount, null)) / "day"("date_add"('day', -1, current_date))) mtd_item_sold
, ("sum"(IF(("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))), item_amount, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_item_sold
, ("sum"(IF(("date"("from_unixtime"(create_timestamp)) BETWEEN "date_trunc"('month', current_date) AND "date_add"('day', -1, current_date)), gmv, null)) / "day"("date_add"('day', -1, current_date))) mtd_adgmv
, ("sum"(IF(("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))), gmv, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_adgmv
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_trunc"('month', current_date) AND "date_add"('day', -1, current_date)) AND (is_flash_sale = 1)) AND (flash_sale_type_id in (0,1))), order_fraction, null)) / "day"("date_add"('day', -1, current_date))) mtd_ado_cfs
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))) AND (is_flash_sale = 1)) AND (flash_sale_type_id in (0,1))), order_fraction, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_ado_cfs
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_trunc"('month', current_date) AND "date_add"('day', -1, current_date)) AND (campaign IS NULL)) AND (is_flash_sale = 0)), order_fraction, null)) / "day"("date_add"('day', -1, current_date))) mtd_ado_org
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))) AND (campaign IS NULL)) AND (is_flash_sale = 0)), order_fraction, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_ado_org
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_trunc"('month', current_date) AND "date_add"('day', -1, current_date)) AND (campaign IS NOT NULL)) AND (is_flash_sale = 0)), order_fraction, null)) / "day"("date_add"('day', -1, current_date))) mtd_ado_cam
, ("sum"(IF(((("date"("from_unixtime"(create_timestamp)) BETWEEN "date_add"('month', -1, "date_trunc"('month', current_date)) AND "date_add"('day', -1, "date_trunc"('month', current_date))) AND (campaign IS NOT NULL)) AND (is_flash_sale = 0)), order_fraction, null)) / "day"("date_add"('day', -1, "date_trunc"('month', current_date)))) m_1_ado_cam

, avg(case when date(from_unixtime(create_timestamp)) = current_date - interval '1' day then order_price_pp end) as D1_price
, avg(case when date(from_unixtime(create_timestamp)) between current_date - interval '7' day and current_date - interval '1' day then order_price_pp end) as L7D_price
, avg(case when date(from_unixtime(create_timestamp)) between current_date - interval '30' day and current_date - interval '1' day then order_price_pp end) as L30D_avg_price
, min(case when date(from_unixtime(create_timestamp)) between current_date - interval '30' day and current_date - interval '1' day then order_price_pp end) as L30D_min_price
FROM
(
SELECT distinct
oi.*
, IF((cam.item_id IS NULL), null, 1) campaign
FROM
((
SELECT 
item_id,
shop_id,
create_timestamp,
order_fraction,
item_amount,
gmv,
is_flash_sale,
flash_sale_type,
order_price_pp,
fulfilment_source,
is_cb_shop,
flash_sale_type_id
FROM
mp_order.dwd_order_item_all_ent_df__my_s0_live
where grass_date > "date_add"('month', -2, "date_trunc"('month', current_date))
and grass_date > "date_add"('month', -2, "date_trunc"('month', current_date))
and is_bi_excluded = 0
and is_cb_shop = 1
) oi
LEFT JOIN (
SELECT DISTINCT
collection_id
, item_id
, start_time 
, end_time 
FROM
mybi_general.campaign_tracker_my_daily_campaign_item
where grass_date > "date_add"('month', -2, "date_trunc"('month', current_date))
) cam ON ((oi.item_id = cam.item_id) AND (oi.create_timestamp BETWEEN cam.start_time AND cam.end_time)))
WHERE ((((is_cb_shop = 1) 
and oi.shop_id not in (445279067) -- lovito 
AND (fulfilment_source = 'FULFILLED_BY_SHOPEE' or fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER')) 
AND ("date"("from_unixtime"(create_timestamp)) >= "date_add"('month', -2, "date_trunc"('month', current_date)))))
) oi
GROUP BY 1, 2
) 


, whs_shops AS (
SELECT
DISTINCT shop_id,
pff_tag
FROM mybi_general.cbwh_shop_history_v2
WHERE grass_date = current_date - INTERVAL '1' DAY
)




, agg as ( 
SELECT DISTINCT 
pff_tag
-- case when fbs_mode = 0 then 1 else 0 end as pff_status -- fbs_mode = 0 (pff) 1 (fff) 
, o.shopid
, user_name
, o.itemid
, i.name
, image_link
, item_link
, cluster
, level1_category
-- , rating_score
, cast(wh_stock as varchar) as whs_stock_on_hand
-- , cast(whs__available_qty as varchar) as whs__available_qty


, try(mp_gross_itemsold_qty_30d / mp_purchasable_days_30d) as l30_selling_speed 
, try(wh_stock / (mp_gross_itemsold_qty_30d / mp_purchasable_days_30d)) as coverage_days 


, mtd_ado
, m_1_ado
-- , IF(((w_2_ado IS NULL) OR (w_2_ado = 0)), null, (w_1_ado / w_2_ado)-1) wow_ado
, mtd_ado / m_1_ado - 1 * 1.000 as mom_ado


, w_1_ado
, w_2_ado
, w_1_ado / w_2_ado - 1 * 1.000 as wow_ado


, mtd_ado_org
, mtd_ado_cfs
, mtd_ado_cam


, mtd_adgmv
, m_1_adgmv


, price


-- , IF(((w_2_ado IS NULL) OR (w_2_ado = 0)), null, (w_1_ado / w_2_ado)-1) wow_ado




-- , mtd_item_views

-- , m_1_ado_org
-- , m_1_ado_cfs
-- , m_1_ado_cam

-- , m1_item_views
-- , m_1_item_sold
-- , m_1_item_sold_org
-- , m_1_item_sold_cfs
-- , m_1_item_sold_cam
-- , D1_price
-- , L7D_price

, L30D_min_price
, L30D_avg_price


FROM orders o
LEFT JOIN shop_details2 st ON (o.shopid = st.shopid)
LEFT JOIN item_details i ON (o.itemid = i.itemid)
LEFT JOIN traffic t ON (o.itemid = t.itemid) 
left join whs_shops as s on o.shopid = s.shop_id
-- left join sbs_mart.shopee_scbs_db__shop_tab__my_daily_s0_live s on o.shopid = s.shop_id
-- left join pure_wh on o.shopid = pure_wh.shop_id
left join wh_sku_info as wh on o.shopid = wh.shopid and o.itemid = wh.mp_sku_item_id
-- ORDER BY mtd_ado DESC
-- limit 200
)


, ranking as (
SELECT 
row_number() over (order by mtd_ado DESC) as rank
, agg.*
FROM agg 
)


SELECT * FROM ranking 
where rank <=200