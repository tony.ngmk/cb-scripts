insert into nexus.test_3f34122ef22702e8716eadfbff5f126dbba132cef99f8de96222a5b504d922f5_48de59b1087c79e15dd20a618812eaa7 WITH top_cat as 
(SELECT grass_region 
, level1_global_be_category as top_l1_cat
, level2_global_be_category as top_l2_cat
, top_cat_ranking
FROM
(
SELECT grass_region
, level1_global_be_category
, level2_global_be_category
, gmv 
, rank() over (partition by grass_region order by gmv desc) as top_cat_ranking
FROM
(
SELECT grass_region 
, level1_global_be_category
, level2_global_be_category
, sum(gmv_usd) as gmv 
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live 
WHERE tz_type = 'local'
AND grass_date >= date_add('day', -14, current_date)
AND DATE(from_unixtime(create_timestamp)) between date_add('day', -14, current_date) and date_add('day', -1, current_date) 
AND is_cb_shop = 1 
AND grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
GROUP BY 1,2,3 
)
) 
WHERE top_cat_ranking <= 20 
and level2_global_be_category not like 'Others'
)








, keyword as 
(
select 
grass_region as site
, main_category
, sub_category
--, keyword
--, W1_daily_search
--, W2_daily_search
--, week_daily_search_uplift
, (case when W2_daily_search < 100 then '飙升引流词'
else '飙升选品词' end) as uplift_type
, keyword
--, cate_global_search_clicks
--, bidding_items
--, ads_CPC
--, ads_ROI
, ads_gmv_usd
, rank() over (partition by grass_region, main_category, sub_category order by ads_gmv_usd desc) as gmv_ranking
from
(
select *
, rank() over (partition by grass_region, main_category, sub_category order by week_daily_search_uplift desc) as uplift_ranking
from
(
select
sch.grass_region
, sch.keyword
, sch.W1_daily_search
, sch.W2_daily_search
, cast(sch.W1_daily_search as double) / sch.W2_daily_search - 1 as week_daily_search_uplift -- coalesce what??
, main_category
, sub_category
, coalesce(ads.bidding_items, 0) as bidding_items
, coalesce(CPC, 99999) as ads_CPC
, coalesce(ROI, 99999) as ads_ROI
, coalesce(cate_global_search_clicks, 0) as cate_global_search_clicks
, ads_gmv_usd
from
(
SELECT
grass_region
, keywords as keyword
, sum(ads_gmv_amt_usd) as ads_gmv_usd 
, sum(expenditure_amt_usd)/sum(click_cnt) as CPC
, sum(ads_gmv_amt_usd)/sum(expenditure_amt_usd) as ROI
, count(distinct item_id) as bidding_items
from mp_paidads.dws_advertise_query_gmv_event_1d__reg_s0_live
where tz_type = 'local' 
AND grass_date between date_add('day', -7, current_date) and date_add('day', -1, current_date)
and grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
group by 1, 2
) ads
right join
------------- keyword daily search volume
(
select
grass_region
, keyword
, cast(sum(IF(grass_date between date_add('day', -7, current_date) and date_add('day', -1, current_date), srp_uv_1d, 0)) as double)/7 as W1_daily_search
, cast(sum(IF(grass_date between date_add('day', -14, current_date) and date_add('day', -8, current_date), srp_uv_1d, 0)) as double)/7 as W2_daily_search
from
(
SELECT grass_region, grass_date, keyword, srp_uv_1d
FROM mp_search.dws_keyword_view_imp_clk_1d__reg_s1_live
WHERE grass_date between date_add('day', -14, current_date) and date_add('day', -1, current_date)
AND grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
)
group by 1, 2
) sch on ads.grass_region = sch.grass_region and ads.keyword = sch.keyword
left join
-------- Link keyword to subcate/cate
(
select *
from
(
select *, rank() over (partition by grass_region, keyword order by cate_global_search_clicks desc) as ranking
from
(
select 
i.grass_region
, i.main_category
, i.sub_category
, keyword
, sum(clicks)/7 as cate_global_search_clicks
from
(
select distinct 
grass_region
, item_id as itemid
, level1_global_be_category as main_category
, level2_global_be_category as sub_category 
from mp_item.dim_item__reg_s0_live 
where is_cb_shop = 1
and tz_type = 'local'
and grass_date = date_add('day', -1, current_date)
and grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
and level1_global_be_category is not null
) i
join
(
select 
grass_region
, keyword
, item_id as itemid
, count(event_id) as clicks
from mp_search.dwd_srp_item_click_di__reg_s1_live
where grass_date between date_add('day', -7, current_date) and date_add('day', -1, current_date)
and grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
and domain = 'global_search'
--and clk_sortby = 'relevancy'
group by 1, 2, 3
) t on i.grass_region = t.grass_region and i.itemid = t.itemid
group by 1, 2, 3, 4
)
)
where ranking <= 5 -- each keyword can be mapped to no more than 5 cates in 1 market
) cat on ads.grass_region = cat.grass_region and ads.keyword = cat.keyword
where 
(case when sch.grass_region = 'ID' then W1_daily_search >= 50
else W1_daily_search >= 20
end)
and main_category is not null
and sch.keyword != ''
and sch.keyword is not null
)
)
where 
(case when W2_daily_search < 50 then week_daily_search_uplift > 2
when W2_daily_search >= 50 and W2_daily_search < 100 then week_daily_search_uplift > 1
else week_daily_search_uplift > 0.5
end)
AND (bidding_items <= 10 OR ads_ROI >= 2)
ORDER BY 1, 2, 3, 4, 5
) 


SELECT k.site 
, k.main_category 
, k.sub_category 
, k.uplift_type 
, k.keyword 
FROM keyword k
LEFT JOIN top_cat c on k.site = c.grass_region and k.main_category = c.top_l1_cat and k.sub_category = c.top_l2_cat
where gmv_ranking <= 100 
AND c.top_l2_cat is not null