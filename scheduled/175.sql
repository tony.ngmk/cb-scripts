/********************************** 
Ingested Table Name: 
regcbbi_general.negative_escrow_project_tracker_bfd_summary 
**********************************/ 
--Get action_time_msec
WITH audit as
(SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_sg_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_id_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_my_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_tw_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_th_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_ph_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_vn_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_br_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_mx_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_co_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_cl_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_pl_db__shopee_audit_log_tab__reg_daily_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_es_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT target_pk, json_extract_scalar(changes, '$.status[1]') as audit_status, json_extract_scalar(changes, '$.reason') as reason, action_time_msec from marketplace.shopee_backend_log_fr_db__shopee_audit_log_tab__reg_continuous_s0_live WHERE json_extract_scalar(changes, '$.status[1]') in ('deleted', 'banned', 'frozen', '0', '2', '3')
UNION SELECT user_id AS target_pk, case when new_status = 1 then 'normal' when new_status = 2 then 'banned' when new_status = 3 then 'frozen' else null end as audit_status, remarks as reason, ctime * 1000 as action_time_msec FROM marketplace.shopee_fraud_treatment_center_db__treatmentcenter_user_account_oplog_tab__reg_daily_s0_live WHERE grass_region != '' AND new_status IN (1, 2, 3)
),


--Get users which status are in B/F/D
user as
(SELECT grass_region, user_id, user_name, shop_id, account_status, account_status_reason, account_status_date
FROM
(SELECT grass_region, user_id, user_name, shop_id, account_status, audit_status, lower(reason) as account_status_reason, date(from_unixtime(action_time_msec/1000)) as account_status_date,
rank() over (partition by target_pk, audit_status order by action_time_msec desc) as action_time_rank
FROM audit a
JOIN
(SELECT distinct grass_region, user_id, user_name, shop_id, IF(status = 0, 'deleted', IF(status = 2, 'banned', 'frozen')) as account_status
FROM mp_user.dim_user__reg_s0_live
WHERE is_cb_shop = 1 AND grass_date = date_add('day', -1, current_date)
AND tz_type = 'local' AND status in (0,2,3)
AND grass_region != ''
) u
ON target_pk = u.user_id
)
WHERE audit_status = account_status
AND action_time_rank = 1
)


--Could be omitted as no segregation is required for this excercise
, sip AS (
SELECT affi_shopid
, t1.cb_option
, t1.country AS psite
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1 
JOIN (
SELECT distinct affi_shopid, affi_userid, affi_username, mst_shopid, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status != 3
AND grass_region != ''
) AS t2 on t1.shopid = t2.mst_shopid
)
, seller_index AS (
SELECT CAST(si.child_shopid as BIGINT) as shop_id
, si.gp_account_name
, si.gp_account_id
, si.gp_account_owner
, si.child_account_owner
, si.gp_account_owner_userrole_name
, si.child_account_owner_userrole_name
, si.gp_account_owner_email
, si.child_userid
, CASE WHEN sip.affi_shopid is not NULL THEN 1 ELSE 0 END as is_sip
, CASE WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
--WHEN cb_option = 0 THEN 'Local'
WHEN sip.affi_shopid IS NOT NULL AND sip.cb_option = 0 THEN 'Local SIP'
--WHEN sip.affi_shopid IS NOT NULL AND sip.psite IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
--WHEN sip.affi_shopid IS NOT NULL AND si.gp_account_billing_country = 'Korea' AND sip.psite IN ('SG') THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL THEN 'CB SIP'
when si.gp_account_billing_country = 'Korea' then 'KRCB'
when si.gp_account_billing_country = 'Japan' then 'JPCB'
when si.gp_account_billing_country is NULL or si.gp_account_billing_country != 'China' then 'Others'
when si.child_account_owner_userrole_name like '%HKCB%' then 'HKCB'
when si.child_account_owner_userrole_name like '%BD_%' then 'CNCB BD'
when si.child_account_owner_userrole_name like '%UST_%' then 'CNCB UST'
when si.child_account_owner_userrole_name like '%ST_%' THEN 'CNCB ST'
when si.child_account_owner_userrole_name like '%MT_%' THEN 'CNCB MT'
when si.child_account_owner_userrole_name like '%LT%' THEN 'CNCB LT'
WHEN si.gp_account_billing_country = 'China' THEN COALESCE(CONCAT('CNCB ', si.cb_group), 'CNCB Others')
else 'CNCB Others' END as seller_type
, gp_account_billing_country
FROM regbd_sf.cb__seller_index_tab AS si 
LEFT JOIN sip ON CAST(si.child_shopid AS BIGINT) = sip.affi_shopid
LEFT JOIN (
SELECT DISTINCT shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date BETWEEN DATE_ADD('day', -15, current_date) AND DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = CAST(si.child_shopid as BIGINT)
WHERE si.grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
),


--Further fine-tuning the user profile
user_agg AS
(select
user.shop_id,
user.user_id,
IF(user.account_status = 'deleted', 'Deleted Account', seller_index.seller_type) as seller_type,
IF(user.account_status = 'deleted', 'Deleted Account', seller_index.gp_account_name) as gp_account_name,
IF(user.account_status = 'deleted', 'Deleted Account', seller_index.gp_account_owner_email) as gp_account_owner_email,
user.user_name as shop_name,
user.grass_region as region,
user.account_status,
user.account_status_reason,
case when regexp_like(user.account_status_reason,'prohibit|counterfeit|fake') then 'Counterfeit / Fake / Prohibited'
when regexp_like(user.account_status_reason,'Inactive') then 'Inactive'
when regexp_like(user.account_status_reason,'scam|fraud') then 'Scam / Fraud'
when regexp_like(user.account_status_reason,'low price|brush') then 'Low Price / Order Brushing'
when regexp_like(user.account_status_reason,'gp.*?limit|limit.*?gp') then 'GP Limit'
when regexp_like(user.account_status_reason,'shadow') then 'Shadow Account'
when regexp_like(user.account_status_reason,'kyc') then 'KYC'
when regexp_like(user.account_status_reason,'seller request') then 'Seller Request'
else 'Others' end
as account_status_reason_processed,
user.account_status_date as account_status_changed_date,
date_diff('day', user.account_status_date, date_add('day', -1, current_date)) as account_status_days,
seller_index.gp_account_billing_country
from user join seller_index on user.user_id = CAST(seller_index.child_userid AS BIGINT)
),


--Filter date
escrow_verified_date_before_2019_aug as
(SELECT order_id, date(from_unixtime(escrow_verified_timestamp)) as ev_date
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE tz_type = 'local'
AND grass_region != ''
AND is_cb_shop = 1
AND escrow_verified_timestamp <= to_unixtime(date'2019-08-31')
),


--Exchange rates
fx as
(SELECT DISTINCT currency, local_to_usd, usd_to_cny, usd_to_krw, usd_to_jpy
FROM
(SELECT currency, grass_date, exchange_rate as local_to_usd FROM mp_order.dim_exchange_rate__reg_s0_live WHERE grass_date = date_add('day',-1,current_date)) fx_usd
JOIN
(SELECT 1 / exchange_rate as usd_to_cny, grass_date FROM mp_order.dim_exchange_rate__reg_s0_live WHERE grass_date = date_add('day',-1,current_date) AND currency = 'CNY') fx_cny
ON fx_usd.grass_date = fx_cny.grass_date
JOIN
(SELECT 1 / exchange_rate as usd_to_krw, grass_date FROM mp_order.dim_exchange_rate__reg_s0_live WHERE grass_date = date_add('day',-1,current_date) AND currency = 'KRW') fx_krw
ON fx_usd.grass_date = fx_krw.grass_date
JOIN
(SELECT 1 / exchange_rate as usd_to_jpy, grass_date FROM mp_order.dim_exchange_rate__reg_s0_live WHERE grass_date = date_add('day',-1,current_date) AND currency = 'JPY') fx_jpy
ON fx_usd.grass_date = fx_jpy.grass_date
)


--Main Query
(SELECT
current_date AS report_extraction,


COUNT(DISTINCT user_agg.shop_id) num_bfd_shops,


COUNT(DISTINCT user_agg.gp_account_name) num_bfd_gp,


COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0 
THEN user_agg.shop_id ELSE NULL END) num_bfd_neg_bal_shops,


CAST(COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0 
THEN user_agg.shop_id ELSE NULL END) AS DOUBLE)/
CAST(COUNT(DISTINCT user_agg.shop_id) AS DOUBLE) neg_bal_shops_vs_bfd_shops,


COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0
THEN user_agg.gp_account_name ELSE NULL END) num_bfd_neg_bal_gp,


CAST(COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0
THEN user_agg.gp_account_name ELSE NULL END) AS DOUBLE)/
CAST(COUNT(DISTINCT user_agg.gp_account_name) AS DOUBLE) neg_bal_gp_vs_bfd_gp, 


SUM(CASE WHEN coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0) < 0 
THEN coalesce(esc_agg.total_escrow_USD_amt, 0) + coalesce(adj_agg.total_adjustment_USD_amt, 0) ELSE 0 END) neg_bal_bfd_usd,


CAST(SUM(CASE WHEN coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0) < 0 
THEN coalesce(esc_agg.total_escrow_USD_amt, 0) + coalesce(adj_agg.total_adjustment_USD_amt, 0) ELSE 0 END) AS DOUBLE)/
(SELECT sum(o.gmv_usd) as L30D_GMV_USD 
FROM mp_user.dim_user__reg_s0_live u JOIN mp_order.dwd_order_all_ent_df__reg_s0_live o
ON u.shop_id = o.shop_id
WHERE DATE(SPLIT(o.create_datetime,' ')[1]) between date_add('day',-30,current_date) and date_add('day',-1,current_date)
AND o.tz_type = 'local'
AND o.grass_region <> ''
AND o.is_cb_shop = 1
AND u.status = 1
AND u.grass_date = current_date - interval '1' day) neg_bal_vs_l30d_gmv,


COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0 
AND account_status_days > 182 THEN user_agg.shop_id ELSE NULL END) num_neg_bal_shops_threshold,


CAST(COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0 
AND account_status_days > 182 THEN user_agg.shop_id ELSE NULL END) AS DOUBLE)/
CAST(COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0 
THEN user_agg.shop_id ELSE NULL END) AS DOUBLE) threshold_vs_neg_bal_shops,


COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0 
AND account_status_days > 182 THEN user_agg.gp_account_name ELSE NULL END) num_neg_bal_gp_threshold,


CAST(COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0 
AND account_status_days > 182 THEN user_agg.gp_account_name ELSE NULL END) AS DOUBLE)/
CAST(COUNT(DISTINCT CASE WHEN (coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0)) < 0 
THEN user_agg.gp_account_name ELSE NULL END) AS DOUBLE) threshold_vs_neg_bal_gp,


SUM(CASE WHEN coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0) < 0 AND account_status_days > 182 
THEN coalesce(esc_agg.total_escrow_USD_amt, 0) + coalesce(adj_agg.total_adjustment_USD_amt, 0) ELSE 0 END) neg_bal_usd_threshold,


CAST(SUM(CASE WHEN coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0) < 0 
AND account_status_days > 182 THEN coalesce(esc_agg.total_escrow_USD_amt, 0) + coalesce(adj_agg.total_adjustment_USD_amt, 0) ELSE 0 END) AS DOUBLE)/
CAST(SUM(CASE WHEN coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0) < 0 
THEN coalesce(esc_agg.total_escrow_USD_amt, 0) + coalesce(adj_agg.total_adjustment_USD_amt, 0) ELSE 0 END) AS DOUBLE) threshold_vs_neg_bal_amt




from user_agg
left join
(select target_account_id,
esc.currency,
max(billing_item_created_date) as last_billing_item_created_date,
min(case when esc.billing_item_created_date > date'2019-08-31' then esc.billing_item_created_date else ev.ev_date end) as earliest_billing_item_created_date,
sum(cast(escrow_amount_inc as double) / 100000) as total_escrow_local_amt,
sum(cast(escrow_amount_inc as double) / 100000 / IF(esc.currency = 'USD', 1, local_to_usd)) as total_escrow_USD_amt
from regcbbi_others.weekly_clawback_tracker_bfd_esc_reg_s0_tmp esc
join fx
on esc.currency = fx.currency
left join escrow_verified_date_before_2019_aug ev
on cast(esc.escrow_entity_id as bigint) = ev.order_id
group by 1,2
) esc_agg
on user_agg.user_id = esc_agg.target_account_id
left join
(select adj.target_account_id,
adj.currency,
max(billing_item_created_date) as last_billing_item_created_date,
min(case when adj.billing_item_created_date > date'2019-08-31' or ev.ev_date is null then adj.billing_item_created_date else ev.ev_date end) as earliest_billing_item_created_date,
sum(cast(adjustment_amount as double) / 100000) as total_adjustment_local_amt,
sum(cast(adjustment_amount as double) / 100000 / if(adj.currency = 'USD', 1, local_to_usd)) as total_adjustment_USD_amt
from regcbbi_others.weekly_clawback_tracker_bfd_adj_reg_s0_tmp adj
join fx
on adj.currency = fx.currency
left join escrow_verified_date_before_2019_aug ev
on cast(adj.escrow_entity_id as bigint) = ev.order_id
group by 1,2
) adj_agg
on user_agg.user_id = adj_agg.target_account_id
left join fx
on coalesce(esc_agg.currency, adj_agg.currency) = fx.currency


--where coalesce(esc_agg.total_escrow_local_amt, 0) + coalesce(adj_agg.total_adjustment_local_amt, 0) < 0
GROUP BY 1)