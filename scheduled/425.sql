insert into nexus.test_463e1da6a3e544ead4d38760b1a5c3ff60aa485842b5cbdd27f73cbbf476c583_5ba1e46413a3a905d2d2fcbb249760e3 -- 4. Top 5 SKUs per market


WITH cb AS (
SELECT
distinct
shop_id AS shopid
, user_name as username
from
dev_regcbbi_kr.jpcb_shop_profile
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.jpcb_shop_profile
where
grass_region <> '')
and grass_region <> ''
),
item AS
(SELECT distinct item_id, name AS item_name
FROM mp_item.dim_item__reg_s0_live i
JOIN mp_user.dim_shop__reg_s0_live s
ON i.shop_id = s.shop_id
JOIN cb
ON i.shop_id = cb.shopid
WHERE s.is_cb_shop = 1
AND i.is_cb_shop = 1
AND i.grass_region in ('SG', 'TW', 'MY', 'TH', 'ID', 'PH')
AND s.grass_region in ('SG', 'TW', 'MY', 'TH', 'ID', 'PH')
AND i.grass_date = date_add('day', -1, current_date)
AND s.grass_date = date_add('day', -1, current_date)
AND i.tz_type = 'local' 
AND s.tz_type = 'local'
),
yst_rank AS
(SELECT *
, CASE
WHEN grass_region = 'SG' then concat('https://shopee.sg/product/', cast(shopid AS VARCHAR), '/', cast(itemid AS VARCHAR))
WHEN grass_region = 'MY' then concat('https://shopee.com.my/product/', cast(shopid AS VARCHAR), '/', cast(itemid AS VARCHAR))
WHEN grass_region = 'TW' then concat('https://shopee.tw/product/', cast(shopid AS VARCHAR), '/', cast(itemid AS VARCHAR))
WHEN grass_region = 'TH' then concat('https://shopee.co.th/product/', cast(shopid AS VARCHAR), '/', cast(itemid AS VARCHAR))
WHEN grass_region = 'ID' then concat('https://shopee.co.id/product/', cast(shopid AS VARCHAR), '/', cast(itemid AS VARCHAR))
WHEN grass_region = 'PH' then concat('https://shopee.ph/product/', cast(shopid AS VARCHAR), '/', cast(itemid AS VARCHAR))
ELSE NULL
END AS link
FROM 
(SELECT DISTINCT o.item_id itemid
, o.grass_region
, i.item_name
, o.shop_id shopid
, cb.username AS seller_name
, placed_order_cnt_1d AS gross_order
, gmv_usd_1d AS gmv_usd
, gmv_usd_1d / item_amount_1d AS price 
, row_number() over (partition BY o.grass_region ORDER BY placed_order_cnt_1d DESC) AS ranking
FROM mp_order.dws_item_gmv_1d__reg_s0_live o
JOIN cb 
ON o.shop_id = cb.shopid 
LEFT JOIN item i 
ON o.item_id = i.item_id
WHERE o.tz_type = 'local'
AND o.grass_region in ('SG', 'TW', 'MY', 'TH', 'ID', 'PH')
AND o.grass_date = date_add('day', -1, current_date)
)
WHERE ranking <= 5
),
cumu_rank AS 
(SELECT *
, CASE
WHEN grass_region = 'SG' then concat('https://shopee.sg/product/', cast(shop_id AS VARCHAR), '/', cast(item_id AS VARCHAR))
WHEN grass_region = 'MY' then concat('https://shopee.com.my/product/', cast(shop_id AS VARCHAR), '/', cast(item_id AS VARCHAR))
WHEN grass_region = 'TW' then concat('https://shopee.tw/product/', cast(shop_id AS VARCHAR), '/', cast(item_id AS VARCHAR))
WHEN grass_region = 'TH' then concat('https://shopee.co.th/product/', cast(shop_id AS VARCHAR), '/', cast(item_id AS VARCHAR))
WHEN grass_region = 'ID' then concat('https://shopee.co.id/product/', cast(shop_id AS VARCHAR), '/', cast(item_id AS VARCHAR))
WHEN grass_region = 'PH' then concat('https://shopee.ph/product/', cast(shop_id AS VARCHAR), '/', cast(item_id AS VARCHAR))
ELSE NULL
END AS link
FROM
(SELECT *
, row_number() over (partition BY grass_region ORDER BY gross_order DESC, net_order DESC, gmv_usd DESC, net_gmv DESC) AS ranking
FROM
(SELECT DISTINCT o1.grass_region
, o1.item_id
, i.item_name
, o1.shop_id 
, o1.username AS seller_name
, o1.placed_order_cnt_td AS gross_order
, o1.gmv_usd_td AS gmv_usd
, o2.net_order
, o2.net_gmv
, (o1.gmv_usd_td / o1.item_amount_td) AS price 
FROM 
(SELECT *
FROM mp_order.dws_item_gmv_td__reg_s0_live oi
JOIN cb
ON oi.shop_id = cb.shopid
WHERE oi.grass_region in ('SG', 'TW', 'MY', 'TH', 'ID', 'PH')
AND oi.grass_date = date_add('day', -1, current_date)
AND oi.tz_type = 'local' 
) o1
LEFT JOIN
(SELECT DISTINCT item_id 
, count(distinct order_id) AS net_order
, sum(gmv_usd) AS net_gmv
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live oi
JOIN cb
ON oi.shop_id = cb.shopid
WHERE oi.grass_region in ('SG', 'TW', 'MY', 'TH', 'ID', 'PH')
AND oi.is_net_order = 1
AND oi.tz_type = 'local' 
GROUP BY 1
) o2
ON o1.item_id = o2.item_id
LEFT JOIN item i
ON o1.item_id = i.item_id
)
)
WHERE ranking <= 5
)
SELECT cumu_rank.grass_region AS country 
, cumu_rank.ranking 
, coalesce(yst_rank.item_name, 'N/A') AS item_name
, coalesce(yst_rank.link, 'N/A') AS link
, coalesce(cast(yst_rank.price AS VARCHAR), 'N/A') AS price
, coalesce(yst_rank.seller_name, 'N/A') AS seller_name
, coalesce(yst_rank.gross_order, 0) AS yesterday_total_orders
, cumu_rank.item_name AS acc_item_name
, cumu_rank.link AS acc_link
, cumu_rank.price AS acc_price
, cumu_rank.seller_name AS acc_seller_name
, cumu_rank.gross_order AS total_orders
FROM yst_rank
RIGHT JOIN cumu_rank
ON yst_rank.grass_region = cumu_rank.grass_region
AND yst_rank.ranking = cumu_rank.ranking
ORDER BY CASE cumu_rank.grass_region
WHEN 'SG' THEN 1
WHEN 'MY' THEN 2
WHEN 'TW' THEN 3
WHEN 'TH' THEN 4
WHEN 'ID' THEN 5
WHEN 'PH' THEN 6
END
, cumu_rank.ranking