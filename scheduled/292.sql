insert into nexus.test_d0894fa9ce8c1b1bb3aa7ddfcd153e14571bc9d753603a2ef84762332543d848_95cf68086726f61031dbc66e4a4447db WITH
sip_shop AS (
SELECT DISTINCT b.affi_shopid
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b ON (b.mst_shopid = a.shopid))
WHERE a.cb_option = 0 AND a.country = 'TW' and b.grass_region != ''
) 
, t1 AS (
SELECT
a.grass_region country
, a.orderid
, a.ordersn
, from_unixtime(b.ctime) status_time
, CASE 
WHEN a.status_ext = 0 THEN 'DELETE' 
WHEN b.new_status = 1 THEN 'UNPAID' 
WHEN b.new_status = 2 THEN 'PAID' 
WHEN b.new_status = 3 THEN 'SHIPPED' 
WHEN b.new_status = 4 THEN 'COMPLETED' 
WHEN b.new_status = 5 THEN 'CANCEL' 
WHEN b.new_status = 6 THEN 'INVALID' 
WHEN b.new_status = 7 THEN 'CANCEL_PROCESSING' 
WHEN b.new_status = 8 THEN 'CANCEL_COMPLETED' 
WHEN b.new_status = 9 THEN 'RETURN_PROCESSING' 
WHEN b.new_status = 10 THEN 'RETURN_COMPLETED' 
WHEN b.new_status = 11 THEN 'ESCROW_PAID' 
WHEN b.new_status = 12 THEN 'ESCROW_CREATED' 
WHEN b.new_status = 13 THEN 'ESCROW_PENDING' 
WHEN b.new_status = 14 THEN 'ESCROW_VERIFIED' 
WHEN b.new_status = 15 THEN 'ESCROW_PAYOUT' 
WHEN b.new_status = 16 THEN 'CANCEL_PENDING' 
ELSE null END backend_status
, row_number() OVER (PARTITION BY a.orderid ORDER BY b.ctime DESC) rn
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live a
LEFT JOIN marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_daily_s0_live b ON (b.orderid = a.orderid)
JOIN sip_shop ON sip_shop.affi_shopid = a.shopid
WHERE date(from_unixtime(b.ctime)) >= date_add('day', -2, current_date) 
AND NOT b.new_status IN (11, 12, 13, 14, 15, 1, 2, 4) 
AND a.grass_region IN ('TW', 'SG', 'MY', 'ID')
) 
SELECT *
FROM
t1
WHERE (rn = 1)