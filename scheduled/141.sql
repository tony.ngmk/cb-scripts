With daily_orders as(Select grass_region
,month
,if(month >= date_trunc('month',date_add('day',-1,current_date)),day(date_add('day',-1,current_date)),day(last_day_of_month(month))) as days 
,sum(platform_orders) as platform_orders
,sum(cb_orders) as cb_orders
,sum(platform_gmv) as platform_gmv
,sum(cb_gmv) as cb_gmv 
from
(SELECT grass_region
,if(grass_region IN ('BR', 'MX'), date_trunc('month',date(from_unixtime(create_timestamp))),date_trunc('month',grass_date)) as month
,cast(count(distinct order_id) as double) as platform_orders
,cast(count(distinct if(is_cb_shop=1,order_id,null)) as double) as cb_orders
,cast(sum(gmv_usd) as double) as platform_gmv
,cast(sum(if(is_cb_shop=1,gmv_usd,null)) as double) as cb_gmv
FROM mp_order.dwd_order_place_pay_complete_di__reg_s0_live
WHERE if(grass_region IN ('BR', 'MX'), date(from_unixtime(create_timestamp)), grass_date) >= date('2021-01-01')
AND tz_type = 'local'
AND is_placed = 1
and is_bi_excluded=0 
and bi_exclude_reason is null
group by 1,2)
group by 1,2,3)


select grass_region,month
,platform_orders/days as platform_ado
,cb_orders/days as cb_ado
,platform_gmv/days as platform_adgmv
,cb_gmv/days as cb_adgmv
from daily_orders 
order by 1,2,3