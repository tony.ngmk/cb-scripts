WITH
dim30 AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE ((grass_date >= (current_date - INTERVAL '30' DAY)) AND (grass_region IN ('TW', 'MY', 'ID', 'VN', 'SG', 'PH', 'TH', 'BR', 'MX')))
) 
, dim7 AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE ((grass_date >= (current_date - INTERVAL '7' DAY)) AND (grass_region IN ('TW', 'MY', 'ID', 'VN', 'SG', 'PH', 'TH', 'BR', 'MX')))
) 
, raw AS (
SELECT DISTINCT
oversea_orderid
, "max"(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE ((("date"("from_unixtime"(ctime)) >= (current_date - INTERVAL '90' DAY)) AND (local_country IN ('my', 'tw', 'id', 'vn', 'th'))) AND (oversea_country IN ('sg', 'my', 'ph', 'th', 'vn', 'br', 'mx')))
GROUP BY 1
) 
, sip AS (
SELECT DISTINCT
B.COUNTRY MST_COUNTRY
, mst_shopid
, affi_shopid
, a.country affi_country
FROM
(((
SELECT
country
, mst_shopid
, affi_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (grass_region IN ('SG', 'MY', 'PH', 'TH', 'VN', 'BR', 'MX'))
) a
INNER JOIN (
SELECT DISTINCT
shopid
, COUNTRY
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE (cb_option = 0) AND country IN ('MY', 'TW', 'ID', 'VN', 'TH')
) b ON (a.mst_shopid = b.shopid)))
WHERE ((a.country IN ('SG', 'MY', 'PH', 'TH', 'VN', 'BR', 'MX')) AND (b.country IN ('MY', 'TW', 'ID', 'VN', 'TH')))
) 
, net AS (
SELECT DISTINCT
shop_id
, "date"("from_unixtime"(create_timestamp)) create_time
, grass_region
, "count"(DISTINCT order_id) gross_order
, "count"(DISTINCT (CASE WHEN ((order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN order_id ELSE null END)) net_order
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live o
WHERE (((("date"("from_unixtime"(create_timestamp)) >= (current_date - INTERVAL '30' DAY)) AND (tz_type = 'local'))) AND (grass_region IN ('TW', 'MY', 'ID', 'VN', 'SG', 'PH', 'TH', 'BR', 'MX')))
AND grass_date >= CURRENT_DATE-INTERVAL'30'DAY 

GROUP BY 1, 2, 3
) 
, cancel AS (
SELECT DISTINCT
shopid
, "date"("from_unixtime"(cancel_time)) cancel_time
, "count"(DISTINCT (CASE WHEN (((grass_region = 'TW') AND (extinfo.cancel_reason IN (1, 2, 3, 200, 201, 204, 302)))) THEN orderid 
WHEN (((grass_region = 'BR') AND (extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302, 801)))) THEN orderid WHEN (((NOT (grass_region IN ('TW', 'BR'))) AND (extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)))) THEN orderid ELSE null END)) cancel_order
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live o
WHERE ((("date"("from_unixtime"(cancel_time)) >= (current_date - INTERVAL '30' DAY))) AND (grass_region IN ('ID', 'TW', 'MY', 'VN', 'SG', 'PH', 'TH', 'BR', 'MX')))
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
shopid
, "date"("from_unixtime"(mtime)) mtime
, "count"(DISTINCT (CASE WHEN (((((grass_region = 'TW') AND (reason IN (1, 2, 3, 103, 105))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5)))) THEN a.orderid WHEN (((((grass_region <> 'TW') AND (reason IN (1, 2, 3, 103, 105, 107))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5)))) THEN a.orderid ELSE null END)) RR_order
FROM
(((marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ((grass_region IN ('TW', 'MY', 'ID', 'VN', 'SG', 'PH', 'TH', 'BR', 'MX'))
AND ("date"("from_unixtime"(cancel_time)) >= (current_date - INTERVAL '30' DAY))
)
) b ON (a.orderid = b.orderid))))
WHERE ((("date"("from_unixtime"(mtime)) >= (current_date - INTERVAL '30' DAY))) AND (grass_region IN ('ID', 'MY', 'TW', 'VN', 'SG', 'PH', 'TH', 'BR', 'MX')))
GROUP BY 1, 2
) 
, output30 AS (
SELECT DISTINCT
shop_id shopid
, sum(gross_order) l30d_gross_order
, sum(net_order) l30d_net_order
, ((COALESCE("sum"((cancel_order)), 0) * DECIMAL '1.000000') / CAST(((((("sum"(cancel_order) + COALESCE("sum"(RR_order), 0)) + "sum"(net_order))))) AS double)) l30d_overall_cancel_rate
FROM
((((dim30)
LEFT JOIN net ON ((dim30.grass_region = net.grass_region) AND (dim30.grass_date = net.create_time)))
LEFT JOIN cancel ON ((net.shop_id = cancel.shopid) AND (dim30.grass_date = cancel.cancel_time)))
LEFT JOIN rr ON ((net.shop_id = rr.shopid) AND (dim30.grass_date = rr.mtime)))
GROUP BY 1
) 
, output7 AS (
SELECT DISTINCT
shop_id shopid
, sum(gross_order) l7d_gross_order
, sum(net_order) l7d_net_order
, ((COALESCE("sum"((cancel_order)), 0) * DECIMAL '1.000000') / CAST(((((("sum"(cancel_order) + COALESCE("sum"(RR_order), 0)) + "sum"(net_order))))) AS double)) l7d_overall_cancel_rate
FROM
((((dim7)
LEFT JOIN net ON ((dim7.grass_region = net.grass_region) AND (dim7.grass_date = net.create_time)))
LEFT JOIN cancel ON ((net.shop_id = cancel.shopid) AND (dim7.grass_date = cancel.cancel_time)))
LEFT JOIN rr ON ((net.shop_id = rr.shopid) AND (dim7.grass_date = rr.mtime)))
GROUP BY 1
) 


SELECT DISTINCT
sip.mst_shopid
, sip.affi_shopid
, sip.mst_country
, sip.affi_country
, p_op7.l7d_overall_cancel_rate p_L7D_cancel_rate
, p_op30.l30d_overall_cancel_rate p_L30D_cancel_rate
, a_op7.l7d_overall_cancel_rate a_L7D_cancel_rate
, a_op30.l30d_gross_order a_l30d_gross_order
, a_op30.l30d_net_order a_l30d_net_order
, a_op30.l30d_overall_cancel_rate a_L30D_cancel_rate
FROM
regcbbi_others.sip__local_sip_reonboarded_high_cancellation_rate_shop__df__region__s0 a
INNER JOIN sip ON (CAST(a.affi_shopid AS bigint) = sip.affi_shopid)
LEFT JOIN output7 p_op7 ON (p_op7.shopid = sip.mst_shopid)
LEFT JOIN output30 p_op30 ON (p_op30.shopid = sip.mst_shopid)
LEFT JOIN output7 a_op7 ON (a_op7.shopid = sip.affi_shopid)
LEFT JOIN output30 a_op30 ON (a_op30.shopid = sip.affi_shopid)