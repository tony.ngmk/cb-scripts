WITH sip_shops AS (
SELECT distinct affi.grass_region
, cast(affi.affi_shopid AS BIGINT) AS affi_shopid
, cast(affi.mst_shopid AS BIGINT) AS mst_shopid
, mst.country AS p_shop_country
, mst.cb_option AS p_shop_cb_option
, date(from_unixtime(offboard_time)) as offboard_date
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live affi
left join marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live mst on affi.mst_shopid = mst.shopid
where grass_region != ''
-- and sip_shop_status != 3
)


, sip_ord AS
(SELECT ov4.grass_region
, sip_shops.p_shop_country
, create_date
, CASE WHEN sip_shops.p_shop_cb_option = 0 then 'Local'
WHEN sip_shops.p_shop_country = 'SG' then 'KRCB'
ELSE 'CNCB'
END AS p_shop_type
, COUNT(distinct ov4.orderid) AS gross_order
, SUM(gmv_usd) AS gmv_usd
FROM
(SELECT grass_region
, order_id orderid
, shop_id shopid
, date(from_unixtime(create_timestamp)) as create_date
, sum(gmv_usd) gmv_usd
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE is_cb_shop = 1
AND tz_type='local'
AND grass_date >= (date(from_unixtime(1655697600)) - INTERVAL '66' DAY)
AND create_timestamp >= to_unixtime(date(from_unixtime(1655697600))) - 86400 * 65
AND grass_region!=''
GROUP BY 1,2,3,4
) ov4
JOIN sip_shops
ON ov4.grass_region = sip_shops.grass_region
AND ov4.shopid = sip_shops.affi_shopid
AND (ov4.create_date <= sip_shops.offboard_date OR sip_shops.offboard_date = date'1970-01-01')
GROUP BY 1,2,3,4
)


, cb_ord AS
(SELECT grass_region
, date(from_unixtime(create_timestamp)) as create_date
, COUNT(distinct order_id) AS gross_order
, SUM(gmv_usd) gmv_usd
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE is_cb_shop = 1
AND grass_date >= (date(from_unixtime(1655697600)) - INTERVAL '66' DAY)
AND tz_type='local'
AND create_timestamp >= to_unixtime(date(from_unixtime(1655697600))) - 86400 * 65
AND grass_region!=''
GROUP BY 1,2
)


, cb_shop_detail AS
(SELECT distinct
s.grass_region
, s.grass_date AS data_date
, s.shop_id
, registration_date
, live_sku
, new_live_sku
FROM
(SELECT grass_date, grass_region, shop_id
FROM mp_user.dim_shop__reg_s0_live
WHERE grass_date between date_add('day', -65, date(from_unixtime(1655697600))) AND date(from_unixtime(1655697600))
AND is_cb_shop = 1
AND status = 1
AND is_holiday_mode = 0
AND tz_type = 'local'
) s
JOIN
(SELECT grass_date, grass_region, shop_id, DATE(from_unixtime(registration_timestamp)) AS registration_date
FROM mp_user.dim_user__reg_s0_live
WHERE grass_date between date_add('day', -65, date(from_unixtime(1655697600))) AND date(from_unixtime(1655697600))
AND status = 1
AND is_seller = 1
AND tz_type = 'local'
AND is_cb_shop = 1
) u
ON s.grass_date = u.grass_date
AND s.grass_region = u.grass_region
AND s.shop_id = u.shop_id
JOIN
(SELECT grass_date, grass_region, shop_id
FROM mp_item.dws_shop_listing_td__reg_s0_live
WHERE grass_date between date_add('day', -65, date(from_unixtime(1655697600))) AND date(from_unixtime(1655697600))
AND active_item_with_stock_cnt > 0
) sl
ON s.grass_date = sl.grass_date
AND s.grass_region = sl.grass_region
AND s.shop_id = sl.shop_id
JOIN
(SELECT grass_region, grass_date, shop_id
FROM mp_user.dws_user_login_td_account_info__reg_s0_live 
WHERE tz_type = 'local'
AND grass_date between date_add('day', -65, date(from_unixtime(1655697600))) AND date(from_unixtime(1655697600))
AND DATE(from_unixtime(last_login_timestamp_td)) >= date_add('day', -6, grass_date)
) l
ON s.grass_region = l.grass_region
AND s.shop_id = l.shop_id
AND s.grass_date = l.grass_date
LEFT JOIN
(
SELECT grass_region
, grass_date AS data_date
, shop_id shopid
, COUNT(distinct item_id) AS live_sku
, COUNT(distinct IF(DATE(from_unixtime(create_timestamp)) = date(from_unixtime(1655697600)), item_id,NULL)) AS new_live_sku
FROM mp_item.dim_item__reg_s0_live
WHERE is_cb_shop = 1
AND stock > 0
AND status = 1
AND grass_region <> ''
AND grass_date = date(from_unixtime(1655697600))
AND tz_type = 'local'
GROUP BY 1,2,3
) iv4
ON iv4.grass_region = s.grass_region
AND iv4.shopid = s.shop_id
AND iv4.data_date = s.grass_date
)


, sip_sku AS
(SELECT sku.grass_region
, sip_shops.p_shop_country
, sku.data_date
, CASE WHEN sip_shops.p_shop_cb_option = 0 then 'Local'
WHEN sip_shops.p_shop_country = 'SG' then 'KRCB'
ELSE 'CNCB'
END AS p_shop_type
, SUM(live_sku) AS live_sku
, SUM(new_live_sku) AS new_live_sku
FROM (
SELECT grass_region, data_date, shop_id, live_sku, new_live_sku
FROM cb_shop_detail
WHERE data_date = date(from_unixtime(1655697600))
) sku
JOIN sip_shops
ON sku.grass_region = sip_shops.grass_region
AND sku.shop_id = sip_shops.affi_shopid
WHERE sip_shops.offboard_date = date'1970-01-01'
GROUP BY 1, 2, 3, 4
)


, cb_sku AS
(SELECT grass_region
, data_date
, SUM(live_sku) AS live_sku
, SUM(new_live_sku) AS new_live_sku
FROM cb_shop_detail
WHERE data_date = date(from_unixtime(1655697600))
GROUP BY 1, 2
)


, sip_shop AS
(SELECT s.grass_region
, s.p_shop_country
, c.data_date
, CASE WHEN s.p_shop_cb_option = 0 then 'Local'
WHEN s.p_shop_country = 'SG' then 'KRCB'
ELSE 'CNCB'
END AS p_shop_type
, COUNT(distinct c.shop_id) AS live_shop
FROM cb_shop_detail c
JOIN sip_shops s
ON c.shop_id = s.affi_shopid
AND c.grass_region = s.grass_region
AND (c.data_date <= s.offboard_date or s.offboard_date = date'1970-01-01')
GROUP BY 1, 2, 3, 4
)


, sip_new_shop AS
(SELECT s.grass_region
, s.p_shop_country
, c.registration_date
, CASE WHEN s.p_shop_cb_option = 0 then 'Local'
WHEN s.p_shop_country = 'SG' then 'KRCB'
ELSE 'CNCB'
END AS p_shop_type
, COUNT(distinct c.shop_id) AS new_live_shop
FROM cb_shop_detail c
JOIN sip_shops s
ON c.shop_id = s.affi_shopid
AND c.grass_region = s.grass_region
AND(c.data_date <= s.offboard_date or s.offboard_date = date'1970-01-01')
GROUP BY 1, 2, 3, 4
)


SELECT sip_shop.grass_region
, sip_shop.p_shop_country
, sip_shop.data_date
, sip_shop.p_shop_type
, sip_ord.gross_order AS sip_gross_order
, sip_ord.gmv_usd AS sip_gmv_usd
, sip_sku.live_sku AS sip_live_sku
, cb_ord.gross_order AS cb_gross_order
, cb_ord.gmv_usd AS cb_gmv_usd
, cb_sku.live_sku AS cb_live_sku
, sip_sku.new_live_sku AS sip_new_live_sku
, sip_new_shop.new_live_shop AS sip_new_live_shop
, sip_shop.live_shop AS sip_live_shop
FROM sip_shop
LEFT JOIN sip_new_shop
ON sip_shop.grass_region = sip_new_shop.grass_region
AND sip_shop.data_date = sip_new_shop.registration_date
AND sip_shop.p_shop_country = sip_new_shop.p_shop_country
AND sip_shop.p_shop_type = sip_new_shop.p_shop_type
LEFT JOIN sip_sku
ON sip_shop.grass_region = sip_sku.grass_region
AND sip_shop.data_date = sip_sku.data_date
AND sip_shop.p_shop_country = sip_sku.p_shop_country
AND sip_shop.p_shop_type = sip_sku.p_shop_type
LEFT JOIN cb_sku
ON sip_shop.grass_region = cb_sku.grass_region
AND sip_shop.data_date = cb_sku.data_date
LEFT JOIN sip_ord
ON sip_shop.grass_region = sip_ord.grass_region
AND sip_shop.data_date = sip_ord.create_date
AND sip_shop.p_shop_country = sip_ord.p_shop_country
AND sip_shop.p_shop_type = sip_ord.p_shop_type
LEFT JOIN cb_ord
ON sip_shop.grass_region = cb_ord.grass_region
AND sip_shop.data_date = cb_ord.create_date
ORDER BY 3 DESC, 1, 2, 4