WITH
SIP AS (
SELECT DISTINCT
mst_shopid
, affi_shopid
, affi_userid ovs_userid
, user_name ovs_seller_user_name
FROM
((marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
INNER JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b ON (a.shopid = b.mst_shopid))
LEFT JOIN mp_user.dim_user__reg_s0_live c ON (b.affi_shopid = c.shop_id))
WHERE ((((((cb_option = 0) AND (a.country = 'TW')) AND (b.grass_region IN ('SG', 'MY'))) AND (c.grass_date = (current_date - INTERVAL '1' DAY))) AND (c.tz_type = 'local')) AND (c.grass_region IN ('SG', 'MY')))
) 
SELECT DISTINCT
grass_region
, (CASE WHEN ((rule[17] = true) AND (shopid <> 0)) THEN 'shop voucher by seller' WHEN ((rule[17] = true) AND (shopid = 0)) THEN 'shop voucher by shopee' WHEN ((rule[17] = false) AND (shopid <> 0)) THEN 'platform voucher by seller' WHEN ((rule[17] = false) AND (shopid = 0)) THEN 'platform voucher by shopee' END) voucher_type
, prefix voucher_code
, promotionid
, ovs_userid
, ovs_seller_user_name
, (CASE WHEN (rule[24] = 1) THEN 'COIN_CASHBACK_VOUCHER' ELSE 'NORMAL_VOUCHER' END) promotion_type
, rule[25][3] max_coin
, rule[25][4] coin_percentage_real
, (value / 100000) discount_amount
, discount
, (max_value / 100000) max_value
, min_price
, total_count claim_limit
, usage_limit
, distributed_count claim_count
, current_usage
, description
, "from_unixtime"(start_time) start_time
, "from_unixtime"(end_time) end_time
FROM
(SIP a
INNER JOIN marketplace.shopee_voucher_db__promotion_tab__reg_daily_s0_live b ON (a.affi_shopid = b.shopid))
WHERE (((("from_unixtime"(end_time) > "date_add"('month', -3, "date_trunc"('month', (current_date - INTERVAL '1' DAY)))) AND ("from_unixtime"(start_time) >= "date"('2021-01-01'))) AND (b.status = 1)) AND (grass_region IN ('MY', 'SG')))
UNION SELECT DISTINCT
grass_region
, (CASE WHEN ((rule[17] = true) AND (shopid <> 0)) THEN 'shop voucher by seller' WHEN ((rule[17] = true) AND (shopid = 0)) THEN 'shop voucher by shopee' WHEN ((rule[17] = false) AND (shopid <> 0)) THEN 'platform voucher by seller' WHEN ((rule[17] = false) AND (shopid = 0)) THEN 'platform voucher by shopee' END) voucher_type
, prefix voucher_code
, promotionid
, ovs_userid
, ovs_seller_user_name
, (CASE WHEN (rule[24] = 1) THEN 'COIN_CASHBACK_VOUCHER' ELSE 'NORMAL_VOUCHER' END) promotion_type
, rule[25][3] max_coin
, rule[25][4] coin_percentage_real
, (value / 100000) discount_amount
, discount
, (max_value / 100000) max_value
, min_price
, total_count claim_limit
, usage_limit
, distributed_count claim_count
, current_usage
, description
, "from_unixtime"(start_time) start_time
, "from_unixtime"(end_time) end_time
FROM
((SIP a
INNER JOIN (
SELECT DISTINCT
COALESCE(sv_promotion_id, pv_promotion_id) promotion_id
, shop_Id
FROM
mp_order.dwd_order_place_pay_complete_di__reg_s0_live
WHERE ((((grass_date >= "date"('2021-01-01')) AND (tz_type = 'local')) AND (is_placed = 1)) AND ((((pv_rebate_by_shopee_amt > 0) OR (sv_rebate_by_shopee_amt > 0)) OR (sv_coin_earn_by_shopee > 0)) OR (pv_coin_earn_by_shopee > 0)))
) b ON (a.affi_shopid = b.shop_id))
INNER JOIN marketplace.shopee_voucher_db__promotion_tab__reg_daily_s0_live c ON (b.promotion_id = c.promotionid))
WHERE (((("from_unixtime"(end_time) > "date_add"('month', -3, "date_trunc"('month', (current_date - INTERVAL '1' DAY)))) AND ("from_unixtime"(start_time) >= "date"('2021-01-01'))) AND (c.status = 1)) AND (grass_region IN ('MY', 'SG')))