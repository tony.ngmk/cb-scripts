insert into nexus.test_fef1a8fe4e11d0842870dbf85241bdc92b47960c0062d280b357d7699e5433b3_1f6345a8f1ac04beb34aae0b3f08cecf with seller as (
select 
s.shop_id as shopid,
s.grass_region,
s.user_name as shop_name,
s.gp_name,
s.gp_account_owner,
s.child_account_owner,
s.user_id as userid,
case when lower(s.gp_smt) not in (
'beauty brands',
'fmcg non-beauty',
'lifestyle',
'el & fashion',
'beauty reseller',
'gr',
'icb',
'ss'
) then 'N/A' else s.gp_smt end as gp_smt
from
dev_regcbbi_kr.krcb_shop_profile as s
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
) 
),
normal_shop as (
select
s.gp_smt,
s.grass_region,
u.grass_date,
u.shop_id
from
mp_user.dim_shop__reg_s0_live as u
join 
seller as s
on
u.shop_id = s.shopid
and u.tz_type = 'local'
and u.status = 1
and (u.is_holiday_mode IS NULL OR u.is_holiday_mode = 0)
where
u.grass_date >= date('2022-01-01')
group by
1,2,3,4
),
active_item_shop as (
select
s.gp_smt,
s.grass_region,
t.grass_date,
t.shop_id
from
mp_item.dws_shop_listing_td__reg_s0_live as t
join
seller as s
on
t.shop_id = s.shopid
and t.tz_type = 'local'
and t.active_item_with_stock_cnt >= 5
where
t.grass_date >= date('2022-01-01')
group by
1,2,3,4
),
normal_account_shops as (
select
s.gp_smt,
s.grass_region,
t.grass_date,
t.shop_id
from
mp_user.dim_user__reg_s0_live as t
join
seller as s
on
t.shop_id = s.shopid
and t.tz_type = 'local'
and t.status = 1
where
t.grass_date >= date('2022-01-01')
group by
1,2,3,4
),
login_shops as (
SELECT 
s.gp_smt,
s.grass_region,
s.shopid as shop_id, 
l.grass_date
FROM 
mp_user.dws_user_login_td_account_info__reg_s0_live as l
join
seller as s
on
l.user_id = s.userid
and l.tz_type = 'local'
and date(split(l.last_login_datetime_td,' ')[1]) >= date_add('day',-7,l.grass_date)
where
l.grass_date >= date('2022-01-01')
group by
1,2,3,4
)
SELECT
a.grass_date,
a.gp_smt,
a.grass_region,
count(distinct a.shop_id) as num_live_shop
FROM 
normal_shop as a
JOIN 
login_shops AS b
ON
a.shop_id = b.shop_id
AND a.grass_date = b.grass_date
JOIN 
active_item_shop AS c
ON
a.shop_id = c.shop_id
AND a.grass_date = c.grass_date
join
normal_account_shops as n
on
a.shop_id = n.shop_id
and a.grass_date = n.grass_date
group by
1,2,3