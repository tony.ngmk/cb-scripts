insert into nexus.test_5d76300b91389a4f12348631d1df22b61083aab76240d46584fc39f4ad312c3d_fbc9cfcb59e035adc97663f413f5745c with seller as (
select 
shop_id as shopid,
grass_region,
user_name as shop_name,
gp_name,
gp_account_owner,
gp_smt
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
) 
),
raw_orders as (
select
*
from
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
where
is_placed = 1
and flash_sale_type_id in (0,1,2)
and grass_date between date_add('day',-8,current_date) and date_add('day',-1,current_date)
and is_bi_excluded = 0
),
orders as (
select
o.grass_date,
o.grass_region,
s.gp_smt,
s.gp_name,
o.shop_id,
s.shop_name,
case
when o.flash_sale_type_id = 2 then 'My Shop Flash Sale'
when o.flash_sale_type_id = 0 then 'Normal Flash Sale'
when o.flash_sale_type_id = 1 then 'Brand Flash Sale'
end as flash_sale_type,
o.item_id,
o.model_id,
sum(o.order_fraction) as gross_orders,
sum(o.gmv_usd) as gmv_usd,
sum(o.item_amount) as item_sold_qty,
level1_global_be_category as level1_category, 
level2_global_be_category as level2_category
from
raw_orders as o
join
seller as s
on
o.shop_id = s.shopid
group by
1,2,3,4,5,6,7,8,9,13,14
),
item_info as (
select
i.item_id,
i.item_name,
i.model_id,
i.model_name
from
mp_item.dim_model__reg_s0_live as i
join
seller as s
on
i.shop_id = s.shopid
and i.tz_type = 'local'
and i.grass_date = date_add('day',-1,current_date)
where
i.grass_region <> ''
group by
1,2,3,4
)
select
o.grass_date,
o.grass_region,
o.gp_smt,
o.gp_name,
o.shop_id,
o.shop_name,
o.flash_sale_type,
o.item_id,
i.item_name,
o.model_id,
i.model_name,
o.gross_orders,
o.gmv_usd,
o.item_sold_qty,
o.level1_category,
o.level2_category
from
orders as o
join
item_info as i
on
i.item_id = o.item_id
and i.model_id = o.model_id
order by
1,2,3,4,5,7,8