insert into nexus.test_65b9adb2064456361d92c00e877e5f8aec33d76900ee9eb2b73da4d7f64895ec_0c1bf5a874626e20d17c95ddb6140cb9 WITH 
jpcb AS (
select 
distinct 
shop_id
, user_id
, grass_region
from
dev_regcbbi_kr.jpcb_shop_profile
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.jpcb_shop_profile
where
grass_region <> '')
and grass_region <> '' 
)
, sku_base AS (
select i.grass_region
, DATE(split(i.create_datetime, ' ')[1]) as sku_create_date
, i.item_id
, i.grass_date
, CASE WHEN l3.shop_live_date IN ('None', 'nan') THEN NULL ELSE DATE_PARSE(l3.shop_live_date, '%Y-%m-%d') END AS shop_live_date
, CASE WHEN l3.last_live_date IN ('None', 'nan') THEN NULL ELSE DATE_PARSE(l3.last_live_date, '%Y-%m-%d') END AS shop_last_live_date
, CASE WHEN l3.m1_live_in_month IN ('None', 'nan') THEN 0 ELSE CAST(CAST(l3.m1_live_in_month AS DOUBLE) AS INT) END AS M1_live_in_month
, CASE WHEN l3.mtd_live_in_month IN ('None', 'nan') THEN 0 ELSE CAST(CAST(l3.mtd_live_in_month AS DOUBLE) AS INT) END AS mtd_live_in_month
, CASE WHEN l3.w1_last_day_live_in_month IN ('None', 'nan') THEN 0 ELSE CAST(CAST(l3.w1_last_day_live_in_month AS DOUBLE) AS INT) END AS w1_last_day_live
, CASE WHEN l3.d1_last_day_live_in_month IN ('None', 'nan') THEN 0 ELSE CAST(CAST(l3.d1_last_day_live_in_month AS DOUBLE) AS INT) END AS d1_last_day_live
from mp_item.dim_item__reg_s0_live AS i
join jpcb on i.shop_id = jpcb.shop_id
LEFT JOIN regcbbi_others.shopee_regional_cb_team__jpcb_shop_live_stats AS l3 ON CAST(l3.shop_id AS BIGINT) = i.shop_id
WHERE i.status = 1
AND i.stock > 0
AND i.tz_type = 'local'
AND i.grass_date IN ( DATE_ADD('day', -1, DATE_TRUNC('month', date(from_unixtime(1655697600))))
, DATE_ADD('day', -1, DATE_TRUNC('week', date(from_unixtime(1655697600))))
, date(from_unixtime(1655697600)) )
and DATE(split(i.create_datetime, ' ')[1]) <= date(from_unixtime(1655697600))
)


SELECT grass_region
, count(distinct case when grass_date = DATE_ADD('day', -1, DATE_TRUNC('month', date(from_unixtime(1655697600))))
AND m1_live_in_month = 1
AND sku_create_date <= date_add('day',-1,date_trunc('month',date(from_unixtime(1655697600))))
then item_id else null end) as M_1_total_live_sku
, count(distinct case when grass_date = date(from_unixtime(1655697600))
AND mtd_live_in_month = 1
AND sku_create_date <= date(from_unixtime(1655697600))
then item_id else null end) as MTD_total_live_sku
, count(distinct case when grass_date = DATE_ADD('day', -1, DATE_TRUNC('week', date(from_unixtime(1655697600))))
AND w1_last_day_live = 1
AND sku_create_date <= date_add('day',-1,date_trunc('week',date(from_unixtime(1655697600))))
then item_id else null end) as W_1_total_live_sku
, count(distinct case when grass_date = date(from_unixtime(1655697600))
AND d1_last_day_live = 1
AND sku_create_date <= date(from_unixtime(1655697600))
then item_id else null end) as WTD_total_live_sku
, count(distinct case when grass_date = DATE_ADD('day', -1, DATE_TRUNC('month', date(from_unixtime(1655697600))))
AND m1_live_in_month = 1
AND sku_create_date between date_add('month',-1,date_trunc('month',date(from_unixtime(1655697600)))) and date_add('day',-1,date_trunc('month',date(from_unixtime(1655697600))))
then item_id else null end) as M_1_new_live_sku
, count(distinct case when grass_date = date(from_unixtime(1655697600))
AND mtd_live_in_month = 1
AND sku_create_date between date_trunc('month',date(from_unixtime(1655697600))) and date(from_unixtime(1655697600))
then item_id else null end) as MTD_new_live_sku
, count(distinct case when grass_date = DATE_ADD('day', -1, DATE_TRUNC('week', date(from_unixtime(1655697600))))
AND w1_last_day_live = 1
AND sku_create_date between date_add('week',-1,date_trunc('week',date(from_unixtime(1655697600)))) and date_add('day',-1,date_trunc('week',date(from_unixtime(1655697600))))
then item_id else null end) as W_1_new_live_sku
, count(distinct case when grass_date = date(from_unixtime(1655697600))
AND d1_last_day_live = 1
AND sku_create_date between date_trunc('week',date(from_unixtime(1655697600))) and date(from_unixtime(1655697600))
then item_id else null end) as WTD_new_live_sku
from sku_base
group by 1
order by 1