insert into nexus.test_3a684a48c737e40b779506d2409ed94b6a1988e98b6f7443783f90a94731a74a_aa887eab371d0405fe730c76994bf951 WITH 
seller_index AS (
SELECT DISTINCT cb.shop_id
, cb.user_id
, CASE WHEN white.whitelist_date IS NOT NULL THEN white.whitelist_date
WHEN cb.grass_region IN ('SG') THEN DATE('2022-03-07')
WHEN cb.grass_region IN ('ID') THEN DATE('2022-03-14')
WHEN cb.grass_region IN ('TH','TW') THEN DATE('2022-03-21')
WHEN cb.grass_region IN ('BR','CL','CO','ES','MX','MY','PH','PL','VN') THEN DATE('2022-04-11')
ELSE white.whitelist_date END AS whitelist_date
FROM mp_user.dim_shop__reg_s0_live AS cb
LEFT JOIN (
SELECT region, CAST(seller_userid AS BIGINT) AS seller_userid, DATE(whitelist_date) AS whitelist_date
FROM regcbbi_others.pay_sts_refactor_whitelist_v2__reg_s0_live
) AS white ON white.seller_userid = cb.user_id
WHERE (cb.grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MX', 'MY', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
OR white.seller_userid IS NOT NULL)
AND cb.tz_type = 'local' AND cb.is_cb_shop = 1
AND cb.grass_date = DATE_ADD('day', -2, current_date)
)
, new_sts AS ( 
SELECT 'BR' AS grass_region
, CAST(bi.billing_item_id AS VARCHAR) AS billing_item_id
, CAST(bi.cycle_no AS BIGINT) AS cycle_no
, CAST(bi.amount AS DOUBLE) / 100000 AS amount
, bi.item_type AS item_type
, bi.item_status AS item_status
, CAST(bi.source_entity_id AS BIGINT) AS source_entity_id
, bi.ctime
, a.seller_userid
FROM marketplace.shopee_order_accounting_settlement_br_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT CAST(account_id AS BIGINT) AS account_id, CAST(rule_id AS BIGINT) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_br_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS BIGINT)
WHERE bi.is_cb = 1 AND bi.item_type IN (1,2,3,4,5) AND bi.item_status IN (1,2,3,4,5,6)


UNION ALL

SELECT 'CL' AS grass_region
, CAST(bi.billing_item_id AS VARCHAR) AS billing_item_id
, CAST(bi.cycle_no AS BIGINT) AS cycle_no
, CAST(bi.amount AS DOUBLE) / 100000 AS amount
, bi.item_type AS item_type
, bi.item_status AS item_status
, CAST(bi.source_entity_id AS BIGINT) AS source_entity_id
, bi.ctime
, a.seller_userid
FROM marketplace.shopee_order_accounting_settlement_cl_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT CAST(account_id AS BIGINT) AS account_id, CAST(rule_id AS BIGINT) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_cl_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS BIGINT)
WHERE bi.is_cb = 1 AND bi.item_type IN (1,2,3,4,5) AND bi.item_status IN (1,2,3,4,5,6)


UNION ALL

SELECT 'CO' AS grass_region
, CAST(bi.billing_item_id AS VARCHAR) AS billing_item_id
, CAST(bi.cycle_no AS BIGINT) AS cycle_no
, CAST(bi.amount AS DOUBLE) / 100000 AS amount
, bi.item_type AS item_type
, bi.item_status AS item_status
, CAST(bi.source_entity_id AS BIGINT) AS source_entity_id
, bi.ctime
, a.seller_userid
FROM marketplace.shopee_order_accounting_settlement_co_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT CAST(account_id AS BIGINT) AS account_id, CAST(rule_id AS BIGINT) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_co_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS BIGINT)
WHERE bi.is_cb = 1 AND bi.item_type IN (1,2,3,4,5) AND bi.item_status IN (1,2,3,4,5,6)


UNION ALL

SELECT 'ES' AS grass_region
, CAST(bi.billing_item_id AS VARCHAR) AS billing_item_id
, CAST(bi.cycle_no AS BIGINT) AS cycle_no
, CAST(bi.amount AS DOUBLE) / 100000 AS amount
, bi.item_type AS item_type
, bi.item_status AS item_status
, CAST(bi.source_entity_id AS BIGINT) AS source_entity_id
, bi.ctime
, a.seller_userid
FROM marketplace.shopee_order_accounting_settlement_es_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT CAST(account_id AS BIGINT) AS account_id, CAST(rule_id AS BIGINT) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_es_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS BIGINT)
WHERE bi.is_cb = 1 AND bi.item_type IN (1,2,3,4,5) AND bi.item_status IN (1,2,3,4,5,6)


UNION ALL

SELECT 'MX' AS grass_region
, CAST(bi.billing_item_id AS VARCHAR) AS billing_item_id
, CAST(bi.cycle_no AS BIGINT) AS cycle_no
, CAST(bi.amount AS DOUBLE) / 100000 AS amount
, bi.item_type AS item_type
, bi.item_status AS item_status
, CAST(bi.source_entity_id AS BIGINT) AS source_entity_id
, bi.ctime
, a.seller_userid
FROM marketplace.shopee_order_accounting_settlement_mx_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT CAST(account_id AS BIGINT) AS account_id, CAST(rule_id AS BIGINT) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_mx_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS BIGINT)
WHERE bi.is_cb = 1 AND bi.item_type IN (1,2,3,4,5) AND bi.item_status IN (1,2,3,4,5,6)


UNION ALL

SELECT 'PL' AS grass_region
, CAST(bi.billing_item_id AS VARCHAR) AS billing_item_id
, CAST(bi.cycle_no AS BIGINT) AS cycle_no
, CAST(bi.amount AS DOUBLE) / 100000 AS amount
, bi.item_type AS item_type
, bi.item_status AS item_status
, CAST(bi.source_entity_id AS BIGINT) AS source_entity_id
, bi.ctime
, a.seller_userid
FROM marketplace.shopee_order_accounting_settlement_pl_db__billing_item_tab__reg_continuous_s0_live AS bi
LEFT JOIN (
SELECT CAST(account_id AS BIGINT) AS account_id, CAST(rule_id AS BIGINT) AS rule_id, external_account_id AS seller_userid
FROM marketplace.shopee_order_accounting_settlement_pl_db__account_tab__reg_continuous_s0_live
) AS a ON a.account_id = CAST(bi.account_id AS BIGINT)
WHERE bi.is_cb = 1 AND bi.item_type IN (1,2,3,4,5) AND bi.item_status IN (1,2,3,4,5,6)
)
, old_adj AS (
SELECT 'BR' AS grass_region, CAST(batch_id AS BIGINT) AS batch_id, CAST(total_adjustment_amount AS DOUBLE) / 100000 AS amount, status, ctime
FROM marketplace.shopee_settlement_system_br_db__settlement_adjustment_batch_tab__reg_daily_s0_live
WHERE region_type = 0 AND DATE(FROM_UNIXTIME(ctime)) >= DATE_ADD('day', -120, current_date) AND DATE(FROM_UNIXTIME(ctime)) >= DATE('2022-03-21')
AND status IN (4)


UNION ALL


SELECT 'CL' AS grass_region, CAST(batch_id AS BIGINT) AS batch_id, CAST(total_adjustment_amount AS DOUBLE) / 100000 AS amount, status, ctime
FROM marketplace.shopee_settlement_system_cl_db__settlement_adjustment_batch_tab__reg_daily_s0_live
WHERE region_type = 0 AND DATE(FROM_UNIXTIME(ctime)) >= DATE_ADD('day', -120, current_date) AND DATE(FROM_UNIXTIME(ctime)) >= DATE('2022-03-21')
AND status IN (4)


UNION ALL


SELECT 'CO' AS grass_region, CAST(batch_id AS BIGINT) AS batch_id, CAST(total_adjustment_amount AS DOUBLE) / 100000 AS amount, status, ctime
FROM marketplace.shopee_settlement_system_co_db__settlement_adjustment_batch_tab__reg_daily_s0_live
WHERE region_type = 0 AND DATE(FROM_UNIXTIME(ctime)) >= DATE_ADD('day', -120, current_date) AND DATE(FROM_UNIXTIME(ctime)) >= DATE('2022-03-21')
AND status IN (4)


UNION ALL


SELECT 'ES' AS grass_region, CAST(batch_id AS BIGINT) AS batch_id, CAST(total_adjustment_amount AS DOUBLE) / 100000 AS amount, status, ctime
FROM marketplace.shopee_settlement_system_es_db__settlement_adjustment_batch_tab__reg_daily_s0_live
WHERE region_type = 0 AND DATE(FROM_UNIXTIME(ctime)) >= DATE_ADD('day', -120, current_date) AND DATE(FROM_UNIXTIME(ctime)) >= DATE('2022-03-21')
AND status IN (4)


UNION ALL


SELECT 'MX' AS grass_region, CAST(batch_id AS BIGINT) AS batch_id, CAST(total_adjustment_amount AS DOUBLE) / 100000 AS amount, status, ctime
FROM marketplace.shopee_settlement_system_mx_db__settlement_adjustment_batch_tab__reg_daily_s0_live
WHERE region_type = 0 AND DATE(FROM_UNIXTIME(ctime)) >= DATE_ADD('day', -120, current_date) AND DATE(FROM_UNIXTIME(ctime)) >= DATE('2022-03-21')
AND status IN (4)


UNION ALL


SELECT 'PL' AS grass_region, CAST(batch_id AS BIGINT) AS batch_id, CAST(total_adjustment_amount AS DOUBLE) / 100000 AS amount, status, ctime
FROM marketplace.shopee_settlement_system_pl_db__settlement_adjustment_batch_tab__reg_daily_s0_live
WHERE region_type = 0 AND DATE(FROM_UNIXTIME(ctime)) >= DATE_ADD('day', -120, current_date) AND DATE(FROM_UNIXTIME(ctime)) >= DATE('2022-03-21')
AND status IN (4)
)
, order_audit AS (
-- finding all orders from BE that are escrow_verified but not escrow_paid
SELECT order_id, grass_region, escrow_created_time, escrow_verified_time, escrow_paid_time
FROM (
SELECT a.orderid AS order_id
, a.grass_region
, cb.whitelist_date
, "date"("from_unixtime"("min"((CASE WHEN (a.new_status = 12) THEN a.ctime ELSE null END)))) escrow_created_time
, "date"("from_unixtime"("min"((CASE WHEN (a.new_status = 14) THEN a.ctime ELSE null END)))) escrow_verified_time
, "date"("from_unixtime"("min"((CASE WHEN (a.new_status = 11) THEN a.ctime ELSE null END)))) escrow_paid_time
FROM marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_continuous_s0_live AS a
INNER JOIN seller_index AS cb ON (cb.shop_id = a.shopid AND cb.whitelist_date <= DATE(FROM_UNIXTIME(a.ctime - (
IF(a.grass_region IN ('ES','PL'), 7, IF(
a.grass_region IN ('MX'), 14, IF(
a.grass_region IN ('CO'), 13, IF(
a.grass_region In ('CL'), 12, IF(
a.grass_region IN ('BR'), 11, IF(
a.grass_region IN ('ID','TH','VN'), 1, 0)))))) * 3600))) )
WHERE "date"("from_unixtime"(a.ctime)) >= DATE_ADD('day', -120, current_date)
AND a.grass_region IN ('BR', 'CL', 'CO', 'ES', 'MX', 'PL')
GROUP BY 1, 2, 3
) 
WHERE escrow_verified_time BETWEEN DATE_ADD('day', -120, current_date) AND current_date
AND escrow_verified_time >= whitelist_date
AND escrow_paid_time IS NULL
)
, order_nonsip AS (
SELECT a.grass_region
, a.orderid AS order_id
, CAST(a.extinfo.escrow_to_seller AS DOUBLE) / 100000 AS escrow_amount
, a.shopid AS shop_id
, CAST(a.extinfo.seller_userid AS BIGINT) AS user_id
FROM marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live AS a
INNER JOIN order_audit AS oa ON oa.order_id = a.orderid
-- LEFT JOIN (
-- SELECT distinct b.affi_shopid
-- , b.mst_shopid
-- from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live AS a
-- inner join marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live AS b on b.mst_shopid = a.shopid
-- --left join regbd_sf.cb__seller_index_tab AS x on a.shopid = CAST(x.child_shopid as INT)
-- --where (gp_account_billing_country is null or gp_account_billing_country = 'China')
-- AND x.grass_region IN ('SG') -- ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MX', 'MY', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
-- AND a.cb_option = 1
-- ) AS SIP ON SIP.affi_shopid = a.shopid
WHERE a.cb_option = 1
--AND SIP.affi_shopid is null -- excluding SIP
AND a.status_ext = 14
AND a.grass_region IN ('BR', 'CL', 'CO', 'ES', 'MX', 'PL')
AND date(from_unixtime(a.create_time)) >= DATE_ADD('day', -120, current_date)
)
, order_sip AS (
SELECT o.order_id
, SUM( COALESCE(o.settlement_escrow_amount,0) * COALESCE(o.exchange_rate,1) ) AS settlement_escrow_amount
FROM ( 
(SELECT CAST(sip.order_id AS BIGINT) AS order_id, exr.exchange_rate
, CAST(trim(regexp_replace(split(_decoded_extinfo,':')[2],'[""},merchandise_subtotal]')) AS DOUBLE) / 100000 AS settlement_escrow_amount
FROM marketplace.shopee_supplier_order_br_db__supplier_order_income_summary_tab__reg_continuous_s0_live AS sip
LEFT JOIN (
SELECT DISTINCT CAST(order_id AS BIGINT) AS order_id
, CAST(regexp_replace(split(_decoded_extinfo,':')[cardinality(split(_decoded_extinfo,':'))],'[""}]') AS DOUBLE) / 100000 AS exchange_rate
FROM marketplace.shopee_supplier_order_br_db__supplier_order_tab__reg_continuous_s0_live
) AS exr ON exr.order_id = CAST(sip.order_id AS BIGINT))


UNION ALL

(SELECT CAST(sip.order_id AS BIGINT) AS order_id, exr.exchange_rate
, CAST(trim(regexp_replace(split(_decoded_extinfo,':')[2],'[""},merchandise_subtotal]')) AS DOUBLE) / 100000 AS settlement_escrow_amount
FROM marketplace.shopee_supplier_order_cl_db__supplier_order_income_summary_tab__reg_continuous_s0_live AS sip
LEFT JOIN (
SELECT DISTINCT CAST(order_id AS BIGINT) AS order_id
, CAST(regexp_replace(split(_decoded_extinfo,':')[cardinality(split(_decoded_extinfo,':'))],'[""}]') AS DOUBLE) / 100000 AS exchange_rate
FROM marketplace.shopee_supplier_order_cl_db__supplier_order_tab__reg_continuous_s0_live
) AS exr ON exr.order_id = CAST(sip.order_id AS BIGINT))


UNION ALL

(SELECT CAST(sip.order_id AS BIGINT) AS order_id, exr.exchange_rate
, CAST(trim(regexp_replace(split(_decoded_extinfo,':')[2],'[""},merchandise_subtotal]')) AS DOUBLE) / 100000 AS settlement_escrow_amount
FROM marketplace.shopee_supplier_order_co_db__supplier_order_income_summary_tab__reg_continuous_s0_live AS sip
LEFT JOIN (
SELECT DISTINCT CAST(order_id AS BIGINT) AS order_id
, CAST(regexp_replace(split(_decoded_extinfo,':')[cardinality(split(_decoded_extinfo,':'))],'[""}]') AS DOUBLE) / 100000 AS exchange_rate
FROM marketplace.shopee_supplier_order_co_db__supplier_order_tab__reg_continuous_s0_live
) AS exr ON exr.order_id = CAST(sip.order_id AS BIGINT))


UNION ALL

(SELECT CAST(sip.order_id AS BIGINT) AS order_id, exr.exchange_rate
, CAST(trim(regexp_replace(split(_decoded_extinfo,':')[2],'[""},merchandise_subtotal]')) AS DOUBLE) / 100000 AS settlement_escrow_amount
FROM marketplace.shopee_supplier_order_es_db__supplier_order_income_summary_tab__reg_continuous_s0_live AS sip
LEFT JOIN (
SELECT DISTINCT CAST(order_id AS BIGINT) AS order_id
, CAST(regexp_replace(split(_decoded_extinfo,':')[cardinality(split(_decoded_extinfo,':'))],'[""}]') AS DOUBLE) / 100000 AS exchange_rate
FROM marketplace.shopee_supplier_order_es_db__supplier_order_tab__reg_continuous_s0_live
) AS exr ON exr.order_id = CAST(sip.order_id AS BIGINT))


UNION ALL

(SELECT CAST(sip.order_id AS BIGINT) AS order_id, exr.exchange_rate
, CAST(trim(regexp_replace(split(_decoded_extinfo,':')[2],'[""},merchandise_subtotal]')) AS DOUBLE) / 100000 AS settlement_escrow_amount
FROM marketplace.shopee_supplier_order_mx_db__supplier_order_income_summary_tab__reg_continuous_s0_live AS sip
LEFT JOIN (
SELECT DISTINCT CAST(order_id AS BIGINT) AS order_id
, CAST(regexp_replace(split(_decoded_extinfo,':')[cardinality(split(_decoded_extinfo,':'))],'[""}]') AS DOUBLE) / 100000 AS exchange_rate
FROM marketplace.shopee_supplier_order_mx_db__supplier_order_tab__reg_continuous_s0_live
) AS exr ON exr.order_id = CAST(sip.order_id AS BIGINT))


UNION ALL

(SELECT CAST(sip.order_id AS BIGINT) AS order_id, exr.exchange_rate
, CAST(trim(regexp_replace(split(_decoded_extinfo,':')[2],'[""},merchandise_subtotal]')) AS DOUBLE) / 100000 AS settlement_escrow_amount
FROM marketplace.shopee_supplier_order_pl_db__supplier_order_income_summary_tab__reg_continuous_s0_live AS sip
LEFT JOIN (
SELECT DISTINCT CAST(order_id AS BIGINT) AS order_id
, CAST(regexp_replace(split(_decoded_extinfo,':')[cardinality(split(_decoded_extinfo,':'))],'[""}]') AS DOUBLE) / 100000 AS exchange_rate
FROM marketplace.shopee_supplier_order_pl_db__supplier_order_tab__reg_continuous_s0_live
) AS exr ON exr.order_id = CAST(sip.order_id AS BIGINT))
) AS o
INNER JOIN order_audit AS aud ON aud.order_id = o.order_id
GROUP BY 1
)
, svs AS (
SELECT DISTINCT a.region AS grass_region
, CAST(a.order_sn AS BIGINT) as ads_source_id
, CAST(a.paid_amount AS DOUBLE) / 100000 AS amount
--, (a.topup_amt - coalesce(if(a.credit_topup_type is null,a.voucher_discount_amt,0),0)) as price_local
--, (topup_amt_usd - coalesce(if(credit_topup_type is null,voucher_discount_amt_usd,0),0)) as price_usd
--, topup_create_datetime as topup_time
, cb.user_id
FROM marketplace.shopee_seller_valueadded_order_db__order_tab__reg_daily_s0_live AS a
INNER JOIN seller_index AS cb ON (cb.shop_id = a.shop_id AND cb.whitelist_date <= DATE(FROM_UNIXTIME(a.ctime - (
IF(a.region IN ('ES','PL'), 7, IF(
a.region IN ('MX'), 14, IF(
a.region IN ('CO'), 13, IF(
a.region In ('CL'), 12, IF(
a.region IN ('BR'), 11, IF(
a.region IN ('ID','TH','VN'), 1, 0)))))) * 3600))) )
JOIN (
SELECT id
FROM marketplace.shopee_seller_valueadded_item_db__item_tab__reg_daily_s0_live
--WHERE category_id = 10102 -- 10101 = 'Seller_Coin', 10102 = 'Ads Credit'
) AS item ON item.id = a.item_id
JOIN (
SELECT DISTINCT order_id
, transaction_id
, payment_channel_name
, from_unixtime(mtime) as svs_paid_time
FROM marketplace.shopee_seller_valueadded_order_db__payment_tab__reg_daily_s0_live
WHERE payment_status = 2 /*svs_payment_status : 1='to_pay',2='paid',3='timeout',4='failed'*/
AND CAST(payment_channel_id AS VARCHAR) LIKE '%999999%'
) AS pay ON a.id = pay.order_id
WHERE a.region IN ('BR', 'CL', 'CO', 'ES', 'MX', 'PL')
--AND a.order_type = 0
AND DATE(from_unixtime(a.ctime)) >= DATE_ADD('day', -120, current_date)
)


select region
, billing_item_id
, source_id
, source_type
, seller_userid
, billing_item_create_date
, billing_item_amount
, source_amount
, billing_item_type
, billing_item_status
from
(SELECT region
, billing_item_id
, source_id
, source_type
, seller_userid
, billing_item_create_date
, billing_item_amount
, source_amount
, billing_item_type
, billing_item_status
, row_number() over(partition by region,non_issue_tag ) as rn
,non_issue_tag
FROM (
SELECT be.grass_region AS region
, esc.billing_item_id
, esc.cycle_no
, be.order_id AS source_id
, CASE WHEN be.is_sip = 1 THEN 'Supplier Escrow'
WHEN be.is_sip = 0 THEN 'Escrow'
ELSE NULL END AS source_type
, be.user_id AS seller_userid
, DATE(FROM_UNIXTIME(esc.ctime)) AS billing_item_create_date
, esc.amount AS billing_item_amount
, be.escrow_amount AS source_amount
, CASE esc.item_type WHEN 1 THEN 'Escrow'
WHEN 2 THEN 'Adjustment'
WHEN 3 THEN 'STS Adjustment'
WHEN 4 THEN 'SVS Order'
WHEN 5 THEN 'Cancelled Order'
ELSE NULL END AS billing_item_type
, CASE esc.item_status WHEN 1 THEN 'SETTLED'
WHEN 2 THEN 'STATEMENTED'
WHEN 3 THEN 'PAYOUT'
WHEN 4 THEN 'PAID'
WHEN 5 THEN 'PAUSED'
WHEN 6 THEN 'CANCELLED'
ELSE NULL END AS billing_item_status
, CASE 
WHEN be.is_sip = 1 AND ROUND(be.escrow_amount, IF( be.grass_region IN ('SG','MY','BR','MX','ES','FR','PL'), 2, 0 ))
= ROUND(esc.amount, IF( be.grass_region IN ('SG','MY','BR','MX','ES','FR','PL'), 2, 0 ))
THEN 'Supplier escrow and STS amount tally'
WHEN be.is_sip = 0 AND ROUND(be.escrow_amount, IF( be.grass_region IN ('SG','MY','BR','MX','ES','FR','PL'), 2, 0 ))
= ROUND(esc.amount, IF( be.grass_region IN ('SG','MY','BR','MX','ES','FR','PL'), 2, 0 ))
THEN 'BE escrow and STS amount tally'
ELSE NULL END AS non_issue_tag


FROM (
SELECT o1.grass_region
, o1.order_id
, o1.user_id
, CASE WHEN o2.order_id IS NOT NULL THEN 1 ELSE 0 END AS is_sip
, COALESCE(o2.settlement_escrow_amount, o1.escrow_amount) AS escrow_amount
FROM order_nonsip AS o1
LEFT JOIN order_sip AS o2 ON o2.order_id = o1.order_id


-- This part added to exclude supplier orders
WHERE o2.order_id IS NULL


) AS be
LEFT JOIN (
SELECT billing_item_id, item_type, item_status, source_entity_id, ctime, amount, grass_region, cycle_no
FROM new_sts 
WHERE item_type IN (1) AND item_status IN (1,2,5,6)
) AS esc ON esc.source_entity_id = be.order_id
where esc.billing_item_id is not null


UNION ALL


SELECT old.grass_region AS region
, '' AS billing_item_id
, 0 AS cycle_no
, old.batch_id AS source_id
, 'Old STS Adjustment Batch' AS source_type
, 0 AS seller_userid
, DATE('2000-01-01') AS billing_item_create_date
, adj.amount AS billing_item_amount
, old.amount AS source_amount
, '' AS billing_item_type
, '' AS billing_item_status
, CASE 
WHEN ROUND(adj.amount, IF( adj.grass_region IN ('SG','MY','BR','MX','ES','FR','PL'), 2, 0 ))
= ROUND(old.amount, IF( old.grass_region IN ('SG','MY','BR','MX','ES','FR','PL'), 2, 0 ))
THEN 'Adjustment and STS amount tally'
ELSE NULL END AS non_issue_tag
FROM old_adj AS old
LEFT JOIN (
SELECT grass_region, CAST(source_entity_id AS BIGINT) / 1000000000 AS batch_id, SUM(amount) AS amount
FROM new_sts
WHERE item_type IN (2,3) AND item_status IN (1,2,3,4,5,6)
GROUP BY 1,2
) AS adj ON (adj.grass_region = old.grass_region AND adj.batch_id = old.batch_id)
where adj.batch_id IS not NULL


UNION ALL


SELECT svs.grass_region AS region
, adj.billing_item_id
, adj.cycle_no
, svs.ads_source_id AS source_id
, 'SVS' AS source_type
, svs.user_id AS seller_userid
, DATE(FROM_UNIXTIME(adj.ctime)) AS billing_item_create_date
, adj.amount AS billing_item_amount
, svs.amount AS source_amount
, CASE adj.item_type WHEN 1 THEN 'Escrow'
WHEN 2 THEN 'Adjustment'
WHEN 3 THEN 'STS Adjustment'
WHEN 4 THEN 'SVS Order'
WHEN 5 THEN 'Cancelled Order'
ELSE NULL END AS billing_item_type
, CASE adj.item_status WHEN 1 THEN 'SETTLED'
WHEN 2 THEN 'STATEMENTED'
WHEN 3 THEN 'PAYOUT'
WHEN 4 THEN 'PAID'
WHEN 5 THEN 'PAUSED'
WHEN 6 THEN 'CANCELLED'
ELSE NULL END AS billing_item_status
, CASE 
WHEN ROUND(adj.amount, IF( adj.grass_region IN ('SG','MY','BR','MX','ES','FR','PL'), 2, 0 ))
= ROUND(-svs.amount, IF( svs.grass_region IN ('SG','MY','BR','MX','ES','FR','PL'), 2, 0 ))
THEN 'SVS and STS amount tally'
ELSE NULL END AS non_issue_tag
FROM svs
LEFT JOIN (
SELECT billing_item_id, item_type, item_status, source_entity_id, ctime, amount, grass_region, cycle_no
FROM new_sts
WHERE item_type IN (4)
) AS adj ON adj.source_entity_id = svs.ads_source_id
where adj.billing_item_id is not null


)
WHERE non_issue_tag IS NOT NULL 
) where rn <= 7


ORDER BY 1 ASC,non_issue_tag


LIMIT 20000