insert into nexus.test_851632abb5fb61cdc302dfe08194e10b454ac666bc576cb1207ef4631bd44a1e_0112ca324c604604832c3a5c5dc2aac6 --#################
--Migration due to FBS migration
-- Oct 12, 2021: aggregated UST shops (is_cb_shop = 1 AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER') data into cbwh metrics and added ust columns for visibility
-- Sep 15, 2021: removed login in live shop def
-- Aug 13，2021：switched to shopee_bd_sbs_mart_v2
-- Mar 21, 2022: Hannah: update cfs slot table
-- May 31 2022： Hannah commented out table regbida_userbehavior.shopee_bi_campaign_item
-- Jun 17 2022： Hannah add nnwh-spwh metrics and nnwh-ust metrics
--#################
WITH inv AS (
SELECT
DISTINCT grass_region,
shopid,
grass_date,
cast(itemid AS bigint) itemid,
sku_id,
listing_price_usd,
is_cb,
sku_status_normal,
fe_stock,
stock_on_hand,
is_live,
color,
coverage,
period_for_coverage_calculation,
avg_sku_age,
l60_gross_itemsold,
l60_purchasable_days
FROM
sbs_mart.shopee_bd_sbs_mart_v2
WHERE
grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND is_cb = 1
),
cnt AS (
SELECT
grass_region,
grass_date,
max(l60_purchasable_days) l60_purchasable_days,
sum(l60_gross_itemsold) l60_gross_itemsold
FROM
inv
GROUP BY
1,
2
),
sku_dim AS(
SELECT
'ID' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_id_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'MY' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_my_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'PH' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_ph_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'TH' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_th_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'SG' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_sg_db__sku_cache_tab__reg_daily_s0_live
),
inv_summary AS (
SELECT
inv.*,
1.0000 * total_stock_on_hand * l60_purchasable_days / nullif(l60_gross_itemsold, 0) coverage_days
FROM
(
SELECT
inv.grass_region,
inv.grass_date,
count(DISTINCT itemid) sku_with_stock_cnt,
count(DISTINCT inv.sku_id) model_with_stock_cnt,
sum(
CASE
WHEN avg_sku_age > 180 THEN stock_on_hand
ELSE 0
END
) aging_stock,
sum(
CASE
WHEN color = 'Black' THEN stock_on_hand
ELSE 0
END
) black_stock,
sum(
CASE
WHEN color = 'Black' THEN stock_on_hand * volume_cbm
ELSE 0
END
) black_stock_volume,
sum(
CASE
WHEN color = 'Black' THEN stock_on_hand * listing_price_usd
ELSE 0
END
) black_stock_value,
sum(stock_on_hand) total_stock_on_hand,
sum(stock_on_hand * volume_cbm) total_stock_volume,
sum(stock_on_hand * listing_price_usd) total_stock_value
FROM
inv
LEFT JOIN (
SELECT
DISTINCT grass_region,
sku_id,
volume_cbm
FROM
sku_dim
) v ON inv.grass_region = v.grass_region
AND inv.sku_id = v.sku_id
WHERE
stock_on_hand > 0
GROUP BY
1,
2
) inv
LEFT JOIN cnt ON inv.grass_date = cnt.grass_date
AND inv.grass_region = cnt.grass_region
),
live_model AS (
SELECT
DISTINCT grass_region,
shopid,
grass_date,
itemid,
sku_id,
l60_gross_itemsold,
avg_sku_age
FROM
inv
WHERE
fe_stock > 0
AND stock_on_hand > 0
AND sku_status_normal = 1
UNION
SELECT
DISTINCT grass_region,
shop_id AS shopid,
grass_date,
item_id AS itemid,
concat(
CAST(item_id AS VARCHAR),
'_',
CAST(model_id AS VARCHAR)
) sku_id,
0 AS l60_gross_itemsold,
0 AS avg_sku_age
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND fulfilment_source IN (
'FULFILLED_BY_SHOPEE',
'FULFILLED_BY_LOCAL_SELLER'
)
AND tz_type = 'local'
AND grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
AND is_placed = 1
AND is_cb_shop = 1
),
shop AS (
--daily live shops
SELECT
DISTINCT s.grass_region,
s.grass_date,
s.shop_id,
s.pff_tag
FROM
regcbbi_general.cbwh_shop_history_v2 s
JOIN (
SELECT
DISTINCT shopid,
grass_date
FROM
live_model
) inv ON s.shop_id = inv.shopid
AND s.grass_date = inv.grass_date
WHERE
s.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
AND s.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND holiday_mode_on = False
AND user_status = 1
AND shop_status = 1
AND active_item_with_stock_cnt > 0 -- AND date_add('day', 7, last_login) >= s.grass_date
AND shop_id NOT IN (
426379311,
445279067,
426377685,
445275287,
446091597,
446089250
) --lovito
),
shop_summary AS (
SELECT
s1.grass_region,
s1.grass_date,
count(DISTINCT s1.shop_id) live_shop,
count(
DISTINCT CASE
WHEN s1.pff_tag = 1 THEN s1.shop_id
ELSE NULL
END
) live_pff_shop,
count(
DISTINCT CASE
WHEN is_official_shop = 1 THEN s1.shop_id
ELSE NULL
END
) live_mall_shop
FROM
shop s1
LEFT JOIN (
SELECT
DISTINCT grass_date,
shop_id,
is_official_shop
FROM
mp_user.dim_shop__reg_s0_live
WHERE
is_cb_shop = 1
AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND tz_type = 'local'
AND grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
) s2 ON s1.grass_date = s2.grass_date
AND s1.shop_id = s2.shop_id
GROUP BY
1,
2
),
pff_skus AS (
-- PFF skus
SELECT
DISTINCT cast(shop_id AS bigint) AS shop_id,
cast(item_id AS bigint) AS item_id,
fbs_tag
FROM
sbs_mart.shopee_scbs_db__sku_tab__reg_daily_s0_live
WHERE
fbs_tag = 1
AND grass_schema = 'shopee_scbs_db'
),
sku_summary AS (
SELECT
i.grass_region,
i.grass_date,
count(DISTINCT itemid) live_sku,
count(
DISTINCT CASE
WHEN l60_gross_itemsold > 0 THEN itemid
ELSE NULL
END
) effective_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age > 180 THEN itemid
ELSE NULL
END
) aging_live_sku,
count(
DISTINCT CASE
WHEN ps.fbs_tag = 1
AND s.pff_tag = 1 THEN itemid
ELSE NULL
END
) live_pff_sku
FROM
live_model i
JOIN shop s ON i.shopid = s.shop_id
AND i.grass_date = s.grass_date
LEFT JOIN pff_skus ps ON i.shopid = ps.shop_id
AND i.itemid = ps.item_id
GROUP BY
1,
2
),
orders AS (
SELECT
grass_region,
grass_date,
sum(order_fraction) country_gross_order,
sum(
CASE
WHEN is_cb_shop = 1 THEN order_fraction
ELSE 0
END
) cb_gross_order,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source IN (
'FULFILLED_BY_SHOPEE',
'FULFILLED_BY_LOCAL_SELLER'
) THEN order_fraction
ELSE 0
END
) cbwh_gross_order,
sum(gmv_usd) country_gross_gmv,
sum(
CASE
WHEN is_cb_shop = 1 THEN gmv_usd
ELSE 0
END
) cb_gross_gmv,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source IN (
'FULFILLED_BY_SHOPEE',
'FULFILLED_BY_LOCAL_SELLER'
) THEN gmv_usd
ELSE 0
END
) cbwh_gross_gmv,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND pff_tag = 1 THEN order_fraction
ELSE 0
END
) cbwh_pff_gross_order,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source IN (
'FULFILLED_BY_SHOPEE',
'FULFILLED_BY_LOCAL_SELLER'
) THEN item_amount
ELSE 0
END
) cbwh_gross_item_sold,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source IN (
'FULFILLED_BY_SHOPEE',
'FULFILLED_BY_LOCAL_SELLER'
)
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN order_fraction
ELSE 0
END
) cbwh_net_order,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source IN (
'FULFILLED_BY_SHOPEE',
'FULFILLED_BY_LOCAL_SELLER'
)
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN gmv_usd
ELSE 0
END
) cbwh_net_gmv,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source IN (
'FULFILLED_BY_SHOPEE',
'FULFILLED_BY_LOCAL_SELLER'
)
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN item_amount
ELSE 0
END
) cbwh_net_item_sold,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER' THEN order_fraction
ELSE 0
END
) cbwh_ust_gross_order,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN order_fraction
ELSE 0
END
) cbwh_ust_net_order,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER' THEN gmv_usd
ELSE 0
END
) cbwh_ust_gross_gmv,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN gmv_usd
ELSE 0
END
) cbwh_ust_net_gmv,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER' THEN item_amount
ELSE 0
END
) cbwh_ust_gross_item_sold,
sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN item_amount
ELSE 0
END
) cbwh_ust_net_item_sold
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
LEFT JOIN (
SELECT
DISTINCT shop_id,
pff_tag
FROM
regcbbi_general.cbwh_shop_history_v2
WHERE
grass_date = current_date - INTERVAL '1' DAY
) s ON o.shop_id = s.shop_id
WHERE
o.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND tz_type = 'local'
AND grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
AND is_placed = 1
AND is_bi_excluded = 0
AND o.shop_id NOT IN (
426379311,
445279067,
426377685,
445275287,
446091597,
446089250
) --lovito
GROUP BY
1,
2
),
-- cfs_order is missing Hannah: it shows normal
fbs_split AS (
SELECT
grass_region,
grass_date,
sum(
CASE
WHEN campaign = 0
AND flash_sale = 0 THEN order_fraction
ELSE 0
END
) AS cbwh_organic_order,
sum(
CASE
WHEN campaign = 1
AND flash_sale = 0 THEN order_fraction
ELSE 0
END
) AS cbwh_campaign_order,
sum(
CASE
WHEN flash_sale = 1 THEN order_fraction
ELSE 0
END
) AS cbwh_cfs_order
FROM
(
SELECT
DISTINCT a.order_id,
a.shop_id,
a.item_id,
a.model_id,
a.level1_category,
a.order_fraction,
a.bundle_deal_id,
a.bundle_order_item_id,
a.add_on_deal_id,
a.is_add_on_sub_item,
a.group_id,
a.group_buy_deal_id,
a.grass_date,
a.grass_region,
CASE
WHEN b.item_id IS NOT NULL THEN 1
ELSE 0
END AS campaign,
CASE
WHEN a.item_promotion_source = 'flash_sale' THEN 1
ELSE 0
END AS flash_sale
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live a
LEFT JOIN (
SELECT
0 collection_id,
0 item_id,
0 start_time,
0 end_time
-- FROM regbida_userbehavior.shopee_bi_campaign_item
--WHERE grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
) b ON a.item_id = b.item_id
AND a.create_timestamp BETWEEN b.start_time
AND b.end_time
WHERE
a.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND is_cb_shop = 1
AND tz_type = 'local'
AND grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
AND is_placed = 1
AND shop_id NOT IN (
426379311,
445279067,
426377685,
445275287,
446091597,
446089250
) --lovito
)
GROUP BY
1,
2
),


-- Hannah: split nnwh orders to spwh and ust（3pf）
nnwh_orders as (
select o.grass_region, 
o.grass_date,
sum(
CASE
WHEN is_cb_shop = 1 and fulfilment_source = 'FULFILLED_BY_SHOPEE'
THEN order_fraction
ELSE 0
END
) nnwh_spwh_gross_order,

sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
THEN order_fraction
END
) nnwh_ust_gross_order,


sum(
CASE
WHEN is_cb_shop = 1 and fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN order_fraction

ELSE 0
END
) nnwh_spwh_net_order,
sum(
CASE
WHEN is_cb_shop = 1 and fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) THEN order_fraction

ELSE 0
END
) nnwh_ust_net_order,


sum(
CASE
WHEN is_cb_shop = 1 and fulfilment_source = 'FULFILLED_BY_SHOPEE'
THEN gmv_usd
ELSE 0
END
) nnwh_spwh_gross_gmv,

sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
then gmv_usd
END
) nnwh_ust_gross_gmv,


sum(
CASE
WHEN is_cb_shop = 1 and fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) 
THEN gmv_usd
ELSE 0
END
) nnwh_spwh_net_gmv,

sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15)
then gmv_usd
END
) nnwh_ust_net_gmv,


sum(
CASE
WHEN is_cb_shop = 1 and fulfilment_source = 'FULFILLED_BY_SHOPEE'
THEN item_amount
ELSE 0
END
) nnwh_spwh_gross_item_sold,

sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
then item_amount
END
) nnwh_ust_gross_item_sold,


sum(
CASE
WHEN is_cb_shop = 1 and fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) 
THEN item_amount
ELSE 0
END
) nnwh_spwh_net_item_sold,

sum(
CASE
WHEN is_cb_shop = 1
AND fulfilment_source = 'FULFILLED_BY_LOCAL_SELLER'
AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) 
then item_amount
END
) nnwh_ust_net_item_sold
from 
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
-- LEFT JOIN (
-- SELECT
-- DISTINCT 
-- grass_date, 
-- shop_id,
-- pff_tag
-- FROM
-- regcbbi_general.cbwh_shop_history_v2
-- where grass_region in ('TH','VN')
-- and warehouse_id = 'CNN'
-- WHERE
-- grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
-- AND date_add('day', -1, current_date)
-- and 
-- ) s ON o.shop_id = s.shop_id and s.grass_date = o.grass_date 
WHERE
o.grass_region IN ('TH', 'VN')
-- only select nnwh orders
and o.warehouse_code = 'CNN'
AND tz_type = 'local'
AND grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
AND is_placed = 1
AND is_bi_excluded = 0
GROUP BY
1,
2


),




replenish AS (
SELECT
i.grass_region,
i.grass_date,
count(DISTINCT i.sku_id) to_replenish
FROM
(
SELECT
DISTINCT grass_region,
grass_date,
sku_id
FROM
inv
WHERE
fe_stock = 0
AND sku_status_normal = 1
) i
JOIN (
SELECT
DISTINCT grass_region,
date_add('day', 1, grass_date) next_date,
sku_id,
sku_order,
sum(sku_order) over (
PARTITION by grass_region,
sku_id
ORDER BY
grass_date ROWS BETWEEN 59 preceding
AND current ROW
) l60d_orders
FROM
(
SELECT
i.grass_region,
i.grass_date,
i.sku_id,
sum(order_fraction) sku_order
FROM
(
SELECT
*
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND tz_type = 'local'
AND grass_date >= date_add(
'day',
-60,
date_trunc('month', date_add('day', -1, current_date))
)
AND is_placed = 1
AND is_cb_shop = 1
) o
RIGHT JOIN (
SELECT
DISTINCT grass_region,
grass_date,
sku_id
FROM
sbs_mart.shopee_bd_sbs_mart_v2
WHERE
grass_date >= date_add(
'day',
-60,
date_trunc('month', date_add('day', -1, current_date))
)
AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND is_cb = 1
) i ON o.grass_date = i.grass_date
AND concat(
cast(o.item_id AS VARCHAR),
'_',
cast(o.model_id AS VARCHAR)
) = i.sku_id
GROUP BY
1,
2,
3
)
) o ON i.grass_region = o.grass_region
AND i.sku_id = o.sku_id
AND i.grass_date = o.next_date
LEFT JOIN live_model l ON i.grass_region = l.grass_region
AND i.sku_id = l.sku_id
AND i.grass_date = l.grass_date
WHERE
l.sku_id IS NULL
AND l60d_orders IS NOT NULL
GROUP BY
1,
2
),
live_model_summary AS (
SELECT
grass_region,
grass_date,
count(DISTINCT sku_id) live_skuid
FROM
live_model
GROUP BY
1,
2
),
putaway_sku_tab AS (
SELECT
DISTINCT whs_id,
sheet_id,
sku_id,
mtime -3600 AS mtime,
recv_count
FROM
wms_mart.shopee_wms_id_db__putaway_sku_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('IDC', 'IDG')
UNION
SELECT
DISTINCT whs_id,
sheet_id,
sku_id,
mtime -3600 AS mtime,
recv_count
FROM
wms_mart.shopee_wms_th_db__putaway_sku_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('THA')
UNION
SELECT
DISTINCT whs_id,
sheet_id,
sku_id,
mtime,
recv_count
FROM
wms_mart.shopee_wms_my_db__putaway_sku_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('MYC', 'MYD')
UNION
SELECT
DISTINCT whs_id,
sheet_id,
sku_id,
mtime,
recv_count
FROM
wms_mart.shopee_wms_sg_db__putaway_sku_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('SGD', 'SGL')
UNION
SELECT
DISTINCT whs_id,
sheet_id,
sku_id,
mtime,
recv_count
FROM
wms_mart.shopee_wms_ph_db__putaway_sku_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('PHN')
),
inbound AS (
SELECT
DISTINCT country grass_region,
last_putaway_date grass_date,
sum(putaway_count) daily_inbound_qty
FROM
(
SELECT
sheet_id,
sku_id,
sum(recv_count) putaway_count,
date(from_unixtime(max(mtime))) last_putaway_date
FROM
putaway_sku_tab
GROUP BY
1,
2
) b
JOIN (
SELECT
DISTINCT country,
category_id_l1,
sku_id
FROM
sbs_mart.shopee_scbs_db__sku_tab__reg_daily_s0_live
WHERE
cb_option = 1
AND country IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
) c ON b.sku_id = c.sku_id
WHERE
putaway_count > 0
AND last_putaway_date >= date_trunc('month', date_add('day', -1, current_date))
GROUP BY
1,
2
),
ads AS (
SELECT
a.grass_date,
a.grass_region,
sum(impression_cnt) ads_impression,
sum(click_cnt) ads_click,
sum(order_cnt) ads_order,
sum(ads_gmv_amt_usd) ads_gmv,
sum(expenditure_amt_usd) ads_expenditure
FROM
mp_paidads.dws_advertise_query_gmv_event_1d__reg_s0_live a
JOIN shop ON a.shop_id = shop.shop_id
AND a.grass_date = shop.grass_date
WHERE
a.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
AND tz_type = 'local'
AND a.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
GROUP BY
1,
2
),
traffic AS (
SELECT
v.grass_region,
v.grass_date,
1.00 * sum(
CASE
WHEN wh.itemid IS NOT NULL THEN views
ELSE 0
END
) cbwh_view,
1.00 * sum(
CASE
WHEN v.is_cross_border = 1 THEN views
ELSE 0
END
) cb_view,
1.00 * sum(views) country_view
FROM
(
SELECT
grass_region,
grass_date,
shop_id,
item_id,
is_cross_border,
sum(pv_cnt_1d) AS views
FROM
mp_shopflow.dws_item_civ_1d__reg_s0_live --shopee.traffic_mart_dws__item_exposure_1d
WHERE
tz_type = 'local'
AND grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date))
AND date_add('day', -1, current_date)
AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND shop_id NOT IN (
426379311,
445279067,
426377685,
445275287,
446091597,
446089250
) --lovito
GROUP BY
1,
2,
3,
4,
5
) v
LEFT JOIN (
SELECT
DISTINCT grass_region,
cast(itemid AS BIGINT) itemid
FROM
inv
) wh ON v.grass_region = wh.grass_region
AND v.item_id = wh.itemid -- LEFT JOIN (
-- SELECT
-- DISTINCT cast(shopid AS BIGINT) shopid
-- FROM
-- shopee.shopee_regional_bi_team__excluded_shop_tab
-- WHERE
-- ingestion_timestamp = (
-- SELECT
-- max(ingestion_timestamp) col
-- FROM
-- shopee.shopee_regional_bi_team__excluded_shop_tab
-- )
-- ) exc ON v.shop_id = exc.shopid
-- WHERE
-- exc.shopid IS NULL
GROUP BY
1,
2
),
/**
fs AS (
SELECT
date(from_unixtime(start_time - 3600)) grass_date,
itemid,
count(DISTINCT promotionid) slots
FROM
marketplace.shopee_promotion_backend_id_db__promotion_flash_sale_item_tab__reg_daily_s0_live
WHERE
date(from_unixtime(start_time - 3600)) BETWEEN date_trunc('month', current_date - interval '1' day)
AND current_date - interval '1' day
AND STATUS = 1
GROUP BY
1,
2
UNION
ALL
SELECT
date(from_unixtime(start_time)) grass_date,
itemid,
count(DISTINCT promotionid) slots
FROM
marketplace.shopee_promotion_backend_my_db__promotion_flash_sale_item_tab__reg_daily_s0_live
WHERE
date(from_unixtime(start_time)) BETWEEN date_trunc('month', current_date - interval '1' day)
AND current_date - interval '1' day
AND STATUS = 1
GROUP BY
1,
2
UNION
ALL
SELECT
date(from_unixtime(start_time)) grass_date,
itemid,
count(DISTINCT promotionid) slots
FROM
marketplace.shopee_promotion_backend_ph_db__promotion_flash_sale_item_tab__reg_daily_s0_live
WHERE
date(from_unixtime(start_time)) BETWEEN date_trunc('month', current_date - interval '1' day)
AND current_date - interval '1' day
AND STATUS = 1
GROUP BY
1,
2
UNION
ALL
SELECT
date(from_unixtime(start_time - 3600)) grass_date,
itemid,
count(DISTINCT promotionid) slots
FROM
marketplace.shopee_promotion_backend_th_db__promotion_flash_sale_item_tab__reg_daily_s0_live
WHERE
date(from_unixtime(start_time - 3600)) BETWEEN date_trunc('month', current_date - interval '1' day)
AND current_date - interval '1' day
AND STATUS = 1
GROUP BY
1,
2
UNION
ALL
SELECT
date(from_unixtime(start_time - 14 * 3600)) grass_date,
itemid,
count(DISTINCT promotionid) slots
FROM
marketplace.shopee_promotion_backend_mx_db__promotion_flash_sale_item_tab__reg_daily_s0_live
WHERE
date(from_unixtime(start_time - 14 * 3600)) BETWEEN date_trunc('month', current_date - interval '1' day)
AND current_date - interval '1' day
AND STATUS = 1
GROUP BY
1,
2
UNION
ALL
SELECT
date(from_unixtime(start_time - 3600)) grass_date,
itemid,
count(DISTINCT promotionid) slots
FROM
marketplace.shopee_promotion_backend_vn_db__promotion_flash_sale_item_tab__reg_daily_s0_live
WHERE
date(from_unixtime(start_time - 3600)) BETWEEN date_trunc('month', current_date - interval '1' day)
AND current_date - interval '1' day
AND STATUS = 1
GROUP BY
1,
2
),
**/
fs AS (
select
date(start_datetime) grass_date,
item_id itemid,
count(DISTINCT (item_id, start_timestamp)) slots
from
mp_promo.dim_flash_sale_sku__reg_s0_live
where
1 = 1
and grass_date = current_date - interval '1' day
and date(start_datetime) BETWEEN date_trunc('month', current_date - interval '1' day)
AND current_date - interval '1' day
AND model_status_id = 1
group by
1,
2
),
slots AS (
SELECT
grass_region,
fs.grass_date,
sum(slots) cbwh_cfs_slots
FROM
fs
JOIN (
SELECT
DISTINCT grass_region,
grass_date,
itemid
FROM
inv
WHERE
is_cb = 1
) sku ON fs.itemid = sku.itemid
AND fs.grass_date = sku.grass_date
GROUP BY
1,
2
)
SELECT
a.grass_date,
a.grass_region,
cbwh_gross_order,
cbwh_net_order,
cbwh_gross_gmv,
cbwh_net_gmv,
cbwh_cfs_slots,
cbwh_cfs_order,
cbwh_campaign_order,
cbwh_organic_order,
cb_gross_order,
cb_gross_gmv,
country_gross_order,
country_gross_gmv,
cbwh_pff_gross_order,
cbwh_gross_order - cbwh_pff_gross_order cbwh_non_pff_gross_order,
cbwh_gross_item_sold,
cbwh_net_item_sold,
cbwh_view,
cb_view,
country_view,
live_shop,
live_pff_shop,
live_shop - coalesce(live_pff_shop, 0) live_non_pff_shop,
live_sku,
live_pff_sku,
live_sku - coalesce(live_pff_sku, 0) live_non_pff_sku,
live_mall_shop,
sku_with_stock_cnt,
total_stock_on_hand,
total_stock_volume,
total_stock_value,
effective_live_sku,
live_skuid,
to_replenish,
coverage_days,
daily_inbound_qty,
aging_live_sku,
aging_stock,
black_stock_volume,
black_stock_value,
ads_expenditure,
ads_order,
ads_gmv,
try(
1.0000 * coalesce(live_skuid, 0) / nullif(
coalesce(live_skuid, 0) + coalesce(to_replenish, 0),
0
)
) ATP,
try(1.0000 * cbwh_gross_item_sold / cbwh_gross_order) item_per_order,
try(1.0000 * cbwh_gross_order / cb_gross_order) cbwh_over_cb_order,
try(1.0000 * cbwh_gross_gmv / cb_gross_gmv) cbwh_gmv_over_cb,
try(1.0000 * cbwh_gross_gmv / cbwh_gross_order) cbwh_abs,
try(1.0000 * cb_gross_gmv / cb_gross_order) cb_abs,
try(1.0000 * country_gross_gmv / country_gross_order) country_abs,
try(1.0000 * cbwh_gross_order / cbwh_view) cbwh_cr,
try(1.0000 * cb_gross_order / cb_view) cb_cr,
try(1.0000 * country_gross_order / country_view) country_cr,
try(1.0000 * ads_expenditure / nullif(ads_order, 0)) CPO,
try(1.0000 * ads_expenditure / nullif(ads_click, 0)) CPC,
try(1.0000 * ads_click / nullif(ads_impression, 0)) CTR,
try(1.0000 * ads_gmv / nullif(ads_expenditure, 0)) ROI,
cbwh_ust_gross_order,
cbwh_ust_net_order,
cbwh_ust_gross_gmv,
cbwh_ust_net_gmv,
cbwh_ust_gross_item_sold,
cbwh_ust_net_item_sold,
nnwh_spwh_gross_order,
nnwh_spwh_net_order,
nnwh_spwh_gross_gmv,
nnwh_spwh_net_gmv,
nnwh_spwh_gross_item_sold,
nnwh_spwh_net_item_sold,
nnwh_ust_gross_order,
nnwh_ust_net_order,
nnwh_ust_gross_gmv,
nnwh_ust_net_gmv,
nnwh_ust_gross_item_sold,
nnwh_ust_net_item_sold
FROM
orders a
LEFT JOIN fbs_split b ON a.grass_region = b.grass_region
AND a.grass_date = b.grass_date
LEFT JOIN inv_summary c ON a.grass_region = c.grass_region
AND a.grass_date = c.grass_date
LEFT JOIN shop_summary d ON a.grass_region = d.grass_region
AND a.grass_date = d.grass_date
LEFT JOIN sku_summary e ON a.grass_region = e.grass_region
AND a.grass_date = e.grass_date
LEFT JOIN live_model_summary f ON a.grass_region = f.grass_region
AND a.grass_date = f.grass_date
LEFT JOIN replenish g ON a.grass_region = g.grass_region
AND a.grass_date = g.grass_date
LEFT JOIN inbound h ON a.grass_region = h.grass_region
AND a.grass_date = h.grass_date
LEFT JOIN ads i ON a.grass_region = i.grass_region
AND a.grass_date = i.grass_date
LEFT JOIN traffic j ON a.grass_region = j.grass_region
AND a.grass_date = j.grass_date
LEFT JOIN slots k ON a.grass_region = k.grass_region
AND a.grass_date = k.grass_date
left join nnwh_orders nn
on nn.grass_date = a.grass_date
and nn.grass_region = a.grass_region