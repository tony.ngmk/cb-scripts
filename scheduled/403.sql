insert into nexus.test_2f22065ed8ff9e9090fde69dbf9a228a3b1b6b1dfe77d907f35c934beb4e9d30_d39eab20ff26762aad6ba8712f73757a WITH item AS (
SELECT COALESCE(TRY(SPLIT(kpi_cate [1], ':') [2]), 'L1_Others') AS level1_cat_name,
COALESCE(TRY(SPLIT(kpi_cate [2], ':') [2]), 'L2_Others') AS level2_cat_name,
COALESCE(TRY(SPLIT(kpi_cate [3], ':') [2]), 'L3_Others') AS level3_cat_name,
item_id,
s1.shop_id,
create_timestamp,
s2.is_cb_shop AS cb_option,
price AS item_price
FROM (
SELECT *
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region = 'TW'
AND (grass_date = DATE('2020-11-11') OR grass_date = DATE('2021-04-04') OR grass_date = DATE('2021-10-10') OR grass_date = DATE('2021-09-09') OR grass_date = current_date - interval '1' day)
AND STATUS = 1
AND seller_status = 1
AND stock > 0
) AS s1
JOIN (
SELECT *
FROM mp_user.dim_shop__reg_s0_live
WHERE grass_region = 'TW'
AND (grass_date = DATE('2020-11-11') OR grass_date = DATE('2021-04-04') OR grass_date = DATE('2021-10-10') OR grass_date = DATE('2021-09-09') OR grass_date = current_date - interval '1' day)
AND STATUS = 1
AND (
is_holiday_mode IS NULL
OR is_holiday_mode = 0
)
AND tz_type = 'local'
) AS s2 ON s1.shop_id = s2.shop_id
CROSS JOIN UNNEST(kpi_categories) AS t(kpi_cate)
),

z1 AS (
SELECT DISTINCT shop_id,
user_id
FROM mp_user.dim_user__reg_s0_live
WHERE grass_region = 'TW'
AND (grass_date = DATE('2020-11-11') OR grass_date = DATE('2021-04-04') OR grass_date = DATE('2021-10-10') OR grass_date = DATE('2021-09-09') OR grass_date = current_date - interval '1' day)
AND tz_type = 'local'
AND STATUS = 1
),

z2 AS (
SELECT DISTINCT user_id
FROM mp_user.ads_activeness_user_activity_td__reg_s0_live
WHERE grass_region = 'TW'
AND tz_type = 'local'
AND (DATE(from_unixtime(last_login_timestamp_td)) = DATE('2020-11-11') 
OR DATE(from_unixtime(last_login_timestamp_td)) = DATE('2021-04-04') 
OR DATE(from_unixtime(last_login_timestamp_td)) = DATE('2021-10-10') 
OR DATE(from_unixtime(last_login_timestamp_td)) = DATE('2021-09-09') 
OR DATE(from_unixtime(last_login_timestamp_td)) = current_date - interval '1' day)
),

z3 AS (
SELECT z1.*
FROM z1
JOIN z2 
ON z1.user_id = z2.user_id
GROUP BY 1, 2
),

fin AS (
SELECT item.*
FROM item
JOIN z3 
ON item.shop_id = z3.shop_id
WHERE item.level1_cat_name IN ('Mobile & Gadgets'
, 'Home Electronic'
, 'Hardware & 3C'
, 'Women''s Apparel'
, 'Sports & Outdoors'
, 'Women Accessories'
, 'Women Bags'
, 'Men''s Bags& Accessories'
, 'Shoes'
, 'Home & Living'
, 'Health & Beauty'
, 'Mother & Baby'
, 'Pets'
, 'Motors'
, 'Game Kingdom'
, 'Life & Entertainment')
),


-- Order details
ord AS (
SELECT item_id
, shop_id
, COALESCE(TRY(SPLIT(kpi_cate [1], ':') [2]), 'L1_Others') AS level1_cat_name
, COALESCE(TRY(SPLIT(kpi_cate [2], ':') [2]), 'L2_Others') AS level2_cat_name
, COALESCE(TRY(SPLIT(kpi_cate [3], ':') [2]), 'L3_Others') AS level3_cat_name
, CAST(COALESCE(TRY(SPLIT(kpi_cate [3], ':') [1]), '0') AS BIGINT) AS level3_cat_id
, is_cb_shop
, grass_date
, item_price_pp as item_price
, SUM(order_fraction) AS order_fraction
, SUM(gmv_usd) AS gmv_usd
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
CROSS JOIN UNNEST(kpi_categories) AS t(kpi_cate)
WHERE grass_region = 'TW'
AND is_placed = 1
AND grass_date BETWEEN date_add('day', - 7, CURRENT_DATE) AND date_add('day', - 1, CURRENT_DATE)

GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9
),


kpi_factor AS (
SELECT item_id
, level3_kpi_category_id
, level3_kpi_category_fraction_factor
FROM mp_item.dwd_item_kpi_category_factor_df__reg_s0_live
WHERE grass_date = date_add('day', - 1, CURRENT_DATE)
AND kpi_category_level = 3
AND grass_region = 'TW'
),


-- Setting price range
live AS (
(SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_10' AS percentile
, 0 AS lower_range
, APPROX_PERCENTILE(item_price, 0.1) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4, 5

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_20' AS percentile
, APPROX_PERCENTILE(item_price, 0.1) AS lower_range
, APPROX_PERCENTILE(item_price, 0.2) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_30' AS percentile, APPROX_PERCENTILE(item_price, 0.2) AS lower_range, APPROX_PERCENTILE(item_price, 0.3) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_40' AS percentile
, APPROX_PERCENTILE(item_price, 0.3) AS lower_range
, APPROX_PERCENTILE(item_price, 0.4) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_50' AS percentile
, APPROX_PERCENTILE(item_price, 0.4) AS lower_range
, APPROX_PERCENTILE(item_price, 0.5) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_60' AS percentile
, APPROX_PERCENTILE(item_price, 0.5) AS lower_range
, APPROX_PERCENTILE(item_price, 0.6) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_70' AS percentile
, APPROX_PERCENTILE(item_price, 0.6) AS lower_range
, APPROX_PERCENTILE(item_price, 0.7) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_80' AS percentile
, APPROX_PERCENTILE(item_price, 0.7) AS lower_range
, APPROX_PERCENTILE(item_price, 0.8) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_90' AS percentile
, APPROX_PERCENTILE(item_price, 0.8) AS lower_range
, APPROX_PERCENTILE(item_price, 0.9) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4

UNION

SELECT level1_cat_name
, level2_cat_name
, level3_cat_name
, 'percentile_100' AS percentile
, APPROX_PERCENTILE(item_price, 0.9) AS lower_range
, APPROX_PERCENTILE(item_price, 1) AS higher_range
FROM fin
GROUP BY 1, 2, 3, 4
) 
ORDER BY 1, 2, 3, 4
) 


SELECT i.level1_cat_name
, i.level2_cat_name
, i.level3_cat_name
, percentile
, lower_range
, higher_range
, SUM(CASE WHEN is_cb_shop = 1 THEN ord.order_fraction * level3_kpi_category_fraction_factor END) / 7.00 AS L7D_CB_orders
, SUM(CASE WHEN is_cb_shop = 0 THEN ord.order_fraction * level3_kpi_category_fraction_factor END) / 7.00 AS L7D_local_orders
, SUM(CASE WHEN is_cb_shop = 1 THEN ord.gmv_usd * level3_kpi_category_fraction_factor END) / 7.00 AS L7D_CB_GMV
, SUM(CASE WHEN is_cb_shop = 0 THEN ord.gmv_usd * level3_kpi_category_fraction_factor END) / 7.00 AS L7D_Local_GMV
FROM live i
LEFT JOIN (SELECT * 
FROM ord 
WHERE level1_cat_name IN ('Mobile & Gadgets'
, 'Home Electronic'
, 'Hardware & 3C'
, 'Women''s Apparel'
, 'Sports & Outdoors'
, 'Women Accessories'
, 'Women Bags'
, 'Men''s Bags& Accessories'
, 'Shoes'
, 'Home & Living'
, 'Health & Beauty'
, 'Mother & Baby'
, 'Pets'
, 'Motors'
, 'Game Kingdom'
, 'Life & Entertainment')
) ord
ON ord.level1_cat_name = i.level1_cat_name
AND ord.level2_cat_name = i.level2_cat_name
AND ord.level3_cat_name = i.level3_cat_name
AND ord.item_price BETWEEN i.lower_range AND i.higher_range
LEFT JOIN kpi_factor k 
ON k.level3_kpi_category_id = ord.level3_cat_id
AND ord.item_id = k.item_id
GROUP BY 1, 2, 3, 4, 5, 6
ORDER BY 1, 2, 3, 4, 5