insert into nexus.test_f1ad0a9233201cc51a8637eb234c86a267fd202d0bc69903ecc4a9a7c0db9210_2dd537f4b72bf1043155b3e531f6bee3 WITH sf_raw as (
SELECT 
a.grass_region 
, a.grass_date 
, a.shopid 
, a.orderid
, a.rule_name 
, case when (
(
grass_region = 'SG'
AND cast(rule_id as int) in (12426)
)
OR 
(
grass_region = 'MY'
AND rule_name like '%FSS CCB%'
)
OR
(
grass_region not in ('SG', 'MY', 'PH')
AND (rule_name like '%FSC%' 
or rule_name like '%535%'
or rule_name like '%321%'
or rule_name like '%CCB and FSS%')
)
--or (sip.shopid is not null and sip.grass_region = 'SG')
)
then 1 else 0 end as if_FSC
, case when (
(
grass_region = 'SG'
AND cast(rule_id as int) in (12429)
)
OR (
grass_region = 'MY'
AND (rule_name like '%FSS%' OR rule_name like '%Dday%')
AND rule_name not like '%CCB%'
)
OR (
grass_region = 'PH'
AND (rule_name like '%FSS%' OR rule_name like '%PSP%')
AND rule_name not like '%CCB%'
) 
OR (
grass_region not in ('SG', 'MY', 'PH')
AND (
rule_name like '%FSS%' 
or lower(rule_name) like '%cn exclusivity%'
or lower(rule_name) like '%free shipping campaign%'
)
and rule_name not like '%CCB%'
)
)
then 1 else 0 end as if_FSS
, case when (
(
grass_region = 'SG'
AND cast(rule_id as int) in (583, 721, 1696, 10237, 16151, 16157)
)
OR (
grass_region = 'MY'
AND rule_name like '%CCB%'
AND rule_name not like '%FSS%'
)
OR (
grass_region = 'PH'
AND rule_name like '%CCB%'
AND rule_name not like '%FSS%'
) 
OR (
grass_region not in ('SG', 'MY', 'PH')
AND (
rule_name like '%CCB%' 
or rule_name like '%CBXTRA%'
) 
and rule_name not like '%FSS%'
)
)
then 1 else 0 end as if_CCB
, case when (rule_name not like '%FSS%' 
and lower(rule_name) not like '%cn exclusivity%'
and lower(rule_name) not like '%free shipping campaign%'
and rule_name not like '%PSP%'
and rule_name not like '%Dday%'
and rule_name not like '%CCB and FSS%' 
and rule_name not like '%CCB%' 
and rule_name not like '%CBXTRA%' 
and rule_name not like '%FSC%' 
and rule_name not like '%535%' 
and rule_name not like '%321%'
and (grass_region = 'SG' and cast(rule_id as int) not in (12426, 12429, 583, 721, 1696, 10237, 16151, 16157)) 
)
--and (sip.shopid is null or sip.grass_region != 'SG')
THEN 1 else 0 end as if_others
FROM 
(
SELECT distinct 
o.grass_region
, o.grass_date
, o.shopid
, o.orderid
, r1.rule_name 
, r1.rule_id
--, sum(fee_amount) service_fee 
FROM 
(
(
select 
grass_region 
, date(from_unixtime(create_timestamp)) as grass_date
, shop_id as shopid
, order_id as orderid
, json_extract_scalar(service_fee_info_list,'$[0].rule_id') as rule_id
--, sum(cast(json_extract_scalar(service_fee_info_list,'$[0].fee_amt') as double)) as fee_amount 
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where tz_type = 'local'
and is_cb_shop = 1
and grass_date >= date_add('day', -90, current_date)
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)
) 
UNION ALL
(
select 
grass_region 
, date(from_unixtime(create_timestamp)) as grass_date
, shop_id as shopid
, order_id as orderid
, json_extract_scalar(service_fee_info_list,'$[1].rule_id') as rule_id
--, sum(cast(json_extract_scalar(service_fee_info_list,'$[1].fee_amt') as double)) as fee_amount 
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where tz_type = 'local'
and is_cb_shop = 1
and grass_date >= date_add('day', -90, current_date)
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)
) 
UNION ALL
(
select 
grass_region 
, date(from_unixtime(create_timestamp)) as grass_date
, shop_id as shopid
, order_id as orderid
, json_extract_scalar(service_fee_info_list,'$[2].rule_id') as rule_id
--, sum(cast(json_extract_scalar(service_fee_info_list,'$[2].fee_amt') as double)) as fee_amount 
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where tz_type = 'local'
and is_cb_shop = 1
and grass_date >= date_add('day', -90, current_date)
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)
)
UNION ALL
(
select 
grass_region 
, date(from_unixtime(create_timestamp)) as grass_date
, shop_id as shopid
, order_id as orderid
, json_extract_scalar(service_fee_info_list,'$[3].rule_id') as rule_id
--, sum(cast(json_extract_scalar(service_fee_info_list,'$[3].fee_amt') as double)) as fee_amount 
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where tz_type = 'local'
and is_cb_shop = 1
and grass_date >= date_add('day', -90, current_date)
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)
)
) o
LEFT JOIN 
(
SELECT distinct 
rule_id
, rule_name
--, cast(fee_rate as double)/10000000 as fee_rate 
FROM marketplace.shopee_service_fee_rule_db__service_fee_rule_tab__reg_daily_s0_live
) r1 on cast(o.rule_id as int) = r1.rule_id


) a
LEFT JOIN
(
SELECT distinct b.affi_shopid as shopid, b.mst_shopid, a.country as mst_country
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b on b.mst_shopid = a.shopid
WHERE a.cb_option = 1 and b.grass_region != ''
) sip on a.shopid = sip.shopid
WHERE a.grass_region in ('SG', 'MY', 'TW', 'ID', 'TH', 'PH', 'VN', 'BR', 'MX')
)




, order_data as (
SELECT 
a.grass_region
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) AND if_FSC =1 then a.orderid else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_FSC_ADO
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) AND if_FSC =1 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_FSC_ADG
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) AND if_FSC =1 then a.orderid else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_FSC_ADO 
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) AND if_FSC =1 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_FSC_ADG 


, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) AND if_FSS =1 then a.orderid else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_FSS_ADO
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) AND if_FSS =1 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_FSS_ADG
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) AND if_FSS =1 then a.orderid else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_FSS_ADO 
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) AND if_FSS =1 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_FSS_ADG 


, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) AND if_CCB =1 then a.orderid else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CCB_ADO
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) AND if_CCB =1 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CCB_ADG
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) AND if_CCB =1 then a.orderid else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CCB_ADO 
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) AND if_CCB =1 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CCB_ADG 


FROM sf_raw r
LEFT JOIN 
(
select 
grass_region 
--, date(from_unixtime(create_timestamp)) as grass_date
, shop_id as shopid
, order_id as orderid
, gmv_usd 
, order_fraction
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where tz_type = 'local'
and is_cb_shop = 1
and grass_date >= date_add('day', -90, current_date)
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)


) a on r.orderid = a.orderid
GROUP BY 1
)


----------------------------------------------shop count-----------------------------------------
, shop_raw as 
(
select 
grass_region
, shopid
, sum(case when start_date <= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) and end_date >= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) then if_FSC else 0 end) as M1_fsc
, sum(case when start_date <= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) and end_date >= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) then if_CCB else 0 end) as M1_ccb
, sum(case when start_date <= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) and end_date >= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) then if_FSS else 0 end) as M1_fss
, sum(case when start_date <= date_add('day', -1, current_date) and end_date >= date_add('day', -1, current_date) then if_FSC else 0 end) as MTD_fsc
, sum(case when start_date <= date_add('day', -1, current_date) and end_date >= date_add('day', -1, current_date) then if_CCB else 0 end) as MTD_ccb
, sum(case when start_date <= date_add('day', -1, current_date) and end_date >= date_add('day', -1, current_date) then if_FSS else 0 end) as MTD_fss
from 
(
select distinct r1.region as grass_region
, r1.rule_id
, shop_id as shopid 
, case when (
(
r1.region = 'SG'
AND cast(r1.rule_id as int) in (12426)
)
OR
(
r1.region = 'MY'
AND rule_name like '%FSS CCB%'
)
OR
(
r1.region not in ('SG', 'MY', 'PH')
AND (r1.rule_name like '%FSC%' 
or r1.rule_name like '%535%'
or r1.rule_name like '%321%'
or r1.rule_name like '%CCB and FSS%')
)
--or (sip.shopid is not null and sip.grass_region = 'SG')
)
then 1 else 0 end as if_FSC
, case when (
(
r1.region = 'SG'
AND cast(r1.rule_id as int) in (12429)
)
OR (
r1.region = 'MY'
AND (r1.rule_name like '%FSS%' OR r1.rule_name like '%Dday%')
AND r1.rule_name NOT like '%CCB%'
)
OR (
r1.region = 'PH'
AND (r1.rule_name like '%FSS%' OR r1.rule_name like '%PSP%')
AND r1.rule_name NOT like '%CCB%'
) 
OR (
r1.region not in ('SG', 'MY', 'PH')
AND (
r1.rule_name like '%FSS%' 
or lower(r1.rule_name) like '%cn exclusivity%'
or lower(r1.rule_name) like '%free shipping campaign%'
)
and r1.rule_name not like '%CCB%'
)
)
then 1 else 0 end as if_FSS
, case when (
(
r1.region = 'SG'
AND cast(r1.rule_id as int) in (583, 721, 1696, 10237, 16151, 16157)
)
OR (
r1.region = 'MY'
AND r1.rule_name like '%CCB%'
AND r1.rule_name NOT like '%FSS%'
)
OR (
r1.region = 'PH'
AND r1.rule_name like '%CCB%'
AND r1.rule_name NOT like '%FSS%'
) 
OR (
r1.region not in ('SG', 'MY', 'PH')
AND (
r1.rule_name like '%CCB%' 
or r1.rule_name like '%CBXTRA%'
) 
and r1.rule_name not like '%FSS%'
)
)
then 1 else 0 end as if_CCB
, date(from_unixtime(r2.start_time)) as start_date
, date(from_unixtime(r2.end_time)) as end_date
from marketplace.shopee_service_fee_rule_db__service_fee_rule_tab__reg_daily_s0_live r1 
join marketplace.shopee_service_fee_rule_shop_db__service_fee_rule_shop_tab__reg_daily_s0_live r2 on r1.rule_id = r2.rule_id
where (
(r1.rule_name like '%FSS%' or r1.rule_name like '%CCB%' or r1.rule_name like '%FSC%' or r1.rule_name like '%CCB and FSS%' 
or r1.rule_name like '%CBXTRA%' or r1.rule_name like '%535%' or r1.rule_name like '%321%' 
or lower(r1.rule_name) like '%cn exclusivity%' or lower(r1.rule_name) like '%free shipping campaign%' 
or rule_name like '%PSP%' or rule_name like '%Dday%')
or (r2.rule_id in (12426, 12429, 583, 721, 1696, 10237, 16151, 16157))
)
and date(from_unixtime(r2.start_time)) <= date_add('day', -1, current_date)
and date(from_unixtime(r2.end_time)) >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) -- Start of M-1
and r1.rule_status = 1
)
group by 1,2
)


, shop_data AS (
SELECT sr.grass_region
, count(distinct case when M1_fsc =1 then sr.shopid else null end) as M1_FSC_shop
, count(distinct case when M1_fss =1 then sr.shopid else null end) as M1_FSS_shop
, count(distinct case when M1_ccb =1 then sr.shopid else null end) as M1_CCB_shop


, count(distinct case when MTD_fsc =1 then sr.shopid else null end) as MTD_FSC_shop
, count(distinct case when MTD_fss =1 then sr.shopid else null end) as MTD_FSS_shop
, count(distinct case when MTD_ccb =1 then sr.shopid else null end) as MTD_CCB_shop
FROM shop_raw sr
JOIN
(
SELECT distinct grass_region, shop_id, is_cb_shop 
FROM mp_user.dim_shop__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = date_add('day', -1, current_date)
AND status = 1
) s on sr.shopid = s.shop_id
JOIN 
(
SELECT distinct grass_region, shop_id
FROM mp_user.dim_user__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = date_add('day', -1, current_date)
AND status = 1
) u on sr.shopid = u.shop_id
WHERE is_cb_shop = 1
GROUP BY 1
)




SELECT o.grass_region
, MTD_FSC_ADO
, MTD_FSC_ADG
, MTD_FSC_shop


, MTD_FSS_ADO
, MTD_FSS_ADG
, MTD_FSS_shop


, MTD_CCB_ADO
, MTD_CCB_ADG
, MTD_CCB_shop


, M1_FSC_ADO
, M1_FSC_ADG
, M1_FSC_shop


, M1_FSS_ADO
, M1_FSS_ADG
, M1_FSS_shop


, M1_CCB_ADO
, M1_CCB_ADG
, M1_CCB_shop


FROM order_data o 
LEFT JOIN shop_data s ON o.grass_region = s.grass_region