INSERT INTO regcbbi_general.cbwh_stock_history_v2 
SELECT *, '20220621T020000+0000' FROM (
WITH wms_sku_location_tab AS (
SELECT
'ID' as grass_region,
whs_id,
zone_id,
location_id,
sku_id,
count
FROM
wms_mart.shopee_wms_id_db__sku_location_tab__reg_daily_s0_live
WHERE 1=1
--and grass_region = 'ID'
AND whs_id IN ('IDG', 'IDC')
UNION
SELECT
'TH' as grass_region,
whs_id,
zone_id,
location_id,
sku_id,
count
FROM
wms_mart.shopee_wms_th_db__sku_location_tab__reg_daily_s0_live
WHERE 1=1
--and grass_region = 'TH'
AND whs_id IN ('THA')
UNION
SELECT
'MY' as grass_region,
whs_id,
zone_id,
location_id,
sku_id,
count
FROM
wms_mart.shopee_wms_my_db__sku_location_tab__reg_daily_s0_live
WHERE 1=1
--and grass_region = 'MY'
AND whs_id IN ('MYC', 'MYD')
UNION
SELECT
'SG' as grass_region,
whs_id,
zone_id,
location_id,
sku_id,
count
FROM
wms_mart.shopee_wms_sg_db__sku_location_tab__reg_daily_s0_live
WHERE 1=1
--and grass_region = 'SG'
AND whs_id IN ('SGD', 'SGL')
UNION
SELECT
'PH' as grass_region,
whs_id,
zone_id,
location_id,
sku_id,
count
FROM
wms_mart.shopee_wms_ph_db__sku_location_tab__reg_daily_s0_live
WHERE 1=1
--and grass_region = 'PH'
AND whs_id IN ('PHN')
),
/* sku dim*/
sku_cache_tab AS(
SELECT
DISTINCT 'ID' AS grass_region,
whs_id,
shopid,
sku_id
FROM
wms_mart.shopee_wms_id_db__sku_cache_tab__reg_daily_s0_live
WHERE
whs_id IN ('IDG', 'IDC')
UNION
SELECT
DISTINCT 'TH' AS grass_region,
whs_id,
shopid,
sku_id
FROM
wms_mart.shopee_wms_th_db__sku_cache_tab__reg_daily_s0_live
WHERE
whs_id IN ('THA')
UNION
SELECT
DISTINCT 'MY' AS grass_region,
whs_id,
shopid,
sku_id
FROM
wms_mart.shopee_wms_my_db__sku_cache_tab__reg_daily_s0_live
WHERE
whs_id IN ('MYC', 'MYD')
UNION
SELECT
DISTINCT 'SG' AS grass_region,
whs_id,
shopid,
sku_id
FROM
wms_mart.shopee_wms_sg_db__sku_cache_tab__reg_daily_s0_live
WHERE
whs_id IN ('SGD', 'SGL')
UNION
SELECT
DISTINCT 'PH' AS grass_region,
whs_id,
shopid,
sku_id
FROM
wms_mart.shopee_wms_ph_db__sku_cache_tab__reg_daily_s0_live
WHERE
whs_id IN ('PHN')
),
sku_location AS (
-- sku location details
SELECT
DISTINCT a.grass_region,
shopid,
a.sku_id,
a.whs_id,
count,
CASE
WHEN a.grass_region = 'ID'
AND location_id IN ('IDG-DA-01-01-3-01', 'IDG-DA-01-01-6-01') THEN 'Missing'
WHEN a.grass_region = 'ID'
AND (
substr(location_id, 5, 2) IN ('RD', 'RO')
OR location_id IN ('IDG-DA-01-01-2-01')
) THEN 'Damage'
WHEN a.grass_region = 'TH'
AND substr(location_id, 5, 2) IN ('XX') THEN 'Missing'
WHEN a.grass_region = 'TH'
AND substr(location_id, 5, 2) IN ('DA') THEN 'Damage'
WHEN a.grass_region = 'PH'
AND substr(location_id, 5, 2) IN ('AV', 'XX') THEN 'Missing'
WHEN a.grass_region = 'PH'
AND substr(location_id, 5, 2) IN ('DA') THEN 'Damage'
WHEN a.grass_region = 'MY'
AND a.whs_id = 'MYC'
AND substr(location_id, 5, 2) IN ('AV', 'MS', 'IB') THEN 'Missing'
WHEN a.grass_region = 'MY'
AND a.whs_id = 'MYC'
AND substr(location_id, 5, 2) IN ('DG') THEN 'Damage'
WHEN a.grass_region = 'MY'
AND a.whs_id = 'MYL'
AND substr(location_id, 5, 2) IN ('AV', 'MS') THEN 'Missing'
WHEN a.grass_region = 'MY'
AND a.whs_id = 'MYL'
AND substr(location_id, 5, 2) IN ('DG') THEN 'Damage'
WHEN a.grass_region = 'MY'
AND a.whs_id = 'MYD'
AND substr(location_id, 5, 2) IN ('MS') THEN 'Missing'
WHEN a.grass_region = 'MY'
AND a.whs_id = 'MYD'
AND substr(location_id, 5, 2) IN ('DG') THEN 'Damage'
WHEN a.grass_region = 'SG'
AND location_id IN ('SGL-DM-01-01-1-01') THEN 'Damage'
WHEN a.grass_region = 'SG'
AND location_id IN ('SGL-AV-01-01-1-01') THEN 'Missing'
WHEN substr(location_id, 5, 2) IN (
'RI',
'RD',
'TE',
'RS',
'UD',
'AV',
'IS',
'DA',
'TS',
'RO',
'EX',
'AG'
) THEN 'Not Available' -- reg Ops
ELSE 'Normal'
END AS location_tag
FROM
wms_sku_location_tab a
JOIN sku_cache_tab b ON a.grass_region = b.grass_region
AND a.sku_id = b.sku_id
WHERE
count > 0
AND a.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG')
),
sku_stock_summary AS (
-- sku stock summary
SELECT
grass_region,
shopid,
sku_id,
whs_id,
location_tag,
sum(
CASE
WHEN location_tag = 'Damage' THEN count
ELSE 0
END
) AS damaged_stock,
sum(
CASE
WHEN location_tag = 'Missing' THEN count
ELSE 0
END
) AS missing_stock,
sum(
CASE
WHEN location_tag = 'Normal' THEN count
ELSE 0
END
) AS normal_stock
FROM
sku_location
GROUP BY
1,
2,
3,
4,
5
),
shop_info AS (
-- shop and user info
SELECT
DISTINCT warehouse_id AS shop_whs_id,
shop_id,
username,
ggp_account_name,
ggp_account_owner
FROM
regcbbi_general.cbwh_shop_history_v2
WHERE
grass_date = current_date - INTERVAL '1' DAY
AND (
holiday_mode_on = false
OR holiday_mode_on IS NULL
)
AND user_status = 1
AND shop_status = 1
),
item_info AS (
-- model level info
SELECT
DISTINCT i.shop_id AS shopid,
i.item_id AS itemid,
i.model_id AS modelid,
i.sku_id,
IF(
i.old_model_id = 0,
concat(
cast(i.item_id AS varchar),
'_',
cast(coalesce(i.old_model_id, i.model_id) AS varchar)
),
sku_id
) AS old_sku_id,
i.model_stock,
i.item_stock,
i.model_status,
i.item_status,
i.old_model_id
FROM
mp_item.dim_model__reg_s0_live i
JOIN (
SELECT
DISTINCT shop_id
FROM
shop_info
) u ON i.shop_id = u.shop_id
WHERE
i.is_cb_shop = 1
AND i.grass_region IN ('ID', 'MY', 'PH', 'TH', 'TW', 'SG')
AND i.tz_type = 'local' -- AND grass_date = date_add('DAY', -1, CURRENT_DATE)
AND i.grass_date = current_date - INTERVAL '1' DAY
AND i.item_status > 0
)
/*To reorder columns*/
SELECT
current_date - INTERVAL '1' DAY AS grass_date,
grass_region,
shop_id,
shop_whs_id,
username,
ggp_account_name,
ggp_account_owner,
sku_id,
whs_id,
itemid,
modelid,
model_stock,
item_stock,
model_status,
item_status,
damaged_stock,
missing_stock,
normal_stock,
location_tag
FROM
(
SELECT
DISTINCT -- date_add('day', -1, current_date) grass_date
current_date - INTERVAL '1' DAY AS grass_date,
t.grass_region,
s.shop_id,
s.shop_whs_id,
s.username,
s.ggp_account_name,
s.ggp_account_owner,
t.sku_id,
t.whs_id,
t.itemid,
t.modelid,
t.model_status,
t.item_status,
t.location_tag,
sum(t.model_stock) AS model_stock,
sum(t.item_stock) AS item_stock,
sum(t.damaged_stock) AS damaged_stock,
sum(t.missing_stock) AS missing_stock,
sum(t.normal_stock) AS normal_stock
FROM
(
/*Need to union due to SKU ID migration*/
SELECT
sk.shopid,
i.sku_id,
sk.grass_region,
sk.whs_id,
i.itemid,
i.modelid,
sk.location_tag,
cast(i.model_stock AS integer) AS model_stock,
cast(i.item_stock AS integer) AS item_stock,
cast(i.model_status AS integer) AS model_status,
cast(i.item_status AS integer) AS item_status,
sk.damaged_stock,
sk.missing_stock,
sk.normal_stock
FROM
sku_stock_summary sk
JOIN item_info i ON i.old_sku_id = sk.sku_id
WHERE
i.old_model_id = 0
UNION
ALL
SELECT
sk.shopid,
i.sku_id,
sk.grass_region,
sk.whs_id,
i.itemid,
i.modelid,
sk.location_tag,
cast(i.model_stock AS integer) AS model_stock,
cast(i.item_stock AS integer) AS item_stock,
cast(i.model_status AS integer) AS model_status,
cast(i.item_status AS integer) AS item_status,
sk.damaged_stock,
sk.missing_stock,
sk.normal_stock
FROM
sku_stock_summary sk
JOIN item_info i ON i.sku_id = sk.sku_id
) t
JOIN shop_info s ON t.shopid = s.shop_id
AND t.whs_id = s.shop_whs_id
GROUP by
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14
)
)