INSERT INTO regcbbi_others.cb_logistics_mart_ph_2 
SELECT * FROM (
SELECT distinct ord_v4.grass_region
, ord_v4.order_placed_local_date
, ord_v4.orderid
, ord_v4.ordersn
, ord_v4.shopid
, ord_v4.buyer_userid
, ord_v4.seller_userid
, ord_v4.cancel_userid
, ord_log.buyer_address_state
, ord_log.buyer_address_city
, ord_log.buyer_shipping_address
, ord_log.buyer_address_zipcode
, ord_log.buyer_address_district
, ord_log.buyer_address_town
, coalesce(sls1.client_channel_id, ord_log.channel_id) AS channel_id
, CASE WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (88001, 88011, 88022, 28016, 48002, 18025, 18028, 78004, 78005, 38012, 38020, 38011, 38024, 58007, 90001, 90002, 18055, 100001, 28052, 108001, 110001, 120001, 78014, 38025, 148001, 148002, 48008, 158001, 168001, 168002, 18099, 78017, 58008, 148003, 158002, 58012, 38066, 148006, 148007) then 'CN'
WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (88021, 18036, 48005, 28050, 58009, 98002, 38015, 38016, 78016, 108004, 108005, 148004, 148005, 38026) then 'KR'
WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (48006, 28051, 18053, 78020, 58010, 98001, 108002, 118001, 128001) then 'ID'
WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (18043, 18042, 28043, 28042, 88042) then 'TW'
WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (18052, 18054, 48010) then 'MY'
WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (28053, 18056, 48007) then 'VN'
WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (18045) then 'JP'
WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (18057, 28054) then 'TW'
WHEN coalesce(sls1.client_channel_id, ord_log.channel_id) in (28055, 18058, 48009) then 'TH'
ELSE NULL
END AS integrated_channel_origin
, ord_log.shipping_carrier
, ord_v4.shipping_category
, ord_v4.status_ext
, ord_log.logistics_status
, ord_log.admin_asf
, sls1.sls_asf
, ord_v4.payment_method
, ord_v4.cancel_reason
, ord_v4.create_time
, ord_v4.pay_time
, ord_v4.shipping_confirm_time
, ord_log.days_to_ship
, ord_log.ship_by_date
, ord_log.expected_receive_time
, ord_log.cdt_with_ext
, ord_log.thirdparty_pickup_time
, ord_log.acl1
, ord_log.acl2
, ord_log.acl3
, CASE WHEN ord_log.channel_id in (18018, 18020, 28027, 79008, 39012, 49003) then coalesce(sls2.sea_shipout_time, sls1.ctime, log_aud.pickup_done_time)
ELSE coalesce(sls1.ctime, log_aud.seller_shipout_time)
END AS seller_shipout_time
, CASE WHEN ord_log.channel_id in (18018, 18020, 28027, 79008, 39012, 49003) then sls2.sea_wh_inbound_time
ELSE log_aud.pickup_done_time
--ELSE coalesce(sls2.fm_pickup_done_time, sls2.wh_inbound_time, sls1.wh_inbound_time, log_aud.pickup_done_time)
END AS pickup_done_time
, CASE WHEN ord_log.channel_id in (18018, 18020, 28027, 79008, 39012, 49003) then coalesce(sls2.sea_delivery_time, sls2.delivery_time, log_aud.delivery_time)
ELSE coalesce(sls2.delivery_time, log_aud.delivery_time)
END AS delivery_time
, log_aud.pickup_failed_time
, log_aud.delivery_failed_time
, log_aud.lost_time
-- , IF(ord_v4.pay_time is NULL, ord_v4.cancel_time, NULL) AS invalid_time
-- , IF(ord_v4.pay_time is NOT NULL, ord_v4.cancel_time, NULL) AS cancel_completed_time
, ord_aud.invalid_time
, ord_aud.cancel_completed_time
, ord_aud.return_completed_time
, sls1.log_id
, sls1.consignment_no
, sls1.lm_tracking_number
, sls1.forderid
, coalesce(conso.service_code, sls1.service_code) AS service_code
, sls1.whs_code
, CASE WHEN sls1.whs_code = 'TWS01' then 'shenzhen'
WHEN sls1.whs_code = 'ECP04' then 'yiwu'
WHEN sls1.whs_code = 'ECP02' then 'shanghai'
WHEN sls1.whs_code = 'ECP03' then 'quanzhou'
END AS whs_location
-- , sls_loc.location AS whs_location
, CASE WHEN ord_log.channel_id in (18018, 18020, 28027, 79008, 39012, 49003) then sls2.sea_wh_inbound_time
ELSE coalesce(sls2.wh_inbound_time, sls1.wh_inbound_time)
END AS wh_inbound_time
, CASE WHEN ord_log.channel_id in (18018, 18020, 28027, 79008, 39012, 49003) then sls2.sea_wh_outbound_time
ELSE coalesce(sls2.wh_outbound_time, sls1.wh_outbound_time, conso.wh_outbound_time)
END AS wh_outbound_time
, sls2.line_haul_departure_time -- for PL D2D mode only
, sls2.dest_country_arrival_time --for BR and MX only
, sls2.lm_cc_done_time
, CASE WHEN ord_log.channel_id in (18018, 18020, 28027, 79008, 39012, 49003) then sls2.sea_lm_inbound_time
WHEN ord_log.channel_id in (18043, 18042, 28043, 28042, 88042) then sls2.twsip_lm_inbound_time
WHEN ord_log.channel_id = 18028 then coalesce(sls2.rb_lm_inbound_time, sls2.lm_inbound_time_1, sls2.lm_inbound_time_2) --SG Economy oversize orders from Wise warehouse are given to Roadbull offline, thus no status integration
ELSE coalesce(sls2.lm_inbound_time_1, sls2.lm_inbound_time_2)
END AS lm_inbound_time_actual
, CASE WHEN ord_log.channel_id in (18018, 18020, 28027, 79008, 39012, 49003) then sls2.sea_lm_inbound_time
WHEN ord_log.channel_id in (18043, 18042, 28043, 28042, 88042) then sls2.twsip_lm_inbound_time
WHEN ord_log.channel_id = 18028 then coalesce(sls2.rb_lm_inbound_time, sls2.lm_inbound_time_1, sls2.lm_inbound_time_2) --SG Economy oversize orders from Wise warehouse are given to Roadbull offline, thus no status integration
WHEN ord_v4.grass_region != 'BR' then coalesce(sls2.lm_inbound_time_1, sls2.lm_inbound_time_2)
ELSE coalesce(sls2.lm_cc_done_time, sls2.lm_inbound_time_1, sls2.lm_inbound_time_2)
END AS lm_inbound_time
, coalesce(sls2.first_delivery_attempt_time, sls2.sea_delivery_time, log_aud.delivery_time)
AS first_delivery_attempt_time
, sls2.deliver_to_store_time
, sls2.return_initiated_time
, sls1.actual_weight
, sls1.charged_weight
, sls1.volume_weight
, sls1.volume_factor
, sls1.actual_length
, sls1.actual_width
, sls1.actual_height
, sls1.shipment_provider
, conso.conso_group_sls_tn
FROM
(SELECT grass_region
, CASE when grass_region in ('ID') then DATE(from_unixtime(create_time, 'Asia/Jakarta'))
when grass_region in ('TH') then DATE(from_unixtime(create_time, 'Asia/Bangkok'))
when grass_region in ('VN') then DATE(from_unixtime(create_time, 'Asia/Ho_Chi_Minh'))
when grass_region = 'BR' then DATE(from_unixtime(create_time, 'America/Sao_Paulo'))
when grass_region = 'MX' then DATE(from_unixtime(create_time, 'America/Mexico_City'))
when grass_region = 'CL' then DATE(from_unixtime(create_time, 'America/Santiago')) --Chile
when grass_region = 'CO' then DATE(from_unixtime(create_time, 'America/Bogota')) --Colombia
when grass_region in ('PL', 'ES', 'FR') then DATE(from_unixtime(create_time, 'CET')) --EU
when grass_region in ('IN') then DATE(from_unixtime(create_time, 'Asia/Kolkata')) --India
else DATE(from_unixtime(create_time)) --SG timezone for SG/MY/TW/PH and unknown countries
END AS order_placed_local_date
, orderid
, ordersn
, shopid
, userid AS buyer_userid
, status_ext
, extinfo.cancel_reason
, payment_method
, create_time
, IF(pay_time > 0, pay_time, NULL) AS pay_time
, IF(cancel_time > 0, cancel_time, NULL) AS cancel_time
, IF(shipping_confirm_time > 0, shipping_confirm_time, NULL) AS shipping_confirm_time
, IF(shipping_method > 0, 'Integrated', 'Non-Integrated') AS shipping_category
, extinfo.cancel_userid
, extinfo.seller_userid
FROM marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE cb_option = 1
AND IF(grass_region NOT in ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
, create_time >= to_unixtime(date_add('day', -149, date(date_parse('20220620','%Y%m%d'))))
, create_time >= to_unixtime(date_add('day', -209, date(date_parse('20220620','%Y%m%d'))))
)
AND grass_region = 'PH'
) ord_v4


LEFT JOIN
(SELECT grass_region
, order_id
, logistics_status
, extinfo.channel_id
, shipping_carrier
, CAST(extinfo.carrier_shipping_fee as DOUBLE)/100000 AS admin_asf
, extinfo.days_to_ship
, extinfo.ship_by_date
, extinfo.expected_receive_time
, extinfo.cdt_with_ext
, extinfo.thirdparty_pickup_time --used by Reg Ops for autocancellation
, extinfo.auto_cancel_arrange_ship_date AS acl1
, extinfo.auto_cancel_3pl_ack_date AS acl2
, extinfo.auto_cancel_arrive_tws_date AS acl3
, extinfo.buyer_address.state AS buyer_address_state
, extinfo.buyer_address.city AS buyer_address_city
, extinfo.buyer_address.address AS buyer_shipping_address
, extinfo.buyer_address.zipcode AS buyer_address_zipcode
, extinfo.buyer_address.district AS buyer_address_district
, extinfo.buyer_address.town AS buyer_address_town
FROM marketplace.shopee_order_logistics_v2_db__order_logistics_v2_tab__reg_daily_s0_live
WHERE cb_option = 1
AND IF(grass_region NOT in ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
, create_time >= to_unixtime(date_add('day', -149, date(date_parse('20220620','%Y%m%d'))))
, create_time >= to_unixtime(date_add('day', -209, date(date_parse('20220620','%Y%m%d'))))
)
AND grass_region = 'PH'
) ord_log
ON ord_v4.orderid = ord_log.order_id


LEFT JOIN
(SELECT grass_region
, orderid
, min(IF(new_status = 1, ctime, NULL)) AS seller_shipout_time
, min(IF(new_status = 2, ctime, NULL)) AS pickup_done_time
, min(IF(new_status = 5, ctime, NULL)) AS delivery_time
, min(IF(new_status = 4, ctime, NULL)) AS pickup_failed_time
, min(IF(new_status = 6, ctime, NULL)) AS delivery_failed_time
, min(IF(new_status = 11, ctime, NULL)) AS lost_time
FROM marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_continuous_s0_live
WHERE IF(grass_region NOT in ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
, ctime >= to_unixtime(date_add('day', -149, date(date_parse('20220620','%Y%m%d'))))
, ctime >= to_unixtime(date_add('day', -209, date(date_parse('20220620','%Y%m%d')))) 
)
AND grass_region = 'PH'
GROUP BY 1, 2
) log_aud
ON ord_v4.orderid = log_aud.orderid


LEFT JOIN 
(
SELECT grass_region
, orderid
, min(IF(new_status = 6, ctime, NULL)) AS invalid_time
, min(IF(new_status = 8, ctime, NULL)) AS cancel_completed_time
, min(IF(new_status = 10, ctime, NULL)) AS return_completed_time
FROM marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_continuous_s0_live
WHERE IF(grass_region NOT in ('BR', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
, ctime >= to_unixtime(date_add('day', -149, date(date_parse('20220620','%Y%m%d'))))
, ctime >= to_unixtime(date_add('day', -209, date(date_parse('20220620','%Y%m%d'))))
)
AND grass_region = 'PH'
GROUP BY 1, 2
-- SELECT grass_region
-- , orderid
-- , IF(return_info.accepted_time > 0, return_info.accepted_time, NULL) AS return_completed_time
-- FROM marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
-- WHERE grass_region = 'PH'
) ord_aud 
ON ord_v4.orderid = ord_aud.orderid


LEFT JOIN 
(
SELECT log_id, ordersn, forderid, consignment_no, lm_tracking_number, service_code, whs_code
, ctime
, actual_shipping_fee AS sls_asf
, IF(wh_inbound_date > 0, wh_inbound_date, NULL) AS wh_inbound_time
, IF(wh_outbound_date > 0, wh_outbound_date, NULL) AS wh_outbound_time
, IF(actual_weight > 0, actual_weight, NULL) AS actual_weight, IF(charged_weight > 0, charged_weight, NULL) AS charged_weight
, IF(volume_weight > 0, volume_weight, NULL) AS volume_weight, IF(volume_factor > 0, volume_factor, NULL) AS volume_factor
, IF(actual_length > 0, actual_length, NULL) AS actual_length, IF(actual_width > 0, actual_width, NULL) AS actual_width, IF(actual_height > 0, actual_height, NULL) AS actual_height
, 'PH' AS grass_region
, cast(client_channel_id AS BIGINT) AS client_channel_id
, shipment_provider
FROM sls_mart.shopee_sls_logistic_ph_db__logistic_request_tab_lfs_union_tmp
WHERE ctime >= to_unixtime(date_add('day', -149, date(date_parse('20220620','%Y%m%d')))) AND cb_flag = 1 AND cast(forderid AS BIGINT) != 0 AND is_return = 0
AND tz_type = 'local'
AND grass_date = date(date_parse('20220620','%Y%m%d'))
) sls1 
ON sls1.ordersn = ord_v4.ordersn


LEFT JOIN
(
SELECT 'PH' AS grass_region, log_id
, min(IF(tracking_code in ('F096', 'F099'), update_time, NULL)) AS fm_pickup_done_time
, min(IF(tracking_code = 'F200', update_time, NULL)) AS wh_inbound_time
, min(IF(tracking_code = 'F299', update_time, NULL)) AS wh_outbound_time
, min(IF(action_status = 29, update_time, NULL)) AS line_haul_departure_time
, min(IF(action_status = 30, update_time, NULL)) AS dest_country_arrival_time --only for BR and MX
, min(IF(action_status in (16), update_time, NULL)) AS lm_cc_done_time --removed action status 28 because it is been found to be used for export CC done for VN
, min(IF(status in (8, 9, 10, 13) AND substr(tracking_code, 1, 2) NOT in ('F0', 'F1', 'F2'), update_time, NULL)) AS lm_inbound_time_1 -- F099 means picked up from CB seller; it is been found to correspond to status 8/action_status 17
, min(IF(action_status in (17, 18, 19, 22) AND substr(tracking_code, 1, 2) NOT in ('F0', 'F1', 'F2'), update_time, NULL)) AS lm_inbound_time_2
, min(IF(status in (11, 13) OR tracking_code = 'F601', update_time, NULL)) AS first_delivery_attempt_time
, min(IF(status = 11, update_time, NULL)) AS delivery_time
, min(IF(status in (12, 18, 19, 20, 22), update_time, NULL)) AS return_initiated_time


, min(IF(tracking_code = 'F601', update_time, NULL)) AS deliver_to_store_time
, cast(NULL AS DOUBLE) AS twsip_lm_inbound_time
, cast(NULL AS DOUBLE) AS rb_lm_inbound_time


, min(IF(regexp_like(lower(message), 'seller updated that your parcel has been shipped'), update_time, NULL)) AS sea_shipout_time
, min(IF(regexp_like(lower(message), 'your parcel has been successfully inbounded at the huixiang guangzhou warehouse'), update_time, NULL)) AS sea_wh_inbound_time
, min(IF(regexp_like(lower(message), 'your parcel has departed from huixiang guangzhou warehouse'), update_time, NULL)) AS sea_wh_outbound_time
, min(IF(regexp_like(lower(message), 'accepted_to_warehouse'), update_time, NULL)) AS sea_lm_inbound_time
, min(IF(regexp_like(lower(message), 'delivery_successful'), update_time, NULL)) AS sea_delivery_time


FROM sls_mart.shopee_sls_logistic_ph_db__logistic_tracking_tab_lfs_union_tmp
WHERE update_time >= to_unixtime(date_add('day', -179, date(date_parse('20220620','%Y%m%d'))))
AND tz_type = 'local'
AND grass_date = date(date_parse('20220620','%Y%m%d'))
GROUP BY 1, 2
) sls2 
ON sls2.log_id = sls1.log_id


LEFT JOIN
(SELECT m.service_code
, c.sls_tracking_number as consignment_no
, m.handovered_time AS wh_outbound_time
, m.group_id AS conso_group_sls_tn
FROM 
(SELECT service_code, group_id, handovered_time
FROM tws_mart.shopee_tws_sz_db__transit_conso_group_tab__reg_daily_s0_live
WHERE handovered_time >= to_unixtime(date_add('day', -239, date(date_parse('20220620','%Y%m%d'))))
AND order_quantity >= 2
AND group_id is NOT NULL
AND group_id != ''
AND grass_region != ''
) m 
JOIN 
(SELECT sls_tracking_number, group_id
FROM tws_mart.shopee_tws_sz_db__transit_conso_order_tab__reg_daily_s0_live
WHERE ctime >= to_unixtime(date_add('day', -239, date(date_parse('20220620','%Y%m%d'))))
AND sls_tracking_number is not NULL
AND group_id is NOT NULL
AND group_id != ''
AND whs_id != 'TWQZ'
) c
ON m.group_id = c.group_id
) conso
ON sls1.consignment_no = conso.consignment_no 
)