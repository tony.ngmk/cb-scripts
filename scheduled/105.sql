-- exclude batch 4 
-- split by seller type 
--- split by organic 
--- exclude dday 


with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, date'2022-06-13' offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and date(from_unixtime(offboard_time)) != date'2022-01-11'
) t2 ON t1.shopid = t2.mst_shopid
)


, t as (select 
sip.affi_shopid 
, date(offboard_time) offboard_time
, batch 
, mst_shopid 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT)
where a.batch in ('mass return batch1')
)


-- select * 
-- from t 
, cb as (SELECT DISTINCT (CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN (LOWER(child_account_owner_userrole_name) LIKE '%lt%' or LOWER(child_account_owner_userrole_name) LIKE '%css%') THEN 'Long Tail'
ELSE 'Others' END) seller_type 
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, grass_region
, child_account_name
, child_account_owner
, CAST(child_shopid AS BIGINT) child_shopid
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
)


-- AND is_flash_sale = 0 AND item_promotion_source <> 'shopee' --organic orders only 


, traffic_by_day AS ( --L30D/
SELECT
case when grass_date < date'2022-06-13' then 'before'
when grass_date between date'2022-06-13' and date'2022-06-13' + interval'1'day then 'return'
else 'after' end as time_range 
-- , case when grass_date < date'2021-12-30' then concat('D-',cast(date_diff('day', grass_date, date'2021-12-30' ) as varchar ))
-- when grass_date =date'2021-12-30' then 'D-0'
-- when grass_date = date'2021-12-30' + interval'1'day then 'D+0'
-- else concat('D+',cast(date_diff('day', date'2021-12-30' +interval'1'day , grass_date) as varchar )) end as period
, seller_type 
-- , tt.grass_region
, shop_id
, tt.grass_region
, t.batch 
, SUM(tt.imp_cnt_1d) AS imp_cnt_1d
, SUM(tt.pv_cnt_1d) AS pv_cnt_1d
, SUM(tt.clk_cnt_1d) AS clk_cnt_1d
FROM (
SELECT distinct 
grass_date, grass_region, shop_id, item_id, 
imp_cnt_1d, pv_cnt_1d, clk_cnt_1d
FROM mp_shopflow.dws_item_civ_1d__reg_s0_live a 
join sip on affi_shopid = a.shop_id 
join t on t.affi_shopid = sip.affi_shopid 
WHERE tz_type = 'local' AND grass_date BETWEEN t.offboard_time- interval'6'day AND t.offboard_time + interval'30'day 
and grass_date not in ( date'2022-05-05',date'2022-06-06',date'2022-07-07')
AND grass_region IN ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')
) tt 
INNER JOIN sip ON (sip.affi_shopid = tt.shop_id)
join t on t.affi_shopid = tt.shop_id 
left join cb on cb.child_shopid = t.mst_shopid 
GROUP BY 1, 2, 3 , 4 , 5 
) 
, day_diff as (
select count(distinct date_id) - 2 day_diff
from regbida_keyreports.dim_date 
where date_id not in ( date'2022-05-05',date'2022-06-06',date'2022-07-07')
and 
date_id between date'2022-06-13' and current_date-interval'1'day 
)






select batch 
, seller_type
, grass_region 
, sum(case when time_range='before' then imp_cnt_1d else null end)/6.00 bef_imp 
, sum(case when time_range='before' then pv_cnt_1d else null end)/6.00 bef_pv
, sum(case when time_range='before' then clk_cnt_1d else null end)/6.00 bef_clk 
, sum(case when time_range='after' then imp_cnt_1d else null end) * decimal'1.000' /day_diff aft_imp 
, sum(case when time_range='after' then pv_cnt_1d else null end)* decimal'1.000' /day_diff aft_pv
, sum(case when time_range='after' then clk_cnt_1d else null end)* decimal'1.000' /day_diff aft_clk 
from traffic_by_day 
join day_diff on 1=1 
group by 1,2 ,3 , day_diff