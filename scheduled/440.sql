insert into nexus.test_ac54a0974e29e44f21ffc9f7ebb2043a2dba54e8a4f1d34385be6b8b759b07ba_563f8ff656c79cfc0453b67e00764c12 WITH supp AS 
(
SELECT *
FROM
(SELECT *
, row_number() over (partition BY ingest_date, service_code, whs_code ORDER BY ingestion_timestamp DESC) AS rn 
FROM
(SELECT coalesce(DATE(date_parse(trim(data_date), '%Y-%m-%d')), DATE(from_unixtime(cast(ingestion_timestamp AS BIGINT)))) AS ingest_date
, trim(service_code) AS service_code
, trim(whs_code) AS whs_code
, trim(first_leg) AS first_leg
, trim(fl_handover) AS fl_handover
, trim(custom_clearance) AS custom_clearance
, trim(lm_handover) AS lm_handover
, trim(last_mile) AS last_mile
, cast(ingestion_timestamp AS BIGINT) AS ingestion_timestamp
FROM regcbbi_others.shopee_regional_cb_team__service_code_supplier_tab_new
WHERE DATE(from_unixtime(cast(ingestion_timestamp AS BIGINT))) >= date_add('day', -179, date(from_unixtime(1656111600)))
)
)
WHERE rn = 1
),

orders AS 
(
SELECT l.integrated_channel_origin
, l.grass_region
, l.orderid
, l.shopid
, l.channel_id
, s.last_mile
, l.service_code
, l.whs_code
, r.shipment_provider
, l.actual_weight
, l.charged_weight
, l.shipping_confirm_time
-- , coalesce(f.fm_pickup_time, l.pickup_done_time) as pickup_done_time
, l.wh_inbound_time
, l.wh_outbound_time
, coalesce(l.deliver_to_store_time, l.delivery_time) AS delivery_time
, l.lm_inbound_time
FROM regcbbi_others.cb_logistics_mart_test l
LEFT JOIN sls_mart.shopee_sls_logistic_sg_db__logistic_request_tab__reg_daily_s0_live r ON l.ordersn = r.ordersn
LEFT JOIN supp s ON DATE(from_unixtime(l.seller_shipout_time)) = s.ingest_date AND l.service_code = s.service_code AND l.whs_code = s.whs_code
AND l.service_code not in ('SOB', 'SOC')
),


integrated AS 
(
SELECT date(from_unixtime(1656111600)) AS data_date
, integrated_channel_origin AS origin_country
, CASE WHEN channel_id = 18025 then 'SG Express'
WHEN channel_id = 18028 AND last_mile = 'SPX' then 'SG Economy'

--WHEN channel_id = 18028 AND service_code in ('SOB', 'SOC') AND shipment_provider = 'Singpost' then 'SG Economy'
WHEN channel_id = 18028 AND service_code not in ('SOB', 'SOC') then 'SG Eco to Exp'
WHEN channel_id = 18055 then 'SG Sea Shipping'
WHEN channel_id = 18099 then 'SG Standard Express - Collection Point'
WHEN channel_id = 28052 then 'MY Sea Shipping'
WHEN channel_id = 38025 then 'TW Economy'
WHEN channel_id = 48008 then 'PH Economy'
ELSE grass_region
END AS grass_region
---yesterday's volume---
, count(distinct IF(DATE(from_unixtime(wh_outbound_time)) = date(from_unixtime(1656111600)), orderid, NULL)) AS shipped_ord
, sum(IF(DATE(from_unixtime(wh_outbound_time)) = date(from_unixtime(1656111600)), coalesce(actual_weight, charged_weight), 0))/1000 AS shipped_weight
, count(distinct IF(DATE(from_unixtime(lm_inbound_time)) = date(from_unixtime(1656111600)), orderid, NULL)) AS lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)), orderid, NULL)) AS delivered_ord
---yesterday's lead time (orders delivered yesterday)---
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS pay_to_whs
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND wh_outbound_time > wh_inbound_time, cast((wh_outbound_time - wh_inbound_time) AS DOUBLE)/86400, NULL)) AS whs_processing
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND lm_inbound_time > wh_outbound_time, cast((lm_inbound_time - wh_outbound_time) AS DOUBLE)/86400, NULL)) AS line_haul
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL)) AS lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS total_lead_time
---MTD total volume---
, count(distinct IF(DATE(from_unixtime(wh_outbound_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)), orderid, NULL)) AS mtd_shipped_ord
, sum(IF(DATE(from_unixtime(wh_outbound_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)), coalesce(actual_weight, charged_weight), 0))/1000 AS mtd_shipped_weight
, count(distinct IF(DATE(from_unixtime(lm_inbound_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)), orderid, NULL)) AS mtd_lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)), orderid, NULL)) AS mtd_delivered_ord
---MTD lead time---
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS mtd_pay_to_whs
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND wh_outbound_time > wh_inbound_time, cast((wh_outbound_time - wh_inbound_time) AS DOUBLE)/86400, NULL)) AS mtd_whs_processing
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND lm_inbound_time > wh_outbound_time, cast((lm_inbound_time - wh_outbound_time) AS DOUBLE)/86400, NULL)) AS mtd_line_haul
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL)) AS mtd_lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS mtd_total_lead_time
---M-1 total volume---
, count(distinct IF(DATE(from_unixtime(wh_outbound_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))), orderid, NULL)) AS m_1_shipped_ord
, sum(IF(DATE(from_unixtime(wh_outbound_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))), coalesce(actual_weight, charged_weight), 0))/1000 AS m_1_shipped_weight
, count(distinct IF(DATE(from_unixtime(lm_inbound_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))), orderid, NULL)) AS m_1_lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))), orderid, NULL)) AS m_1_delivered_ord
---M-1 lead time---
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS m_1_pay_to_whs
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND wh_outbound_time > wh_inbound_time, cast((wh_outbound_time - wh_inbound_time) AS DOUBLE)/86400, NULL)) AS m_1_whs_processing
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND lm_inbound_time > wh_outbound_time, cast((lm_inbound_time - wh_outbound_time) AS DOUBLE)/86400, NULL)) AS m_1_line_haul
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND delivery_time > lm_inbound_time, cast((delivery_time - lm_inbound_time) AS DOUBLE)/86400, NULL)) AS m_1_lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS m_1_total_lead_time
FROM orders
WHERE integrated_channel_origin IS NOT NULL
GROUP BY 1,2,3
),


kr_nonintegrated AS 
(
SELECT date(from_unixtime(1656111600)) AS data_date
, 'KR' AS origin_country
, grass_region
---yesterday's volume---
, count(distinct IF(DATE(from_unixtime(wh_inbound_time)) = date(from_unixtime(1656111600)), orderid, NULL)) AS shipped_ord
, 0 AS shipped_weight
, 0 AS lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)), orderid, NULL)) AS delivered_ord
---yesterday's lead time---
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS pay_to_whs
, 0 AS whs_processing
, 0 AS line_haul
, 0 AS lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS total_lead_time
---MTD total volume---
, count(distinct IF(DATE(from_unixtime(wh_inbound_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)), orderid, NULL)) AS mtd_shipped_ord
, 0 AS mtd_shipped_weight
, 0 AS mtd_lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)), orderid, NULL)) AS mtd_delivered_ord
---MTD lead time---
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS mtd_pay_to_whs
, 0 AS mtd_whs_processing
, 0 AS mtd_line_haul
, 0 AS mtd_lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS mtd_total_lead_time
---M-1 total volume---
, count(distinct IF(DATE(from_unixtime(wh_inbound_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))), orderid, NULL)) AS m_1_shipped_ord
, 0 AS m_1_shipped_weight
, 0 AS m_1_lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))), orderid, NULL)) AS m_1_delivered_ord
---M-1 lead time---
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS m_1_pay_to_whs
, 0 AS m_1_whs_processing
, 0 AS m_1_line_haul
, 0 AS m_1_lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS m_1_total_lead_time
FROM orders
WHERE channel_id in (78003, 58000)
GROUP BY 1,2,3
), 


jpcb AS (
SELECT date(from_unixtime(1656111600)) AS data_date
, 'JP' AS origin_country
, o.grass_region
---yesterday's volume---
, count(distinct IF(DATE(from_unixtime(wh_inbound_time)) = date(from_unixtime(1656111600)), orderid, NULL)) AS shipped_ord
, 0 AS shipped_weight
, 0 AS lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)), orderid, NULL)) AS delivered_ord
---yesterday's lead time---
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS pay_to_whs
, 0 AS whs_processing
, 0 AS line_haul
, 0 AS lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) = date(from_unixtime(1656111600)) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS total_lead_time
---MTD total volume---
, count(distinct IF(DATE(from_unixtime(wh_inbound_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)), orderid, NULL)) AS mtd_shipped_ord
, 0 AS mtd_shipped_weight
, 0 AS mtd_lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)), orderid, NULL)) AS mtd_delivered_ord
---MTD lead time---
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS mtd_pay_to_whs
, 0 AS mtd_whs_processing
, 0 AS mtd_line_haul
, 0 AS mtd_lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) between date_trunc('month', date(from_unixtime(1656111600))) AND date(from_unixtime(1656111600)) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS mtd_total_lead_time
---M-1 total volume---
, count(distinct IF(DATE(from_unixtime(wh_inbound_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))), orderid, NULL)) AS m_1_shipped_ord
, 0 AS m_1_shipped_weight
, 0 AS m_1_lm_inbound_ord
, count(distinct IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))), orderid, NULL)) AS m_1_delivered_ord
---M-1 lead time---
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND wh_inbound_time > shipping_confirm_time, cast((wh_inbound_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS m_1_pay_to_whs
, 0 AS m_1_whs_processing
, 0 AS m_1_line_haul
, 0 AS m_1_lm_delivery
, avg(IF(DATE(from_unixtime(delivery_time)) between date_add('month', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600)))) AND delivery_time > shipping_confirm_time, cast((delivery_time - shipping_confirm_time) AS DOUBLE)/86400, NULL)) AS m_1_total_lead_time
FROM orders o
LEFT JOIN
(SELECT grass_region, cast(child_shopid AS BIGINT) AS shopid
FROM regbd_sf.cb__seller_index_tab
WHERE gp_account_billing_country = 'Japan'
AND grass_region != ''
) s
ON o.grass_region = s.grass_region AND o.shopid = s.shopid
WHERE o.grass_region != 'SG' AND (s.shopid IS NOT NULL OR o.channel_id in (28029, 28041, 38000, 39309, 88000, 88002, 78011, 78010))
GROUP BY 1, 2, 3
),


del_fail as
(select a.*
, b.daily_failed_delivery_qty_MTD
, c.daily_failed_delivery_qty_M1 from 
((select date(from_unixtime(1656111600)) as data_date,
integrated_channel_origin AS origin_country
, CASE WHEN channel_id = 18025 then 'SG Express'
WHEN channel_id = 18028 AND last_mile = 'SPX' then 'SG Economy'

--WHEN channel_id = 18028 AND service_code in ('SOB', 'SOC') AND shipment_provider = 'Singpost' then 'SG Economy'
WHEN channel_id = 18028 AND l.service_code not in ('SOB', 'SOC') then 'SG Eco to Exp'
WHEN channel_id = 18055 then 'SG Sea Shipping'
WHEN channel_id = 18099 then 'SG Standard Express - Collection Point'
WHEN channel_id = 28052 then 'MY Sea Shipping'
WHEN channel_id = 38025 then 'TW Economy'
WHEN channel_id = 48008 then 'PH Economy'
ELSE grass_region
END AS grass_region
, count(distinct l.consignment_no) as daily_failed_delivery_qty
from regcbbi_others.cb_logistics_mart_test l 
--LEFT JOIN sls_mart.shopee_sls_logistic_sg_db__logistic_request_tab__reg_daily_s0_live r ON l.ordersn = r.ordersn
LEFT JOIN supp s ON DATE(from_unixtime(l.seller_shipout_time)) = s.ingest_date AND l.service_code = s.service_code AND l.whs_code = s.whs_code
where date(from_unixtime(delivery_failed_time)) = date_add('day',-1,date(from_unixtime(1656111600))) and integrated_channel_origin is not null
group by 1,2,3) a 
left join ( 
select data_date, origin_country, grass_region, cast(daily_failed_delivery_qty_MTD as double)/ mtd_days as daily_failed_delivery_qty_MTD from (
select date(from_unixtime(1656111600)) as data_date,
integrated_channel_origin AS origin_country, date_diff('day',date_trunc('month',date(from_unixtime(1656111600))),date_add('day',-1,date(from_unixtime(1656111600)))) as mtd_days
, CASE WHEN channel_id = 18025 then 'SG Express'
WHEN channel_id = 18028 AND last_mile = 'SPX' then 'SG Economy'

--WHEN channel_id = 18028 AND service_code in ('SOB', 'SOC') AND shipment_provider = 'Singpost' then 'SG Economy'
WHEN channel_id = 18028 AND l.service_code not in ('SOB', 'SOC') then 'SG Eco to Exp'
WHEN channel_id = 18055 then 'SG Sea Shipping'
WHEN channel_id = 18099 then 'SG Standard Express - Collection Point'
WHEN channel_id = 28052 then 'MY Sea Shipping'
WHEN channel_id = 38025 then 'TW Economy'
WHEN channel_id = 48008 then 'PH Economy'
ELSE grass_region
END AS grass_region
, count(distinct l.consignment_no) as daily_failed_delivery_qty_MTD
from regcbbi_others.cb_logistics_mart_test l 
--LEFT JOIN sls_mart.shopee_sls_logistic_sg_db__logistic_request_tab__reg_daily_s0_live r ON l.ordersn = r.ordersn
LEFT JOIN supp s ON DATE(from_unixtime(l.seller_shipout_time)) = s.ingest_date AND l.service_code = s.service_code AND l.whs_code = s.whs_code
where date(from_unixtime(delivery_failed_time)) between date_trunc('month',date(from_unixtime(1656111600))) and date_add('day',-2,date(from_unixtime(1656111600))) and integrated_channel_origin is not null
group by 1,2,3,4)) b on a.data_date = b.data_date and a.origin_country = b.origin_country and a.grass_region = b.grass_region 
Left join (
select data_date, origin_country, grass_region, cast(daily_failed_delivery_qty_m1 as double)/ m1_days as daily_failed_delivery_qty_m1 from (
select date(from_unixtime(1656111600)) as data_date,
integrated_channel_origin AS origin_country, date_diff('day', date_trunc('month',date_add('month',-1,date(from_unixtime(1656111600)))) , date_trunc('month',date(from_unixtime(1656111600)))) as m1_days
, CASE WHEN channel_id = 18025 then 'SG Express'
WHEN channel_id = 18028 AND last_mile = 'SPX' then 'SG Economy'

--WHEN channel_id = 18028 AND service_code in ('SOB', 'SOC') AND shipment_provider = 'Singpost' then 'SG Economy'
WHEN channel_id = 18028 AND l.service_code not in ('SOB', 'SOC') then 'SG Eco to Exp'
WHEN channel_id = 18055 then 'SG Sea Shipping'
WHEN channel_id = 18099 then 'SG Standard Express - Collection Point'
WHEN channel_id = 28052 then 'MY Sea Shipping'
WHEN channel_id = 38025 then 'TW Economy'
WHEN channel_id = 48008 then 'PH Economy'
ELSE grass_region
END AS grass_region
, count(distinct l.consignment_no) as daily_failed_delivery_qty_m1
from regcbbi_others.cb_logistics_mart_test l 
--LEFT JOIN sls_mart.shopee_sls_logistic_sg_db__logistic_request_tab__reg_daily_s0_live r ON l.ordersn = r.ordersn
LEFT JOIN supp s ON DATE(from_unixtime(l.seller_shipout_time)) = s.ingest_date AND l.service_code = s.service_code AND l.whs_code = s.whs_code
where date(from_unixtime(delivery_failed_time)) between date_trunc('month',date_add('month',-1,date(from_unixtime(1656111600)))) and date_add('day',-1,date_trunc('month',date(from_unixtime(1656111600)))) and integrated_channel_origin is not null
group by 1,2,3,4) ) c on a.data_date = c.data_date and a.origin_country = c.origin_country and a.grass_region = c.grass_region )
) 


SELECT a.data_date, a.origin_country, a.grass_region
, coalesce(shipped_ord, 0) AS shipped_ord
, coalesce(shipped_weight, 0) AS shipped_weight
, coalesce(lm_inbound_ord, 0) AS lm_inbound_ord
, coalesce(delivered_ord, 0) AS delivered_ord
-----
, coalesce(pay_to_whs, 0) AS pay_to_whs
, coalesce(whs_processing, 0) AS whs_processing
, coalesce(line_haul, 0) AS line_haul
, coalesce(lm_delivery, 0) AS lm_delivery
, coalesce(total_lead_time, 0) AS total_lead_time
-----
, cast(mtd_shipped_ord AS DOUBLE)/cast(day_of_month(date(from_unixtime(1656111600))) AS DOUBLE) AS mtd_avg_daily_shipped_ord
, cast(mtd_shipped_weight AS DOUBLE)/cast(day_of_month(date(from_unixtime(1656111600))) AS DOUBLE) AS mtd_avg_daily_shipped_weight
, cast(mtd_lm_inbound_ord AS DOUBLE)/cast(day_of_month(date(from_unixtime(1656111600))) AS DOUBLE) AS mtd_avg_daily_lm_inbound_ord
, cast(mtd_delivered_ord AS DOUBLE)/cast(day_of_month(date(from_unixtime(1656111600))) AS DOUBLE) AS mtd_avg_daily_delivered_ord


, coalesce(mtd_pay_to_whs, 0) AS mtd_pay_to_whs
, coalesce(mtd_whs_processing, 0) AS mtd_whs_processing
, coalesce(mtd_line_haul, 0) AS mtd_line_haul
, coalesce(mtd_lm_delivery, 0) AS mtd_lm_delivery
, coalesce(mtd_total_lead_time, 0) AS mtd_total_lead_time
-----
, cast(m_1_shipped_ord AS DOUBLE)/cast(day_of_month(date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600))))) AS DOUBLE) AS m_1_avg_daily_shipped_ord
, cast(m_1_shipped_weight AS DOUBLE)/cast(day_of_month(date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600))))) AS DOUBLE) AS m_1_avg_daily_shipped_weight
, cast(m_1_lm_inbound_ord AS DOUBLE)/cast(day_of_month(date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600))))) AS DOUBLE) AS m_1_avg_daily_lm_inbound_ord
, cast(m_1_delivered_ord AS DOUBLE)/cast(day_of_month(date_add('day', -1, date_trunc('month', date(from_unixtime(1656111600))))) AS DOUBLE) AS m_1_avg_daily_delivered_ord


, coalesce(m_1_pay_to_whs, 0) AS m_1_pay_to_whs
, coalesce(m_1_whs_processing, 0) AS m_1_whs_processing
, coalesce(m_1_line_haul, 0) AS m_1_line_haul
, coalesce(m_1_lm_delivery, 0) AS m_1_lm_delivery
, coalesce(m_1_total_lead_time, 0) AS m_1_total_lead_time
, b.daily_failed_delivery_qty
, b.daily_failed_delivery_qty_MTD
, b.daily_failed_delivery_qty_M1
FROM ((
SELECT * FROM integrated
UNION ALL
SELECT * FROM kr_nonintegrated
UNION ALL
SELECT * FROM jpcb
) a left join del_fail b on a.data_date = b.data_date and a.origin_country = b.origin_country and a.grass_region = b.grass_region )
ORDER BY 2,3,1