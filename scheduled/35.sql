insert into nexus.test_ea4779fc04ab8f596ef22d7f5b0573c4c8abc671870b6d1918df82258cb2d8af_ef3b0865da9bb9f660283dfd9af19091 with seller as (
select 
shop_id as shopid,
grass_region,
user_id as userid,
user_name as shop_name,
gp_name,
gp_smt
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
) 
),
shop_info as (
select
a.*
from
mp_user.dim_shop__reg_s0_live AS a
join
seller as s
on
a.shop_id = s.shopid
and a.tz_type = 'local'
and a.is_cb_shop = 1
and a.status = 1
and (a.is_holiday_mode IS NULL OR a.is_holiday_mode = 0)
where
grass_date >= date('2020-01-01')
),
user_info as (
SELECT
u.shop_id,
u.grass_date,
u.status
FROM
mp_user.dim_user__reg_s0_live as u
join
seller as s
on
u.shop_id = s.shopid
AND u.tz_type = 'local'
and u.status = 1
where
u.grass_date >= date('2020-01-01')
group by
1,2,3 
),
last_login as (
SELECT
a.user_id,
a.last_login_datetime_td,
a.grass_date
FROM
mp_user.dws_user_login_td_account_info__reg_s0_live as a
join
seller as s
on
a.user_id = s.userid
and a.tz_type = 'local'
and DATE(split(a.last_login_datetime_td, ' ')[1]) >= DATE_ADD('day', -7, a.grass_date)
where
a.grass_date >= date('2020-01-01')
group by
1,2,3 
),
live_sku as (
SELECT
a.shop_id,
a.grass_date,
a.active_item_with_stock_cnt
FROM
mp_item.dws_shop_listing_td__reg_s0_live as a
join
seller as s
on
a.shop_id = s.shopid
and a.tz_type = 'local'
and a.active_item_with_stock_cnt >= 5
where
a.grass_date >= date('2020-01-01')
group by
1,2,3 
)
SELECT
kr.grass_region,
kr.gp_name,
kr.gp_smt,
a.shop_id,
kr.shop_name,
min(a.grass_date) AS new_shop_live_date
FROM
shop_info as a
join
seller as kr
on
a.shop_id = kr.shopid
JOIN 
user_info AS b
ON
a.shop_id = b.shop_id
AND a.grass_date = b.grass_date
join 
last_login as l
on
a.user_id = l.user_id
and a.grass_date = l.grass_date
JOIN 
live_sku AS c
ON
c.shop_id = a.shop_id
AND c.grass_date = a.grass_date
GROUP BY
1,2,3,4,5
having
min(a.grass_date) between date_add('month',-3,current_date) and date_add('day',-1,current_date)