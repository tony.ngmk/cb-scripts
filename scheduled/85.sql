--raw DB not in tag


WITH 
-- raw data from our own database
cbwh_shop as (
select distinct
a.grass_region,
a.shop_id, 
a.fbs_tag, 
a.pff_tag, 
l.shop_level1_category,
shop_level1_global_be_category
,is_official_shop
,is_preferred_shop
,is_fss_shop
,is_ccb_shop
,is_cb_shop
,is_fbs
from regcbbi_general.cbwh_shop_history_v2 a
JOIN (
-- limit data size before join
SELECT
shop_id 
, status as shop_status 
, is_official_shop
,is_preferred_shop
,is_fss_shop
,is_ccb_shop
,is_cb_shop
,is_fbs
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local'
AND is_cb_shop = 1
) AS s ON a.shop_id = s.shop_id
JOIN (
-- limit data size before join
SELECT
shop_id,
shop_level1_category_id,
shop_level1_category,
shop_level1_global_be_category_id,
shop_level1_global_be_category
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local'
) AS l ON a.shop_id = l.shop_id
where a.is_live_shop =1
and a.grass_date = current_date - interval '1' day 


),






user as 
(
SELECT grass_region 
, u.shop_id 
, user_status 
, registration_date as shop_registration_date 
, shop_status
FROM
(
SELECT grass_region 
, shop_id 
, status as user_status
, date(from_unixtime(registration_timestamp)) as registration_date 
FROM mp_user.dim_user__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
and is_cb_shop = 1
) u
LEFT JOIN 
(
SELECT shop_id 
, status as shop_status 
FROM mp_user.dim_shop__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
) s on u.shop_id = s.shop_id
),



category_cluster as (

SELECT
--shop_id,
shop_level1_category,
shop_level1_category_id,
shop_level1_global_be_category_id,
shop_level1_global_be_category,
-- item gobal category percentae -> main global category 
-- using global
case when shop_level1_global_be_category in ('Mobile & Gadgets','Audio','Computer & Accessories','Home Appliances','Cameracs & Drones','Gaming & Consoles') then 'ELECTRONICS'
when shop_level1_global_be_category in ('Women Clothes','Women Shoes','Men Clothes','Men Shoes','Fashion Accessories','Women Bags','Men Bags','Baby & Kids Fashion','Watches','Muslim Fashion','Travel & Luggage') then 'FASHION' 
when shop_level1_global_be_category in ('Beauty','Mom & Baby','Health','Food & Beverages') then 'FMCG'
when shop_level1_global_be_category in ('Home & Living','Automobiles','Sports & Outdoors','Stationary','Hobbies & Collections','Pets','Motorcycles','Books & Magazines') then 'LIFESTYLE'
else 'unknown' end as Cluster
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local' 
)






, tag_type as 
(
SELECT cast(a.tag_id as int) as tag_id 
, type as wh_type
, tag_name 
, market as grass_region 
, cast(commission_rate as double) as tag_commission_rate 
, update_date as tag_update_date
, max(date(from_unixtime(metrics_cycle_time/1000))) - interval '1' day as tag_max_metrics_time
FROM regcbbi_others.mkt__commission_tag_id_mapping__reg_s0 a
LEFT JOIN marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live b on cast(a.tag_id as int) = b.tag_id
WHERE type = 'CBWH'
AND b.grass_region != ''
GROUP BY 1,2,3,4,5,6
)
,


tag_config as 
(
SELECT distinct tag.tag_id
, tag.region as grass_region
, tag.tag_name
, is_cb_shop
,is_preferred_shop
,is_official_shop
,is_fss_shop
,is_ccb_shop
--,is_fbs2_flag
,pff_flag
--,is_pff
,shop_level1_category_id
,first_arrive_months
, COALESCE(fbs_1,fbs_2) as fbs
FROM marketplace.shopee_seller_shoptag_db__shop_tag_tab__reg_daily_s0_live tag 
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_cb_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_cb' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) a on tag.tag_id = a.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_preferred_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_preferred' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) b on tag.tag_id = b.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_fss_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_fss' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) c on tag.tag_id = c.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_official_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_official' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) d on tag.tag_id = d.tag_id
/*
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_fbs2_flag
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'Is FBS2' 
) e on tag.tag_id = e.tag_id
*/
LEFT JOIN 
-- question?
(SELECT tag_id, metrics_value_min as pff_flag
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'pff_flag' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) f on tag.tag_id = f.tag_id
/*
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_pff
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'Is PFF' 
) g on tag.tag_id = g.tag_id
*/
LEFT JOIN 
(SELECT tag_id, metrics_enum_value as shop_level1_category_id
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'shop_level1_category_id' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) h on tag.tag_id = h.tag_id
LEFT JOIN 
--metrics_value_min metrics_value_max metrics_operator
-- 5 = min. 3 < max
(SELECT tag_id, metrics_operator as first_arrive_months
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'first_arrive_months' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) i on tag.tag_id = i.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_ccb_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_ccb' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) j on tag.tag_id = j.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as fbs_1
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'fbs' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) kk on tag.tag_id = kk.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as fbs_2
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'fbs_flag' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
--whether fbs2
) k on tag.tag_id = k.tag_id
where tag. grass_region in ('TH','MY','ID','VN','PH','MX')
),


-- tag_update_time as 
-- (
-- SELECT * from 
-- (
-- SELECT distinct tag_id
-- , region
-- , CASE WHEN a.tag_id in (1645) then 'TH Non Mall'
-- WHEN a.tag_id in (1646) then 'TH Mall'
-- WHEN a.tag_id in (1647) then 'MY Non Mall'
-- WHEN a.tag_id in (1648) then 'MY Mall'
-- WHEN a.tag_id in ( 1649) then 'PH Non Mall'
-- WHEN a.tag_id in (1650) then 'PH Non Mall Exemption - FSS'
-- WHEN a.tag_id in (1651) then 'PH Non Mall Exemption - CCB'
-- WHEN a.tag_id in (1652) then 'PH Non Mall Exemption - less 6months'
-- WHEN a.tag_id in (1653) then 'PH Non Mall Exemption - equal 6months' 
-- WHEN a.tag_id in (1668) then 'PH Mall - Electronics'
-- WHEN a.tag_id in (1667) then 'PH Mall - FMCG'
-- WHEN a.tag_id in (1669) then 'PH Mall - Fashion'
-- when a.tag_id in (1670) then 'PH Mall - Lifestyle'
-- WHEN a.tag_id in(1654) then 'ID Non Star'
-- WHEN a.tag_id in(1655) then 'ID Star' 
-- WHEN a.tag_id in(1656) then 'VN Mall'
-- ELSE 'Unknown' END as rule_type
-- --, (date(from_unixtime(max(metrics_cycle_time/1000))) over (partition by tag_id)) - interval '1' day as tag_max_metrics_time 
-- , max(date(from_unixtime(metrics_cycle_time/1000))) - interval '1' day as tag_max_metrics_time
-- FROM marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live a
-- WHERE status= 1
-- GROUP BY 1,2,3
-- )
-- where rule_type is not null and rule_type <>'Unknow