insert into nexus.test_bba857d4c49a5e8bdd883074ed54f49372fe801a1a3e139752d987ae5fc8f55b_727a71f4ce9698faca76ed91ba59120e WITH 
sip AS (
SELECT affi_shopid
, t1.cb_option
, t1.country AS psite
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1 
JOIN (
SELECT distinct affi_shopid, affi_userid, affi_username, mst_shopid, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status != 3
AND grass_region != ''
) AS t2 on t1.shopid = t2.mst_shopid
)
, cb AS (
SELECT CAST(si.child_shopid as BIGINT) as shop_id
, si.gp_account_name
, grass_region
, si.gp_account_id
, si.gp_account_owner
, si.child_account_owner
, si.child_account_name username
, si.gp_account_billing_country
, (CASE WHEN (si.ggp_account_name IS NOT NULL) THEN si.ggp_account_name ELSE si.gp_account_name END) GGP_GP
, si.gp_account_owner_userrole_name
, si.child_account_owner_userrole_name
, CASE WHEN sip.affi_shopid is not NULL THEN 1 ELSE 0 END as is_sip
, CASE WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
WHEN sip.affi_shopid IS NOT NULL AND sip.cb_option = 0 THEN 'Local SIP'
--WHEN sip.affi_shopid IS NOT NULL AND sip.psite IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
--WHEN sip.affi_shopid IS NOT NULL AND si.gp_account_billing_country = 'Korea' AND sip.psite IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN') THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL THEN 'CB SIP'
when si.gp_account_billing_country = 'Korea' then 'KRCB'
when si.gp_account_billing_country = 'Japan' then 'JPCB'
when si.gp_account_billing_country is NULL or si.gp_account_billing_country != 'China' then 'Others'
when si.child_account_owner_userrole_name like '%HKCB%' then 'HKCB'
when si.child_account_owner_userrole_name like '%BD_%' then 'CNCB BD'
when si.child_account_owner_userrole_name like '%UST_%' then 'CNCB UST'
when si.child_account_owner_userrole_name like '%ST_%' THEN 'CNCB ST'
when si.child_account_owner_userrole_name like '%MT_%' THEN 'CNCB MT'
when si.child_account_owner_userrole_name like '%LT%' THEN 'CNCB LT'
WHEN si.gp_account_billing_country = 'China' THEN COALESCE(CONCAT('CNCB ', si.cb_group), 'CNCB Others')
else 'CNCB Others' END as seller_type
FROM regbd_sf.cb__seller_index_tab AS si 
LEFT JOIN sip ON CAST(si.child_shopid AS BIGINT) = sip.affi_shopid
LEFT JOIN (
SELECT DISTINCT shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date = DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = CAST(si.child_shopid as BIGINT)
WHERE si.grass_region IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
)
, item AS (
SELECT DISTINCT i.grass_region, i.shop_id, i.item_id
FROM mp_item.dim_item__reg_s0_live AS i
INNER JOIN cb ON cb.shop_id = i.shop_id
INNER JOIN (
SELECT shop_id, grass_date, active_item_with_stock_cnt
FROM mp_item.dws_shop_listing_td__reg_s0_live 
WHERE tz_type ='local'
AND grass_date = CURRENT_DATE - interval '2' day
) AS i2 ON (i2.shop_id = i.shop_id)
INNER JOIN (
SELECT shop_id, grass_date
FROM mp_user.dws_user_login_td_account_info__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = CURRENT_DATE - interval '2' day
AND DATE(split(last_login_datetime_td, ' ')[1]) >= DATE_ADD('day', -7, grass_date)
) AS a ON (a.shop_id = i.shop_id)
WHERE i.tz_type = 'local' ANd i.is_cb_shop = 1
AND i.grass_date = CURRENT_DATE - interval '2' day
AND i.status = 1 AND i.shop_status = 1 AND i.seller_status = 1 AND i.stock > 0 AND i.is_holiday_mode = 0
AND i2.active_item_with_stock_cnt >= IF(cb.gp_account_billing_country = 'China', 50, IF(cb.gp_account_billing_country IN ('Korea', 'Japan'), 5, 0))
AND i.grass_region IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
)
, OMI_a AS 
(SELECT grass_region, shop_id, item_id, placed_order_cnt_30d, placed_order_cnt_60d, gmv_30d, gmv_usd_30d
FROM mp_order.dws_item_gmv_nd__reg_s0_live
WHERE grass_date = DATE_ADD('day', -2, current_date)
AND tz_type = 'local'
AND grass_region IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
)
, IC AS (
SELECT extinfo.country AS grass_region, itemid, orderid, MAX(ctime) As ctime, MAX_BY(extinfo, ctime) AS extinfo, MAX_BY(rating_star, ctime) AS rating_star
FROM marketplace.shopee_item_comment_db__item_cmt_v2_tab__reg_daily_s0_live 
WHERE extinfo.country IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
AND status IN (1,2)
GROUP BY 1, 2, 3
)
, IC_a AS 
(SELECT
itemid,
AVG(rating_star) AS L60D_sku_rating,
COUNT(rating_star) AS L60D_sku_rating_cnt
FROM IC
WHERE grass_region IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN') 
AND DATE(from_unixtime(ctime)) BETWEEN DATE_ADD('day', -61, current_date) AND DATE_ADD('day', -2, current_date)
GROUP BY 1
)
, RM_a AS 
(SELECT order_id, item_id, MAX_BY(return_reason_id, return_created_timestamp) AS return_reason_id, MAX(return_created_datetime) AS return_created_datetime
FROM mp_order.dwd_return_item_all_ent_df__reg_s0_live
WHERE grass_region IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
AND grass_date = (SELECT MAX(grass_date) mgd FROM mp_order.dwd_return_item_all_ent_df__reg_s0_live WHERE grass_region IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN'))
AND return_reason_id IN (3, 103, 105, 107)
GROUP BY 1, 2
HAVING DATE(CAST(MAX(return_created_datetime) AS timestamp)) BETWEEN DATE_ADD('day', -60, current_date) AND DATE_ADD('day', -1, current_date)
)
, RM AS
(SELECT item_id
, COUNT(order_id) AS L60D_seleted_RR_orders
FROM RM_a
GROUP BY 1)
, RAW AS 
(SELECT 
item.shop_id
, item.item_id
, item.grass_region
, cb.seller_type
, CASE WHEN IC_a.L60D_sku_rating < 4 AND IC_a.L60D_sku_rating >= 3.5 THEN '3.5 <= rating < 4'
WHEN IC_a.L60D_sku_rating < 3.5 AND IC_a.L60D_sku_rating >= 3 THEN '3 <= rating < 3.5'
WHEN IC_a.L60D_sku_rating < 3 AND IC_a.L60D_sku_rating >= 2.5 THEN '2.5 <= rating < 3'
WHEN IC_a.L60D_sku_rating < 2.5 AND IC_a.L60D_sku_rating >= 2 THEN '2 <= rating < 2.5'
WHEN IC_a.L60D_sku_rating < 2 AND IC_a.L60D_sku_rating >= 1.5 THEN '1.5 <= rating < 2'
WHEN IC_a.L60D_sku_rating < 1.5 THEN 'rating < 1.5'
ELSE 'rating >= 4 - exclude' 
END L60D_sku_rating_range
, CASE WHEN IC_a.L60D_sku_rating_cnt >= 5 AND IC_a.L60D_sku_rating_cnt < 10 THEN '5 <= rating_cnt < 10'
WHEN IC_a.L60D_sku_rating_cnt >= 10 AND IC_a.L60D_sku_rating_cnt < 20 THEN '10 <= rating_cnt < 20'
WHEN IC_a.L60D_sku_rating_cnt >= 20 AND IC_a.L60D_sku_rating_cnt < 30 THEN '20 <= rating_cnt < 30'
WHEN IC_a.L60D_sku_rating_cnt >= 30 THEN 'rating_cnt >= 30'
ELSE 'rating_cnt < 5 - exclude'
END L60D_sku_rating_cnt_range
, IC_a.L60D_sku_rating L60D_sku_rating
, IC_a.L60D_sku_rating_cnt L60D_sku_rating_cnt
, CASE WHEN RM.L60D_seleted_RR_orders >= 3 AND RM.L60D_seleted_RR_orders < 5 THEN '3 <= selected_rr_orders < 5'
WHEN RM.L60D_seleted_RR_orders >= 5 AND RM.L60D_seleted_RR_orders < 10 THEN '5 <= selected_rr_orders < 10'
WHEN RM.L60D_seleted_RR_orders >= 10 THEN 'selected_rr_orders >= 10'
ELSE 'selected_rr_orders < 3 - exclude' 
END L60D_selected_RR_order_range
, RM.L60D_seleted_RR_orders
, CASE WHEN COALESCE(TRY(CAST(RM.L60D_seleted_RR_orders AS DOUBLE)/CAST(OMI_a.placed_order_cnt_60d AS DOUBLE)),0) >= 0.1 AND COALESCE(TRY(CAST(RM.L60D_seleted_RR_orders AS DOUBLE)/CAST(OMI_a.placed_order_cnt_60d AS DOUBLE)),0) < 0.2 THEN '10% <= rr_pctg < 20%'
WHEN COALESCE(TRY(CAST(RM.L60D_seleted_RR_orders AS DOUBLE)/CAST(OMI_a.placed_order_cnt_60d AS DOUBLE)),0) >= 0.2 AND COALESCE(TRY(CAST(RM.L60D_seleted_RR_orders AS DOUBLE)/CAST(OMI_a.placed_order_cnt_60d AS DOUBLE)),0) < 0.3 THEN '20% <= rr_pctg < 30%'
WHEN COALESCE(TRY(CAST(RM.L60D_seleted_RR_orders AS DOUBLE)/CAST(OMI_a.placed_order_cnt_60d AS DOUBLE)),0) >= 0.3 THEN 'rr_pctg >= 30%'
ELSE 'rr_pctg < 10% - exclude'
END L60D_rr_pctg_range
, COALESCE(TRY(CAST(RM.L60D_seleted_RR_orders AS DOUBLE)/CAST(OMI_a.placed_order_cnt_60d AS DOUBLE)),0) L60D_rr_pctg
, CAST(OMI_a.placed_order_cnt_30d AS DOUBLE) / 30 AS L30D_ADO
, CAST(OMI_a.gmv_30d AS DOUBLE)/ 30 AS L30D_ADGMV
, CAST(OMI_a.gmv_usd_30d AS DOUBLE)/ 30 AS L30D_ADGMV_USD
FROM item
LEFT JOIN OMI_a ON OMI_a.item_id = item.item_id 
LEFT JOIN IC_a ON item.item_id = IC_a.itemid
LEFT JOIN RM ON RM.item_id = item.item_id
LEFT JOIN cb ON item.shop_id = cb.shop_id
--GROUP BY 1, 2, 3, 4, 5, 6
)


SELECT grass_region
, current_date - interval '1' day as week
, COALESCE(seller_type, 'Local') seller_type
, L60D_sku_rating
, L60D_sku_rating_range
, L60D_sku_rating_cnt
, L60D_sku_rating_cnt_range
, L60D_seleted_RR_orders
, L60D_selected_RR_order_range
, L60D_rr_pctg
, L60D_rr_pctg_range
, item_id
, shop_id
, L30D_ADO
, L30D_ADGMV_USD
FROM RAW
WHERE grass_region IN ('BR', 'CL', 'CO', 'ID', 'MY', 'MX', 'PH', 'SG', 'TH', 'TW', 'VN')
AND (L60D_sku_rating < 3.5 AND L60D_sku_rating_cnt >= 10)
OR (L60D_seleted_RR_orders >= 3 AND L60D_rr_pctg >= 0.2)