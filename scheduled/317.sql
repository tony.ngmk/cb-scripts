insert into nexus.test_929f3ce1d94a1feeb86561cb9d1505fe57af112e3695d85c11a85bc48f977268_1843ba9134d8df1e17ab3ce0addd0051 WITH sip AS (
SELECT DISTINCT 
shop_id, 
-- shop_level1_category,
-- shop_level2_category,
user_name,
grass_region,
source
FROM regcbbi_others.shop_profile
WHERE 
-- source IN ('CNCB SIP MY', 'CNCB SIP TW', 'KRCB SIP SG', 'Local SIP ID', 'Local SIP MY', 'Local SIP TW') AND 
grass_date = date(from_unixtime(1656111600))
)


, orders_raw AS (
SELECT
o.grass_date,
sip.source,
sip.grass_region,
o.shop_id,
sip.user_name,
o.item_id,
item.level1_category,
item.level2_category,
o.placed_order_fraction_1d,
o.placed_order_fraction_7d,
o.placed_order_fraction_14d,
o.placed_order_fraction_30d,
o.gmv_usd_1d,
o.gmv_usd_7d,
o.gmv_usd_30d,
-- o.item_amount_1d,
-- o.item_amount_7d,
-- o.item_amount_14d,
-- o.item_amount_30d,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_1d DESC) AS l1d_ranking,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_7d DESC) AS l7d_ranking,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_30d DESC) AS l30d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_1d DESC) AS gmv_l1d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_7d DESC) AS gmv_l7d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_30d DESC) AS gmv_l30d_ranking
FROM (
(
SELECT 
grass_date,
shop_id,
item_id,
placed_order_fraction_1d,
placed_order_fraction_7d,
placed_order_fraction_14d,
placed_order_fraction_30d,
gmv_usd_1d,
gmv_usd_7d,
gmv_usd_30d
-- item_amount_30d
FROM mp_order.dws_item_gmv_nd__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = date(from_unixtime(1656111600))
AND grass_region IN ('SG','PH','TW')
AND placed_order_fraction_30d > 0
) o
INNER JOIN sip 
ON sip.shop_id = o.shop_id 
INNER JOIN (
SELECT
item_id,
level1_category_id,
level1_category,
level2_category,
level1_global_be_category,
level2_global_be_category,
level1_global_be_category_id,
level2_global_be_category_id
FROM mp_item.dim_item__reg_s0_live 
WHERE grass_date = date(from_unixtime(1656111600))
AND grass_region IN ('SG','PH','TW')
) item 
ON item.item_id = o.item_id 
)
UNION ALL 
(
SELECT
o.grass_date,
sip.source,
sip.grass_region,
o.shop_id,
sip.user_name,
o.item_id,
item.level1_category,
item.level2_category,
o.placed_order_fraction_1d,
o.placed_order_fraction_7d,
o.placed_order_fraction_14d,
o.placed_order_fraction_30d,
o.gmv_usd_1d,
o.gmv_usd_7d,
o.gmv_usd_30d,
-- o.item_amount_1d,
-- o.item_amount_7d,
-- o.item_amount_14d,
-- o.item_amount_30d,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_1d DESC) AS l1d_ranking,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_7d DESC) AS l7d_ranking,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_30d DESC) AS l30d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_1d DESC) AS gmv_l1d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_7d DESC) AS gmv_l7d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_30d DESC) AS gmv_l30d_ranking
FROM (
(
SELECT 
grass_date,
shop_id,
item_id,
placed_order_fraction_1d,
placed_order_fraction_7d,
placed_order_fraction_14d,
placed_order_fraction_30d,
gmv_usd_1d,
gmv_usd_7d,
gmv_usd_30d
-- item_amount_30d
FROM mp_order.dws_item_gmv_nd__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = date(from_unixtime(1656111600))
AND grass_region IN ('MY','VN','ID','TH')
AND placed_order_fraction_30d > 0
) o
INNER JOIN sip 
ON sip.shop_id = o.shop_id 
INNER JOIN (
SELECT
item_id,
level1_category_id,
level1_category,
level2_category,
level1_global_be_category,
level2_global_be_category,
level1_global_be_category_id,
level2_global_be_category_id
FROM mp_item.dim_item__reg_s0_live 
WHERE grass_date = date(from_unixtime(1656111600))
AND grass_region IN ('MY','VN','ID','TH')
) item 
ON item.item_id = o.item_id 
)
)
UNION ALL 
(
SELECT
o.grass_date,
sip.source,
sip.grass_region,
o.shop_id,
sip.user_name,
o.item_id,
item.level1_category,
item.level2_category,
o.placed_order_fraction_1d,
o.placed_order_fraction_7d,
o.placed_order_fraction_14d,
o.placed_order_fraction_30d,
o.gmv_usd_1d,
o.gmv_usd_7d,
o.gmv_usd_30d,
-- o.item_amount_1d,
-- o.item_amount_7d,
-- o.item_amount_14d,
-- o.item_amount_30d,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_1d DESC) AS l1d_ranking,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_7d DESC) AS l7d_ranking,
RANK() over (partition by sip.grass_region, sip.source ORDER BY o.placed_order_fraction_30d DESC) AS l30d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_1d DESC) AS gmv_l1d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_7d DESC) AS gmv_l7d_ranking,
rank() over (PARTITION BY sip.grass_region, sip.source ORDER BY o.gmv_usd_30d DESC) AS gmv_l30d_ranking
FROM (
(
SELECT 
grass_date,
shop_id,
item_id,
placed_order_fraction_1d,
placed_order_fraction_7d,
placed_order_fraction_14d,
placed_order_fraction_30d,
gmv_usd_1d,
gmv_usd_7d,
gmv_usd_30d
FROM mp_order.dws_item_gmv_nd__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = date(from_unixtime(1656111600)) - INTERVAL '1' DAY
AND grass_region NOT IN ('SG','PH','TW','MY','VN','ID','TH','AR')
AND placed_order_fraction_30d > 0
AND tz_type = 'local'
) o
INNER JOIN sip 
ON sip.shop_id = o.shop_id 
INNER JOIN (
SELECT
item_id,
level1_category_id,
level1_category,
level2_category,
level1_global_be_category,
level2_global_be_category,
level1_global_be_category_id,
level2_global_be_category_id
FROM mp_item.dim_item__reg_s0_live 
WHERE grass_date = date(from_unixtime(1656111600)) - interval '1' DAY
AND grass_region NOT IN ('SG','PH','TW','MY','VN','ID','TH','AR')
AND tz_type = 'local'
) item 
ON item.item_id = o.item_id 
)) 
)


SELECT 
grass_date,
grass_region,
source,
shop_id,
item_id,
level1_category,
level2_category,
placed_order_fraction_1d AS l1d_ado,
placed_order_fraction_7d / 7.00 AS l7d_ado,
placed_order_fraction_30d / 30.00 AS l30d_ado,
gmv_usd_1d AS l1d_adgmv_usd,
gmv_usd_7d / DECIMAL '7.00' AS l7d_adgmv_usd,
gmv_usd_30d / DECIMAL '30.00' AS l30d_adgmv_usd,
l1d_ranking,
l7d_ranking,
l30d_ranking,
gmv_l1d_ranking,
gmv_l7d_ranking,
gmv_l30d_ranking 
FROM orders_raw
WHERE (l1d_ranking <= 10000 OR l7d_ranking <=10000 OR l30d_ranking<=10000
OR gmv_l1d_ranking <= 10000 OR gmv_l7d_ranking <= 10000 OR gmv_l30d_ranking <= 10000
) 
AND placed_order_fraction_30d > 0