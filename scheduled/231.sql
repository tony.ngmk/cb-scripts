insert into nexus.test_a6878859676e87e77e48344e73220accd9492d14e17e2624936830d6e9f042d3_7409e1d0b35d8fe97642d67e7f1584dc with /*fs as (
select distinct 'MY' as grass_region, promotion_rule_id,start_time, end_time, min_spend,description
from
(select snapshot_id,start_time,end_time,description, cast(json_extract(extra_data, '$.min_order_total') as double) as min_spend, extra_data
from shopee.shopee_promotion_backend_my_db__logistic_promotion_rule_tab
where status=1) a
join (select * from shopee.shopee_promotion_backend_my_db__logistic_promotion_rule_snapshot_tab ) b on a.snapshot_id=b.id


UNION All
select distinct 'TH' as grass_region,promotion_rule_id,start_time, end_time, min_spend,description
from
(select snapshot_id,start_time,end_time,description, cast(json_extract(extra_data, '$.min_order_total') as double) as min_spend, extra_data
from shopee.shopee_promotion_backend_th_db__logistic_promotion_rule_tab
where status=1) a
join (select * from shopee.shopee_promotion_backend_th_db__logistic_promotion_rule_snapshot_tab ) b on a.snapshot_id=b.id


UNION All
select distinct 'ID' as grass_region,promotion_rule_id,start_time, end_time, min_spend,description
from
(select snapshot_id,start_time,end_time,description, cast(json_extract(extra_data, '$.min_order_total') as double) as min_spend, extra_data
from shopee.shopee_promotion_backend_id_db__logistic_promotion_rule_tab
where status=1) a
join (select * from shopee.shopee_promotion_backend_id_db__logistic_promotion_rule_snapshot_tab ) b on a.snapshot_id=b.id

UNION All
select distinct 'TW' as grass_region,promotion_rule_id,start_time, end_time, min_spend,description
from
(select snapshot_id,start_time,end_time,description, cast(json_extract(extra_data, '$.min_order_total') as double) as min_spend, extra_data
from shopee.shopee_promotion_backend_tw_db__logistic_promotion_rule_tab
where status=1) a
join (select * from shopee.shopee_promotion_backend_tw_db__logistic_promotion_rule_snapshot_tab ) b on a.snapshot_id=b.id


UNION All
select distinct 'PH' as grass_region,promotion_rule_id,start_time, end_time, min_spend,description
from
(select snapshot_id,start_time,end_time,description, cast(json_extract(extra_data, '$.min_order_total') as double) as min_spend, extra_data
from shopee.shopee_promotion_backend_ph_db__logistic_promotion_rule_tab
where status=1) a
join (select * from shopee.shopee_promotion_backend_ph_db__logistic_promotion_rule_snapshot_tab ) b on a.snapshot_id=b.id


UNION All
select distinct 'BR' as grass_region,promotion_rule_id,start_time, end_time, min_spend,description
from
(select snapshot_id,start_time,end_time,description, cast(json_extract(extra_data, '$.min_order_total') as double) as min_spend, extra_data
from shopee.shopee_promotion_backend_br_db__logistic_promotion_rule_tab
where status=1) a
join (select * from shopee.shopee_promotion_backend_br_db__logistic_promotion_rule_snapshot_tab ) b on a.snapshot_id=b.id


UNION All
select distinct 'VN' as grass_region, promotion_rule_id,start_time, end_time, min_spend,description
from
(select snapshot_id,start_time,end_time,description, cast(json_extract(extra_data, '$.min_order_total') as double) as min_spend, extra_data
from shopee.shopee_promotion_backend_vn_db__logistic_promotion_rule_tab
where status=1) a
join (select * from shopee.shopee_promotion_backend_vn_db__logistic_promotion_rule_snapshot_tab ) b on a.snapshot_id=b.id
) */


order_raw as 
(select distinct o1.*
, case when gp_account_billing_country = 'China' then 'CNCB'
when gp_account_billing_country = 'Korea' then 'KRCB'
when gp_account_billing_country = 'Japan' then 'JPCB'
when gp_account_billing_country = 'Indonesia' then 'IDCB'
when gp_account_billing_country = 'Taiwan' then 'TWCB'
when gp_account_billing_country = 'Malaysia' then 'MYCB' 
when gp_account_billing_country = 'Vietnam' then 'VNCB'
else 'Others' end as seller_group
, if(sip.orderid is not null, 1, 0) as is_local_sip
, count(is_return) over (partition by o1.order_id) as total_rows
, sum(is_return) over (partition by o1.order_id) as return_rows

------------------- Adjust for local SIP order revenue.
, coalesce(sip.commission_fee_usd*fee_proportion, o1.commission_fee_usd) as final_comm_fee
, coalesce(sip.service_fee_usd *fee_proportion, o1.service_fee_usd ) as final_service_fee
, coalesce(sip.txn_fee_usd *fee_proportion, o1.txn_fee_usd ) as final_txn_fee
from 
(SELECT grass_region
, item_id,shop_id
, model_id
, order_id
, gmv_usd
, item_amount
, order_be_status_id
, order_fraction
, if(is_returned_item = 1, 1, 0) as is_return

, bundle_deal_id
, bundle_order_item_id
, add_on_deal_id
, is_add_on_sub_item
, group_id
, item_promotion_id
, item_promotion_source
, item_price_pp
, order_price_pp_usd


, item_rebate_by_shopee_amt_usd as item_rebate 
, pv_rebate_by_shopee_amt_usd as voucher_rebate
, if(actual_shipping_rebate_by_shopee_amt>0,actual_shipping_rebate_by_shopee_amt_usd,estimate_shipping_rebate_by_shopee_amt_usd) as shipping_rebate --estimate_shipping_rebate_by_shopee_amt_usd
, coin_earn_cash_amt_usd as coin_rebate 
, commission_fee_usd
, service_fee_usd
, buyer_txn_fee_usd+seller_txn_fee_usd as txn_fee_usd 

/*, CASE WHEN grass_region IN ('ID', 'TH', 'VN') THEN create_timestamp - 3600
WHEN grass_region IN ('BR', 'MX') THEN create_timestamp - 11*3600
ELSE create_timestamp 
END */
, create_timestamp AS create_time_sg
, date(from_unixtime(create_timestamp)) as grass_date
, coalesce(try(gmv/sum(gmv) over (partition by order_id)), 0) as fee_proportion
, exclusive_price_group_id
, is_flash_sale

, case when (item_rebate_by_shopee_amt>0 or if(estimate_shipping_rebate_by_shopee_amt>0,estimate_shipping_rebate_by_shopee_amt,actual_shipping_rebate_by_shopee_amt)>0
or coin_earn_cash_amt_usd>0 or sv_rebate_by_shopee_amt>0 or pv_rebate_by_shopee_amt>0) then 1 else 0 end as lpp

from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where grass_date >= date_trunc('month',date_add('day',-1,current_date)) - interval '2' month
and tz_type = 'local'
and date(from_unixtime(create_timestamp)) between date_trunc('month',date_add('day',-1,current_date)) - interval '2' month and date_add('day',-1,current_date)
and is_cb_shop = 1
--and grass_region IN ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR')
and is_bi_excluded=0
) o1
left join 
(select distinct grass_date as order_date, orderid, commission_fee_usd, service_fee_usd, cc_fee_usd as txn_fee_usd
from regcbbi_others.shopee_bi_keyreports_local_sip_revenue_v3 --shopee.shopee_bi_keyreports_local_sip_revenue_v2 
where grass_date between date_trunc('month',date_add('day',-1,current_date)) - interval '2' month and date_add('day',-1,current_date)
) sip on o1.order_id = sip.orderid
left join (select cast(child_shopid as bigint) as shopid, gp_account_billing_country from regbd_sf.cb__seller_index_tab where grass_region !=' ') c on c.shopid=o1.shop_id
left join 
(select distinct cast(affi_shopid as bigint) as shopid 
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE grass_region != ''
) sip on o1.shop_id = sip.shopid 
-- where item_id in (3225099946, 4435949092,4549509203) and grass_date = date'2021-07-28'
)
/*,order_raw as (
select distinct o.*, min_spend
from order_raw_v1 as o
join (select distinct order_id,logistics_channel_promotion_rule_id,create_timestamp,grass_region from shopee.order_mart_dwd_order_all_event_final_status_df o
WHERE o.grass_date >= date'2021-07-01'
AND date(from_unixtime(create_timestamp)) between date'2021-07-01'and date'2021-09-30'
AND tz_type = 'local' 
AND is_bi_excluded = 0
and grass_region in ('TH','MY','SG','VN','PH','ID','BR','TW')
) AS o2 on o2.order_id=o.order_id
left join fs on fs.promotion_rule_id = o2.logistics_channel_promotion_rule_id and o2.create_timestamp between fs.start_time and fs.end_time and fs.grass_region = o2.grass_region
)


select * from 
(select grass_region, grass_date --date_trunc('month', grass_date) as month
, item_id
-- , is_lpp
-- , is_healthy
, sum(order_fraction) as ADO --change the denominator /day(max(grass_date))
, sum(gmv_usd)/day(max(grass_date)) as ADGMV --change the denominator
, sum(IF(order_be_status_id in (1, 2, 3, 4, 11, 12, 13, 14, 15), order_fraction, 0))/day(max(grass_date)) AS net_ADO
, sum(IF(order_be_status_id in (1, 2, 3, 4, 11, 12, 13, 14, 15), gmv_usd, 0))/day(max(grass_date)) AS net_ADGMV
, sum(final_comm_fee) as commission_fee 
, sum(final_service_fee) as service_fee 
, sum(final_txn_fee) as txn_fee 
, sum(item_rebate) as item_rebate 
, sum(voucher_rebate) as voucher_rebate 
, sum(shipping_rebate) as shipping_rebate
, sum(coin_rebate) as coin_rebate
from 
(select o.*
, if(round(order_price_pp_usd,1)<=0.3, 1, 0) as is_lpp 
, case when round(order_price_pp_usd,1)<=0.3 and ((min_spend=0 and o.grass_region not in ('SG')) or (item_rebate>0 and o.grass_region in ('SG')) ) then 0
when round(order_price_pp_usd,1)<=0.3 then 1 end as is_healthy
from 
order_raw o
) o 
where grass_region='MY' and grass_date=date'2021-07-28'
group by 1, 2, 3 --, 4
order by 2,3,4
) a 
left join 
( select item_id, sum(order_fraction) as cbADO from shopee.order_mart_dwd_order_item_all_event_final_status_df
where grass_date >= date'2021-07-01'
and tz_type = 'local'
and date(from_unixtime(create_timestamp)) = date'2021-07-28'
and is_cb_shop = 1
and grass_region IN ('MY') 
group by 1) b on a.item_id=b.item_id -- and a.ADO=b.cbADO
where cbADO is null or round(a.ADO,1)!=round(b.cbADO,1)
*/
, net_ord as (
select grass_region
, current_date as grass_date
, to_unixtime(current_timestamp) as grass_timestamp
--, concat(cast(date_trunc('month', grass_date) as varchar), ' to ', cast((date_trunc('month', grass_date) + interval '1' month - interval '1' day) as varchar)) as calculation_duration
, date_trunc('month', grass_date) as month
, is_lpp
, is_healthy


--------------------------------------------- Net rate
, try(round(sum(case when is_net = 1 then order_fraction else 0 end)/sum(order_fraction), 4)) as ADO_net_rate
, try(round(sum(case when is_net = 1 then gmv_usd else 0 end)/sum(gmv_usd), 4)) as GMV_net_rate

, try(round(sum(case when is_net = 1 or (RR_type = 'Full RR' and is_drc = 1) or (RR_type = 'Partial' and is_return = 0) then final_comm_fee else 0 end)/sum(final_comm_fee), 4)) as comm_net_rate
, try(round(sum(case when is_net = 1 or (RR_type = 'Full RR' and is_drc = 1) or (RR_type = 'Partial' and is_return = 0) then final_service_fee else 0 end)/sum(final_service_fee), 4)) as service_net_rate
, try(round(sum(case when is_net = 1 or (RR_type in ('Full RR', 'Partial RR') and is_drc = 1) or (RR_type = 'Partial' and is_drc = 0 and is_return = 0) then final_txn_fee else 0 end)/sum(final_txn_fee), 4)) as txn_net_rate 

, try(round(sum(case when is_net = 1 or (RR_type in ('Full RR', 'Partial RR') and is_drc = 1) or (RR_type = 'Partial' and is_drc = 0 and is_return = 0) then item_rebate else 0 end)/sum(item_rebate), 4)) as item_rebate 
, try(round(sum(case when is_net = 1 or (RR_type in ('Full RR', 'Partial RR') and is_drc = 1) or (RR_type = 'Partial' and is_drc = 0 and is_return = 0) then voucher_rebate else 0 end)/sum(voucher_rebate), 4)) as voucher_rebate 
, try(round(sum(case when is_net = 1 or (RR_type = 'Full RR' and is_drc = 1) or RR_type = 'Partial' then shipping_rebate else 0 end)/sum(shipping_rebate), 4)) as shipping_rebate
, try(round(sum(case when is_net = 1 or (RR_type = 'Partial' and is_return = 0) then coin_rebate else 0 end)/sum(coin_rebate), 4)) as coin_rebate
from 
(select o.*
, if(round(order_price_pp_usd,1)<=0.3, 1, 0) as is_lpp
, if(round(order_price_pp_usd,1)<=0.3 and lpp=1, 0, 1) as is_healthy
, if(order_be_status_id in (1,2,3,4,11,12,13,14,15) and return_rows = 0, 1, 0) as is_net
, case when return_rows = 0 then 'Non RR'
when return_rows > 0 and total_rows = return_rows then 'Full RR'
when return_rows > 0 and total_rows > return_rows then 'Partial RR'
else 'Others'
end as RR_type 
, if(drc.orderid is not null, 1, 0) as is_drc 
from 
(select * from order_raw )o 
left join 
(SELECT distinct orderid
FROM marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
where grass_region !='' and date(from_unixtime(ctime)) >= date_trunc('month',date_add('day',-1,current_date)) - interval '2' month
and status in (2, 5, 7)
and cb_option = 1
and cast(return_info.with_resolution_center as boolean)
-- and grass_region IN ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR')
) drc on o.order_id = drc.orderid 
)
group by 1, 2, 3, 4, 5, 6 ) 


, final_order_info as 
(select grass_region, date_trunc('month', grass_date) as month
, is_lpp
, is_healthy
, seller_group
, sum(order_fraction)/case when date_trunc('month', grass_date) = date_trunc('month', date_add('day',-1,current_date)) then day(date_add('day',-1,current_date)) 
when month(date_trunc('month', grass_date)) in (1,3,5,7,8,10,12) then 31
when month(date_trunc('month', grass_date)) in (2) then 28 
else 30 end as ADO --change the denominator --
, sum(gmv_usd)/case when date_trunc('month', grass_date) = date_trunc('month', date_add('day',-1,current_date)) then day(date_add('day',-1,current_date)) 
when month(date_trunc('month', grass_date)) in (1,3,5,7,8,10,12) then 31
when month(date_trunc('month', grass_date)) in (2) then 28 
else 30 end as ADGMV --change the denominator
, sum(IF(order_be_status_id in (1, 2, 3, 4, 11, 12, 13, 14, 15), order_fraction, 0))/case when date_trunc('month', grass_date) = date_trunc('month', date_add('day',-1,current_date)) then day(date_add('day',-1,current_date)) 
when month(date_trunc('month', grass_date)) in (1,3,5,7,8,10,12) then 31
when month(date_trunc('month', grass_date)) in (2) then 28 
else 30 end AS net_ADO
, sum(IF(order_be_status_id in (1, 2, 3, 4, 11, 12, 13, 14, 15), gmv_usd, 0))/case when date_trunc('month', grass_date) = date_trunc('month', date_add('day',-1,current_date)) then day(date_add('day',-1,current_date)) 
when month(date_trunc('month', grass_date)) in (1,3,5,7,8,10,12) then 31
when month(date_trunc('month', grass_date)) in (2) then 28 
else 30 end AS net_ADGMV
, sum(final_comm_fee) as commission_fee 
, sum(final_service_fee) as service_fee 
, sum(final_txn_fee) as txn_fee 
, sum(item_rebate) as item_rebate 
, sum(voucher_rebate) as voucher_rebate 
, sum(shipping_rebate) as shipping_rebate
, sum(coin_rebate) as coin_rebate
from 
(select distinct o.*
, if(round(order_price_pp_usd,1)<=0.3, 1, 0) as is_lpp
, if(round(order_price_pp_usd,1)<=0.3 and lpp=1, 0, 1) as is_healthy
from 
order_raw o
) o 
group by 1, 2 , 3 , 4 ,5
order by 2,3,4 ,5
) 

/*, ads_raw as 
(select a.grass_region
, if(lpp.item_id is not null, 1, 0) as is_lpp 
, sum(raw_expen) as raw_expen
from 
(select grass_region
, item_id
, shop_id
, grass_date
, sum(expenditure_amt_usd) as raw_expen 
from shopee.paid_ads_dws_advertise_query_gmv_event_1d
where tz_type = 'local'
and grass_date between date '2021-07-01' and date '2021-08-31'
group by 1, 2, 3, 4
) a 
join 
(select grass_region, shop_id
from shopee.user_mart_dim_shop
where tz_type = 'local' and is_cb_shop = 1 and grass_date = date_add('day', -1, current_date)
) s on a.grass_region = s.grass_region and a.shop_id = s.shop_id
left join 
(select distinct grass_region,date_trunc('month', grass_date) as month
, try_cast(split(item_id,'.')[1] as bigint) as item_id
, date(try(cast(start_time as timestamp))) as start_date
, date(try(cast(end_time as timestamp))) as end_date


from
(select *
from shopee.shopee_regional_cb_team__lpp_list
WHERE ingestion_timestamp != '0' and date(cast(start_time as TIMESTAMP))<=date(cast(end_time as TIMESTAMP)) and (ingestion_timestamp!='1623748774' ) 
and (case when grass_region='SG' and date(cast(start_time as timestamp)) >= date'2021-06-01' and is_cfs='1' then try_cast(promo_price as DOUBLE)<3 else TRUE end)
and (case when grass_region='SG' and date(cast(start_time as timestamp)) >= date'2021-06-01' then is_cfs='1' else TRUE end)
)
WHERE try_cast(split(item_id,'.')[1] as bigint) is not null
) lpp on a.item_id = lpp.item_id and a.grass_date between lpp.start_date and lpp.end_date 
group by 1, 2, 3
) 


, mkt_ads_free_credits as 
(select grass_region,date_trunc('month', grass_date) as month
, sum(manual_free_credit_topup_amt_usd_1d)/sum(ads_expenditure_amt_usd_1d) as free_credits_pctg 
from shopee.paid_ads_ads_advertiser_mkt_1d
where tz_type = 'local'
and is_cb_seller = 1 
and grass_date between date '2021-07-01' and date '2021-08-31'
group by 1,2
) */


, coin_utilization as 
(select grass_country,grass_date, avg(rate) as coin_uti_rate
from regbida_keyreports.shopee_bi_coin_utilization_rate
where grass_date in (date '2021-08-01', date '2021-07-01',date '2021-09-01',date '2021-10-01',date '2021-11-01', date '2021-12-01',date '2022-01-01',date '2022-02-01', date '2022-03-01',date '2022-04-01',date '2022-05-01',date '2022-06-01',date '2022-07-01',date '2022-08-01')
group by 1,2
)


, platform as (
select grass_region, date_trunc('month', date(from_unixtime(create_timestamp))) as month, 0 as is_lpp, 1 as is_healthy,'CNCB' as seller_group
, sum(order_fraction)/case when date_trunc('month', date(from_unixtime(create_timestamp))) = date_trunc('month', date_add('day',-1,current_date)) then day(date_add('day',-1,current_date)) 
when month(date_trunc('month', date(from_unixtime(create_timestamp)))) in (1,3,5,7,8,10,12) then 31
when month(date_trunc('month', date(from_unixtime(create_timestamp)))) in (2) then 28 
else 30 end as platform_ADO
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where grass_date >= date_trunc('month',date_add('day',-1,current_date)) - interval '2' month
and tz_type = 'local'
and date(from_unixtime(create_timestamp)) between date_trunc('month',date_add('day',-1,current_date)) - interval '2' month and date_add('day',-1,current_date)
--and grass_region IN ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR')
and is_bi_excluded=0
group by 1,2,3,4,5
)




select o.grass_region, o.month
, o.is_lpp
, o.is_healthy
, o.seller_group

, o.ADO
, o.ADGMV

, o.commission_fee *coalesce(try(cast(n.ado_net_rate as DOUBLE)), 0) as comm_fee
, o.service_fee *coalesce(try(cast(n.ado_net_rate as DOUBLE)), 0) as service_fee
, o.txn_fee *coalesce(try(cast(n.ado_net_rate as DOUBLE)), 0) as txn_fee
-- , ar.raw_expen
-- , ar.raw_expen*af.free_credits_pctg as ads_free_credits

, o.item_rebate *coalesce(try(cast(n.ado_net_rate as DOUBLE)), 0) as item_rebate
, o.voucher_rebate *coalesce(try(cast(n.ado_net_rate as DOUBLE)), 0) as voucher_rebate
, o.shipping_rebate*coalesce(try(cast(n.ado_net_rate as DOUBLE)), 0) as shipping_rebate
, o.coin_rebate *coalesce(try(cast(n.ado_net_rate as DOUBLE)), 0)*c.coin_uti_rate as coin_rebate

, o.net_ADO
, o.net_ADGMV

, platform_ADO


from final_order_info o 
--left join ads_raw ar on o.grass_region = ar.grass_region and o.is_lpp = ar.is_lpp and o.month = ar.month
--left join mkt_ads_free_credits af on o.grass_region = af.grass_region and o.month = af.month
left join coin_utilization c on o.grass_region = c.grass_country and c.grass_date=o.month
left join net_ord n on o.grass_region = n.grass_region and o.is_lpp = n.is_lpp and o.is_healthy = n.is_healthy and if(o.month!=date_trunc('month', date_add('day', -1, current_date)) - interval '2' month, date_trunc('month', date_add('day', -1, current_date)) - interval '2' month = n.month, o.month = n.month)
left join platform m on o.grass_region = m.grass_region and m.month=o.month and o.is_lpp = m.is_lpp and o.is_healthy = m.is_healthy and o.seller_group = m.seller_group 
order by 1,2,3,4,5 
------MH's version------


-- with order_raw as 
-- (select o1.*
-- , if(sip.orderid is not null, 1, 0) as is_local_sip
-- , count(is_return) over (partition by o1.order_id) as total_rows
-- , sum(is_return) over (partition by o1.order_id) as return_rows

-- ------------------- Adjust for local SIP order revenue.
-- , coalesce(sip.commission_fee_usd*fee_proportion, o1.commission_fee_usd) as final_comm_fee
-- , coalesce(sip.service_fee_usd *fee_proportion, o1.service_fee_usd ) as final_service_fee
-- , coalesce(sip.txn_fee_usd *fee_proportion, o1.txn_fee_usd ) as final_txn_fee
-- from 
-- (SELECT grass_region
-- , item_id
-- , model_id
-- , order_id
-- , gmv_usd
-- , item_amount
-- , order_be_status_id
-- , order_fraction
-- , if(is_returned_item = 1, 1, 0) as is_return

-- , bundle_deal_id
-- , bundle_order_item_id
-- , add_on_deal_id
-- , is_add_on_sub_item
-- , group_id

-- , item_rebate_by_shopee_amt_usd as item_rebate 
-- , pv_rebate_by_shopee_amt_usd as voucher_rebate
-- , estimate_shipping_rebate_by_shopee_amt_usd as shipping_rebate 
-- , coin_earn_cash_amt_usd as coin_rebate 
-- , commission_fee_usd
-- , service_fee_usd
-- , buyer_txn_fee_usd+seller_txn_fee_usd as txn_fee_usd 

-- , CASE WHEN grass_region IN ('ID', 'TH', 'VN') THEN create_timestamp - 3600
-- WHEN grass_region IN ('BR', 'MX') THEN create_timestamp - 11*3600
-- ELSE create_timestamp 
-- END AS create_time_sg
-- , date(from_unixtime(create_timestamp)) as grass_date
-- , coalesce(try(gmv/sum(gmv) over (partition by order_id)), 0) as fee_proportion

-- from shopee.order_mart_dwd_order_item_all_event_final_status_df
-- where grass_date >= date_add('day', -60, current_date)
-- and tz_type = 'local'
-- and date(from_unixtime(create_timestamp)) between date_add('day', -60, current_date) and date_add('day', -30, current_date)
-- and is_cb_shop = 1
-- ) o1
-- left join 
-- (select grass_country, DATE(from_unixtime(event_timestamp)) as order_date, orderid, commission_fee_usd, service_fee_usd, cc_fee_usd+buyer_txn_fee_usd as txn_fee_usd
-- from shopee.shopee_bi_keyreports_local_sip_revenue_v2 
-- where DATE(from_unixtime(event_timestamp)) between date_add('day', -60, current_date) and date_add('day', -30, current_date)
-- and grass_date = current_date
-- ) sip on o1.order_id = sip.orderid
-- )


-- select grass_region
-- , current_date as grass_date
-- , to_unixtime(current_timestamp) as grass_timestamp
-- , concat(cast(date_add('day', -60, current_date) as varchar), ' to ', cast(date_add('day', -30, current_date) as varchar)) as calculation_duration
-- , is_lpp

-- --------------------------------------------- Net rate
-- , try(round(sum(case when is_net = 1 then order_fraction else 0 end)/sum(order_fraction), 4)) as ADO_net_rate
-- , try(round(sum(case when is_net = 1 then gmv_usd else 0 end)/sum(gmv_usd), 4)) as GMV_net_rate

-- , try(round(sum(case when is_net = 1 or (RR_type = 'Full RR' and is_drc = 1) or (RR_type = 'Partial' and is_return = 0) then final_comm_fee else 0 end)/sum(final_comm_fee), 4)) as comm_net_rate
-- , try(round(sum(case when is_net = 1 or (RR_type = 'Full RR' and is_drc = 1) or (RR_type = 'Partial' and is_return = 0) then final_service_fee else 0 end)/sum(final_service_fee), 4)) as service_net_rate
-- , try(round(sum(case when is_net = 1 or (RR_type in ('Full RR', 'Partial RR') and is_drc = 1) or (RR_type = 'Partial' and is_drc = 0 and is_return = 0) then final_txn_fee else 0 end)/sum(final_txn_fee), 4)) as txn_net_rate 

-- , try(round(sum(case when is_net = 1 or (RR_type in ('Full RR', 'Partial RR') and is_drc = 1) or (RR_type = 'Partial' and is_drc = 0 and is_return = 0) then item_rebate else 0 end)/sum(item_rebate), 4)) as item_rebate 
-- , try(round(sum(case when is_net = 1 or (RR_type in ('Full RR', 'Partial RR') and is_drc = 1) or (RR_type = 'Partial' and is_drc = 0 and is_return = 0) then voucher_rebate else 0 end)/sum(voucher_rebate), 4)) as voucher_rebate 
-- , try(round(sum(case when is_net = 1 or (RR_type = 'Full RR' and is_drc = 1) or RR_type = 'Partial' then shipping_rebate else 0 end)/sum(shipping_rebate), 4)) as shipping_rebate
-- , try(round(sum(case when is_net = 1 or (RR_type = 'Partial' and is_return = 0) then coin_rebate else 0 end)/sum(coin_rebate), 4)) as coin_rebate
-- from 
-- (select distinct o.*
-- , if(lpp.item_id is not null, 1, 0) as is_lpp
-- , if(order_be_status_id in (1,2,3,4,11,12,13,14,15) and return_rows = 0, 1, 0) as is_net
-- , case when return_rows = 0 then 'Non RR'
-- when return_rows > 0 and total_rows = return_rows then 'Full RR'
-- when return_rows > 0 and total_rows > return_rows then 'Partial RR'
-- else 'Others'
-- end as RR_type 
-- , if(drc.orderid is not null, 1, 0) as is_drc 
-- from 
-- order_raw o 
-- left join 
-- (select distinct grass_region
-- , try_cast(split(item_id,'.')[1] as bigint) as item_id
-- , to_unixtime(cast(start_time as timestamp)) as start_time
-- , to_unixtime(cast(end_time as timestamp)) as end_time
-- from shopee.shopee_regional_cb_team__lpp_list
-- WHERE try_cast(split(item_id,'.')[1] as bigint) is not null
-- and cast(ingestion_timestamp as bigint) > 0
-- ) lpp on o.item_id = lpp.item_id and o.create_time_sg between lpp.start_time and lpp.end_time 
-- left join 
-- (SELECT distinct orderid
-- FROM shopee.shopee_return_v2_db__return_v2_tab
-- where date(from_unixtime(ctime)) >= date_add('day', -60, current_date)
-- and status in (2, 5, 7)
-- and cb_option = 1
-- and cast(return_info.with_resolution_center as boolean)
-- and grass_region IN ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR')
-- ) drc on o.order_id = drc.orderid 
-- )
-- group by 1, 2, 3, 4, 5