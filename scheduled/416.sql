insert into nexus.test_7f0a9f24a7d90f31fe0d3e518b47acc2cb6ff86d0f0362583a73fd9071407efd_9fe111ea837def187516634469fd0d81 -- 3. By date




WITH orders AS 
(SELECT grass_region
, purchase_date as grass_date
, cast(count(distinct order_id) AS DOUBLE) AS gross_orders
, sum(gmv_usd) AS gross_gmv 
, sum(item_amount) AS gross_item_sold
, cast(count(distinct IF(is_net_order=1, order_id, NULL)) AS DOUBLE) AS net_order
, sum(IF(is_net_order=1, gmv_usd, 0)) AS net_gmv 
, count(distinct IF(campaign = 1 AND cfs = 0, order_id, null)) AS campaign_orders
, sum(IF(campaign = 1 AND cfs = 0, gmv_usd, 0)) AS campaign_gmv
, count(distinct IF(cfs = 1, order_id, null)) AS flash_sale_orders 
, count(DISTINCT CASE WHEN is_sls = 1 THEN order_id ELSE null END) AS sls_orders
, count(DISTINCT CASE WHEN is_sls_excluded_shop = 0 THEN order_id ELSE null END) as sls_order_denominator
FROM
(SELECT distinct 
o.purchase_date
, o.grass_region
, o.shop_id 
, order_id 
, o.item_id 
, o.model_id
, o.bundle_deal_id
, o.bundle_order_item_id
, o.add_on_deal_id
, o.is_add_on_sub_item
, gmv_usd
, item_amount
, buyer_paid_shipping_fee_usd
, estimate_shipping_fee_usd 
, is_net_order
, 0 as campaign 
, IF(item_promotion_source = 'flash_sale', 1, 0) AS cfs
, case when o.shipping_channel_id IN (90001,98001,98002,120001,110001,158001,168001,168002,88001,88021,100001,108001,108002,108004,108005,28016,28050,28051,28052,28053,28054,48002,48005,48006,48007,48008,148001,148002,148003,148004,148005,18025,18028,18036,18045,18053,18054,18055,18056,18057,18099,78004,78014,78016,78017,78020,38011,38012,38020,38024,38025,58007,58008,58009,58010,118001) THEN 1 ELSE 0 END AS is_sls



, case when sls_excl.shop_id is not null then 1 else 0 end as is_sls_excluded_shop
FROM 
(SELECT *
, DATE(split(create_datetime, ' ')[1]) as purchase_date
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE grass_region in ('SG', 'TW', 'MY', 'TH', 'ID', 'PH')
AND DATE(split(create_datetime, ' ')[1]) between date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) AND date_add('day', -1, current_date)
AND tz_type = 'local') o 
JOIN 
(SELECT distinct shop_id AS shopid
FROM dev_regcbbi_kr.jpcb_shop_profile
WHERE 
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.jpcb_shop_profile
where
grass_region <> '')
and grass_region <> '') cb
ON o.shop_id = cb.shopid 















LEFT JOIN
(SELECT distinct shop_id 
FROM regcbbi_others.shopee_regional_cb_team__jpcb_sls_performance_excluded_shop
WHERE ingestion_timestamp = (select max(ingestion_timestamp) AS ingestion_timestamp from regcbbi_others.shopee_regional_cb_team__jpcb_sls_performance_excluded_shop where CAST(ingestion_timestamp AS BIGINT) > 0)
) sls_excl
ON o.shop_id = cast(sls_excl.shop_id as bigint)
)
GROUP BY 1, 2)


SELECT orders.grass_region AS country 
, orders.grass_date AS purchased_on
, coalesce(orders.gross_orders, 0) AS gross_orders
, coalesce(round(orders.gross_gmv, 2), 0) AS gross_gmv
, coalesce(orders.gross_item_sold, 0) AS gross_item_sold
, coalesce(orders.net_order, 0) AS net_order
, coalesce(orders.net_gmv, 0) AS net_gmv
, coalesce(orders.campaign_orders, 0) AS campaign_orders
, coalesce(orders.campaign_gmv, 0) AS campaign_gmv
, coalesce(orders.flash_sale_orders, 0) AS flash_sale_orders
, coalesce(orders.sls_orders, 0) AS sls_orders
, coalesce(orders.sls_order_denominator, 0) AS sls_order_denominator


FROM orders 


ORDER BY 2 DESC, 1 DESC