insert into nexus.test_6bd8835c70d6ea0fbcf135f1f4cef549a1406253c75b599bb46ea8d46b63b898_5d0bdc2b1d6786f6d4d45fa519607ce3 with seller as (
select distinct 
shop_id as shopid 
, grass_region
, user_name as shop_name 
, gp_name
from 
dev_regcbbi_kr.krcb_shop_profile s
where 
s.grass_date in (
select 
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where 
grass_region = 'TW'
)
and grass_region = 'TW'
)




, raw_order as (
select
o.grass_region,
o.create_datetime,
o.order_sn,
o.order_id,
case
when r.return_status = 0 then 'RETURN_DELETE'
when r.return_status = 1 then 'RETURN_REQUESTED'
when r.return_status = 2 then 'RETURN_ACCEPTED'
when r.return_status = 3 then 'RETURN_CANCELLED'
when r.return_status = 4 then 'RETURN_JUDGING'
when r.return_status = 5 then 'RETURN_REFUND_PAID'
when r.return_status = 6 then 'RETURN_CLOSED'
when r.return_status = 7 then 'RETURN_PROCESSING'
when r.return_status = 8 then 'RETURN_SELLER_DISPUTE'
end as return_status,
o.logistics_status,
o.order_fe_status,
o.actual_shipping_carrier,
o.buyer_id


from
mp_order.dwd_order_item_all_ent_df__reg_s0_live as o
join
seller as s
on
o.shop_id = s.shopid
LEFT JOIN (
SELECT
return_sn,
orderid,
return_id,
status as return_status,
cast(refund_amount as double) / 10000 as refund_amount
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_continuous_s0_live as r
JOIN
seller as s
ON
r.shopid = s.shopid
where
r.grass_region <> ''
) as r
on
o.order_id = r.orderid
where tz_type = 'local'
and o.grass_region = 'TW'
and o.grass_date >= date_trunc('month', date_add('month', -1, current_date))
and o.is_bi_excluded = 0
and date(split(o.create_datetime,' ')[1]) between date_trunc('month', date_add('month', -1, current_date)) and date_add('day',-1,current_date) 
)


, logistic_request as (
select ordersn
, log_id
, lm_tracking_number
, consignment_no
, extra_data
, service_code
from
sls_mart.shopee_sls_logistic_tw_db__logistic_request_tab_lfs_union_tmp l 
join 
raw_order o 
on 
o.order_sn = l.ordersn
where 
tz_type = 'local'
and 
l.grass_region = 'TW'
and 
grass_date >= date_trunc('month', date_add('month', -1, current_date))
)


, logistic_tracking as (
select log_id
, channel_status 
, update_time
from 
sls_mart.shopee_sls_logistic_tw_db__logistic_tracking_tab_lfs_union_tmp
where 
tz_type = 'local'
and 
grass_region = 'TW'
and 
grass_date >= date_trunc('month', date_add('month', -1, current_date))
and 
log_id in (
select distinct log_id from logistic_request
)
)


, tw_order as (
select
o.grass_region,
date(split(o.create_datetime,' ')[1]) as grass_date,
o.order_sn,
o.order_id,
l.lm_tracking_number,
l.consignment_no as sls_number,
o.return_status,
o.logistics_status,
o.order_fe_status as fe_status,
o.actual_shipping_carrier as shipping_channel,
l.service_code,
json_extract_scalar(l.extra_data, '$.slug') as shipping_option,
max(case when upper(channel_status) = 'RECEIVE' then from_unixtime(t.update_time) else null end) as RECEIVE,
min(case when upper(channel_status) = 'AF' then from_unixtime(t.update_time) else null end) as AF,
max(case when upper(channel_status) = 'DF' then from_unixtime(t.update_time) else null end) as DF,
max(case when upper(channel_status) = 'RC' then from_unixtime(t.update_time) else null end) as RC,
max(case when upper(channel_status) = 'AS' then from_unixtime(t.update_time) else null end) as "as",
coalesce(max(case when upper(channel_status) = 'OKQS' then from_unixtime(t.update_time) else null end), max(case when upper(channel_status) = 'OKDS' then from_unixtime(t.update_time) else null end)) as "OKQSOKDS", 
o.buyer_id


from
raw_order as o
left join
logistic_request as l
on
o.order_sn = l.ordersn
left join
logistic_tracking as t
on 
l.log_id = t.log_id
group by 
1,2,3,4,5,6,7,8,9,10,11,12,19
order by
2,3 
),


jko AS
(select userid, min(ctime) as jko_kyc_approved_timestamp
from marketplace.shopee_account_extension_db__account_audit_tab__reg_daily_s0_live
WHERE audit_type = 32 
and grass_region = 'TW'
and status = 1 
group by 1
),


kyc as --- get all kyc info
(select distinct k.user_id, 
case when kyc_status = 2 then 'APPROVED'
when kyc_status = 5 then 'PHONE_UNVERIFIED (Rejected)'
when kyc_status = 6 then 'NAME_MISMATCH'
when kyc_status = 7 then 'NO_KYC'
else null end as kyc_status,
blacklist_status, k.kyc_name, k.phone_number, k.update_time
from marketplace.shopee_account_kyc_tw_db__kyc_tab__reg_daily_s0_live k
left join marketplace.shopee_account_kyc_tw_db__blacklist_tab__reg_daily_s0_live p on k.user_id = p.user_id
)


select o.*
, case when kyc_status = 'APPROVED' then coalesce(from_unixtime(jko_kyc_approved_timestamp), from_unixtime(kyc.update_time)) end as jko_kyc_approved_timestamp
, kyc_status 
from tw_order o 
left join jko on o.buyer_id = jko.userid 
left join kyc on o.buyer_id = kyc.user_id