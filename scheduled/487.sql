insert into nexus.test_c0e267b7865773d94c4e061fa8b9f7666beda5c21c74f25bc7fcc4ec3d965d2d_c4c9983d2d6bba1f37c8b36efdd18161 -- CN Lockdown - FF Daily (Shipping_Confirm_Time)
WITH
whitelist_seller AS (
select grass_region, cast(shop_id as BIGINT) shopid, cast(golive_timestamp as BIGINT) golive_timestamp
from regops_seller_ops.shopee_regional_op_team__new_flow_whitelisted_seller
where cast(is_cb_shop as INT) = 1 -- only include cb sellers 
)
, comp_audit AS (
select return_id, new_status, min(update_time) as update_time
from marketplace.shopee_return_compensation_audit_db__return_compensation_audit_tab__reg_continuous_s0_live 
where new_status in (3,6,7,8) -- 3 comp not required, 6 comp rejected, 7 comp request cancelled, 8 comp not eligible 
and grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
group by 1,2
) 
, o AS (
SELECT o1.*, o2.gmv, o2.gmv_usd
FROM (
SELECT distinct coalesce(r.orderid, o.orderid) as order_id
, coalesce(r.shopid, o.shopid) as shop_id
, coalesce(r.grass_region, o.grass_region) as grass_region
, date(from_unixtime(shipping_confirm_time)) as cdate
, DATE(FROM_UNIXTIME(o.create_time)) AS order_create_date
, (case when rr_time > 0 then 'r' when cancel_time > 0 then 'c' else 'n' end) as order_type
, CASE WHEN cancel_time is NULL THEN NULL
WHEN is_nfr_cancel = 0 THEN NULL
WHEN (o.grass_region = 'TW' AND cancel_reason in (200,204)) or (o.grass_region != 'TW' AND cancel_reason in (204)) THEN 'ACL1'
WHEN (o.grass_region = 'TW' AND cancel_reason in (201,302)) or (o.grass_region != 'TW' AND cancel_reason in (302)) THEN 'ACL2'
WHEN (cancel_reason IN (303)) THEN 'ACL3'
ELSE 'SC' END AS cancel_type
, reason
, CASE WHEN o.payment_method = 6 THEN 'COD' ELSE 'Non-COD' END AS payment_method
FROM (
SELECT grass_region, orderid, rr_time, shopid, reason
FROM (
select distinct rr.grass_region, rr.orderid, rr.reason
, CASE WHEN ws.shopid IS NULL THEN rr.mtime 
WHEN rr.ctime < ws.golive_timestamp THEN rr.mtime
WHEN rr.reason = 1 THEN aud.ctime
WHEN rr.reason IN (2,103,105,107) THEN ca.update_time
ELSE NULL END as rr_time
, rr.cb_option
, rr.shopid
from marketplace.shopee_return_v2_db__return_v2_tab__reg_continuous_s0_live AS rr
LEFT JOIN whitelist_seller AS ws ON ws.shopid = rr.shopid
LEFT JOIN (
SELECT return_id, MIN(create_time) AS ctime
FROM marketplace.shopee_return_audit_v2_db__return_audit_tab__reg_continuous_s0_live
WHERE new_status = 2 -- return accepted
AND grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
GROUP BY 1
) AS aud ON aud.return_id = rr.return_id
LEFT JOIN comp_audit AS ca ON ca.return_id = rr.return_id
LEFT JOIN (
SELECT DISTINCT orderid, shipping_method
FROM marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live 
WHERE grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
AND cb_option = 1
) AS o ON o.orderid = rr.orderid
where rr.status in (2, 5)
AND rr.cb_option = 1
and (case when rr.grass_region = 'TW' then rr.reason in (1,2,103,105) else rr.reason in (1,2,103,105,107) end)
and ( (ws.shopid IS NULL)
OR (rr.ctime < ws.golive_timestamp)
OR ( (rr.ctime >= ws.golive_timestamp AND rr.reason = 1 AND o.shipping_method = 1)
OR (rr.ctime >= ws.golive_timestamp AND rr.reason IN (2,103,105,107) AND ca.new_status IN (3,6,7,8) ) )
)
and rr.grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
)
--WHERE DATE(FROM_UNIXTIME(rr_time)) = DATE_ADD('day', -2, current_date)


) AS r
RIGHT JOIN (
select grass_region, orderid, status, create_time, cancel_time, shipping_confirm_time, shopid, status, extinfo.cancel_reason, payment_method
, case when ( ( grass_region = 'TW' AND extinfo.cancel_reason in (1,3,200,201,204,302,303) )
OR ( grass_region != 'TW' AND extinfo.cancel_reason in (1,3,4,204,301,302,303) )
OR ( extinfo.buyer_cancel_reason=2 ) )
THEN 1 ELSE 0 END AS is_nfr_cancel
from marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
where cb_option = 1
AND DATE(FROM_UNIXTIME(shipping_confirm_time)) BETWEEN DATE('2022-04-15') AND CURRENT_DATE - INTERVAL '1' DAY
and grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
) AS o on r.orderid = o.orderid
) AS o1
LEFT JOIN (
SELECT DISTINCT order_id, gmv, gmv_usd, payment_method_id
FROM mp_order.dwd_order_all_ent_df__reg_s0_live 
WHERE tz_type = 'local' AND is_cb_shop = 1
AND grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
AND grass_date >= DATE_ADD('day', -2, current_date)
AND DATE(split(create_datetime, ' ')[1]) = DATE_ADD('day', -2, current_date)
) AS o2 ON o2.order_id = o1.order_id
)
, sip AS (
SELECT affi_shopid
, t1.cb_option
, t1.country AS psite
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1 
INNER JOIN (
SELECT distinct affi_shopid, affi_userid, affi_username, mst_shopid, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status != 3
AND grass_region != ''
) AS t2 on t1.shopid = t2.mst_shopid
)
, seller_index AS (
SELECT s.shop_id
, si.gp_account_name
, si.ggp_account_name
, si.gp_account_id
, si.gp_account_owner
, si.child_account_owner
, si.gp_account_owner_userrole_name
, si.child_account_owner_userrole_name
, s.grass_region
, s.user_id
, CASE WHEN cb_group='HKCB' THEN '公司'
WHEN si.gp_account_name like '%公司%' THEN '公司'
WHEN si.gp_account_name like '有限合伙' THEN '公司'
ELSE '个体' END AS company_type
, CASE WHEN sip.affi_shopid is not NULL THEN 1 ELSE 0 END as is_sip
, CASE WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
WHEN sip.affi_shopid IS NOT NULL AND sip.cb_option = 0 THEN 'Local SIP'
WHEN sip.affi_shopid IS NOT NULL AND sip.psite IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL AND s.cb_shop_origin_country = 'KR' THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.affi_shopid IS NOT NULL THEN 'CB SIP'
WHEN s.cb_shop_origin_country = 'KR' THEN 'KRCB'
WHEN s.cb_shop_origin_country = 'JP' THEN 'JPCB'
WHEN (s.cb_shop_origin_country IS NULL OR s.cb_shop_origin_country != 'CN') AND (si.gp_account_billing_country is NULL or si.gp_account_billing_country != 'China') THEN 'Others'
WHEN si.gp_account_owner_userrole_name like '%HKCB%' THEN 'HKCB'
WHEN si.child_account_owner_userrole_name like '%BD_%' THEN 'CNCB BD'
WHEN si.child_account_owner_userrole_name like '%UST_%' THEN 'CNCB UST'
WHEN si.child_account_owner_userrole_name like '%ST_%' THEN 'CNCB ST'
WHEN si.child_account_owner_userrole_name like '%MT_%' THEN 'CNCB MT'
WHEN si.child_account_owner_userrole_name like '%LT%' THEN 'CNCB LT'
WHEN lower(si.gp_account_owner_userrole_name) LIKE '%seller ops%'OR si.child_account_owner_userrole_name LIKE '%BPO%' THEN 'CNCB Others'
WHEN (s.cb_shop_origin_country = 'CN' OR si.gp_account_billing_country = 'China') THEN COALESCE(CONCAT('CNCB ', si.cb_group), 'CNCB Others')
ELSE 'CNCB Others' END as seller_type
, gp_account_billing_country
FROM mp_user.dim_shop__reg_s0_live AS s
LEFT JOIN (
SELECT COALESCE(sp.grass_region, si.grass_region) AS grass_region
, COALESCE(sp.ggp_account_name, si.seller_name ) AS ggp_account_name
, COALESCE(sp.ggp_account_owner, si.seller_owner_email ) AS ggp_account_owner
, COALESCE(sp.gp_account_name, si.seller_owner_email ) AS gp_account_name
, COALESCE(CAST(sp.gp_account_id AS VARCHAR), cast(si.merchant_id as varchar)) AS gp_account_id
, COALESCE(sp.gp_account_owner, si.merchant_owner_email) AS gp_account_owner --email
, COALESCE(sp.child_account_owner, si.shop_owner_email) AS child_account_owner --email
, COALESCE(sp.gp_account_billing_country, si.merchant_region ) AS gp_account_billing_country
, COALESCE(sp.gp_account_owner_userrole_name, si.merchant_organization_name ) AS gp_account_owner_userrole_name
, COALESCE(sp.child_account_owner_userrole_name, si.shop_organization_name ) AS child_account_owner_userrole_name
-- , si.primary_category
, COALESCE(CAST(sp.child_shopid AS BIGINT), CAST(si.shop_id AS BIGINT)) AS shop_id
, COALESCE(sp.cb_group, si.cb_group) AS cb_group
FROM dev_regcbbi_general.cb_seller_profile_reg AS si
FULL OUTER JOIN cncbbi_general.shopee_cb_seller_profile_with_old_column AS sp ON CAST(sp.child_shopid AS BIGINT) = CAST(shop_id AS BIGINT)
WHERE COALESCE(sp.grass_region, si.grass_region) != ''
) AS si ON si.shop_id = s.shop_id
LEFT JOIN sip ON s.shop_id = sip.affi_shopid
LEFT JOIN (
SELECT DISTINCT shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date BETWEEN DATE_ADD('day', -14, current_date) AND DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = s.shop_id
WHERE s.tz_type = 'local' 
AND s.is_cb_shop = 1
AND s.cb_shop_origin_country = 'CN'
AND s.grass_date = DATE_ADD('day', -1, current_date)
)




SELECT o.grass_region
, o.grass_date
, WEEK(o.grass_date) week_number
, o.seller_type
, o.payment_method
, o.total_order_base
, o.net_order_count
, o.total_cancel_count
, o.total_seller_fault_cancel_count
, o.acl1_cancel_count
, o.acl2_cancel_count
, o.acl3_cancel_count
, o.seller_cancel_count
, o.total_rr_count
FROM (
SELECT o.grass_region
, o.cdate AS grass_date
, CASE WHEN cb.seller_type LIKE '%CNCB%' AND cb.seller_type LIKE '%UST%' THEN 'CNCB UST'
WHEN cb.seller_type LIKE '%CNCB%' AND (cb.seller_type LIKE '%BD%' OR cb.seller_type LIKE '%ICB%') THEN 'CNCB BD'
WHEN cb.seller_type LIKE '%CNCB%' AND cb.seller_type LIKE '%CB MKT%' THEN 'CNCB Others'
ELSE cb.seller_type END AS seller_type
, o.payment_method
, count(distinct o.order_id) as total_order_base
, count(distinct CASE WHEN o.order_type = 'n' THEN o.order_id ELSE NULL END) as net_order_count
, count(distinct CASE WHEN o.order_type = 'c' THEN o.order_id ELSE NULL END) total_cancel_count
, count(distinct CASE WHEN o.order_type = 'c' AND o.cancel_type IS NOT NULL THEN o.order_id ELSE NULL END) total_seller_fault_cancel_count
, count(distinct CASE WHEN o.order_type = 'c' AND o.cancel_type = 'ACL1' THEN o.order_id ELSE NULL END) acl1_cancel_count
, count(distinct CASE WHEN o.order_type = 'c' AND o.cancel_type = 'ACL2' THEN o.order_id ELSE NULL END) acl2_cancel_count
, count(distinct CASE WHEN o.order_type = 'c' AND o.cancel_type = 'ACL3' THEN o.order_id ELSE NULL END) acl3_cancel_count
, count(distinct CASE WHEN o.order_type = 'c' AND o.cancel_type = 'SC' THEN o.order_id ELSE NULL END) seller_cancel_count
, COUNT(DISTINCT CASE WHEN o.order_type = 'r' THEN o.order_id ELSE NULL END) AS total_rr_count
FROM o
LEFT JOIN seller_index AS cb ON cb.shop_id = o.shop_id
GROUP BY 1,2,3,4
) AS o
ORDER BY 1,2,3,4