insert into nexus.test_2e8f54e1a24f8ce1db2610c9dce2e343fdbd7da50c3938b92b275218c9be7ecd_141b6629fd33b6293884790edd1be6e7 -- 2022/04/28 updates:
-- restructured stmt and fees into 2 table (sip_supplier_table_detail AND sip__cnsip_supplier_order_tab_fee__df__reg__s0)
-- breakdown service fee into txn, management and spp




WITH ord_tab AS (SELECT * FROM regcbbi_others.sip_cn_pnl_order_tab 
)


, price_config AS (SELECT grass_region
, currency
, CAST(sip_exchange AS DOUBLE) sip_exchange --> USD/grass_region currency
FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2
WHERE cb_option = '1'
AND ingestion_timestamp = (SELECT MAX(ingestion_timestamp) max FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2)
)


, adj AS (SELECT DISTINCT TRY_CAST(orderid AS BIGINT) orderid
, UPPER(mst_curr) mst_curr
, MAX(kind) kind
, MIN(TRY_CAST(actual_amount AS DOUBLE)) AS actual_amount
FROM (SELECT DISTINCT a.*
, CASE WHEN remark = 'SIP Bundle Deal Order' THEN 4
WHEN remark = 'SIP Campaign Order' THEN 5
WHEN remark = 'SIP offline Adjustment' THEN 6 
WHEN remark = 'RR Compensation' THEN 7 
WHEN remark = 'SIP Voucher Adjustment' THEN 8 ELSE 0 END AS kind
FROM regcbbi_others.shopee_regional_cb_team__sip_offline_adj_order_level a 
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp from regcbbi_others.shopee_regional_cb_team__sip_offline_adj_order_level) b 
ON a.ingestion_timestamp = b.ingestion_timestamp)
GROUP BY 1,2)


, stmt_consol AS (
SELECT 
DISTINCT order_id
, currency
, SUM(settlement_price*quantity/100000.00) sip_settlement
FROM regcbbi_others.sip_supplier_table_detail
WHERE grass_date >= CURRENT_DATE - INTERVAL '180' DAY 
GROUP BY 1,2
-- limit 100
)


, stmt_fee AS (
SELECT order_id
, sip_seller_comm_fee AS sip_seller_comm_fee
, sip_seller_service_fee AS sip_seller_service_fee
, COALESCE(sip_seller_comm_rate, 0) sip_seller_comm_rate
, COALESCE(sip_seller_txn_rate + sip_seller_management_rate + sip_seller_spp_rate, 0) AS sip_seller_service_rate
, sip_seller_txn_fee AS sip_seller_txn_fee
, COALESCE(sip_seller_txn_rate,0) sip_seller_txn_rate
, sip_seller_management_fee AS sip_seller_management_fee
, COALESCE(sip_seller_management_rate,0) sip_seller_management_rate
, sip_seller_spp_fee AS sip_seller_spp_fee
, COALESCE(sip_seller_spp_rate,0) sip_seller_spp_rate
FROM (
SELECT 
DISTINCT order_id
-- total comm & svc
, SUM(CASE WHEN rule_type = 'comm' THEN fee ELSE 0 END) OVER (PARTITION BY order_id) AS sip_seller_comm_fee
, SUM(CASE WHEN rule_type = 'svc' THEN fee ELSE 0 END) OVER (PARTITION BY order_id) AS sip_seller_service_fee
, TRY((SUM(CASE WHEN rule_type = 'comm' THEN fee_rate ELSE 0 END) OVER (PARTITION BY order_id))/(SUM(CASE WHEN rule_type = 'comm' THEN 1 ELSE 0 END) OVER (PARTITION BY order_id)))*1.00 AS sip_seller_comm_rate
-- svc breakdown 
, SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'transaction') THEN fee ELSE 0 END) OVER (PARTITION BY order_id) AS sip_seller_txn_fee
, COALESCE(TRY((SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'transaction') THEN fee_rate ELSE 0 END) OVER (PARTITION BY order_id)) / (SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'transaction') THEN 1 ELSE 0 END) OVER (PARTITION BY order_id)))*1.00,0) AS sip_seller_txn_rate
, SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'management fee') THEN fee ELSE 0 END) OVER (PARTITION BY order_id) AS sip_seller_management_fee
, COALESCE(TRY((SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'management fee') THEN fee_rate ELSE 0 END) OVER (PARTITION BY order_id)) / (SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'management fee') THEN 1 ELSE 0 END) OVER (PARTITION BY order_id)))*1.00,0) AS sip_seller_management_rate
, SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'spp') THEN fee ELSE 0 END) OVER (PARTITION BY order_id) AS sip_seller_spp_fee
, COALESCE(TRY((SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'spp') THEN fee_rate ELSE 0 END) OVER (PARTITION BY order_id))/(SUM(CASE WHEN rule_type = 'svc' AND REGEXP_LIKE(lower(rule_name), 'spp') THEN 1 ELSE 0 END) OVER (PARTITION BY order_id)))*1.00,0) AS sip_seller_spp_rate
FROM dev_regcbbi_others.sip__cnsip_supplier_order_tab_fee__df__reg__s0
WHERE grass_region <> ''
AND grass_date >= CURRENT_DATE - INTERVAL '180' DAY 
) 
) 




SELECT DISTINCT ord_tab.*
, sip_settlement
, stmt_consol.currency AS stmt_currency
, pc1.sip_exchange AS stmt_exchange --> USD <> stmt curr
, sip_seller_comm_fee
, sip_seller_service_fee
, sip_seller_comm_rate
, sip_seller_service_rate
, sip_seller_txn_fee
, sip_seller_txn_rate
, sip_seller_management_fee
, sip_seller_management_rate
, sip_seller_spp_fee
, sip_seller_spp_rate
, CASE WHEN pc1.currency IS NULL THEN pc2.sip_exchange 
ELSE pc1.sip_exchange
END AS offline_exchange
, CASE WHEN pc1.currency IS NULL THEN pc2.currency 
ELSE pc1.currency
END AS offline_currency 


FROM ord_tab
JOIN stmt_consol
ON ord_tab.order_id = stmt_consol.order_id 
LEFT JOIN stmt_fee
ON ord_tab.order_id = stmt_fee.order_id
LEFT JOIN adj ON ord_tab.order_id = adj.orderid
LEFT JOIN price_config pc1
ON adj.mst_curr = pc1.currency
LEFT JOIN price_config pc2
ON ord_tab.mst_country = pc2.grass_region
-- where 
-- -- ord_tab.order_id = 101927172533696
-- -- AND ord_tab.grass_region = 'MY'
-- sip_seller_management_fee > 0
-- AND sip_seller_spp_fee > 0
-- limit 100