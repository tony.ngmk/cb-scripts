insert into nexus.test_320ab99b5354c15f14173eb640db72c4bd025a6ee1a920f66cbe9e21b6521710_baeb325c5d0651066ec6a6e24a5e3867 WITH
item AS (
SELECT DISTINCT
grass_region
, shop_id
, item_id
, status
, stock
, "date"("split"(create_datetime, ' ')[1]) item_cdate
, (CASE WHEN ((((comment_cnt IS NOT NULL) AND (comment_cnt > 0)) AND (rating_total_cnt > 0)) AND (rating_total_cnt IS NOT NULL)) THEN 1 ELSE 0 END) comment_cnt
FROM
mp_item.dim_item__reg_s0_live
WHERE ((((((((((is_cb_shop = 1) AND (seller_status = 1)) AND (shop_status = 1)) AND (status = 1)) AND (stock > 0)) AND (is_holiday_mode = 0)) AND (grass_region IN ('BR', 'MX', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN', 'CO', 'CL', 'ID','FR','ES','PL'))) AND (tz_type = 'local')) AND (grass_date = "date_add"('day', -1, current_date))) AND ("date"("split"(create_datetime, ' ')[1]) <= "date_add"('day', -60, current_date)))
) 
, sip AS (
SELECT DISTINCT
SHOPID mst_shopid
, t1.country mst_country
, t1.cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
WHERE ((t1.country <> 'SG') AND (CB_OPTION = 1))
) 
, orders AS (
SELECT
item_id
, (CAST("sum"(order_fraction) AS double) / DECIMAL '360.00') item_L360D_ADO
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (((((tz_type = 'local') AND ("date"("split"(create_datetime, ' ')[1]) BETWEEN "date_add"('day', -360, current_date) AND "date_add"('day', -1, current_date))) AND (grass_region IN ('BR', 'MX', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN', 'CO', 'CL', 'ID','FR','ES','PL') )) AND (is_cb_shop = 1)) AND (is_bi_excluded = 0))
AND (order_be_status_id IN (2,3,4,11,12,13,14,15) OR (payment_method_id = 6 AND order_be_status_id = 1) )
GROUP BY 1
) 
SELECT DISTINCT
grass_region
, item_id
FROM
(
SELECT DISTINCT
SHOP_ID
, ITEM_ID
, grass_region
, "row_number"() OVER (ORDER BY ITEM_ID ASC) N
FROM
(
SELECT DISTINCT
item.grass_region
, item.shop_id
, item.item_id
FROM
(((((item
INNER JOIN sip ON (sip.mst_shopid = item.shop_id))
INNER JOIN (
SELECT DISTINCT
affi_shopid
, mst_shopid
, affi_itemid
, mst_itemid
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE (affi_country IN ('BR', 'MX', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN', 'CO', 'CL', 'ID','FR','ES','PL') )
) MAP ON (MAP.mst_itemid = ITEM.item_id))
LEFT JOIN item item2 ON (item2.item_id = MAP.affi_itemid))
LEFT JOIN orders ON (item.item_id = orders.item_id))
LEFT JOIN orders o2 ON (o2.item_id = MAP.affi_itemid))
WHERE (((orders.item_L360D_ADO IS NULL) AND (item.comment_cnt = 0)) AND ((o2.item_L360D_ADO IS NOT NULL) OR (item2.comment_cnt <> 0)))
) 
) 
WHERE (N <= 1000000)