WITH 
dim AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE (grass_date >= current_date-interval'60'day and grass_region ='SG' )
) 
, sip AS (
SELECT DISTINCT
shopid
, country
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE ((cb_option = 1) AND (country = 'SG'))
) 
, seller AS (
SELECT DISTINCT
child_shopid
, (CASE WHEN (gp_smt IS NOT NULL) THEN gp_smt ELSE 'Others' END) seller_type
FROM
(regbd_sf.cb__seller_index_tab c
LEFT JOIN (
SELECT DISTINCT
gp_smt
, gp_name
FROM
regcbbi_others.shopee_regional_cb_team__krcb_gp_allocation-- has data
WHERE (ingestion_timestamp = (SELECT "max"(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__krcb_gp_allocation
))
) kr ON (kr.gp_name = c.gp_account_name))
WHERE (grass_region IN ('SG')) and gp_account_billing_country = 'Korea'
) 
, net AS (
SELECT DISTINCT
shop_id
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(create_timestamp) )
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(create_timestamp) - INTERVAL'1'HOUR )
WHEN grass_region = 'BR' THEN date(from_unixtime(create_timestamp)-interval'11'hour) end as create_time
, "count"(DISTINCT order_id) gross_order
, "count"(DISTINCT (CASE WHEN (order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15)) THEN order_id ELSE null END)) net_order

FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE ( (date(from_unixtime(create_timestamp) ) >=current_date-interval'60'day )) and tz_type='local' 
and grass_date >= current_date-interval'60'day 
and grass_region in ('MY','ID','PH','TH','VN')
and is_cb_shop=1 
GROUP BY 1, 2
) 
, cancel AS (
SELECT DISTINCT
shopid
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(cancel_time)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(cancel_time) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(cancel_time)-interval'11'hour) end cancel_time
, "count"(DISTINCT (CASE WHEN ((grass_region = 'TW') AND (extinfo.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303))) THEN orderid WHEN ((grass_region <> 'TW') AND (extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,30))) THEN orderid ELSE null END)) cancel_order
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ("date"("from_unixtime"(cancel_time)) >= current_date-interval'60'day ) and grass_region in ('MY','ID','PH','TH','VN')
and cb_option=1 
GROUP BY 1, 2
) 
, rr AS (
SELECT DISTINCT
shopid
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(mtime)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(mtime) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime)-interval'11'hour) end mtime
, "count"(DISTINCT (CASE WHEN ((((grass_region = 'TW') AND (reason IN (1, 2, 3, 103, 105))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5))) THEN a.orderid WHEN ((((grass_region <> 'TW') AND (reason IN (1, 2, 3, 103, 105, 107))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5))) THEN a.orderid ELSE null END)) RR_order
FROM
(marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
where grass_region in ('MY','ID','PH','TH','VN') AND DATE(FROM_UNIXTIME(CREATE_TIME)) >= current_date-interval'60'day 
and cb_option=1 
) b ON (a.orderid = b.orderid))
WHERE ("date"("from_unixtime"(mtime)) >= current_date-interval'60'day )
and grass_region in ('MY','ID','PH','TH','VN') and cb_option=1 
GROUP BY 1, 2
) 
SELECT DISTINCT
dim.grass_date
, COALESCE(seller_type, '5.Others') seller_type
, "sum"(gross_order) gross_order
, "sum"(net_order) net_oorder
, "sum"(cancel_order) cancel_order
, "sum"(RR_order) RR_order
FROM
(((((dim
INNER JOIN sip ON (dim.grass_region = sip.country))
LEFT JOIN net ON ((sip.shopid = net.shop_id) AND (dim.grass_date = net.create_time)))
LEFT JOIN cancel ON ((sip.shopid = cancel.shopid) AND (dim.grass_date = cancel.cancel_time)))
LEFT JOIN rr ON ((sip.shopid = rr.shopid) AND (dim.grass_date = rr.mtime)))
LEFT JOIN seller ON (sip.shopid = CAST(seller.child_shopid AS int)))
GROUP BY 1, 2