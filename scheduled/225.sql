insert into nexus.test_c2db55ac20e6b41ff3b3639aa3a106131bee926f9d792fca49ed6f0e3ae6ae6d_02825554a9f698e7fea80b4a1f334d08 --raw DB not in tag


WITH 
-- raw data from our own database
cbwh_shop as (
select distinct
a.grass_region,
a.shop_id, 
a.fbs_tag, 
a.pff_tag, 
l.shop_level1_category,
shop_level1_global_be_category
,is_official_shop
,is_preferred_shop
,is_fss_shop
,is_ccb_shop
,is_cb_shop
,is_fbs
from regcbbi_general.cbwh_shop_history_v2 a
JOIN (
-- limit data size before join
SELECT
shop_id 
, status as shop_status 
, is_official_shop
,is_preferred_shop
,is_fss_shop
,is_ccb_shop
,is_cb_shop
,is_fbs
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local'
AND is_cb_shop = 1
) AS s ON a.shop_id = s.shop_id
JOIN (
-- limit data size before join
SELECT
shop_id,
shop_level1_category_id,
shop_level1_category,
shop_level1_global_be_category_id,
shop_level1_global_be_category
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local'
) AS l ON a.shop_id = l.shop_id
where a.is_live_shop =1
and a.grass_date = current_date - interval '1' day 


),






user as 
(
SELECT grass_region 
, u.shop_id 
, user_status 
, registration_date as shop_registration_date 
, shop_status
FROM
(
SELECT grass_region 
, shop_id 
, status as user_status
, date(from_unixtime(registration_timestamp)) as registration_date 
FROM mp_user.dim_user__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
and is_cb_shop = 1
) u
LEFT JOIN 
(
SELECT shop_id 
, status as shop_status 
FROM mp_user.dim_shop__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
) s on u.shop_id = s.shop_id
),



category_cluster as (

SELECT
--shop_id,
shop_level1_category,
shop_level1_category_id,
shop_level1_global_be_category_id,
shop_level1_global_be_category,
-- item gobal category percentae -> main global category 
-- using global
case when shop_level1_global_be_category in ('Mobile & Gadgets','Audio','Computer & Accessories','Home Appliances','Cameracs & Drones','Gaming & Consoles') then 'ELECTRONICS'
when shop_level1_global_be_category in ('Women Clothes','Women Shoes','Men Clothes','Men Shoes','Fashion Accessories','Women Bags','Men Bags','Baby & Kids Fashion','Watches','Muslim Fashion','Travel & Luggage') then 'FASHION' 
when shop_level1_global_be_category in ('Beauty','Mom & Baby','Health','Food & Beverages') then 'FMCG'
when shop_level1_global_be_category in ('Home & Living','Automobiles','Sports & Outdoors','Stationary','Hobbies & Collections','Pets','Motorcycles','Books & Magazines') then 'LIFESTYLE'
else 'unknown' end as Cluster
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local' 
)






, tag_type as 
(
SELECT cast(a.tag_id as int) as tag_id 
, type as wh_type
, tag_name 
, market as grass_region 
, cast(commission_rate as double) as tag_commission_rate 
, update_date as tag_update_date
, max(date(from_unixtime(metrics_cycle_time/1000))) - interval '1' day as tag_max_metrics_time
FROM regcbbi_others.mkt__commission_tag_id_mapping__reg_s0 a
LEFT JOIN marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live b on cast(a.tag_id as int) = b.tag_id
WHERE type = 'CBWH'
AND b.grass_region != ''
GROUP BY 1,2,3,4,5,6
)
,


tag_config as 
(
SELECT distinct tag.tag_id
, tag.region as grass_region
, tag.tag_name
, is_cb_shop
,is_preferred_shop
,is_official_shop
,is_fss_shop
,is_ccb_shop
--,is_fbs2_flag
,pff_flag
--,is_pff
,shop_level1_category_id
,first_arrive_months
, COALESCE(fbs_1,fbs_2) as fbs
FROM marketplace.shopee_seller_shoptag_db__shop_tag_tab__reg_daily_s0_live tag 
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_cb_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_cb' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) a on tag.tag_id = a.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_preferred_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_preferred' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) b on tag.tag_id = b.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_fss_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_fss' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) c on tag.tag_id = c.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_official_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_official' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) d on tag.tag_id = d.tag_id
/*
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_fbs2_flag
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'Is FBS2' 
) e on tag.tag_id = e.tag_id
*/
LEFT JOIN 
-- question?
(SELECT tag_id, metrics_value_min as pff_flag
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'pff_flag' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) f on tag.tag_id = f.tag_id
/*
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_pff
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'Is PFF' 
) g on tag.tag_id = g.tag_id
*/
LEFT JOIN 
(SELECT tag_id, metrics_enum_value as shop_level1_category_id
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'shop_level1_category_id' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) h on tag.tag_id = h.tag_id
LEFT JOIN 
--metrics_value_min metrics_value_max metrics_operator
-- 5 = min. 3 < max
(SELECT tag_id, metrics_operator as first_arrive_months
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'first_arrive_months' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) i on tag.tag_id = i.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as is_ccb_shop
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'is_ccb' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) j on tag.tag_id = j.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as fbs_1
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'fbs' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
) kk on tag.tag_id = kk.tag_id
LEFT JOIN 
(SELECT tag_id, metrics_value_min as fbs_2
FROM marketplace.shopee_seller_shoptag_db__shop_tag_config_tab__reg_daily_s0_live 
WHERE metrics_name = 'fbs_flag' 
and grass_region in ('TH','MY','ID','VN','PH','MX')
--whether fbs2
) k on tag.tag_id = k.tag_id
where tag. grass_region in ('TH','MY','ID','VN','PH','MX')
),


-- tag_update_time as 
-- (
-- SELECT * from 
-- (
-- SELECT distinct tag_id
-- , region
-- , CASE WHEN a.tag_id in (1645) then 'TH Non Mall'
-- WHEN a.tag_id in (1646) then 'TH Mall'
-- WHEN a.tag_id in (1647) then 'MY Non Mall'
-- WHEN a.tag_id in (1648) then 'MY Mall'
-- WHEN a.tag_id in ( 1649) then 'PH Non Mall'
-- WHEN a.tag_id in (1650) then 'PH Non Mall Exemption - FSS'
-- WHEN a.tag_id in (1651) then 'PH Non Mall Exemption - CCB'
-- WHEN a.tag_id in (1652) then 'PH Non Mall Exemption - less 6months'
-- WHEN a.tag_id in (1653) then 'PH Non Mall Exemption - equal 6months' 
-- WHEN a.tag_id in (1668) then 'PH Mall - Electronics'
-- WHEN a.tag_id in (1667) then 'PH Mall - FMCG'
-- WHEN a.tag_id in (1669) then 'PH Mall - Fashion'
-- when a.tag_id in (1670) then 'PH Mall - Lifestyle'
-- WHEN a.tag_id in(1654) then 'ID Non Star'
-- WHEN a.tag_id in(1655) then 'ID Star' 
-- WHEN a.tag_id in(1656) then 'VN Mall'
-- ELSE 'Unknown' END as rule_type
-- --, (date(from_unixtime(max(metrics_cycle_time/1000))) over (partition by tag_id)) - interval '1' day as tag_max_metrics_time 
-- , max(date(from_unixtime(metrics_cycle_time/1000))) - interval '1' day as tag_max_metrics_time
-- FROM marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live a
-- WHERE status= 1
-- GROUP BY 1,2,3
-- )
-- where rule_type is not null and rule_type <>'Unknown'
-- ),


-- raw db
db_shop_tag as (


select tag_id, tag_name, tag_max_metrics_time, tag_commission_rate, db.*
from tag_type t
join (


SELECT 'TH Non Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'TH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)


UNION 
SELECT 'TH Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'TH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1


UNION 
SELECT 'MY Non Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'MY'
and a.fbs_flag = '1'
and(a.pff_flag ='0' or a.pff_flag is null)

UNION 
SELECT 'MY Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'MY'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1


UNION 
SELECT 'VN Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'VN'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1






UNION 
SELECT 'VN Non Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'VN'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_official_shop = 0 or a.is_official_shop is null)


UNION 
SELECT 'ID Non Star' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'ID'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_preferred_shop = 0 or a.is_preferred_shop is null)


UNION 
SELECT 'ID Star' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'ID'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_preferred_shop = 1


UNION 
SELECT 'PH Non Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_official_shop = 0 or a.is_official_shop is null)


UNION 
SELECT 'PH Non Mall Exemption - FSS' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
join cbwh_shop b on a.shop_id = b.shop_id 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_official_shop = 0 or a.is_official_shop is null)
and b. is_fss_shop =1


UNION 
SELECT 'PH Non Mall Exemption - CCB' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
left join cbwh_shop b on a.shop_id = b.shop_id 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_official_shop = 0 or a.is_official_shop is null)
and b.is_ccb_shop =1


UNION 
SELECT 'PH Non Mall Exemption - less 6months' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_official_shop = 0 or a.is_official_shop is null)
and (a.first_arrive_months <6 or a.first_arrive_months is null)


UNION 
SELECT 'PH Non Mall Exemption - equal 6months' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_official_shop = 0 or a.is_official_shop is null)
and a.first_arrive_months = 6


UNION 
SELECT 'PH Mall - Electronics' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
left join category_cluster b on a.shop_level1_category_id = b. shop_level1_global_be_category_id
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1
and b.Cluster = 'ELECTRONICS'
UNION 
SELECT 'PH Mall - FMCG' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
left join category_cluster b on a.shop_level1_category_id = b. shop_level1_global_be_category_id
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1
and b.Cluster = 'FMCG'


UNION 
SELECT 'PH Mall - Fashion' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
left join category_cluster b on a.shop_level1_category_id = b. shop_level1_global_be_category_id
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1
and b.Cluster = 'FASHION'


UNION 
SELECT 'PH Mall - Lifestyle' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
left join category_cluster b on a.shop_level1_category_id = b. shop_level1_global_be_category_id
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'PH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1
and b.Cluster = 'LIFESTYLE'


UNION 
SELECT 'MX_all' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
left join category_cluster b on a.shop_level1_category_id = b. shop_level1_global_be_category_id
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'MX'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)


) db
on t.grass_region = db.country and t.tag_name = db.rule_type and t.tag_max_metrics_time = db.dt
--WHERE t.rule_type is not null and t.rule_type <> 'Unknown'
--whether need this condition as it will filter out some shops
--and tag_id in (1645,1646,1647,1648,1649,1650,1651,1652,1653,1668,1667,1669,1670,1654,1655,1656 )

),






-- tag_shop_list
tag_output as (
-- SELECT CASE WHEN a.tag_id in (1645) then 'TH Non Mall'
-- WHEN a.tag_id in (1646) then 'TH Mall'
-- WHEN a.tag_id in (1647) then 'MY Non Mall'
-- WHEN a.tag_id in (1648) then 'MY Mall'
-- WHEN a.tag_id in ( 1649) then 'PH Non Mall'
-- WHEN a.tag_id in (1650) then 'PH Non Mall Exemption - FSS'
-- WHEN a.tag_id in (1651) then 'PH Non Mall Exemption - CCB'
-- WHEN a.tag_id in (1652) then 'PH Non Mall Exemption - less 6months'
-- WHEN a.tag_id in (1653) then 'PH Non Mall Exemption - equal 6months' 
-- WHEN a.tag_id in (1668) then 'PH Mall - Electronics'
-- WHEN a.tag_id in (1667) then 'PH Mall - FMCG'
-- WHEN a.tag_id in (1669) then 'PH Mall - Fashion'
-- when a.tag_id in (1670) then 'PH Mall - Lifestyle'
-- WHEN a.tag_id in(1654) then 'ID Non Star'
-- WHEN a.tag_id in(1655) then 'ID Star' 
-- WHEN a.tag_id in(1656) then 'VN Mall'
-- ELSE 'Unknown' END as rule_type
-- , b.shop_id
-- , date(from_unixtime(b.mtime/1000)) - interval '1' day as tag_shop_mtime
-- , date(from_unixtime(b.metrics_cycle_time/1000)) - interval '1' day as tag_shop_metrics_time
-- , a.* 
-- FROM tag_config a 
-- LEFT JOIN marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live b on a.tag_id = b.tag_id
-- where a.tag_id in (1645,1646,1647,1648,1649,1650,1651,1652,1653,1668,1667,1669,1670,1654,1655,1656 
-- )
-- and b.status = 1


SELECT a.tag_id 
, a.wh_type
, a.tag_name 
, a.grass_region 
, a.tag_commission_rate 
, a.tag_update_date
, a.tag_max_metrics_time
, b.shop_id 
, date(from_unixtime(b.mtime/1000)) - interval '1' day as tag_shop_mtime
, date(from_unixtime(b.metrics_cycle_time/1000)) - interval '1' day as tag_shop_metrics_time
FROM tag_type a 
LEFT JOIN marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live b on a.tag_id = b.tag_id
WHERE 1=1
and b.status= 1
AND b.grass_region != ''






)


select db.tag_name
, db.country as grass_region
, db.tag_id 
, db.tag_max_metrics_time
, db.tag_commission_rate
, db.dt as db_data_date
, db.shop_id as db_shop_id 
, db.is_cb_shop as db_is_cb_shop
, db.fbs_flag as db_fbs_flag
, db.pff_flag as db_pff_flag
, db.is_official_shop as db_official_shop
, db.is_preferred_shop as db_preferred_shop
, db.first_arrive_months as db_fisrt_arrived_month
, db.shop_level1_category_id as db_shop_level1_category_id 


, user_db.shop_registration_date
, user_db.user_status 
, user_db.shop_status
-- tag.* 
-- , user_tag.user_status
-- , user_tag.shop_status
-- , user_tag.registration_date user_tag_registration_tag
-- , db.rule_type
-- , db.tag_id 
-- , db.tag_max_metrics_time
-- , db.country as db_grass_region
-- , db.shop_id as db_shop_id 
-- , db.is_cb_shop as db_is_cb_shop
-- , db.fbs_flag as db_fbs_flag
-- , db.pff_flag as db_pff_flag
-- , db.is_official_shop as db_official_shop
-- , db.is_preferred_shop as db_preferred_shop
-- , db.first_arrive_months as db_fisrt_arrived_month
-- , db.shop_level1_category_id as db_shop_level1_category_id


FROM db_shop_tag db 
LEFT JOIN tag_output tag on db.tag_id = tag.tag_id and db.shop_id = tag.shop_id 
LEFT JOIN user as user_tag on tag.shop_id = user_tag.shop_id 
LEFT JOIN user as user_db on db.shop_id = user_db.shop_id 
where tag.shop_id is null
-- active shop / user
and (user_db.user_status is not null or user_db.shop_status is not null)
order by db.rule_type, db.country, tag.shop_id, db.shop_id