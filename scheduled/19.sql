insert into nexus.test_f1fbfb41dc5c14643eb053468eefce00b6de8d1f808d2377f564de13aa220838_a3dfb562855f0e07e60226f8e9335906 WITH today as (
select
current_date as today
),
raw_date as (
select
today,
date_add('day', -1, today) as yesterday,
date_trunc('month', today) as mtd_from,
date_add('day', -1, today) as mtd_to,
date_add('month', -1, date_trunc('month', today)) as m_1_from,
date_add('day', -1, date_trunc('month', today)) as m_1_to,
date_add('month', -2, date_trunc('month', today)) as m_2_from,
date_add('day', -1, date_add('month', -1, date_trunc('month', today))) as m_2_to,
cast(day_of_month(date_add('day', -1, today)) as double) as mtd_days,
cast(date_diff('day', date_add('month', -1, date_trunc('month', today)), date_trunc('month', today)) as double) as m_1_days,
cast(date_diff('day', date_add('month', -2, date_trunc('month', today)), date_add('month', -1, date_trunc('month', today))) as double) as m_2_days
from
today
),
date_info as (
select
today,
yesterday,
case when day(today) = 1 then m_1_from else mtd_from end as mtd_from,
case when day(today) = 1 then m_1_to else mtd_to end as mtd_to,
case when day(today) = 1 then m_2_from else m_1_from end as m_1_from,
case when day(today) = 1 then m_2_to else m_1_to end as m_1_to,
case when day(today) = 1 then m_1_days else mtd_days end as mtd_days,
case when day(today) = 1 then m_2_days else m_1_days end as m_1_days
from
raw_date
),
shop_item_metric as (
select
i.grass_region,
count(distinct case when date(from_unixtime(s.ctime)) <= mtd_to then i.shop_id else null end) as mtd_total_shops,
count(distinct case when date(from_unixtime(s.ctime)) <= m_1_to then i.shop_id else null end) as m_1_total_shops,
count(distinct case when date(split(create_datetime,' ')[1]) <= mtd_to then i.item_id else null end) as mtd_total_skus,
count(distinct case when date(split(create_datetime,' ')[1]) <= m_1_to then i.item_id else null end) as m_1_total_skus


from
mp_item.dim_item__reg_s0_live as i
join
date_info as d
on
i.grass_date = d.yesterday
and i.tz_type = 'local'
and i.status = 1
and i.stock > 0
join
marketplace.shopee_account_v2_db__account_tab__reg_daily_s0_live as s
on
i.shop_id = s.shopid
where
s.grass_region <> ''
group by
1
),
impression_clicks as (
select
t.grass_region,
sum(case when t.grass_date between mtd_from and mtd_to then t.uniq_imp_cnt_1d else 0 end) as mtd_impression,
sum(case when t.grass_date between m_1_from and m_1_to then t.uniq_imp_cnt_1d else 0 end) as m_1_impression,
sum(case when t.grass_date between mtd_from and mtd_to then t.uniq_clk_cnt_1d else 0 end) as mtd_click,
sum(case when t.grass_date between m_1_from and m_1_to then t.uniq_clk_cnt_1d else 0 end) as m_1_click
from
mp_shopflow.dws_item_civ_1d__reg_s0_live as t
join
date_info as d
on
t.grass_date between d.m_1_from and d.mtd_to
and t.tz_type = 'local'
where
t.grass_region <> ''
group by
1
),
orders as (
select
t.grass_region,
-- count(distinct case when t.grass_date between mtd_from and mtd_to then t.item_id else null end) as mtd_conversions,
-- count(distinct case when t.grass_date between m_1_from and m_1_to then t.item_id else null end) as m_1_conversions,
sum(case when t.grass_date between mtd_from and mtd_to then t.placed_order_fraction_1d else 0 end) as mtd_gross_orders,
sum(case when t.grass_date between m_1_from and m_1_to then t.placed_order_fraction_1d else 0 end) as m_1_gross_orders,
sum(case when t.grass_date between mtd_from and mtd_to then t.gmv_usd_1d else 0 end) as mtd_gmv_usd,
sum(case when t.grass_date between m_1_from and m_1_to then t.gmv_usd_1d else 0 end) as m_1_gmv_usd
-- sum(case when t.grass_date between mtd_from and mtd_to then t.item_amount_1d else 0 end) as mtd_item_sold,
-- sum(case when t.grass_date between m_1_from and m_1_to then t.item_amount_1d else 0 end) as m_1_item_sold
from
mp_order.dws_item_gmv_1d__reg_s0_live as t
join
date_info as d
on
t.grass_date between d.m_1_from and d.mtd_to
and t.tz_type = 'local'
where
t.grass_region <> ''
group by
1
)
select
i.grass_region,
s.mtd_total_shops,
s.m_1_total_shops,
s.mtd_total_skus,
s.m_1_total_skus,
i.mtd_impression/mtd_days as mtd_adimpression,
i.m_1_impression/m_1_days as m_1_adimpression,
i.mtd_click/mtd_days as mtd_adclicks,
i.m_1_click/m_1_days as m_1_adclicks,
-- coalesce(o.mtd_conversions,0)/mtd_days as mtd_adconversions,
-- coalesce(o.m_1_conversions,0)/m_1_days as m_1_adconversions,
coalesce(o.mtd_gross_orders,0)/mtd_days as mtd_adgross_orders,
coalesce(o.m_1_gross_orders,0)/m_1_days as m_1_adgross_orders,
-- coalesce(o.mtd_item_sold,0)/mtd_days as mtd_aditem_sold,
-- coalesce(o.m_1_item_sold,0)/m_1_days as m_1_aditem_sold,
coalesce(o.mtd_gmv_usd,0)/mtd_days as mtd_adgmv_usd,
coalesce(o.m_1_gmv_usd,0)/m_1_days as m_1_adgmv_usd
from
shop_item_metric as s
join
impression_clicks as i
on
s.grass_region = i.grass_region
join
orders as o
on
s.grass_region = o.grass_region
cross join
date_info