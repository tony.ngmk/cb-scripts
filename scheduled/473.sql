insert into nexus.test_d317614d90bf4fa28ae0b88f761b60849940e8096e179912385e41492cee721b_d15f6620f587bf188a0f1caea280ee52 WITH top_cat as 
(SELECT grass_region 
, level1_global_be_category as top_l1_cat
, level2_global_be_category as top_l2_cat
, top_cat_ranking
FROM
(
SELECT grass_region
, level1_global_be_category
, level2_global_be_category
, gmv 
, rank() over (partition by grass_region order by gmv desc) as top_cat_ranking
FROM
(
SELECT grass_region 
, level1_global_be_category
, level2_global_be_category
, sum(gmv_usd) as gmv 
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live 
WHERE tz_type = 'local'
AND grass_date >= date_add('day', -14, current_date)
AND DATE(from_unixtime(create_timestamp)) between date_add('day', -14, current_date) and date_add('day', -1, current_date) 
AND is_cb_shop = 1 
AND grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
GROUP BY 1,2,3 
)
) 
WHERE top_cat_ranking <= 20 
and level2_global_be_category not like 'Others'
)






, keywords as 
(
select
grass_region as site
, main_category
, sub_category
, keyword
-- , search_ranking
-- , bidding_items
-- , ads_CPC
-- ,ads_ROI
-- , cate_global_search_clicks
, ads_gmv_usd
, rank() over (partition by grass_region, main_category, sub_category order by ads_gmv_usd desc) as gmv_ranking


from
(
select *
, rank() over (partition by grass_region, main_category, sub_category order by W_1_daily_search desc) as search_ranking
from
(
select
sch.grass_region
, sch.keyword
, sch.W_1_daily_search
, main_category
, sub_category
, coalesce(ads.bidding_items, 0) as bidding_items
, coalesce(CPC, 99999) as ads_CPC
, coalesce(ROI, 99999) as ads_ROI
, coalesce(cate_global_search_clicks, 0) as cate_global_search_clicks
, ads_gmv_usd
from
(
SELECT
grass_region
, keywords as keyword
, sum(expenditure_amt_usd)/sum(click_cnt) as CPC
, sum(ads_gmv_amt_usd)/sum(expenditure_amt_usd) as ROI
, count(distinct item_id) as bidding_items
, sum(ads_gmv_amt_usd) as ads_gmv_usd 
from mp_paidads.dws_advertise_query_gmv_event_1d__reg_s0_live
where tz_type = 'local' 
AND grass_date >= date_add('day', -7, date_trunc('week', current_date))
AND grass_date < date_trunc('week', current_date)
and grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
group by 1, 2
) ads
right join
------------- keyword daily search volume
(
select
grass_region
, keyword
, cast(sum(srp_uv_1d) as double)/7 as W_1_daily_search
from
(
SELECT grass_region, grass_date, keyword, srp_uv_1d
FROM mp_search.dws_keyword_view_imp_clk_1d__reg_s1_live
WHERE grass_date >= date_add('day', -7, date_trunc('week', current_date))
AND grass_date < date_trunc('week', current_date)
AND grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
)
group by 1, 2 
) sch on ads.grass_region = sch.grass_region and ads.keyword = sch.keyword
left join
-------- Link keyword to subcate/cate
(
select *
from
(
select *
, rank() over (partition by grass_region, keyword order by cate_global_search_clicks desc) as ranking
from
(
select 
i.grass_region
, i.main_category
, i.sub_category
, keyword
, cast(sum(clicks) as double)/7 as cate_global_search_clicks
from
(
select distinct 
grass_region
, item_id as itemid
, level1_global_be_category as main_category
, level2_global_be_category as sub_category 
from mp_item.dim_item__reg_s0_live 
where is_cb_shop = 1
and tz_type = 'local'
and grass_date = date_add('day', -1, current_date)
and level1_global_be_category is not null
AND grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
) i
join
(
select 
grass_region
, keyword
, item_id as itemid
, count(event_id) as clicks
from mp_search.dwd_srp_item_click_di__reg_s1_live
where grass_date >= date_add('day', -7, date_trunc('week', current_date)) and grass_date < date_trunc('week', current_date)
and domain = 'global_search'
AND grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX')
--and clk_sortby = 'relevancy'
group by 1, 2, 3 
) t on i.grass_region = t.grass_region and i.itemid = t.itemid
group by 1, 2, 3, 4
)
--WHERE
)
where ranking <= 5 -- each keyword can be mapped to no more than 5 cates in 1 market
) cat on sch.grass_region = cat.grass_region and sch.keyword = cat.keyword


where main_category is not null
and sch.keyword != ''
and sch.keyword is not null
)
)
where search_ranking <= 20
ORDER BY 1, 2, 3, 4
) 


SELECT k.site
, k.main_category
, k.sub_category
, k.keyword
FROM keywords k
LEFT JOIN top_cat c on k.site = c.grass_region and k.main_category = c.top_l1_cat and k.sub_category = c.top_l2_cat
where gmv_ranking <= 100 
AND c.top_l2_cat is not null