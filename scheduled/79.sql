------- MTD -------
SELECT CASE WHEN mart.grass_region = 'MY' AND mart.buyer_address_state in ('Sarawak', 'Sabah', 'Labuan') then 'EM'
WHEN mart.grass_region = 'MY' then 'WM'
ELSE mart.grass_region
END AS grass_region
, mart.ordersn
, mart.consignment_no
, mart.lm_tracking_number
, DATE(from_unixtime(mart.delivery_failed_time)) AS failed_delivery_date
, mart.order_placed_local_date
, DATE(from_unixtime(mart.seller_shipout_time)) AS seller_shipout_date
, DATE(from_unixtime(coalesce(tw.krtw_wh_outbound_time, mart.wh_outbound_time))) AS wh_outbound_date
, DATE(from_unixtime(coalesce(tw.krtw_lm_inbound_time, mart.lm_inbound_time))) AS lm_inbound_date
, supp.last_mile 
, cast(oms.cogs AS DOUBLE)/100000/exr.exchange_rate AS cogs_usd
, cast(oms.cogs AS DOUBLE)/100000 AS cogs
, mart.payment_method
, DATE(from_unixtime(re.return_initiated_time)) AS return_initiated_date
FROM
(SELECT grass_region, orderid, ordersn, consignment_no, lm_tracking_number, log_id, forderid
, service_code
, whs_code
, IF(payment_method = 6, 'COD', 'Non-COD') AS payment_method
, order_placed_local_date
, seller_shipout_time
, wh_outbound_time
, lm_inbound_time
, delivery_failed_time
, channel_id
, buyer_address_state
FROM regcbbi_others.cb_logistics_mart_test
WHERE (integrated_channel_origin = 'KR' OR channel_id in (38015, 38016))
AND delivery_failed_time is NOT NULL
AND DATE(from_unixtime(seller_shipout_time)) between date_trunc('month', date(from_unixtime(1655589600))) AND date(from_unixtime(1655589600))
) mart
LEFT JOIN
(SELECT tw1.log_id
, min(CASE WHEN cast(tw2.client_channel_id AS BIGINT) = 38016 AND tw1.location = '韩国首尔子公司' AND regexp_like(lower(tw1.message), '抵達圓通物流中心') then update_time
WHEN cast(tw2.client_channel_id AS BIGINT) = 38015 AND regexp_like(lower(tw1.message), '抵達圓通物流中心') then update_time
ELSE NULL
END) AS krtw_wh_inbound_time
, min(IF(regexp_like(lower(message), '已發出'), update_time, NULL)) AS krtw_wh_outbound_time
, min(CASE WHEN cast(tw2.client_channel_id AS BIGINT) = 38016 AND lower(tw1.location) = 'taiwan' AND regexp_like(lower(tw1.message), '抵達圓通物流中心') then update_time
WHEN cast(tw2.client_channel_id AS BIGINT) = 38015 AND regexp_like(lower(tw1.message), '快件在轉運中') then update_time
ELSE NULL
END) AS krtw_lm_inbound_time
, min(IF(regexp_like(lower(message), '配送完成|取件成功'), update_time, NULL)) AS krtw_deliver_to_store_time
FROM sls_mart.shopee_sls_logistic_tw_db__logistic_tracking_tab_lfs_union_tmp tw1
JOIN sls_mart.shopee_sls_logistic_tw_db__logistic_request_tab_lfs_union_tmp tw2
ON tw1.log_id = tw2.log_id
WHERE cast(tw2.client_channel_id AS BIGINT) in (38015, 38016)
AND tw1.tz_type = 'local' AND tw2.tz_type = 'local'
GROUP BY 1
) tw
ON mart.log_id = tw.log_id
LEFT JOIN
(
SELECT log_id
, min(IF(action_status in (42), update_time, NULL)) AS return_initiated_time
FROM sls_mart.shopee_sls_logistic_vn_db__logistic_tracking_tab_lfs_union_tmp
WHERE tz_type = 'local'
GROUP BY 1
UNION
SELECT log_id
, min(IF(action_status in (42, 43, 46), update_time, NULL)) AS return_initiated_time
FROM sls_mart.shopee_sls_logistic_id_db__logistic_tracking_tab_lfs_union_tmp
WHERE tz_type = 'local'
GROUP BY 1
UNION
SELECT log_id
, min(IF(action_status in (42, 43, 46), update_time, NULL)) AS return_initiated_time
FROM sls_mart.shopee_sls_logistic_th_db__logistic_tracking_tab_lfs_union_tmp
WHERE tz_type = 'local'
GROUP BY 1
) re
ON mart.log_id = re.log_id
JOIN
(SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_sg_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600)))
UNION ALL
SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_my_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600)))
UNION ALL
SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_tw_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600)))
UNION ALL
SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_id_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600)))
UNION ALL
SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_th_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600)))
UNION ALL
SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_ph_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600)))
UNION ALL
SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_vn_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600)))
UNION ALL
SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_br_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600)))
UNION ALL
SELECT forder_id, cogs FROM oms_mart.shopee_oms_parcel_mx_db__forder_tab__reg_daily_s0_live
WHERE DATE(from_unixtime(ctime)) >= date_add('month', -6, date(from_unixtime(1655589600))) 
) oms
ON mart.forderid = oms.forder_id
LEFT JOIN
(SELECT grass_region AS country, grass_date, exchange_rate
FROM mp_order.dim_exchange_rate__reg_s0_live
WHERE grass_date >= date_add('month', -6, date(from_unixtime(1655589600)))
) exr
ON mart.grass_region = exr.country
AND mart.order_placed_local_date = exr.grass_date
LEFT JOIN
(SELECT *
FROM
(SELECT *
, row_number() over (partition BY data_date, service_code, whs_code ORDER BY ingestion_timestamp DESC) AS rn 
FROM
(SELECT coalesce(DATE(date_parse(trim(data_date), '%Y-%m-%d')), DATE(from_unixtime(cast(ingestion_timestamp AS BIGINT)))) AS data_date
, trim(service_code) AS service_code
, trim(whs_code) AS whs_code
, trim(first_leg) AS first_leg
, trim(fl_handover) AS fl_handover
, trim(custom_clearance) AS custom_clearance
, trim(lm_handover) AS lm_handover
, trim(last_mile) AS last_mile
, cast(ingestion_timestamp AS BIGINT) AS ingestion_timestamp
FROM regcbbi_others.shopee_regional_cb_team__service_code_supplier_tab_new
WHERE DATE(from_unixtime(cast(ingestion_timestamp AS BIGINT))) >= date_add('month', -6, date(from_unixtime(1655589600)))
)
)
WHERE rn = 1
) supp 
ON mart.service_code = supp.service_code
AND mart.whs_code = supp.whs_code
AND DATE(from_unixtime(mart.seller_shipout_time)) = supp.data_date
ORDER BY 1, 5