WITH im_p
AS (SELECT grass_region,
item_id AS p_itemid,
name --,rating_score, rating_total_cnt
FROM mp_item.dim_item__reg_s0_live
WHERE is_cb_shop = 1
AND grass_region IN ( 'MY', 'BR' )
AND grass_date = (SELECT Max(grass_date) mgd
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region IN ( 'MY', 'BR' ))
AND tz_type = 'local'),
im_a
AS (SELECT item_id AS a_itemid,
name,
status --,rating_score, rating_total_cnt
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region IN ( 'PL' )
AND grass_date = (SELECT Max(grass_date) mgd
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region IN ( 'PL' ))
AND tz_type = 'local'),
rm_p
AS (SELECT grass_region,
order_id,
item_id AS p_itemid,
Max_by(return_reason_id, return_created_timestamp) AS
return_reason_id,
Max(return_created_datetime) AS
return_created_datetime
FROM mp_order.dwd_return_item_all_ent_df__reg_s0_live
WHERE grass_region IN ( 'BR', 'MY' )
AND grass_date =
(SELECT Max(grass_date) mgd
FROM mp_order.dwd_return_item_all_ent_df__reg_s0_live
WHERE grass_region IN ( 'BR', 'MY' ))
GROUP BY 1,
2,
3
HAVING DATE(CAST(Max(return_created_datetime) AS TIMESTAMP)) BETWEEN
Date_add('day', -30, CURRENT_DATE) AND
Date_add('day', -1, CURRENT_DATE)),
rm_a
AS (SELECT order_id,
item_id AS a_itemid,
Max_by(return_reason_id, return_created_timestamp) AS
return_reason_id,
Max(return_created_datetime) AS
return_created_datetime
FROM mp_order.dwd_return_item_all_ent_df__reg_s0_live
WHERE grass_region = 'PL'
AND grass_date =
(SELECT Max(grass_date) mgd
FROM mp_order.dwd_return_item_all_ent_df__reg_s0_live
WHERE grass_region IN ( 'PL' ))
GROUP BY 1,
2
HAVING DATE(CAST(Max(return_created_datetime) AS TIMESTAMP)) BETWEEN
Date_add('day', -30, CURRENT_DATE) AND
Date_add('day', -1, CURRENT_DATE)),
om
AS (SELECT is_cb_shop,
item_id AS a_itemid,
shop_id
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE grass_date > DATE('2021-01-01')
AND is_bi_excluded = 0
AND DATE(CAST(create_datetime AS TIMESTAMP)) <=
Date_add('day', -1, CURRENT_DATE)
AND grass_region = 'PL'
AND tz_type = 'local'
GROUP BY 1,
2,
3),
sip
AS (SELECT affi_itemid AS a_itemid,
mst_shopid,
mst_itemid AS p_itemid
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE affi_country = 'PL'),
omi_a
AS (SELECT item_id AS a_itemid,
placed_order_cnt_30d,
placed_order_fraction_30d
FROM mp_order.dws_item_gmv_nd__reg_s0_live
WHERE grass_date = Date_add('day', -1, CURRENT_DATE)
AND grass_region = 'PL'
AND tz_type = 'local'),
omi_p
AS (SELECT grass_region,
item_id AS p_itemid,
placed_order_cnt_30d,
placed_order_fraction_30d
FROM mp_order.dws_item_gmv_nd__reg_s0_live
WHERE grass_region IN ( 'MY', 'BR' )
AND grass_date = Date_add('day', -1, CURRENT_DATE)
AND tz_type = 'local'),
ic
AS (SELECT extinfo.country AS grass_region,
itemid,
orderid,
Max(ctime) AS ctime,
Max_by(extinfo, ctime) AS extinfo,
Max_by(rating_star, ctime) AS rating_star
FROM
marketplace.shopee_item_comment_db__item_cmt_v2_tab__reg_daily_s0_live
WHERE extinfo.country IN ( 'MY', 'BR', 'PL' )
GROUP BY 1,
2,
3),
ic_p
AS (SELECT grass_region,
itemid AS p_itemid,
Avg(rating_star) AS p_L30D_sku_rating,
COUNT(rating_star) AS p_L30D_sku_rating_cnt
FROM ic
WHERE grass_region IN ( 'MY', 'BR' )
-- AND DATE(at_timezone(from_unixtime(ctime), (CASE extinfo.country WHEN 'MY' THEN 'Singapore' WHEN 'BR' THEN 'America/Araguaina' END))) 
AND DATE(From_unixtime(ctime - CASE grass_region
WHEN 'MY' THEN 0
WHEN 'BR' THEN 11
END * 3600))
-- alternative timezone implementation
BETWEEN Date_add('day', -30, CURRENT_DATE) AND
Date_add('day', -1, CURRENT_DATE)
GROUP BY 1,
2),
ic_a
AS (SELECT itemid
AS
a_itemid,
Avg(rating_star)
AS
a_L30D_sku_rating,
COUNT(rating_star)
AS
a_L30D_sku_rating_cnt,
Avg(Coalesce(extinfo.detailed_rating.product_quality,
rating_star))
AS
a_L30D_sku_rating_pq_replace,
Avg(extinfo.detailed_rating.product_quality)
AS
a_L30D_sku_rating_pq_only,
COUNT(extinfo.detailed_rating.product_quality)
AS
a_L30D_sku_rating_pq_cnt
FROM ic
WHERE grass_region IN ( 'PL' )
AND DATE(From_unixtime(ctime) AT TIME zone 'Poland') BETWEEN
Date_add('day', -30, CURRENT_DATE) AND
Date_add('day', -1, CURRENT_DATE)
GROUP BY 1),
ums
AS (SELECT user_name,
shop_id,
user_id
FROM mp_user.dim_shop__reg_s0_live
WHERE grass_region = 'PL'
AND grass_date = (SELECT Max(grass_date) mgd
FROM mp_user.dim_shop__reg_s0_live
WHERE grass_region = 'PL')
AND tz_type = 'local'),
umse
AS (SELECT shop_id,
is_cb_sip_affiliated
FROM mp_user.dim_shop_ext__reg_s0_live
WHERE grass_region = 'PL'
AND grass_date = (SELECT Max(grass_date) mgd
FROM mp_user.dim_shop_ext__reg_s0_live
WHERE grass_region = 'PL')
AND tz_type = 'local'),
raw
AS (SELECT om.is_cb_shop,
is_cb_sip_affiliated,
mst_shopid
AS
p_shopid,
shop_id
AS
a_shopid,
ums.user_name
AS
a_seller_username,
ums.user_id
AS
a_seller_userid,
p_itemid,
a_itemid,
im_a.name
AS
item_name,
grass_region
AS
origin_country,
ic_p.p_l30d_sku_rating,
ic_p.p_l30d_sku_rating_cnt,
ic_a.a_l30d_sku_rating,
ic_a.a_l30d_sku_rating_cnt,
a_l30d_sku_rating_pq_replace,
a_l30d_sku_rating_pq_only,
a_l30d_sku_rating_pq_cnt,
omi_p.placed_order_cnt_30d
AS
p_L30D_total_placed_order_cnt,
omi_a.placed_order_cnt_30d
AS
a_L30D_total_placed_order_cnt,
CAST(omi_a.placed_order_cnt_30d AS DOUBLE) / 30
AS
a_L30D_ADO,
omi_p.placed_order_fraction_30d
AS
p_L30D_total_placed_order_fraction,
omi_a.placed_order_fraction_30d
AS
a_L30D_total_placed_order_fraction,
COUNT(DISTINCT rm_p.order_id)
AS
p_L30D_RR_Orders,
COUNT(DISTINCT rm_a.order_id)
AS
a_L30D_RR_Orders,
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 1 ) THEN
rm_p.order_id
ELSE NULL
END)
AS
p_L30D_RR_Orders_nonreceipt,
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 2 ) THEN
rm_p.order_id
ELSE NULL
END)
AS
p_L30D_RR_Orders_wrongitem,
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 103 ) THEN
rm_p.order_id
ELSE NULL
END)
AS
p_L30D_RR_Orders_incomplete,
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 105 ) THEN
rm_p.order_id
ELSE NULL
END)
AS
p_L30D_RR_Orders_counterfeit,
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 106 ) THEN
rm_p.order_id
ELSE NULL
END)
AS
p_L30D_RR_Orders_damaged,
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 107 ) THEN
rm_p.order_id
ELSE NULL
END)
AS
p_L30D_RR_Orders_faulty,
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 100 ) THEN
rm_p.order_id
ELSE NULL
END)
AS
p_L30D_RR_Orders_noreason,
( CAST(COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 2 ) THEN
rm_p.order_id
ELSE NULL
END) AS DOUBLE) + ( CAST(
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN (
105
) THEN
rm_p.order_id
ELSE NULL
END) AS DOUBLE) )
+ ( CAST(
COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 107 ) THEN
rm_p.order_id
ELSE NULL
END) AS DOUBLE) ) ) /
omi_p.placed_order_cnt_30d
AS
p_L30D_RR_ratio,
CASE
WHEN COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 2 ) THEN
rm_p.order_id
ELSE NULL
END) >= 3
OR COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 105
)
THEN
rm_p.order_id
ELSE NULL
END) >= 3
OR COUNT(DISTINCT CASE
WHEN rm_p.return_reason_id IN ( 107
)
THEN
rm_p.order_id
ELSE NULL
END) >= 3 THEN 'Yes'
ELSE 'No'
END
AS
RR
FROM om
LEFT JOIN sip USING (a_itemid)
LEFT JOIN im_p USING (p_itemid)
LEFT JOIN im_a USING (a_itemid)
LEFT JOIN omi_p USING (grass_region, p_itemid)
LEFT JOIN omi_a USING (a_itemid)
LEFT JOIN rm_p USING (grass_region, p_itemid)
-- ON RM_p.item_id = IM_p.item_id AND RM_p.grass_region IN ('MY', 'BR')
LEFT JOIN rm_a USING (a_itemid)
-- ON RM_a.item_id = OM.item_id AND RM_a.grass_region = 'PL'
LEFT JOIN ic_p USING (grass_region, p_itemid)
LEFT JOIN ic_a USING (a_itemid)
LEFT JOIN ums USING (shop_id)
LEFT JOIN umse USING (shop_id)
WHERE im_a.status = 1
GROUP BY 1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
16,
17,
18,
19,
20,
21,
22)
SELECT DATE(Date_trunc('week', Date_add('day', 1, CURRENT_DATE))) AS ref_week,
*,
CASE
WHEN ( p_l30d_sku_rating >= 1
AND p_l30d_sku_rating < 4
AND p_l30d_sku_rating_cnt >= 15
-- AND p_L30D_sku_rating_pq_replace > 1 
-- AND p_L30D_sku_rating_pq_replace < 4
) THEN 'Low Rating'
WHEN ( p_l30d_rr_ratio >= 0.1
AND rr = 'Yes' ) THEN 'High RR'
END AS reason
FROM raw
WHERE
-- Poor SKU Rating 
( p_l30d_sku_rating >= 1
AND p_l30d_sku_rating < 4
AND p_l30d_sku_rating_cnt >= 15
-- AND p_L30D_sku_rating_pq_replace > 1 
-- AND p_L30D_sku_rating_pq_replace < 4
)
OR
-- High RR
( p_l30d_rr_ratio >= 0.1
AND rr = 'Yes' )