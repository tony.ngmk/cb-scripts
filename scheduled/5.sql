WITH target AS
(
SELECT lane ,
cast(affi_itemid AS bigint) itemid ,
mst_region ,
affi_region ,
date(date_parse(upload_date,'%Y%m%d')) upload_date
FROM regcbbi_others.sip__bau_optimized_sku_list__wf__reg__s0
WHERE date(date_parse(upload_date,'%Y%m%d')) BETWEEN CURRENT_DATE - interval '21' day AND CURRENT_DATE - interval '15' day
)
,
sip AS
(
SELECT affi_shopid ,
mst_shopid ,
t1.country AS mst_country ,
t2.country AS affi_country ,
t1.cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN
(
SELECT affi_shopid ,
affi_username ,
mst_shopid ,
country
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status!=3
AND grass_region IN ('TW',
'PH',
'CL',
'CO',
'MX',
'MY',
'SG') ) t2
ON t1.shopid = t2.mst_shopid
)
,
ex AS
(
SELECT DISTINCT itemid
FROM marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
CROSS JOIN unnest( cast( try(json_extract(try(from_utf8(data)),'$.Fields')) AS array( row(NAME varchar, old varchar, new varchar) ) ) )
WHERE (
try(json_extract_scalar(try(from_utf8(data)),'$.Operator')) IN ('zhixian.toh@shopee.com')
OR try(json_extract_scalar(try(from_utf8(data)),'$.Source')) = 'Seller Center Single')
AND NAME IN ('name')
AND country IN ('TW',
'PH',
'CL',
'CO',
'MX',
'MY',
'SG') --change countries
AND cast(grass_date AS date) BETWEEN CURRENT_DATE - interval '120' day AND CURRENT_DATE - interval '1' day
UNION
SELECT cast(affi_itemid AS bigint) itemid
FROM regcbbi_others.sip__bau_optimized_sku_list__wf__reg__s0
) SELECT lane
, CAST(affi_itemid AS bigint) itemid 
, mst_region
, affi_region
, date(date_parse(upload_date,'%Y%m%d')) upload_date
FROM regcbbi_others.sip__bau_optimized_sku_list__wf__reg__s0
WHERE date(date_parse(upload_date,'%Y%m%d')) BETWEEN current_date - interval '21' day AND current_date - interval '15' day 
)


, sip AS (
SELECT affi_shopid
, mst_shopid
, t1.country as mst_country 
, t2.country as affi_country 
, t1.cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status!=3 AND grass_region IN ('TW','PH','CL','CO','MX','MY','SG')
) t2 ON t1.shopid = t2.mst_shopid
)


, ex AS (
SELECT DISTINCT itemid
FROM marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
CROSS JOIN UNNEST(
CAST(
try(json_extract(try(from_utf8(data)),'$.Fields')) AS ARRAY(
ROW(name VARCHAR, old VARCHAR, new VARCHAR)
)
)
)
WHERE (try(json_extract_scalar(try(from_utf8(data)),'$.Operator')) IN ('zhixian.toh@shopee.com') 
OR try(json_extract_scalar(try(from_utf8(data)),'$.Source')) = 'Seller Center Single')
AND name IN ('name')
AND country IN ('TW','PH','CL','CO','MX','MY','SG') --change countries
AND CAST(grass_date as date) BETWEEN current_date - interval '120' day AND current_date - interval '1' day 
UNION
SELECT CAST(affi_itemid AS bigint) itemid 
FROM regcbbi_others.sip__bau_optimized_sku_list__wf__reg__s0
)


, item_status AS (
SELECT item_id
, shop_id
, level1_global_be_category
, lane 
, upload_date
, mst_region
, affi_region
, SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END) count_status_normal_days
, SUM(CASE WHEN stock > 0 THEN 1 ELSE 0 END) count_stock_normal_days
, SUM(CASE WHEN seller_status = 1 THEN 1 ELSE 0 END) count_shop_normal_days
, SUM(CASE WHEN is_holiday_mode = 0 THEN 1 ELSE 0 END) count_shop_no_holiday_days
FROM (
SELECT grass_date
, item_id
, shop_id 
, level1_global_be_category
, lane 
, upload_date
, mst_region
, affi_region
, status 
, stock 
, seller_status
, is_holiday_mode
FROM mp_item.dim_item__reg_s0_live i
INNER JOIN sip ON sip.affi_shopid = i.shop_id
INNER JOIN (SELECT DISTINCT lane, mst_region, affi_region, upload_date FROM target) t ON (i.grass_date = t.upload_date + interval '1' day OR i.grass_date = t.upload_date + interval '14' day) AND t.mst_region = sip.mst_country AND t.affi_region = sip.affi_country
LEFT JOIN ex ON ex.itemid = i.item_id 
WHERE i.grass_region IN ('TW','PH','CL','CO','MX','MY','SG')
AND i.grass_date BETWEEN current_date - interval '21' day AND current_date - interval '1' day
AND i.tz_type = 'local'
AND i.is_cb_shop = 1
AND ex.itemid IS NULL
)
GROUP BY 1,2,3,4,5,6,7
)


, item_status_live AS (
SELECT item_id 
, shop_id 
, level1_global_be_category
, lane 
, upload_date
, mst_region
, affi_region
FROM item_status i
WHERE count_status_normal_days = 2
AND count_stock_normal_days = 2
AND count_shop_normal_days = 2
AND count_shop_no_holiday_days = 2
)


, imp AS (
SELECT grass_date
, level1_global_be_category
, lane 
, upload_date
, mst_region 
, affi_region
, COUNT(DISTINCT CASE WHEN i.item_organic_imp_uv_1d > 0 THEN i.item_id ELSE NULL END) AS item_with_imp_cnt
, COUNT(DISTINCT CASE WHEN i.item_organic_pdp_uv_1d > 0 THEN i.item_id ELSE NULL END) AS item_with_view_cnt
, SUM(i.item_organic_pdp_uv_1d) AS item_organic_pdp_uv_1d
, SUM(i.item_organic_imp_uv_1d) AS item_organic_imp_uv_1d
FROM mp_search.dws_item_view_imp_clk_1d__reg_s1_live i 
INNER JOIN item_status_live l ON i.item_id = l.item_id AND i.grass_date BETWEEN l.upload_date - interval '14' day AND l.upload_date + interval '14' day
WHERE i.grass_region IN ('TW','PH','CL','CO','MX','MY','SG')
AND i.tz_type = 'local'
AND grass_date BETWEEN current_date - interval '35' day AND current_date - interval '1' day
GROUP BY 1,2,3,4,5,6
)


, orders AS (
SELECT grass_date
, l.level1_global_be_category
, lane 
, upload_date
, mst_region 
, affi_region
, COUNT(DISTINCT o.item_id) AS item_with_organic_order_cnt
, SUM(o.order_fraction) AS organic_order_fraction 
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o 
INNER JOIN item_status_live l ON o.item_id = l.item_id AND o.grass_date BETWEEN l.upload_date - interval '14' day AND l.upload_date + interval '14' day 
WHERE o.grass_region IN ('TW','PH','CL','CO','MX','MY','SG')
AND o.tz_type = 'local'
AND o.grass_date BETWEEN current_date - interval '35' day AND current_date - interval '1' day
AND o.is_placed = 1 
AND o.is_flash_sale = 0
GROUP BY 1,2,3,4,5,6
)


SELECT imp.grass_date
, i.level1_global_be_category
, i.lane 
, i.mst_region 
, i.affi_region 
, i.upload_date
, i.total_item_count
, imp.item_with_imp_cnt
, imp.item_organic_imp_uv_1d
, imp.item_with_view_cnt
, imp.item_organic_pdp_uv_1d
, o.item_with_organic_order_cnt
, o.organic_order_fraction
FROM imp 
LEFT JOIN orders o ON imp.level1_global_be_category = o.level1_global_be_category
AND imp.lane = o.lane 
AND imp.upload_date = o.upload_date
AND imp.grass_date = o.grass_date
AND imp.mst_region = o.mst_region
AND imp.affi_region = o.affi_region
RIGHT JOIN (
SELECT level1_global_be_category
, lane 
, upload_date 
, mst_region 
, affi_region 
, COUNT(DISTINCT item_id) total_item_count
FROM item_status_live
GROUP BY 1,2,3,4,5
) i ON i.level1_global_be_category = imp.level1_global_be_category
AND i.lane = imp.lane 
AND i.upload_date = imp.upload_date
AND i.mst_region = imp.mst_region
AND i.affi_region = imp.affi_region
LIMIT 1000