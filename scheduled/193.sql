insert into nexus.test_55f06bff109a5035f487d13e5d1f87f45440d92a361c547066ab7499283dbbed_55706f8aeeaefeb65618e7e73fcdb391 WITH rank_raw AS (
SELECT
o.grass_region
, shop_id
, CASE WHEN s.affi_shopid IS NOT NULL THEN 1 ELSE 0 END AS TWCB
, SUM(order_fraction) MTD_orders
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
LEFT JOIN (
SELECT DISTINCT b.affi_shopid
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b
ON b.mst_shopid = a.shopid
WHERE
a.cb_option = 0
AND a.country = 'TW'
AND b.grass_region IN ('MY','SG','ID')
AND b.sip_shop_status <> 3
) s
ON s.affi_shopid = o.shop_id
WHERE
o.grass_region IN ('SG', 'MY', 'ID')
AND o.grass_date >= date_trunc('month', date_add('day', -1, current_date))
AND o.tz_type = 'local'
AND o.is_placed = 1
GROUP BY 1, 2, 3
) 
, sales_rank AS (
SELECT
*
, rank() OVER (PARTITION BY grass_region ORDER BY MTD_orders DESC) country_rank
, rank() OVER (PARTITION BY TWCB, grass_region ORDER BY MTD_orders DESC) TWCB_rank
FROM rank_raw
) 
, top_20 AS (
SELECT DISTINCT
grass_region
, shop_id
, TWCB_rank
, country_rank
, MTD_orders
FROM
sales_rank
WHERE 
TWCB = 1
AND TWCB_rank <= 20
) 
, sales_raw AS (
SELECT DISTINCT
o.grass_region
, o.order_id
, o.shop_id
, o.item_id
, o.model_id
, t.TWCB_rank
, t.country_rank
, CASE WHEN o.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END mtd
, CASE WHEN date_format(o.grass_date, '%y%m') = date_format(date_add('month', -1, date_add('day', -1, current_date)), '%y%m') THEN 1 ELSE 0 END m_1
, CASE WHEN o.grass_date BETWEEN date_trunc('week', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END wtd
, CASE WHEN o.grass_date BETWEEN date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) AND date_add('day', -1, date_trunc('week', date_add('day', -1, current_date))) THEN 1 ELSE 0 END w_1
, CASE WHEN o.grass_date BETWEEN date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) AND date_add('day', -8, date_trunc('week', date_add('day', -1, current_date))) THEN 1 ELSE 0 END w_2
, CASE WHEN o.item_promotion_source = 'shopee' THEN 1 ELSE 0 END campaign
, CASE WHEN o.item_promotion_source = 'flash_sale' THEN 1 ELSE 0 END flash_sale
, CASE WHEN o.pv_voucher_code IS NOT NULL OR o.fsv_voucher_code IS NOT NULL THEN 1 ELSE 0 END shopee_voucher
, CASE WHEN sv_voucher_code IS not NULL OR (is_sv_seller_absorbed = 1) THEN 1 ELSE 0 END seller_voucher
, order_fraction pp
, gmv
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
INNER JOIN top_20 t
ON o.shop_id = t.shop_id
-- LEFT JOIN (
-- SELECT DISTINCT
-- campaignid
-- , itemid
-- , grass_date
-- FROM
-- regbida_userbehavior.shopee_bi_campaign_metrics
-- WHERE ((grass_country IN ('SG', 'MY', 'ID')) AND (grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date)))))
-- ) b ON ((o.item_id = b.itemid) AND (o.grass_date = b.grass_date))
WHERE
o.grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) 
AND o.grass_region IN ('SG', 'MY', 'ID') 
AND o.tz_type = 'local' 
AND o.is_placed = 1
) 
, sales AS (
SELECT
grass_region
, shop_id
, TWCB_rank
, country_rank
, SUM(CASE WHEN mtd = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_ADO
, SUM(CASE WHEN mtd = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_Organic_Order
, SUM(CASE WHEN mtd = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_Campaign_Order
, SUM(CASE WHEN mtd = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_CFS_Order
, SUM(CASE WHEN mtd = 1 AND seller_voucher = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_Seller_Voucher_Order
, SUM(CASE WHEN mtd = 1 AND shopee_voucher = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_Shopee_Voucher_Order
, SUM(CASE WHEN mtd = 1 THEN gmv ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_GMV
, SUM(CASE WHEN m_1 = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_ADO
, SUM(CASE WHEN m_1 = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_Organic_Order
, SUM(CASE WHEN m_1 = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_Campaign_Order
, SUM(CASE WHEN m_1 = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_CFS_Order
, SUM(CASE WHEN m_1 = 1 AND seller_voucher = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_Seller_Voucher_Order
, SUM(CASE WHEN m_1 = 1 AND shopee_voucher = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_Shopee_Voucher_Order
, SUM(CASE WHEN m_1 = 1 THEN gmv ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_GMV
, SUM(CASE WHEN wtd = 1 THEN pp ELSE 0 END) / CAST(day_of_week(date_add('day', -1, current_date)) AS double) WTD_ADO
, SUM(CASE WHEN w_1 = 1 THEN pp ELSE 0 END) / DECIMAL '7.000' W_1_ADO
, SUM(CASE WHEN w_2 = 1 THEN pp ELSE 0 END) / DECIMAL '7.000' W_2_ADO
FROM
sales_raw
GROUP BY 1, 2, 3, 4
) 
, accum_order AS (
SELECT
o.shop_id
, SUM(order_fraction) accum_orders
FROM (
SELECT * 
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
grass_region in ('ID','MY','SG')
AND tz_type = 'local' 
AND is_placed = 1
) o
INNER JOIN sales t 
ON o.shop_id = t.shop_id


GROUP BY 1
) 
, user_info AS (
SELECT
u.shop_id
, user_name
, TWCB_rank
, country_rank
, u.grass_region
FROM mp_user.dim_shop__reg_s0_live u
INNER JOIN sales t 
ON u.shop_id = t.shop_id
WHERE u.grass_date = current_date - interval '1' day
AND u.tz_type = 'local'
) 
, shop_review AS (
SELECT
comment.shopid
, COUNT(DISTINCT cmtid) number_of_reviews
FROM
marketplace.shopee_item_comment_db__item_cmt_v2_tab__reg_daily_s0_live comment
INNER JOIN sales t
ON comment.shopid = t.shop_id
GROUP BY 1
) 
, traffic AS (
SELECT
exposure.shop_id
, SUM(CASE WHEN exposure.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN pv_cnt_1d ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_item_view
, SUM(CASE WHEN exposure.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN pv_cnt_1d ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_shop_view
, SUM(CASE WHEN date_format(exposure.grass_date, '%y%m') = date_format(date_add('month', -1, date_add('day', -1, current_date)), '%y%m') THEN pv_cnt_1d ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_item_view
, SUM(CASE WHEN date_format(exposure.grass_date,'%y%m') = date_format(date_add('month',-1,date_add('day', -1, current_date)),'%y%m') THEN pv_cnt_1d ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_shop_view
FROM
mp_shopflow.dws_item_civ_1d__reg_s0_live exposure
INNER JOIN sales t
ON exposure.shop_id = t.shop_id
WHERE tz_type = 'local'
AND grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date)))
GROUP BY 1
)
SELECT
u.grass_region
, u.shop_id
, u.TWCB_rank
, u.country_rank
, u.user_name
, shop_level1_category
, active_item_with_stock_cnt
, follower_cnt_td
, a.number_of_reviews
, b.MTD_ADO
, b.MTD_Organic_Order
, b.MTD_CFS_Order
, b.MTD_Campaign_Order
, b.MTD_Seller_Voucher_Order
, b.MTD_Shopee_Voucher_Order
, b.MTD_GMV
, d.MTD_shop_view
, d.MTD_item_view
, b.M_1_ADO
, b.M_1_Organic_Order
, b.M_1_CFS_Order
, b.M_1_Campaign_Order
, b.M_1_Seller_Voucher_Order
, b.M_1_Shopee_Voucher_Order
, b.M_1_GMV
, M_1_shop_view
, M_1_item_view
, b.WTD_ADO
, b.W_1_ADO
, b.W_2_ADO
, c.accum_orders
FROM user_info u
LEFT JOIN shop_review a
ON u.shop_id = a.shopid
LEFT JOIN sales b
ON u.shop_id = b.shop_id
LEFT JOIN accum_order c
ON u.shop_id = c.shop_id
LEFT JOIN traffic d
ON u.shop_id = d.shop_id
LEFT JOIN (
SELECT 
shop_id
, shop_level1_category
, active_item_with_stock_cnt
FROM mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date = current_date - interval '1' day
and tz_type = 'local'
) ua on u.shop_id = ua.shop_id
LEFT JOIN (
SELECT
shop_id
, follower_cnt_td
FROM mp_user.dws_shop_interaction_td__reg_s0_live
WHERE
grass_date = current_Date - interval '1' day
and tz_type = 'local'
) ub
on u.shop_id = ub.shop_id
ORDER BY 1 ASC, 3 ASC