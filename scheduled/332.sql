insert into nexus.test_4053f877841a4bc3ecd9c20ad35b9b320a58346f6d01097bea58e7d1e1be5047_e3c743909043a38e4d42894634d1442e WITH sip AS (
SELECT DISTINCT
b.mst_shopid
, affi_shopid
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live AS a
LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live AS b
ON b.mst_shopid = a.shopid
WHERE
a.cb_option = 0
AND a.country IN ('ID', 'TW', 'VN', 'TH')
AND b.country = 'MY'
AND b.grass_region = 'MY' 
)


SELECT DISTINCT
map.affi_itemid
, map.affi_modelid
, map.mst_itemid
, map.mst_modelid
, pi.model_stock
, pi.model_status
, pi.model_name
, pi.item_stock
, mst_shopid
, map.affi_shopid
, grass_region mst_country
, seller_name
, pi.item_status mst_item_status
, stock
, status
, pi.is_holiday_mode mst_is_holiday_mode
, pi.shop_status mst_shop_status
, pi.seller_status mst_seller_status
FROM (
SELECT -- DISTINCT
item_id
, model_id
, model_stock
, model_status
, model_name
, mst_shopid
, grass_region
, seller_name
, item_stock
, item_status
, is_holiday_mode
, seller_status
, shop_status
FROM
mp_item.dim_model__reg_s0_live AS i
INNER JOIN sip
ON sip.mst_shopid = i.shop_id
WHERE
grass_date = (current_date - INTERVAL '1' DAY)
AND grass_region IN ('TW', 'ID', 'VN', 'TH')
AND tz_type = 'local'
AND is_cb_shop = 0
) AS pi
INNER JOIN (
SELECT
affi_itemid
, affi_modelid
, mst_itemid
, mst_modelid
, affi_shopid
FROM
marketplace.shopee_sip_v2_db__msku_map_tab__reg_daily_s0_live
WHERE
affi_country = 'MY'
) AS map
ON pi.item_id = map.mst_itemid
AND pi.model_id = map.mst_modelid
INNER JOIN regcbbi_others.shopee_regional_cb_team__local_campaign_shop_item_model_lvl AS m 
ON CAST(m.affi_itemid AS bigint) = map.affi_itemid
AND CAST(m.affi_modelid AS bigint) = map.affi_modelid
LEFT JOIN (
SELECT -- DISTINCT
item_id
, stock
, status
FROM
mp_item.dim_item__reg_s0_live AS i
WHERE
grass_date = (current_date - INTERVAL '1' DAY)
AND grass_region = 'MY'
AND tz_type = 'local'
AND is_cb_shop = 1
) AS ai 
ON ai.item_id = map.affi_itemid