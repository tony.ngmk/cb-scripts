INSERT INTO regcbbi_others.shopee_bi_keyreports_local_sip_revenue_v3 
SELECT * FROM (
with os AS (SELECT DISTINCT shop_id
, IF(is_official_shop = 1,'OS','Non-OS') AS shop_option
FROM mp_user.dim_shop__reg_s0_live
WHERE grass_date = CURRENT_DATE - INTERVAL '1' DAY AND grass_region = 'MY' AND is_cb_shop = 1
AND is_official_shop = 1)

, s AS (SELECT DISTINCT affi_shopid
, mst_shopid
, CONCAT(m.country,a.grass_region) AS lane 
, COALESCE(os.shop_option,'Non-OS') os_option
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a 
JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live m ON a.mst_shopid = m.shopid
LEFT JOIN os ON a.affi_shopid = os.shop_id
WHERE m.cb_option = 0 AND a.grass_region <> '')

, o AS (SELECT DISTINCT 
shop_id
, DATE(FROM_UNIXTIME(create_timestamp)) AS create_datetime
, order_id
, SUM(gmv) gmv -- charging base for transaction fee
, SUM(gmv_usd) gmv_usd
, SUM(commission_base_amt) commission_base_amt -- charging base for commision and service fee
, SUM(commission_base_amt_usd) commission_base_amt_usd
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE is_cb_shop = 1 AND grass_date >= DATE('2019-01-01')
AND grass_region IN ('SG','MY','TW','PH','TH', 'VN', 'BR', 'MX', 'CO','CL','PL')
GROUP BY 1,2,3)

, lc AS (SELECT DISTINCT lane
, DATE(date_parse(start_date,'%Y-%m-%d')) AS start_date
, DATE(date_parse(end_date,'%Y-%m-%d')) AS end_date
, TRY_CAST(txn_perc AS DOUBLE) txn_perc
, TRY_CAST(com_fee AS DOUBLE) com_fee
, TRY_CAST(ser_perc AS DOUBLE) ser_perc
, IF(COALESCE(os_option,'Non-OS') LIKE '%Non-OS%','Non-OS','OS') AS os_option
FROM regcbbi_others.shopee_regional_cb_team__local_sip_lane_charge_rate a 
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp FROM regcbbi_others.shopee_regional_cb_team__local_sip_lane_charge_rate) b
ON a.ingestion_timestamp = b.ingestion_timestamp)

, sc AS (SELECT DISTINCT lane
, TRY_CAST(shopid AS BIGINT) shopid
, DATE(date_parse(start_date,'%Y-%m-%d')) AS start_date
, DATE(date_parse(end_date,'%Y-%m-%d')) AS end_date
, TRY_CAST(txn_perc AS DOUBLE) txn_perc
, TRY_CAST(com_fee AS DOUBLE) com_fee
, TRY_CAST(ser_perc AS DOUBLE) ser_perc
, IF(os_option LIKE '%Non-OS%','Non-OS','OS') AS os_option
FROM regcbbi_others.shopee_regional_cb_team__local_sip_shop_charge_rate a 
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp FROM regcbbi_others.shopee_regional_cb_team__local_sip_shop_charge_rate) b
ON a.ingestion_timestamp = b.ingestion_timestamp)

SELECT DISTINCT s.lane 
, s.os_option
, s.affi_shopid
, s.mst_shopid 
, create_datetime AS grass_date
, order_id AS orderid
, IF(sc.shopid IS NULL,lc.com_fee,sc.com_fee) comm_fee_perc
, IF(sc.shopid IS NULL,lc.ser_perc,sc.ser_perc) serv_fee_perc
, IF(sc.shopid IS NULL,lc.txn_perc,sc.txn_perc) cc_fee_perc 
, MAX(o.gmv) AS gmv
, MAX(o.gmv_usd) AS gmv_usd
, MAX(o.commission_base_amt) AS commission_base_amt
, MAX(o.commission_base_amt_usd) AS commission_base_amt_usd
, ROUND(MAX(o.gmv*IF(sc.shopid IS NULL,lc.txn_perc,sc.txn_perc)),2) AS cc_fee 
, ROUND(MAX(o.commission_base_amt*IF(sc.shopid IS NULL,lc.com_fee,sc.com_fee)),2) AS commission_fee 
, ROUND(MAX(o.commission_base_amt*IF(sc.shopid IS NULL,lc.ser_perc,sc.ser_perc)),2) AS service_fee
, MAX(o.gmv_usd*IF(sc.shopid IS NULL,lc.txn_perc,sc.txn_perc)) AS cc_fee_usd 
, MAX(o.commission_base_amt_usd*IF(sc.shopid IS NULL,lc.com_fee,sc.com_fee)) AS commission_fee_usd 
, MAX(o.commission_base_amt_usd*IF(sc.shopid IS NULL,lc.ser_perc,sc.ser_perc)) AS service_fee_usd 
FROM s 
JOIN o ON s.affi_shopid = o.shop_id
JOIN lc ON s.lane = lc.lane AND s.os_option = lc.os_option AND o.create_datetime BETWEEN lc.start_date AND lc.end_date
LEFT JOIN sc ON s.lane = sc.lane AND o.create_datetime BETWEEN sc.start_date AND sc.end_date AND o.shop_id = sc.shopid
GROUP BY 1,2,3,4,5,6,7,8,9
-- limit 100 
)