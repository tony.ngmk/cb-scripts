insert into nexus.test_14cd0f48e4d86c6c51b9ae113ba222b0651ae14acb9553757ef7bee9bf91eee5_594ab9e63f4c77ba97e5fe063671ee9b with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status!=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
--and grass_region='MX'
) t2 ON t1.shopid = t2.mst_shopid
-- where cb_option=1 
--and t1.country ='ID'
)


, wh as (select distinct item_id 
from dev_regcbbi_others.sip__item_weight_adj_order__df__reg__s0 o 
where actual_weight >0 
)




select sip.cb_option
, sip.mst_country
, sip.affi_country 
, count(distinct i.item_id) live_sku 
, count(distinct case when o.item_id is not null then i.item_id else null end) live_sku_with_l360d_ado 
, count(distinct case when wh.item_id is not null then i.item_id else null end ) live_sku_with_single_order 
, count(distinct case when i.item_real_weight >0 then i.item_id else null end) live_sku_with_sip_weight 


from (select distinct shop_id 
, item_id 
, status 
, seller_status 
, item_real_weight/100.00 item_real_weight
from mp_item.ads_sip_affi_item_profile__reg_s0_live
where grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and seller_status=1 and shop_status=1 and is_holiday_mode=0 and status=1 and stock>0 
and grass_date = if(grass_region in ('SG','MY','PH','TH','ID','VN','TW'), current_date- interval'1'day ,current_date- interval'2'day )
and primary_seller_region !='SG'
) i 
join sip on sip.affi_shopid = i.shop_id 
left join wh on wh.item_id = i.item_id 
left join (select distinct item_id 
, order_fraction
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where is_cb_shop=1 and grass_date >= current_date-interval'361'day and date(create_datetime) between current_date-interval'360'day and current_date-interval'1'day 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) o on o.item_id= i.item_id 
group by 1,2,3