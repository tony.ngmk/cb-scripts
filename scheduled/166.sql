insert into nexus.test_ce9f8dea2d95f12d59bfbe4ea9dcd3e46ee0bc818503a0e7e620014b0941feb8_fa2c68673d243758681ef101f5db9d9a with fbs_shop as (
select
status,
is_holiday_mode,
shop_id,
is_official_shop,
is_preferred_shop,
rating_star,
response_rate
from
mp_user.dim_shop__reg_s0_live
where
grass_date = date_add('day',-1,current_date)
and is_fbs = 1
and is_cb_shop = 1
and tz_type = 'local'
group by
1,2,3,4,5,6,7
),
seller as (
select 
s.gp_smt,
s.user_name as shop_name,
s.shop_id as shopid,
s.gp_name as gp_account_name,
s.grass_region,
pff_tag
from
dev_regcbbi_kr.krcb_shop_profile as s
join
fbs_shop as f
on
s.shop_id = f.shop_id
LEFT JOIN (
SELECT
shop_id,
pff_tag
FROM
regcbbi_general.cbwh_shop_history_v2
WHERE
grass_date in (
select
max(grass_date) as latest_ingest_date
from
regcbbi_general.cbwh_shop_history_v2
)
) as p 
ON 
p.shop_id = s.shop_id
where
s.user_name not in (
'click_to_korea.my','spigen.os' 
) and s.grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
) 
),
merge as (
SELECT
o.grass_date,
s.gp_smt,
s.gp_account_name,
s.grass_region,
s.shopid,
s.shop_name,
o.order_sn as ordersn,
o.item_id as itemid,
case when o.grass_region = 'BR' then o.level1_category else o.level1_global_be_category end as main_category,
o.model_id as modelid,
case
when o.grass_region = 'MY' then concat('https://shopee.com.my/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'ID' then concat('https://shopee.co.id/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'PH' then concat('https://shopee.ph/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'SG' then concat('https://shopee.sg/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'TH' then concat('https://shopee.co.th/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'TW' then concat('https://shopee.tw/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'VN' then concat('https://shopee.vn/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'BR' then concat('https://shopee.com.br/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'MX' then concat('https://shopee.com.mx/product/',cast(s.shopid as varchar),'/',cast(o.item_id as varchar))
end as product_url,
case when o.item_promotion_source = 'flash_sale' then 'CFS' else 'non-CFS' end as order_type,
avg(item_price_before_discount_pp) as item_price_before_discount_local,
avg(item_price_pp) as item_price_local,
sum(order_fraction) as gross_orders,
sum(item_amount) as item_sold_qty,
sum(gmv_usd) as gmv_usd
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live as o
JOIN
seller as s
ON
o.shop_id = s.shopid
and o.is_placed = 1
and o.is_bi_excluded = 0
and o.fulfilment_source = 'FULFILLED_BY_SHOPEE'
and s.pff_tag = 1
where
o.grass_date between date_add('day',-9,current_date) and date_add('day',-1,current_date)
group by
1,2,3,4,5,6,7,8,9,10,11,12
),
item_info AS (
SELECT
distinct
ip.item_id as itemid,
ip.item_name as name,
ip.model_id as modelid,
ip.model_name
FROM
mp_item.dim_model__reg_s0_live as ip
JOIN
seller as s
ON
ip.shop_id = s.shopid
and ip.tz_type = 'local'
and ip.grass_date = date_add('day',-1,current_date)
where
ip.grass_region <> ''
)
select
m.grass_date,
m.gp_smt,
m.gp_account_name,
m.grass_region,
m.shopid,
m.shop_name,
m.ordersn,
m.itemid,
m.main_category,
i.name,
m.modelid,
i.model_name,
m.product_url,
m.order_type,
m.item_price_before_discount_local,
m.item_price_local,
m.gross_orders,
m.item_sold_qty,
m.gmv_usd
from
merge as m
join
item_info as i
on
i.itemid = m.itemid
and i.modelid = m.modelid
order by
1 desc,2,3,4,5,7,8