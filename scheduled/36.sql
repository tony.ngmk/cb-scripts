insert into nexus.test_1811b3ee0aff4d535cb0483456442440a387febede5b1862ad6b7b1ebca8e27b_6fee5f832936358a4b1188f4f8d1e335 WITH
sip AS (
SELECT DISTINCT
affi_shopid
, mst_shopid
, t1.country mst_country
, t2.country affi_country
, t1.cb_option
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (sip_shop_status <> 3) and grass_region in ('PL','ES','FR')
) t2 ON (t1.shopid = t2.mst_shopid))
WHERE (t2.country in ('PL','ES','FR') )
) 
SELECT DISTINCT
current_date update_time
, affi_country
, "count"(DISTINCT affi_itemid) sku
, "sum"(l30d_ado) l30d_ado
FROM
(
SELECT DISTINCT
affi_shopid
, mst_shopid
, itemid affi_itemid
, mst_itemid
, name
, status
, l30d_ado
, keyword
,affi_country 
FROM
(
SELECT DISTINCT
affi_shopid
, mst_shopid
, mst_itemid
, i.itemid
, name
, status
, map.affi_country 
, (CASE WHEN (k.keyword IS NOT NULL) THEN k.keyword ELSE 'N' END) keyword
, l30d_ado
FROM
(((sip
INNER JOIN (
SELECT DISTINCT
item_id itemid
, shop_id shopid
, replace(lower(name),'_',' ') name 
, status
FROM
mp_item.dim_item__reg_s0_live
WHERE ((NOT (status IN (0, 4, 5,8))) AND (grass_region in('PL','ES','FR') )) and is_cb_shop=1 and grass_date=current_date-interval'1'day and seller_status=1 and shop_status=1 and is_holiday_mode=0 
) i ON (i.shopid = sip.affi_shopid))
INNER JOIN (
SELECT DISTINCT
affi_itemid
, mst_itemid
, affi_country 
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE (affi_country in ('PL','ES','FR') )
) map ON (map.affi_itemid = i.itemid))


INNER JOIN (
SELECT DISTINCT keyword
, country 
FROM
regcbbi_others.prohibited_keyword_tmp_new_market_s0
) k ON regexp_like(name , "concat"('\b', CAST(keyword AS varchar), '\b'))
and k.country = map.affi_country 
LEFT JOIN (
SELECT DISTINCT
item_id
, ("sum"(order_fraction) / DECIMAL '30.00') l30d_ado
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (((is_cb_shop = 1) AND (grass_region IN ('MY', 'BR'))) AND ("date"("split"(create_datetime, ' ')[1]) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) and grass_date >= (current_date - INTERVAL '30' DAY) 
GROUP BY 1
) O ON (O.ITEM_ID = MAP.mst_itemid))
) 
-- WHERE (keyword <> 'N')
) 
GROUP BY 1, 2