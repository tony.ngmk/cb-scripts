insert into nexus.test_65eba2cded0dba7bdb59c17bb6b3fc5d6ba74c99a724b26cad8acf770abf084b_1ca02c00fc966fa2c77348ac96cc89ce --- shop --- live sku --- live model --- model edit y/n y--- price up/down --- avg 


with t as (select distinct cast(affi_shopid as BIGINT) affi_shopid 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0
where batch='batch4'
)




SELECT DISTINCT
m.grass_date, 
count(distinct m.model_id) live_model, 
count(distinct CASE WHEN m.model_price < m.model_price_before_discount THEN m.model_id END ) live_model_dp , 
try(count(distinct CASE WHEN m.model_price < m.model_price_before_discount and dp.model_id is not null 
THEN m.model_id END )*decimal'1.000'/count(distinct m.model_id)) model_with_dp_perc_sip , 


try(count(distinct CASE WHEN m.model_price < m.model_price_before_discount and dp.model_id is null 
THEN m.model_id END )*decimal'1.000'/count(distinct m.model_id)) model_with_dp_perc_seller 
FROM (select distinct 
item_id 
, shop_id 
, model_id 
, model_price
, model_price_before_discount
, grass_date
, grass_region
from 
mp_item.dim_model__reg_s0_live m 
WHERE m.tz_type = 'local' 
and m.seller_status = 1 and m.shop_status = 1 and model_status = 1 and m.item_status = 1 and m.is_cb_shop = 1 and m.is_holiday_mode = 0 
and m.grass_date= current_date-interval'2'day 
and m.grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) m 
join t on t.affi_shopid = m.shop_id 
left join (select distinct 
cast(model_id as bigint ) model_id 
,cast(model_price as decimal(10,2)) model_price 
from regcbbi_others.return_shop_dp_add_reg_s0 
where ingestion_timestamp != '0' 

) dp on dp.model_id = m.model_id 
group by 1