insert into nexus.test_b54befe7d2d30f51a0a2c182180de27dbe18e09f6363913cbd8d8ef780bb25b0_5134a6516839a19b38c2c2dd61b2dc12 with seller as (
select 
shop_id,
user_id as userid,
user_name as shop_name,
gp_smt as gp_smt_pic
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
)
and shop_id not in (
296109316,
295478771,
297793811
)
),
raw_ls_info as (
select 
distinct 
a.grass_region, 
date(split(a.start_time,' ')[1]) as grass_date, 
a.ls_session_id,
a.title as ls_session_name,
a.streamer_id, 
a.streamer_name, 
a.start_time,
a.end_time, 
(cast(a.ls_session_duration as double)/1000)/60 as ls_session_duration_minutes, 
a.max_member_cnt as session_PCU,
a.title, 
a.like_cnt as likes, 
b.streamer_shop_category,
case when date(cast(a.start_time as TIMESTAMP)) = b.streamer_first_streaming_date then '1' else '0' end as is_new_streamer,
'KRCB' as seller_type,
s.shop_name,
s.shop_id
from 
livestream.ls_mart_dim_ls_session as a 
join 
livestream.ls_mart_dim_streamer as b 
on 
a.grass_region= b.grass_region 
and a.streamer_id = b.streamer_id
join
seller as s
on
s.userid = a.streamer_id
and a.grass_date = date_add('day',-1,current_date)
and a.grass_date = b.grass_date
and a.ls_session_duration >0
where
a.tz_type = 'local'
and b.tz_type = 'local'
),
ls_info as (
select
distinct 
b.shop_name,
b.seller_type,
b.shop_id,
b.ls_session_name,
a.grass_region, 
b.grass_date,
a.ls_session_id,
b.streamer_id, 
b.streamer_name,
b.streamer_shop_category,
b.is_new_streamer,
b.start_time,
b.end_time, 
b.session_PCU,
a.viewer_total_view_1d as total_views, 
a.viewer_unique_uu_1d as unique_views,
a.viewer_new_ls_cnt_1d as new_viewer, 
b.ls_session_duration_minutes,
a.viewer_average_watch_time_1d/60 as avg_watch_time_minutes, 
a.viewer_total_watch_time_1d/60 as total_watch_time_minutes,
a.viewer_fe_total_like_1d as total_likes, 
a.viewer_total_comment_1d as total_comments,
a.viewer_total_share_1d as total_shares, 
a.streamer_fe_new_follower_1d as new_shop_follows,
a.viewer_total_atc_1d as atc_cnt
from 
livestream.ls_mart_dws_ls_session_userid_streaming_1d as a 
join 
raw_ls_info as b 
on 
a.grass_region = b.grass_region 
and a.ls_session_id = b.ls_session_id
where 
a.grass_date = date_add('day',-1,current_date)
and a.tz_type = 'local'
),
ls_orders as (
SELECT
o.grass_region,
o.ls_session_id,
o.streamer_id,
max(o.place_order_direct_1d) as direct_orders,
max(o.place_order_direct_gmv_usd_1d) as direct_gmv_usd,
max(o.place_order_direct_units_sold_1d) as direct_item_sold_qty
FROM 
livestream.ls_mart_dws_ls_session_order_nd as o
join
seller as s
on
o.streamer_id = s.userid
WHERE 
tz_type = 'local'
group by
1,2,3
-- haven't updated the data before June.28 into the latest partition.
),
basket_item_clicks as (
SELECT 
grass_region,
ls_session_id,
-- basket_click_total_1d, --Count of click of the Basket
basket_display_item_click_1d, --Count of total click of display item
basket_inside_item_click_total_1d --Count of total click of the items inside Basket
FROM 
livestream.ls_mart_dws_ls_session_basket_item_1d
WHERE 
tz_type = 'local'
AND grass_date = date_add('day',-1,current_date)
group by
1,2,3,4
)
select
i.grass_date,
i.grass_region,
i.seller_type,
i.shop_name,
i.shop_id,
i.streamer_name,
i.streamer_id,
i.ls_session_id,
i.ls_session_name,
i.start_time,
i.end_time,
i.ls_session_duration_minutes,
i.avg_watch_time_minutes,
i.total_views,
i.unique_views,
i.session_PCU,
i.total_likes,
i.total_comments,
i.total_shares,
i.new_shop_follows,
b.basket_display_item_click_1d,
b.basket_inside_item_click_total_1d,
i.atc_cnt,
o.direct_orders,
o.direct_gmv_usd
-- i.streamer_shop_category,
-- i.is_new_streamer,
-- i.new_viewer,
-- i.total_watch_time_minutes,
from
ls_info as i
left join
ls_orders as o
on
i.ls_session_id = o.ls_session_id
and i.grass_region = o.grass_region
and i.streamer_id = o.streamer_id
left join
basket_item_clicks as b
on
i.ls_session_id = b.ls_session_id
and i.grass_region = b.grass_region
where
i.grass_date between DATE_TRUNC('month', DATE_ADD('month', -2, current_date)) and date_add('day',-1,current_date)
and i.shop_id not in (
296089963,
295459418,
297774458
) --shopee korea 계정 제외
order by
1 desc,3,4,8