insert into nexus.test_1a22cc5a430875dd634c9c53ef04ba1e35dca34a4328e985b9b49478f2b8fe48_f74ff6f68c0f0fceb16c66c4c418cf27 -- #####################################
-- Update 2021/04/05: live cb whs shops = live shops who have live skus in CBWH
-- Update 2021/04/28: PFF shops NSS Tag, using intermediate table to implement it 
-- #####################################
WITH whs_sku_shops AS (
-- shops that have live skus in CBWH
SELECT
DISTINCT grass_region,
shopid,
1 AS live_whs_shop
FROM
sbs_mart.shopee_bd_sbs_mart_v2
WHERE
grass_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY) - INTERVAL '1' MONTH
AND date_trunc('month', current_date )- INTERVAL '1' DAY
AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','MX','VN')
AND fe_stock > 0
AND stock_on_hand > 0
AND sku_status_normal = 1
AND is_cb = 1
)
SELECT
DISTINCT shops.grass_region,
shops.warehouse_id,
shops.shop_id,
shops.username AS user_name,
u.user_id,
shops.pff_tag,
x.ggp_account_name,
x.gp_account_name,
x.gp_account_owner,
x.child_account_owner,
l.shop_level1_category,
s.is_official_shop,
/*FOR CHECK*/
-- shop_status, 
-- is_holiday_mode,
-- user_status,
-- last_login_datetime,
-- active_item_with_stock_cnt,
-- live_whs_shop,
/*CHECK END*/
CASE
/*shop level*/
WHEN shops.is_live_shop = 1
AND ws.live_whs_shop = 1 THEN 1
ELSE 0
END AS live_shop_tag,
start.start_as_whs_date,
shop_level1_global_be_category
FROM
(
SELECT
*
FROM
regcbbi_general.cbwh_shop_history_v2
WHERE
grass_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY) - INTERVAL '1' MONTH
AND date_trunc('month', current_date )- INTERVAL '1' DAY
AND warehouse_id in ('IDC', 'IDG', 'PHN', 'MYC', 'THA', 'SGD', 'SGL','MXO','CNN','VNS')
) AS shops
JOIN (
-- limit data size before join
SELECT
shop_id,
is_official_shop
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY) - INTERVAL '1' MONTH
AND date_trunc('month', current_date )- INTERVAL '1' DAY
AND tz_type = 'local'
AND is_cb_shop = 1
) AS s ON shops.shop_id = s.shop_id
JOIN (
-- limit data size before join
SELECT
shop_id,
user_id,
registration_datetime
FROM
mp_user.dim_user__reg_s0_live
WHERE
grass_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY) - INTERVAL '1' MONTH
AND date_trunc('month', current_date )- INTERVAL '1' DAY
AND tz_type = 'local'
) AS u ON shops.shop_id = u.shop_id
JOIN (
-- limit data size before join
SELECT
shop_id,
shop_level1_category,
shop_level1_global_be_category
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY) - INTERVAL '1' MONTH
AND date_trunc('month', current_date )- INTERVAL '1' DAY
AND tz_type = 'local'
) AS l ON shops.shop_id = l.shop_id
LEFT JOIN (
-- cannot combine with shops due to aggregation
-- earliest date when a shop become a FBS/WHS shop
SELECT
shop_id,
min(grass_date) AS start_as_whs_date
FROM
regcbbi_general.cbwh_shop_history_v2
GROUP BY
1
) START ON shops.shop_id = START.shop_id
LEFT JOIN whs_sku_shops ws ON shops.shop_id = ws.shopid
LEFT JOIN (
SELECT
child_shopid,
ggp_account_name,
gp_account_name,
gp_account_owner,
child_account_owner
FROM
regbd_sf.cb__seller_index_tab
WHERE
grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'MX','VN')
) x ON shops.shop_id = cast(x.child_shopid AS BIGINT)