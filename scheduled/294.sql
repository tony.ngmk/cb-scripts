insert into nexus.test_58e989b79ae61e85a5bfd26540f6665b11353dbb10fbbe2baa22e2fb3a8d29da_7b55f6242edc174f407c1ead8f53a350 WITH
dim AS ( 
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE ( (grass_date >= CURRENT_DATE-INTERVAL'90'DAY ))
) 


, raw AS (
SELECT DISTINCT
oversea_orderid
, "max"(local_orderid) local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
where ("date"("from_unixtime"(ctime)) >= CURRENT_DATE-INTERVAL'90'DAY ) 
GROUP BY 1
) 
, sip AS (
SELECT DISTINCT
SHOPID mst_shopid 
, COUNTRY AS MST_COUNTRY 
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE cb_option=0 AND COUNTRY IN ('TW','ID','MY','VN','TH')
) 




, net AS ( 
SELECT DISTINCT
shop_id
, case when raw.local_orderid is null then 'LOCAL(NS)'
else 'LOCAL SIP' end as shop_type
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(create_timestamp) )
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(create_timestamp) - INTERVAL'1'HOUR )
WHEN grass_region = 'BR' THEN date(from_unixtime(create_timestamp)-interval'11'hour)
WHEN grass_region = 'CL' THEN DATE(CAST(format_datetime(from_unixtime(create_timestamp) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN grass_region = 'CO' THEN DATE(CAST(format_datetime(from_unixtime(create_timestamp) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) end create_time
, "count"(DISTINCT order_id) gross_order
, "count"(DISTINCT (CASE WHEN (order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15)) THEN order_id ELSE null END)) net_order
-- 
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live o 
join sip on sip.mst_shopid=o.shop_id 
left join raw on raw.local_orderid= o.order_id
WHERE ( (date(from_unixtime(create_timestamp) ) >=CURRENT_DATE-INTERVAL'90'DAY)) and tz_type='local' and o.is_cb_shop=0 and o.grass_region in ('TW','ID','MY','VN','TH')
AND grass_date>= (current_date - INTERVAL '90' DAY)
-- and raw.local_orderid is null
GROUP BY 1, 2,3 
) 
, cancel AS (
SELECT DISTINCT
shopid
, case when raw.local_orderid is null then 'LOCAL(NS)'
else 'LOCAL SIP' end as shop_type
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(cancel_time)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(cancel_time) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(cancel_time)-interval'11'hour)
WHEN (grass_region = 'CL') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'CO') THEN DATE(CAST(format_datetime(from_unixtime(cancel_time) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) end cancel_time
, "count"(DISTINCT (CASE WHEN ((grass_region = 'TW') AND (extinfo.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303))) THEN orderid 
WHEN ((grass_region = 'BR') AND (extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,801,303))) THEN orderid 
WHEN ((grass_region NOT IN ('TW','BR')) AND (extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) THEN orderid 
ELSE null END)) cancel_order
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live o 
join sip on sip.mst_shopid=o.shopid 
left join raw on raw.local_orderid= o.orderid
WHERE ( ("date"("from_unixtime"(cancel_time)) >= (current_date - INTERVAL '90' DAY))) and o.cb_option=0 and o.grass_region in ('TW','ID','MY','VN','TH')
-- and raw.local_orderid is null 
GROUP BY 1, 2,3 
) 
, rr AS (
SELECT DISTINCT
shopid
, case when raw.local_orderid is null then 'LOCAL(NS)'
else 'LOCAL SIP' end as shop_type
, case when grass_region in ('SG','MY','PH','TW') then date(from_unixtime(mtime)) 
WHEN grass_region IN ('TH','ID','VN') THEN date(from_unixtime(mtime) - INTERVAL'1'HOUR)
WHEN grass_region = 'BR' THEN date(from_unixtime(mtime)-interval'11'hour)
WHEN (grass_region = 'CL') THEN DATE(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Santiago','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) 
WHEN (grass_region = 'CO') THEN DATE(CAST(format_datetime(from_unixtime(mtime) AT TIME ZONE 'America/Bogota','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)) end mtime
, "count"(DISTINCT (CASE WHEN ((((grass_region = 'TW') AND (reason IN (1, 2, 3, 103, 105))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5))) THEN a.orderid WHEN ((((grass_region <> 'TW') AND (reason IN (1, 2, 3, 103, 105, 107))) AND ((cancel_reason = 0) OR (cancel_reason IS NULL))) AND (status IN (2, 5))) THEN a.orderid ELSE null END)) RR_order
FROM
(marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
where cb_option=0 and grass_region in ('TW','ID','MY','VN','TH') 
AND ( ("date"("from_unixtime"(CREATE_TIME)) >=(current_date - INTERVAL '90' DAY)))
) b ON (a.orderid = b.orderid))
join sip on sip.mst_shopid=a.shopid 
left join raw on raw.local_orderid= a.orderid
WHERE ( ("date"("from_unixtime"(mtime)) >= (current_date - INTERVAL '90' DAY))) and a.cb_option=0 and a.grass_region in ('TW','ID','MY','VN','TH')
-- and raw.local_orderid is null 
GROUP BY 1, 2,3 
) 
SELECT DISTINCT
dim.grass_date
, sip.mst_country 


, "sum"(gross_order) gross_order
, "sum"(net_order) net_oorder
, "sum"(cancel_order) cancel_order
, "sum"(RR_order) RR_order
, net.shop_type 
FROM
((dim
INNER JOIN sip ON (dim.grass_region = sip.mst_country ))
LEFT JOIN net ON ((sip.mst_shopid = net.shop_id) AND (dim.grass_date = net.create_time)))
LEFT JOIN cancel ON ((sip.mst_shopid = cancel.shopid) AND (dim.grass_date = cancel.cancel_time)) and cancel.shop_type = net.shop_type 
LEFT JOIN rr ON ((sip.mst_shopid = rr.shopid) AND (dim.grass_date = rr.mtime)) and net.shop_type= rr.shop_type 


GROUP BY 1, 2,7
having sum(gross_order) > 0