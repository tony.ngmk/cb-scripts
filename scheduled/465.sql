insert into nexus.test_e4ee35d58c41560aaf5179dcc98985749804ef48229eeedbf7376966ef3e15ac_c0fc7977cdb6216c0643c51dfabfaa2d WITH
sip AS (
SELECT DISTINCT
affi_shopid
, mst_shopid
, t1.country mst_country
, t2.country affi_country
, t1.cb_option
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE ((sip_shop_status <> 3) AND (grass_region IN ('SG','MY','BR','MX','PH','TH','VN','CO','CL') ) )
) t2 ON (t1.shopid = t2.mst_shopid))
WHERE ((T1.CB_OPTION = 0) AND (T1.COUNTRY IN ('TW', 'ID', 'VN', 'MY','TH')))
) 
SELECT DISTINCT
mst_country
, affi_country
, cb_option
, seller_status
, is_holiday_mode
, level1_global_be_category
, status
, stock
, "count"(DISTINCT i.item_id) active_item_with_stock_cnt
, ("sum"(order_fraction) / DECIMAL '30.00') orders
, ("sum"(gmv_usd) / DECIMAL '30.00') gmv_usd
FROM
(((sip
INNER JOIN (
SELECT DISTINCT
item_id
, (CASE WHEN (seller_status <> 1) THEN 0 ELSE 1 END) seller_status
, shop_status
, is_holiday_mode
, shop_id
, (CASE WHEN (stock > 5) THEN 2 WHEN (stock > 0) THEN 1 ELSE 0 END) stock
, status
, level1_global_be_category
FROM
mp_item.dim_item__reg_s0_live
WHERE (((grass_date = (current_date - INTERVAL '1' DAY)) AND (is_cb_shop = 1)) AND (shop_status = 1))
and tz_type = 'local'
AND GRASS_REGION IN ('SG','MY','BR','MX','PH','TH','VN','CO','CL')
) i ON (i.shop_id = sip.affi_shopid))
INNER JOIN (
SELECT DISTINCT
item_id affi_itemid
, primary_item_id mst_itemid
FROM
mp_item.ads_sip_affi_item_profile__reg_s0_live
where grass_region in ('SG','MY','BR','MX','PH','TH','VN','CO','CL') 
and grass_date =current_date- interval'2'day 
and primary_shop_type=0 and shop_status = 1 and primary_shop_status = 1 and primary_seller_status = 1 and primary_item_status = 1 and primary_item_stock > 0 
and primary_seller_region in ('TW', 'ID', 'VN', 'MY','TH')
and tz_type = 'local'

) a ON (a.affi_itemid = i.item_id))
join (select distinct item_id 
from regcbbi_others.mkt_top_sku_overall
where (case when grass_region in ('BR','CO','CL','MX') AND cast(grass_date as date) = current_date-interval'2'day THEN 1 
WHEN GRASS_REGION IN ('SG','MY','ID','PH','TH','VN','TW') AND cast(grass_date as date) = current_date-interval'1'day THEN 1 
ELSE 0 END )=1
and grass_region in ('SG','MY','BR','MX','PH','TH','VN','CO','CL') 
and l30d_ranking<=1000 AND CAST(GRASS_DATE AS DATE) BETWEEN CURRENT_DATE-INTERVAL'2'DAY AND CURRENT_DATE-INTERVAL'1'DAY )tt on tt.item_id = a.affi_itemid 
LEFT JOIN (
SELECT DISTINCT
item_id
, placed_order_fraction_30d order_fraction
, gmv_usd_30d gmv_usd
FROM
mp_order.dws_item_gmv_nd__reg_s0_live
WHERE ( (grass_region in ('SG','MY','TW','BR','MX','PH','TH','VN','CO','CL') )) and tz_type='local' and grass_date = current_date-interval'2'day 
) o ON (o.item_id = i.item_id))
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8