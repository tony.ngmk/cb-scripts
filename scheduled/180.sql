insert into nexus.test_535636ef6a03196a6ba852a2b5071016a149e7254decf4bfd4df56bcfeec9fec_a190ec5d9a11d60af7b12503ec80f3a8 WITH
sip AS (
SELECT DISTINCT
a.mst_shopid
, a.mst_itemid
, a.affi_shopid
, a.affi_itemid
, b.country mst_country
, affi_country
FROM
((marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
, cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) b ON (a.mst_shopid = b.shopid))
INNER JOIN (
SELECT DISTINCT affi_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE ((sip_shop_status <> 3) AND (grass_region <> ''))
) c ON (c.affi_shopid = a.affi_shopid))
WHERE (((a.affi_country IN ('PH')) AND (b.cb_option = 0)) AND (b.country = 'ID'))
) 
, item AS (
SELECT
shop_id shopid
, item_id itemid
, level1_global_be_category main_category
, level2_global_be_category_id sub_category
, "from_unixtime"(create_timestamp) item_ctime
, name affi_item_name
FROM
(mp_item.dim_item__reg_s0_live i
LEFT JOIN (
SELECT itemid
FROM
marketplace.shopee_listing_qc_backend_ph_db__screening_qclog__reg_daily_s0_live
WHERE ((status IN (2, 4, 5)) AND (classification = 0))
) qc ON (i.item_id = qc.itemid))
WHERE (((((((((grass_date = (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local')) AND (is_cb_shop = 1)) AND (grass_region = 'PH')) AND (seller_status = 1)) AND (shop_status = 1)) AND (status = 1)) AND (stock > 0)) AND ("date"("from_unixtime"(create_timestamp)) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '24' DAY)))
) 
, orders AS (
SELECT
item_id itemid
, "sum"(order_fraction) orders
FROM
(mp_order.dwd_order_item_all_ent_df__reg_s0_live o
INNER JOIN sip ON (o.item_id = sip.affi_itemid))
WHERE (((((grass_region = 'PH') AND ("date"("from_unixtime"(create_timestamp)) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) AND (grass_date >= (current_date - INTERVAL '30' DAY))) AND (tz_type = 'local')) AND (is_cb_shop = 1))
GROUP BY 1
) 
, views AS (
SELECT
item_id itemid
, pv_cnt_30d views
FROM
(mp_shopflow.dws_item_civ_order_nd__reg_s0_live v
INNER JOIN sip ON (v.item_id = sip.affi_itemid))
WHERE (((grass_region = 'PH') AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (tz_type = 'local'))
) 
, ex AS (
SELECT DISTINCT
auditid audit_id
, itemid
, shopid
, country
, name upload_option
, new upload_content
, TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Operator')) operator
, TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Source')) source
, CAST(grass_date AS date) ctime
FROM
(marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
CROSS JOIN UNNEST(CAST(TRY("json_extract"(TRY("from_utf8"(data)), '$.Fields')) AS array(row(name varchar,old varchar,new varchar)))))
WHERE (((((TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Operator')) IN ('zhixian.toh@shopee.com')) OR (TRY("json_extract_scalar"(TRY("from_utf8"(data)), '$.Source')) = 'Seller Center Single')) AND (name IN ('name'))) AND (country = 'PH')) AND (CAST(grass_date AS date) BETWEEN (current_date - INTERVAL '120' DAY) AND (current_date - INTERVAL '1' DAY)))
) 
, focus AS (
SELECT *
FROM
(
SELECT
*
, "rank"() OVER (PARTITION BY main_category ORDER BY orders DESC, views DESC) ranking
FROM
(
SELECT
mst_shopid
, sip.affi_shopid
, mst_itemid
, i.itemid
, i.main_category
, i.sub_category
, affi_item_name
, affi_country
, "concat"('http://shopee.co.id/product/', CAST(mst_shopid AS varchar), '/', CAST(mst_itemid AS varchar)) mst_product_link
, COALESCE(o.orders, 0) orders
, COALESCE(v.views, 0) views
FROM
(((((item i
INNER JOIN sip ON (i.itemid = sip.affi_itemid))
LEFT JOIN ex ON (ex.itemid = i.itemid))
LEFT JOIN regcbbi_others.sip__listing_optimization_bau_exclude_list__wf__reg__s0 ex1 ON (CAST(ex1.affi_itemid AS bigint) = i.itemid))
LEFT JOIN orders o ON (sip.affi_itemid = o.itemid))
LEFT JOIN views v ON (sip.affi_itemid = v.itemid))
WHERE ((ex.itemid IS NULL) AND (ex1.affi_itemid IS NULL))
) 
) 
WHERE (ranking <= 24000)
) 
, mst AS (
SELECT
item_id
, name mst_item_name
FROM
(mp_item.dim_item__reg_s0_live i
INNER JOIN sip ON (i.item_id = sip.mst_itemid))
WHERE ((((grass_date = (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local')) AND (is_cb_shop = 0)) AND (grass_region IN ('ID')))
) 
SELECT *
FROM
(
SELECT
f.affi_country
, f.affi_shopid
, f.itemid affi_itemid
, "concat"('https://shopee.ph/product/', CAST(f.affi_shopid AS varchar), '/', CAST(f.itemid AS varchar)) affi_product_link
, affi_item_name
, '' affi_description
, mst_item_name
, mst_product_link
, '' Priority
, '' Human_translated_title_local_language
, '' Keyword
, '' Human_translated_description_EN
, '' Machine_translation_score_Title
, '' Machine_translation_score_Description
, '' Error_type
, '' Error_1_3_Fill_correct_EN
, '' Error_2_3_Fill_correct_local_language
, '' Error_3_Fill_wrong_local_language
, '' Error_4_Remark_for_correct_sequence
, '' Title_length_Y_N
, '' Description_length_Y_N
, '' Confirm_to_upload_Y_N
, '' Remark_for_N_reason
, '' Other_remarks
, sub_category sub_cat
FROM
(focus f
LEFT JOIN mst ON (mst.item_id = f.mst_itemid))
) 
LIMIT 1000