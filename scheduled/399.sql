insert into nexus.test_844c2c1b681284301a3d234393ba8f8618e8e67f73c7e9f31ad7250ca1bd0a78_253f7be481c11e3bc6cfc60a54f28613 SELECT orders.country
, orders.Purchase_date
, orders.cncb_orders
, orders.cncb_gmv
, orders.Country_Orders_excl
, orders.Country_GMV_excl
, sku.cncb_live AS CNCB_New_SKU
, sku.cncb_total_sku AS CNCB_Total_SKU
, sku.country_total_sku AS Country_Total_SKU
FROM
(SELECT o.grass_region as country
, o.Purchase_date
, count(case when o.cb_option = 1 AND merchant_region in ('CN','HK') AND is_bi_excluded = 0 THEN o.orderid ELSE null END) CNCB_Orders
, sum (case when o.cb_option = 1 AND merchant_region in ('CN','HK') AND is_bi_excluded = 0 THEN gmv_usd ELSE null END) CNCB_GMV
, sum (case when o.cb_option = 1 AND merchant_region in ('CN','HK') AND is_bi_excluded = 0 THEN amount ELSE null END) CNCB_Item_Sold
, count(case when o.cb_option = 1 AND is_bi_excluded = 0 THEN o.orderid ELSE null END) CB_Orders
, sum (case when o.cb_option = 1 AND is_bi_excluded = 0 THEN gmv_usd ELSE null END) CB_GMV
, sum (case when o.cb_option = 1 AND is_bi_excluded = 0 THEN amount ELSE null END) CB_Item_Sold
, count(CASE WHEN o.cb_option = 0 AND is_bi_excluded = 0 THEN o.orderid ELSE null END) Local_Orders
, sum (CASE WHEN o.cb_option = 0 AND is_bi_excluded = 0 THEN o.gmv_usd ELSE null END) Local_GMV
, sum (CASE WHEN o.cb_option = 0 AND is_bi_excluded = 0 THEN amount ELSE null END) Local_Item_Sold
, count(o.orderid) Country_Orders
, sum (o.gmv_usd) Country_GMV
, sum (amount) Country_Item_Sold
, count(CASE WHEN is_bi_excluded = 0 THEN o.orderid ELSE NULL END) Country_Orders_excl
, sum (CASE WHEN is_bi_excluded = 0 THEN o.gmv_usd ELSE NULL END) Country_GMV_excl
-- , sum (CASE WHEN is_bi_excluded = 0 THEN amount ELSE NULL END) Country_Item_Sold 
FROM
(
select order_id as orderid
, grass_region
, shop_id as shopid 
, date(from_unixtime(create_timestamp)) as Purchase_date
, is_cb_shop as cb_option
, is_bi_excluded
, sum(gmv_usd) as gmv_usd
, sum(item_amount) as amount 
from mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
where tz_type = 'local' 
and grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
and is_placed = 1 
and date(from_unixtime(create_timestamp)) BETWEEN date_add('day', -30, date(from_unixtime(1655798400))) AND date(from_unixtime(1655798400))
group by 1, 2, 3, 4, 5, 6
) o
LEFT JOIN 
(select distinct shop_id,merchant_region,grass_region from dev_regcbbi_general.cb_seller_profile_reg where grass_region!='') x ON o.shopid = x.shop_id
group by 1, 2 
) orders
LEFT JOIN 
(
SELECT
country
, create_time
, cb_live
, cncb_live
, country_live
, sum(cb_live) OVER (PARTITION BY country ORDER BY create_time ASC) cb_total_sku
, sum(cncb_live) OVER (PARTITION BY country ORDER BY create_time ASC) cncb_total_sku 
, sum(country_live) OVER (PARTITION BY country ORDER BY create_time ASC) country_total_sku
FROM
(
SELECT
i.grass_region country
, date(from_unixtime(i.ctime)) create_time
, count(DISTINCT (CASE WHEN u.is_cb_shop = 1 THEN itemid ELSE null END)) cb_live 
, count(DISTINCT (CASE WHEN u.is_cb_shop = 1 AND x.merchant_region in ('CN','HK') THEN itemid ELSE null END)) cncb_live
, count(DISTINCT itemid) country_live
FROM
(
SELECT grass_region
, create_timestamp as ctime
, shop_id as shopid
, item_id as itemid
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
and status = 1
and stock > 0
and tz_type = 'local'
and grass_date = date(from_unixtime(1655798400))
) i
JOIN 
(
SELECT grass_region, shop_id, is_cb_shop
FROM mp_user.dim_shop__reg_s0_live 
WHERE grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
and grass_date = date(from_unixtime(1655798400))
and tz_type = 'local'
and status = 1
and (is_holiday_mode = 0 or is_holiday_mode is null)
) u ON i.shopid = u.shop_id
JOIN 
(
SELECT grass_region, shop_id, user_id
FROM mp_user.dim_user__reg_s0_live 
WHERE grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
and grass_date = date(from_unixtime(1655798400))
and tz_type = 'local'
and status = 1
--and date(from_unixtime(last_login_timestamp)) >= date_add('day', -7, current_date)
) u1 ON u.shop_id = u1.shop_id
JOIN 
(
select distinct shop_id, DATE(from_unixtime(last_login_timestamp_td)) AS last_login
from mp_user.dws_user_login_td_account_info__reg_s0_live
where tz_type = 'local' and grass_date = date(from_unixtime(1655798400))
) l on u1.shop_id = l.shop_id
LEFT JOIN 
(
SELECT shop_id, merchant_region
FROM dev_regcbbi_general.cb_seller_profile_reg
WHERE grass_region IN ('SG', 'MY', 'ID', 'TW', 'PH', 'TH', 'BR', 'VN', 'MX', 'CO', 'CL', 'PL', 'ES', 'FR')
) x ON (u.shop_id = x.shop_id)
WHERE l.last_login >= date_add('day', -6, date(from_unixtime(1655798400)))
GROUP BY 1, 2
) 
) sku ON orders.purchase_date = sku.create_time AND orders.country = sku.country
ORDER BY orders.purchase_date DESC, orders.country DESC