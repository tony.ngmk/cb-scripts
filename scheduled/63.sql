-- shop level stats
WITH kr_seller AS (
SELECT u1.grass_region
, u1.shop_id as shopid
, u1.user_id as userid
, u1.user_name as username
, u4.gross_sold_orders
, u2.main_category
, u1.response_rate
, u1.rating_star
, u1.follower_count
, u2.sku
, u1.is_holiday_mode as holiday_mode_on
--, DATE(split(u1.last_login_datetime, ' ')[1]) AS last_login_date
, u1.shop_status
FROM (
SELECT distinct a.shop_id
, a.user_id
, a.user_name
, a.response_rate
, a.rating_star
, a.follower_count
, a.is_holiday_mode
--, u.last_login_datetime
, a.shop_status
, CASE WHEN sip.shopid is null or sip.country in ('MY', 'PH', 'VN') then coalesce(a.grass_region, sip.country) else 'SIP' end as grass_region
FROM (select grass_region, shop_id, user_id, user_name, response_rate, rating_star, follower_count, is_holiday_mode, shop_status, 'Korea' as gp_account_billing_country
from dev_regcbbi_kr.krcb_shop_profile
where grass_date in (
select max(grass_date) as latest_ingest_date 
from dev_regcbbi_kr.krcb_shop_profile
where grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
)
and grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
) AS a
LEFT JOIN (
SELECT affi_shopid as shopid
, affi_userid as userid
, affi_username as username
, country
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live AS a
JOIN (select distinct shop_id
from dev_regcbbi_kr.krcb_shop_profile
where grass_date in (
select max(grass_date) as latest_ingest_date 
from dev_regcbbi_kr.krcb_shop_profile
where grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
)
and grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
) AS cb ON a.mst_shopid = cb.shop_id
WHERE a.grass_region IN ('PL','MX', 'BR', 'ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
) AS sip ON a.shop_id = sip.shopid
WHERE (gp_account_billing_country = 'Korea' OR sip.shopid is not null) 
) u1
LEFT JOIN (
SELECT shop_id
, shop_level1_category as main_category
, active_item_cnt as sku
FROM mp_item.dws_shop_listing_td__reg_s0_live
WHERE grass_date = date(from_unixtime(1655859600))
AND tz_type = 'local'
) AS u2 ON u1.shop_id = u2.shop_id
LEFT JOIN (
SELECT shop_id
, placed_order_cnt_td as gross_sold_orders
FROM mp_order.dws_seller_gmv_td__reg_s0_live
WHERE grass_region IN ('PL','MX', 'BR', 'ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
AND grass_date = if(grass_region IN ('PL','BR', 'MX'), date_add('day', -1, date(from_unixtime(1655859600))), date(from_unixtime(1655859600)))
AND tz_type = 'local'
) AS u4 ON u1.shop_id = u4.shop_id
)




, net_orders as (
SELECT oi.shop_id as shopid
, "count"(DISTINCT oi.order_id) as net_sold_orders 
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live AS oi 
JOIN (SELECT distinct shopid FROM kr_seller) AS kr ON oi.shop_id = kr.shopid
WHERE oi.tz_type = 'local'
AND oi.grass_region IN ('PL','MX', 'BR', 'ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN') 
AND oi.is_bi_excluded = 0 
AND oi.is_net_order = 1
GROUP BY 1
)


SELECT a.userid
, a.shopid
, a.grass_region country
, a.username username_1
, COALESCE(d.daily_gross_orders, 0) daily_gross_orders
, '' as organic_orders -- COALESCE(d.daily_gross_orders-f.campaign_orders-f.cfs_orders-f.inshop_fs_orders, 0)
, '' as campaign_orders -- COALESCE(f.campaign_orders, 0) 
, COALESCE(d.cfs_orders, 0) flash_sale_orders
, COALESCE(d.inshop_fs_orders,0) inshop_fs_orders
, COALESCE(d.sls_orders, 0) sls_orders
, COALESCE(ads_orders, 0) ads_orders
, COALESCE(d.daily_gross_item_sold, 0) daily_gross_item_sold
, COALESCE(d.daily_gross_gmv, 0) daily_gross_gmv
, COALESCE(TRY((d.daily_gross_gmv / d.daily_gross_orders)), 0) daily_abs
, COALESCE(TRY((d.buyer_paid_shipping_fee / d.daily_gross_orders)), 0) buyer_paid_shipping_fee
, COALESCE(TRY((d.shipping_fee / d.daily_gross_orders)), 0) avg_shipping_cost
, a.username username_2
, COALESCE(views.views, 0) views
, COALESCE(TRY((d.daily_gross_orders / CAST(views.views AS double))), 0) convertion_rate
, net_sold_orders -- column deprecated as of 27 nov 2020 from user mart table
, gross_sold_orders
, COALESCE(TRY((net_sold_orders / CAST(gross_sold_orders AS double))), 0) as order_validation_rate
, main_category
, a.response_rate
, COALESCE(a.rating_star, 0) rating_star
, sku
, follower_count
FROM kr_seller AS a
LEFT JOIN (
SELECT oi.shop_id as shopid
, oi.grass_region
, "count"(DISTINCT oi.order_id) daily_gross_orders
, "sum"(oi.item_amount) daily_gross_item_sold
, "sum"(oi.gmv_usd) daily_gross_gmv
, "sum"(oi.estimate_shipping_fee_usd) shipping_fee
, "sum"(oi.buyer_paid_shipping_fee_usd) buyer_paid_shipping_fee
--, "count"(DISTINCT CASE WHEN (sls.channelid is not null OR oi.shipping_channel_id = 58009) THEN oi.order_id ELSE null END) sls_orders
, "count"(DISTINCT case when oi.shipping_channel_id in (18036, 28050, 58009, 48005, 88021, 98002, 78016, 108004, 108005, 38015, 38016, 38026) then 1 else 0 end) sls_orders
, "sum"(CASE WHEN ((oi.item_promotion_source = 'flash_sale') AND (oi.flash_sale_type_id !=2)) then order_fraction else 0 end) as cfs_orders 
, "sum"(CASE WHEN ((oi.item_promotion_source = 'flash_sale') AND (oi.flash_sale_type_id =2)) then order_fraction else 0 end) as inshop_fs_orders
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live AS oi 
/*LEFT JOIN (
SELECT *
FROM shopee.shopee_regional_cb_team__sls_channelid
WHERE ingestion_timestamp = (SELECT max(ingestion_timestamp) AS ingestion_timestamp FROM shopee.shopee_regional_cb_team__sls_channelid WHERE CAST(ingestion_timestamp AS BIGINT) > 0)
) AS sls ON oi.shipping_channel_id = cast(sls.channelid as bigint)*/
JOIN (SELECT distinct shopid FROM kr_seller) AS kr ON oi.shop_id = kr.shopid
WHERE oi.tz_type = 'local'
AND oi.grass_region IN ('PL','MX', 'BR', 'ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN') 
AND oi.grass_date >= date_add('day', -2, current_date)
AND if(oi.grass_region IN ('PL','BR', 'MX'), date(from_unixtime(oi.create_timestamp)), DATE(split(oi.create_datetime, ' ')[1])) = date_add('day', -1, current_date)
GROUP BY 1, 2
) AS d ON a.shopid = d.shopid
LEFT JOIN net_orders AS n ON a.shopid = n.shopid
LEFT JOIN (
SELECT t.shop_id AS shopid
, "sum"(t.pv_cnt_1d) AS views
FROM mp_shopflow.dws_item_civ_1d__reg_s0_live AS t
JOIN (SELECT distinct shopid FROM kr_seller) AS kr ON t.shop_id = kr.shopid
WHERE t.tz_type = 'local'
AND grass_region IN ('PL','MX', 'BR', 'ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
AND t.grass_date = date_add('day', -1, current_date)
GROUP BY t.shop_id
) AS views ON a.shopid = views.shopid
LEFT JOIN (
SELECT a.shop_id
, "sum"(order_cnt_1d) ads_orders
FROM mp_paidads.dws_advertise_performance_1d__reg_s0_live a
JOIN (SELECT distinct shopid FROM kr_seller) AS kr ON a.shop_id = kr.shopid
WHERE tz_type = 'local'
AND grass_region IN ('PL','MX', 'BR', 'ID', 'MY', 'PH', 'SG', 'TH', 'TW', 'VN')
AND grass_date = date_add('day', -1, current_date)
GROUP BY 1
) AS ads ON a.shopid = ads.shop_id
ORDER BY a.grass_region, a.shopid