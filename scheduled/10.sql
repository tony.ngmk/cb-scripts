WITH map
AS (SELECT DISTINCT feature_group,
feature,
grass_date AS dt
FROM mp_foa.ads_feature_lead_1d__reg_s0_livea
WHERE grass_region = 'PH'
AND grass_date BETWEEN Date_add('month', -1, Date_trunc('month',
Date_add('day', -1,
current_date)))
AND Date_add('day', -1, current_date)
AND ( feature_group = 'Others'
OR feature != 'Others' )
AND impress_cnt_item_direct_lead_1d > 0),
cb_item
AS (SELECT DISTINCT item_id,
is_cb_shop
FROM (SELECT item_id,
is_cb_shop,
level1_category
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region = 'PH'
AND grass_date = Date_add('day', -1, current_date)
AND tz_type = 'local') a),
imp
AS (SELECT Date_add('day', -1, current_date)
AS
grass_date,
feature_group,
SUM(CASE
WHEN a.grass_date = Date_add('day', -1, current_date)
AND is_cb_shop = 1 THEN impress_cnt_direct_lead
END)
AS
cb_impress_cnt_direct_lead,
SUM(CASE
WHEN a.grass_date = Date_add('day', -1, current_date)
AND is_cb_shop = 0 THEN impress_cnt_direct_lead
END)
AS
lc_impress_cnt_direct_lead,
SUM(CASE
WHEN a.grass_date >= Date_trunc('week',
Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN impress_cnt_direct_lead
END) * 1.0000 / Day_of_week("Date_add"('day', -1,
current_date))
AS
cb_impress_wtd,
SUM(CASE
WHEN a.grass_date >= Date_trunc('week',
Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN impress_cnt_direct_lead
END) * 1.0000 / Day_of_week("Date_add"('day', -1,
current_date))
AS
lc_impress_wtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval'7'day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN impress_cnt_direct_lead
END) * 1.0000 / 7
AS
cb_impress_lw,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval'7'day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN impress_cnt_direct_lead
END) * 1.0000 / 7
AS
lc_impress_lw,
SUM(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN impress_cnt_direct_lead
END) * 1.0000 / Day("Date_add"('day', -1, current_date))
AS
cb_impress_mtd,
SUM(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN impress_cnt_direct_lead
END) * 1.0000 / Day("Date_add"('day', -1, current_date))
AS
lc_impress_mtd,
SUM(CASE
WHEN grass_date < Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN impress_cnt_direct_lead
END) * 1.0000 / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval'1'day)
AS
cb_impress_lm,
SUM(CASE
WHEN grass_date < Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN impress_cnt_direct_lead
END) * 1.0000 / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval'1'day)
AS
lc_impress_lm
FROM (SELECT grass_date,
step1_feature,
impress_cnt_direct_lead,
item_id
FROM mp_foa.dwd_eventid_impress_di__reg_s0_live
WHERE grass_region = 'PH'
AND grass_date BETWEEN Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
current_date)))
AND
Date_add('day', -1, current_date)
AND tz_type = 'local') a
join map p
ON p.feature = a.step1_feature
AND a.grass_date = p.dt
join cb_item b
ON a.item_id = b.item_id
GROUP BY 1,
2),
pdp_v
AS (SELECT Date_add('day', -1, current_date)
AS
grass_date,
feature_group,
SUM(CASE
WHEN grass_date = Date_add('day', -1, current_date)
AND is_cb_shop = 1 THEN pv_direct_lead
END)
AS
cb_pv_direct_lead,
SUM(CASE
WHEN grass_date = Date_add('day', -1, current_date)
AND is_cb_shop = 0 THEN pv_direct_lead
END)
AS
lc_pv_direct_lead,
SUM(CASE
WHEN grass_date >= Date_trunc('week', Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN pv_direct_lead
END) * 1.0000 / Day_of_week("Date_add"('day', -1,
current_date))
AS
cb_pv_wtd,
SUM(CASE
WHEN grass_date >= Date_trunc('week', Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN pv_direct_lead
END) * 1.0000 / Day_of_week("Date_add"('day', -1,
current_date))
AS
lc_pv_wtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval'7'day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN pv_direct_lead
END) * 1.0000 / 7
AS
cb_pv_lw,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval'7'day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN pv_direct_lead
END) * 1.0000 / 7
AS
lc_pv_lw,
SUM(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN pv_direct_lead
END) * 1.0000 / Day("Date_add"('day', -1, current_date))
AS
cb_pv_mtd,
SUM(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN pv_direct_lead
END) * 1.0000 / Day("Date_add"('day', -1, current_date))
AS
lc_pv_mtd,
SUM(CASE
WHEN grass_date < Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN pv_direct_lead
END) * 1.0000 / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval'1'day)
AS
cb_pv_lm,
SUM(CASE
WHEN grass_date < Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN pv_direct_lead
END) * 1.0000 / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval'1'day)
AS
lc_pv_lm
FROM (SELECT grass_date,
step1_feature,
pv_direct_lead,
item_id
FROM mp_foa.dwd_eventid_view_di__reg_s0_live a
WHERE grass_region = 'PH'
AND grass_date BETWEEN Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
current_date)))
AND
Date_add('day', -1, current_date)
AND tz_type = 'local'
AND page_type = 'product') a
join cb_item b
ON a.item_id = b.item_id
join map p
ON p.feature = a.step1_feature
AND a.grass_date = p.dt
GROUP BY 1,
2),
ord
AS (SELECT Date_add('day', -1, current_date)
AS
grass_date,
feature_group,
SUM(CASE
WHEN grass_date = Date_add('day', -1, current_date)
AND is_cb_shop = 1 THEN place_order_cnt_direct_lead
END)
AS
cb_order_direct_lead,
SUM(CASE
WHEN grass_date = Date_add('day', -1, current_date)
AND is_cb_shop = 0 THEN place_order_cnt_direct_lead
END)
AS
lc_order_direct_lead,
SUM(CASE
WHEN grass_date >= Date_trunc('week', Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN place_order_cnt_direct_lead
END) * 1.0000 / Day_of_week("Date_add"('day', -1,
current_date))
AS
cb_order_wtd,
SUM(CASE
WHEN grass_date >= Date_trunc('week', Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN place_order_cnt_direct_lead
END) * 1.0000 / Day_of_week("Date_add"('day', -1,
current_date))
AS
lc_order_wtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval'7'day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN place_order_cnt_direct_lead
END) * 1.0000 / 7
AS
cb_order_lw,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval'7'day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN place_order_cnt_direct_lead
END) * 1.0000 / 7
AS
lc_order_lw,
SUM(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN place_order_cnt_direct_lead
END) * 1.0000 / Day("Date_add"('day', -1, current_date))
AS
cb_order_mtd,
SUM(CASE
WHEN grass_date >= Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN place_order_cnt_direct_lead
END) * 1.0000 / Day("Date_add"('day', -1, current_date))
AS
lc_order_mtd,
SUM(CASE
WHEN grass_date < Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 1 THEN place_order_cnt_direct_lead
END) * 1.0000 / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval'1'day)
AS
cb_order_lm,
SUM(CASE
WHEN grass_date < Date_trunc('month', Date_add('day', -1,
current_date))
AND is_cb_shop = 0 THEN place_order_cnt_direct_lead
END) * 1.0000 / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval'1'day)
AS
lc_order_lm
FROM (SELECT grass_date,
step1_feature,
place_order_cnt_direct_lead,
item_id
FROM mp_foa.dwd_eventid_item_order_events_di__reg_s0_live a
WHERE grass_region = 'PH'
AND grass_date BETWEEN Date_add('month', -1,
Date_trunc('month',
Date_add('day', -1,
current_date)))
AND
Date_add('day', -1, current_date)
) a
join cb_item b
ON a.item_id = b.item_id
join map p
ON p.feature = a.step1_feature
AND a.grass_date = p.dt
GROUP BY 1,
2) (SELECT p.grass_date,
p.feature_group
--, cb_impress_cnt_direct_lead,lc_impress_cnt_direct_lead, cb_pv_direct_lead, lc_pv_direct_lead, cb_order_direct_lead,lc_order_direct_lead
,
cb_impress_wtd,
lc_impress_wtd,
cb_pv_wtd,
lc_pv_wtd,
cb_order_wtd,
lc_order_wtd,
cb_pv_wtd * 1.0000 / cb_impress_wtd
AS cb_ctr,
( cb_pv_wtd * 1.0000 / cb_impress_wtd ) /
(
( lc_pv_wtd + cb_pv_wtd ) * 1.0000 / ( lc_impress_wtd + cb_impress_wtd )
)
AS
ctr_pene,
cb_order_wtd * 1.0000 / cb_pv_wtd
AS cb_cr,
( cb_order_wtd * 1.0000 / cb_pv_wtd ) / (
( lc_order_wtd + cb_order_wtd ) * 1.0000 / ( lc_pv_wtd + cb_pv_wtd ) )
AS cr_pene,
cb_impress_wtd * 1.0000 / SUM(cb_impress_wtd + lc_impress_wtd)
over (
PARTITION BY p.grass_date ) -
cb_impress_lw * 1.0000 / SUM(cb_impress_lw + lc_impress_lw)
over (
PARTITION BY p.grass_date)
AS cb_impress_pct_wow,
cb_impress_wtd * 1.0000 / ( lc_impress_wtd + cb_impress_wtd ) -
cb_impress_lw * 1.0000 / ( lc_impress_lw + cb_impress_lw )
AS
cb_impress_pene_wow,
( cb_pv_wtd * 1.0000 / cb_impress_wtd ) /
(
( lc_pv_wtd + cb_pv_wtd ) * 1.0000 / ( lc_impress_wtd + cb_impress_wtd )
)
- (
cb_pv_lw * 1.0000 / cb_impress_lw ) /
(
(
lc_pv_lw + cb_pv_lw ) * 1.0000 / ( lc_impress_lw
+ cb_impress_lw ) )
AS cb_ctr_pene_wow,
( cb_order_wtd * 1.0000 / cb_pv_wtd ) / (
( lc_order_wtd + cb_order_wtd ) * 1.0000 / ( lc_pv_wtd + cb_pv_wtd ) ) -
( cb_order_lw * 1.0000 / cb_pv_lw ) / (
( lc_order_lw + cb_order_lw ) * 1.0000 /
( lc_pv_lw + cb_pv_lw ) ) AS
cb_cr_pene_wow,
cb_order_wtd * 1.0000 / ( lc_order_wtd + cb_order_wtd ) -
cb_order_lw * 1.0000 / ( lc_order_lw + cb_order_lw )
AS cb_ord_pene_wow,
cb_impress_mtd * 1.0000 / SUM(cb_impress_mtd + lc_impress_mtd)
over (
PARTITION BY p.grass_date ) -
cb_impress_lm * 1.0000 / SUM(cb_impress_lm + lc_impress_lm)
over (
PARTITION BY p.grass_date)
AS cb_impress_pct_mom,
cb_impress_mtd * 1.0000 / ( lc_impress_mtd + cb_impress_mtd ) -
cb_impress_lm * 1.0000 / ( lc_impress_lm + cb_impress_lm )
AS
cb_impress_pene_mom,
( cb_pv_mtd * 1.0000 / cb_impress_mtd ) /
(
( lc_pv_mtd + cb_pv_mtd ) * 1.0000 / ( lc_impress_mtd + cb_impress_mtd )
)
- (
cb_pv_lm * 1.0000 / cb_impress_lm ) /
(
(
lc_pv_lm + cb_pv_lm ) * 1.0000 / ( lc_impress_lm
+ cb_impress_lm ) )
AS cb_ctr_pene_mom,
( cb_order_mtd * 1.0000 / cb_pv_mtd ) / (
( lc_order_mtd + cb_order_mtd ) * 1.0000 / ( lc_pv_mtd + cb_pv_mtd ) ) -
( cb_order_lm * 1.0000 / cb_pv_lm ) / (
( lc_order_lm + cb_order_lm ) * 1.0000 /
( lc_pv_lm + cb_pv_lm ) ) AS
cb_cr_pene_mom,
cb_order_mtd * 1.0000 / ( lc_order_mtd + cb_order_mtd ) -
cb_order_lm * 1.0000 / ( lc_order_lm + cb_order_lm )
AS cb_ord_pene_mom,
( cb_impress_lw + lc_impress_lw )
AS tl_impress_lw,
( cb_impress_mtd + lc_impress_mtd )
AS tl_impress_mtd,
( cb_impress_lm + lc_impress_lm )
AS tl_impress_lm,
cb_impress_mtd * 1.0000 / ( lc_impress_mtd + cb_impress_mtd )
AS
cb_impress_pene_mtd,
( cb_pv_mtd * 1.0000 / cb_impress_mtd ) /
(
( lc_pv_mtd + cb_pv_mtd ) * 1.0000 / ( lc_impress_mtd + cb_impress_mtd )
)
AS
cb_ctr_pene_mtd,
( cb_order_mtd * 1.0000 / cb_pv_mtd ) / (
( lc_order_mtd + cb_order_mtd ) * 1.0000 / ( lc_pv_mtd + cb_pv_mtd ) )
AS
cb_cr_pene_mtd,
cb_order_mtd * 1.0000 / ( lc_order_mtd + cb_order_mtd )
AS cb_ord_pene_mtd,
cb_impress_mtd * 1.0000 / SUM(cb_impress_mtd + lc_impress_mtd)
over (
PARTITION BY p.grass_date )
AS
cb_impress_mtd_pct_country,
( cb_pv_mtd * 1.0000 / cb_impress_mtd )
AS cb_ctr_mtd,
( cb_order_mtd * 1.0000 / cb_pv_mtd )
cb_cr_mtd,
cb_order_mtd,
lc_order_mtd
-- , p.cluster, cb_impress_mtd, (cb_pv_wtd+lc_pv_wtd)*1.0000/(lc_impress_wtd+lc_impress_wtd) as tl_ctr,(cb_order_wtd+lc_order_wtd)*1.0000/(cb_pv_wtd+lc_pv_wtd) as tl_cr
FROM imp p
left join pdp_v v
ON p.feature_group = v.feature_group --and p.cluster=v.cluster
left join ord o
ON p.feature_group = o.feature_group --and p.cluster=o.cluster 
)
--where cb_impress_cnt_direct_lead>0 
UNION ALL
(SELECT p.grass_date,
'All'
AS feature_group
-- , cb_impress_cnt_direct_lead,lc_impress_cnt_direct_lead, cb_pv_direct_lead, lc_pv_direct_lead, cb_order_direct_lead,lc_order_direct_lead
,
cb_impress_wtd,
lc_impress_wtd,
cb_pv_wtd,
lc_pv_wtd,
cb_order_wtd,
lc_order_wtd,
cb_pv_wtd * 1.0000 / cb_impress_wtd
AS cb_ctr,
( cb_pv_wtd * 1.0000 / cb_impress_wtd ) /
(
( lc_pv_wtd + cb_pv_wtd ) * 1.0000 / ( lc_impress_wtd + cb_impress_wtd )
)
AS
cb_ctr_pene,
cb_order_wtd * 1.0000 / cb_pv_wtd
AS cb_cr,
( cb_order_wtd * 1.0000 / cb_pv_wtd ) / (
( lc_order_wtd + cb_order_wtd ) * 1.0000 / ( lc_pv_wtd + cb_pv_wtd ) )
AS
cb_cr_pene,
cb_impress_wtd * 1.0000 / SUM(cb_impress_wtd + lc_impress_wtd)
over (
PARTITION BY p.grass_date ) -
cb_impress_lw * 1.0000 / SUM(cb_impress_lw + lc_impress_lw)
over (
PARTITION BY p.grass_date)
AS cb_impress_pct_wow,
cb_impress_wtd * 1.0000 / ( lc_impress_wtd + cb_impress_wtd ) -
cb_impress_lw * 1.0000 / ( lc_impress_lw + cb_impress_lw )
AS
cb_impress_pene_wow,
( cb_pv_wtd * 1.0000 / cb_impress_wtd ) /
(
( lc_pv_wtd + cb_pv_wtd ) * 1.0000 / ( lc_impress_wtd + cb_impress_wtd )
)
- (
cb_pv_lw * 1.0000 / cb_impress_lw ) /
(
(
lc_pv_lw + cb_pv_lw ) * 1.0000 / ( lc_impress_lw
+ cb_impress_lw ) )
AS cb_ctr_pene_wow,
( cb_order_wtd * 1.0000 / cb_pv_wtd ) / (
( lc_order_wtd + cb_order_wtd ) * 1.0000 / ( lc_pv_wtd + cb_pv_wtd ) ) -
( cb_order_lw * 1.0000 / cb_pv_lw ) / (
( lc_order_lw + cb_order_lw ) * 1.0000 /
( lc_pv_lw + cb_pv_lw ) ) AS
cb_cr_pene_wow,
cb_order_wtd * 1.0000 / ( lc_order_wtd + cb_order_wtd ) -
cb_order_lw * 1.0000 / ( lc_order_lw + cb_order_lw )
AS cb_ord_pene_wow,
cb_impress_mtd * 1.0000 / SUM(cb_impress_mtd + lc_impress_mtd)
over (
PARTITION BY p.grass_date ) -
cb_impress_lm * 1.0000 / SUM(cb_impress_lm + lc_impress_lm)
over (
PARTITION BY p.grass_date)
AS cb_impress_pct_mom,
cb_impress_mtd * 1.0000 / ( lc_impress_mtd + cb_impress_mtd ) -
cb_impress_lm * 1.0000 / ( lc_impress_lm + cb_impress_lm )
AS
cb_impress_pene_mom,
( cb_pv_mtd * 1.0000 / cb_impress_mtd ) /
(
( lc_pv_mtd + cb_pv_mtd ) * 1.0000 / ( lc_impress_mtd + cb_impress_mtd )
)
- (
cb_pv_lm * 1.0000 / cb_impress_lm ) /
(
(
lc_pv_lm + cb_pv_lm ) * 1.0000 / ( lc_impress_lm
+ cb_impress_lm ) )
AS cb_ctr_pene_mom,
( cb_order_mtd * 1.0000 / cb_pv_mtd ) / (
( lc_order_mtd + cb_order_mtd ) * 1.0000 / ( lc_pv_mtd + cb_pv_mtd ) ) -
( cb_order_lm * 1.0000 / cb_pv_lm ) / (
( lc_order_lm + cb_order_lm ) * 1.0000 /
( lc_pv_lm + cb_pv_lm ) ) AS
cb_cr_pene_mom,
cb_order_mtd * 1.0000 / ( lc_order_mtd + cb_order_mtd ) -
cb_order_lm * 1.0000 / ( lc_order_lm + cb_order_lm )
AS cb_ord_pene_mom,
( cb_impress_lw + lc_impress_lw )
AS tl_impress_lw,
( cb_impress_mtd + lc_impress_mtd )
AS tl_impress_mtd,
( cb_impress_lm + lc_impress_lm )
AS tl_impress_lm,
cb_impress_mtd * 1.0000 / ( lc_impress_mtd + cb_impress_mtd )
AS
cb_impress_pene_mtd,
( cb_pv_mtd * 1.0000 / cb_impress_mtd ) /
(
( lc_pv_mtd + cb_pv_mtd ) * 1.0000 / ( lc_impress_mtd + cb_impress_mtd )
)
AS
cb_ctr_pene_mtd,
( cb_order_mtd * 1.0000 / cb_pv_mtd ) / (
( lc_order_mtd + cb_order_mtd ) * 1.0000 / ( lc_pv_mtd + cb_pv_mtd ) )
AS
cb_cr_pene_mtd,
cb_order_mtd * 1.0000 / ( lc_order_mtd + cb_order_mtd )
AS cb_ord_pene_mtd,
cb_impress_mtd * 1.0000 / SUM(cb_impress_mtd + lc_impress_mtd)
over (
PARTITION BY p.grass_date )
AS
cb_impress_mtd_pct_country,
( cb_pv_mtd * 1.0000 / cb_impress_mtd )
AS cb_ctr_mtd,
( cb_order_mtd * 1.0000 / cb_pv_mtd )
cb_cr_mtd,
cb_order_mtd,
lc_order_mtd
-- , 'all' as cluster, cb_impress_mtd, (cb_pv_wtd+lc_pv_wtd)*1.0000/(lc_impress_wtd+lc_impress_wtd) as tl_ctr,(cb_order_wtd+lc_order_wtd)*1.0000/(cb_pv_wtd+lc_pv_wtd) as tl_cr
FROM (SELECT grass_date,
SUM(cb_impress_cnt_direct_lead) AS cb_impress_cnt_direct_lead,
SUM(lc_impress_cnt_direct_lead) AS lc_impress_cnt_direct_lead,
SUM(cb_impress_wtd) AS cb_impress_wtd,
SUM(lc_impress_wtd) AS lc_impress_wtd,
SUM(cb_impress_lw) AS cb_impress_lw,
SUM(lc_impress_lw) AS lc_impress_lw,
SUM(cb_impress_mtd) AS cb_impress_mtd,
SUM(lc_impress_mtd) AS lc_impress_mtd,
SUM(cb_impress_lm) AS cb_impress_lm,
SUM(lc_impress_lm) AS lc_impress_lm
FROM imp
GROUP BY 1) p
left join (SELECT grass_date,
SUM(cb_pv_direct_lead) AS cb_pv_direct_lead,
SUM(lc_pv_direct_lead) AS lc_pv_direct_lead,
SUM(cb_pv_wtd) AS cb_pv_wtd,
SUM(lc_pv_wtd) AS lc_pv_wtd,
SUM(cb_pv_lw) AS cb_pv_lw,
SUM(lc_pv_lw) AS lc_pv_lw,
SUM(cb_pv_mtd) AS cb_pv_mtd,
SUM(lc_pv_mtd) AS lc_pv_mtd,
SUM(cb_pv_lm) AS cb_pv_lm,
SUM(lc_pv_lm) AS lc_pv_lm
FROM pdp_v
GROUP BY 1) v
ON p.grass_date = v.grass_date
left join (SELECT grass_date,
SUM(cb_order_direct_lead) AS cb_order_direct_lead,
SUM(lc_order_direct_lead) AS lc_order_direct_lead,
SUM(cb_order_wtd) AS cb_order_wtd,
SUM(lc_order_wtd) AS lc_order_wtd,
SUM(cb_order_lw) AS cb_order_lw,
SUM(lc_order_lw) AS lc_order_lw,
SUM(cb_order_mtd) AS cb_order_mtd,
SUM(lc_order_mtd) AS lc_order_mtd,
SUM(cb_order_lm) AS cb_order_lm,
SUM(lc_order_lm) AS lc_order_lm
FROM ord
GROUP BY 1) o
ON p.grass_date = o.grass_date)
ORDER BY 3 DESC