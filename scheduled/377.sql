insert into nexus.test_49a0e68e657b0061d3168b9d5ed1727de0206f49658506045256645e58546488_39c7d10b6334e3229d4e519a91b54333 /**********************************
Name: CBWH E2E Lead Time Tracker - OB LT L7D
Function:
Notes:


Date Developer Activity
2022-05-09 Sharmaine Add (VNS, PHO and IDC)
2022-06-17 Sharmaine Add NNWH and grass_region breakdown


**********************************/
/*By phase end time*/
--#######################
-- Platform
--#######################
WITH raw_order_tab AS (
SELECT
DISTINCT grass_region,
order_id,
order_sn,
create_timestamp AS order_create_timestamp,
pay_timestamp AS paid_timestamp
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE
grass_region IN ('MY', 'TH', 'PH', 'ID', 'VN')
AND tz_type = 'local'
AND grass_date >= CURRENT_DATE - INTERVAL '40' DAY
AND DATE(FROM_UNIXTIME(create_timestamp)) >= CURRENT_DATE - INTERVAL '40' DAY
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND is_cb_shop = 1
AND is_bi_excluded = 0 --AND order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15)
),
--#######################
-- WMS
--#######################
wms_order AS (
SELECT
DISTINCT o.whs_id,
o.ctime,
o.mtime,
o.order_status,
o.ship_by_date,
o.parcel_id,
o.order_number,
sn.shopee_order_sn
FROM
wms_mart.shopee_wms_th_db__sales_outbound_order_v2_tab__reg_daily_s0_live o
LEFT JOIN wms_mart.shopee_wms_th_db__sales_outbound_order_shopee_order_sn_v2_tab__reg_daily_s0_live sn on o.order_number = sn.order_number
WHERE
o.whs_id = 'THA'
AND DATE(FROM_UNIXTIME(o.ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
DISTINCT o.whs_id,
o.ctime,
o.mtime,
o.order_status,
o.ship_by_date,
o.parcel_id,
o.order_number,
sn.shopee_order_sn
FROM
wms_mart.shopee_wms_my_db__sales_outbound_order_v2_tab__reg_daily_s0_live o
LEFT JOIN wms_mart.shopee_wms_my_db__sales_outbound_order_shopee_order_sn_v2_tab__reg_daily_s0_live sn on o.order_number = sn.order_number
WHERE
o.whs_id = 'MYC'
AND DATE(FROM_UNIXTIME(o.ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
DISTINCT o.whs_id,
o.ctime,
o.mtime,
o.order_status,
o.ship_by_date,
o.parcel_id,
o.order_number,
sn.shopee_order_sn
FROM
wms_mart.shopee_wms_ph_db__sales_outbound_order_v2_tab__reg_daily_s0_live o
LEFT JOIN wms_mart.shopee_wms_ph_db__sales_outbound_order_shopee_order_sn_v2_tab__reg_daily_s0_live sn on o.order_number = sn.order_number
WHERE
o.whs_id IN( 'PHN', 'PHO')
AND DATE(FROM_UNIXTIME(o.ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
DISTINCT o.whs_id,
o.ctime,
o.mtime,
o.order_status,
o.ship_by_date,
o.parcel_id,
o.order_number,
sn.shopee_order_sn
FROM
wms_mart.shopee_wms_id_db__sales_outbound_order_v2_tab__reg_daily_s0_live o
LEFT JOIN wms_mart.shopee_wms_id_db__sales_outbound_order_shopee_order_sn_v2_tab__reg_daily_s0_live sn on o.order_number = sn.order_number
WHERE
o.whs_id= 'IDC'
AND DATE(FROM_UNIXTIME(o.ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
DISTINCT o.whs_id,
o.ctime,
o.mtime,
o.order_status,
o.ship_by_date,
o.parcel_id,
o.order_number,
sn.shopee_order_sn
FROM
wms_mart.shopee_wms_vn_db__sales_outbound_order_v2_tab__reg_daily_s0_live o
LEFT JOIN wms_mart.shopee_wms_vn_db__sales_outbound_order_shopee_order_sn_v2_tab__reg_daily_s0_live sn on o.order_number = sn.order_number
WHERE
o.whs_id= 'VNS'
AND DATE(FROM_UNIXTIME(o.ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
DISTINCT o.whs_id,
o.ctime,
o.mtime,
o.order_status,
o.ship_by_date,
o.parcel_id,
o.order_number,
sn.shopee_order_sn
FROM
wms_mart.shopee_wms_cn_db__sales_outbound_order_v2_tab__reg_daily_s0_live o
LEFT JOIN wms_mart.shopee_wms_cn_db__sales_outbound_order_shopee_order_sn_v2_tab__reg_daily_s0_live sn on o.order_number = sn.order_number
WHERE
o.whs_id= 'CNN'
AND DATE(FROM_UNIXTIME(o.ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
),
log_tab AS(
SELECT
whs_id,
order_number,
ctime,
order_status
FROM
wms_mart.shopee_wms_ph_db__sales_outbound_order_tracking_log_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('PHN', 'PHO')
AND DATE(FROM_UNIXTIME(ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
whs_id,
order_number,
ctime,
order_status
FROM
wms_mart.shopee_wms_my_db__sales_outbound_order_tracking_log_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('MYC')
AND DATE(FROM_UNIXTIME(ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
whs_id,
order_number,
ctime - 3600 AS ctime,
order_status
FROM
wms_mart.shopee_wms_th_db__sales_outbound_order_tracking_log_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('THA')
AND DATE(FROM_UNIXTIME(ctime - 3600)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
whs_id,
order_number,
ctime - 3600 AS ctime,
order_status
FROM
wms_mart.shopee_wms_id_db__sales_outbound_order_tracking_log_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('IDC')
AND DATE(FROM_UNIXTIME(ctime - 3600)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
whs_id,
order_number,
ctime - 3600 AS ctime,
order_status
FROM
wms_mart.shopee_wms_vn_db__sales_outbound_order_tracking_log_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('VNS')
AND DATE(FROM_UNIXTIME(ctime - 3600)) >= CURRENT_DATE - INTERVAL '40' DAY
UNION
SELECT
whs_id,
order_number,
ctime AS ctime,
order_status
FROM
wms_mart.shopee_wms_cn_db__sales_outbound_order_tracking_log_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('CNN')
AND DATE(FROM_UNIXTIME(ctime)) >= CURRENT_DATE - INTERVAL '40' DAY
),
seller AS (
SELECT
order_number,
shop_id
FROM
wms_mart.shopee_wms_th_db__sales_outbound_order_seller_info_v2_tab__reg_daily_s0_live
WHERE
whs_id = 'THA'
UNION
SELECT
order_number,
shop_id
FROM
wms_mart.shopee_wms_my_db__sales_outbound_order_seller_info_v2_tab__reg_daily_s0_live
WHERE
whs_id = 'MYC'
UNION
SELECT
order_number,
shop_id
FROM
wms_mart.shopee_wms_ph_db__sales_outbound_order_seller_info_v2_tab__reg_daily_s0_live
WHERE
whs_id IN ('PHO', 'PHN')
UNION
SELECT
order_number,
shop_id
FROM
wms_mart.shopee_wms_id_db__sales_outbound_order_seller_info_v2_tab__reg_daily_s0_live
WHERE
whs_id = 'IDC'
UNION
SELECT
order_number,
shop_id
FROM
wms_mart.shopee_wms_vn_db__sales_outbound_order_seller_info_v2_tab__reg_daily_s0_live
WHERE
whs_id = 'VNS'
UNION
SELECT
order_number,
shop_id
FROM
wms_mart.shopee_wms_cn_db__sales_outbound_order_seller_info_v2_tab__reg_daily_s0_live
WHERE
whs_id = 'CNN'
),
wms_tab AS (
SELECT
a.whs_id,
a.order_number,
a.shopee_order_sn,
MIN(
CASE
WHEN b.order_status = 0 THEN b.ctime
END
) AS wms_create_timestamp,
MIN(
CASE
WHEN b.order_status = 3 THEN b.ctime
END
) AS picked_timestamp,
MIN(
CASE
WHEN b.order_status = 11 THEN b.ctime
END
) AS packed_timestamp,
MIN(
CASE
WHEN b.order_status = 7 THEN b.ctime
END
) AS ship_start_timestamp,
MIN(
CASE
WHEN b.order_status = 8 THEN b.ctime
END
) AS ob_timestamp,
MIN(
CASE
WHEN b.order_status = 1 THEN b.ctime
END
) AS cancelled_timestamp
FROM
wms_order a
LEFT JOIN log_tab b ON a.order_number = b.order_number
LEFT JOIN seller s on s.order_number = a.order_number
INNER JOIN (
SELECT
DISTINCT shop_id
FROM
mp_user.dim_shop__reg_s0_live
WHERE
shop_id IS NOT NULL
and is_cb_shop = 1
and grass_region in ('PH', 'TH', 'MY', 'VN', 'ID')
) d ON d.shop_id = s.shop_id
GROUP BY
1,
2,
3
),
--#######################
-- logistic
--#######################
logistic_tab AS (
SELECT
DISTINCT order_id,
order_sn,
lm_pickup_timestamp AS pickup_timestamp,
lm_first_attempt_delivery_timestamp AS first_attempt_timestamp
FROM
mp_ofm.dwd_forder_all_ent_df__reg_s0_live
WHERE
grass_region IN ('MY', 'TH', 'PH', 'VN', 'ID')
AND grass_date >= current_date - interval '10' day
AND tz_type = 'local'
AND is_valid_parcel = 1
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND is_cb_shop = 1
),
--#######################
-- Order return
--#######################
order_return AS (
SELECT
DISTINCT orderid AS order_id,
shopid,
status,
reason,
CASE
WHEN grass_region IN ('ID', 'VN', 'TH') THEN ctime - 3600
ELSE ctime
END AS return_timestamp
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE
grass_region IN ('MY', 'TH', 'PH', 'VN', 'ID')
AND date(from_unixtime(ctime)) >= current_date - interval '8' day
AND cb_option = 1
AND status IN (1, 2, 4, 5, 6, 7, 8)
),
combined AS (
SELECT
w.whs_id,
o.grass_region,
w.order_number,
w.shopee_order_sn,
o.order_id,
r.return_timestamp,
o.paid_timestamp,
o.order_create_timestamp,
w.wms_create_timestamp,
w.picked_timestamp,
w.packed_timestamp,
w.ship_start_timestamp,
w.ob_timestamp,
w.cancelled_timestamp,
l.pickup_timestamp,
l.first_attempt_timestamp
FROM
wms_tab w
LEFT JOIN raw_order_tab o ON w.shopee_order_sn = o.order_sn
LEFT JOIN logistic_tab l ON o.order_id = l.order_id
LEFT JOIN order_return r ON o.order_id = r.order_id
),
--#######################
-- 1-order create ~ wms order created
--#######################
phase1 AS (
SELECT
whs_id,
grass_region,
COUNT(DISTINCT order_number) AS wms_created_order,
AVG(
IF(
wms_create_timestamp > order_create_timestamp,
cast(
(wms_create_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
)
) AS phase1_avg_lt,
approx_percentile(
IF(
wms_create_timestamp > order_create_timestamp,
cast(
(wms_create_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.05
) AS phase1_lt_5p,
approx_percentile(
IF(
wms_create_timestamp > order_create_timestamp,
cast(
(wms_create_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.5
) AS phase1_lt_50p,
approx_percentile(
IF(
wms_create_timestamp > order_create_timestamp,
cast(
(wms_create_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.8
) AS phase1_lt_80p,
approx_percentile(
IF(
wms_create_timestamp > order_create_timestamp,
cast(
(wms_create_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.95
) AS phase1_lt_95p
FROM
combined
WHERE
DATE(FROM_UNIXTIME(wms_create_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY
GROUP BY
1,2
),
--#######################
-- 2-order create ~ picked
--#######################
phase2 AS (
SELECT
whs_id,
grass_region,
COUNT(DISTINCT order_number) AS picked_order,
AVG(
IF(
picked_timestamp > wms_create_timestamp,
cast(
(picked_timestamp - wms_create_timestamp) AS DOUBLE
) / 86400,
NULL
)
) AS phase2_avg_lt,
approx_percentile(
IF(
picked_timestamp > wms_create_timestamp,
cast(
(picked_timestamp - wms_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.05
) AS phase2_lt_5p,
approx_percentile(
IF(
picked_timestamp > wms_create_timestamp,
cast(
(picked_timestamp - wms_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.5
) AS phase2_lt_50p,
approx_percentile(
IF(
picked_timestamp > wms_create_timestamp,
cast(
(picked_timestamp - wms_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.8
) AS phase2_lt_80p,
approx_percentile(
IF(
picked_timestamp > wms_create_timestamp,
cast(
(picked_timestamp - wms_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.95
) AS phase2_lt_95p
FROM
combined
WHERE
DATE(FROM_UNIXTIME(picked_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY
GROUP BY
1,2
),
--#######################
-- 3-picked ~ packed
--#######################
phase3 AS (
SELECT
whs_id,
grass_region,
COUNT(DISTINCT order_number) AS packed_order,
AVG(
IF(
packed_timestamp > picked_timestamp,
cast(
(packed_timestamp - picked_timestamp) AS DOUBLE
) / 86400,
NULL
)
) AS phase3_avg_lt,
approx_percentile(
IF(
packed_timestamp > picked_timestamp,
cast(
(packed_timestamp - picked_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.05
) AS phase3_lt_5p,
approx_percentile(
IF(
packed_timestamp > picked_timestamp,
cast(
(packed_timestamp - picked_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.5
) AS phase3_lt_50p,
approx_percentile(
IF(
packed_timestamp > picked_timestamp,
cast(
(packed_timestamp - picked_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.8
) AS phase3_lt_80p,
approx_percentile(
IF(
packed_timestamp > picked_timestamp,
cast(
(packed_timestamp - picked_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.95
) AS phase3_lt_95p
FROM
combined
WHERE
DATE(FROM_UNIXTIME(packed_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY
GROUP BY
1,2
),
--#######################
-- 4-packed ~ read to ship
--#######################
phase4 AS(
SELECT
whs_id,
grass_region,
COUNT(DISTINCT order_number) AS rts_order,
AVG(
IF(
ship_start_timestamp > packed_timestamp,
cast(
(ship_start_timestamp - packed_timestamp) AS DOUBLE
) / 86400,
NULL
)
) AS phase4_avg_lt,
approx_percentile(
IF(
ship_start_timestamp > packed_timestamp,
cast(
(ship_start_timestamp - packed_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.05
) AS phase4_lt_5p,
approx_percentile(
IF(
ship_start_timestamp > packed_timestamp,
cast(
(ship_start_timestamp - packed_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.5
) AS phase4_lt_50p,
approx_percentile(
IF(
ship_start_timestamp > packed_timestamp,
cast(
(ship_start_timestamp - packed_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.8
) AS phase4_lt_80p,
approx_percentile(
IF(
ship_start_timestamp > packed_timestamp,
cast(
(ship_start_timestamp - packed_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.95
) AS phase4_lt_95p
FROM
combined
WHERE
DATE(FROM_UNIXTIME(ship_start_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY
GROUP BY
1,2
),
--#######################
-- 5-read to ship ~ pickup
--#######################
phase5 AS(
SELECT
whs_id,
grass_region,
COUNT(DISTINCT order_number) AS pickup_order,
AVG(
IF(
pickup_timestamp > ship_start_timestamp,
cast(
(pickup_timestamp - ship_start_timestamp) AS DOUBLE
) / 86400,
NULL
)
) AS phase5_avg_lt,
approx_percentile(
IF(
pickup_timestamp > ship_start_timestamp,
cast(
(pickup_timestamp - ship_start_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.05
) AS phase5_lt_5p,
approx_percentile(
IF(
pickup_timestamp > ship_start_timestamp,
cast(
(pickup_timestamp - ship_start_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.5
) AS phase5_lt_50p,
approx_percentile(
IF(
pickup_timestamp > ship_start_timestamp,
cast(
(pickup_timestamp - ship_start_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.8
) AS phase5_lt_80p,
approx_percentile(
IF(
pickup_timestamp > ship_start_timestamp,
cast(
(pickup_timestamp - ship_start_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.95
) AS phase5_lt_95p
FROM
combined
WHERE
DATE(FROM_UNIXTIME(pickup_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY
GROUP BY
1,2
),
--#######################
-- 6-pickup ~ first attempt
--#######################
phase6 AS(
SELECT
whs_id,
grass_region,
COUNT(DISTINCT order_number) AS first_attempt_order,
AVG(
IF(
first_attempt_timestamp > pickup_timestamp,
cast(
(first_attempt_timestamp - pickup_timestamp) AS DOUBLE
) / 86400,
NULL
)
) AS phase6_avg_lt,
approx_percentile(
IF(
first_attempt_timestamp > pickup_timestamp,
cast(
(first_attempt_timestamp - pickup_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.05
) AS phase6_lt_5p,
approx_percentile(
IF(
first_attempt_timestamp > pickup_timestamp,
cast(
(first_attempt_timestamp - pickup_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.5
) AS phase6_lt_50p,
approx_percentile(
IF(
first_attempt_timestamp > pickup_timestamp,
cast(
(first_attempt_timestamp - pickup_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.8
) AS phase6_lt_80p,
approx_percentile(
IF(
first_attempt_timestamp > pickup_timestamp,
cast(
(first_attempt_timestamp - pickup_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.95
) AS phase6_lt_95p
FROM
combined
WHERE
DATE(FROM_UNIXTIME(first_attempt_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY
GROUP BY
1,2
),
--#######################
-- ALL-order create ~ first attempt
--#######################
all_phase AS(
SELECT
whs_id,
grass_region,
AVG(
IF(
first_attempt_timestamp > order_create_timestamp,
cast(
(first_attempt_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
)
) AS all_avg_lt,
approx_percentile(
IF(
first_attempt_timestamp > order_create_timestamp,
cast(
(first_attempt_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.05
) AS all_lt_5p,
approx_percentile(
IF(
first_attempt_timestamp > order_create_timestamp,
cast(
(first_attempt_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.5
) AS all_lt_50p,
approx_percentile(
IF(
first_attempt_timestamp > order_create_timestamp,
cast(
(first_attempt_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.8
) AS all_lt_80p,
approx_percentile(
IF(
first_attempt_timestamp > order_create_timestamp,
cast(
(first_attempt_timestamp - order_create_timestamp) AS DOUBLE
) / 86400,
NULL
),
0.95
) AS all_lt_95p
FROM
combined
WHERE
DATE(FROM_UNIXTIME(first_attempt_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY
GROUP BY
1,2
),
--#######################
-- Order related metrics
--#######################
order_sum AS (
SELECT
whs_id,
grass_region,
COUNT(
DISTINCT CASE
WHEN DATE(FROM_UNIXTIME(paid_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY THEN order_number
ELSE NULL
END
) AS paid_order,
COUNT(
DISTINCT CASE
WHEN DATE(FROM_UNIXTIME(ob_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY THEN order_number
ELSE NULL
END
) AS ob_order,
COUNT(
DISTINCT CASE
WHEN DATE(FROM_UNIXTIME(cancelled_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY THEN order_number
ELSE NULL
END
) AS cancelled_order,
COUNT(
DISTINCT CASE
WHEN DATE(FROM_UNIXTIME(return_timestamp)) BETWEEN CURRENT_DATE - INTERVAL '7' DAY
AND CURRENT_DATE - INTERVAL '1' DAY THEN order_number
ELSE NULL
END
) AS returned_order
FROM
combined
GROUP BY
1,2
)
SELECT
current_date - interval '1' day AS reporting_date,
a.whs_id,
a.grass_region,
wms_created_order,
picked_order,
packed_order,
rts_order,
pickup_order,
first_attempt_order,
phase1_avg_lt,
phase1_lt_5p,
phase1_lt_50p,
phase1_lt_80p,
phase1_lt_95p,
phase2_avg_lt,
phase2_lt_5p,
phase2_lt_50p,
phase2_lt_80p,
phase2_lt_95p,
phase3_avg_lt,
phase3_lt_5p,
phase3_lt_50p,
phase3_lt_80p,
phase3_lt_95p,
phase4_avg_lt,
phase4_lt_5p,
phase4_lt_50p,
phase4_lt_80p,
phase4_lt_95p,
phase5_avg_lt,
phase5_lt_5p,
phase5_lt_50p,
phase5_lt_80p,
phase5_lt_95p,
phase6_avg_lt,
phase6_lt_5p,
phase6_lt_50p,
phase6_lt_80p,
phase6_lt_95p,
all_avg_lt,
all_lt_5p,
all_lt_50p,
all_lt_80p,
all_lt_95p,
paid_order,
ob_order,
cancelled_order,
returned_order
FROM
phase1 a
LEFT JOIN phase2 b ON a.whs_id = b.whs_id AND a.grass_region = b.grass_region
LEFT JOIN phase3 c ON a.whs_id = c.whs_id AND a.grass_region = c.grass_region
LEFT JOIN phase4 d ON a.whs_id = d.whs_id AND a.grass_region = d.grass_region
LEFT JOIN phase5 e ON a.whs_id = e.whs_id AND a.grass_region = e.grass_region
LEFT JOIN phase6 f ON a.whs_id = f.whs_id AND a.grass_region = f.grass_region
LEFT JOIN all_phase g ON a.whs_id = g.whs_id AND a.grass_region = g.grass_region
LEFT JOIN order_sum s ON a.whs_id = s.whs_id AND a.grass_region = s.grass_region