insert into nexus.test_ce9f8dea2d95f12d59bfbe4ea9dcd3e46ee0bc818503a0e7e620014b0941feb8_1f2f1d956ca98d0066c2eec2ba77e0b1 with seller as (
select 
gp_smt,
user_name as shop_name,
shop_id as shopid,
grass_region,
gp_name as gp_account_name
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
)
and gp_smt = 'Beauty Reseller'
group by
1,2,3,4,5
),
model_info AS (
SELECT
ip.item_id,
ip.model_id,
ip.model_name
FROM
mp_item.dim_model__reg_s0_live as ip
JOIN
seller as s
ON
ip.shop_id = s.shopid
and ip.tz_type = 'local'
and ip.grass_date = date_add('day',-1,current_date)
where
ip.grass_region <> ''
group by
1,2,3
)
SELECT
o.grass_date,
s.gp_smt,
s.gp_account_name,
s.grass_region,
s.shopid,
s.shop_name,
o.order_sn as ordersn,
o.item_id as itemid,
o.level1_global_be_category,
o.level2_global_be_category,
o.level3_global_be_category,
o.item_name as name,
o.model_id,
m.model_name,
case
when o.grass_region = 'MY' then concat('https://shopee.com.my/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'ID' then concat('https://shopee.co.id/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'PH' then concat('https://shopee.ph/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'SG' then concat('https://shopee.sg/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'TH' then concat('https://shopee.co.th/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'TW' then concat('https://shopee.tw/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'VN' then concat('https://shopee.vn/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'BR' then concat('https://shopee.com.br/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'MX' then concat('https://shopee.com.mx/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
when o.grass_region = 'PL' then concat('https://shopee.pl/product/',cast(shopid as varchar),'/',cast(o.item_id as varchar))
end as product_url,
case when o.is_flash_sale = 1 then 'Y' else 'N' end as is_flash_sale,
o.item_price_before_discount_pp as item_price_before_discount,
o.item_price_pp as item_price,
o.order_fraction as gross_orders,
o.item_amount as item_sold_qty,
o.gmv_usd as gmv_usd
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live as o
JOIN
seller as s
ON
o.shop_id = s.shopid
and o.is_placed = 1
and o.is_bi_excluded = 0
and o.grass_date between date_add('day',-7,current_date) and date_add('day',-1,current_date)
join
model_info as m
on
o.item_id = m.item_id
and o.model_id = m.model_id
where
tz_type = 'local'
order by
1 desc,2,3,4,5,7,8