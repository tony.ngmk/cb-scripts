insert into nexus.test_1d3c8ce854098c4df0201c9114861e3f6e02f7b327b88dc568c162f771786778_d39b2c10f08bc6405e64755717057c46 with icb_gps as (
select
distinct
gp_name
from
regcbbi_others.shopee_regional_bi_team__krcb_icb_shop
where
ingestion_timestamp in (
select
max(ingestion_timestamp) as maxing
from
regcbbi_others.shopee_regional_bi_team__krcb_icb_shop
)
),
seller as (
select 
distinct
child_account_owner,
follower_count,
gp_account_billing_country,
gp_account_owner,
gp_create_date,
gp_email,
s.gp_name,
gp_phone,
gp_smt,
-- grass_date,
grass_region,
is_holiday_mode,
is_official_shop,
is_preferred_shop,
is_shop_live,
is_sip_shop,
last_login_date,
rating_star,
response_rate,
seller_type,
shop_email,
shop_id,
shop_level1_global_be_category,
shop_level2_global_be_category,
shop_live_sku,
shop_name,
shop_phone,
shop_status,
shopee_account_created_date,
total_liked_num,
user_id,
user_name,
user_status
from
dev_regcbbi_kr.krcb_shop_profile as s
join
icb_gps as i
on
lower(s.gp_name) = lower(i.gp_name)
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
)
),
raw_orders as (
select
o.*
from
mp_order.dwd_order_all_ent_df__reg_s0_live as o 
join
seller as s
on
o.shop_id = s.shop_id
where
o.grass_region <> ''
and o.is_bi_excluded = 0
and o.tz_type = 'local'
),


orders as (
SELECT
a.shop_id,


count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) = date_add('day', -1, current_date) THEN a.order_id ELSE null END) yesterday_orders,


count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('week', current_date) AND date_add('day', -1, current_date) THEN a.order_id ELSE null END) WTD_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('day', -7, date_trunc('week', current_date)) AND date_add('day', -1, date_trunc('week', current_date)) THEN a.order_id ELSE null END) W_1_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('day', -14, date_trunc('week', current_date)) AND date_add('day', -8, date_trunc('week', current_date)) THEN a.order_id ELSE null END) W_2_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('month', current_date) AND date_add('day', -1, current_date) THEN a.order_id ELSE null END) MTD_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date)) THEN a.order_id ELSE null END) M_1_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date))) THEN a.order_id ELSE null END) M_2_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -3, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -2, date_trunc('month', current_date))) THEN a.order_id ELSE null END) M_3_orders,


sum(CASE WHEN date(split(a.create_datetime,' ')[1]) = date_add('day', -1, current_date) THEN a.gmv_usd ELSE 0 END) yesterday_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('week', current_date) AND date_add('day', -1, current_date) THEN gmv_usd ELSE 0 END) WTD_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('day', -7, date_trunc('week', current_date)) AND date_add('day', -1, date_trunc('week', current_date)) THEN gmv_usd ELSE 0 END) W_1_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('day', -14, date_trunc('week', current_date)) AND date_add('day', -8, date_trunc('week', current_date)) THEN gmv_usd ELSE 0 END) W_2_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('month', current_date) AND date_add('day', -1, current_date) THEN gmv_usd ELSE 0 END) MTD_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date)) THEN gmv_usd ELSE 0 END) M_1_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date))) THEN gmv_usd ELSE 0 END) M_2_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -3, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -2, date_trunc('month', current_date))) THEN gmv_usd ELSE 0 END) M_3_GMV,


case
when date_trunc('quarter',current_date) = current_date 
then count(distinct(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('quarter',date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN a.order_id ELSE null END))
else count(distinct(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('quarter',current_date) AND date_add('day', -1, current_date) THEN a.order_id ELSE null END))
end as QTD_orders,
case
when date_trunc('quarter',current_date) = current_date 
then sum((CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('quarter',date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN a.gmv_usd ELSE 0 END))
else sum((CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('quarter',current_date) AND date_add('day', -1, current_date) THEN a.gmv_usd ELSE 0 END))
end as QTD_GMV


FROM
raw_orders as a
WHERE 
date(split(a.create_datetime,' ')[1]) between date_add('day', -130, current_date) and date_add('day', -1, current_date)
and a.grass_date >= date_add('day', -130, current_date)
GROUP BY 
1
),
ado AS (
SELECT
shop_id,
yesterday_orders,
cast(WTD_orders as double) / day_of_week(date_add('day', -1, current_date)) WTD_ADO,
cast(W_1_orders as double) / 7 W_1_ADO,
cast(W_2_orders as double) / 7 W_2_ADO,
cast(MTD_orders as double) / day_of_month(date_add('day', -1, current_date)) MTD_ADO,
cast(M_1_orders as double) / date_diff('day', date_add('month', -1, date_trunc('month', current_date)), date_trunc('month', current_date)) M_1_ADO,
cast(M_2_orders as double) / date_diff('day', date_add('month', -2, date_trunc('month', current_date)), date_add('month', -1, date_trunc('month', current_date))) M_2_ADO,
cast(M_3_orders as double) / date_diff('day', date_add('month', -3, date_trunc('month', current_date)), date_add('month', -2, date_trunc('month', current_date))) M_3_ADO,


yesterday_GMV,
WTD_GMV / day_of_week(date_add('day', -1, current_date)) as WTD_GMV,
W_1_GMV / 7 as W_1_GMV,
W_2_GMV / 7 as W_2_GMV,
MTD_GMV / day_of_month(date_add('day', -1, current_date)) as MTD_GMV,
M_1_GMV / date_diff('day', date_add('month', -1, date_trunc('month', current_date)), date_trunc('month', current_date)) as M_1_GMV,
M_2_GMV / date_diff('day', date_add('month', -2, date_trunc('month', current_date)), date_add('month', -1, date_trunc('month', current_date))) as M_2_GMV,
M_3_GMV / date_diff('day', date_add('month', -3, date_trunc('month', current_date)), date_add('month', -2, date_trunc('month', current_date))) as M_3_GMV,


QTD_orders / if(date_trunc('quarter',current_date) = current_date, 
date_diff('day',date_trunc('quarter',date_add('day',-1,current_date)),current_date),
date_diff('day',date_trunc('quarter',current_date),current_date)
) as QTD_ADO,


QTD_GMV / if(date_trunc('quarter',current_date) = current_date, 
date_diff('day',date_trunc('quarter',date_add('day',-1,current_date)),current_date),
date_diff('day',date_trunc('quarter',current_date),current_date)
) as QTD_GMV


FROM
orders
),
cfs_orders as (
select
o.shop_id,
sum(case when o.item_promotion_source = 'flash_sale' then o.order_fraction else 0 end) as yesterday_cfs_orders
from
mp_order.dwd_order_item_all_ent_df__reg_s0_live as o 
join
seller as s
on
o.shop_id = s.shop_id
where
o.grass_region <> ''
and tz_type = 'local'
and o.is_cb_shop = 1
and o.is_bi_excluded = 0
and date(split(o.create_datetime,' ')[1]) = date_add('day', -1, current_date)
and o.grass_date >= date_add('day', -1, current_date)
and o.item_promotion_source = 'flash_sale'
group by
1
),
skus AS (
SELECT
a.shop_id,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) = date_add('day', -1, current_date)) THEN item_id ELSE null END)) yesterday_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) >= date_trunc('week', current_date)) THEN item_id ELSE null END)) WTD_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('day', -7, date_trunc('week', current_date)) AND date_add('day', -1, date_trunc('week', current_date))) THEN item_id ELSE null END)) W_1_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('day', -14, date_trunc('week', current_date)) AND date_add('day', -8, date_trunc('week', current_date))) THEN item_id ELSE null END)) W_2_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date) AND date_add('day', -1, current_date)) THEN item_id ELSE null END)) MTD_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date))) THEN item_id ELSE null END)) M_1_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date)))) THEN item_id ELSE null END)) M_2_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('month', -3, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -2, date_trunc('month', current_date)))) THEN item_id ELSE null END)) M_3_uploaded_sku
FROM
mp_item.dim_item__reg_s0_live as a
JOIN
seller as s
ON
a.shop_id = s.shop_id
WHERE
status = 1
AND stock > 0
and a.grass_date = date_add('day',-1,current_date) --어제 status 날짜로 필터 및 tz_type 추가
and a.tz_type = 'local'
and a.grass_region <> ''
GROUP BY
1
),
penalty1 AS (
SELECT
p.shop_id,
sum(point) as current_penalty_score
FROM
marketplace.shopee_seller_account_health_db__penalty_history_log_tab__reg_continuous_s0_live as p
JOIN 
seller as s 
ON 
p.shop_id = s.shop_id
WHERE 
manual_status = 1
AND auto_status = 1 
AND date_parse(execute_dt, '%Y%m%d') >= date_trunc('quarter',current_date) 
AND region != 'REGION_NA'
and p.grass_region <> ''
GROUP BY
1
),
penalty2 AS (
SELECT
p.shop_id,
sum(CASE WHEN date(parse_datetime(execute_dt, 'yyyymmdd')) 
BETWEEN date_add('day', -7, date_trunc('week', current_date)) AND date_add('day', -1, date_trunc('week', current_date)) THEN point ELSE null END) as W_1_penalty_points,
sum(CASE WHEN date(parse_datetime(execute_dt, 'yyyymmdd')) 
BETWEEN date_add('day', -14, date_trunc('week', current_date)) AND date_add('day', -8, date_trunc('week', current_date)) THEN point ELSE null END) as W_2_penalty_points
FROM
marketplace.shopee_seller_account_health_db__penalty_history_log_tab__reg_continuous_s0_live as p
JOIN 
seller as s 
ON 
p.shop_id = s.shop_id
WHERE 
date(parse_datetime(execute_dt, 'yyyymmdd')) >= date_add('day', -21, current_date)
AND metrics_name = 'point'
AND auto_status = 1
AND manual_status = 1
and p.grass_region <> ''
GROUP BY 
1
),
apt AS (
SELECT
a.shopid,
round(avg(CASE 
WHEN a.payment_method_id = 6 THEN b.wh_ctime - a.create_timestamp 
ELSE b.wh_ctime - a.pay_timestamp
END) / 86400, 4) as APT
FROM (
select
shop_id as shopid,
payment_method_id,
order_id,
create_timestamp,
pay_timestamp
from
raw_orders
where
date(split(create_datetime,' ')[1]) between date_add('day', -7, current_date) and date_add('day', -1, current_date)
and grass_date >= date_add('day', -7, current_date)
group by
1,2,3,4,5
) as a
JOIN (
SELECT
a.orderid,
min(a.ctime) as wh_ctime
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_continuous_s0_live as a
join
seller as s
on
a.shopid = s.shop_id 
and a.new_status = 2 
where
a.grass_region <> ''
GROUP BY 
1
having
date(from_unixtime(min(a.ctime))) >= date_add('day',-30,current_date)
) as b 
ON 
a.order_id = b.orderid
GROUP BY 
1
),
cancel AS (
SELECT
shop_id,
(CASE WHEN (lifetime_order = 0) THEN null ELSE (CAST(lifetime_cancellation AS double) / CAST(lifetime_order AS double)) END) lifttime_cancellation_rate,
(CASE WHEN (M_1_order = 0) THEN null ELSE (CAST(M_1_cancellation AS double) / CAST(M_1_order AS double)) END) M_1_cancellation_rate,
(CASE WHEN (M_2_order = 0) THEN null ELSE (CAST(M_2_cancellation AS double) / CAST(M_2_order AS double)) END) M_2_cancellation_rate
FROM
(
SELECT
o.shop_id,
count(DISTINCT (CASE WHEN (cancel_timestamp IS NOT NULL) THEN order_id ELSE null END)) lifetime_cancellation,
count(DISTINCT order_id) lifetime_order,
count(DISTINCT (CASE WHEN ((cancel_timestamp IS NOT NULL) AND (date(split(create_datetime, ' ')[1]) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date)))) THEN order_id ELSE null END)) M_1_cancellation,
count(DISTINCT (CASE WHEN (date(split(create_datetime, ' ')[1]) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date))) THEN order_id ELSE null END)) M_1_order,
count(DISTINCT (CASE WHEN ((cancel_timestamp IS NOT NULL) AND (date(split(create_datetime, ' ')[1]) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date))))) THEN order_id ELSE null END)) M_2_cancellation,
count(DISTINCT (CASE WHEN (date(split(create_datetime, ' ')[1]) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date)))) THEN order_id ELSE null END)) M_2_order
FROM
raw_orders as o
GROUP BY 
1
)
),
u5 as (
select
s.shop_id,
u.placed_order_cnt_td
from
mp_order.dws_seller_gmv_td__reg_s0_live as u
join
seller as s
on
u.shop_id = s.shop_id
and u.grass_date = date_add('day',-1,current_date)
where
u.tz_type = 'local'
group by
1,2
),
last_login_time as (
select
u.user_id,
u.last_login_datetime_td
from
mp_user.dws_user_login_td_account_info__reg_s0_live as u
join
seller as m
on
u.user_id = m.user_id
and u.grass_date = current_date - interval '1' day
and u.tz_type = 'local'
where
u.grass_region <> ''
group by
1,2 
),
merge as (
SELECT
distinct
s.grass_region,
s.shop_id as shopid,
s.user_id as userid,
s.user_name as username,
s.gp_name as gp_account_name,
s.gp_account_owner,
s.child_account_owner,
s.gp_smt,
s.shop_level1_global_be_category as shop_main_category,
s.shop_level2_global_be_category as shop_sub_category,
s.is_sip_shop,
s.shopee_account_created_date shop_reg_date,
s.gp_create_date as gp_created_date,
s.user_status,
s.shop_status,
date(split(l.last_login_datetime_td,' ')[1]) as last_login_date,
(CASE WHEN (s.is_holiday_mode IS NULL) THEN 0 ELSE s.is_holiday_mode END) holiday_mode_on,
(CASE WHEN (s.is_official_shop = 1) THEN 'Y' ELSE 'N' END) official_store,
(CASE WHEN (s.is_preferred_shop = 1) THEN 'Y' ELSE 'N' END) prefer_seller,
s.rating_star,
COALESCE(s.total_liked_num, 0) total_liked_num,
COALESCE(s.follower_count, 0) follower_count,
coalesce(s.shop_live_sku,0) as shop_live_SKU,
COALESCE(current_penalty_score, 0) current_penalty_score,
COALESCE(W_1_penalty_points, 0) W_1_penalty_points,
COALESCE(W_2_penalty_points, 0) W_2_penalty_points,
s.response_rate,
COALESCE(u5.placed_order_cnt_td, 0) gross_sold_orders,
COALESCE(cfs.yesterday_cfs_orders, 0) yesterday_cfs_orders,
COALESCE(orders.yesterday_orders, 0) yesterday_orders,
COALESCE(WTD_ADO, 0) WTD_ADO,
COALESCE(W_1_ADO, 0) W_1_ADO,
COALESCE(W_2_ADO, 0) W_2_ADO,
COALESCE(MTD_ADO, 0) MTD_ADO,
COALESCE(M_1_ADO, 0) M_1_ADO,
COALESCE(M_2_ADO, 0) M_2_ADO,
COALESCE(M_3_ADO, 0) M_3_ADO,
coalesce(QTD_ADO, 0) as QTD_ADO,
COALESCE(yesterday_GMV, 0) yesterday_ADGMV_USD,
COALESCE(WTD_GMV, 0) WTD_ADGMV_USD,
COALESCE(W_1_GMV, 0) W_1_ADGMV_USD,
COALESCE(W_2_GMV, 0) W_2_ADGMV_USD,
COALESCE(MTD_GMV, 0) MTD_ADGMV_USD,
COALESCE(M_1_GMV, 0) M_1_ADGMV_USD,
COALESCE(M_2_GMV, 0) M_2_ADGMV_USD,
COALESCE(M_3_GMV, 0) M_3_ADGMV_USD,
coalesce(QTD_GMV, 0) as QTD_ADGMV_USD,
COALESCE(skus.yesterday_uploaded_sku, 0) yesterday_uploaded_sku,
COALESCE(skus.WTD_uploaded_sku, 0) WTD_uploaded_sku,
COALESCE(skus.W_1_uploaded_sku, 0) W_1_uploaded_sku,
COALESCE(skus.W_2_uploaded_sku, 0) W_2_uploaded_sku,
COALESCE(skus.MTD_uploaded_sku, 0) MTD_uploaded_sku,
COALESCE(skus.M_1_uploaded_sku, 0) M_1_uploaded_sku,
COALESCE(skus.M_2_uploaded_sku, 0) M_2_uploaded_sku,
COALESCE(skus.M_3_uploaded_sku, 0) M_3_uploaded_sku,
APT,
lifttime_cancellation_rate,
M_1_cancellation_rate,
M_2_cancellation_rate,
CASE
WHEN s.grass_region = 'MY' THEN concat('https://shopee.com.my/', s.user_name)
WHEN s.grass_region = 'ID' THEN concat('https://shopee.co.id/', s.user_name)
WHEN s.grass_region = 'PH' THEN concat('https://shopee.ph/', s.user_name)
WHEN s.grass_region = 'SG' THEN concat('https://shopee.sg/', s.user_name)
WHEN s.grass_region = 'TH' THEN concat('https://shopee.co.th/', s.user_name)
WHEN s.grass_region = 'TW' THEN concat('https://shopee.tw/', s.user_name)
WHEN s.grass_region = 'VN' THEN concat('https://shopee.vn/', s.user_name)
WHEN s.grass_region = 'BR' THEN concat('https://shopee.com.br/', s.user_name)
when s.grass_region = 'MX' then concat('https://shopee.com.mx/', s.user_name)
when s.grass_region = 'PL' then concat('https://shopee.pl/', s.user_name)
END as shop_url,
s.seller_type
FROM
seller s
LEFT JOIN
penalty1 p1
ON
s.shop_id = p1.shop_id
LEFT JOIN
penalty2 p2
ON
s.shop_id = p2.shop_id
LEFT JOIN
apt
ON
s.shop_id = apt.shopid 
LEFT JOIN
cancel
ON
s.shop_id = cancel.shop_id
LEFT JOIN
skus
ON
s.shop_id = skus.shop_id
LEFT JOIN
ado as orders
ON
s.shop_id = orders.shop_id
left join
cfs_orders as cfs
on
s.shop_id = cfs.shop_id
LEFT JOIN
u5
ON
s.shop_id = u5.shop_id
left join
last_login_time as l
on
s.user_id = l.user_id
WHERE
s.grass_region IS NOT NULL
)
select
*
from
merge
order by
5,6,1,4