insert into nexus.test_bf28943cc0472eaaf11413f93d9eb636e20065b7d9eb89ec942b628a96785e99_1806bbefd81558dc292c3a7adb1d2028 ---- new market ----- 
WITH
sip AS (
SELECT DISTINCT
affi_shopid
, mst_shopid
, t1.country mst_country
, t2.country affi_country
, t1.cb_option
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (sip_shop_status <> 3) AND GRASS_REGION in ('PL','ES','FR')
) t2 ON (t1.shopid = t2.mst_shopid))
WHERE (t2.country in ('PL','ES','FR') )
) 


, track as (
select distinct keyword
, caSt(min_price as BIGINT) min_price
from regcbbi_others.shopee_regional_cb_team__twtopskukeywords 
where ingestion_timestamp = (select max(ingestion_timestamp) col
from regcbbi_others.shopee_regional_cb_team__twtopskukeywords)


) 


, un as (select distinct mst_itemid 
, unlist_region
from marketplace.shopee_sip_db__item_unlist_config_tab__reg_daily_s0_live) 


select distinct


map.affi_shopid 
, map.mst_shopid 
, map.mst_itemid 
, sip.mst_country 
, map.affi_country 
, map.affi_itemid 
, name 
, status
, 'mobile keywords' keywords
, unlist_region


from (select distinct item_id 
, shop_id 
, name 
, price
, status 
from mp_item.dim_item__reg_s0_live 
where grass_date = current_date-interval'1'day and grass_region in ('FR','ES')
AND level2_global_be_category = 'Mobile Phones'
and is_cb_shop=1 and tz_type='local' and shop_status=1 and seller_status=1 and is_holiday_mode=0 and status not in (4,0,5,3,8)
AND regexp_like(name, 'Carcasa para móvil|Funda para móvil|Estuche para móvil|Carcasa|Funda|Estuche|Funda protectora|Carcasa protectora|Estuche proctector|Protector|Protector de voltaje|Línea de transmisión|Cable de transmisión|Cable|Cargar|Recargar|Batería|Pila|Célula|Enchufe|Clavija|Plug|Enchufe de conversión|Adaptador de enchufe|Adaptador para enchufe|Móvil demo|Móvil de exhibición|Prototipo móvil|Modelo|Auriculares|Audífonos|Auriculares con cable|Audífonos con cable|Audífonos inalámbricos|Auriculares inalámbricos|Auriculares sin cable|Auriculares Bluetooth|Audífonos Bluetooth|Palo de selfie|Stick de selfie|Barra de selfie|Espejo para selfies|Espejo para autofotos|Membrana de pantalla|Protector de pantalla|Cristal templado|Protector|Membrana|Soporte|Base|Stand|Soporte para móvil|Base para móvil|Stand para móvil|Tarjeta de almacenamiento|Tarjeta de memoria|Tarjeta de datos|Reloj inteligente / Pulsera inteligente|Smartwatch / Pulsera inteligente')=False 
) i 
join (select distinct affi_itemid 
, mst_itemid 
, affi_shopid 
, mst_shopid 
, affi_country 
from marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
where affi_country in ('FR','ES')
) map on map.affi_itemid = i.item_id 

join track t on regexp_like(lower(name),t.keyword) and i.price <= t.min_price
join(select * 
from sip 
where affi_country in ('FR','ES') ) sip on sip.affi_shopid = i.shop_id 
left join un on un.mst_itemid = map.mst_itemid 

union 


SELECT DISTINCT
affi_shopid
, mst_shopid
, map.mst_itemid
, mst_country
, map.affi_country



, i.itemid
, name
, status
, (CASE WHEN (k.keyword IS NOT NULL) THEN k.keyword ELSE 'N' END) keyword
, unlist_region
FROM
((sip
INNER JOIN (
SELECT DISTINCT
item_id itemid
, shop_id shopid
, replace(lower(name),'_',' ') name 
, status
FROM
mp_item.dim_item__reg_s0_live
WHERE ((NOT (status IN (0, 4, 5,8))) AND (grass_region in('PL','ES','FR') )) and is_cb_shop=1 and grass_date=current_date-interval'1'day 
and seller_status=1 and shop_status=1 and is_holiday_mode=0 
) i ON (i.shopid = sip.affi_shopid))
INNER JOIN (
SELECT DISTINCT
affi_itemid
, mst_itemid
, affi_country 
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE (affi_country in ('PL','ES','FR') )
) map ON (map.affi_itemid = i.itemid))
INNER JOIN (
SELECT DISTINCT keyword
, country
FROM
regcbbi_others.prohibited_keyword_tmp_new_market_s0
) k ON regexp_like(name , "concat"('\b', CAST(keyword AS varchar), '\b')) 
and k.country = map.affi_country 


left join un on un.mst_itemid = map.mst_itemid