SELECT o.Purchase_date as Purchase_Date
, COALESCE(CB_Order, 0) as CB_Order
, 'NA' as P9_cfs_orders
, 'NA' as P9_campaign_orders
, '0' as P9_orders_CB_percent
, 'NA' as CB_WH_Non_resell_Order
-- , nvl(wh_resell, 0) as `CB WH Resell Order`
, '0' as Campaign_Order
, COALESCE(CB_GMV, 0) as CB_GMV
, COALESCE(cb_new, 0) AS CB_New_SKU
, COALESCE(cb_live, 0) as CB_Total_SKU
, COALESCE(CB_Items_Sold, 0) as CB_Items_Sold
, COALESCE(round(CB_GMV/CB_Order, 2), 0) as CB_ABS_usd
, COALESCE(round(Cancelled_Orders_no/CB_Order, 4), 0) as Cancellation_Rate
, COALESCE(round(CB_Order/(country_order-local_order_all+local_order), 4), 0) as CB_Order_percent
, COALESCE(round((CB_Order/ CB_view) / ((country_order-local_order_all+local_order)/ Country_view), 4), 0) as percent_of_Country_CR
, COALESCE(round( CB_view / Country_view, 4), 0) as percent_of_Country_View
, COALESCE(round(CB_GMV/(country_GMV-local_GMV_all+local_GMV), 4), 0) as CB_GMV_percent
, COALESCE(round(CAST(cb_live AS DOUBLE)/country_live, 4), 0) as CB_SKU_percent
, round((country_GMV-local_GMV_all+local_GMV)/(country_order-local_order_all+local_order), 2) as Country_ABS_usd
, country_order-(local_order_all-local_order) as Total_Country_Orders
, 'NA' as CNCB_Campaign_Item_Sold
, COALESCE(Cancelled_Orders_no, 0) as CNCB_Cancelled_Orders
, country_live
, country_new
, country_order as country_order_all
, country_GMV as country_GMV_all
, country_GMV-local_GMV_all+local_GMV as Total_Country_GMV


FROM /* SKU */ 
(select cb.Purchase_date, cb_live, cb_new, country_live, country_new
from
(SELECT distinct create_date as Purchase_date
, count(itemid) over (ORDER BY create_date) as cb_live
, count(itemid) over (PARTITION BY create_date) as cb_new
FROM
(
SELECT DISTINCT shop_id shopid, item_id itemid, date(from_unixtime(create_timestamp)) create_date
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region = 'PH'
AND tz_type = 'local'
AND grass_date = date(from_unixtime(1655596800))
AND seller_status = 1
AND shop_status = 1
AND is_holiday_mode = 0
AND status = 1
AND stock > 0
AND is_cb_shop = 1
) i 
) cb
left join
(SELECT distinct create_date as Purchase_date,
count(itemid) over (ORDER BY create_date) as country_live, 
count(itemid) over (partition BY create_date) as country_new
FROM
(
SELECT DISTINCT shop_id shopid, item_id itemid, date(from_unixtime(create_timestamp)) create_date
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region = 'PH'
AND tz_type = 'local'
AND grass_date = date(from_unixtime(1655596800))
AND seller_status = 1
AND shop_status = 1
AND is_holiday_mode = 0
AND status = 1
AND stock > 0
) i 
) total on cb.Purchase_date=total.Purchase_date
) s

right join /* ADO GMV */
(
SELECT 
date(from_unixtime(create_timestamp)) as Purchase_date
, count(distinct b.order_id) as country_order
, CAST(count(distinct case when b.is_cb_shop = 1 then b.order_id end) AS DOUBLE) AS CB_Order
, count(distinct case when b.is_cb_shop = 0 then b.order_id end) AS local_order_all
, count(distinct case when b.is_cb_shop = 0 and b.shop_id != 140193611 then b.order_id end) AS local_order
, round(sum(case when b.is_cb_shop = 1 then gmv_usd end), 2) AS CB_GMV
, round(sum(case when b.is_cb_shop = 0 and b.shop_id != 140193611 then gmv_usd end), 2) AS local_GMV
, round(sum(case when b.is_cb_shop = 0 then gmv_usd end), 2) AS local_GMV_all
, round(sum(gmv_usd), 2) AS country_GMV
, sum(case when b.is_cb_shop = 1 then item_amount end) as CB_Items_Sold
, CAST(count(distinct case when b.order_be_status_id = 8 and b.is_cb_shop = 1 THEN b.order_id END) AS DOUBLE) as Cancelled_Orders_no 
from mp_order.dwd_order_item_all_ent_df__reg_s0_live b
WHERE tz_type = 'local'
and b.grass_region = 'PH' 
--and grass_date >= date(from_unixtime(1655596800)) - interval '6' day
and date(from_unixtime(create_timestamp)) BETWEEN date(from_unixtime(1655596800)) - interval '6' day and date(from_unixtime(1655596800))
group by 1
) o 
on s.Purchase_date = o.Purchase_date

-- LEFT JOIN /* campaign */
-- (select Purchase_date
-- , sum(case when campaign = 1 and flash_sale = 0 then order_fraction end) as Campaign_Order
-- , sum(case when price = 9 then order_fraction end) as P9_orders
-- -- , SUM(case when campaign = 1 and flash_sale = 0 then amount else 0 end) as `Campaign Item Sold`
-- -- , sum(case when (flash_sale = 1 and cfs_price = 9) then order_fraction else 0 end) as P9_cfs_orders
-- -- , sum(case when (campaign = 1 and flash_sale = 0 and price = 9) then order_fraction else 0 end) as P9_campaign_orders
-- from 
-- (
-- select distinct 
-- date(from_unixtime(create_timestamp)) as Purchase_date
-- , c2.order_id as orderid
-- , c2.item_id as itemid
-- , c2.model_id as modelid
-- , bundle_order_item_id as bundle_order_itemid
-- , c2.item_amount as amount
-- , c2.order_fraction
-- , c2.item_price_pp as price
-- , case when c1.item_id is not null then 1 else 0 end as campaign
-- , case when c2.item_promotion_source = 'flash_sale' and c2.item_promotion_source is not null then 1 else 0 end as flash_sale
-- from 
-- (select distinct item_id, collection_id, start_time, end_time 
-- from regbida_userbehavior.shopee_bi_campaign_item 
-- where grass_region = 'PH' and grass_date between date(from_unixtime(1655596800)) - interval '6' day and date(from_unixtime(1655596800))
-- ) c1 
-- right join 
-- (
-- SELECT *
-- from mp_order.dwd_order_item_all_ent_df__reg_s0_live
-- WHERE tz_type = 'local'
-- and grass_region = 'PH' 
-- and is_cb_shop = 1
-- --and grass_date >= date(from_unixtime(1655596800)) - interval '7' day
-- and date(from_unixtime(create_timestamp)) BETWEEN date(from_unixtime(1655596800)) - interval '6' day and date(from_unixtime(1655596800))
-- ) c2 on c1.item_id = c2.item_id and c2.create_timestamp between c1.start_time and c1.end_time
-- )
-- group by 1) c 
-- on o.Purchase_date = c.Purchase_date
LEFT JOIN 
(
SELECT 
v.grass_date
, sum(case when s.is_cb_shop = 1 then daily_view else null end) CB_view
, sum(daily_view) Country_view
FROM 
(
SELECT shop_id
, grass_date
, cast(pv_cnt_1d as double) daily_view
FROM mp_shopflow.dws_item_civ_1d__reg_s0_live
WHERE tz_type = 'local'
AND grass_region = 'PH'
AND grass_date >= date(from_unixtime(1655596800)) - interval '6' day
AND grass_date <= date(from_unixtime(1655596800))
) v
LEFT JOIN 
(
SELECT distinct shop_id, is_cb_shop
FROM mp_user.dim_shop__reg_s0_live
WHERE tz_type = 'local'
AND grass_region = 'PH'
AND grass_date >= date(from_unixtime(1655596800)) - interval '6' day
) s on v.shop_id = s.shop_id
GROUP BY 1
) t on o.purchase_date = t.grass_date


order by o.purchase_date DESC