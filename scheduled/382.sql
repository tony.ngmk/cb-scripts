insert into nexus.test_4873dc296ee26632860704be6d852b556af1be9130fbe0b74e553b66783ac3e2_9ca735bd494eab1c704ef09942c02179 WITH
raw AS (
SELECT
a.order_id as orderid
, a.item_id as itemid
, a.model_id as modelid
, a.gmv_usd
, a.grass_date purchase_date
, a.level1_category as main_category
, is_cb_shop
, item_price_pp_usd
-- , a.status_ext
, a.order_fraction pp
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live a
where grass_region='MY' and is_placed=1 and grass_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, current_date) and is_bi_excluded=0
) 

,map as
(
SELECT distinct
cast(grass_region as varchar) grass_region
, cast(l1_cat_id as int) l1_cat_id
, cast(l1_cat as varchar) as l1_cat
, cast(cluster as varchar) as cluster
FROM regcbbi_others.shopee_regional_cb_team__regional_cb_cluster_mapping
WHERE ingestion_timestamp!='1' and CAST(ingestion_timestamp AS bigint) = (SELECT max(CAST(ingestion_timestamp AS bigint)) aa FROM regcbbi_others.shopee_regional_cb_team__regional_cb_cluster_mapping where ingestion_timestamp!='1' ) and cast(grass_region as varchar)='MY'
) 

, ado AS (
SELECT
main_category
, try("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN pp ELSE 0 END))*1.0000/day_of_week("date_add"('day', -1, current_date)) ) cb_WTD_ADO
, try("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN pp ELSE 0 END))*1.0000/day_of_week("date_add"('day', -1, current_date)) ) WTD_ADO
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date)))) and is_cb_shop=1 THEN pp ELSE 0 END)*1.0000/7) cb_W1_ADO
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date)))) THEN pp ELSE 0 END)*1.0000/7) W1_ADO
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN pp ELSE 0 END))*1.0000 / day("date_add"('day', -1, current_date))) cb_MTD_ADO
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN pp ELSE 0 END)) / day("date_add"('day', -1, current_date))) MTD_ADO
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) and is_cb_shop=1 THEN pp ELSE 0 END))*1.0000 / day("date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date))))) cb_M1_ADO
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) THEN pp ELSE 0 END))*1.0000 / day("date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date))))) M1_ADO


, ("avg"(CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_WTD_ASP
, ("avg"(CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN item_price_pp_usd ELSE 0 END) ) WTD_ASP
, "avg"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date))) ) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END) cb_W1_ASP
, "avg"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date))) ) THEN item_price_pp_usd ELSE 0 END ) W1_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_MTD_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN item_price_pp_usd ELSE 0 END)) MTD_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_M1_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) THEN item_price_pp_usd ELSE 0 END)) M1_ASP

FROM
raw
GROUP BY 1
) 


, tf_raw as (
select purchase_date,a.item_id,main_category,is_cb_shop,daily_imp,daily_vw,daily_clk
from
(SELECT
grass_date AS purchase_date,
shop_id,
item_id,
SUM(imp_cnt_1d) AS daily_imp,
SUM(pv_cnt_1d) AS daily_vw,
SUM(clk_cnt_1d) AS daily_clk
FROM mp_shopflow.dws_item_civ_1d__reg_s0_live--traffic_mart_dws__item_exposure_1d
WHERE
grass_region = 'MY'
AND grass_date between "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) and date_add('day',-1,current_date) and tz_type='local'
GROUP BY 1, 2, 3
) a
left join (select item_id, is_cb_shop , level1_category as main_category from mp_item.dim_item__reg_s0_live where grass_region='MY' and grass_date=date_add('day',-1,current_date) and tz_type='local') b on a.item_id=b.item_id
-- left join map on map.l1_cat_id=b.level1_category_id
-- group by 1,2
)


, tf as (
SELECT
main_category
, try("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN daily_imp ELSE 0 END))*1.0000/day_of_week("date_add"('day', -1, current_date)) ) cb_WTD_imp 
, try("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN daily_imp ELSE 0 END))*1.0000/day_of_week("date_add"('day', -1, current_date)) ) WTD_imp 
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date))) ) and is_cb_shop=1 THEN daily_imp ELSE 0 END)*1.0000/7) cb_W1_imp 
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date))) ) THEN daily_imp ELSE 0 END)*1.0000/7 ) W1_imp 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN daily_imp ELSE 0 END))*1.0000 / day("date_add"('day', -1, current_date))) cb_MTD_imp 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN daily_imp ELSE 0 END))*1.0000 / day("date_add"('day', -1, current_date))) MTD_imp 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) and is_cb_shop=1 THEN daily_imp ELSE 0 END)) *1.0000/ day("date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date))))) cb_M1_imp 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) THEN daily_imp ELSE 0 END)) *1.0000/ day("date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date))))) M1_imp 


, try("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN daily_vw ELSE 0 END))*1.0000/day_of_week("date_add"('day', -1, current_date)) ) cb_WTD_vw 
, try("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN daily_vw ELSE 0 END))*1.0000/day_of_week("date_add"('day', -1, current_date)) ) WTD_vw 
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date)))) and is_cb_shop=1 THEN daily_vw ELSE 0 END)*1.0000/7) cb_W1_vw 
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date)))) THEN daily_vw ELSE 0 END)*1.0000/7) W1_vw 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN daily_vw ELSE 0 END)) *1.0000/ day("date_add"('day', -1, current_date))) cb_MTD_vw 
,try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN daily_vw ELSE 0 END))*1.0000 / day("date_add"('day', -1, current_date))) MTD_vw 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) and is_cb_shop=1 THEN daily_vw ELSE 0 END))*1.0000 / day("date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date))))) cb_M1_vw 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) THEN daily_vw ELSE 0 END))*1.0000 / day("date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date))))) M1_vw 


, try("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN daily_clk ELSE 0 END))*1.0000/day_of_week("date_add"('day', -1, current_date)) ) cb_WTD_clk 
, try("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN daily_clk ELSE 0 END))*1.0000/day_of_week("date_add"('day', -1, current_date)) ) WTD_clk 
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date))) ) and is_cb_shop=1 THEN daily_clk ELSE 0 END)*1.0000/7) cb_W1_clk 
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date))) ) THEN daily_clk ELSE 0 END)*1.0000/7 ) W1_clk 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN daily_clk ELSE 0 END)) *1.0000/ day("date_add"('day', -1, current_date))) cb_MTD_clk 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN daily_clk ELSE 0 END))*1.0000 / day("date_add"('day', -1, current_date))) MTD_clk 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) and is_cb_shop=1 THEN daily_clk ELSE 0 END))*1.0000 / day("date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date))))) cb_M1_clk 
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) THEN daily_clk ELSE 0 END))*1.0000 / day("date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date))))) M1_clk 

FROM tf_raw
GROUP BY 1


)


, total as (
select
sum(cb_WTD_ADO) as cb_WTD_ADO,
sum(WTD_ADO) as WTD_ADO,
sum(cb_W1_ADO) as cb_W1_ADO,
sum(W1_ADO) as W1_ADO,
sum(cb_MTD_ADO) as cb_MTD_ADO,
sum(MTD_ADO) as MTD_ADO,
sum(cb_M1_ADO) as cb_M1_ADO,
sum(M1_ADO) as M1_ADO,

sum(cb_WTD_imp) as cb_WTD_imp,
sum(WTD_imp) as WTD_imp,
sum(cb_W1_imp) as cb_W1_imp,
sum(W1_imp) as W1_imp,
sum(cb_MTD_imp) as cb_MTD_imp,
sum(MTD_imp) as MTD_imp,
sum(cb_M1_imp) as cb_M1_imp,
sum(M1_imp) as M1_imp,

sum(cb_WTD_vw) as cb_WTD_vw,
sum(WTD_vw) as WTD_vw,
sum(cb_W1_vw) as cb_W1_vw,
sum(W1_vw) as W1_vw,
sum(cb_MTD_vw) as cb_MTD_vw,
sum(MTD_vw) as MTD_vw,
sum(cb_M1_vw) as cb_M1_vw,
sum(M1_vw) as M1_vw,

sum(cb_WTD_clk) as cb_WTD_clk,
sum(WTD_clk) as WTD_clk,
sum(cb_W1_clk) as cb_W1_clk,
sum(W1_clk) as W1_clk,
sum(cb_MTD_clk) as cb_MTD_clk,
sum(MTD_clk) as MTD_clk,
sum(cb_M1_clk) as cb_M1_clk,
sum(M1_clk) as M1_clk

from ado 
left join tf t ON (t.main_category = ado.main_category)
)


, asp as (
select ("avg"(CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_WTD_ASP
, ("avg"(CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN item_price_pp_usd ELSE 0 END) ) WTD_ASP
, "avg"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date))) ) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END) cb_W1_ASP
, "avg"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -1, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -1, current_date))) ) THEN item_price_pp_usd ELSE 0 END ) W1_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_MTD_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN item_price_pp_usd ELSE 0 END)) MTD_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_M1_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) THEN item_price_pp_usd ELSE 0 END)) M1_ASP
from raw

)

(SELECT
cluster, ado.main_category
, cb_WTD_ADO
, try(cb_WTD_ADO/cb_W1_ADO) - 1 as cb_ado_wow
, try(cb_MTD_ADO/cb_M1_ADO) - 1 as cb_ado_mom
, try(cb_WTD_ADO/WTD_ADO) as ado_pene_wtd
, try(cb_MTD_ADO/MTD_ADO) as ado_pene_mtd
, try(cb_WTD_ADO/WTD_ADO )- try(cb_W1_ADO/W1_ADO) as ado_pene_wow
, try(cb_MTD_ADO/MTD_ADO) - try(cb_M1_ADO/M1_ADO) as ado_pene_mom

, cb_WTD_vw
, try(cb_WTD_vw/cb_W1_vw) - 1 as cb_vw_wow
, try(cb_MTD_vw/cb_M1_vw) - 1 as cb_vw_mom
, try(cb_WTD_vw/WTD_vw) as vw_pene_wtd
, try(cb_MTD_vw/MTD_vw) as vw_pene_mtd
, try(cb_WTD_vw/WTD_vw) - try(cb_W1_vw/W1_vw) as vw_pene_wow
, try(cb_MTD_vw/MTD_vw) - try(cb_M1_vw/M1_vw) as vw_pene_mom

, cb_WTD_imp
, try(cb_WTD_imp/cb_W1_imp) - 1 as cb_imp_wow
, try(cb_MTD_imp/cb_M1_imp) - 1 as cb_imp_mom
, try(cb_WTD_imp/WTD_imp) as imp_pene_wtd
, try(cb_MTD_imp/MTD_imp) as imp_pene_mtd
, try(cb_WTD_imp/WTD_imp) - try(cb_W1_imp/W1_imp) as imp_pene_wow
, try(cb_MTD_imp/MTD_imp) - try(cb_M1_imp/M1_imp) as imp_pene_mom

, try(cb_WTD_clk/cb_WTD_imp) as cb_WTD_ctr
, try(cb_WTD_clk/cb_WTD_imp) - try(cb_W1_clk/cb_W1_imp) as cb_ctr_wow
, try(cb_MTD_clk/cb_MTD_imp) - try(cb_M1_clk/cb_M1_imp) as cb_ctr_mom
, try((cb_WTD_clk/cb_WTD_imp)/(WTD_clk/WTD_imp)) as cb_WTD_ctr_pene
, (cb_MTD_clk/cb_MTD_imp)/(MTD_clk/MTD_imp) as cb_MTD_ctr_pene
, try((cb_WTD_clk/cb_WTD_imp)/(WTD_clk/WTD_imp) )- try((cb_W1_clk/cb_W1_imp)/(W1_clk/W1_imp)) as cb_ctr_pene_wow
, try((cb_MTD_clk/cb_MTD_imp)/(MTD_clk/MTD_imp)) - try((cb_M1_clk/cb_M1_imp)/(M1_clk/M1_imp)) as cb_ctr_pene_mom

, cb_WTD_ADO/cb_WTD_vw as cb_WTD_cr
, try(cb_WTD_ADO/cb_WTD_vw) - try(cb_W1_ADO/cb_W1_vw ) as cb_cr_wow
, try(cb_MTD_ADO/cb_MTD_vw) - try(cb_M1_ADO/cb_M1_vw) as cb_cr_mom
, try((cb_WTD_ADO/cb_WTD_vw)/(WTD_ADO/WTD_vw)) as cb_WTD_cr_pene
, try((cb_MTD_ADO/cb_MTD_vw)/(MTD_ADO/MTD_vw)) as cb_MTD_cr_pene
, try((cb_WTD_ADO/cb_WTD_vw)/(WTD_ADO/WTD_vw)) - try((cb_W1_ADO/cb_W1_vw)/(W1_ADO/W1_vw)) as cb_cr_pene_wow
, try((cb_MTD_ADO/cb_MTD_vw)/(MTD_ADO/MTD_vw)) - try((cb_M1_ADO/cb_M1_vw)/(M1_ADO/M1_vw)) as cb_cr_pene_mom

, cb_WTD_ASP
, try(cb_WTD_ASP/cb_W1_ASP) - 1 as cb_ASP_wow
, try(cb_MTD_ASP/cb_M1_ASP) - 1 as cb_ASP_mom
, try(cb_WTD_ASP/WTD_ASP) as ASP_pene_wtd
, try(cb_MTD_ASP/MTD_ASP) as ASP_pene_mtd
, try(cb_WTD_ASP/WTD_ASP) - try(cb_W1_ASP/W1_ASP) as ASP_pene_wow
, try(cb_MTD_ASP/MTD_ASP) - try(cb_M1_ASP/M1_ASP) as ASP_pene_mom

FROM
ado
LEFT JOIN tf t ON (t.main_category = ado.main_category)
Left join map p on p.l1_cat=ado.main_category
where cluster is not null
)
UNION 
(SELECT
'CB' as cluster
, 'total' as main_category
, cb_WTD_ADO
, try(cb_WTD_ADO/cb_W1_ADO) - 1 as cb_ado_wow
, try(cb_MTD_ADO/cb_M1_ADO) - 1 as cb_ado_mom
, try(cb_WTD_ADO/WTD_ADO) as ado_pene_wtd
, try(cb_MTD_ADO/MTD_ADO) as ado_pene_mtd
, try(cb_WTD_ADO/WTD_ADO )- try(cb_W1_ADO/W1_ADO) as ado_pene_wow
, try(cb_MTD_ADO/MTD_ADO) - try(cb_M1_ADO/M1_ADO) as ado_pene_mom

, cb_WTD_vw
, try(cb_WTD_vw/cb_W1_vw) - 1 as cb_vw_wow
, try(cb_MTD_vw/cb_M1_vw) - 1 as cb_vw_mom
, try(cb_WTD_vw/WTD_vw) as vw_pene_wtd
, try(cb_MTD_vw/MTD_vw) as vw_pene_mtd
, try(cb_WTD_vw/WTD_vw) - try(cb_W1_vw/W1_vw) as vw_pene_wow
, try(cb_MTD_vw/MTD_vw) - try(cb_M1_vw/M1_vw) as vw_pene_mom

, cb_WTD_imp
, try(cb_WTD_imp/cb_W1_imp) - 1 as cb_imp_wow
, try(cb_MTD_imp/cb_M1_imp) - 1 as cb_imp_mom
, try(cb_WTD_imp/WTD_imp) as imp_pene_wtd
, try(cb_MTD_imp/MTD_imp) as imp_pene_mtd
, try(cb_WTD_imp/WTD_imp) - try(cb_W1_imp/W1_imp) as imp_pene_wow
, try(cb_MTD_imp/MTD_imp) - try(cb_M1_imp/M1_imp) as imp_pene_mom

, try(cb_WTD_clk/cb_WTD_imp) as cb_WTD_ctr
, try(cb_WTD_clk/cb_WTD_imp) - try(cb_W1_clk/cb_W1_imp) as cb_ctr_wow
, try(cb_MTD_clk/cb_MTD_imp) - try(cb_M1_clk/cb_M1_imp) as cb_ctr_mom
, try((cb_WTD_clk/cb_WTD_imp)/(WTD_clk/WTD_imp)) as cb_WTD_ctr_pene
, (cb_MTD_clk/cb_MTD_imp)/(MTD_clk/MTD_imp) as cb_MTD_ctr_pene
, try((cb_WTD_clk/cb_WTD_imp)/(WTD_clk/WTD_imp) )- try((cb_W1_clk/cb_W1_imp)/(W1_clk/W1_imp)) as cb_ctr_pene_wow
, try((cb_MTD_clk/cb_MTD_imp)/(MTD_clk/MTD_imp)) - try((cb_M1_clk/cb_M1_imp)/(M1_clk/M1_imp)) as cb_ctr_pene_mom

, cb_WTD_ADO/cb_WTD_vw as cb_WTD_cr
, try(cb_WTD_ADO/cb_WTD_vw) - try(cb_W1_ADO/cb_W1_vw ) as cb_cr_wow
, try(cb_MTD_ADO/cb_MTD_vw) - try(cb_M1_ADO/cb_M1_vw) as cb_cr_mom
, try((cb_WTD_ADO/cb_WTD_vw)/(WTD_ADO/WTD_vw)) as cb_WTD_cr_pene
, try((cb_MTD_ADO/cb_MTD_vw)/(MTD_ADO/MTD_vw)) as cb_MTD_cr_pene
, try((cb_WTD_ADO/cb_WTD_vw)/(WTD_ADO/WTD_vw)) - try((cb_W1_ADO/cb_W1_vw)/(W1_ADO/W1_vw)) as cb_cr_pene_wow
, try((cb_MTD_ADO/cb_MTD_vw)/(MTD_ADO/MTD_vw)) - try((cb_M1_ADO/cb_M1_vw)/(M1_ADO/M1_vw)) as cb_cr_pene_mom

, cb_WTD_ASP
, try(cb_WTD_ASP/cb_W1_ASP) - 1 as cb_ASP_wow
, try(cb_MTD_ASP/cb_M1_ASP) - 1 as cb_ASP_mom
, try(cb_WTD_ASP/WTD_ASP) as ASP_pene_wtd
, try(cb_MTD_ASP/MTD_ASP) as ASP_pene_mtd
, try(cb_WTD_ASP/WTD_ASP) - try(cb_W1_ASP/W1_ASP) as ASP_pene_wow
, try(cb_MTD_ASP/MTD_ASP) - try(cb_M1_ASP/M1_ASP) as ASP_pene_mom
from (select 'total' as main_category, * from total) a
left join (select 'total' as main_category,* from asp) b on a.main_category=b.main_category
)
ORDER BY 1,3 DESC