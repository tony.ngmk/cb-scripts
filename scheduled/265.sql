insert into nexus.test_85f65254b40143d58889e70cb83b995e4304eb0778a533707f14296945fa0a79_63c0cdf8121619d4e0715c315e2a28d2 /********************************** 
Name: inventory tracker MTD
Function: 
Notes: 

Date Developer Activity 
2021-10-00 Mingyu Create 
2022-03-15 Hannah Add aging live stock by age group
2022-05-19 Sharmaine Add VN 

**********************************/ 


WITH inv AS (
SELECT
DISTINCT grass_region,
shopid,
main_category,
itemid,
sku_id,
is_cb,
sku_status_normal,
fe_stock,
stock_on_hand,
color,
coverage,
period_for_coverage_calculation,
avg_sku_age,
l60_gross_itemsold,
l60_purchasable_days,
coalesce(
try(
l60_gross_itemsold / nullif(l60_purchasable_days, 0)
),
0
) AS sku_l60d_selling_speed
FROM
sbs_mart.shopee_bd_sbs_mart_v2
WHERE
grass_date = current_date - INTERVAL '1' DAY
AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'TW', 'VN')
AND is_cb = 1
),
sku_dim AS(
SELECT
'ID' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_id_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'MY' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_my_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'PH' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_ph_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'TH' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_th_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'SG' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_sg_db__sku_cache_tab__reg_daily_s0_live
UNION
SELECT
'VN' AS grass_region,
sku_id,
try(
1.0000 * cast(length AS BIGINT) * cast(width AS BIGINT) * cast(height AS BIGINT) / 1000000000
) volume_cbm
FROM
wms_mart.shopee_wms_vn_db__sku_cache_tab__reg_daily_s0_live
),
cnt AS (
SELECT
inv.grass_region,
main_category,
count(DISTINCT itemid) AS item_with_stock_cnt,
count(DISTINCT inv.sku_id) AS sku_with_stock_cnt,
sum(
CASE
WHEN avg_sku_age > 180 THEN stock_on_hand
ELSE 0
END
) AS aging_180_stock,
sum(
CASE
WHEN color = 'Black' THEN stock_on_hand
ELSE 0
END
) AS black_stock,
sum(
CASE
WHEN color = 'Black' THEN stock_on_hand * volume_cbm
ELSE 0
END
) AS black_stock_volume,
sum(stock_on_hand) AS total_stock_on_hand,
sum(stock_on_hand * volume_cbm) AS total_stock_volume,
sum(
CASE
WHEN avg_sku_age > 180
AND fe_stock > 0
AND sku_status_normal = 1 THEN stock_on_hand * volume_cbm
ELSE 0
END
) aging_live_stock_volume,
max(l60_purchasable_days) AS l60_purchasable_days,
sum(l60_gross_itemsold) AS l60_gross_itemsold
FROM
inv
LEFT JOIN sku_dim ON inv.grass_region = sku_dim.grass_region
AND inv.sku_id = sku_dim.sku_id
WHERE
stock_on_hand > 0
GROUP BY
1,
2
),
/**
live_sku_split AS (
SELECT
grass_region,
main_category,
count(DISTINCT sku_id) total_live_sku,
count(
DISTINCT CASE
WHEN l60_gross_itemsold > 0 THEN sku_id
ELSE NULL
END
) effective_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age > 180 THEN sku_id
ELSE NULL
END
) aging_180_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 180
AND avg_sku_age > 150 THEN sku_id
ELSE NULL
END
) aging_150_180_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 150
AND avg_sku_age > 120 THEN sku_id
ELSE NULL
END
) aging_120_150_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 120
AND avg_sku_age > 90 THEN sku_id
ELSE NULL
END
) aging_90_120_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 90
AND avg_sku_age > 60 THEN sku_id
ELSE NULL
END
) aging_60_90_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 60
AND avg_sku_age > 30 THEN sku_id
ELSE NULL
END
) aging_30_60_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 30 THEN sku_id
ELSE NULL
END
) aging_0_30_live_sku,
sum(
DISTINCT CASE
WHEN avg_sku_age > 180
THEN stock_on_hand
ELSE 0
END 
) AS aging_180_live_stock,
sum(
DISTINCT CASE
WHEN avg_sku_age <= 180
AND avg_sku_age > 150 THEN stock_on_hand
ELSE 0
END 
) AS aging_150_180_live_stock,
sum(
DISTINCT CASE
WHEN avg_sku_age <= 150
AND avg_sku_age > 120 THEN stock_on_hand
ELSE 0
END 
) AS aging_120_150_live_stock,
sum(
DISTINCT CASE
WHEN avg_sku_age <= 120
AND avg_sku_age > 90 THEN stock_on_hand
ELSE 0
END 
) AS aging_90_120_live_stock,
sum(
DISTINCT CASE
WHEN avg_sku_age <= 90
AND avg_sku_age > 60 THEN stock_on_hand
ELSE 0
END 
) AS aging_60_90_live_stock,
sum(
DISTINCT CASE
WHEN avg_sku_age <= 60
AND avg_sku_age > 30 THEN stock_on_hand
ELSE 0
END 
) AS aging_30_60_live_stock,
sum(
DISTINCT CASE
WHEN avg_sku_age <= 30
THEN stock_on_hand
ELSE 0
END 
) AS aging_0_30_live_stock,
sum(
DISTINCT CASE
WHEN sku_id is not null
THEN stock_on_hand
ELSE 0
END
) total_live_stock
FROM
inv
WHERE 1=1 
--and fe_stock > 0
AND stock_on_hand > 0
--AND sku_status_normal = 1
GROUP BY
1,
2
),
**/
live_sku_split AS (
SELECT
grass_region,
main_category,
count(DISTINCT sku_id) total_live_sku, 
count(
DISTINCT CASE
WHEN l60_gross_itemsold > 0 THEN sku_id
ELSE NULL
END
) effective_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age > 180 THEN sku_id
ELSE NULL
END
) aging_180_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 180
AND avg_sku_age > 150 THEN sku_id
ELSE NULL
END
) aging_150_180_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 150
AND avg_sku_age > 120 THEN sku_id
ELSE NULL
END
) aging_120_150_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 120
AND avg_sku_age > 90 THEN sku_id
ELSE NULL
END
) aging_90_120_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 90
AND avg_sku_age > 60 THEN sku_id
ELSE NULL
END
) aging_60_90_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 60
AND avg_sku_age > 30 THEN sku_id
ELSE NULL
END
) aging_30_60_live_sku,
count(
DISTINCT CASE
WHEN avg_sku_age <= 30 THEN sku_id
ELSE NULL
END
) aging_0_30_live_sku
FROM
inv
WHERE 1=1 
and fe_stock > 0
AND stock_on_hand > 0
AND sku_status_normal = 1
GROUP BY
1,
2
),
sku_qty_cal AS (

SELECT
grass_region,
main_category, 
sum(
CASE
WHEN avg_sku_age > 180
THEN stock_on_hand
ELSE 0
END 
) AS aging_180_stock,
sum(
CASE
WHEN avg_sku_age <= 180
AND avg_sku_age > 150 THEN stock_on_hand
ELSE 0
END 
) AS aging_150_180_stock,
sum(
CASE
WHEN avg_sku_age <= 150
AND avg_sku_age > 120 THEN stock_on_hand
ELSE 0
END 
) AS aging_120_150_stock,
sum(
CASE
WHEN avg_sku_age <= 120
AND avg_sku_age > 90 THEN stock_on_hand
ELSE 0
END 
) AS aging_90_120_stock,
sum(
CASE
WHEN avg_sku_age <= 90
AND avg_sku_age > 60 THEN stock_on_hand
ELSE 0
END 
) AS aging_60_90_stock,
sum(
CASE
WHEN avg_sku_age <= 60
AND avg_sku_age > 30 THEN stock_on_hand
ELSE 0
END 
) AS aging_30_60_stock,
sum(
CASE
WHEN avg_sku_age <= 30
THEN stock_on_hand
ELSE 0
END 
) AS aging_0_30_stock,
sum( 
stock_on_hand

) total_sku_stock
FROM
inv
WHERE 1=1
-- Hannah: fe_stock / sku_status_normal comment out AND change the name
--and fe_stock > 0, separate
AND stock_on_hand > 0
--AND sku_status_normal = 1
GROUP BY
1,
2


),


live_sku AS (
SELECT
DISTINCT grass_region,
shopid,
main_category,
sku_id
FROM
inv
WHERE
fe_stock > 0
AND stock_on_hand > 0
AND sku_status_normal = 1
),
replenish AS (
SELECT
i.grass_region,
i.main_category,
count(DISTINCT i.sku_id) to_replenish
FROM
(
SELECT
DISTINCT grass_region,
shopid,
main_category,
sku_id
FROM
inv
WHERE
fe_stock = 0
AND sku_status_normal = 1
) i
JOIN (
SELECT
grass_region,
concat(
cast(item_id AS VARCHAR),
'_',
cast(model_id AS VARCHAR)
) sku_id,
sum(order_fraction) l60d_orders
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'TW', 'VN')
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND tz_type = 'local'
AND grass_date BETWEEN date_add('day', -60, current_date - INTERVAL '1' DAY)
AND date_add('day', -1, current_date - INTERVAL '1' DAY)
AND is_placed = 1
AND is_cb_shop = 1
GROUP BY
1,
2
) o ON i.grass_region = o.grass_region
AND i.sku_id = o.sku_id
LEFT JOIN live_sku l ON i.grass_region = l.grass_region
AND i.sku_id = l.sku_id
WHERE
l.sku_id IS NULL
GROUP BY
1,
2
),
outbound AS (
SELECT
b.grass_region,
c.category_id_l1,
sum(packed_count) outbound_qty,
sum(
CASE
WHEN out_date = current_date - INTERVAL '1' DAY THEN packed_count
ELSE 0
END
) d1_outbound_qty
FROM
(
SELECT
DISTINCT a.grass_region,
sku_id,
packed_count,
date(from_unixtime(a.mtime)) out_date
FROM
regcbbi_general.shopee_wms_db__order_tab a
JOIN wms_mart.shopee_wms_db__order_sku_list_tab__reg_daily_s0_live b ON b.order_id = a.order_id
WHERE
a.status = 8
AND a.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN')
AND date(from_unixtime(a.mtime)) BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY)
AND current_date - INTERVAL '1' DAY
UNION
ALL
SELECT
substring(a.whs_id, 1, 2) grass_region,
sku_id,
actual_quantity,
date(from_unixtime(a.mtime)) out_date
FROM
regcbbi_general.shopee_wms_db__sales_outbound_order_v2_tab a
JOIN regcbbi_general.shopee_wms_db__sales_outbound_sku_v2_tab b ON b.order_number = a.order_number
WHERE
a.order_status = 8
AND substring(a.whs_id, 1, 2) IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN')
AND date(from_unixtime(a.mtime)) BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY)
AND current_date - INTERVAL '1' DAY
) b
JOIN (
SELECT
DISTINCT country,
category_id_l1,
sku_id
FROM
sbs_mart.shopee_scbs_db__sku_tab__reg_daily_s0_live
WHERE
cb_option = 1
AND country IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN')
) c ON b.grass_region = c.country
AND b.sku_id = c.sku_id
GROUP BY
1,
2
),
out_summary AS (
SELECT
DISTINCT grass_region,
main_name AS main_category,
try(
1.0000 * outbound_qty / DAY(current_date - INTERVAL '1' DAY)
) AS avg_outbound_qty,
d1_outbound_qty
FROM
outbound a
JOIN (
SELECT
main_cat,
main_name,
status
FROM
mp_item.dim_category__reg_s0_live
WHERE
grass_region in ('ID', 'MY', 'PH', 'TH', 'SG', 'TW', 'VN')
) b ON a.category_id_l1 = b.main_cat
WHERE
STATUS = 1
),
inbound AS (
SELECT
country,
category_id_l1,
sum(putaway_count) mtd_total_inbound_qty
FROM
(
SELECT
sheet_id,
sku_id,
sum(recv_count) putaway_count,
date(from_unixtime(max(mtime))) last_putaway_date
FROM
regcbbi_general.shopee_wms_db__putaway_sku_v2_tab
GROUP BY
1,
2
UNION
ALL
SELECT
po_inbound_id,
sku_id,
sum(putaway_count) putaway_count,
date(from_unixtime(max(last_putaway_time))) last_putaway_date
FROM
regcbbi_general.shopee_wms_db__po_inbound_sku_list_tab
GROUP BY
1,
2
) b
JOIN (
SELECT
DISTINCT country,
category_id_l1,
sku_id
FROM
sbs_mart.shopee_scbs_db__sku_tab__reg_daily_s0_live
WHERE
cb_option = 1
AND country IN ('ID', 'MY', 'PH', 'TH', 'SG', 'TW', 'VN')
) c ON b.sku_id = c.sku_id
WHERE
putaway_count > 0
AND last_putaway_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY)
AND current_date - INTERVAL '1' DAY
GROUP BY
1,
2
),
in_summary AS (
SELECT
DISTINCT a.country AS grass_region,
main_name AS main_category,
mtd_total_inbound_qty
FROM
inbound a
JOIN (
SELECT
main_cat,
main_name,
status
FROM
mp_item.dim_category__reg_s0_live
WHERE
grass_region in ('ID', 'MY', 'PH', 'TH', 'SG', 'TW', 'VN')
) b ON a.category_id_l1 = b.main_cat
WHERE
STATUS = 1
),
--##########################
--To add ST ATP
--##########################
sku_order AS (
SELECT
grass_region,
shop_id,
item_id,
model_id,
sum(order_fraction) AS orders
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
grass_region IN ('ID', 'SG', 'PH', 'TH', 'MY', 'VN')
AND grass_date BETWEEN current_date - INTERVAL '30' DAY
AND current_date - INTERVAL '1' DAY
AND tz_type = 'local'
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND is_placed = 1
AND is_cb_shop = 1
GROUP BY
1,
2,
3,
4
),
overall_order AS (
SELECT
grass_region,
count(DISTINCT order_id) AS wh_ado
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
grass_region IN ('ID', 'SG', 'PH', 'TH', 'MY', 'VN')
AND grass_date BETWEEN current_date - INTERVAL '30' DAY
AND current_date - INTERVAL '1' DAY
AND tz_type = 'local'
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND is_placed = 1
AND is_cb_shop = 1
GROUP BY
1
),
ST_sku AS (
-- items which order contribution to the whole region reaches the top 80%
SELECT
DISTINCT grass_region,
shop_id,
item_id,
model_id,
concat(
cast(item_id AS varchar),
'_',
cast(model_id AS varchar)
) AS sku_id
FROM
(
SELECT
i.*,
sum(orders / wh_ado) over(
PARTITION by i.grass_region
ORDER BY
orders DESC
) AS ado_contri
FROM
sku_order i
LEFT JOIN overall_order o ON i.grass_region = o.grass_region
)
WHERE
ado_contri <= 0.8
),
live_sku_ST AS (
SELECT
a.grass_region,
a.main_category,
count(DISTINCT a.sku_id) live_skuid_st
FROM
live_sku a
INNER JOIN ST_sku b ON a.sku_id = b.sku_id
AND a.grass_region = b.grass_region
GROUP BY
1,
2
),
replenish_ST AS (
SELECT
i.grass_region,
i.main_category,
count(DISTINCT i.sku_id) to_replenish_st
FROM
(
SELECT
DISTINCT inv.grass_region,
inv.shopid,
inv.main_category,
inv.sku_id
FROM
inv
INNER JOIN ST_sku ON inv.sku_id = ST_sku.sku_id
WHERE
fe_stock = 0
AND sku_status_normal = 1
) i
JOIN (
SELECT
grass_region,
concat(
cast(item_id AS VARCHAR),
'_',
cast(model_id AS VARCHAR)
) sku_id,
sum(order_fraction) l60d_orders
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'TW', 'VN')
AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
AND tz_type = 'local'
AND grass_date BETWEEN date_add('day', -60, current_date - INTERVAL '1' DAY)
AND date_add('day', -1, current_date - INTERVAL '1' DAY)
AND is_placed = 1
AND is_cb_shop = 1
GROUP BY
1,
2
) o ON i.grass_region = o.grass_region
AND i.sku_id = o.sku_id
LEFT JOIN live_sku l ON i.grass_region = l.grass_region
AND i.sku_id = l.sku_id
WHERE
l.sku_id IS NULL
GROUP BY
1,
2
)
SELECT
a.grass_region,
a.main_category,
a.item_with_stock_cnt,
a.sku_with_stock_cnt,
a.total_stock_on_hand,
try(
1.0000 * a.l60_gross_itemsold / nullif(a.l60_purchasable_days, 0)
) AS l60d_selling_speed,
try(
1.0000 * a.total_stock_on_hand * a.l60_purchasable_days / nullif(a.l60_gross_itemsold, 0)
) AS coverage_days_l60d_speed,
b.effective_live_sku,
try(
1.0000 * b.effective_live_sku / nullif(b.total_live_sku, 0)
) AS effective_sku_pctg,
a.aging_180_stock,
try(
1.0000 * a.aging_180_stock / nullif(a.total_stock_on_hand, 0)
) AS aging_180_stock_pctg,
try(
1.0000 * coalesce(b.total_live_sku, 0) / nullif(
coalesce(b.total_live_sku, 0) + coalesce(d.to_replenish, 0),
0
)
) ATP,
d.to_replenish,
aging_0_30_live_sku,
aging_30_60_live_sku,
aging_60_90_live_sku,
aging_90_120_live_sku,
aging_120_150_live_sku,
aging_150_180_live_sku,
aging_180_live_sku,
total_live_sku,
black_stock,
black_stock_volume,
total_stock_volume,
avg_outbound_qty,
d1_outbound_qty,
mtd_total_inbound_qty,
aging_live_stock_volume,
try(
1.0000 * coalesce(st_m.live_skuid_st, 0) / nullif(
coalesce(st_m.live_skuid_st, 0) + coalesce(st_r.to_replenish_st, 0),
0
)
) ST_ATP,
0 AS ranks,
aging_0_30_stock,
aging_30_60_stock,
aging_60_90_stock,
aging_90_120_stock,
aging_120_150_stock,
aging_150_180_stock,
c.aging_180_stock aging_180_stock_1,
total_sku_stock
FROM
cnt a
LEFT JOIN live_sku_split b ON a.grass_region = b.grass_region
AND a.main_category = b.main_category
LEFT JOIN replenish d ON a.grass_region = d.grass_region
AND a.main_category = d.main_category
LEFT JOIN out_summary e ON a.grass_region = e.grass_region
AND a.main_category = e.main_category
LEFT JOIN in_summary f ON a.grass_region = f.grass_region
AND a.main_category = f.main_category
LEFT JOIN live_sku_ST st_m ON a.grass_region = st_m.grass_region
AND a.main_category = st_m.main_category
LEFT JOIN replenish_ST st_r ON a.grass_region = st_r.grass_region
AND a.main_category = st_r.main_category
left join sku_qty_cal c
ON a.grass_region = c.grass_region
AND a.main_category = c.main_category
UNION
SELECT
a.grass_region,
'Total' col,
sum(item_with_stock_cnt) item_with_stock_cnt,
sum(sku_with_stock_cnt) sku_with_stock_cnt,
sum(total_stock_on_hand) total_stock_on_hand,
try(
1.0000 * sum(l60_gross_itemsold) / max(l60_purchasable_days)
) l60d_selling_speed,
try(
1.0000 * sum(total_stock_on_hand) * max(l60_purchasable_days) / sum(l60_gross_itemsold)
) coverage_days_l60d_speed,
sum(effective_live_sku) effective_live_sku,
try(
1.0000 * sum(effective_live_sku) / sum(total_live_sku)
) effective_sku_pctg,
sum(a.aging_180_stock) aging_180_stock,
try(
1.0000 * sum(a.aging_180_stock) / sum(total_stock_on_hand)
) aging_180_stock_pctg,
try(
1.0000 * coalesce(sum(b.total_live_sku), 0) / nullif(
coalesce(sum(b.total_live_sku), 0) + coalesce(sum(d.to_replenish), 0),
0
)
) ATP,
sum(d.to_replenish) to_replenish,
sum(aging_0_30_live_sku) aging_0_30_live_sku,
sum(aging_30_60_live_sku) aging_30_60_live_sku,
sum(aging_60_90_live_sku) aging_60_90_live_sku,
sum(aging_90_120_live_sku) aging_90_120_live_sku,
sum(aging_120_150_live_sku) aging_120_150_live_sku,
sum(aging_150_180_live_sku) aging_150_180_live_sku,
sum(aging_180_live_sku) aging_180_live_sku,
sum(total_live_sku) total_live_sku,
sum(black_stock) black_stock,
sum(black_stock_volume) black_stock_volume,
sum(total_stock_volume) total_stock_volume,
sum(avg_outbound_qty) avg_outbound_qty,
sum(d1_outbound_qty) d1_outbound_qty,
sum(mtd_total_inbound_qty) mtd_total_inbound_qty,
sum(aging_live_stock_volume) aging_live_stock_volume,
try(
1.0000 * coalesce(sum(st_m.live_skuid_st), 0) / nullif(
coalesce(sum(st_m.live_skuid_st), 0) + coalesce(sum(st_r.to_replenish_st), 0),
0
)
) ST_ATP,
1 AS ranks,
sum(aging_0_30_stock) aging_0_30_stock,
sum(aging_30_60_stock) aging_30_60_stock,
sum(aging_60_90_stock) aging_60_90_stock,
sum(aging_90_120_stock) aging_90_120_stock,
sum(aging_120_150_stock) aging_120_150_stock,
sum(aging_150_180_stock) aging_150_180_stock,
sum(c.aging_180_stock) aging_180_stock_1,
sum(total_sku_stock) total_sku_stock
FROM
cnt a
LEFT JOIN live_sku_split b ON a.grass_region = b.grass_region
AND a.main_category = b.main_category
LEFT JOIN replenish d ON a.grass_region = d.grass_region
AND a.main_category = d.main_category
LEFT JOIN out_summary e ON a.grass_region = e.grass_region
AND a.main_category = e.main_category
LEFT JOIN in_summary f ON a.grass_region = f.grass_region
AND a.main_category = f.main_category
LEFT JOIN live_sku_ST st_m ON a.grass_region = st_m.grass_region
AND a.main_category = st_m.main_category
LEFT JOIN replenish_ST st_r ON a.grass_region = st_r.grass_region
AND a.main_category = st_r.main_category
left join sku_qty_cal c
ON a.grass_region = c.grass_region
AND a.main_category = c.main_category


GROUP BY
1,
2
ORDER BY
1,
ranks DESC,
2