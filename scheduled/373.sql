insert into nexus.test_5658ea944910e843112289708ed77a5d94537b979932adfadee9c2470e4e039f_0b31fa8ebc79e8910e00153b550fa1dc WITH
sip AS (
SELECT t1.country as P_country
, t2.country as A_country
, affi_shopid AS shopid
, t1.cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1 
JOIN
(SELECT distinct affi_shopid, affi_userid, affi_username, mst_shopid, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE grass_region != 'FR' AND sip_shop_status != 3 ) t2 on t1.shopid=t2.mst_shopid
WHERE t1.country != 'FR'
AND t2.country != 'FR'
)
, seller_index AS (
SELECT si.grass_region
, CAST(si.child_shopid as BIGINT) as shop_id
, COALESCE(si.ggp_account_name, si.gp_account_name) AS ggp_or_gp
, si.gp_account_id
, si.gp_account_owner
, si.child_account_owner
, si.child_account_name
, si.gp_account_owner_userrole_name
, si.child_account_owner_userrole_name
, CASE WHEN sip.shopid is not NULL THEN 1 ELSE 0 END as is_sip
, CASE WHEN cbwh.shop_id IS NOT NULL THEN 'CBWH'
WHEN sip.shopid IS NOT NULL AND sip.cb_option = 0 THEN 'Local SIP'
WHEN sip.shopid IS NOT NULL AND sip.p_country IN ('TW') THEN 'TB SIP' --remove this line for Local/CB dichotomy
WHEN sip.shopid IS NOT NULL AND si.gp_account_billing_country = 'Korea' AND sip.p_country IN ('SG') THEN 'KR SIP' --remove this line for Local/CB dichotomy
WHEN sip.shopid IS NOT NULL THEN 'CB SIP'
when si.gp_account_billing_country = 'Korea' then 'KRCB'
when si.gp_account_billing_country = 'Japan' then 'JPCB'
when si.gp_account_billing_country is NULL or si.gp_account_billing_country != 'China' then 'Others'
when si.child_account_owner_userrole_name like '%HKCB%' then 'HKCB'
when si.child_account_owner_userrole_name like '%BD_%' then 'CNCB BD'
when si.child_account_owner_userrole_name like '%UST_%' then 'CNCB UST'
when si.child_account_owner_userrole_name like '%ST_%' THEN 'CNCB ST'
when si.child_account_owner_userrole_name like '%MT_%' THEN 'CNCB MT'
when si.child_account_owner_userrole_name like '%LT%' THEN 'CNCB LT'
WHEN si.gp_account_billing_country = 'China' THEN COALESCE(CONCAT('CNCB ', si.cb_group), 'CNCB Others')
else 'CNCB Others' END as seller_type
, gp_account_billing_country
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column AS si 
LEFT JOIN sip ON CAST(si.child_shopid AS BIGINT) = sip.shopid
LEFT JOIN (
SELECT DISTINCT shop_id
FROM regcbbi_general.cbwh_shop_history_v2
WHERE grass_date = DATE_ADD('day', -2, current_date)
) AS cbwh ON cbwh.shop_id = CAST(si.child_shopid as BIGINT)
WHERE si.grass_region IN ('BR', 'CL', 'CO', 'ES', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
AND si.gp_account_billing_country = 'China'
)
, o AS (
SELECT shopid
, "count"(DISTINCT (CASE WHEN IF((grass_region = 'TW'), (extinfo.cancel_reason IN (1, 3)), (extinfo.cancel_reason IN (1, 3, 4))) THEN orderid ELSE null END)) seller_cancellations
, "count"(DISTINCT (CASE WHEN IF((grass_region = 'TW'), (extinfo.cancel_reason IN (200, 201, 204, 302, 303, 303)), (extinfo.cancel_reason IN (204, 301, 302, 303))) THEN orderid ELSE null END)) system_cancellations
FROM marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE (
(((CASE WHEN (grass_region IN ('MX') ) THEN "date"("from_unixtime"((create_time - (14 * 3600)))) 
WHEN (grass_region = 'CO') THEN "date"("from_unixtime"((create_time - (13 * 3600)))) 
WHEN (grass_region = 'BR') THEN "date"("from_unixtime"((create_time - (11 * 3600)))) 
WHEN (grass_region = 'CL') THEN "date"("from_unixtime"((create_time - (12 * 3600))))
WHEN (grass_region IN ('ID', 'TH', 'VN')) THEN "date"("from_unixtime"((create_time - 3600))) 
WHEN (grass_region IN ('ES', 'FR', 'PL')) THEN "date"("from_unixtime"((create_time - (7 * 3600))))
ELSE "date"("from_unixtime"(create_time)) END) BETWEEN "date"('2022-06-03') AND "date"('2022-06-09')) AND (cb_option = 1)) AND (grass_region <> '')
)
AND grass_region != 'FR'
GROUP BY 1
) 


SELECT --current_date AS flagged_date
--,
grass_region
, cb.shop_id
, child_account_name
, ggp_or_gp
, gp_account_owner
--, 'CN' AS seller_market
, seller_type
, seller_cancellations
, system_cancellations
, (seller_cancellations + system_cancellations) total_cancellations
FROM seller_index AS cb
INNER JOIN o ON cb.shop_id = o.shopid
ORDER BY 9 DESC -- 11 DESC
LIMIT 100