insert into nexus.test_925255a22d4617e0def5a7f036891fbd1b1f0f91eb44b7bd81a08efb9417988c_bc54517cd5fb0b39d906c4791575e3b5 with map as (
select distinct feature_group,feature, grass_date as dt
from mp_foa.ads_feature_lead_1d__reg_s0_live
where grass_region='TW' and grass_date between date_add('month',-1,date_trunc('month',date_add('day',-1,current_date))) and date_add('day',-1,current_date) and (feature_group='Others' or feature!='Others') and impress_cnt_item_direct_lead_1d>0 
)



, cb_item as (
select distinct item_id, is_cb_shop
from
(select item_id, is_cb_shop,level1_category from mp_item.dim_item__reg_s0_live where grass_region='TW' and grass_date=date_add('day',-1,current_date) and tz_type='local') a 
)


, imp as ( 
select date_add('day',-1,current_date) as grass_date, feature_group
, sum(case when a.grass_date = date_add('day',-1,current_date) and is_cb_shop=1 then impress_cnt_direct_lead end) as cb_impress_cnt_direct_lead
, sum(case when a.grass_date = date_add('day',-1,current_date) and is_cb_shop=0 then impress_cnt_direct_lead end) as lc_impress_cnt_direct_lead 
, sum(case when a.grass_date >= date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=1 then impress_cnt_direct_lead end)*1.0000/day_of_week("date_add"('day', -1, current_date)) as cb_impress_wtd
, sum(case when a.grass_date >= date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=0 then impress_cnt_direct_lead end)*1.0000/day_of_week("date_add"('day', -1, current_date)) as lc_impress_wtd 
, sum(case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval'7'day and date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=1 then impress_cnt_direct_lead end)*1.0000/7 as cb_impress_lw
, sum(case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval'7'day and date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=0 then impress_cnt_direct_lead end)*1.0000/7 as lc_impress_lw
, sum(case when grass_date >= date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=1 then impress_cnt_direct_lead end)*1.0000/day("date_add"('day', -1, current_date)) as cb_impress_mtd
, sum(case when grass_date >= date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=0 then impress_cnt_direct_lead end)*1.0000/day("date_add"('day', -1, current_date)) as lc_impress_mtd 
, sum(case when grass_date < date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=1 then impress_cnt_direct_lead end)*1.0000/day(date_trunc('month',date_add('day',-1,current_date)) - interval'1'day) as cb_impress_lm
, sum(case when grass_date < date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=0 then impress_cnt_direct_lead end)*1.0000/day(date_trunc('month',date_add('day',-1,current_date)) - interval'1'day) as lc_impress_lm

from
(select grass_date,step1_feature, impress_cnt_direct_lead, item_id
from mp_foa.dwd_eventid_impress_di__reg_s0_live 
where grass_region='TW' and grass_date between date_add('month',-1,date_trunc('month',date_add('day',-1,current_date))) and date_add('day',-1,current_date) and tz_type='local' ) a 
join map p on p.feature=a.step1_feature and a.grass_date=p.dt
join cb_item b on a.item_id=b.item_id
group by 1,2
)


,pdp_v as ( 
select date_add('day',-1,current_date) as grass_date,feature_group
, sum(case when grass_date = date_add('day',-1,current_date) and is_cb_shop=1 then pv_direct_lead end) as cb_pv_direct_lead
, sum(case when grass_date = date_add('day',-1,current_date) and is_cb_shop=0 then pv_direct_lead end) as lc_pv_direct_lead 
, sum(case when grass_date >= date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=1 then pv_direct_lead end)*1.0000/day_of_week("date_add"('day', -1, current_date)) as cb_pv_wtd
, sum(case when grass_date >= date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=0 then pv_direct_lead end)*1.0000/day_of_week("date_add"('day', -1, current_date)) as lc_pv_wtd 
, sum(case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval'7'day and date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=1 then pv_direct_lead end)*1.0000/7 as cb_pv_lw
, sum(case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval'7'day and date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=0 then pv_direct_lead end)*1.0000/7 as lc_pv_lw
, sum(case when grass_date >= date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=1 then pv_direct_lead end)*1.0000/day("date_add"('day', -1, current_date)) as cb_pv_mtd
, sum(case when grass_date >= date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=0 then pv_direct_lead end)*1.0000/day("date_add"('day', -1, current_date)) as lc_pv_mtd 
, sum(case when grass_date < date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=1 then pv_direct_lead end)*1.0000/day(date_trunc('month',date_add('day',-1,current_date)) - interval'1'day) as cb_pv_lm
, sum(case when grass_date < date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=0 then pv_direct_lead end)*1.0000/day(date_trunc('month',date_add('day',-1,current_date)) - interval'1'day) as lc_pv_lm


from
(select grass_date,step1_feature, pv_direct_lead,item_id
from mp_foa.dwd_eventid_view_di__reg_s0_live a 
where grass_region='TW' and grass_date between date_add('month',-1,date_trunc('month',date_add('day',-1,current_date))) and date_add('day',-1,current_date) and tz_type='local' and page_type='product' ) a 
join cb_item b on a.item_id=b.item_id
join map p on p.feature=a.step1_feature and a.grass_date=p.dt
group by 1,2
)


, ord as ( 
select date_add('day',-1,current_date) as grass_date, feature_group
, sum(case when grass_date = date_add('day',-1,current_date) and is_cb_shop=1 then place_order_cnt_direct_lead end) as cb_order_direct_lead
, sum(case when grass_date = date_add('day',-1,current_date) and is_cb_shop=0 then place_order_cnt_direct_lead end) as lc_order_direct_lead
, sum(case when grass_date >= date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=1 then place_order_cnt_direct_lead end)*1.0000/day_of_week("date_add"('day', -1, current_date)) as cb_order_wtd
, sum(case when grass_date >= date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=0 then place_order_cnt_direct_lead end)*1.0000/day_of_week("date_add"('day', -1, current_date)) as lc_order_wtd 
, sum(case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval'7'day and date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=1 then place_order_cnt_direct_lead end)*1.0000/7 as cb_order_lw
, sum(case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval'7'day and date_trunc('week',date_add('day',-1,current_date)) and is_cb_shop=0 then place_order_cnt_direct_lead end)*1.0000/7 as lc_order_lw
, sum(case when grass_date >= date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=1 then place_order_cnt_direct_lead end)*1.0000/day("date_add"('day', -1, current_date)) as cb_order_mtd
, sum(case when grass_date >= date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=0 then place_order_cnt_direct_lead end)*1.0000/day("date_add"('day', -1, current_date)) as lc_order_mtd 
, sum(case when grass_date < date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=1 then place_order_cnt_direct_lead end)*1.0000/day(date_trunc('month',date_add('day',-1,current_date)) - interval'1'day) as cb_order_lm
, sum(case when grass_date < date_trunc('month',date_add('day',-1,current_date)) and is_cb_shop=0 then place_order_cnt_direct_lead end)*1.0000/day(date_trunc('month',date_add('day',-1,current_date)) - interval'1'day) as lc_order_lm
from
(select grass_date,step1_feature, place_order_cnt_direct_lead,item_id
from mp_foa.dwd_eventid_item_order_events_di__reg_s0_live a 
where grass_region='TW' and grass_date between date_add('month',-1,date_trunc('month',date_add('day',-1,current_date))) and date_add('day',-1,current_date) ) a 
join cb_item b on a.item_id=b.item_id
join map p on p.feature=a.step1_feature and a.grass_date=p.dt
group by 1,2
)


(select p.grass_date, p.feature_group
--, cb_impress_cnt_direct_lead,lc_impress_cnt_direct_lead, cb_pv_direct_lead, lc_pv_direct_lead, cb_order_direct_lead,lc_order_direct_lead
,cb_impress_wtd,lc_impress_wtd, cb_pv_wtd, lc_pv_wtd, cb_order_wtd,lc_order_wtd
, cb_pv_wtd*1.0000/cb_impress_wtd as cb_ctr, (cb_pv_wtd*1.0000/cb_impress_wtd)/((lc_pv_wtd+cb_pv_wtd)*1.0000/(lc_impress_wtd+cb_impress_wtd)) as cb_ctr_pene, cb_order_wtd*1.0000/cb_pv_wtd as cb_cr, (cb_order_wtd*1.0000/cb_pv_wtd)/((lc_order_wtd+cb_order_wtd)*1.0000/(lc_pv_wtd+cb_pv_wtd)) as cb_cr_pene
, cb_impress_wtd*1.0000/ sum(cb_impress_wtd+lc_impress_wtd) over (partition by p.grass_date ) - cb_impress_lw*1.0000/ sum(cb_impress_lw+lc_impress_lw) over (partition by p.grass_date) as cb_impress_pct_wow
, cb_impress_wtd*1.0000/(lc_impress_wtd+cb_impress_wtd) - cb_impress_lw*1.0000/(lc_impress_lw+cb_impress_lw) as cb_impress_pene_wow
, (cb_pv_wtd*1.0000/cb_impress_wtd)/((lc_pv_wtd+cb_pv_wtd)*1.0000/(lc_impress_wtd+cb_impress_wtd)) - (cb_pv_lw*1.0000/cb_impress_lw)/((lc_pv_lw+cb_pv_lw)*1.0000/(lc_impress_lw+cb_impress_lw)) as cb_ctr_pene_wow
, (cb_order_wtd*1.0000/cb_pv_wtd)/((lc_order_wtd+cb_order_wtd)*1.0000/(lc_pv_wtd+cb_pv_wtd)) - (cb_order_lw*1.0000/cb_pv_lw)/((lc_order_lw+cb_order_lw)*1.0000/(lc_pv_lw+cb_pv_lw)) as cb_cr_pene_wow
, cb_order_wtd*1.0000/(lc_order_wtd+cb_order_wtd) - cb_order_lw*1.0000/(lc_order_lw+cb_order_lw) as cb_ord_pene_wow
, cb_impress_mtd*1.0000/ sum(cb_impress_mtd+lc_impress_mtd) over (partition by p.grass_date ) - cb_impress_lm*1.0000/ sum(cb_impress_lm+lc_impress_lm) over (partition by p.grass_date) as cb_impress_pct_mom
, cb_impress_mtd*1.0000/(lc_impress_mtd+cb_impress_mtd) - cb_impress_lm*1.0000/(lc_impress_lm+cb_impress_lm) as cb_impress_pene_mom
, (cb_pv_mtd*1.0000/cb_impress_mtd)/((lc_pv_mtd+cb_pv_mtd)*1.0000/(lc_impress_mtd+cb_impress_mtd)) - (cb_pv_lm*1.0000/cb_impress_lm)/((lc_pv_lm+cb_pv_lm)*1.0000/(lc_impress_lm+cb_impress_lm)) as cb_ctr_pene_mom
, (cb_order_mtd*1.0000/cb_pv_mtd)/((lc_order_mtd+cb_order_mtd)*1.0000/(lc_pv_mtd+cb_pv_mtd)) - (cb_order_lm*1.0000/cb_pv_lm)/((lc_order_lm+cb_order_lm)*1.0000/(lc_pv_lm+cb_pv_lm)) as cb_cr_pene_mom
, cb_order_mtd*1.0000/(lc_order_mtd+cb_order_mtd) - cb_order_lm*1.0000/(lc_order_lm+cb_order_lm) as cb_ord_pene_mom
,(cb_impress_lw+lc_impress_lw) as tl_impress_lw,(cb_impress_mtd+lc_impress_mtd) as tl_impress_mtd, (cb_impress_lm+lc_impress_lm) as tl_impress_lm

, cb_impress_mtd*1.0000/(lc_impress_mtd+cb_impress_mtd) as cb_impress_pene_mtd
, (cb_pv_mtd*1.0000/cb_impress_mtd)/((lc_pv_mtd+cb_pv_mtd)*1.0000/(lc_impress_mtd+cb_impress_mtd)) as cb_ctr_pene_mtd
, (cb_order_mtd*1.0000/cb_pv_mtd)/((lc_order_mtd+cb_order_mtd)*1.0000/(lc_pv_mtd+cb_pv_mtd)) as cb_cr_pene_mtd , cb_order_mtd*1.0000/(lc_order_mtd+cb_order_mtd) as cb_ord_pene_mtd 
-- , p.cluster, cb_impress_mtd, (cb_pv_wtd+lc_pv_wtd)*1.0000/(lc_impress_wtd+lc_impress_wtd) as tl_ctr,(cb_order_wtd+lc_order_wtd)*1.0000/(cb_pv_wtd+lc_pv_wtd) as tl_cr
from imp p
left join pdp_v v on p.feature_group=v.feature_group --and p.cluster=v.cluster
left join ord o on p.feature_group=o.feature_group --and p.cluster=o.cluster 
)
--where cb_impress_cnt_direct_lead>0 
UNION All
(
select p.grass_date, 'All' as feature_group
-- , cb_impress_cnt_direct_lead,lc_impress_cnt_direct_lead, cb_pv_direct_lead, lc_pv_direct_lead, cb_order_direct_lead,lc_order_direct_lead
, cb_impress_wtd,lc_impress_wtd, cb_pv_wtd, lc_pv_wtd, cb_order_wtd,lc_order_wtd
, cb_pv_wtd*1.0000/cb_impress_wtd as cb_ctr, (cb_pv_wtd*1.0000/cb_impress_wtd)/((lc_pv_wtd+cb_pv_wtd)*1.0000/(lc_impress_wtd+cb_impress_wtd)) as cb_ctr_pene, cb_order_wtd*1.0000/cb_pv_wtd as cb_cr, (cb_order_wtd*1.0000/cb_pv_wtd)/((lc_order_wtd+cb_order_wtd)*1.0000/(lc_pv_wtd+cb_pv_wtd)) as cb_cr_pene
, cb_impress_wtd*1.0000/ sum(cb_impress_wtd+lc_impress_wtd) over (partition by p.grass_date ) - cb_impress_lw*1.0000/ sum(cb_impress_lw+lc_impress_lw) over (partition by p.grass_date) as cb_impress_pct_wow
, cb_impress_wtd*1.0000/(lc_impress_wtd+cb_impress_wtd) - cb_impress_lw*1.0000/(lc_impress_lw+cb_impress_lw) as cb_impress_pene_wow
, (cb_pv_wtd*1.0000/cb_impress_wtd)/((lc_pv_wtd+cb_pv_wtd)*1.0000/(lc_impress_wtd+cb_impress_wtd)) - (cb_pv_lw*1.0000/cb_impress_lw)/((lc_pv_lw+cb_pv_lw)*1.0000/(lc_impress_lw+cb_impress_lw)) as cb_ctr_pene_wow
, (cb_order_wtd*1.0000/cb_pv_wtd)/((lc_order_wtd+cb_order_wtd)*1.0000/(lc_pv_wtd+cb_pv_wtd)) - (cb_order_lw*1.0000/cb_pv_lw)/((lc_order_lw+cb_order_lw)*1.0000/(lc_pv_lw+cb_pv_lw)) as cb_cr_pene_wow
, cb_order_wtd*1.0000/(lc_order_wtd+cb_order_wtd) - cb_order_lw*1.0000/(lc_order_lw+cb_order_lw) as cb_ord_pene_wow
, cb_impress_mtd*1.0000/ sum(cb_impress_mtd+lc_impress_mtd) over (partition by p.grass_date ) - cb_impress_lm*1.0000/ sum(cb_impress_lm+lc_impress_lm) over (partition by p.grass_date) as cb_impress_pct_mom
, cb_impress_mtd*1.0000/(lc_impress_mtd+cb_impress_mtd) - cb_impress_lm*1.0000/(lc_impress_lm+cb_impress_lm) as cb_impress_pene_mom
, (cb_pv_mtd*1.0000/cb_impress_mtd)/((lc_pv_mtd+cb_pv_mtd)*1.0000/(lc_impress_mtd+cb_impress_mtd)) - (cb_pv_lm*1.0000/cb_impress_lm)/((lc_pv_lm+cb_pv_lm)*1.0000/(lc_impress_lm+cb_impress_lm)) as cb_ctr_pene_mom
, (cb_order_mtd*1.0000/cb_pv_mtd)/((lc_order_mtd+cb_order_mtd)*1.0000/(lc_pv_mtd+cb_pv_mtd)) - (cb_order_lm*1.0000/cb_pv_lm)/((lc_order_lm+cb_order_lm)*1.0000/(lc_pv_lm+cb_pv_lm)) as cb_cr_pene_mom
, cb_order_mtd*1.0000/(lc_order_mtd+cb_order_mtd) - cb_order_lm*1.0000/(lc_order_lm+cb_order_lm) as cb_ord_pene_mom
,(cb_impress_lw+lc_impress_lw) as tl_impress_lw,(cb_impress_mtd+lc_impress_mtd) as tl_impress_mtd, (cb_impress_lm+lc_impress_lm) as tl_impress_lm

, cb_impress_mtd*1.0000/(lc_impress_mtd+cb_impress_mtd) as cb_impress_pene_mtd
, (cb_pv_mtd*1.0000/cb_impress_mtd)/((lc_pv_mtd+cb_pv_mtd)*1.0000/(lc_impress_mtd+cb_impress_mtd)) as cb_ctr_pene_mtd
, (cb_order_mtd*1.0000/cb_pv_mtd)/((lc_order_mtd+cb_order_mtd)*1.0000/(lc_pv_mtd+cb_pv_mtd)) as cb_cr_pene_mtd , cb_order_mtd*1.0000/(lc_order_mtd+cb_order_mtd) as cb_ord_pene_mtd 
-- , 'all' as cluster, cb_impress_mtd, (cb_pv_wtd+lc_pv_wtd)*1.0000/(lc_impress_wtd+lc_impress_wtd) as tl_ctr,(cb_order_wtd+lc_order_wtd)*1.0000/(cb_pv_wtd+lc_pv_wtd) as tl_cr
from (select grass_date, sum(cb_impress_cnt_direct_lead) AS cb_impress_cnt_direct_lead, sum(lc_impress_cnt_direct_lead) as lc_impress_cnt_direct_lead, sum(cb_impress_wtd) as cb_impress_wtd, sum(lc_impress_wtd) as lc_impress_wtd, sum(cb_impress_lw) as cb_impress_lw, sum(lc_impress_lw) as lc_impress_lw, sum(cb_impress_mtd) as cb_impress_mtd, sum(lc_impress_mtd) as lc_impress_mtd, sum(cb_impress_lm) as cb_impress_lm, sum(lc_impress_lm) as lc_impress_lm
from imp 
group by 1) p
left join (select grass_date, sum(cb_pv_direct_lead) AS cb_pv_direct_lead, sum(lc_pv_direct_lead) as lc_pv_direct_lead, sum(cb_pv_wtd) as cb_pv_wtd, sum(lc_pv_wtd) as lc_pv_wtd, sum(cb_pv_lw) as cb_pv_lw, sum(lc_pv_lw) as lc_pv_lw, sum(cb_pv_mtd) as cb_pv_mtd, sum(lc_pv_mtd) as lc_pv_mtd, sum(cb_pv_lm) as cb_pv_lm, sum(lc_pv_lm) as lc_pv_lm
from pdp_v 
group by 1) v on p.grass_date=v.grass_date 
left join (select grass_date, sum(cb_order_direct_lead) AS cb_order_direct_lead, sum(lc_order_direct_lead) as lc_order_direct_lead, sum(cb_order_wtd) as cb_order_wtd, sum(lc_order_wtd) as lc_order_wtd, sum(cb_order_lw) as cb_order_lw, sum(lc_order_lw) as lc_order_lw, sum(cb_order_mtd) as cb_order_mtd, sum(lc_order_mtd) as lc_order_mtd, sum(cb_order_lm) as cb_order_lm, sum(lc_order_lm) as lc_order_lm
from ord 
group by 1) o on p.grass_date=o.grass_date 
)
order by 3 desc