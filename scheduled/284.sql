insert into nexus.test_c579166d00758403cf3e7f2cf16c1df8461991ba1d4f93ee2fc9de5b471c86f2_bd6e2710894a53ebb83880b5bbf247e8 /********************************** 
Name: Aging Inventory Monitoring 
Function: 
Notes: 

Date Developer Activity 
2022-04-22 Sharmaine Create 


**********************************/ 
WITH cbwh_shops AS (
SELECT
DISTINCT shop_id
FROM
regcbbi_general.cbwh_shop_history_v2
WHERE
grass_date = date_trunc('week', current_date) - INTERVAL '1' DAY -- take shops as of Last Sunday 
AND grass_region IN ('PH', 'MY', 'ID', 'TH')
),
sku_info AS (
SELECT
distinct m.shop_id,
m.shop_name,
m.seller_id,
m.seller_name,
m.item_id,
m.item_name,
m.level1_global_be_category,
m.level2_global_be_category,
m.level3_global_be_category,
m.model_id,
m.sku_id,
(CASE WHEN m.grass_region = 'PH' THEN CONCAT('http://shopee.ph/product/',CAST(m.shop_id AS varchar), '/',CAST(m.item_id AS varchar))
WHEN m.grass_region = 'TH' THEN CONCAT('http://shopee.co.th/product/',CAST(m.shop_id AS varchar), '/',CAST(m.item_id AS varchar))
WHEN m.grass_region = 'ID' THEN CONCAT('http://shopee.co.id/product/',CAST(m.shop_id AS varchar), '/',CAST(m.item_id AS varchar))
WHEN m.grass_region = 'MY' THEN CONCAT('http://shopee.com.my/product/',CAST(m.shop_id AS varchar), '/',CAST(m.item_id AS varchar))
ELSE NULL END



) AS product_link,
(CASE WHEN m.grass_region = 'PH' THEN CONCAT('http://f.shopee.ph/file/',"split"(images, ',') [1])
WHEN m.grass_region = 'TH' THEN CONCAT('http://f.shopee.co.th/file/',"split"(images, ',') [1])
WHEN m.grass_region = 'ID' THEN CONCAT('http://f.shopee.co.id/file/',"split"(images, ',') [1])
WHEN m.grass_region = 'MY' THEN CONCAT('http://f.shopee.com.my/file/',"split"(images, ',') [1])
ELSE NULL END
) AS image_link
FROM
mp_item.dim_model__reg_s0_live m
INNER JOIN cbwh_shops ON cbwh_shops.shop_id = m.shop_id --filter cbwh shops 
LEFT JOIN (
SELECT
item_id,
images
FROM
mp_item.dim_item_ext__reg_s0_live
WHERE
grass_region IN ('PH', 'MY', 'ID', 'TH')
AND tz_type = 'local'
AND grass_date = current_date - interval '1' day
AND is_cb_shop = 1
) b on b.item_id = m.item_id
WHERE
tz_type = 'local'
AND grass_date = current_date - interval '1' day
AND grass_region IN ('PH', 'MY', 'ID', 'TH')
AND is_cb_shop = 1
GROUP BY
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13
),
/** 2) Get selling speed from sbs_mart **/
inv_selling_speed AS (
SELECT
DISTINCT
shopid,
itemid,
sku_id,
fe_stock,
stock_on_hand,
l60_gross_itemsold,
l60_purchasable_days,
coalesce(
try(
l60_gross_itemsold / nullif(l60_purchasable_days, 0)
),
0
) AS sku_l60d_selling_speed
FROM
sbs_mart.shopee_bd_sbs_mart_v2
WHERE
grass_date = date_trunc('week', current_date) - INTERVAL '1' DAY -- take selling speed as of Last Sunday 
AND grass_region IN ('PH', 'MY', 'ID', 'TH')
AND is_cb = 1
),
/** 3) Get inbound history - follow charging script logic**/
ph_inbound_history__raw AS (
SELECT
substr(whs_id, 1, 2) AS grass_region,
whs_id,
inbound_date,
po_id,
po_inbound_id,
sku_id,
SUM(inbound_quantity) AS inbound_quantity
FROM
(
-- BEFORE WMS migration
SELECT
t2.whs_id,
--t2.inbound_date,
t2.po_id,
t1.po_inbound_id,
t1.sku_id,
t1.inbound_quantity,
t1.inbound_date
FROM
(
SELECT
distinct po_inbound_id,
sku_id,
putaway_count AS inbound_quantity,
date(from_unixtime(mtime)) AS inbound_date
FROM
wms_mart.shopee_wms_ph_db__po_inbound_sku_list_tab__reg_daily_s0_live
WHERE
putaway_count > 0
) AS t1
JOIN (
SELECT
distinct whs_id,
po_inbound_id,
--date(from_unixtime(real_inbound_date)) AS inbound_date,
po_id
FROM
wms_mart.shopee_wms_ph_db__po_inbound_tab__reg_daily_s0_live
--WHERE
--grass_region = 'PH' -- Target Region
-- AND (
-- real_inbound_date is NOT null
-- and real_inbound_date > 0
-- )
) AS t2 ON t1.po_inbound_id = t2.po_inbound_id
UNION
-- AFTER WMS migration
SELECT
t4.whs_id,
--t4.inbound_date,
t4.po_id,
t3.po_inbound_id,
t3.sku_id,
t3.inbound_quantity,
t3.inbound_date
FROM
(
SELECT
distinct inbound_id AS po_inbound_id,
sku_id,
putaway_count AS inbound_quantity,
date(from_unixtime(mtime)) AS inbound_date
FROM
wms_mart.shopee_wms_ph_db__inbound_sku_list_v2_tab__reg_daily_s0_live
WHERE
putaway_count > 0
) AS t3
JOIN (
SELECT
distinct whs_id,
inbound_id AS po_inbound_id,
--date(from_unixtime(real_inbound_date)) AS inbound_date,
order_no AS po_id
FROM
wms_mart.shopee_wms_ph_db__inbound_v2_tab__reg_daily_s0_live
WHERE
biz_type IN (1, 2) -- 1=purchase_inbound; 2=return_inbound
AND substr(whs_id, 1, 2) = 'PH' -- AND (
-- real_inbound_date is NOT null
-- and real_inbound_date > 0
-- )
) AS t4 ON t3.po_inbound_id = t4.po_inbound_id
)
GROUP BY
1,
2,
3,
4,
5,
6
),


my_inbound_history__raw AS (
SELECT
substr(whs_id, 1, 2) AS grass_region,
whs_id,
inbound_date,
po_id,
po_inbound_id,
sku_id,
SUM(inbound_quantity) AS inbound_quantity
FROM
(
-- BEFORE WMS migration
SELECT
t2.whs_id,
--t2.inbound_date,
t2.po_id,
t1.po_inbound_id,
t1.sku_id,
t1.inbound_quantity,
t1.inbound_date
FROM
(
SELECT
distinct po_inbound_id,
sku_id,
putaway_count AS inbound_quantity,
date(from_unixtime(mtime)) AS inbound_date
FROM
wms_mart.shopee_wms_my_db__po_inbound_sku_list_tab__reg_daily_s0_live
WHERE
putaway_count > 0
) AS t1
JOIN (
SELECT
distinct whs_id,
po_inbound_id,
--date(from_unixtime(real_inbound_date)) AS inbound_date,
po_id
FROM
wms_mart.shopee_wms_my_db__po_inbound_tab__reg_daily_s0_live
--WHERE
--grass_region = 'MY' -- Target Region
-- AND (
-- real_inbound_date is NOT null
-- and real_inbound_date > 0
-- )
) AS t2 ON t1.po_inbound_id = t2.po_inbound_id
UNION
-- AFTER WMS migration
SELECT
t4.whs_id,
--t4.inbound_date,
t4.po_id,
t3.po_inbound_id,
t3.sku_id,
t3.inbound_quantity,
t3.inbound_date
FROM
(
SELECT
distinct inbound_id AS po_inbound_id,
sku_id,
putaway_count AS inbound_quantity,
date(from_unixtime(mtime)) AS inbound_date
FROM
wms_mart.shopee_wms_my_db__inbound_sku_list_v2_tab__reg_daily_s0_live
WHERE
putaway_count > 0
) AS t3
JOIN (
SELECT
distinct whs_id,
inbound_id AS po_inbound_id,
--date(from_unixtime(real_inbound_date)) AS inbound_date,
order_no AS po_id
FROM
wms_mart.shopee_wms_my_db__inbound_v2_tab__reg_daily_s0_live
WHERE
biz_type IN (1, 2) -- 1=purchase_inbound; 2=return_inbound
AND substr(whs_id, 1, 2) = 'MY' -- AND (
-- real_inbound_date is NOT null
-- and real_inbound_date > 0
-- )
) AS t4 ON t3.po_inbound_id = t4.po_inbound_id
)
GROUP BY
1,
2,
3,
4,
5,
6
),


th_inbound_history__raw AS (
SELECT
substr(whs_id, 1, 2) AS grass_region,
whs_id,
inbound_date,
po_id,
po_inbound_id,
sku_id,
SUM(inbound_quantity) AS inbound_quantity
FROM
(
-- BEFORE WMS migration
SELECT
t2.whs_id,
--t2.inbound_date,
t2.po_id,
t1.po_inbound_id,
t1.sku_id,
t1.inbound_quantity,
t1.inbound_date
FROM
(
SELECT
distinct po_inbound_id,
sku_id,
putaway_count AS inbound_quantity,
date(from_unixtime(mtime)) AS inbound_date
FROM
wms_mart.shopee_wms_th_db__po_inbound_sku_list_tab__reg_daily_s0_live
WHERE
putaway_count > 0
) AS t1
JOIN (
SELECT
distinct whs_id,
po_inbound_id,
--date(from_unixtime(real_inbound_date)) AS inbound_date,
po_id
FROM
wms_mart.shopee_wms_th_db__po_inbound_tab__reg_daily_s0_live
--WHERE
--grass_region = 'MY' -- Target Region
-- AND (
-- real_inbound_date is NOT null
-- and real_inbound_date > 0
-- )
) AS t2 ON t1.po_inbound_id = t2.po_inbound_id
UNION
-- AFTER WMS migration
SELECT
t4.whs_id,
--t4.inbound_date,
t4.po_id,
t3.po_inbound_id,
t3.sku_id,
t3.inbound_quantity,
t3.inbound_date
FROM
(
SELECT
distinct inbound_id AS po_inbound_id,
sku_id,
putaway_count AS inbound_quantity,
date(from_unixtime(mtime)) AS inbound_date
FROM
wms_mart.shopee_wms_th_db__inbound_sku_list_v2_tab__reg_daily_s0_live
WHERE
putaway_count > 0
) AS t3
JOIN (
SELECT
distinct whs_id,
inbound_id AS po_inbound_id,
--date(from_unixtime(real_inbound_date)) AS inbound_date,
order_no AS po_id
FROM
wms_mart.shopee_wms_th_db__inbound_v2_tab__reg_daily_s0_live
WHERE
biz_type IN (1, 2) -- 1=purchase_inbound; 2=return_inbound
AND substr(whs_id, 1, 2) = 'TH' -- AND (
-- real_inbound_date is NOT null
-- and real_inbound_date > 0
-- )
) AS t4 ON t3.po_inbound_id = t4.po_inbound_id
)
GROUP BY
1,
2,
3,
4,
5,
6
),




id_inbound_history__raw AS (
SELECT
substr(whs_id, 1, 2) AS grass_region,
whs_id,
inbound_date,
po_id,
po_inbound_id,
sku_id,
SUM(inbound_quantity) AS inbound_quantity
FROM
(
-- BEFORE WMS migration
SELECT
t2.whs_id,
--t2.inbound_date,
t2.po_id,
t1.po_inbound_id,
t1.sku_id,
t1.inbound_quantity,
t1.inbound_date
FROM
(
SELECT
distinct po_inbound_id,
sku_id,
putaway_count AS inbound_quantity,
date(from_unixtime(mtime)) AS inbound_date
FROM
wms_mart.shopee_wms_id_db__po_inbound_sku_list_tab__reg_daily_s0_live
WHERE
putaway_count > 0
) AS t1
JOIN (
SELECT
distinct whs_id,
po_inbound_id,
--date(from_unixtime(real_inbound_date)) AS inbound_date,
po_id
FROM
wms_mart.shopee_wms_id_db__po_inbound_tab__reg_daily_s0_live
--WHERE
--grass_region = 'MY' -- Target Region
-- AND (
-- real_inbound_date is NOT null
-- and real_inbound_date > 0
-- )
) AS t2 ON t1.po_inbound_id = t2.po_inbound_id
UNION
-- AFTER WMS migration
SELECT
t4.whs_id,
--t4.inbound_date,
t4.po_id,
t3.po_inbound_id,
t3.sku_id,
t3.inbound_quantity,
t3.inbound_date
FROM
(
SELECT
distinct inbound_id AS po_inbound_id,
sku_id,
putaway_count AS inbound_quantity,
date(from_unixtime(mtime)) AS inbound_date
FROM
wms_mart.shopee_wms_id_db__inbound_sku_list_v2_tab__reg_daily_s0_live
WHERE
putaway_count > 0
) AS t3
JOIN (
SELECT
distinct whs_id,
inbound_id AS po_inbound_id,
--date(from_unixtime(real_inbound_date)) AS inbound_date,
order_no AS po_id
FROM
wms_mart.shopee_wms_id_db__inbound_v2_tab__reg_daily_s0_live
WHERE
biz_type IN (1, 2) -- 1=purchase_inbound; 2=return_inbound
AND substr(whs_id, 1, 2) = 'ID' -- AND (
-- real_inbound_date is NOT null
-- and real_inbound_date > 0
-- )
) AS t4 ON t3.po_inbound_id = t4.po_inbound_id
)
GROUP BY
1,
2,
3,
4,
5,
6
),


inbound_history__raw AS (
SELECT
*
FROM
ph_inbound_history__raw
UNION
SELECT
*
FROM
id_inbound_history__raw
UNION
SELECT
*
FROM
my_inbound_history__raw
UNION
SELECT
*
FROM
th_inbound_history__raw
),


mt_mp_mapping AS (
-- CNSC Cross Shop Listing Feature
SELECT
distinct sku_id,
mtsku_id
FROM
sbs_mart.shopee_bd_sbs_mart_v2
WHERE
grass_region IN ('PH', 'MY', 'ID', 'TH')
AND is_cb = 1
AND grass_date = date(date_trunc('month', current_date)) - interval '1' day --AND mtsku_id is NOT NULL
),
inbound_history AS (
/*
The background here is:
for updated SKUs (old sku_id => MT/MP structure), the inbound records would keep old sku_id
for new SKUs (default MT/MP structure), the inbound records will use MT SKU id
*/
SELECT
a.grass_region,
a.whs_id,
a.inbound_date,
a.po_id,
a.po_inbound_id,
a.sku_id,
a.inbound_quantity
FROM
inbound_history__raw AS a
JOIN mt_mp_mapping AS b ON a.sku_id = b.sku_id
UNION
-- convert MT inbound sku id to MP inbound sku id
SELECT
a.grass_region,
a.whs_id,
a.inbound_date,
a.po_id,
a.po_inbound_id,
b.sku_id -- converted
,
a.inbound_quantity
FROM
inbound_history__raw AS a
JOIN mt_mp_mapping AS b ON a.sku_id = b.mtsku_id -- MT SKU (New)
),
current_inventory AS (
SELECT
distinct grass_region,
sku_id,
stock_on_hand AS current_quantity
FROM
sbs_mart.shopee_bd_sbs_mart_v2
WHERE
grass_region IN ('PH', 'MY', 'ID', 'TH') 
AND grass_date = date_trunc('week', current_date) - INTERVAL '1' DAY -- take stock_on_hand as of last Sunday 
),
inventory_history AS (
SELECT
grass_region,
whs_id,
sku_id,
po_id,
po_inbound_id,
inbound_date,
inbound_quantity,
current_quantity,
IF(
remain_quantity > 0,
inbound_quantity,
inbound_quantity + remain_quantity
) AS actual_remaining_stock
FROM
(
SELECT
a.grass_region,
a.whs_id,
a.sku_id,
a.po_id,
po_inbound_id,
a.inbound_date,
a.inbound_quantity,
b.current_quantity,
b.current_quantity - SUM(a.inbound_quantity) OVER (
partition by a.grass_region,
a.sku_id
order by
a.inbound_date DESC,
a.po_inbound_id DESC
) AS remain_quantity
FROM
(
SELECT
grass_region,
whs_id,
sku_id,
po_id,
po_inbound_id,
inbound_date,
inbound_quantity
FROM
inbound_history
WHERE
inbound_date <= date_trunc('week', current_date) - INTERVAL '1' DAY -- take POs which were inbounded before last Sunday 
) AS a
LEFT JOIN (
SELECT
grass_region,
sku_id,
current_quantity
FROM
current_inventory
) AS b ON a.sku_id = b.sku_id
WHERE
b.current_quantity > 0
)
),
sku_ages AS (
SELECT
grass_region,
whs_id,
sku_id,
po_id,
inbound_date,
actual_remaining_stock,
date_diff('day',
inbound_date,
date_trunc('week', current_date) - INTERVAL '1' DAY
) AS aging_days -- calculate aging days as difference between inbound date and last Sunday. Charging script is using datediff, we use date_diff here for Presto SQL 
FROM
inventory_history
WHERE
actual_remaining_stock > 0
),
rts_stock AS (
SELECT
r.whs_id,
s.sku_id,
SUM(s.approved_return_qty) AS rts_qty
FROM
sbs_mart.shopee_pms_db__fbs_rts_request_sku_tab__reg_daily_s0_live s
INNER JOIN sbs_mart.shopee_pms_db__fbs_rts_request_tab__reg_daily_s0_live r ON s.rts_id = r.request_id
WHERE
r.region IN ('PH', 'MY', 'ID', 'TH')
AND r.request_status IN (4, 6, 7, 8, 9)
AND r.is_approved = 1
GROUP BY
1,
2
),
sku_stock_adjusted AS (
SELECT
a.grass_region,
a.whs_id,
a.sku_id,
a.po_id,
a.inbound_date,
a.actual_remaining_stock,
a.aging_days,
b.rts_qty,
GREATEST(
SUM(
a.actual_remaining_stock
) OVER (
partition by a.grass_region,
a.whs_id,
a.sku_id
) - COALESCE(b.rts_qty, 0),
0
) AS adjusted_total_stock
FROM
(
SELECT
grass_region,
whs_id,
sku_id,
po_id,
inbound_date,
actual_remaining_stock,
aging_days
FROM
sku_ages
) AS a
LEFT JOIN (
SELECT
whs_id,
sku_id,
rts_qty
FROM
rts_stock
WHERE
rts_qty > 0
) AS b ON a.whs_id = b.whs_id
AND a.sku_id = b.sku_id
),
sku_stock_adjusted_2 AS (
SELECT
grass_region,
whs_id,
sku_id,
po_id,
inbound_date,
aging_days,
IF(
remain_quantity > 0,
actual_remaining_stock,
actual_remaining_stock + remain_quantity
) AS actual_remaining_stock_adjusted
FROM
(
SELECT
grass_region,
whs_id,
sku_id,
po_id,
inbound_date,
actual_remaining_stock,
aging_days,
rts_qty,
adjusted_total_stock - SUM(actual_remaining_stock) OVER (
partition by grass_region,
whs_id,
sku_id
order by
inbound_date DESC,
po_id DESC
) AS remain_quantity
FROM
sku_stock_adjusted
WHERE
adjusted_total_stock > 0
)
),
sku_ages_adjusted AS (
SELECT
grass_region,
whs_id,
sku_id,
po_id,
inbound_date,
actual_remaining_stock_adjusted,
aging_days
FROM
sku_stock_adjusted_2
WHERE
actual_remaining_stock_adjusted > 0
),
ph_whs_sku_info AS (
SELECT
CAST(shopid as bigint) AS shop_id,
sku_id,
AVG(length) AS length_mm,
AVG(width) AS width_mm,
AVG(height) AS height_mm
FROM
wms_mart.shopee_wms_ph_db__sku_cache_tab__reg_daily_s0_live
WHERE
country = 'PH'
GROUP BY
1,
2
),


my_whs_sku_info AS (
SELECT
CAST(shopid as bigint) AS shop_id,
sku_id,
AVG(length) AS length_mm,
AVG(width) AS width_mm,
AVG(height) AS height_mm
FROM
wms_mart.shopee_wms_my_db__sku_cache_tab__reg_daily_s0_live
WHERE
country = 'MY'
GROUP BY
1,
2
),


th_whs_sku_info AS (
SELECT
CAST(shopid as bigint) AS shop_id,
sku_id,
AVG(length) AS length_mm,
AVG(width) AS width_mm,
AVG(height) AS height_mm
FROM
wms_mart.shopee_wms_th_db__sku_cache_tab__reg_daily_s0_live
WHERE
country = 'TH'
GROUP BY
1,
2
),


id_whs_sku_info AS (
SELECT
CAST(shopid as bigint) AS shop_id,
sku_id,
AVG(length) AS length_mm,
AVG(width) AS width_mm,
AVG(height) AS height_mm
FROM
wms_mart.shopee_wms_id_db__sku_cache_tab__reg_daily_s0_live
WHERE
country = 'ID'
GROUP BY
1,
2
),


whs_sku_info AS (
SELECT * FROM ph_whs_sku_info
UNION 
SELECT * FROM my_whs_sku_info
UNION 
SELECT * FROM th_whs_sku_info
UNION 
SELECT * FROM id_whs_sku_info
),
base_data AS (
SELECT
distinct a.grass_region,
a.whs_id,
a.sku_id,
a.po_id,
a.inbound_date,
a.actual_remaining_stock_adjusted,
a.aging_days,
b.shop_id,
CAST(
(b.length_mm * b.width_mm * b.height_mm) as double
) / 1000 / 1000 / 1000 AS avg_volume_cbm
FROM
sku_ages_adjusted AS a
LEFT JOIN whs_sku_info AS b ON a.sku_id = b.sku_id
),


/** 4) Construct raw data **/
raw_data AS (SELECT
date_trunc('week', current_date) - INTERVAL '1' DAY AS reporting_date,
a.grass_region,
b.seller_name,
b.seller_id,
c.gp_account_name,
c.gp_account_owner,
c.ggp_account_owner_userrole,
a.shop_id,
b.shop_name,
b.item_id,
b.item_name,
b.model_id,
a.sku_id,
b.level1_global_be_category,
b.level2_global_be_category,
b.level3_global_be_category,
b.product_link,
b.image_link,
a.avg_volume_cbm,
MIN(a.inbound_date) AS aging_day_calculation_start_date,
SUM(
IF(
a.aging_days > 120,
a.actual_remaining_stock_adjusted,
0
)
) AS aging_stock_qty_greater_120_days,
SUM(
IF(
a.aging_days > 120,
a.actual_remaining_stock_adjusted * a.avg_volume_cbm,
0
)
) AS aging_stock_vol_cbm_greater_120_days
FROM
base_data AS a
JOIN sku_info AS b ON a.shop_id = b.shop_id
and a.sku_id = b.sku_id
LEFT JOIN (
SELECT
distinct CAST(child_shopid as bigint) AS shop_id,
gp_account_name,
gp_account_owner,
ggp_account_owner_userrole
FROM
cncbbi_general.shopee_cb_seller_profile_with_old_column
WHERE
grass_region IN ('PH', 'MY', 'ID', 'TH')
) AS c ON a.shop_id = c.shop_id
GROUP BY
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
16,
17,
18,
19)


/** 5) Final SELECT **/


SELECT * FROM (
SELECT r. reporting_date,
r.grass_region,
r.seller_name,
r.seller_id,
r.gp_account_name,
r.gp_account_owner,
r.ggp_account_owner_userrole,
r.shop_id,
r.shop_name,
r.item_id,
r.item_name,
r.model_id,
r.sku_id,
r.level1_global_be_category,
r.level2_global_be_category,
r.level3_global_be_category,
r.product_link, 
r.image_link,
i.fe_stock,
i.stock_on_hand,
i.l60_gross_itemsold,
i.l60_purchasable_days,
r.avg_volume_cbm,
r.aging_stock_qty_greater_120_days,
r.aging_stock_vol_cbm_greater_120_days,
ROW_NUMBER() OVER (PARTITION BY r.grass_region ORDER BY r.aging_stock_vol_cbm_greater_120_days DESC) AS ranking
FROM raw_data r
left join inv_selling_speed i on i.sku_id = r.sku_id)


WHERE RANKING <=1000