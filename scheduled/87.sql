WITH sip AS (
SELECT 
grass_date,
grass_region,
shop_id,
user_name,
source
FROM dev_regcbbi_others.sip__shop_profile__reg__s0
WHERE grass_date = current_date - interval '1' day 
AND source IN ('Local SIP ID', 'Local SIP MY', 'Local SIP TW', 'Other SIP', 'Local SIP VN', 'Local SIP TH')
)


, top_sku AS (
SELECT 
grass_region,
source,
shop_id,
item_id,
l7d_ord_ranking
FROM dev_regcbbi_others.sip__mkt_top_sku__di__reg__s0
WHERE grass_date = current_date - interval '1' day 
AND source IN ('Local SIP ID', 'Local SIP MY', 'Local SIP TW', 'Other SIP', 'Local SIP VN', 'Local SIP TH')
AND l7d_ord_ranking <= 500
)
, orders AS (
SELECT shop_id
,item_id
,placed_order_fraction_1d
,placed_order_fraction_7d
,placed_order_fraction_14d
,placed_order_fraction_30d
,item_amount_1d
,item_amount_7d
,item_amount_14d
,item_amount_30d
FROM mp_order.dws_item_gmv_nd__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = date(current_date - INTERVAL '1' DAY)
AND grass_region IN ('SG', 'PH', 'MY', 'ID', 'TH', 'VN') )




, item AS (
SELECT 
shop_id
, item_id
, name
-- , concat('http://f.shopee.vn/file/', split(images, ',')[1]) image_link
-- , level1_global_be_category_id
, level1_global_be_category
, level2_global_be_category
, level3_global_be_category
, price
, price_before_discount
, rating_score
FROM
mp_item.dim_item__reg_s0_live
WHERE grass_date = (current_date - INTERVAL '1' DAY) 
AND grass_region IN ('SG', 'PH', 'MY', 'ID', 'TH','VN') 
AND tz_type = 'local' 
AND is_cb_shop = 1
)




, traffic AS (
SELECT
shop_id
, item_id
, pv_cnt_7d L7D_view
, imp_cnt_7d L7D_imp
FROM
mp_shopflow.dws_item_civ_order_nd__reg_s0_live
WHERE (grass_region IN ('SG', 'PH', 'MY', 'ID','TH','VN') 
AND grass_date = (current_date - INTERVAL '1' DAY) 
AND tz_type = 'local' )
) 
, penalty AS (
SELECT
affi_shopid
, penalty_point
FROM
regcbbi_others.sip_ops_performance
) 


, image AS (
SELECT
shop_id,
item_id,
images
-- split(images,',')[1] AS image_hash
-- concat('http://f.shopee.vn/file/',split(images,',')[1]) AS image_link
FROM mp_item.dim_item_ext__reg_s0_live
WHERE grass_date = current_date - interval '1' day
AND grass_region IN ('SG', 'PH', 'MY', 'ID','TH','VN')
AND tz_type = 'local'
AND is_cb_shop = 1 
)


-- SELECT * FROM orders 


SELECT
sip.grass_region
, sip.source
, top_sku.l7d_ord_ranking
, penalty_point
, item.level1_global_be_category
, item.level2_global_be_category
, item.level3_global_be_category
, sip.user_name
, top_sku.shop_id
, top_sku.item_id
, item.name
--, '' AS product_link
, (CASE WHEN (top_sku.grass_region = 'TW') THEN concat('http://shopee.tw/product/', CAST(top_sku.shop_id AS varchar), '/', CAST(top_sku.item_id AS varchar)) 
WHEN (top_sku.grass_region = 'MY') THEN concat('http://shopee.com.my/product/', CAST(top_sku.shop_id AS varchar), '/', CAST(top_sku.item_id AS varchar)) 
WHEN (top_sku.grass_region = 'ID') THEN concat('http://shopee.co.id/product/', CAST(top_sku.shop_id AS varchar), '/', CAST(top_sku.item_id AS varchar)) 
WHEN (top_sku.grass_region = 'PH') THEN concat('http://shopee.ph/product/', CAST(top_sku.shop_id AS varchar), '/', CAST(top_sku.item_id AS varchar)) 
WHEN (top_sku.grass_region = 'SG') THEN concat('http://shopee.sg/product/', CAST(top_sku.shop_id AS varchar), '/', CAST(top_sku.item_id AS varchar)) 
WHEN (top_sku.grass_region = 'VN') THEN concat('http://shopee.vn/product/', CAST(top_sku.shop_id AS varchar), '/', CAST(top_sku.item_id AS varchar)) 
WHEN (top_sku.grass_region = 'TH') THEN concat('http://shopee.co.th/product/', CAST(top_sku.shop_id AS varchar), '/', CAST(top_sku.item_id AS varchar)) 
WHEN (top_sku.grass_region = 'BR') THEN concat('http://shopee.com.br/product/', CAST(top_sku.shop_id AS varchar), '/', CAST(top_sku.item_id AS varchar)) 
ELSE null END) product_link
, item.price_before_discount
, item.price
, item.rating_score
, (orders.placed_order_fraction_7d / DECIMAL '7.00') L7D_ado
, ((orders.placed_order_fraction_14d - orders.placed_order_fraction_7d) / DECIMAL '7.00') D8_D14_ado
, TRY(((orders.placed_order_fraction_7d / (orders.placed_order_fraction_14d - orders.placed_order_fraction_7d)) * DECIMAL '1.000')) ado_wow
, (orders.item_amount_7d / DECIMAL '7.00') L7D_avg_daily_item_sold
,'' AS image_link
-- , concat('http://f.shopee.vn/file/',split(image.images,',')[1]) AS image_link
, TRY(((orders.placed_order_fraction_7d / traffic.L7D_view) * DECIMAL '1.000')) L7D_CR
, TRY((CAST(traffic.L7D_view AS decimal(10,4)) / CAST(traffic.L7D_imp AS decimal(10,4)))) L7D_CTR
, (traffic.L7D_view / DECIMAL '7.00') L7D_avg_view
, (traffic.L7D_imp / DECIMAL '7.00') L7D_avg_imp
, (orders.placed_order_fraction_30d / DECIMAL '30.00') L30D_ADO
FROM
item
-- LEFT JOIN image ON item.item_id = image.item_id 
LEFT JOIN traffic ON traffic.item_id = item.item_id
LEFT JOIN penalty ON penalty.affi_shopid = item.shop_id
LEFT JOIN sip ON sip.shop_id = item.shop_id
LEFT JOIN orders ON orders.item_id = item.item_id
INNER JOIN top_sku ON top_sku.item_id = item.item_id 


ORDER BY sip.source ASC, sip.grass_region DESC, l7d_ord_ranking ASC