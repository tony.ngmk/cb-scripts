insert into nexus.test_d0f219e5e56ece2207926cf342748a79489766112e88f4c1ef47941d08e89bdb_dbe887457966e8fefbd606c7c62328e2 /**********************************
Name: SMT Performance Dashboard
Function: key metrics for WHS to monitor SMT group performnace
Notes: -

Date Developer Activity
2022-04-07 Mingyu Create Script 

**********************************/
-- ####################
-- Data Range: all CB shops
-- ####################
WITH cb_shop AS(
SELECT
DISTINCT u.grass_date,
u.grass_region,
u.shop_id,
DATE(u.registration_datetime) AS registration_date,
s.is_official_shop,
CASE
WHEN s.shop_status = 1
AND u.user_status = 1
AND (
s.is_holiday_mode = 0
OR s.is_holiday_mode IS NULL
)
AND i.active_item_with_stock_cnt > 0
--AND l.last_login >= u.grass_date - interval '7' day 
THEN 1
ELSE 0
END AS is_live
FROM
(
SELECT
DISTINCT grass_date,
grass_region,
shop_id,
user_id,
STATUS AS user_status,
registration_datetime
FROM
mp_user.dim_user__reg_s0_live
WHERE
grass_date BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month
AND date_trunc('month', current_date - interval '1' day) - interval '1' day
AND tz_type = 'local'
AND grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
AND is_cb_shop = 1
AND is_seller = 1
) u
JOIN (
SELECT
DISTINCT grass_date,
grass_region,
shop_id,
is_official_shop,
STATUS AS shop_status,
is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_date BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month
AND date_trunc('month', current_date - interval '1' day) - interval '1' day
AND tz_type = 'local'
AND grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
AND is_cb_shop = 1
) s ON u.shop_id = s.shop_id
AND u.grass_date = s.grass_date
JOIN (
SELECT
DISTINCT grass_date,
shop_id,
active_item_with_stock_cnt
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month
AND date_trunc('month', current_date - interval '1' day) - interval '1' day
AND tz_type = 'local'
AND grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
) i ON u.shop_id = i.shop_id
AND u.grass_date = i.grass_date
-- LEFT JOIN (
-- SELECT
-- DISTINCT grass_date,
-- user_id,
-- DATE(from_unixtime(last_login_timestamp_td)) AS last_login
-- FROM
-- mp_user.dws_user_login_td_account_info__reg_s0_live
-- WHERE
-- grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
-- AND tz_type = 'local'
-- AND grass_date BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month
-- AND date_trunc('month', current_date - interval '1' day) - interval '1' day
-- ) l on u.user_id = l.user_id
-- AND u.grass_date = l.grass_date
),
-- ####################
-- GGP Info
-- ####################
ggp_info AS (
SELECT
DISTINCT cast(child_shopid AS bigint) AS shop_id,
ggp_account_name AS ggp,
split(ggp_account_owner_userrole, '_') [1] AS cb_group,
ggp_account_owner_userrole AS userrole
FROM
cncbbi_general.shopee_cb_seller_profile_with_old_column --regbd_sf.cb__seller_index_tab
WHERE
grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
),
--#################
-- SKU
--#################
cb_live_sku AS (
SELECT
DISTINCT grass_date,
grass_region,
shop_id,
item_id
FROM
mp_item.dim_item__reg_s0_live
WHERE
grass_date BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month
AND date_trunc('month', current_date - interval '1' day) - interval '1' day
AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
AND tz_type = 'local'
ANd is_cb_shop = 1
AND status = 1
AND shop_status = 1
AND seller_status = 1
AND stock > 0
AND (
is_holiday_mode = 0
OR is_holiday_mode IS NULL
)
),
live_shop_w_sku AS (
SELECT
a.grass_region,
a.grass_date,
a.shop_id,
COUNT(DISTINCT item_id) AS shop_live_item_cnt
FROM
cb_shop a
LEFT JOIN cb_live_sku c ON a.grass_date = c.grass_date
AND a.shop_id = c.shop_id
WHERE
a.is_live = 1
GROUP BY
1,
2,
3
),
-- ####################
-- Order
-- ####################
order_raw AS (
SELECT
grass_region,
grass_date,
shop_id,
order_id,
order_fraction,
gmv_usd
FROM
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE
grass_region IN ('SG', 'MY', 'ID', 'TH', 'PH', 'MX', 'VN')
AND tz_type = 'local'
AND grass_date BETWEEN date_trunc('month', current_date - interval '1' day) - interval '1' month
AND date_trunc('month', current_date - interval '1' day) - interval '1' day
AND is_placed = 1
AND is_cb_shop = 1
),
order_sum AS(
SELECT
grass_region,
grass_date,
shop_id,
sum(order_fraction) AS shop_ado,
sum(gmv_usd) AS shop_gmv 
FROM
order_raw
GROUP BY
1,
2,
3
),
-- ##################
-- Combined
-- ##################
shop_summary AS (
SELECT
s.grass_region,
s.grass_date,
gi.ggp,
gi.cb_group,
gi.userrole,
s.shop_id,
s.is_official_shop,
s.registration_date,
CASE
WHEN l.shop_id IS NOT NULL THEN 1
ELSE 0
END AS is_live_shop,
l.shop_live_item_cnt,
o.shop_ado,
o.shop_gmv
FROM
cb_shop s
LEFT JOIN ggp_info gi ON s.shop_id = gi.shop_id
LEFT JOIN live_shop_w_sku l ON s.grass_date = l.grass_date
AND s.shop_id = l.shop_id
LEFT JOIN order_sum o ON s.grass_date = o.grass_date
AND s.shop_id = o.shop_id
),
combined AS (
SELECT
grass_region,
grass_date,
cb_group,
userrole,
COUNT(DISTINCT ggp) AS ggp_cnt,
COUNT(DISTINCT shop_id) AS shop_cnt,
COUNT(
DISTINCT CASE
WHEN is_live_shop = 1 THEN shop_id
END
) AS live_shop_cnt,
COUNT(
DISTINCT CASE
WHEN is_official_shop = 1 THEN shop_id
END
) AS official_shop_cnt,
COUNT(
DISTINCT CASE
WHEN registration_date = grass_date THEN shop_id
END
) AS new_shop_cnt,
SUM(shop_ado) AS cb_ado,
SUM(shop_live_item_cnt) AS live_sku_cnt,
sum(shop_gmv) AS cb_gmv
FROM
shop_summary
GROUP BY
1,
2,
3,
4
),
combined_last AS (
SELECT
grass_region,
cb_group,
userrole,
ggp_cnt,
shop_cnt,
live_shop_cnt,
official_shop_cnt,
live_sku_cnt
FROM
combined
WHERE
grass_date = date_trunc('month', current_date - interval '1' day) - interval '1' day
),
combined_avg AS(
SELECT
grass_region,
date_trunc('month', current_date - interval '1' day) - interval '1' month AS start_reporting_date,
date_trunc('month', current_date - interval '1' day) - interval '1' day AS end_reporting_date,
cb_group,
userrole,
-- AVG(ggp_cnt) AS ggp_cnt,
-- AVG(shop_cnt) AS shop_cnt,
-- AVG(live_shop_cnt) AS live_shop_cnt,
-- AVG(official_shop_cnt) AS official_shop_cnt,
SUM(new_shop_cnt) AS new_shop_cnt,
AVG(cb_ado) AS cb_ado ,-- AVG(live_sku_cnt) AS live_sku_cnt
AVG(cb_gmv) as cb_gmv
FROM
combined
GROUP BY
1,
2,
3,
4,
5
)
SELECT
a.grass_region,
a.start_reporting_date,
a.end_reporting_date,
a.cb_group,
l.userrole,
l.ggp_cnt,
l.shop_cnt,
l.live_shop_cnt,
try(1.000 * l.live_shop_cnt / l.shop_cnt) AS live_shop_pct,
a.new_shop_cnt,
l.official_shop_cnt,
a.cb_ado,
try(
a.cb_ado / SUM(a.cb_ado) OVER (PARTITION BY a.grass_region)
) AS ado_contri,
SUM(a.cb_ado) OVER (PARTITION BY a.grass_region) AS total_ado,
l.live_sku_cnt,
a.cb_gmv
FROM
combined_avg a
LEFT JOIN combined_last l ON a.grass_region = l.grass_region
AND a.userrole = l.userrole
ORDER BY
1,
4,
5