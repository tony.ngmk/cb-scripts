insert into nexus.test_8fe92e7c9daf5efb0d4dbace320c304d2e561c048c1f4ffe82b92b2fb75946d2_2048a2ee5a947066f5d45357952573dd WITH
ads AS (
SELECT DISTINCT
grass_region
, ads_type
, shop_id
, item_id
, ads_id
, level1_category
, level2_category
, level3_category
, (CASE WHEN ((grass_region = 'SG') AND (ads_type LIKE '%keyword%')) THEN "concat"('https://seller.sg.shopee.cn/portal/marketing/pas/assembly/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'SG') AND (ads_type LIKE '%targeting%')) THEN "concat"('https://seller.sg.shopee.cn/portal/marketing/pas/assembly/targeting/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'MY') AND (ads_type LIKE '%keyword%')) THEN "concat"('https://seller.my.shopee.cn/portal/marketing/pas/assembly/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'MY') AND (ads_type LIKE '%targeting%')) THEN "concat"('https://seller.my.shopee.cn/portal/marketing/pas/assembly/targeting/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'ID') AND (ads_type LIKE '%keyword%')) THEN "concat"('https://seller.id.shopee.cn/portal/marketing/pas/assembly/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'ID') AND (ads_type LIKE '%targeting%')) THEN "concat"('https://seller.id.shopee.cn/portal/marketing/pas/assembly/targeting/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'PH') AND (ads_type LIKE '%keyword%')) THEN "concat"('https://seller.ph.shopee.cn/portal/marketing/pas/assembly/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'PH') AND (ads_type LIKE '%targeting%')) THEN "concat"('https://seller.ph.shopee.cn/portal/marketing/pas/assembly/targeting/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'TH') AND (ads_type LIKE '%keyword%')) THEN "concat"('https://seller.th.shopee.cn/portal/marketing/pas/assembly/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'TH') AND (ads_type LIKE '%targeting%')) THEN "concat"('https://seller.th.shopee.cn/portal/marketing/pas/assembly/targeting/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'VN') AND (ads_type LIKE '%keyword%')) THEN "concat"('https://seller.vn.shopee.cn/portal/marketing/pas/assembly/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'VN') AND (ads_type LIKE '%targeting%')) THEN "concat"('https://seller.vn.shopee.cn/portal/marketing/pas/assembly/targeting/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'BR') AND (ads_type LIKE '%keyword%')) THEN "concat"('https://seller.br.shopee.cn/portal/marketing/pas/assembly/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'BR') AND (ads_type LIKE '%targeting%')) THEN "concat"('https://seller.br.shopee.cn/portal/marketing/pas/assembly/targeting/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'TW') AND (ads_type LIKE '%keyword%')) THEN "concat"('https://seller.xiapi.shopee.cn/portal/marketing/pas/assembly/', CAST(campaign_id AS varchar)) WHEN ((grass_region = 'TW') AND (ads_type LIKE '%targeting%')) THEN "concat"('https://seller.xiapi.shopee.cn/portal/marketing/pas/assembly/targeting/', CAST(campaign_id AS varchar)) ELSE 'NA' END) link
, "sum"(impression_cnt) impression_cnt
, "sum"(click_cnt) click_cnt
, COALESCE("sum"(order_cnt), 0) order_cnt
, "sum"(ads_expenditure_amt_usd) ads_expenditure_amt_usd
, COALESCE("sum"(ads_gmv_usd), 0) ads_gmv_usd
, TRY(("sum"(ads_expenditure_amt_usd) / TRY_CAST("sum"(order_cnt) AS double))) cpo
, "avg"(avg_ads_ranks) avg_ads_ranks
FROM
mp_paidads.ads_advertise_mkt_1d__reg_s0_live
WHERE ((((((grass_date BETWEEN (current_date - INTERVAL '21' DAY) AND (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local')) AND (impression_cnt > 0)) AND (((("date"("date_parse"(campaign_end_datetime, '%Y-%m-%d %T')) = "date"('1970-01-01')) OR ("date"("date_parse"(campaign_end_datetime, '%Y-%m-%d %T')) = "date"('1969-12-31'))) OR ("date_parse"(campaign_end_datetime, '%Y-%m-%d %T') >= current_date)) OR (campaign_end_datetime IS NULL))) AND ("date_parse"(campaign_start_datetime, '%Y-%m-%d %T') <= (current_date - INTERVAL '14' DAY))) AND (campaign_status_text = 'ADS_NORMAL'))
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9
) 
, s AS (
SELECT DISTINCT
shop_id
, source
, user_name
FROM
regcbbi_others.shop_profile
WHERE ((grass_date = (current_date - INTERVAL '1' DAY)) AND (source LIKE '%SIP%'))
) 
, r AS (
SELECT DISTINCT
grass_region
, ads_type
, level1_category
, level2_category
, level3_category
, "approx_percentile"(impression_cnt, DECIMAL '0.2') imp_20p
, "approx_percentile"(IF((cpo > 0), cpo, null), DECIMAL '0.8') cpo_80p
FROM
(ads
INNER JOIN s ON (ads.shop_id = s.shop_id))
GROUP BY 1, 2, 3, 4, 5
) 
SELECT DISTINCT
s.source
, ads.grass_region
, ads.shop_id
, s.user_name
, ads.item_id
, ads.ads_type
, ads.link ads_link
, (CASE WHEN (ads.grass_region = 'SG') THEN "concat"('http://shopee.sg/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) WHEN (ads.grass_region = 'MY') THEN "concat"('http://shopee.com.my/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) WHEN (ads.grass_region = 'ID') THEN "concat"('http://shopee.co.id/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) WHEN (ads.grass_region = 'TH') THEN "concat"('http://shopee.co.th/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) WHEN (ads.grass_region = 'PH') THEN "concat"('http://shopee.ph/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) WHEN (ads.grass_region = 'VN') THEN "concat"('http://shopee.vn/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) WHEN (ads.grass_region = 'TW') THEN "concat"('http://shopee.tw/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) WHEN (ads.grass_region = 'BR') THEN "concat"('http://shopee.com.br/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) WHEN (ads.grass_region = 'MX') THEN "concat"('http://shopee.com.mx/product/', TRY_CAST(ads.shop_id AS varchar), '/', TRY_CAST(ads.item_id AS varchar)) ELSE 'NA' END) product_link
, ads.level1_category
, ads.level2_category
, ads.level3_category
, impression_cnt
, click_cnt
, order_cnt
, ads_expenditure_amt_usd
, ads_gmv_usd
, cpo
, avg_ads_ranks
, COALESCE(o.L7D_orders, 0) L7D_orders
, COALESCE(o.organic_orders, 0) organic_orders
FROM
((((ads
INNER JOIN s ON (ads.shop_id = s.shop_id))
INNER JOIN r ON (((((ads.grass_region = r.grass_region) AND (ads.ads_type = r.ads_type)) AND (ads.level1_category = r.level1_category)) AND (ads.level2_category = r.level2_category)) AND (ads.level3_category = r.level3_category)))
LEFT JOIN (
SELECT DISTINCT
item_id
, "count"(DISTINCT order_id) L7D_orders
, "count"(DISTINCT IF(((is_flash_sale = 1) OR (item_rebate_by_shopee_amt > 0)), null, order_id)) organic_orders
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE ((("date"("from_unixtime"(create_timestamp)) BETWEEN (current_date - INTERVAL '21' DAY) AND (current_date - INTERVAL '1' DAY)) AND (is_cb_shop = 1)) AND (grass_region <> ''))
GROUP BY 1
) o ON (ads.item_id = o.item_id))
LEFT JOIN (
SELECT DISTINCT TRY_CAST(affi_shopid AS bigint) affi_shopid
FROM
(regcbbi_others.shopee_regional_cb_team__sip_seller_run_ads a
INNER JOIN (
SELECT "max"(ingestion_timestamp) ingestion_timestamp
FROM
regcbbi_others.shopee_regional_cb_team__sip_seller_run_ads
) b ON (a.ingestion_timestamp = b.ingestion_timestamp))
) sr ON (ads.shop_id = sr.affi_shopid))
WHERE (((((sr.affi_shopid IS NULL) AND ((ads.order_cnt = 0) OR (ads.order_cnt IS NULL))) AND (ads.impression_cnt > r.imp_20p)) AND (ads.ads_expenditure_amt_usd >= 1.5)) AND ((COALESCE(o.organic_orders, 0) < 7) OR (o.organic_orders IS NULL)))