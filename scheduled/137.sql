with mall as (
select grass_region
, COUNT(DISTINCT case when grass_date=date_add('day',-1,current_date) then shop_id end) as MTD_shop
, COUNT(DISTINCT case when grass_date=date_trunc('month',date_add('day',-1,current_date)) - interval '1' day then shop_id end) as LM_shop
from
(
select grass_region,grass_date, is_cb_shop,shop_id
from mp_user.dim_shop__reg_s0_live
where (grass_date=date_add('day',-1,current_date) or grass_date=date_trunc('month',date_add('day',-1,current_date)) - interval '1' day ) --date_add('day',-1,current_date)
and tz_type='local'
and is_official_shop=1
and is_cb_shop=1
and status=1
)
group by 1
)


, o as (
SELECT grass_region, 
count( distinct case when grass_date between date_trunc('month',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_official_shop=1 and is_cb_shop=1 then order_id end)/day(date_add('day',-1,current_date)) as cb_mall_ado_mtd,
count( distinct case when grass_date between date_trunc('month',date_add('day',-1,current_date)) - interval '1' month and date_trunc('month',date_add('day',-1,current_date)) - interval '1' day and is_official_shop=1 and is_cb_shop=1 then order_id end)/day(date_trunc('month',date_add('day',-1,current_date)) - interval '1' day) as cb_mall_ado_lm,
count( distinct case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval '7' day and date_trunc('week',date_add('day',-1,current_date)) - interval '1' day and is_official_shop=1 and is_cb_shop=1 then order_id end)/7 as cb_mall_ado_w1,
count( distinct case when grass_date between date_trunc('week',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_official_shop=1 and is_cb_shop=1 then order_id end)/day_of_week(date_add('day',-1,current_date)) as cb_mall_ado_wtd,
count( distinct case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval '7' day and date_trunc('week',date_add('day',-1,current_date)) - interval '1' day and is_official_shop=1 then order_id end)/7 as mall_ado_w1,
count( distinct case when grass_date between date_trunc('week',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_official_shop=1 then order_id end)/day_of_week(date_add('day',-1,current_date)) as mall_ado_wtd,
count( distinct case when grass_date between date_trunc('month',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_cb_shop=1 then order_id end)/day(date_add('day',-1,current_date)) as cb_ado_mtd,
count( distinct case when grass_date between date_trunc('month',date_add('day',-1,current_date)) - interval '1' month and date_trunc('month',date_add('day',-1,current_date)) - interval '1' day and is_cb_shop=1 then order_id end)/day(date_trunc('month',date_add('day',-1,current_date)) - interval '1' day) as cb_ado_lm,
count( distinct case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval '7' day and date_trunc('week',date_add('day',-1,current_date)) - interval '1' day and is_cb_shop=1 then order_id end)/7 as cb_ado_w1,
count( distinct case when grass_date between date_trunc('week',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_cb_shop=1 then order_id end)/day_of_week(date_add('day',-1,current_date)) as cb_ado_wtd,
count( distinct case when grass_date between date_trunc('month',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_official_shop=1 then order_id end)/day(date_add('day',-1,current_date)) as mall_ado_mtd,
count( distinct case when grass_date between date_trunc('month',date_add('day',-1,current_date)) - interval '1' month and date_trunc('month',date_add('day',-1,current_date)) - interval '1' day and is_official_shop=1 then order_id end)/day(date_trunc('month',date_add('day',-1,current_date)) - interval '1' day) as mall_ado_lm, 
sum( case when grass_date between date_trunc('month',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_official_shop=1 and is_cb_shop=1 then gmv_usd end)/day(date_add('day',-1,current_date)) as cb_mall_gmv_mtd,
sum( case when grass_date between date_trunc('month',date_add('day',-1,current_date)) - interval '1' month and date_trunc('month',date_add('day',-1,current_date)) - interval '1' day and is_official_shop=1 and is_cb_shop=1 then gmv_usd end)/day(date_trunc('month',date_add('day',-1,current_date)) - interval '1' day) as cb_mall_gmv_lm,
sum( case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval '7' day and date_trunc('week',date_add('day',-1,current_date)) - interval '1' day and is_official_shop=1 and is_cb_shop=1 then gmv_usd end)/7 as cb_mall_gmv_w1,
sum( case when grass_date between date_trunc('week',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_official_shop=1 and is_cb_shop=1 then gmv_usd end)/day_of_week(date_add('day',-1,current_date)) as cb_mall_gmv_wtd,
sum( case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval '7' day and date_trunc('week',date_add('day',-1,current_date)) - interval '1' day and is_official_shop=1 then gmv_usd end)/7 as mall_gmv_w1,
sum( case when grass_date between date_trunc('week',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_official_shop=1 then gmv_usd end)/day_of_week(date_add('day',-1,current_date)) as mall_gmv_wtd,
sum( case when grass_date between date_trunc('month',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_cb_shop=1 then gmv_usd end)/day(date_add('day',-1,current_date)) as cb_gmv_mtd,
sum( case when grass_date between date_trunc('month',date_add('day',-1,current_date)) - interval '1' month and date_trunc('month',date_add('day',-1,current_date)) - interval '1' day and is_cb_shop=1 then gmv_usd end)/day(date_trunc('month',date_add('day',-1,current_date)) - interval '1' day) as cb_gmv_lm,
sum( case when grass_date between date_trunc('week',date_add('day',-1,current_date)) - interval '7' day and date_trunc('week',date_add('day',-1,current_date)) - interval '1' day and is_cb_shop=1 then gmv_usd end)/7 as cb_gmv_w1,
sum( case when grass_date between date_trunc('week',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_cb_shop=1 then gmv_usd end)/day_of_week(date_add('day',-1,current_date)) as cb_gmv_wtd,
sum( case when grass_date between date_trunc('month',date_add('day',-1,current_date)) and date_add('day',-1,current_date) and is_official_shop=1 then gmv_usd end)/day(date_add('day',-1,current_date)) as mall_gmv_mtd,
sum( case when grass_date between date_trunc('month',date_add('day',-1,current_date)) - interval '1' month and date_trunc('month',date_add('day',-1,current_date)) - interval '1' day and is_official_shop=1 then gmv_usd end)/day(date_trunc('month',date_add('day',-1,current_date)) - interval '1' day) as mall_gmv_lm
from mp_order.dwd_order_place_pay_complete_di__reg_s0_live
where 
grass_date between date_trunc('month',date_add('day',-1,current_date)) - interval '1' month and date_add('day',-1,current_date)
and is_placed=1 and tz_type='local' 
group by 1
) 


, total as (
select sum(MTD_shop) as MTD_shop, sum(LM_shop) as LM_shop, sum(cb_mall_ado_mtd) as cb_mall_ado_mtd, sum(cb_mall_ado_lm) as cb_mall_ado_lm, sum(cb_mall_ado_w1) as cb_mall_ado_w1, sum(cb_mall_ado_wtd) as cb_mall_ado_wtd
, sum(cb_ado_mtd) as cb_ado_mtd, sum(cb_ado_lm) as cb_ado_lm, sum(cb_ado_w1) as cb_ado_w1, sum(cb_ado_wtd) as cb_ado_wtd, sum(mall_ado_mtd) as mall_ado_mtd, SUM(mall_ado_lm) as mall_ado_lm
, sum(cb_mall_gmv_mtd) as cb_mall_gmv_mtd, sum(cb_mall_gmv_lm) as cb_mall_gmv_lm, sum(cb_mall_gmv_w1) as cb_mall_gmv_w1, sum(cb_mall_gmv_wtd) as cb_mall_gmv_wtd, sum(cb_gmv_mtd) as cb_gmv_mtd, sum(cb_gmv_lm) as cb_gmv_lm
, sum(cb_gmv_w1) as cb_gmv_w1, sum(cb_gmv_wtd) as cb_gmv_wtd, sum(mall_gmv_mtd) as mall_gmv_mtd, sum( mall_gmv_lm) as mall_gmv_lm, sum(mall_ado_w1) as mall_ado_w1, sum(mall_gmv_w1) as mall_gmv_w1, sum(mall_ado_wtd) as mall_ado_wtd, sum(mall_gmv_wtd) as mall_gmv_wtd
from mall l 
left join o on o.grass_region=l.grass_region
) 



(select m.grass_region, MTD_shop, (MTD_shop- LM_shop) as mall_num_delta, cb_mall_ado_mtd
, (cb_mall_ado_mtd*1.0000/cb_mall_ado_lm)-1 as cb_mall_ado_mom, (mall_ado_mtd*1.0000/mall_ado_lm)-1 as platform_mall_ado_mom
, cb_mall_ado_w1, (cb_mall_ado_wtd*1.0000/cb_mall_ado_w1)-1 AS cb_mall_ado_WOW
, cb_mall_ado_mtd*1.0000/cb_ado_mtd as cb_mall_ado_contri
, (cb_mall_ado_mtd*1.0000/cb_ado_mtd) -(cb_mall_ado_lm*1.0000/cb_ado_lm) as cb_mall_ado_contri_mom
, cb_mall_ado_w1*1.0000/cb_ado_w1 as cb_mall_ado_contri_w1
, (cb_mall_ado_wtd*1.0000/cb_ado_wtd) - (cb_mall_ado_w1*1.0000/cb_ado_w1) as cb_mall_ado_contri_wow
, cb_mall_gmv_mtd
, (cb_mall_gmv_mtd*1.0000/cb_mall_gmv_lm)-1 as cb_mall_gmv_mom, (mall_gmv_mtd*1.0000/mall_gmv_lm)-1 as platform_mall_gmv_mom
, cb_mall_gmv_w1, (cb_mall_gmv_wtd*1.0000/cb_mall_gmv_w1)-1 AS cb_mall_gmv_WOW
, cb_mall_gmv_mtd*1.0000/cb_gmv_mtd as cb_mall_gmv_contri
, (cb_mall_gmv_mtd*1.0000/cb_gmv_mtd) -(cb_mall_gmv_lm*1.0000/cb_gmv_lm) as cb_mall_gmv_contri_mom
, cb_mall_gmv_w1*1.0000/cb_gmv_w1 as cb_mall_gmv_contri_w1
, (cb_mall_gmv_wtd*1.0000/cb_gmv_wtd) - (cb_mall_gmv_w1*1.0000/cb_gmv_w1) as cb_mall_gmv_contri_wow
, cb_mall_gmv_mtd*1.0000/cb_mall_ado_mtd as cb_mtd_abs
, ((cb_mall_gmv_mtd*1.0000/cb_mall_ado_mtd)*1.0000/(cb_mall_gmv_lm*1.0000/cb_mall_ado_lm) -1) as ABS_mom
, mall_ado_w1, (mall_ado_wtd*1.0000/mall_ado_w1)-1 AS mall_ado_WOW
, mall_gmv_w1, (mall_gmv_wtd*1.0000/mall_gmv_w1)-1 AS mall_gmv_WOW
from mall m 
left join o on o.grass_region=m.grass_region)
UNION ALL
(select 'total' as grass_region , MTD_shop, (MTD_shop- LM_shop) as mall_num_delta, cb_mall_ado_mtd
, (cb_mall_ado_mtd*1.0000/cb_mall_ado_lm)-1 as cb_mall_ado_mom, (mall_ado_mtd*1.0000/mall_ado_lm)-1 as platform_mall_ado_mom
, cb_mall_ado_w1, (cb_mall_ado_wtd*1.0000/cb_mall_ado_w1)-1 AS cb_mall_ado_WOW
, cb_mall_ado_mtd*1.0000/cb_ado_mtd as cb_mall_ado_contri
, (cb_mall_ado_mtd*1.0000/cb_ado_mtd) -(cb_mall_ado_lm*1.0000/cb_ado_lm) as cb_mall_ado_contri_mom
, cb_mall_ado_w1*1.0000/cb_ado_w1 as cb_mall_ado_contri_w1
, (cb_mall_ado_wtd*1.0000/cb_ado_wtd) -(cb_mall_ado_w1*1.0000/cb_ado_w1) as cb_mall_ado_contri_wow
, cb_mall_gmv_mtd
, (cb_mall_gmv_mtd*1.0000/cb_mall_gmv_lm)-1 as cb_mall_gmv_mom, (mall_gmv_mtd*1.0000/mall_gmv_lm)-1 as platform_mall_gmv_mom
, cb_mall_gmv_w1, (cb_mall_gmv_wtd*1.0000/cb_mall_gmv_w1)-1 AS cb_mall_gmv_WOW
, cb_mall_gmv_mtd*1.0000/cb_gmv_mtd as cb_mall_gmv_contri
, (cb_mall_gmv_mtd*1.0000/cb_gmv_mtd) -(cb_mall_gmv_lm*1.0000/cb_gmv_lm) as cb_mall_gmv_contri_mom
, cb_mall_gmv_w1*1.0000/cb_gmv_w1 as cb_mall_gmv_contri_w1
, (cb_mall_gmv_wtd*1.0000/cb_gmv_wtd) - (cb_mall_gmv_w1*1.0000/cb_gmv_w1) as cb_mall_gmv_contri_wow
, cb_mall_gmv_mtd*1.0000/cb_mall_ado_mtd as cb_mtd_abs
, ((cb_mall_gmv_mtd*1.0000/cb_mall_ado_mtd)*1.0000/(cb_mall_gmv_lm*1.0000/cb_mall_ado_lm) -1) as ABS_mom
, mall_ado_w1, (mall_ado_wtd*1.0000/mall_ado_w1)-1 AS mall_ado_WOW
, mall_gmv_w1, (mall_gmv_wtd*1.0000/mall_gmv_w1)-1 AS mall_gmv_WOW
from total)
order by 1