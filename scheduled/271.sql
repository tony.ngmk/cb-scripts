insert into nexus.test_158107c1a6a44363b798e37440154459489715e67aaa0e9be0406d7fdf87d66b_7540af3ee119c4e9d5131f43cc764720 WITH dim30 AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE
grass_date >= (current_date - INTERVAL '30' DAY)
AND grass_region IN ('TW', 'MY', 'ID', 'VN', 'TH') 
)


, dim7 AS (
SELECT DISTINCT
grass_date
, grass_region
FROM
mp_order.dim_exchange_rate__reg_s0_live
WHERE
grass_date >= (current_date - INTERVAL '7' DAY)
AND grass_region IN ('TW', 'MY', 'ID', 'VN', 'TH')
)


, raw AS (
SELECT DISTINCT
oversea_orderid
, MAX(local_orderid) AS local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE
date(from_unixtime(ctime)) >= (current_date - INTERVAL '90' DAY)
AND local_country IN ('MY', 'TW', 'ID', 'VN', 'TH')
AND oversea_country IN ('SG', 'MY', 'PH', 'TH', 'VN', 'BR', 'MX', 'CO', 'CL')
GROUP BY 1
)


, sip AS (
SELECT DISTINCT
b.country AS mst_country
, mst_shopid
, affi_shopid
FROM (
SELECT
country
, mst_shopid
, affi_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE
grass_region IN ('SG', 'MY', 'PH', 'TH', 'VN', 'BR', 'MX', 'CO', 'CL')
) AS a
INNER JOIN (
SELECT DISTINCT
shopid
, country
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE
cb_option = 0
AND country IN ('MY', 'TW', 'ID', 'VN', 'TH')
) AS b
ON a.mst_shopid = b.shopid
WHERE
a.country IN ('SG', 'MY', 'PH', 'TH', 'VN', 'BR', 'MX', 'CO', 'CL')
AND b.country IN ('MY', 'TW', 'ID', 'VN', 'TH')
),


net AS (
SELECT DISTINCT
shop_id
, date(from_unixtime(create_timestamp)) AS create_time
, COUNT(DISTINCT order_id) AS gross_order
, COUNT(DISTINCT (CASE
WHEN is_net_order = 1
THEN order_id
ELSE null
END)) AS net_order
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live AS o
WHERE
date(from_unixtime(create_timestamp)) >= (current_date - INTERVAL '30' DAY)
AND tz_type = 'local'
AND o.is_cb_shop = 0
AND grass_region IN ('TW', 'MY', 'ID', 'VN', 'TH')
AND grass_date >= current_date - INTERVAL '31' DAY
GROUP BY 1,2
)


, cancel AS (
SELECT DISTINCT
shopid
, date(from_unixtime(cancel_time)) cancel_time
, COUNT(DISTINCT (CASE
WHEN grass_region = 'TW'
AND extinfo.cancel_reason IN (1, 2, 3, 200, 201, 204, 302)
THEN orderid
WHEN grass_region = 'BR'
AND extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302, 801)
THEN orderid
WHEN NOT grass_region IN ('TW', 'BR')
AND extinfo.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)
THEN orderid
ELSE null
END)) AS cancel_order
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live AS o
WHERE
date(from_unixtime(cancel_time)) >= (current_date - INTERVAL '30' DAY)
AND o.cb_option = 0
AND grass_region IN ('ID', 'TW', 'MY', 'VN', 'TH')
GROUP BY 1,2
)


, rr AS (
SELECT DISTINCT
shopid
, date(from_unixtime(mtime)) AS mtime
, COUNT(DISTINCT (CASE
WHEN grass_region = 'TW'
AND reason IN (1, 2, 3, 103, 105)
AND (cancel_reason = 0 OR cancel_reason IS NULL)
AND status IN (2, 5)
THEN a.orderid
WHEN grass_region <> 'TW'
AND reason IN (1, 2, 3, 103, 105, 107)
AND (cancel_reason = 0 OR cancel_reason IS NULL)
AND status IN (2, 5)
THEN a.orderid
ELSE null
END)) AS RR_order
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
orderid,
extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE
cb_option = 0
AND grass_region IN ('TW', 'MY', 'ID', 'VN', 'TH')
AND date(from_unixtime(cancel_time)) >= (current_date - INTERVAL '30' DAY)
) AS b
ON (a.orderid = b.orderid)
WHERE
date(from_unixtime(mtime)) >= (current_date - INTERVAL '30' DAY)
AND a.cb_option = 0
AND (grass_region IN ('ID', 'MY', 'TW', 'VN', 'TH'))
GROUP BY 1,2
)


, output30 AS (
SELECT DISTINCT
sip.mst_country
, sip.mst_shopid
, COALESCE(SUM(cancel_order), 0) * DECIMAL '1.000000'
/ CAST((SUM(cancel_order) + COALESCE(SUM(RR_order), 0) + SUM(net_order)) AS double) AS l30d_overall_cancel_rate
FROM
dim30
INNER JOIN sip
ON dim30.grass_region = sip.mst_country
LEFT JOIN net
ON sip.mst_shopid = net.shop_id
AND dim30.grass_date = net.create_time
LEFT JOIN cancel
ON sip.mst_shopid = cancel.shopid
AND dim30.grass_date = cancel.cancel_time
LEFT JOIN rr
ON sip.mst_shopid = rr.shopid
AND dim30.grass_date = rr.mtime
GROUP BY 1,2
)


, output7 AS (
SELECT DISTINCT
sip.mst_country
, sip.mst_shopid
, COALESCE(SUM(cancel_order), 0) * DECIMAL '1.000000'
/ CAST((SUM(cancel_order) + COALESCE(SUM(RR_order), 0) + SUM(net_order)) AS double) AS l7d_overall_cancel_rate
FROM
dim7
INNER JOIN sip
ON dim7.grass_region = sip.mst_country
LEFT JOIN net
ON sip.mst_shopid = net.shop_id
AND dim7.grass_date = net.create_time
LEFT JOIN cancel
ON sip.mst_shopid = cancel.shopid
AND dim7.grass_date = cancel.cancel_time
LEFT JOIN rr
ON sip.mst_shopid = rr.shopid
AND dim7.grass_date = rr.mtime
GROUP BY 1,2
)


SELECT DISTINCT
a.affi_shopid
, op7.mst_country
, sip.mst_shopid
, l7d_overall_cancel_rate
, l30d_overall_cancel_rate
FROM
regcbbi_others.shopee_regional_cb_team__local_campaign_shop_item_model_lvl AS a
LEFT JOIN sip
ON CAST(a.affi_shopid AS bigint) = sip.affi_shopid
LEFT JOIN output7 AS op7
ON op7.mst_shopid = sip.mst_shopid
LEFT JOIN output30 AS op30
ON op30.mst_shopid = sip.mst_shopid
WHERE
op7.mst_country IN ('TW', 'MY', 'ID', 'VN', 'TH')