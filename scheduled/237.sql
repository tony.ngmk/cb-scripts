insert into nexus.test_684d027fd3b28134627e8b8b7b8234b9c798f9652c716afb1756f60bb0b4ed5a_755f56ef2423187e715caf6df9021758 WITH sip AS (SELECT DISTINCT
grass_region
, grass_date 
, shop_id
, source
FROM
regcbbi_others.shop_profile
WHERE grass_date = current_date - INTERVAL '1' DAY
AND regexp_like(source, 'Local SIP'))

, ord AS (
SELECT DISTINCT o.*
FROM
((
SELECT DISTINCT
DATE(CAST(create_datetime AS TIMESTAMP)) grass_date
, grass_region
, shop_id
, item_id
, model_id
, item_price_pp
, item_amount
, order_id
, CAST(create_datetime AS timestamp) create_datetime
, order_fraction
, gmv_usd
, gmv
, actual_shipping_rebate_by_seller_amt_usd
, estimate_shipping_rebate_by_seller_amt_usd
, seller_snapshot_discount_shipping_fee_usd
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (((((DATE(CAST(create_datetime AS TIMESTAMP)) BETWEEN (current_date - INTERVAL '100' DAY) AND (current_date - INTERVAL '1' DAY)) 
-- AND (is_placed = 1)
)
AND (is_cb_shop = 1)) 
) AND (tz_type = 'local')
AND shipping_discount_promotion_type = 'seller'
-- AND estimate_shipping_rebate_by_seller_amt_usd > 0
)
) o
INNER JOIN (
SELECT DISTINCT
local_orderid
, oversea_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE (((schema <> '')) AND ((local_orderid <> 0) AND (local_orderid IS NOT NULL)))
) map ON (o.order_id = map.oversea_orderid))
) 




SELECT DISTINCT
ord.grass_date
, ord.grass_region
, ord.shop_id
, source
, ord.order_id
, "sum"(item_amount) amount
, "sum"(-actual_shipping_rebate_by_seller_amt_usd) actual_rebate_usd
, "sum"(-estimate_shipping_rebate_by_seller_amt_usd) est_rebate_usd
, "sum"(-seller_snapshot_discount_shipping_fee_usd) offered_rebate_usd
, sum(gmv_usd) gmv_usd
-- , remark
FROM
(ord
INNER JOIN sip ON ord.grass_region = sip.grass_region 
AND ord.shop_id = sip.shop_id 
)
GROUP BY 1, 2, 3, 4, 5