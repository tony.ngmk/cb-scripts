insert into nexus.test_af3f3da5bcb664ce9da2923dfab5ce023df377d8b2ed01e9d523345f998a5ae4_7cd437fe6e9088a4fd2b5950a55e4a4f with price_config AS (SELECT grass_region
, currency
, CAST(sip_exchange AS DOUBLE) sip_exchange 
FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2
WHERE cb_option = '1'
AND ingestion_timestamp = (SELECT MAX(ingestion_timestamp) max FROM regcbbi_others.sip__sip_exchange__tmp__reg__s2)
)




,sip_weight_upward as (select distinct order_sn 
, sum(affi_real_weight*item_amount) sip_weight 
, sum(seller_input_order_weight) seller_input_order_weight 
from dev_regcbbi_others.sip_cb_sip_order_sip_weight_df_reg_s0 
where date(create_datetime) >=date'2022-04-01'
group by 1 
having sum(affi_real_weight*item_amount) >= sum(seller_input_order_weight) 
)


, sku as (select 
distinct item_id affi_itemid 
, primary_item_id mst_itemid 
, item_real_weight/100.00 item_real_weight
, status 
, seller_status
from mp_item.ads_sip_affi_item_profile__reg_s0_live
where grass_date = if(grass_region in ('SG','MY','PH','TH','ID','VN','TW'), current_date- interval'1'day ,current_date- interval'2'day )
and grass_date >= current_date- interval'2'day
and primary_shop_type=1 and shop_status=1 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')
and primary_seller_region in ('TW','MY','BR') and tz_type='local' 
and shop_status=1 
)


, wh as (select distinct case when p.mst_itemid is not null then p.mst_itemid else a.mst_itemid end as mst_itemid 
from dev_regcbbi_others.sip__item_weight_adj_order__df__reg__s0 o 
left join sku p on p.mst_itemid = o.item_id 
left join sku a on a.affi_itemid = o.item_id 
where actual_weight >0 and (p.mst_itemid is not null or a.affi_itemid is not null)
)
, margin AS (SELECT DISTINCT grass_region
, mst_country
, CAST(sip_margin AS DOUBLE)/100.00 AS sip_margin
, CAST(valid_from AS DATE) valid_from
, CAST(valid_to AS DATE) valid_to
FROM regcbbi_others.sip__country_margin__tmp__reg__s2
WHERE ingestion_timestamp = (SELECT MAX(ingestion_timestamp) max_ingestion FROM regcbbi_others.sip__country_margin__tmp__reg__s2)
AND cb_option = '1'
)


-- all settlement related amount converted to USD using SIP system exchange rates
, o AS (
SELECT DISTINCT 
o.wh_outbound_date
, o.grass_date AS create_date
, o.mst_country
, o.grass_region
, o.order_id
, o.order_sn
, o.payment_method_id
, o.logistics_status_id
, asf_sls
, o.actual_buyer_paid_shipping_fee
, o.actual_shipping_rebate_by_shopee_amt
, o.pv_rebate_by_shopee_amt
, o.sv_rebate_by_shopee_amt
, o.actual_shipping_rebate_by_seller_amt
, o.seller_snapshot_discount_shipping_fee
, o.escrow_to_seller_amt
, o.gmv
, o.gmv_usd


, actual_weight
, reason
, o.shop_id
, mst_shopid
, sip_rate
, shop_margin
, sip_margin
, o.sip_settlement --order level stmt 
, sip_seller_comm_fee -- SIP charge seller
, sip_seller_service_fee -- SIP charge seller
, pc3.sip_exchange AS affi_exchange
, pc3.currency AS affi_currency
, o.offline_exchange AS offline_ord_exchange
, o.offline_currency AS offline_ord_currency
FROM regcbbi_others.sip_cn_pnl_order_tab_v2 o 
LEFT JOIN (SELECT *
FROM price_config) pc3 
ON o.grass_region = pc3.grass_region -- USD / affi curr
LEFT JOIN margin
ON o.grass_region = margin.grass_region
AND o.mst_country = margin.mst_country
AND o.grass_date BETWEEN margin.valid_from AND margin.valid_to 
WHERE (o.wh_inbound_date + interval '1' day < o.cancel_timestamp OR o.cancel_user_id IS NULL)
)


, oi AS (SELECT DISTINCT 
order_id
, order_sn 
, max(seller_status+status) overall_status 
, max(sku_level_update) sku_level_update
, min(item_real_weight) item_real_weight
, SUM(item_amount) AS item_amount
, SUM(item_weight_pp) AS item_weight_pp
, SUM(item_price_pp) AS item_price_pp
, SUM(order_price_pp) AS order_price_pp
, SUM(affi_hpfn) AS affi_hpfn 
FROM (SELECT DISTINCT 
i.order_id
, i.order_sn
, i.item_id
, case when wh.mst_itemid is not null then 1 else 0 end as sku_level_update 
, item_real_weight
, status 
, seller_status
, SUM(commission_base_amt) commission_base_amt
, sum(actual_shipping_fee) actual_shipping_fee
, sum(actual_shipping_rebate_by_shopee_amt) actual_shipping_rebate_by_shopee_amt
, sum(affi_hpfn*item_amount) affi_hpfn
, sum(actual_buyer_paid_shipping_fee) actual_buyer_paid_shipping_fee
, SUM(item_amount) AS item_amount
, SUM(commission_fee) AS commission_fee 
, SUM(i.service_fee) AS service_fee_item 
, SUM(item_tax_amt) AS item_tax_amt
, SUM(item_tax_exemption_amt) AS item_tax_exemption_amt
, SUM(item_weight_pp*item_amount) AS item_weight_pp
, SUM(item_price_pp*item_amount) AS item_price_pp
, SUM(order_price_pp*item_amount) AS order_price_pp 
FROM regcbbi_others.sip_cn_pnl_item_tab i
join sku on sku.affi_itemid = i.item_id 
left join (select mst_itemid 
from wh) wh on wh.mst_itemid = sku.mst_itemid 
GROUP BY 1,2,3,4,5 , 6,7 
)
GROUP BY 1, 2
) 

, r AS (
SELECT DISTINCT wh_outbound_date
, create_date
, mst_country
, grass_region
, mst_shopid
, o.shop_id
, o.order_id
, o.order_sn
, shop_margin AS current_shop_margin
, sip_rate
, affi_exchange
, case when item_real_weight >0 then 'Y' else 'N' end as has_sip_weight 
, case when overall_status=2 then 'Y' else 'N' end as live_sku
, case when item_real_weight > 0 and sku_level_update =1 then 'Y' else 'N' end as sku_level_update 
, case when swu.order_sn is not null then 'Y' else 'N' end as upward_update 
, SUM(escrow_to_seller_amt) escrow_to_seller_amt
, SUM(gmv) AS gmv
, sum(gmv_usd) gmv_usd
, SUM(asf_sls) AS actual_shipping_fee
, SUM(o.actual_buyer_paid_shipping_fee) AS actual_buyer_paid_shipping_fee
, SUM(o.actual_shipping_rebate_by_shopee_amt) AS actual_shipping_rebate_by_shopee_amt 
, SUM(actual_shipping_rebate_by_seller_amt) actual_shipping_rebate_by_seller_amt
, SUM(oi.affi_hpfn) AS affi_hpfn
, SUM(item_weight_pp) AS item_weight
, SUM(actual_weight) AS actual_weight

FROM o
LEFT JOIN oi ON o.order_id = oi.order_id
left join sip_weight_upward swu on swu.order_sn = o.order_sn 
WHERE wh_outbound_date = CURRENT_DATE - INTERVAL '1' DAY AND mst_country <> 'SG'
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15


)
-- select * 
-- from r 






SELECT DISTINCT r.mst_country 
, grass_region affi_country 
, wh_outbound_date
, has_sip_weight
, case when has_sip_weight='Y' and sku_level_update='Y' then 'Y' else 'N' end as sku_level_update
, case when has_sip_weight='Y' and sku_level_update='N' then 'Y' else 'N' end as cat_level_update
, live_sku 
, case when sku_level_update = 'Y' and has_sip_weight='Y' and upward_update ='Y' then 'Y' else 'N' end as upward_update
, case when sku_level_update = 'Y' and has_sip_weight='Y' and upward_update ='N' then 'Y' else 'N' end as downward_update 
, sum((actual_buyer_paid_shipping_fee + actual_shipping_rebate_by_shopee_amt + affi_hpfn - actual_shipping_fee)*decimal'1.000'/affi_exchange) AS shipping_loss_usd 
, count(distinct order_sn) orders 
, count(distinct case when (actual_buyer_paid_shipping_fee + actual_shipping_rebate_by_shopee_amt + affi_hpfn - actual_shipping_fee)*decimal'1.000'/affi_exchange <0 then order_sn 
else null end
) loss_order 
, sum(gmv_usd) gmv_usd
, avg(actual_weight) avg_actual_weight
, sum(actual_weight) sum_actual_weight
, avg(actual_shipping_fee*decimal'1.000'/affi_exchange) avg_shipping_fee_usd
, sum(actual_shipping_fee*decimal'1.000'/affi_exchange) sum_shipping_fee_usd 
FROM r 
group by 1,2 , 3 ,4 , 5 ,6 , 7 , 8 , 9