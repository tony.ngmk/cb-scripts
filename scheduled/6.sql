WITH map AS
(
SELECT DISTINCT feature_group,
feature,
grass_date AS dt
FROM mp_foa.ads_feature_lead_1d__reg_s0_live
WHERE grass_region='PH'
AND grass_date BETWEEN date_add('month',-1,date_trunc('month',date_add('day',-1,CURRENT_DATE))) AND date_add('day',-1,CURRENT_DATE)
AND (
feature_group='Others'
OR feature!='Others')
AND impress_cnt_item_direct_lead_1d>0
AND feature_group IN ('Search',
'You May Also Like',
'Daily Discover',
'From the Same Shop',
'Similar Products',
'Product Hot Sale',
'Shop Homepage Recommendation',
'Bundle Deal',
'My Like',
'Microsite',
'Add-on Deal',
'Flash Sales' )
)
,
cat_map AS
(
SELECT DISTINCT cast(grass_region AS varchar) grass_region ,
cast(l1_cat_id AS int) l1_cat_id ,
cast(l1_cat AS varchar) AS l1_cat ,
cast(cluster AS varchar) AS cluster
FROM regcbbi_others.shopee_regional_cb_team__regional_cb_cluster_mapping
WHERE cast(ingestion_timestamp AS bigint) =
(
SELECT max(cast(ingestion_timestamp AS bigint)) aa
FROM regcbbi_others.shopee_regional_cb_team__regional_cb_cluster_mapping
WHERE try_cast(ingestion_timestamp AS bigint) > 0)
AND grass_region='PH'
)
,
cb_item AS
(
SELECT DISTINCT item_id,
is_cb_shop,
cluster
FROM (
SELECT item_id,
is_cb_shop,
level1_category
FROM mp_item.dim_item__reg_s0_live
WHERE grass_region='PH'
AND grass_date=date_add('day',-1,CURRENT_DATE)
AND tz_type='local') a
LEFT JOIN cat_map c
ON c.l1_cat=a.level1_category
)
,
imp AS
(
SELECT date_add('day',-1,CURRENT_DATE) AS grass_date,
feature_group,
cluster ,
sum(
CASE
WHEN a.grass_date = date_add('day',-1,CURRENT_DATE)
AND is_cb_shop=1 THEN impress_cnt_direct_lead
END) AS cb_impress_cnt_direct_lead ,
sum(
CASE
WHEN a.grass_date = date_add('day',-1,CURRENT_DATE)
AND is_cb_shop=0 THEN impress_cnt_direct_lead
END) AS lc_impress_cnt_direct_lead ,
sum(
CASE
WHEN a.grass_date >= date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN impress_cnt_direct_lead
END)*1.0000/day_of_week("date_add"('day', -1, CURRENT_DATE)) AS cb_impress_wtd ,
sum(
CASE
WHEN a.grass_date >= date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN impress_cnt_direct_lead
END)*1.0000/day_of_week("date_add"('day', -1, CURRENT_DATE)) AS lc_impress_wtd ,
sum(
CASE
WHEN grass_date BETWEEN date_trunc('week',date_add('day',-1,CURRENT_DATE)) - interval'7'day AND date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN impress_cnt_direct_lead
END)*1.0000/7 AS cb_impress_lw ,
sum(
CASE
WHEN grass_date BETWEEN date_trunc('week',date_add('day',-1,CURRENT_DATE)) - interval'7'day AND date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN impress_cnt_direct_lead
END)*1.0000/7 AS lc_impress_lw ,
sum(
CASE
WHEN grass_date >= date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN impress_cnt_direct_lead
END)*1.0000/day("date_add"('day', -1, CURRENT_DATE)) AS cb_impress_mtd ,
sum(
CASE
WHEN grass_date >= date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN impress_cnt_direct_lead
END)*1.0000/day("date_add"('day', -1, CURRENT_DATE)) AS lc_impress_mtd ,
sum(
CASE
WHEN grass_date < date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN impress_cnt_direct_lead
END)*1.0000/day(date_trunc('month',date_add('day',-1,CURRENT_DATE)) - interval'1'day) AS cb_impress_lm ,
sum(
CASE
WHEN grass_date < date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN impress_cnt_direct_lead
END)*1.0000/day(date_trunc('month',date_add('day',-1,CURRENT_DATE)) - interval'1'day) AS lc_impress_lm
FROM (
SELECT grass_date,
step1_feature,
impress_cnt_direct_lead,
item_id
FROM mp_foa.dwd_eventid_impress_di__reg_s0_live
WHERE grass_region='PH'
AND grass_date BETWEEN date_add('month',-1,date_trunc('month',date_add('day',-1,CURRENT_DATE))) AND date_add('day',-1,CURRENT_DATE)
AND tz_type='local' ) a
JOIN map p
ON p.feature=a.step1_feature
AND a.grass_date=p.dt
JOIN cb_item b
ON a.item_id=b.item_id
GROUP BY 1,
2,
3
)
,
pdp_v AS
(
SELECT date_add('day',-1,CURRENT_DATE) AS grass_date,
feature_group,
cluster ,
sum(
CASE
WHEN grass_date = date_add('day',-1,CURRENT_DATE)
AND is_cb_shop=1 THEN pv_direct_lead
END) AS cb_pv_direct_lead ,
sum(
CASE
WHEN grass_date = date_add('day',-1,CURRENT_DATE)
AND is_cb_shop=0 THEN pv_direct_lead
END) AS lc_pv_direct_lead ,
sum(
CASE
WHEN grass_date >= date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN pv_direct_lead
END)*1.0000/day_of_week("date_add"('day', -1, CURRENT_DATE)) AS cb_pv_wtd ,
sum(
CASE
WHEN grass_date >= date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN pv_direct_lead
END)*1.0000/day_of_week("date_add"('day', -1, CURRENT_DATE)) AS lc_pv_wtd ,
sum(
CASE
WHEN grass_date BETWEEN date_trunc('week',date_add('day',-1,CURRENT_DATE)) - interval'7'day AND date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN pv_direct_lead
END)*1.0000/7 AS cb_pv_lw ,
sum(
CASE
WHEN grass_date BETWEEN date_trunc('week',date_add('day',-1,CURRENT_DATE)) - interval'7'day AND date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN pv_direct_lead
END)*1.0000/7 AS lc_pv_lw ,
sum(
CASE
WHEN grass_date >= date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN pv_direct_lead
END)*1.0000/day("date_add"('day', -1, CURRENT_DATE)) AS cb_pv_mtd ,
sum(
CASE
WHEN grass_date >= date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN pv_direct_lead
END)*1.0000/day("date_add"('day', -1, CURRENT_DATE)) AS lc_pv_mtd ,
sum(
CASE
WHEN grass_date < date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN pv_direct_lead
END)*1.0000/day(date_trunc('month',date_add('day',-1,CURRENT_DATE)) - interval'1'day) AS cb_pv_lm ,
sum(
CASE
WHEN grass_date < date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN pv_direct_lead
END)*1.0000/day(date_trunc('month',date_add('day',-1,CURRENT_DATE)) - interval'1'day) AS lc_pv_lm
FROM (
SELECT grass_date,
step1_feature,
pv_direct_lead,
item_id
FROM mp_foa.dwd_eventid_view_di__reg_s0_live a
WHERE grass_region='PH'
AND grass_date BETWEEN date_add('month',-1,date_trunc('month',date_add('day',-1,CURRENT_DATE))) AND date_add('day',-1,CURRENT_DATE)
AND tz_type='local'
AND page_type='product' ) a
JOIN cb_item b
ON a.item_id=b.item_id
JOIN map p
ON p.feature=a.step1_feature
AND a.grass_date=p.dt
GROUP BY 1,
2,
3
)
,
ord AS
(
SELECT date_add('day',-1,CURRENT_DATE) AS grass_date,
feature_group,
cluster ,
sum(
CASE
WHEN grass_date = date_add('day',-1,CURRENT_DATE)
AND is_cb_shop=1 THEN place_order_cnt_direct_lead
END) AS cb_order_direct_lead ,
sum(
CASE
WHEN grass_date = date_add('day',-1,CURRENT_DATE)
AND is_cb_shop=0 THEN place_order_cnt_direct_lead
END) AS lc_order_direct_lead ,
sum(
CASE
WHEN grass_date >= date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN place_order_cnt_direct_lead
END)*1.0000/day_of_week("date_add"('day', -1, CURRENT_DATE)) AS cb_order_wtd ,
sum(
CASE
WHEN grass_date >= date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN place_order_cnt_direct_lead
END)*1.0000/day_of_week("date_add"('day', -1, CURRENT_DATE)) AS lc_order_wtd ,
sum(
CASE
WHEN grass_date BETWEEN date_trunc('week',date_add('day',-1,CURRENT_DATE)) - interval'7'day AND date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN place_order_cnt_direct_lead
END)*1.0000/7 AS cb_order_lw ,
sum(
CASE
WHEN grass_date BETWEEN date_trunc('week',date_add('day',-1,CURRENT_DATE)) - interval'7'day AND date_trunc('week',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN place_order_cnt_direct_lead
END)*1.0000/7 AS lc_order_lw ,
sum(
CASE
WHEN grass_date >= date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN place_order_cnt_direct_lead
END)*1.0000/day("date_add"('day', -1, CURRENT_DATE)) AS cb_order_mtd ,
sum(
CASE
WHEN grass_date >= date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN place_order_cnt_direct_lead
END)*1.0000/day("date_add"('day', -1, CURRENT_DATE)) AS lc_order_mtd ,
sum(
CASE
WHEN grass_date < date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=1 THEN place_order_cnt_direct_lead
END)*1.0000/day(date_trunc('month',date_add('day',-1,CURRENT_DATE)) - interval'1'day) AS cb_order_lm ,
sum(
CASE
WHEN grass_date < date_trunc('month',date_add('day',-1,CURRENT_DATE))
AND is_cb_shop=0 THEN place_order_cnt_direct_lead
END)*1.0000/day(date_trunc('month',date_add('day',-1,CURRENT_DATE)) - interval'1'day) AS lc_order_lm
FROM (
SELECT grass_date,
step1_feature,
place_order_cnt_direct_lead,
item_id
FROM mp_foa.dwd_eventid_item_order_events_di__reg_s0_live a
WHERE grass_region='PH'
AND grass_date BETWEEN date_add('month',-1,date_trunc('month',date_add('day',-1,CURRENT_DATE))) AND date_add('day',-1,CURRENT_DATE) ) a
JOIN cb_item b
ON a.item_id=b.item_id
JOIN map p
ON p.feature=a.step1_feature
AND a.grass_date=p.dt
GROUP BY 1,
2,
3
)
(
SELECT p.grass_date,
p.feature_group
--, cb_impress_cnt_direct_lead,lc_impress_cnt_direct_lead, cb_pv_direct_lead, lc_pv_direct_lead, cb_order_direct_lead,lc_order_direct_lead
,
cb_impress_wtd,
lc_impress_wtd,
cb_pv_wtd,
lc_pv_wtd,
cb_order_wtd,
lc_order_wtd ,
cb_pv_wtd*1.0000/cb_impress_wtd AS cb_ctr,
(cb_pv_wtd*1.0000/cb_impress_wtd)/((lc_pv_wtd+cb_pv_wtd)*1.0000/(lc_impress_wtd+cb_impress_wtd)) AS cb_ctr_pene,
cb_order_wtd*1.0000/cb_pv_wtd AS cb_cr,
(cb_order_wtd*1.0000/cb_pv_wtd)/((lc_order_wtd+cb_order_wtd)*1.0000/(lc_pv_wtd+cb_pv_wtd)) AS cb_cr_pene ,
cb_impress_wtd*1.0000/ sum(cb_impress_wtd+lc_impress_wtd) OVER (partition BY p.grass_date ) - cb_impress_lw*1.0000/ sum(cb_impress_lw+lc_impress_lw) OVER (partition BY p.grass_date) AS cb_impress_pct_wow ,
cb_impress_wtd*1.0000/(lc_impress_wtd+cb_impress_wtd) - cb_impress_lw*1.0000/(lc_impress_lw+cb_impress_lw) AS cb_impress_pene_wow ,
(cb_pv_wtd *1.0000/cb_impress_wtd)/((lc_pv_wtd+cb_pv_wtd)*1.0000/(lc_impress_wtd+cb_impress_wtd)) - (cb_pv_lw*1.0000/cb_impress_lw)/((lc_pv_lw+cb_pv_lw)*1.0000/(lc_impress_lw+cb_impress_lw)) AS cb_ctr_pene_wow ,
(cb_order_wtd*1.0000/cb_pv_wtd)/((lc_order_wtd+cb_order_wtd)*1.0000/(lc_pv_wtd+cb_pv_wtd)) - (cb_order_lw*1.0000/cb_pv_lw)/((lc_order_lw+cb_order_lw)*1.0000/(lc_pv_lw+cb_pv_lw)) AS cb_cr_pene_wow ,
cb_order_wtd *1.0000/(lc_order_wtd+cb_order_wtd) - cb_order_lw*1.0000/(lc_order_lw+cb_order_lw) AS cb_ord_pene_wow ,
cb_impress_mtd*1.0000/ sum(cb_impress_mtd+lc_impress_mtd) OVER (partition BY p.grass_date ) - cb_impress_lm*1.0000/ sum(cb_impress_lm+lc_impress_lm) OVER (partition BY p.grass_date) AS cb_impress_pct_mom ,
cb_impress_mtd*1.0000/(lc_impress_mtd+cb_impress_mtd) - cb_impress_lm*1.0000/(lc_impress_lm+cb_impress_lm) AS cb_impress_pene_mom ,
(cb_pv_mtd *1.0000/cb_impress_mtd)/((lc_pv_mtd+cb_pv_mtd)*1.0000/(lc_impress_mtd+cb_impress_mtd)) - (cb_pv_lm*1.0000/cb_impress_lm)/((lc_pv_lm+cb_pv_lm)*1.0000/(lc_impress_lm+cb_impress_lm)) AS cb_ctr_pene_mom ,
(cb_order_mtd*1.0000/cb_pv_mtd)/((lc_order_mtd+cb_order_mtd)*1.0000/(lc_pv_mtd+cb_pv_mtd)) - (cb_order_lm*1.0000/cb_pv_lm)/((lc_order_lm+cb_order_lm)*1.0000/(lc_pv_lm+cb_pv_lm)) AS cb_cr_pene_mom ,
cb_order_mtd*1.0000/(lc_order_mtd+cb_order_mtd) - cb_order_lm*1.0000/(lc_order_lm+cb_order_lm) AS cb_ord_pene_mom ,
(cb_impress_lw +lc_impress_lw) AS tl_impress_lw,
(cb_impress_mtd+lc_impress_mtd) AS tl_impress_mtd,
(cb_impress_lm +lc_impress_lm) AS tl_impress_lm ,
p.cluster,
cb_impress_mtd,
(cb_pv_wtd +lc_pv_wtd)*1.0000/(lc_impress_wtd+lc_impress_wtd) AS tl_ctr,
(cb_order_wtd+lc_order_wtd)*1.0000/(cb_pv_wtd+lc_pv_wtd) AS tl_cr ,
cb_impress_mtd*1.0000/(lc_impress_mtd+cb_impress_mtd) AS cb_impress_pene_mtd ,
(cb_pv_mtd *1.0000/cb_impress_mtd)/((lc_pv_mtd+cb_pv_mtd)*1.0000/(lc_impress_mtd+cb_impress_mtd)) AS cb_ctr_pene_mtd ,
(cb_order_mtd*1.0000/cb_pv_mtd)/((lc_order_mtd+cb_order_mtd)*1.0000/(lc_pv_mtd+cb_pv_mtd)) AS cb_cr_pene_mtd ,
cb_order_mtd*1.0000/(lc_order_mtd+cb_order_mtd) AS cb_ord_pene_mtd
FROM imp p
LEFT JOIN pdp_v v
ON p.feature_group=v.feature_group
AND p.cluster=v.cluster
LEFT JOIN ord o
ON p.feature_group=o.feature_group
AND p.cluster=o.cluster )
) o on p.grass_date=o.grass_date
)
ORDER BY 3 DESC