insert into nexus.test_47a00c6abb724cd362a09476a1dc6c0e83e0830959e701c508cb306492e2d117_2f0da5b52749383aceb319811389c060 WITH fss_ccb as 
(
select 
grass_region
, shopid
, sum(case when start_date <= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) and end_date >= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) then is_fss else 0 end) as M1_fss
, sum(case when start_date <= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) and end_date >= date_add('day', -1, date_trunc('month', date_add('day', -1, current_date))) then is_ccb else 0 end) as M1_ccb
, sum(case when start_date <= date_add('day', -1, current_date) and end_date >= date_add('day', -1, current_date) then is_fss else 0 end) as D1_fss
, sum(case when start_date <= date_add('day', -1, current_date) and end_date >= date_add('day', -1, current_date) then is_ccb else 0 end) as D1_ccb
-- , sum(case when start_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and end_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) then is_fss else 0 end) as W2_fss
-- , sum(case when start_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and end_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) then is_ccb else 0 end) as W2_ccb
-- , sum(case when start_date < date_trunc('week', date_add('day', -1, current_date)) and end_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) then is_fss else 0 end) as W1_fss
-- , sum(case when start_date < date_trunc('week', date_add('day', -1, current_date)) and end_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) then is_ccb else 0 end) as W1_ccb 


from 
(
select distinct 
r1.region as grass_region
, r1.rule_id
, shop_id as shopid 
, case when (rule_name like '%FSS%' or rule_name like '%FSC%' or r1.rule_name like '%535%' or r1.rule_name like '%321%' or lower(r1.rule_name) like '%cn exclusivity%' or lower(r1.rule_name) like '%freeship xtra%' or r1.rule_name like '%FSP%' ) then 1 else 0 end as is_fss
, case when (rule_name like '%CCB%' or rule_name like '%FSC%' or rule_name like '%CBXTRA%' or r1.rule_name like '%535%' or r1.rule_name like '%321%' or lower(r1.rule_name) like '%coin cashback%' or r1.rule_name like '%10pct%') then 1 else 0 end as is_ccb
, date(from_unixtime(r2.start_time)) as start_date
, date(from_unixtime(r2.end_time)) as end_date
from marketplace.shopee_service_fee_rule_db__service_fee_rule_tab__reg_daily_s0_live r1 
join marketplace.shopee_service_fee_rule_shop_db__service_fee_rule_shop_tab__reg_daily_s0_live r2 on r1.rule_id = r2.rule_id
where (r1.rule_name like '%FSS%' or r1.rule_name like '%CCB%' or r1.rule_name like '%FSC%' or r1.rule_name like '%CBXTRA%' or r1.rule_name like '%535%' or r1.rule_name like '%321%' or lower(r1.rule_name) like '%cn exclusivity%' or lower(r1.rule_name) like '%freeship xtra%' or lower(r1.rule_name) like '%coin cashback%' or r1.rule_name like '%FSP%' or r1.rule_name like '%10pct%')
and date(from_unixtime(r2.start_time)) <= date_add('day', -1, current_date) -- End of date_add('day', -1, current_date)
--and date(from_unixtime(r2.end_time)) >= date_add('day', -1, current_date)
and date(from_unixtime(r2.end_time)) >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) -- Start of M-1
and r1.rule_status = 1
)
group by 1, 2
)




, st as 
(
SELECT 
s.grass_region
, s.shop_id
, s.is_cb_shop
, c.gp_account_billing_country as origin
, case when sip.affi_shopid is not NULL and c.gp_account_billing_country = 'China' then 'TB SIP'
WHEN sip.affi_shopid is not NULL and c.gp_account_billing_country IN ('Taiwan', 'Malaysia', 'Indonesia', 'Vietnam') then 'Local SIP'
else null end as sip_type
, IF(s.grass_region = 'SG' and sip.affi_shopid is not null, 1, D1_fss) as D1_fss
, IF(s.grass_region = 'SG' and sip.affi_shopid is not null, 1, D1_ccb) as D1_ccb
, IF(s.grass_region = 'SG' and sip.affi_shopid is not null, 1, M1_fss) as M1_fss
, IF(s.grass_region = 'SG' and sip.affi_shopid is not null, 1, M1_ccb) as M1_ccb
FROM 
(
SELECT distinct grass_region, shop_id, is_cb_shop 
FROM mp_user.dim_shop__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = date_add('day', -1, current_date)
AND status = 1
) s 
JOIN 
(
SELECT distinct grass_region, shop_id
FROM mp_user.dim_user__reg_s0_live
WHERE tz_type = 'local'
AND grass_date = date_add('day', -1, current_date)
AND status = 1
) u on s.shop_id = u.shop_id
left join fss_ccb on s.shop_id = fss_ccb.shopid
LEFT JOIN regbd_sf.cb__seller_index_tab c on s.shop_id = cast(c.child_shopid as bigint)
LEFT JOIN 
(
SELECT * 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live 
WHERE grass_region != ''
) sip on s.shop_id = cast(sip.affi_shopid as bigint)
where c.grass_region != ''
)


, shop_count as 
(
SELECT 
grass_region
, count(distinct case when D1_fss > 0 then shop_id else null end) as D1_FSS_shops
, count(distinct case when D1_ccb > 0 then shop_id else null end) as D1_CCB_shops
, count(distinct case when M1_fss > 0 then shop_id else null end) as M1_FSS_shops
, count(distinct case when M1_ccb > 0 then shop_id else null end) as M1_CCB_shops 


, count(distinct case when D1_fss > 0 and origin = 'China' then shop_id else null end) as D1_CNCB_FSS_shops
, count(distinct case when D1_ccb > 0 and origin = 'China' then shop_id else null end) as D1_CNCB_CCB_shops
, count(distinct case when M1_fss > 0 and origin = 'China' then shop_id else null end) as M1_CNCB_FSS_shops
, count(distinct case when M1_ccb > 0 and origin = 'China' then shop_id else null end) as M1_CNCB_CCB_shops


, count(distinct case when D1_fss > 0 and origin = 'Korea' then shop_id else null end) as D1_KRCB_FSS_shops
, count(distinct case when D1_ccb > 0 and origin = 'Korea' then shop_id else null end) as D1_KRCB_CCB_shops 
, count(distinct case when M1_fss > 0 and origin = 'Korea' then shop_id else null end) as M1_KRCB_FSS_shops
, count(distinct case when M1_ccb > 0 and origin = 'Korea' then shop_id else null end) as M1_KRCB_CCB_shops 


, count(distinct case when D1_fss > 0 and origin = 'Japan' then shop_id else null end) as D1_JPCB_FSS_shops
, count(distinct case when D1_ccb > 0 and origin = 'Japan' then shop_id else null end) as D1_JPCB_CCB_shops 
, count(distinct case when M1_fss > 0 and origin = 'Japan' then shop_id else null end) as M1_JPCB_FSS_shops
, count(distinct case when M1_ccb > 0 and origin = 'Japan' then shop_id else null end) as M1_JPCB_CCB_shops 


, count(distinct case when D1_fss > 0 and sip_type = 'TB SIP' then shop_id else null end) as D1_TBSIP_FSS_shops
, count(distinct case when D1_ccb > 0 and sip_type = 'TB SIP' then shop_id else null end) as D1_TBSIP_CCB_shops 
, count(distinct case when M1_fss > 0 and sip_type = 'TB SIP' then shop_id else null end) as M1_TBSIP_FSS_shops
, count(distinct case when M1_ccb > 0 and sip_type = 'TB SIP' then shop_id else null end) as M1_TBSIP_CCB_shops 


, count(distinct case when D1_fss > 0 and sip_type = 'Local SIP' then shop_id else null end) as D1_LocalSIP_FSS_shops
, count(distinct case when D1_ccb > 0 and sip_type = 'Local SIP' then shop_id else null end) as D1_LocalSIP_CCB_shops 
, count(distinct case when M1_fss > 0 and sip_type = 'Local SIP' then shop_id else null end) as M1_LocalSIP_FSS_shops
, count(distinct case when M1_ccb > 0 and sip_type = 'Local SIP' then shop_id else null end) as M1_LocalSIP_CCB_shops 
FROM st
WHERE is_cb_shop = 1
GROUP BY 1
)


, cb as 
(
SELECT
'CB' as seller_origin 
, a.grass_region
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_ADG 


, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_FSS_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_FSS_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_FSS_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_FSS_ADG 
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_CCB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_CCB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CB_CCB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CB_CCB_ADG 


, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then order_id else null end) as double)
/ 7 as W1_CB_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) then order_id else null end) as double)
/ 7 as W2_CB_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) then gmv_usd else null end)
/ 7 as W1_CB_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) then gmv_usd else null end)
/ 7 as W2_CB_ADG 
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 then order_id else null end) as double)
/ 7 as W1_CB_FSS_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 then order_id else null end) as double)
/ 7 as W2_CB_FSS_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 then gmv_usd else null end)
/ 7 as W1_CB_FSS_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 then gmv_usd else null end)
/ 7 as W2_CB_FSS_ADG 
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_CCB > 0 then order_id else null end) as double)
/ 7 as W1_CB_CCB_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_CCB > 0 then order_id else null end) as double)
/ 7 as W2_CB_CCB_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_CCB > 0 then gmv_usd else null end)
/ 7 as W1_CB_CCB_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_CCB > 0 then gmv_usd else null end)
/ 7 as W2_CB_CCB_ADG 


, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 and origin = 'China' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CNCB_FSS_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 and origin = 'China' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CNCB_FSS_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 and origin = 'China' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CNCB_FSS_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 and origin = 'China' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CNCB_FSS_ADG 
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 and origin = 'China' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CNCB_CCB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 and origin = 'China' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CNCB_CCB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 and origin = 'China' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_CNCB_CCB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 and origin = 'China' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_CNCB_CCB_ADG 




, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 and origin = 'China' then order_id else null end) as double)
/ 7 as W1_CNCB_FSS_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 and origin = 'China' then order_id else null end) as double)
/ 7 as W2_CNCB_FSS_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 and origin = 'China' then gmv_usd else null end)
/ 7 as W1_CNCB_FSS_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 and origin = 'China' then gmv_usd else null end)
/ 7 as W2_CNCB_FSS_ADG 
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_CCB > 0 and origin = 'China' then order_id else null end) as double)
/ 7 as W1_CNCB_CCB_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_CCB > 0 and origin = 'China' then order_id else null end) as double)
/ 7 as W2_CNCB_CCB_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_CCB > 0 and origin = 'China' then gmv_usd else null end)
/ 7 as W1_CNCB_CCB_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_CCB > 0 and origin = 'China' then gmv_usd else null end)
/ 7 as W2_CNCB_CCB_ADG 



, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 and origin = 'Korea' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_KRCB_FSS_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 and origin = 'Korea' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_KRCB_FSS_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 and origin = 'Korea' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_KRCB_FSS_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 and origin = 'Korea' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_KRCB_FSS_ADG 
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 and origin = 'Korea' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_KRCB_CCB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 and origin = 'Korea' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_KRCB_CCB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 and origin = 'Korea' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_KRCB_CCB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 and origin = 'Korea' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_KRCB_CCB_ADG 




, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 and origin = 'Korea' then order_id else null end) as double)
/ 7 as W1_KRCB_FSS_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 and origin = 'Korea' then order_id else null end) as double)
/ 7 as W2_KRCB_FSS_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 and origin = 'Korea' then gmv_usd else null end)
/ 7 as W1_KRCB_FSS_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 and origin = 'Korea' then gmv_usd else null end)
/ 7 as W2_KRCB_FSS_ADG 
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_CCB > 0 and origin = 'Korea' then order_id else null end) as double)
/ 7 as W1_KRCB_CCB_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_CCB > 0 and origin = 'Korea' then order_id else null end) as double)
/ 7 as W2_KRCB_CCB_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_CCB > 0 and origin = 'Korea' then gmv_usd else null end)
/ 7 as W1_KRCB_CCB_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_CCB > 0 and origin = 'Korea' then gmv_usd else null end)
/ 7 as W2_KRCB_CCB_ADG 


, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 and origin = 'Japan' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_JPCB_FSS_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 and origin = 'Japan' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_JPCB_FSS_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 and origin = 'Japan' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_JPCB_FSS_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 and origin = 'Japan' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_JPCB_FSS_ADG 
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 and origin = 'Japan' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_JPCB_CCB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 and origin = 'Japan' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_JPCB_CCB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 and origin = 'Japan' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_JPCB_CCB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 and origin = 'Japan' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_JPCB_CCB_ADG 




, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 and origin = 'Japan' then order_id else null end) as double)
/ 7 as W1_JPCB_FSS_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 and origin = 'Japan' then order_id else null end) as double)
/ 7 as W2_JPCB_FSS_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 and origin = 'Japan' then gmv_usd else null end)
/ 7 as W1_JPCB_FSS_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 and origin = 'Japan' then gmv_usd else null end)
/ 7 as W2_JPCB_FSS_ADG 
, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_CCB > 0 and origin = 'Japan' then order_id else null end) as double)
/ 7 as W1_JPCB_CCB_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_CCB > 0 and origin = 'Japan' then order_id else null end) as double)
/ 7 as W2_JPCB_CCB_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_CCB > 0 and origin = 'Japan' then gmv_usd else null end)
/ 7 as W1_JPCB_CCB_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_CCB > 0 and origin = 'Japan' then gmv_usd else null end)
/ 7 as W2_JPCB_CCB_ADG 


, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 and sip_type = 'TB SIP' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_TBSIP_FSS_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 and sip_type = 'TB SIP' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_TBSIP_FSS_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_fss > 0 and sip_type = 'TB SIP' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_TBSIP_FSS_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_fss > 0 and sip_type = 'TB SIP' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_TBSIP_FSS_ADG 
, cast(count(distinct case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 and sip_type = 'TB SIP' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, current_date)) as MTD_TBSIP_CCB_ADO
, cast(count(distinct case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 and sip_type = 'TB SIP' then order_id else null end) as double)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_TBSIP_CCB_ADO 
, sum(case when grass_date >= date_trunc('month', date_add('day', -1, current_date)) and grass_date <= date_add('day', -1, current_date) and D1_CCB > 0 and sip_type = 'TB SIP' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, current_date)) as MTD_TBSIP_CCB_ADG
, sum(case when grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date))) and grass_date < date_trunc('month', date_add('day', -1, current_date)) and M1_CCB > 0 and sip_type = 'TB SIP' then gmv_usd else null end)
/ day_of_month(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) as M1_TBSIP_CCB_ADG 




, cast(count(distinct case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 and sip_type = 'TB SIP' then order_id else null end) as double)
/ 7 as W1_TBSIP_FSS_ADO 
, cast(count(distinct case when grass_date >= date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and D1_fss > 0 and sip_type = 'TB SIP' then order_id else null end) as double)
/ 7 as W2_TBSIP_FSS_ADO 
, sum(case when grass_date >= date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) and grass_date < date_trunc('week', date_add('day', -1, current_date)) and D1_fss > 0 and sip_type = 'TB SIP' then gmv_usd else null end)
/ 7 as W1_TBSIP_FSS_ADG 
, sum(case when grass_date >= date_add('week', -2, date_trunc