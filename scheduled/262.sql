insert into nexus.test_fe22ea01655a2f04d0e8b3078d6f60f190969490b8542f547f7529df87408462_ddfc9c49d01c2143f0eabd014d8eb381 --fm2 adp% weekly
with sps_list as (
select *
from regcbbi_kr.shopee_kr_bi_team__sps_seller_list
where ingestion_timestamp = (select max(ingestion_timestamp) as max_ingest
from regcbbi_kr.shopee_kr_bi_team__sps_seller_list)
)


,fm_detail AS(
select s.fm_tn
,s.sls_tn
,fm_code_level_pickup_time --fm揽收时间
--IF(fmr.shipment_method = 1, 'pickup', 'dropoff') AS shipping_method
from sls_mart.shopee_ssc_lfs_cross_resync_db__fm_tn_sls_tn_map__reg_continuous_s0_live as s
left join (
select fm_tn
,min(update_time) AS fm_code_level_pickup_time --最早的update time即为fm揽收时间
from sls_mart.shopee_ssc_lfs_cross_resync_db__fm_tracking_tab__reg_continuous_s0_live 
where fm_status in (3, 4) --订单状态为PICKED_UP 或者 DELIVERED
and date(from_unixtime(ctime)) > current_date - INTERVAL '180' DAY
group by 1
) fmt
on s.fm_tn = fmt.fm_tn
left join (
select fm_tn
,max(shipment_method) shipment_method
from sls_mart.shopee_ssc_lfs_cross_resync_db__fm_request_tab__reg_continuous_s0_live 
group by 1
) fmr
on s.fm_tn = fmr.fm_tn
where date(from_unixtime(ctime)) > current_date - INTERVAL '180' DAY --预报fm时间
group by 1, 2, 3
)


, kr_seller as (
SELECT DISTINCT shop_id as shopid
, user_name as child_account_name
, gp_name as gp_account_name 
from dev_regcbbi_kr.krcb_shop_profile
where grass_date in (
select max(grass_date) as latest_ingest_date 
from dev_regcbbi_kr.krcb_shop_profile
where grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
)
and grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')


)


, cb_seller as (
SELECT gp_account_name,
a.shopid,
child_account_name,
CASE WHEN type_2 IS NOT NULL THEN type_2
ELSE 'Others'
END AS gp_smt, 
CASE WHEN lower(partner_name) in ('supermarket', 'shopee 2', 'playauto') then 'Integrated'
WHEN lower(partner_name) like '%ecremmoce%' then 'Integrated'
WHEN ps.shopid is null then 'No ERP'
ELSE 'Non-integrated'
END AS erp_type 
FROM kr_seller a
left join (select * from marketplace.shopee_partner_db__partner_shop_tab__reg_daily_s0_live where grass_region != 'XX') as ps on a.shopid = ps.shopid
left join marketplace.shopee_partner_db__partner_tab__reg_daily_s0_live as p on cast(ps.partner_id as varchar) = cast(p.partner_id as varchar)
LEFT JOIN
(SELECT *
FROM regcbbi_others.shopee_regional_cb_team__new_krcb_gp_allocation
WHERE ingestion_timestamp =
(SELECT max(ingestion_timestamp)as col1
FROM regcbbi_others.shopee_regional_cb_team__new_krcb_gp_allocation)
) AS b 
ON a.gp_account_name = b.gp_name
)




--得到所有month confirm的订单信息 
,whs_scan_order AS(
select o.shop_id shopid,
o.order_sn ordersn,
o.order_id orderid,
o.cancel_user_id cancel_userid,
o.grass_date,
gp_smt,
(case when date(from_unixtime(wh_inbound_date))>= date_trunc('week', CURRENT_DATE - interval '1' DAY) and date(from_unixtime(wh_inbound_date))< CURRENT_DATE
then 'W-0'
when date(from_unixtime(wh_inbound_date))>= date_trunc('week',date_add('week',-1,CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(wh_inbound_date))< date_trunc('week', CURRENT_DATE - interval '1' DAY) 
then 'W-1'
when date(from_unixtime(wh_inbound_date))>= date_trunc('week', date_add('week', -2, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(wh_inbound_date))< date_trunc('week',date_add('week',-1,CURRENT_DATE - interval '1' DAY)) 
then 'W-2'
when date(from_unixtime(wh_inbound_date))>= date_trunc('week', date_add('week', -3, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(wh_inbound_date))< date_trunc('week',date_add('week',-2,CURRENT_DATE - interval '1' DAY)) 
then 'W-3'
when date(from_unixtime(wh_inbound_date))>= date_trunc('week', date_add('week', -4, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(wh_inbound_date))< date_trunc('week',date_add('week',-3,CURRENT_DATE - interval '1' DAY)) 
then 'W-4'
else null
end)as week_period,
case when s.sps_users is not null then 'sps_seller' else 'none_sps_seller' end as seller_type,
cb.erp_type
FROM mp_order.dwd_order_all_ent_df__reg_s0_live o
JOIN cb_seller cb 
ON o.shop_id = cb.shopid
LEFT JOIN sps_list s
on s.sps_users = cb.gp_account_name
left join
(SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_sg_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL 
SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_my_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL
SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_tw_db__logistic_request_tab_lfs_union_tmp
where tz_type='local'
UNION ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_id_db__logistic_request_tab_lfs_union_tmp
where tz_type='local'
UNION ALL
SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_th_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_ph_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL
SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_vn_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_br_db__logistic_request_tab_lfs_union_tmp
where tz_type='local'
Union ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_mx_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
Union ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_cl_db__logistic_request_tab__reg_daily_s0_live
Union ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_co_db__logistic_request_tab__reg_daily_s0_live
) l1
on o.order_sn = l1.ordersn
where 
o.is_cb_shop = 1
and o.tz_type='local'
and o.shipping_method_id >0
and date(from_unixtime(wh_inbound_date)) >= date_trunc('week', date_add('week', -4, CURRENT_DATE - interval '1' DAY))
and date(from_unixtime(wh_inbound_date)) < CURRENT_DATE
and o.grass_date>=current_date - INTERVAL '180' DAY
and date(split(create_datetime, ' ')[1])>= current_date - INTERVAL '180' DAY
)


, raw_orders as (
select
*
from
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
where
cb_option = 1
and date(from_unixtime(delivery_time)) >= date_trunc('week', date_add('week', -4, CURRENT_DATE - interval '1' DAY))
and grass_region <> ''
)


, logi_status AS (
SELECT
orderid,
min(ctime) pickup_done_time
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_continuous_s0_live
WHERE
(new_status = 2 and grass_region <> '')
GROUP BY
1
having
from_unixtime(min(ctime)) >= date_add('day',-30,current_date) 
)


, delivery_done_orders as (
SELECT distinct a.ordersn, 
(case when date(from_unixtime(a.delivery_time))>= date_trunc('week', CURRENT_DATE - interval '1' DAY) and date(from_unixtime(a.delivery_time))< CURRENT_DATE
then 'W-0'
when date(from_unixtime(a.delivery_time))>= date_trunc('week',date_add('week',-1,CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(a.delivery_time))< date_trunc('week', CURRENT_DATE - interval '1' DAY) 
then 'W-1'
when date(from_unixtime(a.delivery_time))>= date_trunc('week', date_add('week', -2, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(a.delivery_time))< date_trunc('week',date_add('week',-1,CURRENT_DATE - interval '1' DAY)) 
then 'W-2'
when date(from_unixtime(a.delivery_time))>= date_trunc('week', date_add('week', -3, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(a.delivery_time))< date_trunc('week',date_add('week',-2,CURRENT_DATE - interval '1' DAY)) 
then 'W-3'
when date(from_unixtime(a.delivery_time))>= date_trunc('week', date_add('week', -4, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(a.delivery_time))< date_trunc('week',date_add('week',-3,CURRENT_DATE - interval '1' DAY)) 
then 'W-4'
else null
end)as week_period
,gp_smt
,case when s.sps_users is not null then 'sps_seller' else 'none_sps_seller' end as seller_type
,cb.erp_type


FROM
raw_orders as a
INNER JOIN
cb_seller cb 
ON
cb.shopid = a.shopid
LEFT JOIN
logi_status b ON a.orderid = b.orderid
left join sps_list s
on s.sps_users = cb.gp_account_name
)




--得到被fm pick up的订单
,w_fm_pickup_order AS(
SELECT distinct 
o.order_sn ordersn
,o.order_id orderid
,(case when date(from_unixtime(wh_inbound_date))>= date_trunc('week', CURRENT_DATE - interval '1' DAY) and date(from_unixtime(wh_inbound_date))< CURRENT_DATE
then 'W-0'
when date(from_unixtime(wh_inbound_date))>= date_trunc('week',date_add('week',-1,CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(wh_inbound_date))< date_trunc('week', CURRENT_DATE - interval '1' DAY) 
then 'W-1'
when date(from_unixtime(wh_inbound_date))>= date_trunc('week', date_add('week', -2, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(wh_inbound_date))< date_trunc('week',date_add('week',-1,CURRENT_DATE - interval '1' DAY)) 
then 'W-2'
when date(from_unixtime(wh_inbound_date))>= date_trunc('week', date_add('week', -3, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(wh_inbound_date))< date_trunc('week',date_add('week',-2,CURRENT_DATE - interval '1' DAY)) 
then 'W-3'
when date(from_unixtime(wh_inbound_date))>= date_trunc('week', date_add('week', -4, CURRENT_DATE - interval '1' DAY)) and date(from_unixtime(wh_inbound_date))< date_trunc('week',date_add('week',-3,CURRENT_DATE - interval '1' DAY)) 
then 'W-4'


-- case when week(date(from_unixtime(wh_inbound_date))) - week(CURRENT_DATE - interval '1' DAY) = 0 then 'W-0'
-- when week(date(from_unixtime(wh_inbound_date))) - week(CURRENT_DATE - interval '1' DAY) = -1 then 'W-1'
-- when week(date(from_unixtime(wh_inbound_date))) - week(CURRENT_DATE - interval '1' DAY) = -2 then 'W-2'
-- when week(date(from_unixtime(wh_inbound_date))) - week(CURRENT_DATE - interval '1' DAY) = -3 then 'W-3'
-- when week(date(from_unixtime(wh_inbound_date))) - week(CURRENT_DATE - interval '1' DAY) = -4 then 'W-4'
else null
end)as week_period
,gp_smt
,fm_code_level_pickup_time
,case when s.sps_users is not null then 'sps_seller' else 'none_sps_seller' end as seller_type
,cb.erp_type
from mp_order.dwd_order_all_ent_df__reg_s0_live o
left join
(SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_sg_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL 
SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_my_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL
SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_tw_db__logistic_request_tab_lfs_union_tmp
where tz_type='local'
UNION ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_id_db__logistic_request_tab_lfs_union_tmp
where tz_type='local'
UNION ALL
SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_th_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_ph_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL
SELECT distinct ordersn,consignment_no, wh_inbound_date
FROM sls_mart.shopee_sls_logistic_vn_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
UNION ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_br_db__logistic_request_tab_lfs_union_tmp
where tz_type='local'
Union ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_mx_db__logistic_request_tab_lfs_union_tmp
where tz_type='local' 
Union ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_cl_db__logistic_request_tab__reg_daily_s0_live
Union ALL
SELECT distinct ordersn,consignment_no,wh_inbound_date
FROM sls_mart.shopee_sls_logistic_co_db__logistic_request_tab__reg_daily_s0_live) l1
on o.order_sn = l1.ordersn
JOIN fm_detail f
ON l1.consignment_no = f.sls_tn
JOIN cb_seller cb 
ON o.shop_id = cb.shopid
left join sps_list s
on s.sps_users = cb.gp_account_name
where 
o.is_cb_shop = 1
and o.tz_type='local'
and o.shipping_method_id > 0
and date(from_unixtime(wh_inbound_date)) >= date_trunc('week', date_add('week', -4, CURRENT_DATE - interval '1' DAY))
and date(from_unixtime(wh_inbound_date)) < CURRENT_DATE
and o.grass_date>=current_date - INTERVAL '180' DAY
and date(split(create_datetime, ' ')[1])>= current_date - INTERVAL '180' DAY 
)




select week_period
,gp_smt
,seller_type
,erp_type
,sum(fm_predeclare_orders) as fm_predeclare_orders
,sum(fm_pickup_orders) as fm_pickup_orders_sum
,sum(whs_scan_orders) as whs_scan_orders_sum
,sum(delivery_done_orders) as delivery_done_orders_sum
,coalesce(cast(sum(fm_pickup_orders) as double),0)/sum(whs_scan_orders) as adp_rate
,coalesce(cast(sum(fm_pickup_orders) as double),0)/sum(delivery_done_orders) as adp_rate_delivery_done
from(
select 
whs.week_period ,
whs.gp_smt,
whs.seller_type,
whs.erp_type,
coalesce(fm_predeclare_orders,0) as fm_predeclare_orders,
coalesce(fm_pickup_orders,0) as fm_pickup_orders,
coalesce(whs_scan_orders,0)as whs_scan_orders,
coalesce(delivery_done_orders,0) as delivery_done_orders,
coalesce(cast(fm_pickup_orders as double),0)/whs_scan_orders as adp_rate_whs_scan,
coalesce(cast(fm_pickup_orders as double),0)/delivery_done_orders as adp_rate_delivery_done
from 
(select week_period,
gp_smt,
seller_type,
erp_type,
coalesce(count(distinct ordersn),0) as whs_scan_orders
from whs_scan_order
group by 1,2,3,4) whs
left join 
(select week_period,
gp_smt, 
seller_type,
erp_type,
coalesce(count(distinct ordersn),0) as delivery_done_orders
from delivery_done_orders
group by 1,2,3,4) do
on whs.week_period = do.week_period and whs.gp_smt = do.gp_smt and whs.seller_type = do.seller_type and whs.erp_type = do.erp_type 
left join 
(select week_period,
gp_smt,
seller_type,
erp_type,
coalesce(count(distinct ordersn),0)as fm_predeclare_orders,
coalesce(count(distinct case when fm_code_level_pickup_time is not null then ordersn else null end),0)as fm_pickup_orders
from w_fm_pickup_order
group by 1,2,3,4) p
on whs.week_period = p.week_period and whs.gp_smt = p.gp_smt and whs.seller_type = p.seller_type and whs.erp_type = p.erp_type)
group by 1,2,3,4
order by (case when week_period ='W-0' then 1
when week_period ='W-1' then 2
when week_period ='W-2' then 3
when week_period ='W-3' then 4
when week_period ='W-4' then 5
end)