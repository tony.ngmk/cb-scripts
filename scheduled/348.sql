insert into nexus.test_35e994d41cd945d3441b32a2ef00b153d8b6542b8cffcc7153e394dbc2c090e2_764142ceeac0e6c251c8c27e76ceb9c6 WITH order_info AS (
SELECT
date(from_unixtime(create_timestamp)) AS grass_date
,COUNT(distinct order_id) AS CB_ADO
,SUM(gmv_usd) AS CB_GMV_usd
,AVG(buyer_paid_shipping_fee_usd) AS CB_buyer_shipping_fee_usd
,COUNT(distinct IF(order_be_status_id in (7,8), order_id, null)) AS CB_cancelled_order
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE
tz_type = 'local'
AND grass_region = 'MY'
AND is_bi_excluded = 0
AND is_cb_shop = 1
AND grass_date >= current_date - interval '14' day
AND date(from_unixtime(create_timestamp)) between current_date - interval '14' day and current_date - interval '1' day
GROUP BY 1
)


,sku_info AS (
SELECT
a.ctime AS grass_date
,COUNT(distinct a.itemid) AS CB_new_live_sku
FROM 
(SELECT distinct
date(from_unixtime(create_timestamp)) AS ctime
,shop_id AS shopid
,item_id AS itemid
FROM mp_item.dim_item__reg_s0_live
WHERE
tz_type = 'local'
AND grass_date = current_date - interval '1' day
AND grass_region = 'MY'
AND is_cb_shop = 1
AND status = 1
AND stock > 0
AND date(from_unixtime(create_timestamp)) 
between current_date - interval '14' day and current_date - interval '1' day
AND shop_status = 1
AND seller_status = 1
AND is_holiday_mode = 0
) AS a
JOIN
(SELECT distinct
shop_id AS shopid, user_id 
FROM mp_user.dim_user__reg_s0_live
WHERE
tz_type = 'local'
AND grass_date = current_date - interval '1' day
AND grass_region = 'MY'
--AND date(from_unixtime(last_login_timestamp)) >= current_date - interval '8' day
) AS b
ON a.shopid = b.shopid
JOIN 
(SELECT user_id as userid
, DATE(from_unixtime(last_login_timestamp_td)) AS last_login
FROM mp_user.dws_user_login_td_account_info__reg_s0_live 
WHERE tz_type = 'local'
AND grass_region = 'MY'
AND grass_date = current_date - interval '1' day ) l 
on b.user_id = l.userid
WHERE l.last_login >= current_date - interval '8' day
GROUP BY 1
)


,cb_sku_pct AS (
SELECT
grass_date
,1.00*cb_sku/country_sku AS cb_sku_cnt_pct
FROM
(SELECT
grass_date
,COUNT(distinct IF(is_cb_shop=1, item_id, NULL)) AS cb_sku
,COUNT(distinct item_id) AS country_sku
FROM mp_item.dim_item__reg_s0_live
WHERE
tz_type = 'local'
AND grass_date between current_date - interval '14' day and current_date - interval '1' day
AND grass_region = 'MY'
AND status = 1
GROUP BY 1
)
)


,traffic_info AS (
SELECT 
a.grass_date
,a.country_ADO
,b.country_view
,b.CB_view
FROM 
(SELECT
date(from_unixtime(create_timestamp)) AS grass_date
,COUNT(distinct order_id) AS country_ADO
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE
tz_type = 'local'
AND grass_region = 'MY'
AND is_bi_excluded = 0
AND grass_date >= current_date - interval '14' day
AND date(from_unixtime(create_timestamp)) between current_date - interval '14' day and current_date - interval '1' day
GROUP BY 1
) AS a
LEFT JOIN
(SELECT
grass_date
,SUM(pv_cd) AS country_view
,SUM(IF(is_cross_border = 1, pv_cd, 0)) AS CB_view
FROM 
(SELECT
grass_date
,grass_region
,is_cross_border
,shopid
,itemid
,pv_cd
FROM 
(SELECT
grass_date
,grass_region
,is_cross_border
,shop_id AS shopid
,item_id AS itemid
,pv_cnt_1d AS pv_cd
FROM mp_shopflow.dws_item_civ_1d__reg_s0_live 
WHERE
tz_type = 'local'
AND grass_region = 'MY'
AND grass_date between current_date - interval '14' day and current_date - interval '1' day
) -- AS t1
--LEFT JOIN 
-- (SELECT distinct
-- CAST(shopid AS BIGINT) AS shopid
-- FROM shopee.shopee_regional_bi_team__excluded_shop_tab 
-- WHERE
-- ingestion_timestamp = (SELECT max(ingestion_timestamp) AS col1 FROM shopee.shopee_regional_bi_team__excluded_shop_tab)
-- ) AS t2
-- ON t1.shopid = t2.shopid
-- WHERE
-- t2.shopid is null
) 
GROUP BY 1
) AS b
ON a.grass_date = b.grass_date
)


SELECT 
a.grass_date
,round(a.CB_GMV_usd, 2) AS CB_GMV_usd
,a.CB_ADO
,round(1.0000*a.CB_ADO/c.country_ADO, 4) AS CB_ADO_penetration
,b.CB_new_live_sku
,e.cb_sku_cnt_pct
,round(a.CB_GMV_usd/a.CB_ADO, 2) AS CB_ABS
,round(1.0000*a.CB_cancelled_order/a.CB_ADO, 4) AS CB_cancel_rate 
,c.CB_view
,round(1.0000*a.CB_ADO/c.CB_view, 4) AS CB_CR
,c.country_view
,round(1.0000*c.country_ADO/c.country_view, 4) AS Country_CR
FROM order_info AS a


LEFT JOIN sku_info AS b
ON a.grass_date = b.grass_date


LEFT JOIN traffic_info AS c
ON a.grass_date = c.grass_date


LEFT JOIN cb_sku_pct AS e
ON a.grass_date = e.grass_date


ORDER BY 1 DESC