insert into nexus.test_ddba6640815b5a7668fc3879a7b385daa47268677f499e04b0e78cb3da4ea409_5d22424a1be30ca2077e2b4a2e6625ba with
livesku as 
(
select t1.grass_region
,t1.global_be_category_id
,t1.level1_global_be_category
,t1.level2_global_be_category
,t1.level3_global_be_category
,t1.level4_global_be_category
,t1.level5_global_be_category
,t1.item_id
,t1.create_timestamp
from mp_item.dim_item__reg_s0_live t1
join (
select distinct grass_date
,grass_region
,shop_id
,DATE(from_unixtime(last_login_timestamp_td)) AS last_login
from mp_user.dws_user_login_td_account_info__reg_s0_live
where grass_date >= current_date - interval '1' day
--in (DATE(date_parse(cast(${PRE_DAY} as varchar), '%Y%m%d')), date_add('day', -7, DATE(date_parse(cast(${PRE_DAY} as varchar), '%Y%m%d'))), date_add('day', -14, DATE(date_parse(cast(${PRE_DAY} as varchar), '%Y%m%d'))), date_add('day', -1, date_trunc('month', DATE(date_parse(cast(${PRE_DAY} as varchar), '%Y%m%d')))))
and DATE(from_unixtime(last_login_timestamp_td)) >= date_add('day', -7, grass_date )
and tz_type = 'local'
and grass_region in ('BR', 'PH', 'PL')
) t2
on t1.shop_id = t2.shop_id
and t1.grass_date = t2.grass_date
where t1.grass_date >= current_date - interval '1' day
and t1.is_cb_shop = 1
and t1.tz_type = 'local'
and t1.grass_region in ('BR', 'PH', 'PL')
and t1.status = 1
and t1.stock > 0
and seller_status = 1 --dim_user.status
and shop_status = 1 --dim_shop.status
and (
is_holiday_mode = 0
or is_holiday_mode is NULL
)
group by 1, 2, 3, 4, 5, 6, 7, 8 ,9
order by 8 desc 
)


select t1.grass_region
,t1.global_be_category_id
,t1.level1_global_be_category
,t1.level2_global_be_category
,t1.level3_global_be_category
,t1.level4_global_be_category
,t1.level5_global_be_category
,count(distinct t1.item_id) as live_sku_qty
,count(distinct t2.item_id) as live_sku_qty_with_ord
,count(distinct if(date(from_unixtime(t1.create_timestamp)) between date_add('month', -1, current_date) and current_date, t1.item_id, null)) as M_1_new_sku_qty
,count(distinct if(date(from_unixtime(t1.create_timestamp)) between date_add('month', -1, current_date) and current_date, t2.item_id, null)) as M_1_new_sku_qty_with_order
,count(distinct if(date(from_unixtime(t1.create_timestamp)) between date_add('month', -1, current_date) and current_date, t2.order_id, null)) as M_1_new_sku_order_qty
,count(distinct if(date(from_unixtime(t1.create_timestamp)) between date_add('week', -1, current_date) and current_date, t1.item_id, null)) as W_1_new_sku_qty
,count(distinct if(date(from_unixtime(t1.create_timestamp)) between date_add('week', -1, current_date) and current_date, t2.item_id, null)) as W_1_new_sku_qty_with_order
,count(distinct if(date(from_unixtime(t1.create_timestamp)) between date_add('week', -1, current_date) and current_date, t2.order_id, null)) as W_1_new_sku_order_qty
from livesku t1
left join (
select order_id
,item_id
from mp_order.dwd_order_item_all_ent_df__reg_s0_live t3
where t3.grass_date >= current_date - interval '180' day
and t3.is_cb_shop = 1
and t3.tz_type = 'local'
and t3.grass_region in ('BR', 'PH', 'PL')
) t2
on t2.item_id = t1.item_id
group by 1, 2, 3, 4, 5, 6, 7
order by 8 DESC