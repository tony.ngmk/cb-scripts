/*


(select count(distinct(a.shop_id)) as shopid_count
from
(select shop_id
from mkpldp_openplatform.shopee_seller_openplatform_openapi_caller
where date(dt) between date'2022-04-01' and date'2022-04-30') a
inner join
(
select shopid as shop_id
from marketplace.shopee_partner_db__partner_shop_tab__reg_daily_s0_live
where cb_option = 1
) b
on (a.shop_id)=(b.shop_id))




WITH cb_shop_detail AS
(SELECT distinct
s.grass_region
, s.grass_date AS data_date
, s.shop_id
, registration_date
, live_sku

FROM
(SELECT grass_date, grass_region, shop_id
FROM mp_user.dim_shop__reg_s0_live
WHERE grass_date = DATE'2022-05-01'
AND is_cb_shop = 1
AND status = 1
AND is_holiday_mode = 0
AND tz_type = 'local'
) s
JOIN
(SELECT grass_date, grass_region, shop_id, DATE(from_unixtime(registration_timestamp)) AS registration_date
FROM mp_user.dim_user__reg_s0_live
WHERE grass_date = DATE'2022-05-01'
AND status = 1
AND is_seller = 1
AND tz_type = 'local'
AND is_cb_shop = 1
) u
ON s.grass_date = u.grass_date
AND s.grass_region = u.grass_region
AND s.shop_id = u.shop_id
JOIN
(SELECT grass_date, grass_region, shop_id
FROM mp_item.dws_shop_listing_td__reg_s0_live
WHERE grass_date = DATE'2022-05-01'
AND active_item_with_stock_cnt > 0
) sl
ON s.grass_date = sl.grass_date
AND s.grass_region = sl.grass_region
AND s.shop_id = sl.shop_id
JOIN
(SELECT grass_region, grass_date, shop_id
FROM mp_user.dws_user_login_td_account_info__reg_s0_live 
WHERE tz_type = 'local'
AND grass_date= DATE'2022-05-01'
AND DATE(from_unixtime(last_login_timestamp_td)) >= date_add('day', -6, grass_date)
) l
ON s.grass_region = l.grass_region
AND s.shop_id = l.shop_id
AND s.grass_date = l.grass_date
LEFT JOIN
(
SELECT grass_region
, grass_date AS data_date
, shop_id shopid
, COUNT(distinct item_id) AS live_sku


FROM mp_item.dim_item__reg_s0_live
WHERE is_cb_shop = 1
AND stock > 0
AND status = 1
AND grass_region <> ''
AND grass_date = DATE'2022-05-01'
AND tz_type = 'local'
GROUP BY 1,2,3
) iv4
ON iv4.grass_region = s.grass_region
AND iv4.shopid = s.shop_id
AND iv4.data_date = s.grass_date
),


api_data as (select distinct a.shop_id -- past 30d shop list used at least once API
from
(select DISTINCT shop_id
from mkpldp_openplatform.shopee_seller_openplatform_openapi_caller
where date(dt) between DATE'2022-05-01' - interval '30' day and DATE'2022-05-01' - interval '1' day 
and api like '%returns%'
) a
inner join
(
select DISTINCT shopid as shop_id
from marketplace.shopee_partner_db__partner_shop_tab__reg_daily_s0_live
where cb_option = 1
) b
on (a.shop_id)=(b.shop_id)) 
, cb_sku AS
(SELECT grass_region
, data_date
, COUNT(DISTINCT a.shop_id) AS shops
, COUNT( DISTINCT CASE WHEN api_data.shop_id IS NOT NULL THEN a.shop_id ELSE NULL end) AS used_api_shops


FROM cb_shop_detail a
left join api_data on a.shop_id = api_data.shop_id 
WHERE data_date = DATE'2022-05-01'
GROUP BY 1, 2
)
,
cb_ord AS
(SELECT grass_region
, month(date(from_unixtime(create_timestamp))) as create_month
, COUNT(distinct order_id)/30.00 AS gross_order
, COUNT( DISTINCT CASE WHEN api_data.shop_id IS NOT NULL THEN a.order_id ELSE NULL end)/30.00 AS used_api_shop_ado


FROM mp_order.dwd_order_all_ent_df__reg_s0_live a 
left join api_data on a.shop_id = api_data.shop_id 


WHERE is_cb_shop = 1
AND grass_date >= DATE'2022-05-01' - interval '31' day
AND tz_type='local'
AND create_timestamp between to_unixtime(DATE'2022-05-01' - interval '30' day) and to_unixtime(DATE'2022-05-01' - interval '1' day)
AND grass_region!=''
GROUP BY 1,2
)


SELECT * FROM cb_sku;
-- select * from cb_ord








WITH 
api_data as (select distinct a.shop_id -- past 30d shop list used at least once API
from
(select DISTINCT shop_id
from mkpldp_openplatform.shopee_seller_openplatform_openapi_caller
where date(dt) between DATE'2022-05-01' - interval '30' day and DATE'2022-05-01' - interval '1' day 
and api like '%returns%' and (api like '%confirm%'or api like '%accept_offer%' or api like '%dispute%' or api like '%offer%')
) a
inner join
(
select DISTINCT shopid as shop_id
from marketplace.shopee_partner_db__partner_shop_tab__reg_daily_s0_live
where cb_option = 1
) b
on (a.shop_id)=(b.shop_id)) 
,
cb_ord AS
(SELECT grass_region
, month(date(from_unixtime(create_timestamp))) as create_month
, COUNT(distinct order_id)/30.00 AS gross_order
, COUNT( DISTINCT CASE WHEN api_data.shop_id IS NOT NULL THEN a.order_id ELSE NULL end)/30.00 AS used_api_shop_ado


FROM mp_order.dwd_order_all_ent_df__reg_s0_live a 
left join api_data on a.shop_id = api_data.shop_id 


WHERE is_cb_shop = 1
AND grass_date >= DATE'2022-05-01' - interval '31' day
AND tz_type='local'
AND create_timestamp between to_unixtime(DATE'2022-05-01' - interval '30' day) and to_unixtime(DATE'2022-05-01' - interval '1' day)
AND grass_region!=''
GROUP BY 1,2
)


select * from cb_ord




*/


with r AS (
SELECT
distinct
orderid
,return_sn
, (CASE WHEN (r.reason = 0) THEN 'None' 
WHEN (r.reason = 1) THEN 'Non Receipt' 
WHEN (r.reason = 2) THEN 'Wrong Item' 
WHEN (r.reason = 3) THEN 'Item Damaged' 
WHEN (r.reason = 4) THEN 'Different from Description' 
WHEN (r.reason = 5) THEN 'Mutual Agree' 
WHEN (r.reason = 6) THEN 'Others' 
WHEN (r.reason = 101) THEN 'Item Wrong Damaged' 
WHEN (r.reason = 102) THEN 'Change Mind' 
WHEN (r.reason = 103) THEN 'Item Missing' 
WHEN (r.reason = 104) THEN 'Expectation Failed' 
WHEN (r.reason = 105) THEN 'Item Fake' 
WHEN (r.reason = 106) THEN 'Physical Damage' 
WHEN (r.reason = 107) THEN 'Functional Damage' ELSE 'Others' END) reason
, r.grass_region
, r.shopid
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live r
WHERE cb_option = 1 and r.grass_region <>''
and date("from_unixtime"(r.ctime)) between DATE'2022-05-01' - interval '30' day and DATE'2022-05-01' - interval '1' day 
and status=1
) ,


api_data as (


select distinct return_sn,shop_id from
(
select dt,
cast(shop_id as bigint) as shop_id,
coalesce(REGEXP_EXTRACT(lower(regexp_replace(request, '\n',' ')),'"returnsn":"(.*?)(?=\")', 1) ,
REGEXP_EXTRACT(lower(regexp_replace(request, '\n',' ')),'"returnsn": "(.*?)(?=\")', 1) ,
REGEXP_EXTRACT(lower(regexp_replace(request, '\n',' ')),'"returnsn" : "(.*?)(?=\")', 1) ,
REGEXP_EXTRACT(lower(regexp_replace(request, '\n',' ')),'"return_sn": "(.*?)(?=\")', 1) ,
REGEXP_EXTRACT(lower(regexp_replace(request, '\n',' ')),'"return_sn":"(.*?)(?=\")', 1) ,
REGEXP_EXTRACT(lower(regexp_replace(request, '\n',' ')),'"return_sn" : "(.*?)(?=\")', 1) ) as return_sn,
request,
api
from mkpldp_openplatform.shopee_seller_openplatform_openapi_caller
where date(dt) between DATE'2022-05-01' - interval '30' day and DATE'2022-05-01' - interval '1' day 
and api like '%returns%' and (api like '%confirm%'or api like '%accept_offer%' or api like '%dispute%' or api like '%offer%')
and request is not null 
and request <>'')
where return_sn is not null

)




select 
grass_region,
count(distinct r.return_sn) as cb_rr_request_count,
COUNT( DISTINCT CASE WHEN api_data.return_sn IS NOT NULL THEN r.return_sn ELSE NULL end) AS used_api_cb_rr_request_count
from r 
left join api_data
on r.return_sn=api_data.return_sn 
and cast(r.shopid as bigint) = cast(api_data.shop_id as bigint)
group by 1