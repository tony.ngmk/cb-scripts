insert into nexus.test_d37f2233945fcb39fe23a7205b797d9f7504ee9756f3264c8e95ae796875562e_3f741686195e9e921ebf759ce77688ea WITH
wms AS (
SELECT shop_id
FROM sbs_mart.shopee_scbs_db__shop_tab__reg_daily_s0_live
WHERE country IN ('ID')
AND ((fbs_tag = 0 AND vacation_mode = 0 AND shop_status = 1)
OR (fbs_tag = 1 AND vacation_mode = 0 AND cb_option = 1)
OR (fbs_tag = 1 AND vacation_mode = 0 AND cb_option = 0))
) 
, TRACK AS (
SELECT DISTINCT CAST(mst_shopid AS bigint) mst_shopid
FROM
regcbbi_others.shopee_regional_cb_team__id_localsip_tos_shoplist
WHERE (ingestion_timestamp = (SELECT "max"(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__id_localsip_tos_shoplist
))
) 
, shop AS (
SELECT DISTINCT
mu.shopid
, mu.main_category
, mu.sku live_sku
, is_official_shop
, is_preferred_shop
, mu.USERNAME
, IF((wms.shop_id IS NULL), 'Y', 'N') NOT_WH_SHOP
, (CASE WHEN (SIP.SHOPID IS NULL) THEN 'Y' ELSE 'N' END) NOT_SIP_SHOP
, (CASE WHEN (is_official_shop = 1) THEN 'official shop' WHEN ((is_official_shop = 0) AND (is_managed_seller = 1)) THEN 'mall seller' ELSE 'other' END) seller_type
, shop_first_listing_timestamp
, user_status
, is_holiday_mode
FROM
((((
SELECT DISTINCT
shop_id shopid
, shop_level1_category main_category
, active_item_with_stock_cnt sku
, is_official_shop
, is_preferred_shop
, USER_NAME USERNAME
, shop_first_listing_timestamp
, user_status
, is_holiday_mode
FROM
regcbbi_others.shop_profile
WHERE (((((status = 1) AND (is_cb_shop = 0)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (grass_region = 'ID')) AND (tz_type = 'local'))
) mu
LEFT JOIN (
SELECT DISTINCT
shop_id
, is_managed_seller
FROM
mp_user.dim_seller_ext__reg_s0_live
WHERE ((grass_date = (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local'))
) u2 ON (u2.shop_id = mu.shopid))
LEFT JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live sip ON (sip.shopid = mu.shopid))
LEFT JOIN wms ON (wms.shop_id = mu.shopid))
) 
, o AS (
SELECT DISTINCT
o2.orderid
, o1.shop_id SHOPID
, o1.create_time
, o1.cancel_time
, o2.cancel_reason
, o1.status_ext
, o1.complete_time
, o1.grass_date
FROM
((
SELECT DISTINCT
order_id
, shop_id
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) create_time
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) cancel_time
, order_be_status_id status_ext
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR) WHEN (grass_region = 'BR') THEN ("from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR) END) complete_time
, (CASE WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp)) WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '1' HOUR)) WHEN (grass_region = 'BR') THEN "date"(("from_unixtime"(create_timestamp) - INTERVAL '11' HOUR)) END) grass_date
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE (((is_cb_shop = 0) AND (tz_type = 'local')) AND (grass_region = 'ID') and date(split(create_datetime,' ')[1]) >= current_date-interval'120'day
and grass_date >= current_date-interval'120'day)
) o1
INNER JOIN (
SELECT DISTINCT
orderid
, extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'ID') and date(from_unixtime(create_time)) >= current_date-interval'120'day )
) o2 ON (o1.order_id = o2.orderid))
) 
, r AS (
SELECT DISTINCT
orderid
, shopid
, reason
, status
, "from_unixtime"(ctime) ctime
, "from_unixtime"(mtime) mtime
FROM
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE ((cb_option = 0) AND (grass_region = 'ID') and date(from_unixtime(ctime)) >= current_date-interval'120'day)
) 
, sku AS (
SELECT DISTINCT
shopid
, "count"(DISTINCT (CASE WHEN ((JNE_8003_CHANNEL = 1)) THEN itemid ELSE null END)) skus
FROM
(
SELECT DISTINCT
b.item_id itemid
, b.shop_id shopid
, (CASE WHEN (CAST("json_extract"(shipping_info, '$.8003.enabled') AS bigint) = 1) THEN 1 ELSE 0 END) JNE_8003_CHANNEL
FROM
(mp_item.dim_item__reg_s0_live b
INNER JOIN shop u ON (u.shopid = b.shop_id))
WHERE ((((b.grass_region = 'ID') AND (status = 1)) AND (stock > 0)) AND (is_cb_shop = 0) AND grass_date = current_date - interval '1' day AND tz_type = 'local')
) 
GROUP BY 1
) 
, normal_sku_amount AS (
SELECT
"count"(DISTINCT itemid) normal_sku
, "count"(DISTINCT (CASE WHEN (stock > 0) THEN itemid ELSE null END)) live_item
, shopid
FROM
(
SELECT DISTINCT
item_id itemid
, shop_id shopid
, stock
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((((grass_region = 'ID') AND (status = 1)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (is_cb_shop = 0)) AND (seller_status = 1)) AND (shop_status = 1)) AND (is_holiday_mode = 0))
) 
GROUP BY 3
) 
, ingestion_cat AS (
SELECT DISTINCT CAST(leaf_node_category_id AS bigint) leaf_node_category_id
FROM
regcbbi_others.shopee_regional_cb_team__idsip_prohibited_global_cat
WHERE (ingestion_timestamp = (SELECT "max"(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__idsip_prohibited_global_cat
))
) 








, prohibited_item AS (
SELECT DISTINCT
shopid
, "count"(DISTINCT (CASE WHEN (global_be_category_id IN (SELECT DISTINCT leaf_node_category_id
FROM
ingestion_cat
)) THEN itemid ELSE null END)) prohibited_item_cat
, "count"(DISTINCT (CASE WHEN (k.keyword IS NOT NULL) THEN itemid ELSE null END)) prohibited_item_keyword
FROM
((
SELECT DISTINCT
item_id itemid
, shop_id shopid
, name
, global_be_category_id
FROM
mp_item.dim_item__reg_s0_live
WHERE ((((((((grass_region = 'ID') AND (is_cb_shop = 0)) AND (status = 1)) AND (stock > 0)) AND (grass_date = (current_date - INTERVAL '1' DAY))) AND (seller_status = 1)) AND (shop_status = 1)) AND (is_holiday_mode = 0))
) i
LEFT JOIN (
SELECT DISTINCT keyword
FROM
regcbbi_others.shopee_regional_cb_team__idsip_prohibited_keyword
WHERE (ingestion_timestamp = (SELECT "max"(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__idsip_prohibited_keyword
))
) k ON "regexp_like"(name, keyword))
GROUP BY 1
) 
, opt_check_for_prohibited AS (
SELECT DISTINCT
shopid
, status_prohibited_cat
, status_prohibited_keyword
FROM
(
SELECT
s.shopid
, live_item
, (CASE WHEN ((CAST(COALESCE(prohibited_item_cat, 0) AS double) / live_item) >= DECIMAL '0.7') THEN 'Fail' WHEN ((CAST(COALESCE(prohibited_item_cat, 0) AS double) / live_item) < DECIMAL '0.7') THEN 'Pass' END) status_prohibited_cat
, (CASE WHEN ((CAST(COALESCE(prohibited_item_keyword, 0) AS double) / live_item) >= DECIMAL '0.7') THEN 'Fail' WHEN ((CAST(COALESCE(prohibited_item_keyword, 0) AS double) / live_item) < DECIMAL '0.7') THEN 'Pass' END) status_prohibited_keyword
FROM
(normal_sku_amount s
LEFT JOIN prohibited_item l ON (s.shopid = l.shopid))
) 
) 




SELECT DISTINCT
grass_region
, shopid
, is_official_shop
, main_category
, live_sku
, userid
, USERNAME
, (CASE WHEN ((((((((((gross_orders_30d > DECIMAL '0.1') AND (cr_l90d <= DECIMAL '0.03') AND (rr_l30d <= DECIMAL '0.01')) AND (NOT_WH_SHOP = 'Y')) AND (prohibited_status = 'Pass')) AND (shop_channel_pass = 'Y')) AND (supportive_channel_sku = 'Y')) AND (not_fnb_shop = 'Y')) AND (not_ds_shop = 'Y')) AND (live_larger_3month = 'Y')) AND (not_bpom_shop = 'Y') AND (not_cig_shop = 'Y')) THEN 'Y' ELSE 'N' END) MET_ALL_CRITERIA
, (CASE WHEN (gross_orders_30d > DECIMAL '0.1') THEN 'Y' ELSE 'N' END) MET_ADO_CRITERIA
, (CASE WHEN (cr_l90d <= DECIMAL '0.03') THEN 'Y' ELSE 'N' END) met_CR_criteria
, (CASE WHEN (rr_l30d <= DECIMAL '0.01') THEN 'Y' ELSE 'N' END) met_RR_criteria
, NOT_WH_SHOP
, prohibited_status 
, shop_channel_pass
, supportive_channel_sku
, not_fnb_shop
, not_ds_shop
, live_larger_3month LIVE_LARGER_THAN_THREE_MONTH
, NOT_SIP_SHOP
, NOT_BPOM_SHOP
, NOT_cig_SHOP
, gross_orders_30d l30d_ado
FROM
(
SELECT DISTINCT
grass_region
, a.shopid
, a.userid
, main_category
, live_sku
, USERNAME
, is_official_shop
, NOT_WH_SHOP
, NOT_SIP_SHOP
, seller_type
, case when status_prohibited_keyword='Pass' and status_prohibited_cat='Pass' then 'Pass' 
else 'Fail' end as prohibited_status 
, user_status
, is_holiday_mode
, (CASE WHEN (((overall >= 1) AND (mask_channel = 'Y'))
-- AND (channelid = 'Y')
) THEN 'Y' ELSE 'N' END) shop_channel_pass
, (CASE WHEN (supportive_channel_sku > 0) THEN 'Y' ELSE 'N' END) supportive_channel_sku
, (CASE WHEN (fnb.shop_id IS NULL) THEN 'Y' ELSE 'N' END) not_fnb_shop
, (CASE WHEN (ds.shop_id IS NULL) THEN 'Y' ELSE 'N' END) not_ds_shop
, (CASE WHEN (bp.SHOP_ID IS NULL) THEN 'Y' ELSE 'N' END) NOT_BPOM_SHOP
, (CASE WHEN (cig.SHOP_ID IS NULL) THEN 'Y' ELSE 'N' END) NOT_cig_SHOP
, (CASE WHEN ("date"("from_unixtime"(shop_first_listing_timestamp)) <= (current_date - INTERVAL '90' DAY)) THEN 'Y' ELSE 'N' END) live_larger_3month
, COALESCE("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)), 0) NFR_included_cancellations_30d
, COALESCE("count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END)), 0) NFR_included_rr_30d
, COALESCE("count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid ELSE null END)), 0) net_orders_30d
, (COALESCE("count"(DISTINCT (CASE WHEN (o.create_time BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) THEN o.orderid ELSE null END)), 0) / DECIMAL '30.000') gross_orders_30d
, COALESCE(TRY((("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) * DECIMAL '1.000000') / (("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) + "count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END))) + "count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '90' DAY) AND (current_date - INTERVAL '1' DAY)) AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid ELSE null END))))), 0) cr_l90d
, COALESCE(TRY((("count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END)) * DECIMAL '1.000000') / (("count"(DISTINCT (CASE WHEN ((o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302)) AND ("date"(o.cancel_time) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN o.orderid ELSE null END)) + "count"(DISTINCT (CASE WHEN ((((r.reason IN (1, 2, 3, 103, 105, 107)) AND ((o.cancel_reason = 0) OR (o.cancel_reason IS NULL))) AND (r.status IN (2, 5))) AND ("date"(r.mtime) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY))) THEN r.orderid ELSE null END))) + "count"(DISTINCT (CASE WHEN ((o.create_time BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))) THEN o.orderid ELSE null END))))), 0) rr_l30d
FROM
(((((((
SELECT DISTINCT
a.shopid
, a.userid
, normal_sku
, live_sku
, is_official_shop
, is_preferred_shop
, NOT_WH_SHOP
, COALESCE(skus, 0) supportive_channel_sku
, shop.main_category
, NOT_SIP_SHOP
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80014.enabled') AS bigint), 0) J_T_Express
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80005.enabled') AS bigint), 0) JNE_Regular__Cashless
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80015.enabled') AS bigint), 0) SICEPAT
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80023.enabled') AS bigint), 0) IDE
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80020.enabled') AS bigint), 0) njv
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80025.enabled') AS bigint), 0) Anteraja
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80088.enabled') AS bigint), 0) SPX
, (COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80023.enabled') AS bigint), 0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80015.enabled') AS bigint), 0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80005.enabled') AS bigint), 0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80014.enabled') AS bigint),0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80020.enabled') AS bigint),0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80025.enabled') AS bigint),0) + COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.80088.enabled') AS bigint),0)) overall
, status_prohibited_cat
, status_prohibited_keyword
, a.grass_region
, USERNAME
, seller_type
, shop_first_listing_timestamp
, user_status
, is_holiday_mode
, mask_channel
-- , (CASE WHEN (C.SHOPID IS NOT NULL) THEN 'Y' ELSE 'N' END) channelid
FROM
(((((((
SELECT DISTINCT
SHOPID
, extinfo
, userid
, grass_region
, (CASE WHEN (CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.8003.enabled') AS bigint) = 1) THEN 'Y' ELSE 'N' END) mask_channel
FROM
marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE (grass_region = 'ID')
) a
INNER JOIN shop ON (shop.shopid = a.shopid))
LEFT JOIN (
SELECT DISTINCT shopid
FROM
sls_mart.shopee_logistic_id_db__logistic_shop_tab__reg_daily_s0_live
WHERE (channelid = 8003)
) c ON (c.shopid = shop.shopid))
INNER JOIN TRACK T ON (T.MST_SHOPID = a.SHOPID))
LEFT JOIN sku ON (a.shopid = sku.shopid))
LEFT JOIN normal_sku_amount ON (normal_sku_amount.shopid = a.shopid))
LEFT JOIN opt_check_for_prohibited oc ON (oc.shopid = a.shopid))
) a
LEFT JOIN o ON (o.shopid = a.shopid))
LEFT JOIN (
SELECT DISTINCT CAST(shop_id AS bigint) shop_id
FROM
regcbbi_others.idsip_book
) bk ON (bk.shop_id = a.shopid))
LEFT JOIN (
SELECT DISTINCT shop_id
FROM
regcbbi_others.sip__idsip_new_onboarding_criteria_simulation_1__df__id__s0
) bp ON (bp.shop_id = a.shopid))
LEFT JOIN (
SELECT DISTINCT CAST(shop_id AS bigint) shop_id
FROM
regcbbi_others.idsip_drop_shipper
) ds ON (ds.shop_id = a.shopid))
LEFT JOIN (
SELECT DISTINCT CAST(shop_id AS bigint) shop_id
FROM
regcbbi_others.idsip_fnb
) fnb ON (fnb.shop_id = a.shopid))
LEFT JOIN r ON (o.orderid = r.orderid)
LEFT JOIN (
SELECT DISTINCT shop_id
FROM
regcbbi_others.sip__idsip_cigarette_seller__df__reg__s0
) cig ON (CAST(cig.shop_id AS bigint) = a.shopid))
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
) B
LIMIT 1000