WITH sip AS (
SELECT
DISTINCT affi_shopid,
mst_shopid,
t1.country mst_country,
t2.country affi_country,
cb_option
FROM
(
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT
DISTINCT affi_shopid,
affi_username,
mst_shopid,
country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE
(
grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES',
'FR'
)
)
and sip_shop_status != 3
) t2 ON (t1.shopid = t2.mst_shopid)
)
),
t2 AS (
SELECT
DISTINCT t2.main_account_id,
shop_id,
status,
"from_unixtime"(ctime) ctime,
"from_unixtime"(mtime) mtime,
account_type,
agent_acc_username,
agent_acc_password
FROM
(
(
(
marketplace.shopee_subaccount_db__account_shop_tab__reg_daily_s0_live t2
INNER JOIN (
SELECT
DISTINCT main_account_id,
account_type
FROM
marketplace.shopee_subaccount_db__account_tab__reg_daily_s0_live
WHERE
(account_type = 1003)
) t3 ON (t2.main_account_id = t3.main_account_id)
)
INNER JOIN (
SELECT
DISTINCT main_account_id,
team_id
FROM
marketplace.shopee_subaccount_db__account_team_tab__reg_daily_s0_live
WHERE
(team_id = 1000)
) team ON (team.main_account_id = t2.main_account_id)
)
INNER JOIN (
SELECT
DISTINCT CAST(main_acc_id AS bigint) main_acc_id,
agent_acc_username,
agent_acc_password
FROM
regcbbi_others.shopee_regional_cb_team__cs_mapping
) ag ON (ag.main_acc_id = t2.main_account_id)
)
WHERE
(status = 1)
and t2.grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES',
'FR'
)
),
t1 AS (
SELECT
DISTINCT affi_shopid,
t4.username affi_username,
affi_country,
COALESCE(t2.main_account_id, 0) main_account_id,
account_type,
(
CASE
WHEN (CAST(t2.status AS varchar) = '1') THEN 'Binded'
WHEN (CAST(t2.status AS varchar) = '-1') THEN 'Not Binded'
END
) shop_bind_status,
t2.ctime,
t2.mtime,
t4.status shop_status,
t4.holiday_mode_on,
(
CASE
WHEN (t2.main_account_id IS NOT NULL) THEN agent_acc_username
ELSE t4.username
END
) agent_acc_username,
(
CASE
WHEN (t2.main_account_id IS NOT NULL) THEN agent_acc_password
ELSE 'Scb88@Sip99'
END
) agent_acc_password,
order_status,
enquiry_type,
enquiry_details,
reason
FROM
(
SELECT
DISTINCT shop_id shopid,
user_status status,
is_holiday_mode holiday_mode_on,
user_id userid,
shop_level1_category main_category,
grass_region,
user_name username
FROM
regcbbi_others.shop_profile
WHERE
(
(is_cb_shop = 1)
AND (grass_date = (current_date - INTERVAL '1' DAY))
)
and grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES',
'FR'
)
and source like '%SIP%'
and status = 1
and user_status = 1
and is_holiday_mode = 0
) t4
join sip on sip.affi_shopid = t4.shopid
LEFT JOIN t2 ON (t2.shop_id = sip.affi_shopid)
LEFT JOIN (
SELECT
*
FROM
regcbbi_others.shopee_regional_cb_team__cs_chat_mapping
WHERE
(
(
ingestion_timestamp = (
SELECT
"max"(ingestion_timestamp) col_1
FROM
regcbbi_others.shopee_regional_cb_team__cs_chat_mapping
)
)
AND (initiative = 'Review Rewards')
)
) cm ON (cm.grass_region = sip.affi_country)
),
o AS (
SELECT
DISTINCT o2.orderid,
o1.shop_id SHOPID,
o1.create_time,
o1.cancel_time,
o2.cancel_reason,
o1.status_ext,
o1.complete_time,
o1.grass_date,
o1.itemid,
buyer_id,
seller_id,
buyer_name,
seller_name,
order_sn
FROM
(
(
SELECT
DISTINCT order_id,
shop_id,
(
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp))
WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(
(
"from_unixtime"(create_timestamp) - INTERVAL '1' HOUR
)
)
WHEN (grass_region = 'BR') THEN "date"(
(
"from_unixtime"(create_timestamp) - INTERVAL '11' HOUR
)
)
END
) create_time,
(
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp)
WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN (
"from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR
)
WHEN (grass_region = 'BR') THEN (
"from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR
)
END
) cancel_time,
order_be_status_id status_ext,
(
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(cancel_timestamp)
WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN (
"from_unixtime"(cancel_timestamp) - INTERVAL '1' HOUR
)
WHEN (grass_region = 'BR') THEN (
"from_unixtime"(cancel_timestamp) - INTERVAL '11' HOUR
)
END
) complete_time,
(
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "date"("from_unixtime"(create_timestamp))
WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN "date"(
(
"from_unixtime"(create_timestamp) - INTERVAL '1' HOUR
)
)
WHEN (grass_region = 'BR') THEN "date"(
(
"from_unixtime"(create_timestamp) - INTERVAL '11' HOUR
)
)
END
) grass_date,
item_id itemid,
buyer_id,
seller_id,
buyer_name,
seller_name,
order_sn
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE
(
(
(is_cb_shop = 1)
AND (tz_type = 'local')
)
AND (
date(split(create_datetime, ' ') [ 1 ]) BETWEEN (current_date - INTERVAL '90' DAY)
AND (current_date - INTERVAL '1' DAY)
)
)
and grass_date >= (current_date - INTERVAL '90' DAY)
and grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES',
'FR'
)
) o1
INNER JOIN (
SELECT
DISTINCT orderid,
extinfo.cancel_reason
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE
(
(cb_option = 1)
AND "date"("from_unixtime"(create_time)) BETWEEN (current_date - INTERVAL '90' DAY)
AND (current_date - INTERVAL '1' DAY)
)
and grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES',
'FR'
)
) o2 ON (o1.order_id = o2.orderid)
)
),
r AS (
SELECT
DISTINCT orderid,
itemid,
status,
REASON,
amount requested_return_qty,
((DECIMAL '1.0000' * refund_amount) / 100000) refund_amount_local_currency,
"from_unixtime"(ctime) ctime,
"from_unixtime"(mtime) mtime,
shopid
FROM
(
marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live a
CROSS JOIN UNNEST(return_info.item) t (
col1,
col2,
col3,
itemid,
modelid,
amount,
col7,
col8,
col9,
col10,
col11,
col12,
col13,
col14,
col15
)
)
WHERE
(
(cb_option = 1)
and grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES',
'FR'
)
)
and date(from_unixtime(ctime)) >= current_date - interval '90' day
)
SELECT
DISTINCT tt.affi_country region,
seller_name shop_name,
agent_acc_username agent_account_name,
agent_acc_password password,
shopid from_shop_id,
buyer_id to_user_id,
orderid,
current_date update_time,
order_sn,
tt.mst_country,
cb_option
FROM
(
(
SELECT
DISTINCT o1.orderid,
o1.ctime,
o.order_sn,
grass_region,
o1.shopid,
buyer_id,
buyer_name,
seller_name,
o.itemid,
affi_country,
mst_country,
cb_option,
(
CASE
WHEN (opuserid = -1) THEN 'SYSTEM'
ELSE 'BUYER CLICK'
END
) CLICK_TYPE,
"count"(
DISTINCT (
CASE
WHEN (
(
(affi_country = 'TW')
AND (o.cancel_reason IN (1, 2, 3, 200, 201, 204, 302))
)
AND (
"date"(o.cancel_time) BETWEEN (current_date - INTERVAL '30' DAY)
AND (current_date - INTERVAL '1' DAY)
)
) THEN o.orderid
WHEN (
(
(affi_country <> 'TW')
AND (o.cancel_reason IN (1, 2, 3, 4, 204, 301, 302))
)
AND (
"date"(o.cancel_time) BETWEEN (current_date - INTERVAL '30' DAY)
AND (current_date - INTERVAL '1' DAY)
)
) THEN o.orderid
ELSE null
END
)
) NFR_included_cancellations_30d,
"count"(
DISTINCT (
CASE
WHEN (
(
(
(
(affi_country = 'TW')
AND (r.reason IN (1, 2, 3, 103, 105))
)
AND (
(o.cancel_reason = 0)
OR (o.cancel_reason IS NULL)
)
)
AND (r.status IN (2, 5))
)
AND (
"date"(r.mtime) BETWEEN (current_date - INTERVAL '30' DAY)
AND (current_date - INTERVAL '1' DAY)
)
) THEN r.orderid
WHEN (
(
(
(
(affi_country <> 'TW')
AND (r.reason IN (1, 2, 3, 103, 105, 107))
)
AND (
(o.cancel_reason = 0)
OR (o.cancel_reason IS NULL)
)
)
AND (r.status IN (2, 5))
)
AND (
"date"(r.mtime) BETWEEN (current_date - INTERVAL '30' DAY)
AND (current_date - INTERVAL '1' DAY)
)
) THEN r.orderid
ELSE null
END
)
) NFR_included_rr_30d,
"count"(
DISTINCT (
CASE
WHEN (
(
o.create_time BETWEEN (current_date - INTERVAL '30' DAY)
AND (current_date - INTERVAL '1' DAY)
)
AND (o.status_ext IN (1, 2, 3, 4, 11, 12, 13, 14, 15))
) THEN o.orderid
ELSE null
END
)
) net_orders_30d
FROM
(
(
(
(
(
SELECT
DISTINCT orderid,
shopid,
old_status,
new_status,
opuserid,
grass_region,
(
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(ctime)
WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(ctime) - INTERVAL '1' HOUR)
WHEN (grass_region = 'BR') THEN ("from_unixtime"(ctime) - INTERVAL '11' HOUR)
WHEN (grass_region = 'CL') THEN CAST(
"format_datetime"(
"from_unixtime"(ctime) AT TIME ZONE 'America/Santiago',
'yyyy-MM-dd HH:mm:ss'
) AS timestamp
)
WHEN (grass_region = 'CO') THEN CAST(
"format_datetime"(
"from_unixtime"(ctime) AT TIME ZONE 'America/Bogota',
'yyyy-MM-dd HH:mm:ss'
) AS timestamp
)
WHEN (grass_region = 'MX') THEN CAST(
"format_datetime"(
"from_unixtime"(ctime) AT TIME ZONE 'America/Mexico_City',
'yyyy-MM-dd HH:mm:ss'
) AS timestamp
)
ELSE null
END
) ctime
FROM
marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_daily_s0_live
WHERE
(
(new_status = 4)
AND (
"date"(
(
CASE
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW')) THEN "from_unixtime"(ctime)
WHEN (grass_region IN ('TH', 'ID', 'VN')) THEN ("from_unixtime"(ctime) - INTERVAL '1' HOUR)
WHEN (grass_region = 'BR') THEN ("from_unixtime"(ctime) - INTERVAL '11' HOUR)
WHEN (grass_region = 'CL') THEN CAST(
"format_datetime"(
"from_unixtime"(ctime) AT TIME ZONE 'America/Santiago',
'yyyy-MM-dd HH:mm:ss'
) AS timestamp
)
WHEN (grass_region = 'CO') THEN CAST(
"format_datetime"(
"from_unixtime"(ctime) AT TIME ZONE 'America/Bogota',
'yyyy-MM-dd HH:mm:ss'
) AS timestamp
)
WHEN (grass_region = 'MX') THEN CAST(
"format_datetime"(
"from_unixtime"(ctime) AT TIME ZONE 'America/Mexico_City',
'yyyy-MM-dd HH:mm:ss'
) AS timestamp
)
ELSE null
END
)
) = (current_date - INTERVAL '1' DAY)
)
)
and grass_region in (
'SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES',
'FR'
)
) o1
INNER JOIN sip ON (sip.affi_shopid = o1.shopid)
)
LEFT JOIN o ON (o.orderid = o1.orderid)
)
LEFT JOIN r ON (r.orderid = o.orderid)
)
LEFT JOIN (
SELECT
DISTINCT orderid,
extinfo,
rating_star,
comment,
"from_unixtime"(ctime) r_time
FROM
marketplace.shopee_item_comment_db__item_cmt_v2_tab__reg_daily_s0_live
WHERE
(
"date"("from_unixtime"(ctime)) BETWEEN (current_date - INTERVAL '30' DAY)
AND (current_date - INTERVAL '1' DAY)
)
) rr ON (rr.orderid = o1.orderid)
)
WHERE
(rr.orderid IS NULL)
GROUP BY
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13
ORDER BY
15 DESC
) tt
LEFT JOIN t1 ON (t1.affi_shopid = tt.shopid)
)
WHERE
(
TRY(
(
(NFR_included_cancellations_30d * DECIMAL '1.000000') / (
(
NFR_included_cancellations_30d + NFR_included_rr_30d
) + net_orders_30d
)
)
) < DECIMAL '0.1'
)