insert into nexus.test_51d14cfaaca516fff99f747103e0f994e4a60337128e3d6984664659f44f805c_de594c90a0258642c914f6a2f8b4af97 -- major changes Feb 22, 2022
-- 1. added ID - CO, ID - CL
-- 2. Update join and filter logic
-- 3. optimised format


WITH sip_shop AS (
SELECT
DISTINCT grass_region
, b.affi_shopid
, b.mst_shopid
, b.affi_userid
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b 
ON b.mst_shopid = a.shopid
WHERE
(a.cb_option = 0)
AND (a.country = 'ID')
AND 
b.grass_region IN ('MY', 'SG', 'PH', 'TH', 'VN', 'BR', 'MX', 'CO', 'CL')
)


, sku AS (
SELECT
COUNT(DISTINCT item_id) SKU_AMOUNT
, a.shop_id
FROM
mp_item.dim_item__reg_s0_live a
INNER JOIN sip_shop 
ON (sip_shop.affi_shopid = a.shop_id)
WHERE
a.grass_region IN ('SG', 'MY', 'PH', 'TH', 'VN', 'BR', 'MX', 'CO', 'CL')
AND (stock > 0)
AND (tz_type = 'local')
AND (grass_date = current_date - interval '1' day)
GROUP BY 2
)


-- , psku AS (
-- SELECT
-- "count"(DISTINCT item_id) P_SKU_AMOUNT
-- , a.shop_id
-- , COUNT(DISTINCT CASE WHEN sold >= 1 THEN itemid end) AS sku_sales
-- FROM
-- (shopee.item_mart_dim_item a
-- INNER JOIN sip_shop ON (sip_shop.mst_shopid = a.shop_id)
-- LEFT JOIN shopee.shopee_item_v4_db__item_v4_tab b on a.item_id = b.itemid)
-- WHERE ((A.grass_region IN ('ID')) AND (tz_type = 'local') AND (grass_date = current_date - interval '1' day))
-- GROUP BY 2
-- ) 


, l30_order AS (
SELECT
DISTINCT shop_id
, (COUNT(DISTINCT order_id) / DECIMAL '30.00') AS L30_ADO
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live o
WHERE
(o.grass_region = 'ID')
AND (o.is_cb_shop = 0)
AND date(split(create_datetime, ' ') [1]) BETWEEN date_add('day', -30, current_date) AND date_add('day', -1, current_date)
AND (tz_type = 'local')
GROUP BY 1
)


, my AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid my_shopid
, ub.user_name my_username
, ub.user_id my_userid
, date(from_unixtime(registration_timestamp)) MY_REG
, is_holiday_mode MY_HM
, ua.status my_shop_status
, SKU_AMOUNT my_sku
FROM sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub 
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua 
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku 
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'MY')
AND (ub.grass_region = 'MY')
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, sg AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid sg_shopid
, ub.user_id sg_userid
, ub.user_name sg_username
, is_holiday_mode SG_HM
, ua.status sg_shop_status
, SKU_AMOUNT sg_sku
FROM
sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua 
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku 
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'SG') 
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, ph AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid ph_shopid
, ub.user_name ph_username
, ub.user_id ph_userid
, is_holiday_mode PH_HM
, ua.status ph_shop_status
, SKU_AMOUNT ph_sku
FROM
sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua 
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'PH')
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, th AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid th_shopid
, ub.user_name th_username
, ub.user_id th_userid
, is_holiday_mode TH_HM
, ua.status th_shop_status
, SKU_AMOUNT th_sku
FROM
sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub 
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'TH')
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, vn AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid vn_shopid
, ub.user_name vn_username
, ub.user_id vn_userid
, is_holiday_mode VN_HM
, ua.status vn_shop_status
, SKU_AMOUNT vn_sku
FROM
sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub 
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua 
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'VN')
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, BR AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid BR_shopid
, ub.user_name BR_username
, ub.user_id BR_userid
, is_holiday_mode BR_HM
, ua.status BR_shop_status
, SKU_AMOUNT BR_sku
FROM
sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub 
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua 
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'BR')
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, mx AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid mx_shopid
, ub.user_name mx_username
, ub.user_id mx_userid
, is_holiday_mode MX_HM
, ua.status mx_shop_status
, SKU_AMOUNT mx_sku
FROM
sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub 
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua 
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'MX')
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, co AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid co_shopid
, ub.user_name co_username
, ub.user_id co_userid
, is_holiday_mode CO_HM
, ua.status co_shop_status
, SKU_AMOUNT co_sku
FROM
sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub 
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua 
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'CO')
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, cl AS (
SELECT
DISTINCT sip.mst_shopid
, sip.affi_shopid cl_shopid
, ub.user_name cl_username
, ub.user_id cl_userid
, is_holiday_mode CL_HM
, ua.status cl_shop_status
, SKU_AMOUNT cl_sku
FROM
sip_shop sip
LEFT JOIN mp_user.dim_user__reg_s0_live ub 
ON (sip.affi_userid = ub.user_id)
LEFT JOIN (
SELECT
shop_id
, status
, is_holiday_mode
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_Date = current_date - interval '1' day
AND tz_type = 'local'
) ua 
ON (ua.shop_id = sip.affi_shopid)
LEFT JOIN sku
ON (sku.shop_id = sip.affi_shopid)
WHERE
(sip.grass_region = 'CL')
AND (ub.tz_type = 'local')
AND (ub.grass_date = current_date - interval '1' day)
)


, managed as (
SELECT *
FROM (
SELECT
*
, RANK() OVER (PARTITION BY shop_id ORDER BY grass_date DESC) AS ranking
FROM (
SELECT
DISTINCT grass_date
, is_managed_seller
, shop_id
FROM
mp_user.dim_seller_ext__reg_s0_live UMA
WHERE
uma.grass_region IN ('ID')
AND uma.tz_type = 'local'
)
)
WHERE
ranking = 1
)


SELECT
id_shopid
, id_userid
, id_username
, SELLER_TYPE
, main_category
, Batch
, my_shopid
-- , my_userid
, my_username
, sg_shopid
-- , sg_userid
, sg_username
, ph_shopid
-- , ph_userid
, ph_username
, th_shopid
-- , th_userid
, th_username
, vn_shopid
-- , th_userid
, vn_username
, br_shopid
, br_username
, mx_shopid
, mx_username
, co_shopid
, co_username
, cl_shopid
, cl_username
-- ,P_SKUs
-- ,sku_sales
, IF(
(
(MY_Shop_Status = 'Normal')
AND (MY_Stock_Status = 'Got Stock')
)
OR (
(SG_Shop_Status = 'Normal')
AND (SG_Stock_Status = 'Got Stock')
)
OR (
(PH_Shop_Status = 'Normal')
AND (PH_Stock_Status = 'Got Stock')
)
OR (
(TH_Shop_Status = 'Normal')
AND (TH_Stock_Status = 'Got Stock')
)
OR (
(VN_Shop_Status = 'Normal')
AND (VN_Stock_Status = 'Got Stock')
)
OR (
(BR_Shop_Status = 'Normal')
AND (BR_Stock_Status = 'Got Stock')
)
OR (
(MX_Shop_Status = 'Normal')
AND (MX_Stock_Status = 'Got Stock')
)
OR (
(CO_Shop_Status = 'Normal')
AND (CO_Stock_Status = 'Got Stock')
)
OR (
(CL_Shop_Status = 'Normal')
AND (CL_Stock_Status = 'Got Stock')
), 'Live', 'Not Live') AS Finished_Stock_Allocation
, IF(
(
(MY_Shop_Status = 'Normal')
AND (MY_Stock_Status = 'Got Stock')
AND (MY_Vacation_Status = 'Off')
)
OR (
(SG_Shop_Status = 'Normal')
AND (SG_Stock_Status = 'Got Stock')
AND (SG_Vacation_Status = 'Off')
)
OR (
(TH_Shop_Status = 'Normal')
AND (TH_Stock_Status = 'Got Stock')
AND (TH_Vacation_Status = 'Off')
)
OR (
(PH_Shop_Status = 'Normal')
AND (PH_Stock_Status = 'Got Stock')
AND (PH_Vacation_Status = 'Off')
)
OR (
(VN_Shop_Status = 'Normal')
AND (VN_Stock_Status = 'Got Stock')
AND (VN_Vacation_Status = 'Off')
)
OR(
(BR_Shop_Status = 'Normal')
AND (BR_Stock_Status = 'Got Stock')
AND (BR_Vacation_Status = 'Off')
)
OR (
(MX_Shop_Status = 'Normal')
AND (MX_Stock_Status = 'Got Stock')
AND (MX_Vacation_Status = 'Off')
)
OR (
(CO_Shop_Status = 'Normal')
AND (CO_Stock_Status = 'Got Stock')
AND (CO_Vacation_Status = 'Off')
)
OR (
(CL_Shop_Status = 'Normal')
AND (CL_Stock_Status = 'Got Stock')
AND (CL_Vacation_Status = 'Off')
), 'Live', 'Not Live') AS Live_Status
FROM (
SELECT
DISTINCT sip.mst_shopid AS id_shopid
, ub.user_id AS id_userid
-- , u.userid id_userid
, ub.user_name AS id_username
, CASE
WHEN (is_official_shop = 1) THEN 'OS'
WHEN (is_official_shop = 0)
AND (is_managed_seller = 1) THEN 'MS'
WHEN (is_official_shop = 0)
AND (is_managed_seller = 0)
AND (is_preferred_shop = 1) THEN 'PS'
WHEN (is_official_shop = 0)
AND (is_managed_seller = 0)
AND (is_preferred_shop = 0)
AND (L30_ADO >= 10) THEN 'Long Tail_1(L30_ADO >= 10)'
WHEN (is_official_shop = 0)
AND (is_managed_seller = 0)
AND (is_preferred_shop = 0)
AND (1 <= L30_ADO)
AND (L30_ADO < 10) THEN 'Long Tail_2(ADO between 1 and 10)'
WHEN (is_official_shop = 0)
AND (is_managed_seller = 0)
AND (is_preferred_shop = 0)
AND (0.1 <= L30_ADO)
AND (L30_ADO <= 1) THEN 'Long Tail_3(ADO between 0.1 and 1)'
WHEN (is_official_shop = 0)
AND (is_managed_seller = 0)
AND (is_preferred_shop = 0)
AND (0.1 > L30_ADO) THEN 'Long Tail_4(ADO < 0.1)'
ELSE 'OTHERS'
END AS SELLER_TYPE
, UCAT.shop_level1_category main_category
--, ' ' RM
, MY_REG Batch
, my_shopid 
-- , my_userid
, my_username
, sg_shopid
-- , sg_userid
, sg_username
, ph_shopid
-- , ph_userid
, ph_username
, th_shopid
-- , th_userid
, th_username
, vn_shopid
-- , vn_userid
, vn_username
, br_shopid
-- , vn_userid
, br_username
, mx_shopid
, mx_username
, co_shopid
, co_username
, cl_shopid
, cl_username
, IF((MY_HM != 1), 'Off', 'On') AS MY_Vacation_Status
, IF((my_shop_status != 1), 'Adnormal', 'Normal') AS MY_Shop_Status
, IF((my_sku > 0), 'Got Stock', 'No_stock') AS MY_Stock_Status
, IF((SG_HM != 1), 'Off', 'On') AS SG_Vacation_Status
, IF((sg_shop_status != 1), 'Adnormal', 'Normal') AS SG_Shop_Status
, IF((sg_sku > 0), 'Got Stock', 'No_stock') AS SG_Stock_Status
, IF((PH_HM != 1), 'Off', 'On') AS PH_Vacation_Status
, IF((ph_shop_status != 1), 'Adnormal', 'Normal') AS PH_Shop_Status
, IF((ph_sku > 0), 'Got Stock', 'No_stock') AS PH_Stock_Status
, IF((TH_HM != 1), 'Off', 'On') AS TH_Vacation_Status
, IF((th_shop_status != 1), 'Adnormal', 'Normal') AS TH_Shop_Status
, IF((th_sku > 0), 'Got Stock', 'No_stock') AS TH_Stock_Status
, IF((VN_HM != 1), 'Off', 'On') AS VN_Vacation_Status
, IF((vn_shop_status != 1), 'Adnormal', 'Normal') AS VN_Shop_Status
, IF((vn_sku > 0), 'Got Stock', 'No_stock') AS VN_Stock_Status
, IF((BR_HM != 1), 'Off', 'On') AS BR_Vacation_Status
, IF((BR_shop_status != 1), 'Adnormal', 'Normal') AS BR_Shop_Status
, IF((BR_sku > 0), 'Got Stock', 'No_stock') AS BR_Stock_Status
, IF((MX_HM != 1), 'Off', 'On') AS MX_Vacation_Status
, IF((mx_shop_status != 1), 'Adnormal', 'Normal') AS MX_Shop_Status
, IF((mx_sku > 0), 'Got Stock', 'No_stock') AS MX_Stock_Status
, IF((CO_HM != 1), 'Off', 'On') AS CO_Vacation_Status
, IF((co_shop_status != 1), 'Adnormal', 'Normal') AS CO_Shop_Status
, IF((co_sku > 0), 'Got Stock', 'No_stock') AS CO_Stock_Status
, IF((CL_HM != 1), 'Off', 'On') AS CL_Vacation_Status
, IF((cl_shop_status != 1), 'Adnormal', 'Normal') AS CL_Shop_Status
, IF((cl_sku > 0), 'Got Stock', 'No_stock') AS CL_Stock_Status
-- , sg_sku
-- , my_sku
-- , ph_sku
-- , th_sku
-- , vn_sku
-- , P_SKU_AMOUNT as P_SKUs
-- , sku_sales
FROM (
SELECT
DISTINCT mst_shopid
FROM
sip_shop) sip
LEFT JOIN (
SELECT
DISTINCT user_id
, user_name
, shop_id
FROM mp_user.dim_user__reg_s0_live
WHERE grass_region = 'ID'
AND tz_type = 'local'
AND grass_date = current_date - interval '1' day) ub
ON (sip.mst_shopid = ub.shop_id)
LEFT JOIN (
SELECT
DISTINCT shop_id
, user_id
, user_name
, is_official_shop
, is_preferred_shop
FROM
mp_user.dim_shop__reg_s0_live
WHERE grass_region = 'ID'
AND tz_type = 'local'
AND grass_date = current_date - interval '1' day
AND status = 1) u
ON (sip.mst_shopid = u.shop_id)
LEFT JOIN my 
ON (my.mst_shopid = sip.mst_shopid)
LEFT JOIN sg 
ON (sg.mst_shopid = sip.mst_shopid)
LEFT JOIN ph 
ON (ph.mst_shopid = sip.mst_shopid)
LEFT JOIN th 
ON (th.mst_shopid = sip.mst_shopid)
LEFT JOIN vn 
ON (vn.mst_shopid = sip.mst_shopid)
LEFT JOIN BR 
ON (br.mst_shopid = sip.mst_shopid)
LEFT JOIN mx 
ON (mx.mst_shopid = sip.mst_shopid)
LEFT JOIN co 
ON (co.mst_shopid = sip.mst_shopid)
LEFT JOIN cl 
ON (cl.mst_shopid = sip.mst_shopid)
LEFT JOIN l30_order 
ON (l30_order.shop_id = sip.mst_shopid)
-- LEFT JOIN sku ON sku.shop_id = sip.MST_shopid
-- LEFT JOIN psku ON psku.shop_id = sip.mst_shopid
LEFT JOIN managed uM 
ON (uM.shop_id = sip.mst_shopid)
LEFT JOIN (
SELECT
DISTINCT shop_id
, shop_level1_category
FROM mp_item.dws_shop_listing_td__reg_s0_live
WHERE grass_region = 'ID'
AND tz_type = 'local'
AND grass_date = current_date - interval '1' day) uCAT
ON (uCAT.shop_id = sip.mst_shopid)
-- WHERE
-- (u.grass_region = 'ID')
-- AND (u.tz_type = 'local')
-- AND (u.grass_date = current_date - interval '1' day)
-- AND (uCAT.grass_region = 'ID')
-- AND (uCAT.tz_type = 'local')
-- AND (uCAT.grass_date = current_date - interval '1' day)
-- AND (u.status = 1)
) 
-- GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28