insert into nexus.test_bb4c851086222e784f6a9f598f50177db3d390b1c0cd96d901f823aa8526529f_c9000edbed963a32b7a8b77d60704316 WITH
sip AS (
SELECT
affi_shopid
, mst_shopid
, t2.country affi_country
, t1.country mst_country
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (((country IN ('PH')) AND (sip_shop_status <> 3)) AND (grass_region <> '0'))
) t2 ON (t1.shopid = t2.mst_shopid))
WHERE ((cb_option = 0) AND (t1.country IN ('MY')))
) 
, arrange_ship AS (
SELECT DISTINCT
a.orderid
, a.shopid
, "from_unixtime"("min"(ctime)) arrange_shipment_time
FROM
(((marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shop_id shopid
, order_id orderid
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (("date"("split"(create_datetime, ' ')[1]) >= date '2022-03-28') AND (tz_type = 'local') AND grass_region IN ('PH') AND is_cb_shop = 1)
) o ON (o.orderid = a.orderid))
INNER JOIN sip ON (sip.affi_shopid = a.shopid))
LEFT JOIN (
SELECT DISTINCT
shop_id shopid
, order_id orderid
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE (((cancel_user_id = buyer_id) AND (("date"("split"(create_datetime, ' ')[1]) >= date '2022-03-28') AND (tz_type = 'local'))) AND (is_cb_shop = 1) AND grass_region IN ('PH'))
) o1 ON (o1.orderid = o.orderid))
WHERE (((new_status = 1) AND (o1.orderid IS NULL)) AND (grass_region <> '0'))
GROUP BY 1, 2
) 
, pickupdone AS (
SELECT DISTINCT
affi_shopid
, o.orderid
, "from_unixtime"(pickup_done_time) pickup_done_time
FROM
(((sip
INNER JOIN (
SELECT DISTINCT
shop_id shopid
, order_id orderid
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (("date"("split"(create_datetime, ' ')[1]) >= date '2022-03-28') AND (tz_type = 'local') AND grass_region IN ('PH') AND is_cb_shop = 1)
) o ON (o.shopid = sip.affi_shopid))
INNER JOIN (
SELECT
orderid
, "min"(ctime) pickup_done_time
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live
WHERE ((("date"("from_unixtime"(ctime)) BETWEEN "date_add"('day', -30, current_date) AND "date_add"('day', -1, current_date)) AND (new_status = 2)) AND (grass_region <> '0'))
GROUP BY 1
) p ON (p.orderid = o.orderid))
LEFT JOIN (
SELECT DISTINCT
shop_id shopid
, order_id orderid
FROM
mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE (((cancel_user_id = buyer_id) AND (("date"("split"(create_datetime, ' ')[1]) >= date '2022-03-28') AND (tz_type = 'local'))) AND (is_cb_shop = 1) AND grass_region IN ('PH'))
) o1 ON (o1.orderid = o.orderid))
WHERE (o1.orderid IS NULL)
) 
, o AS (
SELECT DISTINCT
o1.order_id orderid
, o1.shop_id shopid
, "date"("from_unixtime"(create_timestamp)) grass_date
, "from_unixtime"(cancel_timestamp) cancel_time
, o1.order_sn ordersn
, o2.cancel_reason
, o1.order_be_status_id status_ext
, o1.order_fe_status fe_status
, "from_unixtime"(shipping_confirm_timestamp) shipping_confirm_time
, "from_unixtime"(pay_timestamp) pay_time
, o1.payment_method_id
, o2.days_to_ship
, "from_unixtime"(create_timestamp) create_time
, (CASE WHEN (o2.cancel_userid = buyer_id) THEN 1 ELSE 0 END) buyer_cancel
, grass_region
, auto_cancel_arrange_ship_datetime ACL1
, auto_cancel_3pl_ack_datetime ACL2
, order_be_status
, logistics_status
FROM
(mp_order.dwd_order_all_ent_df__reg_s0_live o1
INNER JOIN (
SELECT
orderid
, extinfo.cancel_reason
, extinfo.cancel_userid
, extinfo.days_to_ship
FROM
marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE ((cb_option = 1) AND (grass_region <> '0'))
) o2 ON (o1.order_id = o2.orderid))
WHERE ((((is_cb_shop = 1) AND (grass_region <> '0')) AND ("date"("split"(create_datetime, ' ')[1]) >= date '2022-03-28')) AND (tz_type = 'local')) AND grass_region IN ('PH')
) 
SELECT DISTINCT
grass_date
, o.grass_region
, map.local_orderid
, o.orderid affi_orderid
, po.shop_id mst_shopid
, o.shopid affi_shopid
, create_time
, pay_time
, shipping_confirm_time
, po.cancel_time mst_cancel_time
, o.cancel_time affi_cancel_time
, buyer_cancel
, arrange_shipment_time
, pickup_done_time
, ACL1
, ACL2
, CASE WHEN wh_inbound_date > date('1970-01-01') THEN wh_inbound_date ELSE NULL END wh_inbound_date
, CASE WHEN wh_outbound_date > date('1970-01-01') THEN wh_outbound_date ELSE NULL END wh_outbound_date
, o.order_be_status a_order_be_status
, po.order_be_status p_order_be_status
, o.logistics_status a_logistics_status
, po.logistics_status p_logistics_status
FROM
(((sip
INNER JOIN o ON (o.shopid = sip.affi_shopid))
LEFT JOIN arrange_ship a ON (a.orderid = o.orderid))
LEFT JOIN pickupdone p ON (p.orderid = o.orderid))
LEFT JOIN (
SELECT DISTINCT oversea_orderid, local_orderid
FROM
marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
WHERE ((local_orderid <> 0) AND (schema <> ''))
) map ON o.orderid = map.oversea_orderid
LEFT JOIN (SELECT DISTINCT order_id, shop_id, "from_unixtime"(cancel_timestamp) cancel_time, order_be_status, logistics_status FROM mp_order.dwd_order_all_ent_df__reg_s0_live 
WHERE is_cb_shop = 0 AND grass_region = 'MY' AND ("date"("split"(create_datetime, ' ')[1]) >= date '2022-03-28') AND tz_type = 'local') po ON po.order_id = map.local_orderid
LEFT JOIN regcbbi_others.logistics_basic_info wh ON wh.ordersn = o.ordersn
-- WHERE pay_time IS NOT NULL