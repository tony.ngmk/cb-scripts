insert into nexus.test_548f50e832b15b5358f69919d9b3b3b3149d16887ec019ebbfbe3bd847be33b4_271082eb9ac4bc8b2a332007c7408dbb WITH target AS (
SELECT lane
, CAST(affi_itemid AS bigint) itemid 
, mst_region
, affi_region
, date(date_parse(upload_date,'%Y%m%d')) upload_date
FROM regcbbi_others.sip__bau_optimized_sku_list__wf__reg__s0
WHERE date(date_parse(upload_date,'%Y%m%d')) BETWEEN current_date - interval '21' day AND current_date - interval '15' day 
)


, item_status AS (
SELECT item_id
, shop_id
, level1_global_be_category
, lane 
, upload_date
, mst_region
, affi_region
, SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END) count_status_normal_days
, SUM(CASE WHEN stock > 0 THEN 1 ELSE 0 END) count_stock_normal_days
, SUM(CASE WHEN seller_status = 1 THEN 1 ELSE 0 END) count_shop_normal_days
, SUM(CASE WHEN is_holiday_mode = 0 THEN 1 ELSE 0 END) count_shop_no_holiday_days
FROM (
SELECT grass_date
, item_id
, shop_id 
, level1_global_be_category
, lane 
, upload_date
, mst_region
, affi_region
, status 
, stock 
, seller_status
, is_holiday_mode
FROM mp_item.dim_item__reg_s0_live i
INNER JOIN target t ON i.item_id = t.itemid AND (i.grass_date = t.upload_date + interval '1' day OR i.grass_date = t.upload_date + interval '14' day)
WHERE i.grass_region IN ('TW','PH','CL','CO','MX','MY','SG')
AND i.grass_date BETWEEN current_date - interval '21' day AND current_date - interval '1' day
AND i.tz_type = 'local'
AND i.is_cb_shop = 1
)
GROUP BY 1,2,3,4,5,6,7
)


, item_status_live AS (
SELECT item_id 
, shop_id 
, level1_global_be_category
, lane 
, upload_date
, mst_region
, affi_region
FROM item_status i
WHERE count_status_normal_days = 2
AND count_stock_normal_days = 2
AND count_shop_normal_days = 2
AND count_shop_no_holiday_days = 2
)


, imp AS (
SELECT grass_date
, level1_global_be_category
, lane 
, upload_date
, mst_region 
, affi_region
, COUNT(DISTINCT CASE WHEN i.item_organic_imp_uv_1d > 0 THEN i.item_id ELSE NULL END) AS item_with_imp_cnt
, COUNT(DISTINCT CASE WHEN i.item_organic_pdp_uv_1d > 0 THEN i.item_id ELSE NULL END) AS item_with_view_cnt
, SUM(i.item_organic_pdp_uv_1d) AS item_organic_pdp_uv_1d
, SUM(i.item_organic_imp_uv_1d) AS item_organic_imp_uv_1d
FROM mp_search.dws_item_view_imp_clk_1d__reg_s1_live i 
INNER JOIN item_status_live l ON i.item_id = l.item_id AND i.grass_date BETWEEN l.upload_date - interval '14' day AND l.upload_date + interval '14' day
WHERE i.grass_region IN ('TW','PH','CL','CO','MX','MY','SG')
AND i.tz_type = 'local'
AND grass_date BETWEEN current_date - interval '35' day AND current_date - interval '1' day
GROUP BY 1,2,3,4,5,6
)


, orders AS (
SELECT grass_date
, l.level1_global_be_category
, lane 
, upload_date
, mst_region 
, affi_region
, COUNT(DISTINCT o.item_id) AS item_with_organic_order_cnt
, SUM(o.order_fraction) AS organic_order_fraction 
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o 
INNER JOIN item_status_live l ON o.item_id = l.item_id AND o.grass_date BETWEEN l.upload_date - interval '14' day AND l.upload_date + interval '14' day 
WHERE o.grass_region IN ('TW','PH','CL','CO','MX','MY','SG')
AND o.tz_type = 'local'
AND o.grass_date BETWEEN current_date - interval '35' day AND current_date - interval '1' day
AND o.is_placed = 1 
AND o.is_flash_sale = 0
GROUP BY 1,2,3,4,5,6
)


SELECT imp.grass_date
, i.level1_global_be_category
, i.lane 
, i.mst_region
, i.affi_region
, i.upload_date
, i.total_item_count
, imp.item_with_imp_cnt
, imp.item_organic_imp_uv_1d
, imp.item_with_view_cnt
, imp.item_organic_pdp_uv_1d
, o.item_with_organic_order_cnt
, o.organic_order_fraction
FROM imp 
LEFT JOIN orders o ON imp.level1_global_be_category = o.level1_global_be_category
AND imp.lane = o.lane 
AND imp.upload_date = o.upload_date
AND imp.grass_date = o.grass_date
AND imp.mst_region = o.mst_region
AND imp.affi_region = o.affi_region
RIGHT JOIN (
SELECT level1_global_be_category
, lane 
, upload_date 
, mst_region 
, affi_region 
, COUNT(DISTINCT item_id) total_item_count
FROM item_status_live
GROUP BY 1,2,3,4,5
) i ON i.level1_global_be_category = imp.level1_global_be_category
AND i.lane = imp.lane 
AND i.upload_date = imp.upload_date
AND i.mst_region = imp.mst_region
AND i.affi_region = imp.affi_region