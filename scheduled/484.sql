insert into nexus.test_79d0aec9e60deb50bd89cffe98fb230a387f4d5c43545588857950f37e7fbce3_d6f470a9b2add10b667481ce62ea9b30 with holiday as (
select distinct u.shop_id, grass_region , b.grass_date, sku, coalesce(ggp_account_name,gp_account_name) ggp_account_name
from (select grass_date, shop_id,grass_region from mp_user.dim_shop__reg_s0_live
where grass_date = date(from_unixtime(1655870400)) 
and tz_type='local'
and is_holiday_mode=1
and is_cb_shop=1
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
and status=1) u 
join (select child_shopid, ggp_account_name,gp_account_name from regbd_sf.cb__seller_index_tab where grass_region!=' ' and gp_account_billing_country = 'China') a on cast(a.child_shopid as bigint)=u.shop_id
join (select distinct grass_date,shop_id, count(distinct item_id) as sku from mp_item.dim_item__reg_s0_live where grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN') and is_cb_shop=1 and seller_status=1 and shop_status=1 and grass_date = date(from_unixtime(1655870400)) and status=1 and stock>0 group by 1,2) b on b.shop_id=u.shop_id and b.grass_date=u.grass_date
) 

, po as (
select distinct shop_id, grass_region , grass_date, item_id, date(from_unixtime(create_timestamp)) as create_time
from (select *
from mp_item.dim_item__reg_s0_live u 
where 
grass_date =date(from_unixtime(1655870400)) 
and tz_type='local'
and (is_holiday_mode=0 or is_holiday_mode is null)
and is_cb_shop=1
and is_pre_order=1
and stock>0
and status=1
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
and shop_status=1 and seller_status=1) u 
join (select child_shopid, ggp_account_name,gp_account_name from regbd_sf.cb__seller_index_tab where grass_region!=' ' and gp_account_billing_country = 'China') a on cast(a.child_shopid as bigint)=u.shop_id
)


, oos as (
select distinct shop_id, grass_region , grass_date, item_id
from (select *
from mp_item.dim_item__reg_s0_live u 
where 
grass_date = date(from_unixtime(1655870400)) 
and tz_type='local'
and (is_holiday_mode=0 or is_holiday_mode is null)
and is_cb_shop=1
and stock=0
and status=1
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
and shop_status=1 and seller_status=1) U 
join (select child_shopid, ggp_account_name,gp_account_name from regbd_sf.cb__seller_index_tab where grass_region!=' ' and gp_account_billing_country = 'China') a on cast(a.child_shopid as bigint)=u.shop_id
)
, daily as (
select o.grass_region, if(o.grass_region in ('BR','CL','CO','MX','PL','ES'), date(from_unixtime(create_timestamp)), o.grass_date) as grass_date
, count(distinct case when is_cb_shop=1 then order_id end) as cb_ado
, count(distinct order_id ) as ado
, count(distinct case when is_cb_shop=1 and p.item_id is not null then order_id end) as cb_po_ado
from
(select *
from mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
where --grass_region in ('VN')
(if(grass_region in ('BR','CL','CO','MX','PL','ES'), date(from_unixtime(create_timestamp)), grass_date) = date(from_unixtime(1655870400)) )
and tz_type='local'
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
and is_placed=1
and is_bi_excluded=0
) o 
left join (select * from po ) p on p.item_id=o.item_id and if(o.grass_region in ('BR','CL','CO','MX','PL','ES'), date(from_unixtime(create_timestamp)), o.grass_date)=p.grass_date
group by 1,2
)
, sku as (
select distinct grass_region , grass_date
, count(distinct case when is_cb_shop=1 then item_id end ) as cb_sku
, count(distinct item_id ) as country_sku
from mp_item.dim_item__reg_s0_live u 
where 
grass_date =date(from_unixtime(1655870400)) 
and tz_type='local'
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
and (is_holiday_mode=0 or is_holiday_mode is null)
and stock>0
and status=1
and shop_status=1 and seller_status=1
group by 1,2
)
, hol as (
select l.grass_region,l.grass_date
, sum(holiday_ado) as holiday_ado_pre 
from holiday l
left join 
(select shop_id, count(distinct order_id)*1.0000/7 as holiday_ado
from mp_order.dwd_order_place_pay_complete_di__reg_s0_live
where 
(if(grass_region in ('BR','CL','CO','MX','PL','ES'), date(from_unixtime(create_timestamp)), grass_date) between date'2022-03-01' and date'2022-03-07') --date'2020-12-01' and date'2020-12-31'
and tz_type='local'
and is_cb_shop=1
and is_placed=1
and is_bi_excluded=0
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
group by 1
) o on o.shop_id = l.shop_id 
group by 1,2
)


, po_ado as (
select l.grass_region,l.grass_date
, sum(holiday_ado) as po_ado_pre --count(distinct order_id) as holiday_ado
from po l
left join 
(select item_id, sum(order_fraction)*1.0000/7 as holiday_ado
from mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
where --grass_region in ('VN')
(if(grass_region in ('BR','CL','CO','MX','PL','ES'), date(from_unixtime(create_timestamp)), grass_date) between date'2022-03-01' and date'2022-03-07') --date'2020-12-01' and date'2020-12-31'
and tz_type='local'
and is_cb_shop=1
and is_placed=1
and is_bi_excluded=0
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
group by 1
) o on o.item_id = l.item_id 
group by 1,2
)


, os as (
select l.grass_region,l.grass_date
, sum(oos_ado) as oos_ado_pre --count(distinct order_id) as holiday_ado
from oos l
left join 
(select item_id, sum(order_fraction)*1.0000/7 as oos_ado
from mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
where --grass_region in ('VN')
(if(grass_region in ('BR','CL','CO','MX','PL','ES'), date(from_unixtime(create_timestamp)), grass_date) between date'2022-03-01' and date'2022-03-07') --date'2020-12-01' and date'2020-12-31'
and tz_type='local'
and is_cb_shop=1
and is_placed=1
and is_bi_excluded=0
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
group by 1
) o on o.item_id = l.item_id 
group by 1,2
)


, pre as (
select o.grass_region, date(from_unixtime(1655870400)) as grass_date
, count(distinct case when is_cb_shop=1 then order_id end )*1.0000/7 as cb_pre_ado
, count(distinct order_id )*1.0000/7 as pre_ado
from mp_order.dwd_order_place_pay_complete_di__reg_s0_live o
where 
(if(grass_region in ('BR','CL','CO','MX','PL','ES'), date(from_unixtime(create_timestamp)), grass_date) between date'2022-03-01' and date'2022-03-07') 
and grass_region in ('BR','CL','CO','MX','PL','ES','SG','TW','MY','TH','PH','ID','VN')
and tz_type='local'
and is_placed=1
and is_bi_excluded=0
group by 1,2
)


(select o.grass_date, o.grass_region, cb_ado,cb_ado*1.0000/ado as cb_ado_pene, holiday_shop,holiday_ggp,holiday_ado_pre, try(holiday_ado_pre/pre_ado) as holiday_coverage,cb_po_ado*1.0000/ado as po_coverage ,po_item,try(po_ado_pre/pre_ado) as po_coverage_pre,oos_ado_pre,oos_item,try(oos_ado_pre/pre_ado) as oos_coverage,(cb_pre_ado*1.000/pre_ado) as cb_ado_pre_pene, cb_sku,(cb_sku*1.000/country_sku) as cb_sku_pene ,sku as holiday_sku
from daily o 
left join (select grass_region, grass_date, count(distinct shop_id) as holiday_shop, count(distinct ggp_account_name) as holiday_ggp, sum(sku) as sku from holiday group by 1,2) s on o.grass_region=s.grass_region and o.grass_date=s.grass_date
left join hol h on h.grass_region=o.grass_region and o.grass_date=h.grass_date
left join pre d on d.grass_region=o.grass_region 
left join po_ado p on p.grass_region=o.grass_region and o.grass_date=p.grass_date
left join os s1 on s1.grass_region=o.grass_region and o.grass_date=s1.grass_date
left join sku on sku.grass_region=o.grass_region and o.grass_date=sku.grass_date
left join (select grass_region, grass_date, count(distinct item_id) as oos_item from oos group by 1,2) o1 on o.grass_region=o1.grass_region and o.grass_date=o1.grass_date
left join (select grass_region, grass_date, count(distinct item_id) as po_item from po group by 1,2) p1 on o.grass_region=p1.grass_region and o.grass_date=p1.grass_date
order by 2,1)
UNION
(select o.grass_date, 'Total' as grass_region, cb_ado,cb_ado*1.0000/ado as cb_ado_pene, holiday_shop,holiday_ggp,holiday_ado_pre, try(holiday_ado_pre/pre_ado) as holiday_coverage,cb_po_ado*1.0000/ado as po_coverage ,po_item,try(po_ado_pre/pre_ado) as po_coverage_pre,oos_ado_pre,oos_item,try(oos_ado_pre/pre_ado) as oos_coverage,(cb_pre_ado*1.000/pre_ado) as cb_ado_pre_pene, cb_sku,(cb_sku*1.000/country_sku) as cb_sku_pene ,sku as holiday_sku
from (select grass_date,sum(cb_ado) as cb_ado, sum(ado) as ado,sum(cb_po_ado) as cb_po_ado from daily group by 1) o 
left join (select grass_date, count(distinct shop_id) as holiday_shop, count(distinct ggp_account_name) as holiday_ggp, sum(sku) as sku from holiday group by 1) s on o.grass_date=s.grass_date
left join (select grass_date, sum(holiday_ado_pre) as holiday_ado_pre from hol group by 1 ) h on o.grass_date=h.grass_date
left join (select grass_date, sum(cb_pre_ado) as cb_pre_ado, sum(pre_ado) as pre_ado from pre group by 1) d on d.grass_date=o.grass_date 
left join (select grass_date, sum(oos_ado_pre) as oos_ado_pre from os group by 1) s1 on o.grass_date=s1.grass_date
left join (select grass_date, count(distinct item_id) as oos_item from oos group by 1) o1 on o.grass_date=o1.grass_date
left join (select grass_date, count(distinct item_id) as po_item from po group by 1) p1 on o.grass_date=p1.grass_date
left join (select grass_date, sum( po_ado_pre) as po_ado_pre from po_ado group by 1) po_ado on o.grass_date=po_ado.grass_date
left join (select grass_date, sum( cb_sku) as cb_sku, sum( country_sku) as country_sku from sku group by 1) sku on o.grass_date=sku.grass_date
order by 2,1)