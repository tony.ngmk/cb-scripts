insert into nexus.test_183a028acade5d219d2229bb4cac214847c95f3219a9e7c3dbf0a04282c4ad61_1d546c9425beb3cb19c847bcd07584a0 -- 2022/04/06 updates:
-- resolved bundle deal recording issue 
-- in dwd_order_item_all_ent_df__reg_s0_live, if one model is bought many pieces, some are in bundles deals, some are not, then the table will record 2 entries, one with bundle deal quantity, and one with the rest of the quantity
-- this will cause double countting on settlement price




WITH wh AS (SELECT distinct * FROM regcbbi_others.logistics_basic_info WHERE wh_outbound_date >= CURRENT_DATE - INTERVAL '240' DAY)

, sip AS (SELECT DISTINCT affi_itemid
,item_margin/100000.00 AS item_margin
,affi_real_weight/100.00 AS affi_real_weight
,b.country AS mst_country 
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a 
JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON a.mst_shopid = b.shopid
WHERE b.cb_option = 1 )


, oi AS (
SELECT order_id,
item_id,
shop_id,
model_id,
order_sn,

mtsku_item_id,
mtsku_model_id,
merchant_id,
is_flash_sale,
shipping_carrier,
shipping_channel_id,
category_id,
category_level,
level1_category_id,
level1_category,
level2_category_id,
level2_category,
level3_category_id,
level3_category,
level4_category_id,
level4_category,
level5_category_id,
level5_category,
global_be_category_id,
global_be_category,
global_be_category_level,
level1_global_be_category_id,
level1_global_be_category,
level2_global_be_category_id,
level2_global_be_category,
level3_global_be_category_id,
level3_global_be_category,
level4_global_be_category_id,
level4_global_be_category,
level5_global_be_category_id,
level5_global_be_category,
item_amount,
gmv,
gmv_usd,
nmv,
nmv_usd,
cogs,
cogs_usd,
merchandise_subtotal_amt,
merchandise_subtotal_amt_usd,
item_input_price_pp,
item_input_price_pp_usd,
item_price_pp,
item_price_pp_usd,
order_price_pp,
order_price_pp_usd,
item_price_before_discount_pp,
item_price_before_discount_pp_usd,
item_promotion_id,
item_promotion_source,
flash_sale_type_id,
flash_sale_type,
commission_rule_id,
commission_fee,
commission_fee_usd,
commission_cap_amt,
commission_cap_amt_usd,
commission_rate/100.00 AS commission_rate,
commission_rule_name,
commission_base_amt,
commission_base_amt_usd,
item_tax_amt,
item_tax_amt_usd,
item_tax_exemption_amt,
item_tax_exemption_amt_usd,
item_weight_pp*1000 AS item_weight_pp,
is_bundle_deal,
bundle_order_item_id,
o.grass_region,
item_margin,
wh_outbound_date,
item_rebate_by_seller_amt,
item_rebate_by_seller_amt_usd,
item_rebate_by_shopee_amt,
item_rebate_by_shopee_amt_usd,
coin_earn,
coin_earn_cash_amt,
coin_earn_cash_amt_usd,
coin_earn_by_basic,
coin_earn_by_basic_cash_amt,
coin_earn_by_basic_cash_amt_usd,
coin_used,
coin_used_cash_amt,
coin_used_cash_amt_usd,
pv_coin_earn_by_shopee,
pv_coin_earn_by_shopee_amt,
pv_coin_earn_by_shopee_amt_usd,
pv_coin_earn_by_seller,
pv_coin_earn_by_seller_amt,
pv_coin_earn_by_seller_amt_usd,
sv_coin_earn_by_shopee,
sv_coin_earn_by_shopee_amt,
sv_coin_earn_by_shopee_amt_usd,
sv_coin_earn_by_seller,
sv_coin_earn_by_seller_amt,
sv_coin_earn_by_seller_amt_usd,
fsv_promotion_id,
fsv_promotion_name,
fsv_voucher_code,
sv_promotion_id,
sv_promotion_name,
sv_voucher_code,
sv_voucher_type,
is_sv_seller_absorbed,
sv_rebate_by_shopee_amt,
sv_rebate_by_shopee_amt_usd,
sv_rebate_by_seller_amt,
sv_rebate_by_seller_amt_usd,
pv_promotion_id,
pv_promotion_name,
pv_voucher_code,
pv_voucher_type,
is_pv_seller_absorbed,
pv_rebate_by_shopee_amt,
pv_rebate_by_shopee_amt_usd,
pv_rebate_by_seller_amt,
pv_rebate_by_seller_amt_usd,
estimate_shipping_fee,
estimate_shipping_fee_usd,
o.actual_shipping_fee,
actual_shipping_fee_usd,
actual_buyer_paid_shipping_fee,
actual_buyer_paid_shipping_fee_usd,
buyer_paid_shipping_fee,
buyer_paid_shipping_fee_usd,
shopee_snapshot_discount_shipping_fee,
shopee_snapshot_discount_shipping_fee_usd,
seller_snapshot_discount_shipping_fee,
seller_snapshot_discount_shipping_fee_usd,
shipping_discount_promotion_type_id,
shipping_discount_promotion_type,
estimate_shipping_rebate_by_shopee_amt,
estimate_shipping_rebate_by_shopee_amt_usd,
actual_shipping_rebate_by_shopee_amt,
actual_shipping_rebate_by_shopee_amt_usd,
estimate_shipping_rebate_by_seller_amt,
estimate_shipping_rebate_by_seller_amt_usd,
actual_shipping_rebate_by_seller_amt,
actual_shipping_rebate_by_seller_amt_usd,
card_rebate_by_bank_amt,
card_rebate_by_bank_amt_usd,
card_rebate_by_shopee_amt,
card_rebate_by_shopee_amt_usd,
seller_txn_fee,
seller_txn_fee_usd,
seller_txn_fee_shipping_fee,
seller_txn_fee_shipping_fee_usd,
buyer_txn_fee,
buyer_txn_fee_usd,
service_fee,
service_fee_usd,
service_fee_info_list,
order_fraction,
affi_real_weight,
wh_inbound_date,
DATE(FROM_UNIXTIME(cancel_timestamp)) AS cancel_timestamp,
cancel_user_id,
mst_country,
DATE(date_parse(create_datetime,'%Y-%m-%d %T')) AS grass_date
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
JOIN sip ON o.item_id = sip.affi_itemid
LEFT JOIN wh ON o.order_sn = wh.ordersn
WHERE is_cb_shop = 1 AND grass_date >= CURRENT_DATE - INTERVAL '240' DAY 
AND DATE(date_parse(create_datetime,'%Y-%m-%d %T')) >= CURRENT_DATE - INTERVAL '240' DAY
-- AND o.order_id = 101927172533696
-- AND o.grass_region = 'MY'
-- AND o.grass_date = DATE('2022-03-26')
)

, sip_item AS (SELECT affi_itemid
, affi_modelid
, mst_itemid
, mst_modelid
, mst_promo_price/100000.00 AS mst_promo_price
, mst_orig_price/100000.00 AS mst_orig_price
, mst_hpfs/100000.00 AS mst_hpfs
, mst_hpfn/100000.00 AS mst_hpfn
, affi_hpfn/100000.00 AS affi_hpfn
, item_price/100000.00 AS item_price
, TRY_CAST(JSON_EXTRACT(extinfo,'$.settlement_price') AS DOUBLE)/100000.00 AS settlement_price
, orderid
FROM marketplace.shopee_sip_db__sip_order_item_snapshot_tab__reg_daily_s0_live
WHERE DATE(FROM_UNIXTIME(ctime)) >= CURRENT_DATE - INTERVAL '240' DAY)

, sku_off AS (SELECT DISTINCT * FROM regcbbi_others.sip_sku_offline_settlement_v2 WHERE grass_date >= CURRENT_DATE - INTERVAL '240' DAY)




, stmt_consol AS (
SELECT 
DISTINCT order_id
, itemid
, modelid
, currency real_currency
, exchange_rate -- shopee USD/affi 
, quantity cnsc_qty
, AVG(settlement_price/100000.00) sip_settlement_pp
, SUM(settlement_price*quantity)/100000.00 AS sip_settlement
FROM regcbbi_others.sip_supplier_table_detail
GROUP BY 1,2,3,4,5,6
)



SELECT DISTINCT oi.*
, mst_itemid
, mst_modelid
, mst_promo_price
, mst_orig_price
, mst_hpfs
, mst_hpfn
, affi_hpfn
, item_price
-- , sip_settlement settlement_price 
, sip_settlement -- stmt currency
, sip_settlement_pp
, real_currency AS stmt_currency
, COALESCE(campaign_price,0) AS campaign_price
, COALESCE(kind,0) kind
, UPPER(mst_curr) AS mst_curr
FROM oi 
JOIN (SELECT DISTINCT orderid FROM marketplace.shopee_sip_db__sip_order_tab__reg_daily_s0_live 
WHERE DATE(FROM_UNIXTIME(ctime)) >= CURRENT_DATE - INTERVAL '240' DAY) so ON oi.order_id = so.orderid
LEFT JOIN sip_item 
ON oi.item_id = sip_item.affi_itemid 
AND oi.model_id = sip_item.affi_modelid 
AND oi.order_id = sip_item.orderid 
LEFT JOIN stmt_consol 
ON oi.item_id = stmt_consol.itemid 
AND oi.model_id = stmt_consol.modelid 
AND oi.order_id = stmt_consol.order_id 
AND oi.item_amount = stmt_consol.cnsc_qty
LEFT JOIN sku_off ON oi.item_id = sku_off.itemid AND sku_off.grass_date = oi.grass_date
-- WHERE oi.order_id = 86053147081042
-- AND oi.grass_region = 'BR'
-- AND oi.grass_date = DATE('2022-03-26')
-- limit 10