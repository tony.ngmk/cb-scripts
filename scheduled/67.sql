SELECT o.Purchase_date as PurchaseDate
, coalesce(CBOrder, 0) as CBOrder
, round(coalesce(CB_airpay_orders,0)*1.0000/CBOrder, 2) as CBAirpayorder_pct
, '0' as CampaignOrder
, '0' as CampaignOrder_pct
, coalesce(cb_CFS,0) as CFSOrder
, coalesce(cb_CFS,0)*1.0000/country_cfs as CFSOrder_pct
, coalesce(cb_1k_CFS, 0) as CBKCFSOrder
, coalesce(cb_1k_CFS, 0)*1.0000/nullif(country_1k_cfs, 0) as KCFSOrder_pct
, coalesce(cb_ads_orders, 0) as CBAdsOrder
, coalesce(cb_ads_orders, 0)*1.0000/nullif(country_ads_orders, 0) as CBAdsOrder_pct
, coalesce(CBGMV, 0) as CBGMV
, coalesce(cb_new, 0) AS CBNewSKU
, coalesce(cb_live, 0) as CBTotalSKU
, coalesce(lc_new+cb_new, 0) AS CountryNewSKU
, coalesce(lc_live+cb_live, 0) AS CountryTotalSKU
, coalesce(CBItemsSold, 0) as CBItemsSold
, coalesce(round(CBGMV*1.0000/CBOrder, 2), 0) as CBABSUSD
, coalesce(round(CancelledOrders*1.0000/CBOrder, 4), 0) as CancellationRate
, coalesce(round(CBOrder*1.0000/country_order, 4), 0) as CBOrder_pct
, coalesce(round(CB_order_COD_and_CC_only*1.0000/country_order_COD_and_CC_only, 4), 0) as CBOrder_pct_CODandCConly 
, coalesce(view,0) as CBView
, coalesce(round(view*1.0000/country_view, 4), 0) as CBView_pct
, coalesce((CBOrder*1.0000/view),0) as CBCR
, coalesce(round((CBOrder*1.0000/view)*1.0000/(country_order*1.0000/country_view), 4), 0) as CBCR_pct
, coalesce(round(CBGMV*1.0000/country_gmv, 4), 0) as CBGMV_pct
, coalesce(round(cb_live*1.0000/(lc_live+cb_live), 4), 0) as CBSKU_pct
, round(country_gmv*1.0000/country_order, 2) as CountryABSUSD
, country_order as TotalCountryOrders
, '0' as CampaignItemSold
, coalesce(CancelledOrders, 0) as CancelledOrders
, KRCBOrder
, KRCBGMV
, coalesce(krcb_CFS,0) as KRCBCFSOrder
, '0' as KRCBCampaignOrder


FROM /* SKU */ 
(SELECT distinct date(split(create_datetime,' ')[1]) as Purchase_date, 
count(CASE WHEN u.is_cb_shop = 1 THEN item_id else null end) over (ORDER BY date(split(create_datetime,' ')[1])) as cb_live, 
count(CASE WHEN u.is_cb_shop = 0 THEN item_id else null end) over (ORDER BY date(split(create_datetime,' ')[1])) as lc_live, 
count(CASE WHEN u.is_cb_shop = 1 THEN item_id else null end) over (PARTITION BY date(split(create_datetime,' ')[1])) as cb_new,
count(CASE WHEN u.is_cb_shop = 0 THEN item_id else null end) over (PARTITION BY date(split(create_datetime,' ')[1])) as lc_new
from (select * from mp_item.dim_item__reg_s0_live where grass_region='VN' and status=1 and stock>0 and grass_date= date(from_unixtime(1655686800)) and tz_type='local' and seller_status=1 and shop_status=1 and (is_holiday_mode = 0 or is_holiday_mode is null) )u 
join (select * from mp_user.dws_user_login_td_account_info__reg_s0_live where grass_region='VN' and grass_date=date(from_unixtime(1655686800)) and tz_type='local' and shop_id not in (134344983,145418026,23921767,1754914,116639257) and date(split(last_login_datetime_td,' ')[1])>=date_add('day',-6,date(from_unixtime(1655686800))) ) i on i.shop_id = u.shop_id
) s

right join /* ADO GMV */
(SELECT b.grass_date as Purchase_date
, count(distinct case when b.is_cross_border = 1 then b.orderid ELSE null end) AS CBOrder
, count(distinct b.orderid) AS country_order
, count(distinct case when b.is_cross_border = 1 and payment_method_id in (28, 30) then b.orderid ELSE null end) AS CB_airpay_orders
, count(distinct (case when (payment_method_id in (1,6)) then b.orderid else null end)) as country_order_COD_and_CC_only
, count(distinct (case when (payment_method_id in (1,6)) and b.is_cross_border=1 then b.orderid else null end)) as CB_order_COD_and_CC_only
, round(sum(case when b.is_cross_border = 1 then gmv_usd else 0 end), 2) AS CBGMV
, round(sum(gmv_usd), 2) AS country_gmv
, sum(case when b.is_cross_border = 1 then amount else 0 end) as CBItemsSold
, count(distinct case when b.status = 4 and b.is_cross_border = 1 THEN b.orderid ELSE null END) as CancelledOrders
, count(distinct case when b.is_cross_border = 1 and gp_account_billing_country='KR' then b.orderid ELSE null end) as KRCBOrder
, round(sum(case when b.is_cross_border = 1 and gp_account_billing_country='KR' then gmv_usd else 0 end), 2) AS KRCBGMV
from
(select 
grass_date
, shop_id
, order_id as orderid
, is_cb_shop as is_cross_border
, gmv_usd
, payment_method_id
, item_amount as amount
, order_fe_status_id as status
FROM 
mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE 
grass_region = 'VN' 
AND grass_date BETWEEN date_add('day',-13,date(from_unixtime(1655686800))) and date(from_unixtime(1655686800))
and tz_type='local' 
and is_placed = 1 
AND (is_bi_excluded = 0 OR is_bi_excluded IS NULL)
) b
LEFT JOIN (SELECT 
DISTINCT shop_id
, merchant_region AS gp_account_billing_country
FROM
marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live
) s 
on 
s.shop_id= b.shop_id
group by 
1
) o 
on 
s.Purchase_date = o.Purchase_date

left join /* view */
(select grass_date as Purchase_date, sum(CASE WHEN is_cb_shop = 1 then pv_cnt_1d ELSE 0 END) as view, sum(pv_cnt_1d) as country_view
from
(select * 
from mp_shopflow.dws_item_civ_1d__reg_s0_live
where grass_region = 'VN' and grass_date between date_add('day',-13,date(from_unixtime(1655686800))) and date(from_unixtime(1655686800)) and tz_type='local'
and shop_id not in (134344983,145418026,23921767,1754914,116639257)
) a 
left join (select shop_id, is_cb_shop from mp_user.dim_shop__reg_s0_live where grass_region = 'VN' and grass_date= date(from_unixtime(1655686800)) and tz_type='local' ) b on a.shop_id=b.shop_id
group by 1) v
on o.Purchase_date = v.Purchase_date


-- LEFT JOIN /* campaign */
-- (select Purchase_date, 
-- sum(case when is_cross_border = 1 then order_fraction else 0 end) as CampaignOrder, 
-- SUM(case when is_cross_border = 1 then amount else 0 end) as CampaignItemSold,
-- sum(order_fraction) as country_campaign,
-- sum(case when is_cross_border = 1 and gp_account_billing_country='KR' then order_fraction else 0 end) as KRCBCampaignOrder
-- from 
-- (select distinct c2.grass_date as Purchase_date, c2.orderid, c2.shop_id shopid, c2.itemid, c2.is_cross_border, sum(c2.order_fraction) as order_fraction, sum(amount) as amount
-- from 
-- (select distinct item_id, collection_id, start_time, end_time 
-- from regbida_userbehavior.shopee_bi_campaign_item 
-- where grass_region = 'VN' and grass_date between date_add('day',-13,date(from_unixtime(1655686800))) and date(from_unixtime(1655686800))
-- ) c1 
-- right join 
-- (select grass_date, shop_id, order_id as orderid, is_cb_shop as is_cross_border, gmv_usd, item_id as itemid, model_id as modelid,order_fraction, create_timestamp-3600 as event_timestamp, item_amount as amount
-- FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
-- WHERE grass_region = 'VN' AND grass_date BETWEEN date_add('day',-13,date(from_unixtime(1655686800))) and date(from_unixtime(1655686800)) and tz_type='local' and is_placed =1
-- and shop_id not in (134344983,145418026,23921767,1754914,116639257) ) c2 on c1.item_id = c2.itemid
-- where c2.event_timestamp between c1.start_time and c1.end_time
-- group by 1,2,3,4,5 ) o
-- LEFT JOIN (SELECT 
-- DISTINCT shop_id
-- , merchant_region AS gp_account_billing_country
-- FROM
-- marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live) s on s.shop_id= o.shopid
-- group by 1
-- ) c on o.Purchase_date = c.Purchase_date


left join

(select grass_date as purchase_date
, sum(case when promo_source = 'flash_sale' and is_cross_border=1 then order_fraction else 0 end) as cb_CFS
, sum(case when promo_source = 'flash_sale' then order_fraction else 0 end) as country_cfs
, sum(case when promo_source = 'flash_sale' and is_cross_border = 1 and item_price = 1000 then order_fraction else 0 end) as cb_1k_CFS
, sum(case when promo_source = 'flash_sale' and item_price = 1000 then order_fraction else 0 end) as country_1k_cfs
, sum(case when promo_source = 'flash_sale' and is_cross_border=1 and gp_account_billing_country='KR' then order_fraction else 0 end) as krcb_CFS

from 
(select grass_date, shop_id, order_id as orderid, is_cb_shop as is_cross_border, gmv_usd, item_id as itemid, item_price_pp as item_price,order_fraction, item_promotion_source as promo_source
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE grass_region = 'VN' AND grass_date BETWEEN date_add('day',-13,date(from_unixtime(1655686800))) and date(from_unixtime(1655686800)) and tz_type='local' and is_placed =1
and shop_id not in (134344983,145418026,23921767,1754914,116639257) 
) o 
LEFT JOIN (SELECT 
DISTINCT shop_id
, merchant_region AS gp_account_billing_country
FROM
marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live) s on s.shop_id= o.shop_id
group by 1
) A
on O.purchase_date = A.purchase_date


LEFT JOIN /* paid-ads order */
(select grass_date
, sum(case when is_cross_border = 1 then ads_orders else 0 end) as cb_ads_orders
, sum(ads_orders) as country_ads_orders
from
(select grass_date
, shop_id
, order_cnt as ads_orders
from mp_paidads.ads_advertise_mkt_1d__reg_s0_live
where grass_region = 'VN'
and tz_type = 'local'
and grass_date between date_add('day',-13,date(from_unixtime(1655686800))) and date(from_unixtime(1655686800))) ad
join
(select distinct shop_id as shopid, is_cb_shop as is_cross_border
from mp_user.dim_shop__reg_s0_live
where grass_region = 'VN' and grass_date=date(from_unixtime(1655686800)) and tz_type='local'
) u
on ad.shop_id = u.shopid
group by 1
) ads
on o.Purchase_date = ads.grass_date


order by o.purchase_date DESC