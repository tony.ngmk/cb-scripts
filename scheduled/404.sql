insert into nexus.test_f4e47d0c894101f1a1015052d20bea4d0ed04066f502956ef83e4f174aca7b7f_1eefad8653ac69ea656e57de15860482 WITH sip as (
SELECT DISTINCT
b.affi_shopid
, b.mst_shopid
, d.user_name affi_username
, c.user_name mst_username
FROM (SELECT shopid
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live WHERE country = 'ID' AND cb_option = 0 ) a
LEFT JOIN (SELECT distinct affi_shopid,mst_shopid 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE country IN ( 'TH' ) and grass_region = grass_region) b ON b.mst_shopid = a.shopid 
LEFT JOIN ( SELECT distinct user_name,shop_id
FROM mp_user.dim_shop__reg_s0_live
WHERE grass_date = current_date - interval '2' day AND tz_type ='local'
) c ON c.shop_id = b.mst_shopid
LEFT JOIN ( SELECT distinct user_name,shop_id
FROM mp_user.dim_shop__reg_s0_live
WHERE grass_date = current_date - interval '2' day AND tz_type ='local'
) d ON d.shop_id = b.affi_shopid
)




, u AS (
SELECT DISTINCT
user_id
, shop_id
, user_name
, grass_region
FROM mp_user.dim_user__reg_s0_live
WHERE grass_region IN ( 'TH')
and grass_date = current_date - interval '1' day
)




SELECT DISTINCT
ctime comment_timestamp
, a.userid buyer_userid
, buyer.user_name buyer_username
, seller.user_id seller_userid
, seller.user_name seller_username
, a.shopid seller_shopid
, itemid
, comment
, rating_star
, (CASE 
WHEN buyer.grass_region = 'SG' THEN CONCAT('https://shopee.sg/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
WHEN buyer.grass_region = 'PH' THEN CONCAT('https://shopee.ph/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
WHEN buyer.grass_region = 'TH' THEN CONCAT('https://shopee.co.th/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
WHEN buyer.grass_region = 'VN' THEN CONCAT('https://shopee.vn/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
WHEN buyer.grass_region = 'BR' THEN CONCAT('https://shopee.com.br/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
WHEN buyer.grass_region = 'MX' THEN CONCAT('https://shopee.com.mx/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
WHEN buyer.grass_region = 'CO' THEN CONCAT('https://shopee.com.co/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
WHEN buyer.grass_region = 'CL' THEN CONCAT('https://shopee.cl/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
ELSE CONCAT('https://shopee.com.my/product/', CAST(a.shopid as VARCHAR), '/', CAST(itemid as VARCHAR))
END
) product_link
, date_diff('day', DATE(ctime), current_date) days_since_comment
, buyer.grass_region country
FROM (
SELECT DISTINCT
cmtid
, userid
, orderid
, shopid
, itemid
, modelid
, rating_star
, from_unixtime(ctime) ctime
, comment
, extinfo
, extinfo.itemratingreply
FROM marketplace.shopee_item_comment_db__item_cmt_v2_tab__reg_daily_s0_live a
JOIN sip ON sip.affi_shopid = a.shopid
WHERE rating_star <= 3 
AND DATE(from_unixtime(ctime)) >= date_add('day', -7, current_date)
AND extinfo.itemratingreply is null
AND extinfo.country IN ( 'TH' )
) a 


LEFT JOIN u buyer on buyer.user_id = a.userid
LEFT JOIN u seller ON seller.shop_id = a.shopid
ORDER BY 1 DESC