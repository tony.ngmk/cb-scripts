insert into nexus.test_4f5a60ea323246a66246f544cdeb7d90fba9f45af17192fc15aac7a93d42e727_82575be22d424980da3d5ec7cae76505 WITH
sip AS (
SELECT DISTINCT
affi_shopid
, mst_shopid
, t1.country mst_country
, t2.country affi_country
, t1.cb_option
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE ((sip_shop_status <> 3) AND (grass_region IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR')))
) t2 ON (t1.shopid = t2.mst_shopid))
) 
, unlist_reason AS (
SELECT DISTINCT affi_itemid
FROM
((marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT item_id
FROM
mp_item.dim_item__reg_s0_live
WHERE ((((((grass_date = (current_date - INTERVAL '1' DAY)) AND (is_cb_shop = 1)) AND (seller_status = 1)) AND (shop_status = 1)) AND (is_holiday_mode = 0)) AND (status = 8))
) i ON (i.item_id = a.affi_itemid))
LEFT JOIN (
SELECT DISTINCT
mst_itemid
, unlist_region
FROM
marketplace.shopee_sip_db__item_unlist_config_tab__reg_daily_s0_live
) b ON ((b.mst_itemid = a.mst_itemid) AND (a.affi_country = b.unlist_region)))
WHERE ((unlist_factor = 0) AND (b.unlist_region IS NULL))
) 
, listing_rank AS (
SELECT
grass_region
, item_id
, shop_id
, stock
, "row_number"() OVER (PARTITION BY shop_id ORDER BY sold DESC, max_mtime DESC) item_rank
FROM
(
SELECT DISTINCT
itemid item_id
, shopid shop_id
, grass_region
, stock
, COALESCE(sold, 0) sold
, "max"(mtime) OVER (PARTITION BY shopid, itemid) max_mtime
FROM
marketplace.shopee_item_v4_db__item_v4_tab__reg_daily_s0_live
WHERE ((cb_option = 1) AND (status IN (1, 8)))
and grass_region IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR')
) 
) 


SELECT DISTINCT
t1.*
, t2.current_listing_limit new_listing_limit
, user_status
, is_holiday_mode
, active_item_cnt
, active_item_with_stock_cnt
, (CASE WHEN ((t2.current_listing_limit - active_item_cnt) > 10) THEN 'Y' ELSE 'N' END) CAN_RESYNC_TIER1
, ITEMID
, mtime
, L30D_ADO
, L60D_ADO
, sold
, item_rank
FROM
(((((((
SELECT DISTINCT
affi_shopid
, affi_country
, mst_country
, cb_option
, current_listing_limit
FROM
regcbbi_others.ops_shop_limit_log
WHERE ((CAST(grass_date AS date) = (current_date - INTERVAL '2' DAY)) AND (AFFI_COUNTRY IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR')))
) t1
INNER JOIN (
SELECT DISTINCT
shop_id
, status
, user_status
, is_holiday_mode
, active_item_cnt
, active_item_with_stock_cnt
FROM
regcbbi_others.shop_profile
WHERE (((grass_date = (current_date - INTERVAL '1' DAY)) AND (is_cb_shop = 1)) AND (grass_region IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR')))
) u ON (u.shop_id = t1.affi_shopid))
INNER JOIN (
SELECT DISTINCT
affi_shopid
, current_listing_limit
FROM
regcbbi_others.ops_shop_limit_log
WHERE ((CAST(grass_date AS date) = (current_date - INTERVAL '1' DAY)) AND (AFFI_COUNTRY IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR')))
) t2 ON (t2.affi_shopid = t1.affi_shopid))
INNER JOIN (
SELECT DISTINCT
itemid
, shopid
, sold
, stock
, "date"("from_unixtime"(mtime)) mtime
FROM
marketplace.shopee_item_v4_db__item_v4_tab__reg_daily_s0_live
WHERE ((status = 8) AND (grass_region IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR')))
) i ON (i.SHOPID = T1.AFFI_SHOPID))
INNER JOIN unlist_reason ur ON (ur.affi_itemid = i.itemid))
LEFT JOIN listing_rank lr ON (lr.item_id = i.itemid))
LEFT JOIN (
SELECT DISTINCT
item_id
, ("sum"(order_fraction) / DECIMAL '60.00') L60D_ADO
, ("sum"((CASE WHEN ("date"("split"(create_datetime, ' ')[1]) BETWEEN (current_date - INTERVAL '30' DAY) AND (current_date - INTERVAL '1' DAY)) THEN order_fraction ELSE null END)) / DECIMAL '30.00') L30D_ADO
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (((is_cb_shop = 1) AND ("date"("split"(create_datetime, ' ')[1]) BETWEEN (current_date - INTERVAL '60' DAY) AND (current_date - INTERVAL '1' DAY))) AND (grass_region IN ('SG', 'MY', 'ID', 'PH', 'TH', 'VN', 'TW', 'BR')))
GROUP BY 1
) o ON (o.item_id = I.ITEMID))
WHERE (((t1.current_listing_limit > t2.current_listing_limit) AND (sold > 0)) AND (i.stock > 0))