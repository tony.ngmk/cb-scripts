insert into nexus.test_f38e5e179d1046c89744deff7a25899f143ff8d97e85aff6d3d4c354ebd58e13_e784dab6efb42f2cdc15fbf70962a026 WITH sip AS (
SELECT
DISTINCT b.affi_shopid,
b.affi_username affi_username,
a.shopid mst_shopid,
a.username mst_username
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b ON b.mst_shopid = a.shopid
WHERE
a.cb_option = 0
AND b.grass_region <> ''
)
SELECT
DISTINCT a.mst_shopid,
a.mst_itemid,
a.affi_shopid,
a.affi_itemid,
c.item_status mst_item_status
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN sip ON a.affi_shopid = sip.affi_shopid
INNER JOIN (
SELECT
item_id itemid,
status item_status
FROM
mp_item.dim_item__reg_s0_live
WHERE
tz_type = 'local'
AND grass_date = (current_date - INTERVAL '1' DAY)
AND is_cb_shop = 1
AND shop_status = 1
AND seller_status = 1
AND status = 1
AND is_holiday_mode = 0
) b ON a.affi_itemid = b.itemid
INNER JOIN (
SELECT
item_id itemid,
status item_status
FROM
mp_item.dim_item__reg_s0_live
WHERE
tz_type = 'local'
AND grass_date = (current_date - INTERVAL '1' DAY)
AND is_cb_shop = 0
AND status <> 1
AND shop_status = 1
AND seller_status = 1
AND is_holiday_mode = 0
) c ON a.mst_itemid = c.itemid