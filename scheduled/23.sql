insert into nexus.test_b15671939e204b15906e57b93b00fb723fde0b81db5876d766a8c071a2e5b0a2_5568135ca908f9af6c7ab186479c4e5b with icb_shops as (
select *
from regcbbi_others.shopee_regional_bi_team__krcb_icb_shop 
where ingestion_timestamp in (
select max(ingestion_timestamp) as max_time 
from regcbbi_others.shopee_regional_bi_team__krcb_icb_shop 
)
)


, kr_seller as (
select distinct s.gp_name, s.gp_account_owner, user_name as shop_name, shop_id as shopid, grass_region
from dev_regcbbi_kr.krcb_shop_profile s
where s.grass_date in (
select max(grass_date) as latest_ingest_date
from dev_regcbbi_kr.krcb_shop_profile
where grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
)
and grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
)


, seller as (
select c.grass_region, c.shopid, c.shop_name, c.gp_name
from icb_shops icb 
join 
kr_seller c 
on 
icb.gp_name = c.gp_name
left join (
select
distinct
shopid
from
regcbbi_others.shopee_regional_bi_team__krcb_excluding_shops
where
ingestion_timestamp in (
select
max(ingestion_timestamp) as max_date
from
regcbbi_others.shopee_regional_bi_team__krcb_excluding_shops
)
and seller_request_unfreeze = 'N'
) e 
on c.shopid = cast(e.shopid as bigint) 


where 
e.shopid is null
), 


sgmyid_ads as (
select shop_id as shopid
, sum(order_cnt) as ads_orders 
, sum(ads_gmv_usd) as ads_gmv_usd
, sum(ads_expenditure_amt_usd) as ads_expenditure_usd
, sum(impression_cnt) as impression
, sum(click_cnt) as clicks
, count(distinct case when grass_date = current_date - interval '1' day and is_ads_active = 1 then ads_id else null end) as active_ads
from mp_paidads.ads_advertise_mkt_1d__reg_s0_live ads 
where grass_region in ('SG', 'MY', 'ID')
and tz_type = 'local'
and shop_id in (select distinct shopid from seller)
group by 1
), 


phthtwvn_ads as (
select shop_id as shopid
, sum(order_cnt) as ads_orders 
, sum(ads_gmv_usd) as ads_gmv_usd
, sum(ads_expenditure_amt_usd) as ads_expenditure_usd
, sum(impression_cnt) as impression
, sum(click_cnt) as clicks
, count(distinct case when grass_date = current_date - interval '1' day and is_ads_active = 1 then ads_id else null end) as active_ads
from mp_paidads.ads_advertise_mkt_1d__reg_s0_live ads 
where grass_region in ('PH', 'TH', 'TW')
and tz_type = 'local'
and shop_id in (select distinct shopid from seller)
group by 1
),


brmxpl_ads as (
select shop_id as shopid
, sum(order_cnt) as ads_orders 
, sum(ads_gmv_usd) as ads_gmv_usd
, sum(ads_expenditure_amt_usd) as ads_expenditure_usd
, sum(impression_cnt) as impression
, sum(click_cnt) as clicks
, count(distinct case when grass_date = current_date - interval '1' day and is_ads_active = 1 then ads_id else null end) as active_ads
from mp_paidads.ads_advertise_mkt_1d__reg_s0_live ads 
where grass_region in ('BR', 'MX', 'PL')
and tz_type = 'local'
and shop_id in (select distinct shopid from seller)
group by 1
),


ads as (
select * from sgmyid_ads 
union all 
select * from phthtwvn_ads 
union all 
select * from brmxpl_ads 
),


orders as ( 
select s.shopid
, gmv_usd_td as total_gmv 
from seller s
join mp_order.dws_seller_gmv_td__reg_s0_live o on o.shop_id = s.shopid
where o.grass_region in ('SG', 'MY', 'ID', 'PH', 'TH', 'TW', 'VN', 'BR', 'MX', 'PL')
and o.tz_type = 'local'
and grass_date = current_date - interval '1' day
)


select grass_region 
, gp_name 
, shopid
, shop_name
, impression 
, clicks 
, ads_orders 
, ads_gmv_usd 
, ads_expenditure_usd 
, active_ads
, total_gmv 
from seller s 
left join ads using (shopid)
left join orders using (shopid)
where ads_expenditure_usd > 0