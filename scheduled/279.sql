insert into nexus.test_8076b10b3eb5e56375518967ae568dde6ec342970d5bd9935c8df55f1e0cc0fe_6b1766b251bb6fecc0820732e8b5222e WITH
sip AS (
SELECT DISTINCT
affi_shopid
, mst_shopid
, t1.country mst_country
, t2.country affi_country
, t1.cb_option
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
INNER JOIN (
SELECT DISTINCT
affi_shopid
, affi_username
, mst_shopid
, country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE ((sip_shop_status <> 3) AND (grass_region IN ('SG','MY','ID','PH','TH','VN','TW')))
) t2 ON (t1.shopid = t2.mst_shopid))
WHERE ((T1.cb_option = 1) AND (T1.COUNTRY IN ('MY','TW','BR')))
) 


, banned AS (
SELECT DISTINCT
i.itemid item_id 
, shopid shop_id
, country
, cast(grass_date as date) action_time 
, json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description') ban_reason 
FROM
marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live i 
join (select itemid item_id
,max(ctime) max_ctime
from marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
WHERE (((( cast(grass_date as date) >= (current_date - INTERVAL '30' DAY))
AND (DATE(FROM_UNIXTIME(CTIME )) >= (current_date - INTERVAL '30' DAY)) AND (DATE(FROM_UNIXTIME(MTIME )) >= (current_date - INTERVAL '30' DAY))
AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].Name')) = 'status')) 
AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].New')) IN ('3','4'))) 
AND (country IN ('SG','MY','ID','PH','TH','VN','TW'))) 
and try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description')) is not null 
group by 1 ) i2 on i2.item_id = i.itemid and i2.max_ctime=i.ctime 
-- join top on top.item_id = i.item_id 
WHERE ((((cast(grass_date as date) >= (current_date - INTERVAL '30' DAY))
AND (DATE(FROM_UNIXTIME(CTIME )) >= (current_date - INTERVAL '30' DAY)) 
AND (DATE(FROM_UNIXTIME(MTIME )) >= (current_date - INTERVAL '30' DAY))
AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].Name')) = 'status')) 
AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].New')) IN ('3','4'))) 
AND (country IN ('SG','MY','ID','PH','TH','VN','TW'))) 
and try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description')) is not null 
) 
, sku as (
SELECT 
affi_itemid
, MST_ITEMID
, affi_country
, unlist_factor
, affi_shopid
FROM
marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE (affi_country IN ('SG','MY','ID','PH','TH','VN','TW'))



)


, UNLIST_REASON AS (
SELECT DISTINCT
a.affi_itemid
, unlist_factor
, (CASE WHEN ("bitwise_and"(unlist_factor, 1) > 0) THEN 'MstItemUnlist' WHEN ("bitwise_and"(unlist_factor, 2) > 0) THEN 'RecomCatScoreTooLow' WHEN ("bitwise_and"(unlist_factor, 4) > 0) THEN 'MstPriceTooLow' WHEN ("bitwise_and"(unlist_factor, 8) > 0) THEN 'VariationNameTooLong' 
WHEN ("bitwise_and"(unlist_factor, 16) > 0) THEN 'VariationOptionTooLong' WHEN ("bitwise_and"(unlist_factor, 32) > 0) THEN 'AffiPriceOutOfRange' WHEN ("bitwise_and"(unlist_factor, 64) > 0) THEN 'AitemQcBlackListForbidden' 
WHEN ("bitwise_and"(unlist_factor, 128) > 0) THEN 'SyncItemFail' WHEN ("bitwise_and"(unlist_factor, 256) > 0) THEN 'SyncItemFail' WHEN ("bitwise_and"(unlist_factor, 512) > 0) THEN 'NoHitChannelWhiteList' WHEN ("bitwise_and"(unlist_factor, 1024) > 0) THEN 'PitemQcBlackListForbidden' WHEN ("bitwise_and"(unlist_factor, 2048) > 0) THEN 'PitemForbiddenList' WHEN ("bitwise_and"(unlist_factor, 4096) > 0) THEN 'DuplicateItem' WHEN ("bitwise_and"(unlist_factor, 16384) > 0) THEN 'AffiitemForbiddenList' WHEN ("bitwise_and"(unlist_factor, 32768) > 0) THEN 'Mst item under QC' WHEN ("bitwise_and"(unlist_factor, 65536) > 0) THEN 'Invalid DTS setting' WHEN (("bitwise_and"(unlist_factor, 131072) > 0) OR ((unlist_factor = 0) AND (d.unlist_region IS NOT NULL))) THEN 'Ops require to unlist' WHEN ("bitwise_and"(unlist_factor, 262144) > 0) THEN 'Not used code' WHEN ("bitwise_and"(unlist_factor, 524288) > 0) THEN 'Shop not sale' WHEN ("bitwise_and"(unlist_factor, 1048576) > 0) THEN 'PFF product' WHEN ("bitwise_and"(unlist_factor, 2097152) > 0) THEN 'Obtain category fail' WHEN ("bitwise_and"(unlist_factor, 4194304) > 0) THEN 'Forbidden attribute language' WHEN ("bitwise_and"(unlist_factor, 8388608) > 0) THEN 'Return affi shop' WHEN ("bitwise_and"(unlist_factor, 16777216) > 0) THEN 'Fail to build global tree' WHEN ("bitwise_and"(unlist_factor, 33554432) > 0) THEN 'Hit Price restriction' WHEN ("bitwise_and"(unlist_factor, 67108864) > 0) THEN 'Unlist before Unlink mtsku' WHEN ("bitwise_and"(unlist_factor, 134217728) > 0) THEN 'Violate TW NCC' WHEN ("bitwise_and"(unlist_factor, 268435456) > 0) THEN 'Title Language Not Met' WHEN ("bitwise_and"(unlist_factor, 536870912) > 0) THEN 'Description Language Not Met' WHEN ("bitwise_and"(unlist_factor, 1073741824) > 0) THEN 'Variation Language Not Met' WHEN ("bitwise_and"(unlist_factor, 2147483648) > 0) THEN 'Not Qualified SLS Channel' ELSE 'Others' END) unlist_reason
FROM
(sku a
-- join top on top.item_id = a.affi_itemid 
LEFT JOIN (
SELECT DISTINCT
mst_itemid
, unlist_region
FROM
marketplace.shopee_sip_db__item_unlist_config_tab__reg_daily_s0_live
WHERE unlist_region IN ('SG','MY','ID','PH','TH','VN','TW')
) d ON ((a.mst_itemid = d.mst_itemid) AND (a.affi_country = d.unlist_region)))
) 


SELECT distinct 
sip.mst_country 
, sip.affi_country 
, i.level1_global_be_category
, sip.affi_shopid 
, i.item_id affi_itemid
, oo.l30d_ado
, cfs_orders l39d_cfs_ado 
, oo.l30d_adgmv_usd
, i.status 
, STOCK 
, i.price 
,'' image_link
,'' image 


, case when i.status =8 then unlist_reason
when i.status in (3,4) then ban_reason 
else null end as abnormal_reason 
, backend_link
FROM (
SELECT DISTINCT
ITEM_ID
, price
, SHOP_ID
, level1_global_be_category
, stock
, status
, grass_region 

--, concat('http://f.shopee.vn/file/',split(images,',')[1]) as image_link
, CASE WHEN grass_region = 'SG' then concat('https://admin.shopee.sg/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'TW' then concat('https://admin.shopee.tw/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'MY' then concat('https://admin.shopee.com.my/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'BR' then concat('https://admin.shopee.com.br/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'TH' then concat('https://admin.shopee.co.th/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'ID' then concat('https://admin.shopee.co.id/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'VN' then concat('https://admin.shopee.vn/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'PH' then concat('https://admin.shopee.ph/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'MX' then concat('https://admin.shopee.com.mx/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR)) 
WHEN grass_region = 'CO' then concat('https://admin.shopee.com.CO/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'CL' then concat('https://admin.shopee.com.CL/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'PL' then concat('https://admin.shopee.com.PL/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR))
WHEN grass_region = 'ES' then concat('https://admin.shopee.com.ES/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR)) 
WHEN grass_region = 'FR' then concat('https://admin.shopee.com.FR/product/listing/detail/', CAST(shop_id AS VARCHAR), '/', CAST(item_id AS VARCHAR)) 
ELSE '' END as backend_link
FROM
mp_item.dim_item__reg_s0_live
WHERE ((((((grass_date = (current_date - INTERVAL '1' DAY)) AND (is_cb_shop = 1)) AND (grass_region IN ('SG','MY','ID','PH','TH','VN','TW'))) 
AND (seller_status = 1)) AND (shop_status = 1)) AND (is_holiday_mode = 0)) and (status !=1 or stock = 0 )
) I 
-- join sku m on m.affi_itemid=i.item_id 
-- -- INNER JOIN TOP ON (TOP.ITEM_ID = I.ITEM_ID)
-- INNER JOIN (
-- SELECT DISTINCT item_id
-- , grass_region 
-- FROM
-- shopee.item_mart_dim_item
-- WHERE ((((((((grass_date = (current_date - INTERVAL '1' DAY)) AND (status = 1)) AND (stock > 0)) AND (seller_status = 1)) AND (shop_status = 1)) 
-- AND (is_holiday_mode = 0)) AND (is_cb_shop = 1)) AND (grass_region IN ('TW', 'MY','BR')))
-- ) ii ON (ii.item_id = m.mst_itemid)


join (select item_id 
, sum(order_fraction )/30.00 l30d_ado 
, sum(gmv_usd)/30.00 l30d_adgmv_usd
, sum( case when flash_sale_type_id<2 and is_flash_sale=1 then order_fraction else null end)/30.00 cfs_orders
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where date(split(create_datetime,' ')[1]) between current_Date-interval'30'day and current_date-interval'1'day and grass_date>= current_Date-interval'30'day
and grass_region in ('SG','MY','ID','PH','TH','VN','TW') and is_cb_shop=1 
group by 1 
) oo on oo.item_id = i.item_id 
join sip on sip.affi_shopid= i.shop_id 
LEFT JOIN UNLIST_REASON ur ON (ur.affi_itemid = i.item_id )
LEFT JOIN banned b ON (b.item_id = i.item_id)


order by 2, 1 , oo.l30d_ado desc