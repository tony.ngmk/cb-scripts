insert into nexus.test_26a46e8760b67c52a73b3b79458eaa628233d366fdb8d19babfe3101a2188014_9a32615c584707830eba545163c2f132 WITH today as (
select
current_date as today
),
raw_date as (
select
today,
date_add('day', -1, today) as yesterday,
date_trunc('week', today) as wtd_from,
date_add('day', -1, today) as wtd_to,
date_add('day', -7, date_trunc('week', today)) as w_1_from,
date_add('day', -1, date_trunc('week', today)) as w_1_to,
date_add('day', -14, date_trunc('week', today)) as w_2_from,
date_add('day', -8, date_trunc('week', today)) as w_2_to,
date_add('day', -21, date_trunc('week', today)) as w_3_from,
date_add('day', -15, date_trunc('week', today)) as w_3_to,
date_add('day', -28, date_trunc('week', today)) as w_4_from,
date_add('day', -22, date_trunc('week', today)) as w_4_to,
date_trunc('month', today) as mtd_from,
date_add('day', -1, today) as mtd_to,
date_add('month', -1, date_trunc('month', today)) as m_1_from,
date_add('day', -1, date_trunc('month', today)) as m_1_to,
date_add('month', -2, date_trunc('month', today)) as m_2_from,
date_add('day', -1, date_add('month', -1, date_trunc('month', today))) as m_2_to,
date_add('month', -3, date_trunc('month', today)) as m_3_from,
date_add('day', -1, date_add('month', -2, date_trunc('month', today))) as m_3_to,
date_add('month', -4, date_trunc('month', today)) as m_4_from,
date_add('day', -1, date_add('month', -3, date_trunc('month', today))) as m_4_to,
cast(day_of_week(date_add('day', -1, today)) as double) as wtd_days,
cast(7 as double) as w_1_days,
cast(7 as double) as w_2_days,
cast(7 as double) as w_3_days,
cast(7 as double) as w_4_days,
cast(day_of_month(date_add('day', -1, today)) as double) as mtd_days,
cast(date_diff('day', date_add('month', -1, date_trunc('month', today)), date_trunc('month', today)) as double) as m_1_days,
cast(date_diff('day', date_add('month', -2, date_trunc('month', today)), date_add('month', -1, date_trunc('month', today))) as double) as m_2_days,
cast(date_diff('day', date_add('month', -3, date_trunc('month', today)), date_add('month', -2, date_trunc('month', today))) as double) as m_3_days,
cast(date_diff('day', date_add('month', -4, date_trunc('month', today)), date_add('month', -3, date_trunc('month', today))) as double) as m_4_days


from
today
),
date_info as (
select
today,
yesterday,
case when wtd_from = today then w_1_from else wtd_from end as wtd_from,
case when wtd_from = today then w_1_to else wtd_to end as wtd_to,
case when wtd_from = today then w_2_from else w_1_from end as w_1_from,
case when wtd_from = today then w_2_to else w_1_to end as w_1_to,
case when day(today) = 1 then m_1_from else mtd_from end as mtd_from,
case when day(today) = 1 then m_1_to else mtd_to end as mtd_to,
case when day(today) = 1 then m_2_from else m_1_from end as m_1_from,
case when day(today) = 1 then m_2_to else m_1_to end as m_1_to,
case when wtd_from = today then w_1_days else wtd_days end as wtd_days,
case when wtd_from = today then w_2_days else w_1_days end as w_1_days,
case when day(today) = 1 then m_1_days else mtd_days end as mtd_days,
case when day(today) = 1 then m_2_days else m_1_days end as m_1_days
from
raw_date
),
seller as (
select 
shop_id as shopid,
user_id as userid,
shopee_account_created_date,
gp_create_date as gp_account_shopee_account_created_date,
grass_region,
user_name as shop_name,
gp_name,
gp_account_owner,
child_account_owner,
case 
when gp_smt not in (
'ICB',
'MSM',
'Tier 1',
'Tier 2',
'Tier 3'
) then 'Others'
else coalesce(gp_smt, 'Others')
end as gp_smt
from
dev_regcbbi_kr.jpcb_shop_profile
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.jpcb_shop_profile
where
grass_region <> ''
) 
),
ads_metric as (
select
s.grass_region,
s.gp_smt,


-- p.shop_id,
-- s.shop_name,


count(distinct case when p.impression_cnt > 0 and p.grass_date between d.wtd_from and d.wtd_to then p.shop_id else null end) as wtd_num_shops_impression,
count(distinct case when p.impression_cnt > 0 and p.grass_date between d.w_1_from and d.w_1_to then p.shop_id else null end) as w_1_num_shops_impression,
count(distinct case when p.impression_cnt > 0 and p.grass_date between d.mtd_from and d.mtd_to then p.shop_id else null end) as mtd_num_shops_impression,
count(distinct case when p.impression_cnt > 0 and p.grass_date between d.m_1_from and d.m_1_to then p.shop_id else null end) as m_1_num_shops_impression,


sum(case when p.grass_date between d.wtd_from and d.wtd_to then p.expenditure_amt_usd else 0 end) as wtd_expenditure_amt_usd,
sum(case when p.grass_date between d.w_1_from and d.w_1_to then p.expenditure_amt_usd else 0 end) as w_1_expenditure_amt_usd,
sum(case when p.grass_date between d.mtd_from and d.mtd_to then p.expenditure_amt_usd else 0 end) as mtd_expenditure_amt_usd,
sum(case when p.grass_date between d.m_1_from and d.m_1_to then p.expenditure_amt_usd else 0 end) as m_1_expenditure_amt_usd,


sum(case when p.grass_date between d.wtd_from and d.wtd_to then p.ads_gmv_amt_usd else 0 end) as wtd_direct_ads_gmv_usd,
sum(case when p.grass_date between d.w_1_from and d.w_1_to then p.ads_gmv_amt_usd else 0 end) as w_1_direct_ads_gmv_usd,
sum(case when p.grass_date between d.mtd_from and d.mtd_to then p.ads_gmv_amt_usd else 0 end) as mtd_direct_ads_gmv_usd,
sum(case when p.grass_date between d.m_1_from and d.m_1_to then p.ads_gmv_amt_usd else 0 end) as m_1_direct_ads_gmv_usd,


sum(case when p.grass_date between d.mtd_from and d.mtd_to then p.order_cnt else 0 end) as mtd_ads_orders,
sum(case when p.grass_date between d.m_1_from and d.m_1_to then p.order_cnt else 0 end) as m_1_ads_orders


from
mp_paidads.dws_advertise_query_gmv_event_1d__reg_s0_live as p
join
seller as s
on
p.shop_id = s.shopid
join
date_info as d
on
p.grass_date between d.m_1_from and d.yesterday
where
p.tz_type = 'local'
group by
1,2
),
order_metric as (
select
s.grass_region,
s.gp_smt,
-- p.shop_id,
-- s.shop_name,
sum(case when p.grass_date between d.wtd_from and d.wtd_to then p.gmv_usd_1d else 0 end) as wtd_gmv_usd,
sum(case when p.grass_date between d.w_1_from and d.w_1_to then p.gmv_usd_1d else 0 end) as w_1_gmv_usd,
sum(case when p.grass_date between d.mtd_from and d.mtd_to then p.gmv_usd_1d else 0 end) as mtd_gmv_usd,
sum(case when p.grass_date between d.m_1_from and d.m_1_to then p.gmv_usd_1d else 0 end) as m_1_gmv_usd,


sum(case when p.grass_date between d.mtd_from and d.mtd_to then p.placed_order_cnt_1d else 0 end) as mtd_gross_orders,
sum(case when p.grass_date between d.m_1_from and d.m_1_to then p.placed_order_cnt_1d else 0 end) as m_1_gross_orders
from
mp_order.dws_seller_gmv_1d__reg_s0_live as p
join
seller as s
on
p.shop_id = s.shopid
join
date_info as d
on
p.grass_date between d.m_1_from and d.yesterday
where
p.tz_type = 'local'
group by
1,2
),
normal_shop as (
select
s.gp_smt,
s.grass_region,
u.grass_date,
u.shop_id
from
mp_user.dim_shop__reg_s0_live as u
join 
seller as s
on
u.shop_id = s.shopid
and u.tz_type = 'local'
and u.status = 1
and (u.is_holiday_mode IS NULL OR u.is_holiday_mode = 0)
join
date_info as d
on
u.grass_date between d.m_1_from and d.yesterday
where
u.grass_date >= date('2022-01-01')
group by
1,2,3,4
),
active_item_shop as (
select
s.gp_smt,
s.grass_region,
t.grass_date,
t.shop_id
from
mp_item.dws_shop_listing_td__reg_s0_live as t
join
seller as s
on
t.shop_id = s.shopid
and t.tz_type = 'local'
and t.active_item_with_stock_cnt >= 5
join
date_info as d
on
t.grass_date between d.m_1_from and d.yesterday
where
t.grass_date >= date('2022-01-01')
group by
1,2,3,4
),
normal_account_shops as (
select
s.gp_smt,
s.grass_region,
t.grass_date,
t.shop_id
from
mp_user.dim_user__reg_s0_live as t
join
seller as s
on
t.shop_id = s.shopid
and t.tz_type = 'local'
and t.status = 1
join
date_info as d
on
t.grass_date between d.m_1_from and d.yesterday
where
t.grass_date >= date('2022-01-01')
group by
1,2,3,4
),
login_shops as (
SELECT 
s.gp_smt,
s.grass_region,
s.shopid as shop_id, 
l.grass_date
FROM 
mp_user.ads_activeness_user_activity_td__reg_s0_live as l
join
seller as s
on
l.user_id = s.userid
and l.tz_type = 'local'
and date(split(l.last_login_datetime_td,' ')[1]) >= date_add('day',-7,l.grass_date)
join
date_info as d
on
l.grass_date between d.m_1_from and d.yesterday
where
l.grass_date >= date('2022-01-01')
group by
1,2,3,4
),
num_shops as (
SELECT
a.gp_smt,
a.grass_region,
count(distinct case when a.grass_date between d.wtd_from and d.wtd_to then a.shop_id else null end) as wtd_num_shops,
count(distinct case when a.grass_date between d.w_1_from and d.w_1_to then a.shop_id else null end) as w_1_num_shops,
count(distinct case when a.grass_date between d.mtd_from and d.mtd_to then a.shop_id else null end) as mtd_num_shops,
count(distinct case when a.grass_date between d.m_1_from and d.m_1_to then a.shop_id else null end) as m_1_num_shops
FROM 
normal_shop as a
JOIN 
login_shops AS b
ON
a.shop_id = b.shop_id
AND a.grass_date = b.grass_date
JOIN 
active_item_shop AS c
ON
a.shop_id = c.shop_id
AND a.grass_date = c.grass_date
join
normal_account_shops as n
on
a.shop_id = n.shop_id
and a.grass_date = n.grass_date
join
date_info as d
on
a.grass_date between d.m_1_from and d.yesterday
group by
1,2
)
select
s.grass_region,
s.gp_smt,
-- s.gp_name,
-- s.shopid,
-- s.shop_name,


coalesce(o.wtd_gmv_usd,0) as wtd_gmv_usd,
coalesce(o.w_1_gmv_usd,0) as w_1_gmv_usd,
coalesce(o.mtd_gmv_usd,0) as mtd_gmv_usd,
coalesce(o.m_1_gmv_usd,0) as m_1_gmv_usd,
coalesce(n.wtd_num_shops,0) as wtd_num_shops, 
coalesce(n.w_1_num_shops,0) as w_1_num_shops, 
coalesce(n.mtd_num_shops,0) as mtd_num_shops, 
coalesce(n.m_1_num_shops,0) as m_1_num_shops, 
coalesce(a.wtd_num_shops_impression,0) as wtd_num_shops_impression,
coalesce(a.w_1_num_shops_impression,0) as w_1_num_shops_impression,
coalesce(a.mtd_num_shops_impression,0) as mtd_num_shops_impression,
coalesce(a.m_1_num_shops_impression,0) as m_1_num_shops_impression,
coalesce(a.wtd_expenditure_amt_usd,0) as wtd_expenditure_amt_usd,
coalesce(a.w_1_expenditure_amt_usd,0) as w_1_expenditure_amt_usd,
coalesce(a.mtd_expenditure_amt_usd,0) as mtd_expenditure_amt_usd,
coalesce(a.m_1_expenditure_amt_usd,0) as m_1_expenditure_amt_usd,
coalesce(a.wtd_direct_ads_gmv_usd,0) as wtd_direct_ads_gmv_usd,
coalesce(a.w_1_direct_ads_gmv_usd,0) as w_1_direct_ads_gmv_usd,
coalesce(a.mtd_direct_ads_gmv_usd,0) as mtd_direct_ads_gmv_usd,
coalesce(a.m_1_direct_ads_gmv_usd,0) as m_1_direct_ads_gmv_usd,


coalesce(a.mtd_ads_orders,0) as mtd_ads_orders,
coalesce(o.mtd_gross_orders,0) as mtd_gross_orders,


d.wtd_days,
d.w_1_days,
d.mtd_days,
d.m_1_days,


coalesce(a.m_1_ads_orders,0) as m_1_ads_orders,
coalesce(o.m_1_gross_orders,0) as m_1_gross_orders


from
seller as s
left join
order_metric as o
on
s.gp_smt = o.gp_smt
and s.grass_region = o.grass_region
-- and s.shopid = o.shop_id
left join
ads_metric as a
on
s.gp_smt = a.gp_smt
and s.grass_region = a.grass_region
-- and s.shopid = a.shop_id
left join
num_shops as n
on
s.gp_smt = n.gp_smt
and s.grass_region = n.grass_region
-- and s.shopid = n.shop_id
cross join
date_info as d
group by
1,2,3,4,5,6,7,8,9,10,
11,12,13,14,15,16,17,18,19,
20,21,22,23,24,25,26,27,28,29,30
order by
2,1