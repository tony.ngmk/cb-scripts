WITH sip
AS (
SELECT DISTINCT affi_shopid,
mst_shopid,
t1.country mst_country,
t2.country affi_country
FROM (
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1 INNER JOIN (
SELECT DISTINCT affi_shopid,
affi_username,
mst_shopid,
country
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE grass_region in ('SG', 'MY', 'PH', 'TH', 'ID', 'VN', 'TW', 'MX', 'BR', 'CO', 'CL', 'PL', 'ES', 'FR')
AND sip_shop_status != 3
) t2 ON (t1.shopid = t2.mst_shopid)
)
WHERE (cb_option = 1)
),
t2
AS (
SELECT DISTINCT t2.main_account_id,
shop_id,
STATUS,
"from_unixtime" (ctime) ctime,
"from_unixtime" (mtime) mtime,
account_type,
agent_acc_username,
agent_acc_password
FROM (
(
(
marketplace.shopee_subaccount_db__account_shop_tab__reg_daily_s0_live t2 INNER JOIN (
SELECT DISTINCT main_account_id,
account_type
FROM marketplace.shopee_subaccount_db__account_tab__reg_daily_s0_live
WHERE (account_type = 1003)
) t3 ON (t2.main_account_id = t3.main_account_id)
) INNER JOIN (
SELECT DISTINCT main_account_id,
team_id
FROM marketplace.shopee_subaccount_db__account_team_tab__reg_daily_s0_live
WHERE (team_id = 1000)
) team ON (team.main_account_id = t2.main_account_id)
) INNER JOIN (
SELECT DISTINCT CAST(main_acc_id AS BIGINT) main_acc_id,
agent_acc_username,
agent_acc_password
FROM regcbbi_others.shopee_regional_cb_team__cs_mapping
) ag ON (ag.main_acc_id = t2.main_account_id)
)
WHERE (STATUS = 1)
-- chng: added grass_region partition as required by Data Nexus
AND t2.grass_region IN ('SG', 'MY', 'PH', 'TH', 'ID', 'VN', 'TW', 'MX', 'BR', 'CO', 'CL', 'PL', 'ES', 'FR')
),
t1
AS (
SELECT DISTINCT affi_shopid,
t4.username affi_username,
country affi_country,
COALESCE(t2.main_account_id, 0) main_account_id,
account_type,
(
CASE 
WHEN (CAST(t2.STATUS AS VARCHAR) = '1')
THEN 'Binded'
WHEN (CAST(t2.STATUS AS VARCHAR) = '-1')
THEN 'Not Binded'
END
) shop_bind_status,
t2.ctime,
t2.mtime,
t4.STATUS shop_status,
t4.holiday_mode_on,
(
CASE 
WHEN (t2.main_account_id IS NOT NULL)
THEN agent_acc_username
ELSE t4.username
END
) agent_acc_username,
(
CASE 
WHEN (t2.main_account_id IS NOT NULL)
THEN agent_acc_password
ELSE 'Scb88@Sip99'
END
) agent_acc_password,
order_status,
enquiry_type,
enquiry_details,
reason
FROM (
(
(
(
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live t1 
INNER JOIN (
SELECT shopid,
cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE (cb_option = 1)
) a ON (t1.mst_shopid = a.shopid)
) INNER JOIN (
SELECT DISTINCT shop_id shopid,
user_status STATUS,
is_holiday_mode holiday_mode_on,
user_id userid,
shop_level1_category main_category,
grass_region,
user_name username
FROM regcbbi_others.shop_profile
WHERE (
(is_cb_shop = 1)
AND (grass_date = (CURRENT_DATE - INTERVAL '1' DAY))
)
) t4 ON (t4.shopid = t1.affi_shopid)
) LEFT JOIN t2 ON (t2.shop_id = t1.affi_shopid)
) LEFT JOIN (
SELECT *
FROM regcbbi_others.shopee_regional_cb_team__cs_chat_mapping
WHERE (
(
ingestion_timestamp = (
SELECT "max" (ingestion_timestamp) col_1
FROM regcbbi_others.shopee_regional_cb_team__cs_chat_mapping
)
)
AND (initiative = 'Review Rewards')
)
) cm ON (cm.grass_region = t1.country)
)
WHERE t1.grass_region in ('SG', 'MY', 'PH', 'TH', 'ID', 'VN', 'TW', 'MX', 'BR', 'CO', 'CL', 'PL', 'ES', 'FR')
),




a as (
select distinct affi_shopid 
from regcbbi_others.sip_ops_performance
where try(NFR_included_rr_30d * decimal'1.000'/(NFR_included_rr_30d
+NFR_included_cancellations_30d+net_orders_30d))<0.1)


,
review
AS (
SELECT DISTINCT a.*
FROM (
SELECT DISTINCT shopid,
itemid,
"count" (DISTINCT cmtid) reviews
FROM marketplace.shopee_item_comment_db__item_cmt_v2_tab__reg_daily_s0_live
WHERE (orderid <> 0)
GROUP BY 1,
2
) a
WHERE (reviews > 2)
)






SELECT DISTINCT shopid affi_shopid,
orderid,
CURRENT_DATE update_time,
order_sn,
tt.affi_country,
tt.mst_country,
agent_acc_username
FROM ( SELECT DISTINCT o1.orderid,
o1.ctime,
oo.order_sn,
grass_region,
o1.shopid,
buyer_id,
buyer_name,
seller_name,
oo.itemid,
affi_country,
mst_country,
(
CASE 
WHEN (opuserid = - 1)
THEN 'SYSTEM'
ELSE 'BUYER CLICK'
END
) CLICK_TYPE
FROM ( SELECT DISTINCT orderid,
shopid,
old_status,
new_status,
opuserid,
grass_region,
(
CASE 
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW'))
THEN "from_unixtime" (ctime)
WHEN (grass_region IN ('TH', 'ID', 'VN'))
THEN ("from_unixtime" (ctime) - INTERVAL '1' HOUR)
WHEN (grass_region = 'BR')
THEN ("from_unixtime" (ctime) - INTERVAL '11' HOUR)
WHEN (grass_region = 'CL')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Santiago', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CO')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Bogota', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'MX')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Mexico_City', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'ES') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN (grass_region = 'FR') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN (grass_region = 'PL') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
ELSE NULL
END
) ctime
FROM marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_daily_s0_live
WHERE (
(new_status = 4)
AND (
"date" (
CASE 
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW'))
THEN "from_unixtime" (ctime)
WHEN (grass_region IN ('TH', 'ID', 'VN'))
THEN ("from_unixtime" (ctime) - INTERVAL '1' HOUR)
WHEN (grass_region = 'BR')
THEN ("from_unixtime" (ctime) - INTERVAL '11' HOUR)
WHEN (grass_region = 'CL')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Santiago', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CO')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Bogota', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'MX')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Mexico_City', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'ES') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN (grass_region = 'FR') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN (grass_region = 'PL') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
ELSE NULL
END
) = (CURRENT_DATE - INTERVAL '1' DAY)
)
)
AND (opuserid = - 1)

UNION

SELECT DISTINCT orderid,
shopid,
old_status,
new_status,
opuserid,
grass_region,
(
CASE 
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW'))
THEN "from_unixtime" (ctime)
WHEN (grass_region IN ('TH', 'ID', 'VN'))
THEN ("from_unixtime" (ctime) - INTERVAL '1' HOUR)
WHEN (grass_region = 'BR')
THEN ("from_unixtime" (ctime) - INTERVAL '11' HOUR)
WHEN (grass_region = 'CL')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Santiago', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CO')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Bogota', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'MX')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Mexico_City', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'ES') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN (grass_region = 'FR') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN (grass_region = 'PL') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
ELSE NULL
END
) ctime
FROM marketplace.shopee_order_audit_v3_db__order_audit_tab__reg_daily_s0_live
WHERE (
(
(new_status = 4)
AND (
"date" (
CASE 
WHEN (grass_region IN ('SG', 'MY', 'PH', 'TW'))
THEN "from_unixtime" (ctime)
WHEN (grass_region IN ('TH', 'ID', 'VN'))
THEN ("from_unixtime" (ctime) - INTERVAL '1' HOUR)
WHEN (grass_region = 'BR')
THEN ("from_unixtime" (ctime) - INTERVAL '11' HOUR)
WHEN (grass_region = 'CL')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Santiago', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'CO')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Bogota', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'MX')
THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'America/Mexico_City', 'yyyy-MM-dd HH:mm:ss') AS TIMESTAMP)
WHEN (grass_region = 'ES') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Madrid','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN (grass_region = 'FR') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Paris','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
WHEN (grass_region = 'PL') THEN CAST(format_datetime(from_unixtime(ctime) AT TIME ZONE 'Europe/Warsaw','yyyy-MM-dd HH:mm:ss') AS TIMESTAMP) 
ELSE NULL
END
) = (CURRENT_DATE - INTERVAL '3' DAY)
)
)
AND (opuserid <> - 1)
)
) o1
INNER JOIN sip ON (sip.affi_shopid = o1.shopid)
join a on a.affi_shopid = sip.affi_shopid 
INNER JOIN (
SELECT DISTINCT order_id,
order_sn,
buyer_id,
seller_id,
buyer_name,
seller_name,
item_id itemid
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE (is_cb_shop = 1)
AND grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) oo ON (oo.order_id = o1.orderid)
LEFT JOIN review re ON (re.itemid = oo.itemid)
LEFT JOIN (
SELECT DISTINCT orderid,
extinfo,
rating_star,
comment,
"from_unixtime" (ctime) r_time
FROM marketplace.shopee_item_comment_db__item_cmt_v2_tab__reg_daily_s0_live
WHERE (
"date" ("from_unixtime" (ctime)) BETWEEN (CURRENT_DATE - INTERVAL '30' DAY)
AND (CURRENT_DATE - INTERVAL '1' DAY)
)
) rr ON (rr.orderid = o1.orderid)

WHERE (
(re.itemid IS NULL)
AND (rr.orderid IS NULL)
)
GROUP BY 1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12
-- ORDER BY 15 DESC
) tt 
LEFT JOIN t1 ON (t1.affi_shopid = tt.shopid)


LIMIT 10000