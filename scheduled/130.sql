-- tag not in ra db




WITH 
-- raw data from our own database
cbwh_shop as (
select distinct
a.grass_region,
a.shop_id, 
a.fbs_tag, 
a.pff_tag, 
l.shop_level1_category,
shop_level1_global_be_category
,is_official_shop
,is_preferred_shop
,is_fss_shop
,is_ccb_shop
,is_cb_shop
,is_fbs
from regcbbi_general.cbwh_shop_history_v2 a
JOIN (
-- limit data size before join
SELECT
shop_id 
, status as shop_status 
, is_official_shop
,is_preferred_shop
,is_fss_shop
,is_ccb_shop
,is_cb_shop
,is_fbs
FROM
mp_user.dim_shop__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local'
AND is_cb_shop = 1
) AS s ON a.shop_id = s.shop_id
JOIN (
-- limit data size before join
SELECT
shop_id,
shop_level1_category_id,
shop_level1_category,
shop_level1_global_be_category_id,
shop_level1_global_be_category
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local'
) AS l ON a.shop_id = l.shop_id
where a.is_live_shop =1
and a.grass_date = current_date - interval '1' day 


),


tag_type as 
(
SELECT cast(a.tag_id as int) as tag_id 
, type as wh_type
, tag_name 
, market as grass_region 
, cast(commission_rate as double) as tag_commission_rate 
, update_date as tag_update_date
, max(date(from_unixtime(metrics_cycle_time/1000))) - interval '1' day as tag_max_metrics_time
FROM regcbbi_others.mkt__commission_tag_id_mapping__reg_s0 a
LEFT JOIN marketplace.shopee_seller_shoptag_db__shop_tag_status_tab__reg_daily_s0_live b on cast(a.tag_id as int) = b.tag_id
WHERE type = 'CBWH'
AND b.grass_region != ''
GROUP BY 1,2,3,4,5,6
)
,




user as 
(
SELECT grass_region 
, u.shop_id 
, user_status 
, registration_date as shop_registration_date 
, shop_status
FROM
(
SELECT grass_region 
, shop_id 
, status as user_status
, date(from_unixtime(registration_timestamp)) as registration_date 
FROM mp_user.dim_user__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
and is_cb_shop = 1
) u
LEFT JOIN 
(
SELECT shop_id 
, status as shop_status 
FROM mp_user.dim_shop__reg_s0_live 
WHERE grass_date = current_date - interval '1' day 
and tz_type = 'local'
) s on u.shop_id = s.shop_id
),



category_cluster as (

SELECT
--shop_id,
shop_level1_category,
shop_level1_category_id,
shop_level1_global_be_category_id,
shop_level1_global_be_category,
-- item gobal category percentae -> main global category 
-- using global
case when shop_level1_global_be_category in ('Mobile & Gadgets','Audio','Computer & Accessories','Home Appliances','Cameracs & Drones','Gaming & Consoles') then 'ELECTRONICS'
when shop_level1_global_be_category in ('Women Clothes','Women Shoes','Men Clothes','Men Shoes','Fashion Accessories','Women Bags','Men Bags','Baby & Kids Fashion','Watches','Muslim Fashion','Travel & Luggage') then 'FASHION' 
when shop_level1_global_be_category in ('Beauty','Mom & Baby','Health','Food & Beverages') then 'FMCG'
when shop_level1_global_be_category in ('Home & Living','Automobiles','Sports & Outdoors','Stationary','Hobbies & Collections','Pets','Motorcycles','Books & Magazines') then 'LIFESTYLE'
else 'unknown' end as Cluster
FROM
mp_item.dws_shop_listing_td__reg_s0_live
WHERE
grass_date = CURRENT_DATE - INTERVAL '1' DAY
AND tz_type = 'local' 
),




-- raw db
db_shop_tag as (
select tag_id, tag_name, tag_max_metrics_time, tag_commission_rate, db.*
from tag_type t
join (


SELECT 'TH Non Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'TH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)


UNION 
SELECT 'TH Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
-- , a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'TH'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1


UNION 
SELECT 'MY Non Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'MY'
and a.fbs_flag = '1'
and(a.pff_flag ='0' or a.pff_flag is null)

UNION 
SELECT 'MY Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'MY'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1


UNION 
SELECT 'VN Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'VN'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and a.is_official_shop = 1


UNION 
SELECT 'VN Non Mall' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'VN'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_official_shop = 0 or a.is_official_shop is null)


UNION 
SELECT 'ID Non Star' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt 
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month', current_date) - interval'1' month
and a.is_cb_shop = 1
and a.country = 'ID'
and a.fbs_flag = '1'
and (a.pff_flag ='0' or a.pff_flag is null)
and (a.is_preferred_shop = 0 or a.is_preferred_shop is null)


UNION 
SELECT 'ID Star' as rule_type 
, a.country 
, a.shop_id 
, a.is_cb_shop
, a.fbs_flag
, a.pff_flag
--, a.is_cb_shop
, a.is_official_shop
, a.is_preferred_shop
, a.first_arrive_months
, a.shop_level1_category_id 
, date(dt) as dt
FROM mkpldp_shop_health.aggr_seller_cbcommision_metric_da a 
-- join marketplace.shopee_seller_management_db__merchant_shop_tab__reg_daily_s0_live b on a.shop_id = b.shop_id 
WHERE date(dt) >= date_trunc('month