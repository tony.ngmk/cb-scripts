insert into nexus.test_ec40bfcf15eeacfae12d14cb76f7f79030ce45aa65de87e6ff5d3a173330c12f_28fc941341c685edefd18e1a8f8fe5b8 --- shop --- live sku --- live model --- model edit y/n y--- price up/down --- avg 


with t as (select distinct cast(affi_shopid as BIGINT) affi_shopid 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0
where batch='batch4'
)




SELECT DISTINCT
-- m.grass_date, 
try(count(distinct CASE WHEN m.model_price < m.model_price_before_discount THEN m.model_id END )*decimal'1.000'/count(distinct m.model_id)) model_with_dp_perc 
FROM (select distinct 
item_id 
, shop_id 
, model_id 
, model_price
, model_price_before_discount
, grass_date
from 
mp_item.dim_model__reg_s0_live m 
WHERE m.tz_type = 'local' 
and m.seller_status = 1 and m.shop_status = 1 and model_status = 1 and m.item_status = 1 and m.is_cb_shop = 1 and m.is_holiday_mode = 0 
and m.grass_date= current_date-interval'1'day 
and m.grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) m 
join t on t.affi_shopid = m.shop_id 
-- group by 1