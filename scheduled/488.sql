insert into nexus.test_4dd93883664875f86eb50e9834f8c5698c4f561c4cd52b566e7b3e17b7c9880d_8c83769ce5d2bc3779c517ba61a139bc SELECT i.grass_date AS data_date
, i.grass_region
, count(distinct IF(i.is_cb_shop = 1 AND i.item_cdate >= date_trunc('month', i.grass_date), i.item_id, NULL)) AS cb_mtd_new_sku
, count(distinct IF(i.is_cb_shop = 1, i.item_id, NULL)) AS cb_total_sku
, count(distinct i.item_id) AS country_total_sku
FROM
(
SELECT distinct grass_date
, grass_region
, shop_id
, item_id
, DATE(from_unixtime(create_timestamp)) AS item_cdate
, is_cb_shop
FROM mp_item.dim_item__reg_s0_live
WHERE tz_type = 'local'
AND status = 1
AND stock > 0
AND seller_status = 1 --dim_user.status
AND shop_status = 1 --dim_shop.status
AND (is_holiday_mode = 0 OR is_holiday_mode is NULL)
AND grass_date in (date(from_unixtime(1655935200)), date_add('day', -7, date(from_unixtime(1655935200))), date_add('day', -14, date(from_unixtime(1655935200))), date_add('day', -1, date_trunc('month', date(from_unixtime(1655935200)))))
AND grass_region NOT in ('AR', 'IN', 'FR')
) i 
JOIN
(
SELECT distinct grass_date, grass_region, shop_id, DATE(from_unixtime(last_login_timestamp_td)) AS last_login
FROM mp_user.dws_user_login_td_account_info__reg_s0_live
WHERE grass_date in (date(from_unixtime(1655935200)), date_add('day', -7, date(from_unixtime(1655935200))), date_add('day', -14, date(from_unixtime(1655935200))), date_add('day', -1, date_trunc('month', date(from_unixtime(1655935200)))))
AND DATE(from_unixtime(last_login_timestamp_td)) >= date_add('day', -6, grass_date)
AND tz_type = 'local'
AND grass_region NOT in ('AR', 'IN', 'FR')
) u3
ON i.shop_id = u3.shop_id
AND i.grass_date = u3.grass_date
LEFT JOIN 
(
SELECT distinct grass_date, item_id
FROM mp_item.dim_censorship_item__reg_s0_live
WHERE tz_type = 'local'
AND grass_date in (date(from_unixtime(1655935200)), date_add('day', -7, date(from_unixtime(1655935200))), date_add('day', -14, date(from_unixtime(1655935200))), date_add('day', -1, date_trunc('month', date(from_unixtime(1655935200)))))
AND grass_region in ('ID', 'TW', 'BR')
AND item_censored_reason_id is NOT NULL
) excl
ON i.grass_date = excl.grass_date
AND i.item_id = excl.item_id
WHERE excl.item_id is NULL
GROUP BY 1, 2
ORDER BY 1 DESC, 2