insert into nexus.test_0e7bdae285d54095812bdc6caff7052f15cf3e32d8340b1b2de02e8d1bdd7666_a25dee75b3b8f372ff4a8a9163e34379 WITH sip AS (
SELECT DISTINCT
c.affi_shopid
, c.country affi_country
, mst_shopid
, b.country mst_country
, b.cb_option
FROM
(select distinct shopid 
, country 
, cb_option
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live)b 
join (SELECT DISTINCT AFFI_shopid 
, mst_shopid
, country 
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where grass_region IN ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and sip_shop_status!=3 ) c on c.mst_shopid = b.shopid 
)


select distinct 
current_date-interval'1'day update_time 
, sip.mst_country 
, affi_country 
, cb_option 
, case when t.shopid is not null then 'Y' else 'N' end as is_cnsc 
, count(distinct i.shop_id) shop
, count(distinct I.item_id) sku 
, count(distinct case when create_timestamp=current_date-interval'1'day then i.item_id else null end) new_create_sku 
, count(distinct case when create_timestamp<current_date-interval'1'day and audit.item_id is not null then i.item_id else null end) re_edit_sku 
, sum( case when create_timestamp<current_date-interval'1'day and audit.item_id is not null then oo.placed_order_fraction_30d else null end) re_edit_l30d_ado
from (select item_id
, date(from_unixtime(create_timestamp)) create_timestamp
, shop_id 
from mp_item.dim_item__reg_s0_live
where 
grass_date=current_date-interval'1'day and is_cb_shop=1 and shop_status=1 and seller_status=1 and is_holiday_mode=0 and status=1 and stock> 0 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) i 
join sip on sip.affi_shopid = i.shop_id 
left join (select distinct cast(shopid as BIGINT) shopid 
from regcbbi_others.shopee_regional_cb_team__cnsc_pre_whitelist
where ingestion_timestamp = (select max(ingestion_timestamp) col
from regcbbi_others.shopee_regional_cb_team__cnsc_pre_whitelist
)) t on t.shopid = sip.affi_shopid 
left join ( SELECT distinct item_id
, shop_id
, country
, data.client_id
, audit_id
, data.reason
, data.operator
, type_info.new 
, type_info.old 
, data.fields[1].new new_2
, ctime
, date(from_unixtime(ctime)) ctime_date 
, type_info.name type_name 
FROM marketplace.shopee_item_audit_log_db__item_audit_tab__reg_daily_s0_live
CROSS JOIN UNNEST(data.fields) AS type_info
WHERE country IN ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR') -- ,'TW','ID','PH','BR','MX','CO','CL','PL','ES','FR'
AND date = current_date-interval'1'day 
AND date(from_unixtime(ctime)) = current_date-interval'1'day 
AND date(from_unixtime(ctime)) = current_date-interval'1'day 
AND REGEXP_like(CAST(type_info.name as VARCHAR) ,'(name|description|attributes|images|categories)')
-- and TRY(data.fields[1].new) IN ('1')
and type_info.old !=''
) as audit on audit.item_id = i.item_id 
left join (select distinct item_id
, placed_order_fraction_30d/30.000 placed_order_fraction_30d
from mp_order.dws_item_gmv_nd__reg_s0_live
where grass_date=current_date-interval'1'day and tz_type='local' and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) oo on oo.item_id = i.item_id 
GROUP BY 1,2,3,4, 5