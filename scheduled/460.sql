insert into nexus.test_3026b78152ceb74b0d27ff468c2f7e6eea0260927ea2eda9a5a960d6150a1f5b_b17092783def7770079a92de873521b2 with seller as (
select 
c.gp_name as gp_account_name
, shop_id as shopid
, user_id as userid
, c.user_name as child_account_name 
, c.grass_region
, c.gp_create_date
, c.shopee_account_created_date
, icb.gp_smt_pic
, icb.gp_smt_tier
, icb.seller_is_brand
from
dev_regcbbi_kr.jpcb_shop_profile c
join
regcbbi_kr.jpcb_icb_gp_allocation icb
on 
c.gp_name = icb.gp_name
where
c.grass_region <> ''
and ingestion_timestamp = (select max(ingestion_timestamp) as latest_ingestion from regcbbi_kr.jpcb_icb_gp_allocation)
and grass_date = (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.jpcb_shop_profile
where grass_region <> '')
group by 1,2,3,4,5,6,7,8,9,10
),
raw_orders as (
select
o.*
from
mp_order.dwd_order_all_ent_df__reg_s0_live as o 
join
seller as s
on
o.shop_id = s.shopid
where
o.grass_region in ('SG','MY','ID','PH','TH','TW')
and o.is_bi_excluded = 0
and o.tz_type = 'local'
and o.is_cb_shop = 1
),
orders as (
SELECT
a.shop_id,


count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) = date_add('day', -1, current_date) THEN a.order_id ELSE null END) yesterday_orders,


count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('week', current_date) AND date_add('day', -1, current_date) THEN a.order_id ELSE null END) WTD_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('day', -7, date_trunc('week', current_date)) AND date_add('day', -1, date_trunc('week', current_date)) THEN a.order_id ELSE null END) W_1_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('day', -14, date_trunc('week', current_date)) AND date_add('day', -8, date_trunc('week', current_date)) THEN a.order_id ELSE null END) W_2_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('month', current_date) AND date_add('day', -1, current_date) THEN a.order_id ELSE null END) MTD_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date)) THEN a.order_id ELSE null END) M_1_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date))) THEN a.order_id ELSE null END) M_2_orders,
count(distinct CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -3, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -2, date_trunc('month', current_date))) THEN a.order_id ELSE null END) M_3_orders,


sum(CASE WHEN date(split(a.create_datetime,' ')[1]) = date_add('day', -1, current_date) THEN a.gmv_usd ELSE 0 END) yesterday_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('week', current_date) AND date_add('day', -1, current_date) THEN gmv_usd ELSE 0 END) WTD_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('day', -7, date_trunc('week', current_date)) AND date_add('day', -1, date_trunc('week', current_date)) THEN gmv_usd ELSE 0 END) W_1_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('day', -14, date_trunc('week', current_date)) AND date_add('day', -8, date_trunc('week', current_date)) THEN gmv_usd ELSE 0 END) W_2_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('month', current_date) AND date_add('day', -1, current_date) THEN gmv_usd ELSE 0 END) MTD_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date)) THEN gmv_usd ELSE 0 END) M_1_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date))) THEN gmv_usd ELSE 0 END) M_2_GMV,
sum(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_add('month', -3, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -2, date_trunc('month', current_date))) THEN gmv_usd ELSE 0 END) M_3_GMV,


case
when date_trunc('quarter',current_date) = current_date 
then count(distinct(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('quarter',date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN a.order_id ELSE null END))
else count(distinct(CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('quarter',current_date) AND date_add('day', -1, current_date) THEN a.order_id ELSE null END))
end as QTD_orders,
case
when date_trunc('quarter',current_date) = current_date 
then sum((CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('quarter',date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN a.gmv_usd ELSE 0 END))
else sum((CASE WHEN date(split(a.create_datetime,' ')[1]) BETWEEN date_trunc('quarter',current_date) AND date_add('day', -1, current_date) THEN a.gmv_usd ELSE 0 END))
end as QTD_GMV


FROM
raw_orders as a
WHERE 
date(split(a.create_datetime,' ')[1]) between date_add('day', -130, current_date) and date_add('day', -1, current_date)
and a.grass_date >= date_add('day', -130, current_date)
GROUP BY 
1
),
ado AS (
SELECT
shop_id,
yesterday_orders,
cast(WTD_orders as double) / day_of_week(date_add('day', -1, current_date)) WTD_ADO,
cast(W_1_orders as double) / 7 W_1_ADO,
cast(W_2_orders as double) / 7 W_2_ADO,
cast(MTD_orders as double) / day_of_month(date_add('day', -1, current_date)) MTD_ADO,
cast(M_1_orders as double) / date_diff('day', date_add('month', -1, date_trunc('month', current_date)), date_trunc('month', current_date)) M_1_ADO,
cast(M_2_orders as double) / date_diff('day', date_add('month', -2, date_trunc('month', current_date)), date_add('month', -1, date_trunc('month', current_date))) M_2_ADO,
cast(M_3_orders as double) / date_diff('day', date_add('month', -3, date_trunc('month', current_date)), date_add('month', -2, date_trunc('month', current_date))) M_3_ADO,
cast(MTD_orders as double) as MTD_gross_order,
cast(M_1_orders as double) as M_1_gross_order,
cast(M_2_orders as double) as M_2_gross_order,
cast(M_3_orders as double) as M_3_gross_order,


yesterday_GMV,
WTD_GMV / day_of_week(date_add('day', -1, current_date)) as WTD_GMV,
W_1_GMV / 7 as W_1_GMV,
W_2_GMV / 7 as W_2_GMV,
MTD_GMV / day_of_month(date_add('day', -1, current_date)) as MTD_GMV,
M_1_GMV / date_diff('day', date_add('month', -1, date_trunc('month', current_date)), date_trunc('month', current_date)) as M_1_GMV,
M_2_GMV / date_diff('day', date_add('month', -2, date_trunc('month', current_date)), date_add('month', -1, date_trunc('month', current_date))) as M_2_GMV,
M_3_GMV / date_diff('day', date_add('month', -3, date_trunc('month', current_date)), date_add('month', -2, date_trunc('month', current_date))) as M_3_GMV,


QTD_orders / if(date_trunc('quarter',current_date) = current_date, 
date_diff('day',date_trunc('quarter',date_add('day',-1,current_date)),current_date),
date_diff('day',date_trunc('quarter',current_date),current_date)
) as QTD_ADO,


QTD_GMV / if(date_trunc('quarter',current_date) = current_date, 
date_diff('day',date_trunc('quarter',date_add('day',-1,current_date)),current_date),
date_diff('day',date_trunc('quarter',current_date),current_date)
) as QTD_GMV


FROM
orders
),
cfs_orders as (
select
o.shop_id,
sum(case when o.item_promotion_source = 'flash_sale' then o.order_fraction else 0 end) as yesterday_cfs_orders
from
mp_order.dwd_order_item_all_ent_df__reg_s0_live as o 
join
seller as s
on
o.shop_id = s.shopid
and o.is_bi_excluded = 0
and date(split(o.create_datetime,' ')[1]) = date_add('day', -1, current_date)
and o.item_promotion_source = 'flash_sale'
where
o.grass_region in ('SG','MY','ID','PH','TH','TW')
group by
1
),
skus AS (
SELECT
a.shop_id,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) = date_add('day', -1, current_date)) THEN item_id ELSE null END)) yesterday_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) >= date_trunc('week', current_date)) THEN item_id ELSE null END)) WTD_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('day', -7, date_trunc('week', current_date)) AND date_add('day', -1, date_trunc('week', current_date))) THEN item_id ELSE null END)) W_1_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('day', -14, date_trunc('week', current_date)) AND date_add('day', -8, date_trunc('week', current_date))) THEN item_id ELSE null END)) W_2_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_trunc('month', current_date) AND date_add('day', -1, current_date)) THEN item_id ELSE null END)) MTD_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date))) THEN item_id ELSE null END)) M_1_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date)))) THEN item_id ELSE null END)) M_2_uploaded_sku,
count(DISTINCT (CASE WHEN (date(from_unixtime(create_timestamp)) BETWEEN date_add('month', -3, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -2, date_trunc('month', current_date)))) THEN item_id ELSE null END)) M_3_uploaded_sku
FROM
mp_item.dim_item__reg_s0_live as a
JOIN
seller as s
ON
a.shop_id = s.shopid
WHERE
status = 1
AND stock > 0
and a.grass_date = date_add('day',-1,current_date) --어제 status 날짜로 필터 및 tz_type 추가
and a.tz_type = 'local'
GROUP BY
1
),
penalty1 AS (
SELECT
shop_id,
sum(point) as current_penalty_score
FROM
(marketplace.shopee_seller_account_health_db__penalty_history_log_tab__reg_continuous_s0_live p
INNER JOIN seller s ON (shop_id = s.shopid))
WHERE 
manual_status = 1
AND auto_status = 1 
AND date_parse(execute_dt, '%Y%m%d') >= date_trunc('quarter',current_date) 
AND region != 'REGION_NA'
and p.grass_region in ('SG','MY','ID','PH','TH','TW')
GROUP BY
1
),
penalty2 AS (
SELECT
shop_id,
sum((CASE WHEN (date(parse_datetime(execute_dt, 'yyyymmdd')) BETWEEN date_add('day', -7, date_trunc('week', current_date)) AND date_add('day', -1, date_trunc('week', current_date))) THEN point ELSE null END)) W_1_penalty_points,
sum((CASE WHEN (date(parse_datetime(execute_dt, 'yyyymmdd')) BETWEEN date_add('day', -14, date_trunc('week', current_date)) AND date_add('day', -8, date_trunc('week', current_date))) THEN point ELSE null END)) W_2_penalty_points
FROM
(marketplace.shopee_seller_account_health_db__penalty_history_log_tab__reg_continuous_s0_live p
INNER JOIN seller s ON (shop_id = s.shopid))
WHERE 
date(parse_datetime(execute_dt, 'yyyymmdd')) >= date_add('day', -21, current_date)
AND metrics_name = 'point'
AND auto_status = 1
AND manual_status = 1
and p.grass_region in ('SG','MY','ID','PH','TH','TW')
GROUP BY 1
),
apt AS (
SELECT
a.shopid,
round(avg(CASE 
WHEN a.payment_method_id = 6 THEN b.wh_ctime - a.create_timestamp 
ELSE b.wh_ctime - a.pay_timestamp
END) / 86400, 4) as APT
FROM (
select
shop_id as shopid,
payment_method_id,
order_id,
create_timestamp,
pay_timestamp
from
raw_orders
where
date(split(create_datetime,' ')[1]) between date_add('day', -7, current_date) and date_add('day', -1, current_date)
and grass_date >= date_add('day', -7, current_date)
group by
1,2,3,4,5
) as a
JOIN (
SELECT
a.orderid,
min(a.ctime) as wh_ctime
FROM
marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live as a
join
seller as s
on
a.shopid = s.shopid 
and a.new_status = 2 
where
a.grass_region in ('SG','MY','ID','PH','TH','TW')
GROUP BY 
1
having
date(from_unixtime(min(a.ctime))) >= date_add('day',-30,current_date)
) as b 
ON 
a.order_id = b.orderid
GROUP BY 
1
),
cancel AS (
SELECT
shop_id,
(CASE WHEN (lifetime_order = 0) THEN null ELSE (CAST(lifetime_cancellation AS double) / CAST(lifetime_order AS double)) END) lifttime_cancellation_rate,
(CASE WHEN (M_1_order = 0) THEN null ELSE (CAST(M_1_cancellation AS double) / CAST(M_1_order AS double)) END) M_1_cancellation_rate,
(CASE WHEN (M_2_order = 0) THEN null ELSE (CAST(M_2_cancellation AS double) / CAST(M_2_order AS double)) END) M_2_cancellation_rate
FROM
(
SELECT
o.shop_id,
count(DISTINCT (CASE WHEN (cancel_timestamp IS NOT NULL) THEN order_id ELSE null END)) lifetime_cancellation,
count(DISTINCT order_id) lifetime_order,
count(DISTINCT (CASE WHEN ((cancel_timestamp IS NOT NULL) AND (date(split(create_datetime, ' ')[1]) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date)))) THEN order_id ELSE null END)) M_1_cancellation,
count(DISTINCT (CASE WHEN (date(split(create_datetime, ' ')[1]) BETWEEN date_add('month', -1, date_trunc('month', current_date)) AND date_add('day', -1, date_trunc('month', current_date))) THEN order_id ELSE null END)) M_1_order,
count(DISTINCT (CASE WHEN ((cancel_timestamp IS NOT NULL) AND (date(split(create_datetime, ' ')[1]) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date))))) THEN order_id ELSE null END)) M_2_cancellation,
count(DISTINCT (CASE WHEN (date(split(create_datetime, ' ')[1]) BETWEEN date_add('month', -2, date_trunc('month', current_date)) AND date_add('day', -1, date_add('month', -1, date_trunc('month', current_date)))) THEN order_id ELSE null END)) M_2_order
FROM
raw_orders as o
GROUP BY 
1
)
),
u1 as (
select
u.*
from
mp_user.dim_user__reg_s0_live as u
join
seller as s
on
u.shop_id = s.shopid
and u.grass_date = date_add('day',-1,current_date)
where
u.tz_type = 'local'
),
u2 as (
select
u.*
from
mp_item.dws_shop_listing_td__reg_s0_live as u
join
seller as s
on
u.shop_id = s.shopid
and u.grass_date = date_add('day',-1,current_date)
where
u.tz_type = 'local'
),
u3 as (
select
u.*
from
mp_user.dws_shop_interaction_td__reg_s0_live as u
join
seller as s
on
u.shop_id = s.shopid
and u.grass_date = date_add('day',-1,current_date)
where
u.tz_type = 'local'
),
u4 as (
select
u.*
from
mp_user.dim_shop__reg_s0_live as u
join
seller as s
on
u.shop_id = s.shopid
and u.grass_date = date_add('day',-1,current_date)
where
u.tz_type = 'local'
),
u5 as (
select
u.*
from
mp_order.dws_seller_gmv_td__reg_s0_live as u
join
seller as s
on
u.shop_id = s.shopid
and u.grass_date = date_add('day',-1,current_date)
where
u.tz_type = 'local'
),
u6 as (
select
u.*
from
mp_user.ads_activeness_user_activity_td__reg_s0_live as u
join
seller as s
on
u.user_id = s.userid
and u.grass_date = date_add('day',-1,current_date)
where 
u.tz_type = 'local' 
),
sip as (
SELECT
distinct
a.affi_shopid AS shopid,
a.affi_userid AS userid,
a.affi_username AS username,
a.country
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live AS a
JOIN (
SELECT
distinct
shopid
FROM
seller
) AS s
ON
a.mst_shopid = s.shopid
where
a.grass_region in ('SG','MY','ID','PH','TH','TW')
),
merge as (
SELECT
distinct
s.grass_region,
s.shopid,
s.userid,
s.child_account_name username,
s.gp_account_name,
s.gp_smt_tier,
if(s.grass_region in ('MX','BR'), u2.shop_level1_category, u2.shop_level1_global_be_category) as shop_main_category,
if(s.grass_region in ('MX','BR'), u2.shop_level2_category, u2.shop_level2_global_be_category) as shop_sub_category,
case when sip.shopid is not null then 1 else 0 end as is_sip_shop,
s.shopee_account_created_date,
s.gp_create_date,
u1.status user_status,
u4.status shop_status,
date(split(u6.last_login_datetime_td,' ')[1]) last_login_date,
(CASE WHEN (u4.is_holiday_mode IS NULL) THEN 0 ELSE u4.is_holiday_mode END) holiday_mode_on,
(CASE WHEN (u4.is_official_shop = 1) THEN 'Y' ELSE 'N' END) official_store,
(CASE WHEN (u4.is_preferred_shop = 1) THEN 'Y' ELSE 'N' END) prefer_seller,
u4.rating_star,
COALESCE(u3.like_cnt_td, 0) total_liked_num,
COALESCE(u3.follower_cnt_td, 0) follower_count,
u2.active_item_with_stock_cnt shop_live_SKU,
COALESCE(current_penalty_score, 0) current_penalty_score,
COALESCE(W_1_penalty_points, 0) W_1_penalty_points,
COALESCE(W_2_penalty_points, 0) W_2_penalty_points,
u4.response_rate,
COALESCE(u5.placed_order_cnt_td, 0) gross_sold_orders,
COALESCE(cfs.yesterday_cfs_orders, 0) yesterday_cfs_orders,
COALESCE(orders.yesterday_orders, 0) yesterday_orders,
COALESCE(MTD_gross_order, 0) MTD_gross_order,
COALESCE(M_1_gross_order, 0) M_1_gross_order,
COALESCE(M_2_gross_order, 0) M_2_gross_order,
COALESCE(M_3_gross_order, 0) M_3_gross_order,
COALESCE(WTD_ADO, 0) WTD_ADO,
COALESCE(W_1_ADO, 0) W_1_ADO,
COALESCE(W_2_ADO, 0) W_2_ADO,
COALESCE(MTD_ADO, 0) MTD_ADO,
COALESCE(M_1_ADO, 0) M_1_ADO,
COALESCE(M_2_ADO, 0) M_2_ADO,
COALESCE(M_3_ADO, 0) M_3_ADO,
coalesce(QTD_ADO, 0) as QTD_ADO,
COALESCE(yesterday_GMV, 0) yesterday_ADGMV_USD,
COALESCE(WTD_GMV, 0) WTD_ADGMV_USD,
COALESCE(W_1_GMV, 0) W_1_ADGMV_USD,
COALESCE(W_2_GMV, 0) W_2_ADGMV_USD,
COALESCE(MTD_GMV, 0) MTD_ADGMV_USD,
COALESCE(M_1_GMV, 0) M_1_ADGMV_USD,
COALESCE(M_2_GMV, 0) M_2_ADGMV_USD,
COALESCE(M_3_GMV, 0) M_3_ADGMV_USD,
coalesce(QTD_GMV, 0) as QTD_ADGMV_USD,
COALESCE(skus.yesterday_uploaded_sku, 0) yesterday_uploaded_sku,
COALESCE(skus.WTD_uploaded_sku, 0) WTD_uploaded_sku,
COALESCE(skus.W_1_uploaded_sku, 0) W_1_uploaded_sku,
COALESCE(skus.W_2_uploaded_sku, 0) W_2_uploaded_sku,
COALESCE(skus.MTD_uploaded_sku, 0) MTD_uploaded_sku,
COALESCE(skus.M_1_uploaded_sku, 0) M_1_uploaded_sku,
COALESCE(skus.M_2_uploaded_sku, 0) M_2_uploaded_sku,
COALESCE(skus.M_3_uploaded_sku, 0) M_3_uploaded_sku,
APT,
lifttime_cancellation_rate,
M_1_cancellation_rate,
M_2_cancellation_rate,
CASE
WHEN s.grass_region = 'MY' THEN concat('https://shopee.com.my/', s.child_account_name)
WHEN s.grass_region = 'ID' THEN concat('https://shopee.co.id/', s.child_account_name)
WHEN s.grass_region = 'PH' THEN concat('https://shopee.ph/', s.child_account_name)
WHEN s.grass_region = 'SG' THEN concat('https://shopee.sg/', s.child_account_name)
WHEN s.grass_region = 'TH' THEN concat('https://shopee.co.th/', s.child_account_name)
WHEN s.grass_region = 'TW' THEN concat('https://shopee.tw/', s.child_account_name)
WHEN s.grass_region = 'VN' THEN concat('https://shopee.vn/', s.child_account_name)
WHEN s.grass_region = 'BR' THEN concat('https://shopee.com.br/', s.child_account_name)
when s.grass_region = 'MX' then concat('https://shopee.com.mx/', s.child_account_name)
END as shop_url
FROM
seller s
LEFT JOIN
u1
ON
s.shopid = u1.shop_id
LEFT JOIN
u2
ON
s.shopid = u2.shop_id
LEFT JOIN
u3
ON
s.shopid = u3.shop_id
LEFT JOIN
u4
ON
s.shopid = u4.shop_id
LEFT JOIN
u5
ON
s.shopid = u5.shop_id
LEFT JOIN
u6
ON
s.userid = u6.user_id
LEFT JOIN
penalty1 p1
ON
p1.shop_id = s.shopid
LEFT JOIN
penalty2 p2
ON
p2.shop_id = s.shopid
LEFT JOIN
apt
ON
apt.shopid = s.shopid
LEFT JOIN
cancel
ON
cancel.shop_id = s.shopid
LEFT JOIN
skus
ON
skus.shop_id = s.shopid
LEFT JOIN
ado as orders
ON
orders.shop_id = s.shopid
left join
sip
on
s.shopid = sip.shopid
left join
cfs_orders as cfs
on
s.shopid = cfs.shop_id
WHERE
s.grass_region IS NOT NULL
)
select
*
from
merge
order by
5,6,1,4