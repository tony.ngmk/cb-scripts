insert into nexus.test_5e8b7d1f1bdf3f467312342af037c49a54f30ec7cb670b2c7168cde91b0589de_73ff283d48c3c8a7b3d28b3bff1036ec with allocation as (
SELECT * 
FROM regcbbi_others.shopee_regional_cb_team__new_krcb_gp_allocation
WHERE ingestion_timestamp in (
select max(ingestion_timestamp) as update_time from regcbbi_others.shopee_regional_cb_team__new_krcb_gp_allocation
)
)


, seller as (
select distinct s.gp_name as gp_name 
, s.user_name as shop_name
, s.shop_id as shopid
, a.type_1 as seller_type 
, a.type_2 as allocation
from dev_regcbbi_kr.krcb_shop_profile s
left join (
select
distinct
shopid
from
regcbbi_others.shopee_regional_bi_team__krcb_excluding_shops
where
ingestion_timestamp in (
select
max(ingestion_timestamp) as max_date
from
regcbbi_others.shopee_regional_bi_team__krcb_excluding_shops
)
and seller_request_unfreeze = 'N'
) e 
on s.shop_id = cast(e.shopid as bigint)
join allocation a
on s.gp_name = a.gp_name
where s.grass_date in (
select 
max(grass_date) as latest_ingest_date
from 
dev_regcbbi_kr.krcb_shop_profile
where 
grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
)
and 
grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
and 
e.shopid is null
and 
type_2 in ('SA', 'ICB')
)


, ado_category as (
select * 
from (
select gp_name
, level1_global_be_category_id
, level1_global_be_category
, sum(order_fraction)/60.0 as ado
, row_number() over (partition by gp_name order by sum(order_fraction) desc) as order_rank
from mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
join seller s on s.shopid = o.shop_id 
where tz_type = 'local'
and grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
and grass_date >= date_add('day', -60, current_date)
and is_bi_excluded = 0 
and date(create_datetime) >= date_add('day', -60, current_date) 
group by 1,2,3
)
where order_rank = 1 
)


, sku_category as (
select * 
from (
select gp_name
, level1_global_be_category_id
, level1_global_be_category
, count(distinct item_id) as sku
, row_number() over (partition by gp_name order by count(distinct item_id) desc) as sku_rank
from mp_item.dim_item__reg_s0_live i 
join seller s on s.shopid = i.shop_id 
where tz_type = 'local'
and grass_date = current_date - interval '1' day 
and is_cb_shop = 1 
group by 1,2,3
)
where sku_rank = 1 
)


, orders as (
select gp_name
, sum(order_fraction) as l60d_gross_orders
, sum(order_fraction)/60.0 as l60d_ado
, sum(case when date(create_datetime) >= date_add('day', -30, current_date) then order_fraction else 0 end) as l30d_gross_orders 
, sum(case when date(create_datetime) >= date_add('day', -30, current_date) then order_fraction else 0 end)/30.0 as l30d_ado
from mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
join seller s on s.shopid = o.shop_id 
where tz_type = 'local'
and grass_region in ('SG', 'MY', 'ID', 'TH', 'TW', 'PH', 'VN', 'BR', 'MX', 'PL')
and grass_date >= date_add('day', -60, current_date)
and is_bi_excluded = 0 
and date(create_datetime) >= date_add('day', -60, current_date) 
group by 1
)


select distinct allocation
, gp_name 
, cluster 
, level1_global_be_category 
, seller_type 
, l60d_ado 
, l60d_gross_orders 
, l30d_ado 
, l30d_gross_orders
from (
select s.allocation
, gp_name 
, coalesce(a.level1_global_be_category_id, s.level1_global_be_category_id) as level1_global_be_category_id 
, coalesce(a.level1_global_be_category, s.level1_global_be_category) as level1_global_be_category
, seller_type 
, l60d_ado 
, l60d_gross_orders
, l30d_ado 
, l30d_gross_orders 
from seller s 
left join ado_category a using (gp_name) 
left join sku_category s using (gp_name) 
left join orders o using (gp_name) 
group by 1,2,3,4,5,6,7,8,9
) a 
left join regcbbi_kr.category_cluster_map c on c.l1_global_be_cat_id = cast(a.level1_global_be_category_id as varchar)