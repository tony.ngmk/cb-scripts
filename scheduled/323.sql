insert into nexus.test_8182cb8c0cc1f3d2bebd079164f57467631816982f2674541e050d0402e3cf69_709d5b6a03134d87dcc11bb709992faa with gov_agency_list as (
select
try(cast(shopid as int)) as shopid,
project_type
from
regcbbi_kr.krcb_gov_agency_shop_list
where
ingestion_timestamp in (
select
max(ingestion_timestamp) as latest_ingest_time
from
regcbbi_kr.krcb_gov_agency_shop_list
)
group by
1,2
),
seller as (
select 
shop_id as shopid,
user_id as userid,
shopee_account_created_date,
gp_create_date as gp_account_shopee_account_created_date,
grass_region,
user_name as shop_name,
gp_name,
gp_account_owner,
gp_smt,
child_account_owner,
seller_type,
project_type
from
dev_regcbbi_kr.krcb_shop_profile as s
join
gov_agency_list as i
on
s.shop_id = i.shopid
where
grass_date in (
select
max(grass_date) as latest_ingest_date
from
dev_regcbbi_kr.krcb_shop_profile
where
grass_region <> ''
) 
),
raw_tws as (
select
shopee_order_sn,
actual_weight,
relabel_time,
handover_time
from
tws_mart.shopee_tws_db__transit_order_tab__reg_daily_s0_live as t
where 
pickup_country = 'KR'
and from_unixtime(order_time) >= CURRENT_DATE - interval '9' day
group by
1,2,3,4
),
model_info as (
select
item_id,
item_name,
model_id,
model_name
from
mp_item.dim_model__reg_s0_live as m
join
seller as s
on
m.shop_id = s.shopid
and m.tz_type = 'local'
and m.grass_date = date_add('day',-1,current_date)
where
m.grass_region <> ''
group by
1,2,3,4
)
select
date(split(o.create_datetime,' ')[1]) as grass_date,
s.project_type,
o.grass_region,
s.gp_name,
s.shopid,
o.order_id,
o.order_sn,
o.create_datetime as local_create_time,
from_unixtime(o.create_timestamp) as create_time_regional,
from_unixtime(o.shipping_confirm_timestamp) as shipping_confirm_time_regional,
from_unixtime(t.relabel_time) as Shipped_Time_regional,
from_unixtime(t.handover_time) as Handover_Time_regional,
from_unixtime(o.complete_timestamp) as Deliver_Time_regional,
o.item_id,
o.item_name,
o.model_id,
m.model_name,
o.item_amount,
o.order_fraction as gross_orders,
t.actual_weight as actual_weight_per_order,
o.gmv,
o.gmv_usd,
o.order_price_pp as deal_price,
o.order_price_pp_usd as deal_price_usd,
o.item_price_before_discount_pp as original_price,
o.item_price_before_discount_pp_usd as original_price_usd,
o.actual_shipping_carrier,
o.order_fe_status as order_status,
o.actual_shipping_fee,
o.actual_shipping_fee_usd,
o.buyer_paid_shipping_fee,
o.buyer_paid_shipping_fee_usd,
o.actual_shipping_rebate_by_shopee_amt,
o.actual_shipping_rebate_by_shopee_amt_usd,
o.actual_shipping_rebate_by_seller_amt,
o.actual_shipping_rebate_by_seller_amt_usd,
o.commission_fee,
o.commission_fee_usd,
o.service_fee,
o.service_fee_usd,
o.level1_global_be_category,
o.level2_global_be_category,
o.level3_global_be_category
from
mp_order.dwd_order_item_all_ent_df__reg_s0_live as o 
join
seller as s
on
o.shop_id = s.shopid
and o.tz_type = 'local'
and date(split(o.create_datetime,' ')[1]) between CURRENT_DATE - interval '8' day and CURRENT_DATE -interval '1' day
and o.grass_date >= CURRENT_DATE - interval '8' day
and o.is_bi_excluded = 0
left join
raw_tws as t
on
o.order_sn = t.shopee_order_sn
join
model_info as m
on
o.item_id = m.item_id
and o.model_id = m.model_id
where
o.grass_region <> ''