insert into nexus.test_04c6f40ebc587ea8ce115b93f02c1dd53def5294745a94bf843ca4967b39bd7f_88a2efa43da0ddc818047ddd0442de12 -- 2022/03/09 updates:
-- adjusted : RR logic: to exclude non receipt buyer win RR
-- 2022/03/14 updates:
-- excluded offboarded shop new order from medium table (only include )
-- 2022/04/14 updates:
-- optimized tax calculation: 
-- this code only calculate for order level tax using order_v4 (TW, MX, CL)
-- for item level tax, can be obtained from order_item tables




WITH wh AS (SELECT distinct * FROM regcbbi_others.logistics_basic_info WHERE wh_outbound_date >= CURRENT_DATE - INTERVAL '180' DAY) 


, sip AS (SELECT DISTINCT b.affi_shopid
, b.mst_shopid
, b.country AS affi_country
, a.country AS mst_country
, price_ratio/100000.00 AS sip_rate
, sip_shop_status
, offboard_time --UNIXTIME
, shop_margin/100000.00 AS shop_margin --CN default: 0
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a 
JOIN (SELECT DISTINCT * FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE grass_region <> '') b 
on b.mst_shopid = a.shopid 
WHERE a.cb_option = 1)


, o AS (
SELECT distinct order_id,
order_sn,
order_fe_status,
order_be_status,
payment_method_id,
shop_id,
shop_name,
seller_id, 
seller_name,
seller_shipping_address_state,
seller_shipping_address_city,
buyer_id,
buyer_name,
-- buyer_shipping_address,
buyer_shipping_address_state,
buyer_shipping_address_city,
shipping_discount_by_3pl_to_seller_amt,
shipping_discount_by_3pl_to_seller_amt_usd,
estimate_shipping_fee,
estimate_shipping_fee_usd,
actual_shipping_fee,
actual_shipping_fee_usd,
actual_buyer_paid_shipping_fee,
actual_buyer_paid_shipping_fee_usd,
buyer_paid_shipping_fee,
buyer_paid_shipping_fee_usd,
shopee_snapshot_discount_shipping_fee,
shopee_snapshot_discount_shipping_fee_usd,
seller_snapshot_discount_shipping_fee,
seller_snapshot_discount_shipping_fee_usd,
shipping_discount_promotion_type_id,
shipping_discount_promotion_type,
shipping_traceno,
logistics_status_id,
logistics_status,
logistics_channel_promotion_rule_id,
logistics_channel_discount_promotion_rule_id,
is_seller_cover_shipping_fee,
estimate_shipping_rebate_by_shopee_amt,
estimate_shipping_rebate_by_shopee_amt_usd,
actual_shipping_rebate_by_shopee_amt,
actual_shipping_rebate_by_shopee_amt_usd,
estimate_shipping_rebate_by_seller_amt,
estimate_shipping_rebate_by_seller_amt_usd,
actual_shipping_rebate_by_seller_amt,
actual_shipping_rebate_by_seller_amt_usd,
is_escrow_includes_asf,
escrow_to_seller_amt,
escrow_to_seller_amt_usd,
gmv,
gmv_usd,
nmv,
nmv_usd,
currency,
pv_coin_earn_by_shopee,
pv_coin_earn_by_seller,
sv_coin_earn_by_shopee,
sv_coin_earn_by_seller,
pv_coin_earn_by_seller_amt,
pv_coin_earn_by_seller_amt_usd,
pv_coin_earn_by_shopee_amt,
pv_coin_earn_by_shopee_amt_usd,
sv_coin_earn_by_seller_amt,
sv_coin_earn_by_seller_amt_usd,
sv_coin_earn_by_shopee_amt,
sv_coin_earn_by_shopee_amt_usd,
coin_used,
coin_used_cash_amt,
coin_used_cash_amt_usd,
pv_promotion_id,
pv_promotion_name,
pv_voucher_code,
pv_voucher_type,
pv_rebate_by_seller_amt,
pv_rebate_by_seller_amt_usd,
pv_rebate_by_shopee_amt,
pv_rebate_by_shopee_amt_usd,
is_pv_seller_absorbed,
sv_promotion_id,
sv_promotion_name,
sv_voucher_code,
sv_voucher_type,
sv_rebate_by_seller_amt,
sv_rebate_by_seller_amt_usd,
sv_rebate_by_shopee_amt,
sv_rebate_by_shopee_amt_usd,
is_sv_seller_absorbed,
fsv_promotion_id,
fsv_promotion_name,
fsv_voucher_code,
card_promotion_id,
card_rebate_by_bank_amt,
card_rebate_by_bank_amt_usd,
card_rebate_by_shopee_amt,
card_rebate_by_shopee_amt_usd,
seller_txn_fee,
seller_txn_fee_usd,
seller_txn_fee_shipping_fee,
seller_txn_fee_shipping_fee_usd,
buyer_txn_fee,
buyer_txn_fee_usd,
service_fee,
service_fee_usd, 
initial_service_fee, 
initial_service_fee_usd,
tax_exemption_amt,
tax_exemption_amt_usd,
grass_region,
shipping_carrier,
shipping_channel_id,
DATE(FROM_UNIXTIME(cancel_timestamp)) AS cancel_timestamp,
cancel_user_id,
cancel_datetime,
DATE(date_parse(create_datetime,'%Y-%m-%d %T')) AS grass_date,
shop_margin,
sip_rate,
mst_shopid,
mst_country,
create_timestamp


FROM mp_order.dwd_order_all_ent_df__reg_s0_live o 
JOIN sip ON o.shop_id = sip.affi_shopid
WHERE is_cb_shop = 1 
AND grass_date >= current_date - interval '270' day 
AND DATE(date_parse(create_datetime,'%Y-%m-%d %T')) >= current_date - interval '270' day
AND CASE WHEN sip_shop_status = 3 THEN create_timestamp < offboard_time 
ELSE create_timestamp > 0 END -- exclude offboarded shops' new orders
)


, rr AS (SELECT DISTINCT orderid
, reason
, SUM(refund_amount/100000.00) AS refund_amount
FROM marketplace.shopee_return_v2_db__return_v2_tab__reg_daily_s0_live
WHERE grass_region <> '' AND DATE(FROM_UNIXTIME(ctime)) >= CURRENT_DATE - INTERVAL '180' DAY 
AND refund_amount > 0 
AND status IN (2,5) -- buyer win RR
AND reason <> 1 -- exclude non receipt RR
GROUP BY 1,2)





, adj AS (SELECT DISTINCT TRY_CAST(orderid AS BIGINT) orderid
, MAX(kind) kind
, MIN(TRY_CAST(actual_amount AS DOUBLE)) AS actual_amount
FROM (SELECT DISTINCT a.*
, CASE WHEN remark = 'SIP Bundle Deal Order' THEN 4
WHEN remark = 'SIP Campaign Order' THEN 5
WHEN remark = 'SIP offline Adjustment' THEN 6 
WHEN remark = 'RR Compensation' THEN 7 
WHEN remark = 'SIP Voucher Adjustment' THEN 8 ELSE 0 END AS kind
FROM regcbbi_others.shopee_regional_cb_team__sip_offline_adj_order_level a 
JOIN (SELECT MAX(ingestion_timestamp) ingestion_timestamp from regcbbi_others.shopee_regional_cb_team__sip_offline_adj_order_level) b 
ON a.ingestion_timestamp = b.ingestion_timestamp)
GROUP BY 1)

, tax1 AS (SELECT DISTINCT orderid
, COALESCE(extinfo.tax_info.tax_amount/100000.00, 0) tax 
FROM marketplace.shopee_order_v4_db__order_v4_tab__reg_daily_s0_live
WHERE grass_region IN ('MX','TW','CL')
AND DATE(FROM_UNIXTIME(create_time)) >= CURRENT_DATE - INTERVAL '270' DAY)

, tax2 AS (SELECT DISTINCT orderid
, COALESCE(SUM(extinfo.order_item_tax_info.item_vat_info.seller_vat_amount/100000.00),0) AS tax
FROM marketplace.shopee_order_item_v3_db__order_item_v3_tab__reg_daily_s0_live
WHERE grass_region IN ('PL','ES','FR','CO')
GROUP BY 1)


SELECT DISTINCT wh_outbound_date
, wh_inbound_date
, o.*
, wh.actual_shipping_fee AS asf_sls
, wh.actual_weight
, reason
, COALESCE(refund_amount,0) AS refund_amount
, COALESCE(tax1.tax,COALESCE(tax2.tax,0)) AS tax 
, COALESCE(adj.kind,0) AS order_adj_type
, COALESCE(adj.actual_amount,0) AS order_adj_amount
FROM o 
LEFT JOIN wh ON o.order_sn = wh.ordersn
LEFT JOIN tax1 ON o.order_id = tax1.orderid
LEFT JOIN tax2 ON o.order_id = tax2.orderid
LEFT JOIN rr ON o.order_id = rr.orderid
LEFT JOIN adj ON o.order_id = adj.orderid
-- limit 100
-- WHERE o.grass_region = 'SG'
-- AND o.order_id = 96556836439505