insert into nexus.test_1492c4362c3a5e8a35aacb8bb8f50b7d5eb39b9713c3ab4ddff6c45c23263ac8_a558cfae3aaa52938c0682bc07427bde WITH
sip AS (
SELECT DISTINCT b.affi_shopid shopid
FROM
(marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b ON (b.mst_shopid = a.shopid))
WHERE (((a.cb_option = 0) AND (a.country = 'TW')) AND (b.grass_region IN ('MY', 'SG')))
) 
, top_100 AS (SELECT DISTINCT * FROM (
SELECT
o.grass_region
, o.grass_date
, o.shop_id
, "sum"(order_fraction) LD_orders
, rank() OVER (PARTITION BY grass_region ORDER BY sum(order_fraction) DESC) AS country_rank
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
INNER JOIN sip ON (sip.shopid = o.shop_id)
WHERE (o.grass_region IN ('SG', 'MY')) AND (o.grass_date = "date_add"('day', -1, current_date)) AND (is_placed = 1) AND (tz_type = 'local')
GROUP BY 1, 2, 3
) WHERE country_rank <= 100)

, sales_raw AS (
SELECT DISTINCT
o.grass_region
, o.order_id
, o.shop_id
, o.item_id
, o.model_id
, t.country_rank
, t.grass_date
, t.LD_orders
, (CASE WHEN (o.grass_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN 1 ELSE 0 END) mtd
, (CASE WHEN ("date_format"(o.grass_date, '%y%m') = "date_format"("date_add"('month', -1, "date"(current_date)), '%y%m')) THEN 1 ELSE 0 END) m_1
, (CASE WHEN (o.grass_date BETWEEN "date_trunc"('week', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN 1 ELSE 0 END) wtd
, (CASE WHEN (o.grass_date BETWEEN "date_add"('week', -1, "date_trunc"('week', current_date)) AND "date_add"('day', -1, "date_trunc"('week', current_date))) THEN 1 ELSE 0 END) w_1
, (CASE WHEN (o.grass_date BETWEEN "date_add"('week', -2, "date_trunc"('week', current_date)) AND "date_add"('day', -8, "date_trunc"('week', current_date))) THEN 1 ELSE 0 END) w_2
, (CASE WHEN (o.item_promotion_source = 'flash_sale') THEN 1 ELSE 0 END) flash_sale
, (CASE WHEN ((o.pv_voucher_code IS NOT NULL) OR (o.fsv_voucher_code IS NOT NULL)) THEN 1 ELSE 0 END) shopee_voucher
, (CASE WHEN ((sv_voucher_code IS NOT NULL) OR (is_sv_seller_absorbed = 1)) THEN 1 ELSE 0 END) seller_voucher
, order_fraction pp
, gmv_usd
FROM
(mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
INNER JOIN top_100 t ON (o.shop_id = t.shop_id))
WHERE ((o.grass_date >= "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))) AND (o.grass_region IN ('SG', 'MY', 'ID'))) AND (o.is_placed = 1) AND (o.tz_type = 'local')
) 

, sales AS (
SELECT
grass_region
, grass_date
, shop_id
, country_rank
, LD_orders
, ("sum"((CASE WHEN (mtd = 1) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double)) MTD_ADO
, ("sum"((CASE WHEN (((mtd = 1)) AND (flash_sale = 0)) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double)) MTD_Organic_Order
, ("sum"((CASE WHEN ((mtd = 1) AND (flash_sale = 1)) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double)) MTD_CFS_Order
, ("sum"((CASE WHEN ((mtd = 1) AND (seller_voucher = 1)) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double)) MTD_Seller_Voucher_Order
, ("sum"((CASE WHEN ((mtd = 1) AND (shopee_voucher = 1)) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double)) MTD_Shopee_Voucher_Order
, ("sum"((CASE WHEN (mtd = 1) THEN gmv_usd ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double)) MTD_GMV_USD
, ("sum"((CASE WHEN (m_1 = 1) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double)) M_1_ADO
, ("sum"((CASE WHEN (((m_1 = 1)) AND (flash_sale = 0)) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double)) M_1_Organic_Order
, ("sum"((CASE WHEN ((m_1 = 1) AND (flash_sale = 1)) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double)) M_1_CFS_Order
, ("sum"((CASE WHEN ((m_1 = 1) AND (seller_voucher = 1)) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double)) M_1_Seller_Voucher_Order
, ("sum"((CASE WHEN ((m_1 = 1) AND (shopee_voucher = 1)) THEN pp ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double)) M_1_Shopee_Voucher_Order
, ("sum"((CASE WHEN (m_1 = 1) THEN gmv_usd ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double)) M_1_GMV_USD
, ("sum"((CASE WHEN (wtd = 1) THEN pp ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double)) WTD_ADO
, ("sum"((CASE WHEN (w_1 = 1) THEN pp ELSE 0 END)) / DECIMAL '7.000') W_1_ADO
, ("sum"((CASE WHEN (w_2 = 1) THEN pp ELSE 0 END)) / DECIMAL '7.000') W_2_ADO
, ("sum"((CASE WHEN ((wtd = 1) AND (flash_sale = 1)) THEN pp ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double)) WTD_CFS_Order
, ("sum"((CASE WHEN ((w_1 = 1) AND (flash_sale = 1)) THEN pp ELSE 0 END)) / DECIMAL '7.000') W_1_CFS_Order
, ("sum"((CASE WHEN (((wtd = 1)) AND (flash_sale = 0)) THEN pp ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double)) WTD_Organic_Order
, ("sum"((CASE WHEN (((w_1 = 1)) AND (flash_sale = 0)) THEN pp ELSE 0 END)) / DECIMAL '7.000') W_1_Organic_Order
, ("sum"((CASE WHEN ((wtd = 1) AND (shopee_voucher = 1)) THEN pp ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double)) WTD_Shopee_Voucher_Order
, ("sum"((CASE WHEN ((w_1 = 1) AND (shopee_voucher = 1)) THEN pp ELSE 0 END)) / DECIMAL '7.000') W_1_Shopee_Voucher_Order
, ("sum"((CASE WHEN ((wtd = 1) AND (seller_voucher = 1)) THEN pp ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double)) WTD_Seller_Voucher_Order
, ("sum"((CASE WHEN ((w_1 = 1) AND (seller_voucher = 1)) THEN pp ELSE 0 END)) / DECIMAL '7.000') W_1_Seller_Voucher_Order
, ("sum"((CASE WHEN (wtd = 1) THEN gmv_usd ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double)) WTD_GMV_USD
, ("sum"((CASE WHEN (w_1 = 1) THEN gmv_usd ELSE 0 END)) / DECIMAL '7.000') W_1_GMV_USD
, ("sum"((CASE WHEN (w_2 = 1) THEN gmv_usd ELSE 0 END)) / DECIMAL '7.000') W_2_GMV_USD
FROM
sales_raw
GROUP BY 1, 2, 3, 4, 5
) 




, user_info AS (
SELECT
u.shop_id
, user_name username
, country_rank
, u.grass_region
, u.rating_star
, u.is_holiday_mode
, u.status
FROM
(mp_user.dim_shop__reg_s0_live u
INNER JOIN sales t ON (u.shop_id = t.shop_id))
WHERE u.tz_type = 'local'
AND u.grass_date = current_date - interval '1' day
) 


, total_sku AS (
SELECT DISTINCT
u.shop_id
, "count"(DISTINCT item_id) total_sku
FROM
(mp_item.dim_item__reg_s0_live i
INNER JOIN user_info u ON (i.shop_id = u.shop_id))
WHERE grass_date = current_date - interval '1' day
AND tz_type = 'local'
GROUP BY 1
) 

, traffic AS (
SELECT
exposure.shop_id
, ("sum"((CASE WHEN (exposure.grass_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN pv_cnt_1d ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double)) MTD_shop_view
, ("sum"((CASE WHEN ("date_format"(exposure.grass_date, '%y%m') = "date_format"("date_add"('month', -1, "date"(current_date)), '%y%m')) THEN pv_cnt_1d ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double)) M_1_shop_view
, ("sum"((CASE WHEN (exposure.grass_date BETWEEN "date_trunc"('week', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN pv_cnt_1d ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double)) WTD_shop_view
, ("sum"((CASE WHEN (exposure.grass_date BETWEEN "date_add"('week', -1, "date_trunc"('week', current_date)) AND "date_add"('day', -1, "date_trunc"('week', current_date))) THEN pv_cnt_1d ELSE 0 END)) / DECIMAL '7.000') W_1_shop_view
, ("sum"((CASE WHEN (exposure.grass_date BETWEEN "date_add"('week', -2, "date_trunc"('week', current_date)) AND "date_add"('day', -8, "date_trunc"('week', current_date))) THEN pv_cnt_1d ELSE 0 END)) / DECIMAL '7.000') W_2_shop_view
FROM
(mp_shopflow.dws_shop_view_1d__reg_s0_live exposure
INNER JOIN sales t ON (exposure.shop_id = t.shop_id))
WHERE tz_type = 'local'
AND exposure.grass_date >= date_add('month',-1,date_trunc('month',current_date - interval '1' day))
GROUP BY 1
) 

, accum_order AS (
SELECT
o.shop_id
, placed_order_cnt_td accum_orders
FROM
(mp_order.dws_seller_gmv_td__reg_s0_live o
INNER JOIN sales t ON (o.shop_id = t.shop_id))
WHERE o.tz_type = 'local' AND o.grass_date = current_date - interval '1' day
) 


SELECT
b.grass_date
, u.grass_region
, u.country_rank
, u.shop_id oversea_shopid
, u.username oversea_shopname
, b.LD_orders d_1_order
, b.MTD_ADO mtd_order
, b.M_1_ADO lm_order
, b.MTD_CFS_Order mtd_cfs
, b.M_1_CFS_Order lm_cfs
, b.MTD_Organic_Order mtd_organic
, b.M_1_Organic_Order lm_organic
, b.MTD_Seller_Voucher_Order mtd_seller_voucher
, b.M_1_Seller_Voucher_Order lm_seller_voucher
, b.MTD_Shopee_Voucher_Order mtd_shopee_voucher
, b.M_1_Shopee_Voucher_Order lm_shopee_voucher
, b.MTD_GMV_USD mtd_gmv_usd
, b.M_1_GMV_USD lm_gmv_usd
, d.MTD_shop_view mtd_unique_views
, d.M_1_shop_view lm_unique_views
, b.WTD_ADO wtd_order
, b.W_1_ADO lw_order
, B.W_2_ADO w_2_order
, b.WTD_CFS_Order wtd_cfs
, b.W_1_CFS_Order lw_cfs
, b.WTD_Organic_Order wtd_organic
, b.W_1_Organic_Order lw_organic
, b.WTD_Shopee_Voucher_Order wtd_shopee_voucher
, b.W_1_Shopee_Voucher_Order lw_shopee_voucher
, b.WTD_Seller_Voucher_Order wtd_seller_voucher
, b.W_1_Seller_Voucher_Order lw_seller_voucher
-- , u.live_sku live_sku
, c.total_sku total_sku
, b.WTD_GMV_USD wtd_gmv_usd
, b.W_1_GMV_USD w_1_gmv_usd
, d.WTD_shop_view wtd_views
, d.W_1_shop_view lw_views
, b.W_2_GMV_USD w_2_gmv_usd
, d.W_2_shop_view w_2_views
, u.is_holiday_mode
, u.rating_star
, a.accum_orders
, active_item_with_stock_cnt live_sku
, ua.follower_cnt_td
FROM
((((user_info u
LEFT JOIN sales b ON (u.shop_id = b.shop_id))
LEFT JOIN total_sku c ON (u.shop_id = c.shop_id))
LEFT JOIN traffic d ON (u.shop_id = d.shop_id))
LEFT JOIN accum_order a ON (u.shop_id = a.shop_id))
LEFT JOIN (
SELECT DISTINCT shop_id
, follower_cnt_td
FROM mp_user.dws_shop_interaction_td__reg_s0_live 
WHERE grass_date = current_date - interval '1' day
AND tz_type = 'local'
)ua on u.shop_id = ua.shop_id
LEFT JOIN (
SELECT DISTINCT shop_id
, active_item_with_stock_cnt
FROM mp_item.dws_shop_listing_td__reg_s0_live 
WHERE grass_date = current_date - interval '1' day
AND tz_type = 'local'
)ub on u.shop_id = ub.shop_id
ORDER BY 2 ASC, 3 ASC