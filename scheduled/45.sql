with zz as
(
SELECT
date(from_unixtime(create_timestamp)) as grass_date
, buyer_id as userid
, cast(count(distinct order_id) as double) as orders
, sum(gmv) as gmv
, cast(count(distinct case when order_be_status = 'INVALID' then order_id end) as double) as inv_orders
, sum(case when order_be_status = 'INVALID' then gmv end) as inv_gmv
FROM mp_order.dwd_order_item_all_ent_df__reg_s0_live
WHERE tz_type = 'local'
and grass_region = 'MY'
and grass_date >= date_add('month', -1, date_trunc('month', current_date))
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)
group by 1, 2
)


, yy as
(
SELECT zz.*, inv_gmv/gmv as pct_inv_gmv, inv_orders/orders as pct_inv_orders
FROM zz
)


, xx as
(
SELECT distinct grass_date, userid
FROM yy
WHERE pct_inv_gmv >= 0.8
and pct_inv_orders >= 0.8
and inv_gmv >= 10000
)


, aa as
(
SELECT distinct orderid
FROM xx
join
(
select distinct
order_id as orderid
, date(from_unixtime(create_timestamp)) as grass_date
, buyer_id as userid
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where tz_type = 'local'
and grass_region = 'MY'
and grass_date >= date_add('month', -1, date_trunc('month', current_date))
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)
) ww on xx.grass_date = ww.grass_date and xx.userid = ww.userid
) */


select
to_char(c.grass_date, 'yyyy-mm') as p_month
, c.grass_region , count(distinct c.orderid) as country_order
, count(distinct case when is_cb_shop = 1 then c.orderid else NULL end) as cb_order
, count(distinct case when is_cb_shop = 1 and is_net_order=1 then c.orderid else NULL end) as cb_order_net
, count(distinct case when is_cb_shop = 1 and merchant_region in ('CN','HK') then c.orderid else NULL end) as cncb_order
, count(distinct case when is_cb_shop = 1 and is_net_order=1 and merchant_region in ('CN','HK') then c.orderid else NULL end) as cncb_net_order
, count(distinct case when is_cb_shop = 1 and merchant_region = 'KR' then c.orderid else NULL end) as krcb_order
, count(distinct case when is_cb_shop = 1 and is_net_order=1 and merchant_region = 'KR' then c.orderid else NULL end) as krcb_net_order
, sum(gmv_usd) as country_gmv
, sum(case when is_cb_shop = 1 then gmv_usd else 0 end) as cb_gmv_usd
, sum(case when is_cb_shop = 1 and merchant_region in ('CN','HK') then gmv_usd else 0 end) as cncb_gmv
, sum(case when is_cb_shop = 1 and merchant_region in ('CN','HK') then nmv_usd else 0 end) as cncb_nmv
, sum(case when is_cb_shop = 1 and merchant_region = 'KR' then gmv_usd else 0 end) as krcb_gmv
, sum(case when is_cb_shop = 1 and merchant_region = 'KR' then nmv_usd else 0 end) as krcb_nmv
, sum(case when is_cb_shop = 1 and merchant_region in ('CN','HK') and is_net_order=1 then comm_base else 0 end) as cncb_net_comm_base
, sum(case when is_cb_shop = 1 then comm_fee else 0 end) as cb_comm_fee
, sum(case when is_net_order=1 and is_cb_shop = 1 then comm_fee else 0 end) as cb_net_comm_fee
, sum(case when is_cb_shop = 1 and merchant_region = 'KR' and is_net_order=1 then comm_base else 0 end) as krcb_net_comm_base
, sum(case when is_cb_shop = 1 and merchant_region = 'KR' then comm_fee else 0 end) as kr_comm_fee
, sum(case when is_net_order=1 and merchant_region = 'KR' and is_cb_shop = 1 then comm_fee else 0 end) as kr_net_comm_fee
, sum(case when is_cb_shop = 1 and merchant_region in ('CN','HK') then seller_txn_fee else 0 end) as cncb_seller_txn_fee
, sum(case when is_net_order=1 and merchant_region in ('CN','HK') and is_cb_shop = 1 then seller_txn_fee else 0 end) as cncb_net_seller_txn_fee
, sum(case when is_cb_shop = 1 and merchant_region = 'KR' then seller_txn_fee else 0 end) as krcb_seller_txn_fee
, sum(case when is_net_order=1 and merchant_region = 'KR' and is_cb_shop = 1 then seller_txn_fee else 0 end) as krcb_seller_net_txn_fee
, sum(case when is_cb_shop = 1 and merchant_region in ('CN','HK') then buyer_txn_fee else 0 end) as cncb_buyer_txn_fee
, sum(case when is_net_order=1 and merchant_region in ('CN','HK') and is_cb_shop = 1 then buyer_txn_fee else 0 end) as cncb_net_buyer_txn_fee
, sum(case when is_cb_shop = 1 and merchant_region = 'KR' then buyer_txn_fee else 0 end) as krcb_buyer_txn_fee
, sum(case when is_net_order=1 and merchant_region = 'KR' and is_cb_shop = 1 then buyer_txn_fee else 0 end) as krcb_buyer_net_txn_fee
from
(
select
order_id as orderid
, is_net_order
, shop_id as shopid
, grass_region
, date(from_unixtime(create_timestamp)) as grass_date
, is_cb_shop
, sum(commission_base_amt_usd) as comm_base
, sum(commission_fee_usd) as comm_fee
, sum(gmv_usd) as gmv_usd
, sum(nmv_usd) as nmv_usd
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where tz_type = 'local'
and grass_date >= date_add('day', -100, current_date)
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)
and is_bi_excluded = 0
group by 1, 2, 3, 4, 5, 6
) c
left join
(
select order_id as orderid
-----exclude those Spaylater handling fee, which should under shopee credit biz
, case when (grass_region='ID' and payment_be_channel_id IN (85001,85002,85003,85004,85005,85006,85010,85011,85012,85013,85014) )
OR (grass_region='SG' and payment_spm_channel_id IN (1002601) )
OR (grass_region='PH' and payment_spm_channel_id IN (4002601) )
OR (grass_region='TH' and payment_method_id IN (24) ) 
OR (grass_region='MY' and payment_method = 'PAY_SHOPEE_CREDIT' ) then 0 else buyer_txn_fee_usd end as buyer_txn_fee
, case when (grass_region='SG' and payment_spm_channel_id IN (1002601) )
OR (grass_region='PH' and payment_spm_channel_id IN (4002601) )
OR (grass_region='TH' and payment_method_id IN (24) ) 
OR (grass_region='MY' and payment_method = 'PAY_SHOPEE_CREDIT' ) then 0 else seller_txn_fee_usd end as seller_txn_fee
from mp_order.dwd_order_all_ent_df__reg_s0_live
where tz_type = 'local'
and grass_date >= date_add('month', -1, date_trunc('month', current_date))
and date(from_unixtime(create_timestamp)) between date_add('month', -1, date_trunc('month', current_date)) and date_add('day', -1, current_date)
and is_bi_excluded = 0
--and grass_date >= date_add('day', -100, current_date)
) c1 on c.orderid = c1.orderid
left join
(SELECT distinct merchant_region, shop_id FROM dev_regcbbi_general.cb_seller_profile_reg WHERE grass_region != '') b on c.shopid = b.shop_id
--left join aa on c.orderid = aa.orderid
--where aa.orderid is null
group by 1, 2