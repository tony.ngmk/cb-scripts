insert into nexus.test_ee3101136503534937c69b6f2cabb6698ed80eac81043f32fdf880cb35ba4a09_486cd808706a6754e0a99e995cb53e58 --- shop --- live sku --- live model --- model edit y/n y--- price up/down --- avg 


with t as (select distinct cast(affi_shopid as BIGINT) affi_shopid 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0
where batch='batch2'
)
, before as (SELECT DISTINCT item_id
, model_id
, CASE WHEN m.model_price < m.model_price_before_discount THEN m.model_price END AS model_dp
, model_price model_dp_v2
, model_price_before_discount
FROM mp_item.dim_model__reg_s0_live m 
join t on t.affi_shopid = m.shop_id 
WHERE m.tz_type = 'local' and m.is_cb_shop = 1 and m.is_holiday_mode = 0 
and m.seller_status = 1 and m.shop_status = 1 and m.item_status = 1 and model_status = 1 
and m.grass_date = DATE('2022-03-06') --- one day before offboarded
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
)
--select * from before 


SELECT DISTINCT
m.shop_id 
, count(distinct m.item_id) live_sku 
, count(distinct m.model_id) live_model 
, count(distinct CASE WHEN m.model_price < m.model_price_before_discount THEN m.model_id END ) edit_dp 
, COUNT(distinct CASE WHEN m.model_price = m.model_price_before_discount THEN m.model_id END) AS model_need_dp




--, count(distinct case when m.model_price_before_discount > b.model_price_before_discount then m.model_id else null end ) up_edit_of_op
--, count(distinct case when m.model_price_before_discount = b.model_price_before_discount then m.model_id else null end ) same_edit_of_op
--, count(distinct case when m.model_price_before_discount < b.model_price_before_discount then m.model_id else null end ) down_edit_of_op


, count(distinct case when m.model_price > b.model_dp_v2 then m.model_id else null end ) up_edit 


, count(distinct case when m.model_price = b.model_dp_v2 then m.model_id else null end ) same_edit
, count(distinct case when m.model_price < b.model_dp_v2 then m.model_id else null end ) down_edit
, avg(b.model_dp_v2) before_model_price 
, avg(m.model_price) avg_model_price 


, case when avg(m.model_price)> avg(b.model_dp_v2) then 'TRUE' Else 'FALSE' end as avg_up
FROM (select distinct 
item_id 
, shop_id 
, model_id 
, model_price
, model_price_before_discount
from 
mp_item.dim_model__reg_s0_live m 
WHERE m.tz_type = 'local' 
and m.seller_status = 1 and m.shop_status = 1 and model_status = 1 and m.item_status = 1 and m.is_cb_shop = 1 and m.is_holiday_mode = 0 
and m.grass_date = current_date-interval'2'day 
and m.grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) m 
join before b on b.item_id = m.item_id and b.model_id = m.model_id 
join t on t.affi_shopid = m.shop_id 


group by 1