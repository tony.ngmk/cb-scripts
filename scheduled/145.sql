WITH focus AS (
SELECT map.affi_country
, map.affi_shopid
, map.affi_itemid
, a_i.item_name affi_item_name
, p_i.name mst_item_name
, map.mst_itemid 
, map.mst_shopid
, sub_cat
, l30d_ado
FROM (
SELECT item_id 
, name item_name
, shop_id
, level2_global_be_category_id sub_cat 
FROM mp_item.dim_item__reg_s0_live
WHERE status = 1 
AND shop_status = 1
AND seller_status = 1
AND stock > 0
AND is_holiday_mode = 0
AND is_cb_shop = 1
AND grass_region IN ('TW')
AND grass_date = current_date - interval '1' day 
AND tz_type = 'local'
) a_i
INNER JOIN (
SELECT CAST(affi_shopid AS bigint) affi_shopid
FROM regcbbi_others.sip__hps_shop_tw__df__reg__s0
) a_s ON a_s.affi_shopid = a_i.shop_id
INNER JOIN (
SELECT affi_itemid
, affi_shopid
, mst_itemid
, mst_shopid
, affi_country
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live
WHERE affi_country = 'TW'
) map ON a_i.item_id = map.affi_itemid
INNER JOIN (
SELECT item_id, name 
FROM mp_item.dim_item__reg_s0_live
WHERE status = 1 
AND shop_status = 1
AND seller_status = 1
AND stock > 0
AND is_holiday_mode = 0
AND grass_region IN ('MY')
AND is_cb_shop = 1
AND grass_date = current_date - interval '1' day 
AND tz_type = 'local'
) p_i ON p_i.item_id = map.mst_itemid
-- LEFT JOIN (
-- SELECT itemid
-- FROM marketplace.shopee_listing_qc_backend_tw_db__screening_qclog__reg_daily_s0_live
-- WHERE status IN (2,4,5)
-- AND classification = 0
-- ) ex_i ON ex_i.itemid = a_i.item_id 
LEFT JOIN (
SELECT item_id
, sum(order_fraction) / 30.00 l30d_ado
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
WHERE is_placed = 1 
AND tz_type = 'local'
AND is_cb_shop = 1
AND grass_region IN ('TW')
AND grass_date >= current_date - interval '30' day
GROUP BY 1
) o ON o.item_id = a_i.item_id
-- WHERE ex_i.itemid IS NULL
)


, ex AS (
SELECT DISTINCT 
auditid AS audit_id
, itemid 
, shopid 
, country 
, name upload_option
, new AS upload_content
, try(json_extract_scalar(try(from_utf8(data)),'$.Operator')) AS operator
, try(json_extract_scalar(try(from_utf8(data)),'$.Source')) AS source
, CAST(grass_date as date) ctime
FROM 
marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
CROSS JOIN UNNEST(
CAST(
try(json_extract(try(from_utf8(data)),'$.Fields')) AS ARRAY(
ROW(name VARCHAR, old VARCHAR, new VARCHAR)
)
)
)
WHERE (try(json_extract_scalar(try(from_utf8(data)),'$.Operator')) IN ('zhixian.toh@shopee.com') 
OR try(json_extract_scalar(try(from_utf8(data)),'$.Source')) = 'Seller Center Single')
AND name IN ('name')
AND country = 'TW' --change countries
AND CAST(grass_date as date) BETWEEN current_date - interval '120' day AND current_date - interval '1' day 
)


-- , ph AS (
-- SELECT affi_itemid 
-- , mst_itemid
-- , item_name
-- , CONCAT('http://shopee.ph/product/',cast(m.affi_shopid AS VARCHAR),'/',cast(i.item_id AS VARCHAR)) AS eng_reference_link
-- FROM (
-- SELECT item_id
-- , name item_name
-- FROM mp_item.dim_item__reg_s0_live
-- WHERE grass_date = current_date - interval '1' day 
-- AND tz_type = 'local'
-- AND is_cb_shop = 1
-- AND grass_region = 'PH'
-- AND status = 1
-- ) i 
-- INNER JOIN marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live m
-- ON i.item_id = m.affi_itemid
-- WHERE affi_country = 'PH'
-- )


-- , sg AS (
-- SELECT affi_itemid 
-- , mst_itemid
-- , item_name
-- , CONCAT('http://shopee.sg/product/',cast(m.affi_shopid AS VARCHAR),'/',cast(i.item_id AS VARCHAR)) AS eng_reference_link
-- FROM (
-- SELECT item_id
-- , name item_name
-- FROM mp_item.dim_item__reg_s0_live
-- WHERE grass_date = current_date - interval '1' day 
-- AND tz_type = 'local'
-- AND is_cb_shop = 1
-- AND grass_region = 'SG'
-- AND status = 1
-- ) i 
-- INNER JOIN marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live m
-- ON i.item_id = m.affi_itemid
-- WHERE affi_country = 'SG'
-- )


-- , my AS (
-- SELECT affi_itemid 
-- , mst_itemid
-- , item_name
-- , CONCAT('http://shopee.com.my/product/',cast(m.affi_shopid AS VARCHAR),'/',cast(i.item_id AS VARCHAR)) AS eng_reference_link
-- FROM (
-- SELECT item_id
-- , name item_name
-- FROM mp_item.dim_item__reg_s0_live
-- WHERE grass_date = current_date - interval '1' day 
-- AND tz_type = 'local'
-- AND is_cb_shop = 1
-- AND grass_region = 'MY'
-- AND status = 1
-- ) i 
-- INNER JOIN marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live m
-- ON i.item_id = m.affi_itemid
-- WHERE affi_country = 'MY'
-- )


, summary AS (
SELECT f.affi_country
, f.affi_shopid
, f.affi_itemid
, CONCAT('https://shopee.tw/product/',cast(f.affi_shopid AS VARCHAR),'/',cast(f.affi_itemid AS VARCHAR)) AS affi_product_link
, f.affi_item_name
, f.mst_item_name
, f.sub_cat
, f.l30d_ado affi_l30d_ado
-- , CASE WHEN ph.affi_itemid IS NOT NULL THEN ph.item_name
-- WHEN ph.affi_itemid IS NULL AND sg.affi_itemid IS NOT NULL THEN sg.item_name
-- WHEN ph.affi_itemid IS NULL AND sg.affi_itemid IS NULL AND my.affi_itemid IS NOT NULL THEN my.item_name
-- ELSE NULL END english_name
-- , CASE WHEN ph.affi_itemid IS NOT NULL THEN ph.eng_reference_link
-- WHEN ph.affi_itemid IS NULL AND sg.affi_itemid IS NOT NULL THEN sg.eng_reference_link
-- WHEN ph.affi_itemid IS NULL AND sg.affi_itemid IS NULL AND my.affi_itemid IS NOT NULL THEN my.eng_reference_link
-- ELSE NULL END eng_reference_link
, CONCAT('http://shopee.my/product/',cast(f.mst_shopid AS VARCHAR),'/',cast(f.mst_itemid AS VARCHAR)) AS mst_product_link
FROM focus f
LEFT JOIN ex ON ex.itemid = f.affi_itemid
LEFT JOIN regcbbi_others.sip__listing_optimization_bau_exclude_list__wf__reg__s0 ex1 ON CAST(ex1.affi_itemid AS bigint) = f.affi_itemid
-- LEFT JOIN ph ON ph.mst_itemid = f.mst_itemid
-- LEFT JOIN sg ON sg.mst_itemid = f.mst_itemid
-- LEFT JOIN my ON my.mst_itemid = f.mst_itemid
WHERE ex.itemid IS NULL AND ex1.affi_itemid IS NULL
)


SELECT affi_country
, affi_shopid
, affi_itemid
, affi_product_link
, affi_item_name
, '' affi_description
, mst_product_link
, mst_item_name
-- , english_name
-- , eng_reference_link
, '' Priority
, '' Human_translated_title_local_language
, '' Keyword
, '' Human_translated_description_EN
, '' Machine_translation_score_Title
, '' Machine_translation_score_Description
, '' Title_length_Y_N
, '' Description_length_Y_N
, '' Confirm_to_upload_Y_N
, '' Remark_for_N_reason
, '' Other_remarks
, sub_cat
, affi_l30d_ado
, rank() OVER (PARTITION BY affi_country ORDER BY affi_l30d_ado DESC) ado_rank
FROM summary
-- WHERE english_name IS NOT NULL
ORDER BY affi_country, affi_l30d_ado DESC
LIMIT 1000