WITH sip AS
(
SELECT affi_shopid ,
mst_shopid ,
t1.country AS mst_country ,
t2.country AS affi_country ,
t1.cb_option ,
offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN
(
SELECT affi_shopid ,
affi_username ,
mst_shopid ,
country ,
date'2022-06-13' offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status=3
AND grass_region IN ('SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES',
'FR')
AND date(from_unixtime(offboard_time)) != date'2022-01-11' ) t2
ON t1.shopid = t2.mst_shopid
)
,
t AS
(
SELECT sip.affi_shopid ,
date(offboard_time) offboard_time ,
batch ,
mst_shopid
FROM regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a
JOIN sip
ON sip.affi_shopid = cast(a.affi_shopid AS bigint)
WHERE a.batch IN ('mass return batch1')
)
-- select *
-- from t
,
cb AS
(
SELECT DISTINCT (
CASE
WHEN lower(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN lower(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail'
WHEN lower(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN (
lower(child_account_owner_userrole_name) LIKE '%lt%'
OR lower(child_account_owner_userrole_name) LIKE '%css%') THEN 'Long Tail'
ELSE 'Others'
END) seller_type ,
ggp_account_name ,
gp_account_name ,
gp_account_owner_userrole_name ,
gp_account_seller_classification ,
gp_account_owner ,
grass_region ,
child_account_name ,
child_account_owner ,
cast(child_shopid AS bigint) child_shopid
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
)
-- AND is_flash_sale = 0 AND item_promotion_source <> 'shopee' --organic orders only
,
traffic_by_day AS
( --L30D/
SELECT
CASE
WHEN grass_date < date'2022-06-13' THEN 'before'
WHEN grass_date BETWEEN date'2022-06-13' AND date'2022-06-13' + interval'1'day THEN 'return'
ELSE 'after'
END AS time_range
-- , case when grass_date < date'2021-12-30' then concat('D-',cast(date_diff('day', grass_date, date'2021-12-30' ) as varchar ))
-- when grass_date =date'2021-12-30' then 'D-0'
-- when grass_date = date'2021-12-30' + interval'1'day then 'D+0'
-- else concat('D+',cast(date_diff('day', date'2021-12-30' +interval'1'day , grass_date) as varchar )) end as period
,
seller_type
-- , tt.grass_region
,
shop_id ,
tt.grass_region ,
t.batch ,
sum(tt.imp_cnt_1d) AS imp_cnt_1d ,
sum(tt.pv_cnt_1d) AS pv_cnt_1d ,
sum(tt.clk_cnt_1d) AS clk_cnt_1d
FROM (
SELECT DISTINCT grass_date,
grass_region,
shop_id,
item_id,
imp_cnt_1d,
pv_cnt_1d,
clk_cnt_1d
FROM mp_shopflow.dws_item_civ_1d__reg_s0_live a
JOIN sip
ON affi_shopid = a.shop_id
JOIN t
ON t.affi_shopid = sip.affi_shopid
WHERE tz_type = 'local'
AND grass_date BETWEEN t.offboard_time- interval'6'day AND t.offboard_time + interval'30'day
AND grass_date NOT IN ( date'2022-05-05',
date'2022-06-06',
date'2022-07-07')
AND grass_region IN ('SG',
'MY',
'PH',
'TH',
'ID',
'VN',
'TW',
'MX',
'BR',
'CO',
'CL',
'PL',
'ES') ) tt
INNER JOIN sip
ON (
sip.affi_shopid = tt.shop_id)
JOIN t
ON t.affi_shopid = tt.shop_id
LEFT JOIN cb
ON cb.child_shopid = t.mst_shopid
GROUP BY 1,
2,
3 ,
4 ,
5
)
,
day_diff AS
(
SELECT count(DISTINCT date_id) - 2 day_diff
FROM regbida_keyreports.dim_date
WHERE date_id NOT IN ( date'2022-05-05',
date'2022-06-06',
date'2022-07-07')
AND date_id BETWEEN date'2022-06-13' AND CURRENT_DATE-interval'1'day
)SELECT batch ,
seller_type ,
grass_region ,
Sum(
CASE
WHEN time_range='before' THEN imp_cnt_1d
ELSE NULL
END)/6.00 bef_imp ,
Sum(
CASE
WHEN time_range='before' THEN pv_cnt_1d
ELSE NULL
END)/6.00 bef_pv ,
Sum(
CASE
WHEN time_range='before' THEN clk_cnt_1d
ELSE NULL
END)/6.00 bef_clk ,
Sum(
CASE
WHEN time_range='after' THEN imp_cnt_1d
ELSE NULL
END) * decimal'1.000' /day_diff aft_imp ,
sum(
CASE
WHEN time_range='after' THEN pv_cnt_1d
ELSE NULL
END)* decimal'1.000' /day_diff aft_pv ,
sum(
CASE
WHEN time_range='after' THEN clk_cnt_1d
ELSE NULL
END)* decimal'1.000' /day_diff aft_clk
FROM traffic_by_day
JOIN day_diff
ON 1=1
GROUP BY 1,
2 ,
3 ,
day_diff