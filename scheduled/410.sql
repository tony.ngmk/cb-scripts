insert into nexus.test_36ae26f2d0e2877b53f2da164d6a11184336cbc0c9ee372c69e87bb79d07ee59_5efc42bcc5b3fe8a046ecb8154519951 WITH
raw AS (
SELECT
a.order_id as orderid
, a.item_id as itemid
, a.model_id as modelid
, a.gmv_usd
, a.grass_date purchase_date
, a.level1_category as main_category
, is_cb_shop
, item_price_pp_usd
, item_promotion_source
-- , a.status_ext
, a.order_fraction pp
, grass_region
FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live a
where grass_region='BR' and is_placed=1 and grass_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -2, current_date) and is_bi_excluded=0
and grass_date not in (select distinct date(spike_date) as spike_date from regcb_bi.mkt__spikeday__di__br_s0)
) 


, item as (
select item_id, is_cb_shop , level1_category as main_category,status as item_status, seller_status, shop_status, is_holiday_mode, create_datetime, stock,shop_id
from mp_item.dim_item__reg_s0_live
where grass_region='BR' and grass_date=date_add('day',-1,current_date) and tz_type='local')


, spike as (
select 'BR' as grass_region
, sum(case when spike_date between date_trunc('week',date_add('day', -2, current_date)) and date_add('day', -2, current_date) then 1 else 0 end) as wtd_d
, sum(case when spike_date between date_trunc('week',date_add('day', -2, current_date)) - interval '7'day and date_trunc('week',date_add('day', -2, current_date)) - interval '1'day then 1 else 0 end) w1_d
, sum(case when spike_date between date_trunc('month',date_add('day', -2, current_date)) and date_add('day', -2, current_date) then 1 else 0 end) as mtd_d
, sum(case when spike_date between date_trunc('month',date_add('day', -2, current_date)) - interval '1'month and date_trunc('month',date_add('day', -2, current_date)) - interval '1'day then 1 else 0 end) m1_d
from (select distinct date(spike_date) as spike_date from regcb_bi.mkt__spikeday__di__br_s0)
)
,map as
(
SELECT distinct
cast(grass_region as varchar) grass_region
, cast(l1_cat_id as int) l1_cat_id
, cast(l1_cat as varchar) as l1_cat
, cast(cluster as varchar) as cluster
FROM regcbbi_others.shopee_regional_cb_team__regional_cb_cluster_mapping
WHERE ingestion_timestamp!='1' and CAST(ingestion_timestamp AS bigint) = (SELECT max(CAST(ingestion_timestamp AS bigint)) aa FROM regcbbi_others.shopee_regional_cb_team__regional_cb_cluster_mapping where ingestion_timestamp!='1' ) and cast(grass_region as varchar)='BR'
) 


, ado AS (
SELECT
grass_region,main_category
, try(("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN pp ELSE 0 END))*1.0000 )) cb_WTD_ADO
, try(("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN pp ELSE 0 END))*1.0000 ) ) WTD_ADO
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week', "date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week', "date_add"('day', -2, current_date)))) and is_cb_shop=1 THEN pp ELSE 0 END)*1.0000) cb_W1_ADO
, try("sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week', "date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week', "date_add"('day', -2, current_date)))) THEN pp ELSE 0 END)*1.0000) W1_ADO
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN pp ELSE 0 END))*1.0000 cb_MTD_ADO
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN pp ELSE 0 END))) MTD_ADO
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and is_cb_shop=1 THEN pp ELSE 0 END))*1.0000 ) cb_M1_ADO
, try("sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) THEN pp ELSE 0 END))*1.0000 ) M1_ADO


, ("avg"(CASE WHEN (purchase_date BETWEEN date_trunc('week', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_WTD_ASP
, ("avg"(CASE WHEN (purchase_date BETWEEN date_trunc('week', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN item_price_pp_usd ELSE 0 END) ) WTD_ASP
, "avg"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week', "date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week', "date_add"('day', -2, current_date))) ) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END) cb_W1_ASP
, "avg"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week', "date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week', "date_add"('day', -2, current_date))) ) THEN item_price_pp_usd ELSE 0 END ) W1_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_MTD_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN item_price_pp_usd ELSE 0 END)) MTD_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_M1_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) THEN item_price_pp_usd ELSE 0 END)) M1_ASP

, ("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 and item_promotion_source='flash_sale' THEN pp ELSE 0 END))*1.0000 ) cb_WTD_ADO_cfs
, ("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and item_promotion_source='flash_sale' THEN pp ELSE 0 END))*1.0000 ) WTD_ADO_cfs
, "sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date)))) and item_promotion_source='flash_sale' and is_cb_shop=1 THEN pp ELSE 0 END)*1.0000 cb_W1_ADO_cfs
, "sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date)))) and item_promotion_source='flash_sale' THEN pp ELSE 0 END)*1.0000 W1_ADO_cfs
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and item_promotion_source='flash_sale' and is_cb_shop=1 THEN pp ELSE 0 END))*1.0000 cb_MTD_ADO_cfs
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and item_promotion_source='flash_sale' THEN pp ELSE 0 END)) MTD_ADO_cfs
, "sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and item_promotion_source='flash_sale' and is_cb_shop=1 THEN pp ELSE 0 END))*1.0000 cb_M1_ADO_cfs
, "sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and item_promotion_source='flash_sale' THEN pp ELSE 0 END))*1.0000 M1_ADO_cfs


FROM
raw
GROUP BY 1,2
) 


, tf_raw as (
select purchase_date,a.item_id,main_category,is_cb_shop,daily_imp,daily_vw,daily_clk
from
(SELECT
grass_date AS purchase_date,
shop_id,
item_id,
SUM(imp_cnt_1d) AS daily_imp,
SUM(pv_cnt_1d) AS daily_vw,
SUM(clk_cnt_1d) AS daily_clk
FROM mp_shopflow.dws_item_civ_1d__reg_s0_live--traffic_mart_dws__item_exposure_1d
WHERE
grass_region = 'BR'
AND grass_date between "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) and date_add('day',-2,current_date) and tz_type='local'
and grass_date not in (select distinct date(spike_date) as spike_date from regcb_bi.mkt__spikeday__di__br_s0)
GROUP BY 1, 2, 3
) a
left join item b on a.item_id=b.item_id
-- left join map on map.l1_cat_id=b.level1_category_id
-- group by 1,2
)


, tf as (
SELECT
main_category
, ("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN daily_imp ELSE 0 END))*1.0000 ) cb_WTD_imp 
, ("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN daily_imp ELSE 0 END))*1.0000 ) WTD_imp 
, "sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date))) ) and is_cb_shop=1 THEN daily_imp ELSE 0 END)*1.0000 cb_W1_imp 
, "sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date))) ) THEN daily_imp ELSE 0 END)*1.0000 W1_imp 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN daily_imp ELSE 0 END))*1.0000 cb_MTD_imp 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN daily_imp ELSE 0 END))*1.0000 MTD_imp 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and is_cb_shop=1 THEN daily_imp ELSE 0 END)) *1.0000 cb_M1_imp 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) THEN daily_imp ELSE 0 END)) *1.0000 M1_imp 


, ("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN daily_vw ELSE 0 END))*1.0000 ) cb_WTD_vw 
, ("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN daily_vw ELSE 0 END))*1.0000 ) WTD_vw 
, "sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date)))) and is_cb_shop=1 THEN daily_vw ELSE 0 END)*1.0000 cb_W1_vw 
, "sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date)))) THEN daily_vw ELSE 0 END)*1.0000 W1_vw 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN daily_vw ELSE 0 END)) *1.0000 cb_MTD_vw 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN daily_vw ELSE 0 END))*1.0000 MTD_vw 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and is_cb_shop=1 THEN daily_vw ELSE 0 END))*1.0000 cb_M1_vw 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) THEN daily_vw ELSE 0 END))*1.0000 M1_vw 


, ("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN daily_clk ELSE 0 END))*1.0000 ) cb_WTD_clk 
, ("sum"((CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN daily_clk ELSE 0 END))*1.0000 ) WTD_clk 
, "sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date))) ) and is_cb_shop=1 THEN daily_clk ELSE 0 END)*1.0000 cb_W1_clk 
, "sum"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date))) ) THEN daily_clk ELSE 0 END)*1.0000 W1_clk 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN daily_clk ELSE 0 END)) *1.0000 cb_MTD_clk 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN daily_clk ELSE 0 END))*1.0000 MTD_clk 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and is_cb_shop=1 THEN daily_clk ELSE 0 END))*1.0000 cb_M1_clk 
, "sum"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) THEN daily_clk ELSE 0 END))*1.0000 M1_clk 
FROM tf_raw
GROUP BY 1


)




, ads as (
SELECT
level1_category
, ("sum"((CASE WHEN (grass_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_seller=1 THEN order_cnt ELSE 0 END))*1.0000 ) cb_WTD_ADO_ads
, ("sum"((CASE WHEN (grass_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN order_cnt ELSE 0 END))*1.0000 ) WTD_ADO_ads
, "sum"(CASE WHEN (grass_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date)))) and is_cb_seller=1 THEN order_cnt ELSE 0 END)*1.0000 cb_W1_ADO_ads
, "sum"(CASE WHEN (grass_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date)))) THEN order_cnt ELSE 0 END)*1.0000 W1_ADO_ads
, "sum"((CASE WHEN (grass_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_seller=1 THEN order_cnt ELSE 0 END))*1.0000 cb_MTD_ADO_ads
, "sum"((CASE WHEN (grass_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN order_cnt ELSE 0 END)) MTD_ADO_ads
, "sum"((CASE WHEN (grass_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and is_cb_seller=1 THEN order_cnt ELSE 0 END))*1.0000 cb_M1_ADO_ads
, "sum"((CASE WHEN (grass_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) THEN order_cnt ELSE 0 END))*1.0000 M1_ADO_ads
FROM mp_paidads.ads_advertise_mkt_1d__reg_s0_live a
WHERE a.grass_date between "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) and date_add('day',-2,current_date)
AND a.tz_type = 'local'
AND a.grass_region='BR'
and grass_date not in (select distinct date(spike_date) as spike_date from regcb_bi.mkt__spikeday__di__br_s0)
GROUP BY 1
ORDER BY 1
)


, cfs as ( SELECT
level1_category
, count( distinct CASE WHEN date(from_unixtime(start_time)) BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date) and is_cb_seller=1 THEN slotid ELSE null END)*1.0000 cb_WTD_slot
, count( distinct CASE WHEN date(from_unixtime(start_time)) BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date) THEN slotid ELSE null END)*1.0000 WTD_slot
, count( distinct CASE WHEN date(from_unixtime(start_time)) BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date))) and is_cb_seller=1 THEN slotid ELSE null END)*1.0000 cb_W1_slot
, count( distinct CASE WHEN date(from_unixtime(start_time)) BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date))) THEN slotid ELSE null END)*1.0000 W1_slot
, count( distinct CASE WHEN date(from_unixtime(start_time)) BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date) and is_cb_seller=1 THEN slotid ELSE null END)*1.0000 cb_MTD_slot
, count( distinct CASE WHEN date(from_unixtime(start_time)) BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date) THEN slotid ELSE null END) MTD_slot
, count( distinct CASE WHEN date(from_unixtime(start_time)) BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) and is_cb_seller=1 THEN slotid ELSE null END)*1.0000 cb_M1_slot
, count( distinct CASE WHEN date(from_unixtime(start_time)) BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) THEN slotid ELSE null END)*1.0000 M1_slot
FROM
(SELECT distinct promotionid, start_time-11*3600 as start_time, fs.itemid, modelid, fs.stock, "concat"(cast(start_time as VARCHAR), cast(fs.itemid as VARCHAR)) slotid, is_cb_shop as is_cb_seller,main_category as level1_category
FROM
marketplace.shopee_promotion_backend_br_db__promotion_flash_sale_item_tab__reg_daily_s0_live fs
INNER JOIN item i ON (fs.itemid = i.item_id)
WHERE fs.status = 1 and "date"("from_unixtime"(start_time -11*3600)) between "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) and date_add('day',-2,current_date)
and "date"("from_unixtime"(start_time -11*3600)) not in (select distinct date(spike_date) as spike_date from regcb_bi.mkt__spikeday__di__br_s0)
)
GROUP BY 1
) 


, cat as (
select distinct
ado.main_category,
cb_WTD_ADO/(day_of_week(date_add('day', -2, current_date))-wtd_d) as cb_WTD_ADO,
WTD_ADO/(day_of_week("date_add"('day', -2, current_date))-wtd_d) as WTD_ADO,
cb_W1_ADO/(7-w1_d) as cb_W1_ADO,
W1_ADO/(7-w1_d) as W1_ADO,
cb_MTD_ADO/(day(date_add('day', -2, current_date))- mtd_d) as cb_MTD_ADO,
MTD_ADO/(day(date_add('day', -2, current_date))- mtd_d) as MTD_ADO,
cb_M1_ADO/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as cb_M1_ADO,
M1_ADO/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as M1_ADO,

cb_WTD_ADO_cfs/(day_of_week(date_add('day', -2, current_date))-wtd_d) as cb_WTD_ADO_cfs,
WTD_ADO_cfs/(day_of_week(date_add('day', -2, current_date))-wtd_d) as WTD_ADO_cfs,
cb_W1_ADO_cfs/(7-w1_d) as cb_W1_ADO_cfs,
W1_ADO_cfs/(7-w1_d) as W1_ADO_cfs,
cb_MTD_ADO_cfs/(day(date_add('day', -2, current_date))- mtd_d) as cb_MTD_ADO_cfs,
MTD_ADO_cfs/(day(date_add('day', -2, current_date))- mtd_d) as MTD_ADO_cfs,
cb_M1_ADO_cfs/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as cb_M1_ADO_cfs,
M1_ADO_cfs/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as M1_ADO_cfs,

cb_WTD_imp/(day_of_week(date_add('day', -2, current_date))-wtd_d) as cb_WTD_imp,
WTD_imp/(day_of_week(date_add('day', -2, current_date))-wtd_d) as WTD_imp,
cb_W1_imp/(7-w1_d) as cb_W1_imp,
W1_imp/(7-w1_d) as W1_imp,
cb_MTD_imp/(day(date_add('day', -2, current_date))- mtd_d) as cb_MTD_imp,
MTD_imp/(day(date_add('day', -2, current_date))- mtd_d) as MTD_imp,
cb_M1_imp/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as cb_M1_imp,
M1_imp/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as M1_imp,

cb_WTD_vw/(day_of_week(date_add('day', -2, current_date))-wtd_d) as cb_WTD_vw,
WTD_vw/(day_of_week(date_add('day', -2, current_date))-wtd_d) as WTD_vw,
cb_W1_vw/(7-w1_d) as cb_W1_vw,
W1_vw/(7-w1_d) as W1_vw,
cb_MTD_vw/(day(date_add('day', -2, current_date))- mtd_d) as cb_MTD_vw,
MTD_vw/(day(date_add('day', -2, current_date))- mtd_d) as MTD_vw,
cb_M1_vw/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as cb_M1_vw,
M1_vw/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as M1_vw,

cb_WTD_clk/(day_of_week(date_add('day', -2, current_date))-wtd_d) as cb_WTD_clk,
WTD_clk/(day_of_week(date_add('day', -2, current_date))-wtd_d) as WTD_clk,
cb_W1_clk/(7-w1_d) as cb_W1_clk,
W1_clk/(7-w1_d) as W1_clk,
cb_MTD_clk/(day(date_add('day', -2, current_date))- mtd_d) as cb_MTD_clk,
MTD_clk/(day(date_add('day', -2, current_date))- mtd_d) as MTD_clk,
cb_M1_clk/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as cb_M1_clk,
M1_clk/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as M1_clk,

cb_WTD_ADO_ads/(day_of_week(date_add('day', -2, current_date))-wtd_d) as cb_WTD_ADO_ads,
WTD_ADO_ads/(day_of_week(date_add('day', -2, current_date))-wtd_d) as WTD_ADO_ads,
cb_W1_ADO_ads/(7-w1_d) as cb_W1_ADO_ads,
W1_ADO_ads/(7-w1_d) as W1_ADO_ads,
cb_MTD_ADO_ads/(day(date_add('day', -2, current_date))- mtd_d) as cb_MTD_ADO_ads,
MTD_ADO_ads/(day(date_add('day', -2, current_date))- mtd_d) as MTD_ADO_ads,
cb_M1_ADO_ads/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as cb_M1_ADO_ads,
M1_ADO_ads/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as M1_ADO_ads,

cb_WTD_slot/(day_of_week(date_add('day', -2, current_date))-wtd_d) as cb_WTD_slot,
WTD_slot/(day_of_week(date_add('day', -2, current_date))-wtd_d) as WTD_slot,
cb_W1_slot/(7-w1_d) as cb_W1_slot,
W1_slot/(7-w1_d) as W1_slot,
cb_MTD_slot/(day(date_add('day', -2, current_date))- mtd_d) as cb_MTD_slot,
MTD_slot/(day(date_add('day', -2, current_date))- mtd_d) as MTD_slot,
cb_M1_slot/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as cb_M1_slot,
M1_slot/(day(date_add('day', -1, date_trunc('month', date_add('day', -2, current_date))))-m1_d) as M1_slot,


cb_WTD_ASP,
WTD_ASP,
cb_W1_ASP,
W1_ASP,
cb_MTD_ASP,
MTD_ASP,
cb_M1_ASP,
M1_ASP


FROM
ado
LEFT JOIN spike k on (k.grass_region=ado.grass_region)
LEFT JOIN tf t ON (t.main_category = ado.main_category)
LEFT JOIN ads s ON (s.level1_category = ado.main_category)
Left join cfs c on c.level1_category=ado.main_category
-- left join asp on asp.level1_category=ado.main_category
)


, total as (
select
sum(cb_WTD_ADO) as cb_WTD_ADO,
sum(WTD_ADO) as WTD_ADO,
sum(cb_W1_ADO) as cb_W1_ADO,
sum(W1_ADO) as W1_ADO,
sum(cb_MTD_ADO) as cb_MTD_ADO,
sum(MTD_ADO) as MTD_ADO,
sum(cb_M1_ADO) as cb_M1_ADO,
sum(M1_ADO) as M1_ADO,

sum(cb_WTD_ADO_cfs) as cb_WTD_ADO_cfs,
sum(WTD_ADO_cfs) as WTD_ADO_cfs,
sum(cb_W1_ADO_cfs) as cb_W1_ADO_cfs,
sum(W1_ADO_cfs) as W1_ADO_cfs,
sum(cb_MTD_ADO_cfs) as cb_MTD_ADO_cfs,
sum(MTD_ADO_cfs) as MTD_ADO_cfs,
sum(cb_M1_ADO_cfs) as cb_M1_ADO_cfs,
sum(M1_ADO_cfs) as M1_ADO_cfs,

sum(cb_WTD_imp) as cb_WTD_imp,
sum(WTD_imp) as WTD_imp,
sum(cb_W1_imp) as cb_W1_imp,
sum(W1_imp) as W1_imp,
sum(cb_MTD_imp) as cb_MTD_imp,
sum(MTD_imp) as MTD_imp,
sum(cb_M1_imp) as cb_M1_imp,
sum(M1_imp) as M1_imp,

sum(cb_WTD_vw) as cb_WTD_vw,
sum(WTD_vw) as WTD_vw,
sum(cb_W1_vw) as cb_W1_vw,
sum(W1_vw) as W1_vw,
sum(cb_MTD_vw) as cb_MTD_vw,
sum(MTD_vw) as MTD_vw,
sum(cb_M1_vw) as cb_M1_vw,
sum(M1_vw) as M1_vw,

sum(cb_WTD_clk) as cb_WTD_clk,
sum(WTD_clk) as WTD_clk,
sum(cb_W1_clk) as cb_W1_clk,
sum(W1_clk) as W1_clk,
sum(cb_MTD_clk) as cb_MTD_clk,
sum(MTD_clk) as MTD_clk,
sum(cb_M1_clk) as cb_M1_clk,
sum(M1_clk) as M1_clk,

sum(cb_WTD_ADO_ads) as cb_WTD_ADO_ads,
sum(WTD_ADO_ads) as WTD_ADO_ads,
sum(cb_W1_ADO_ads) as cb_W1_ADO_ads,
sum(W1_ADO_ads) as W1_ADO_ads,
sum(cb_MTD_ADO_ads) as cb_MTD_ADO_ads,
sum(MTD_ADO_ads) as MTD_ADO_ads,
sum(cb_M1_ADO_ads) as cb_M1_ADO_ads,
sum(M1_ADO_ads) as M1_ADO_ads,

sum(cb_WTD_slot) as cb_WTD_slot,
sum(WTD_slot) as WTD_slot,
sum(cb_W1_slot) as cb_W1_slot,
sum(W1_slot) as W1_slot,
sum(cb_MTD_slot) as cb_MTD_slot,
sum(MTD_slot) as MTD_slot,
sum(cb_M1_slot) as cb_M1_slot,
sum(M1_slot) as M1_slot


from cat
)


, asp as (
select ("avg"(CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_WTD_ASP
, ("avg"(CASE WHEN (purchase_date BETWEEN date_trunc('week',"date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN item_price_pp_usd ELSE 0 END) ) WTD_ASP
, "avg"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date))) ) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END) cb_W1_ASP
, "avg"(CASE WHEN (purchase_date BETWEEN date_add('day',-7,date_trunc('week',"date_add"('day', -2, current_date))) AND date_add('day',-1,date_trunc('week',"date_add"('day', -2, current_date))) ) THEN item_price_pp_usd ELSE 0 END ) W1_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_MTD_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_trunc"('month', "date_add"('day', -2, current_date)) AND "date_add"('day', -2, current_date)) THEN item_price_pp_usd ELSE 0 END)) MTD_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) and is_cb_shop=1 THEN item_price_pp_usd ELSE 0 END)) cb_M1_ASP
, "avg"((CASE WHEN (purchase_date BETWEEN "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -2, current_date))) AND "date_add"('day', -1, "date_trunc"('month', "date_add"('day', -2, current_date)))) THEN item_price_pp_usd ELSE 0 END)) M1_ASP
from raw
)

(SELECT
cluster, main_category
, cb_WTD_ADO
, cb_MTD_ADO
, try(cb_WTD_ADO/cb_W1_ADO) - 1 as cb_ado_wow
, try(cb_MTD_ADO/cb_M1_ADO) - 1 as cb_ado_mom
, try(cb_WTD_ADO/WTD_ADO) as ado_pene_wtd
, try(cb_MTD_ADO/MTD_ADO) as ado_pene_mtd
, try(cb_WTD_ADO/WTD_ADO )- try(cb_W1_ADO/W1_ADO) as ado_pene_wow
, try(cb_MTD_ADO/MTD_ADO) - try(cb_M1_ADO/M1_ADO) as ado_pene_mom

, cb_WTD_vw
, cb_MTD_vw
, try(cb_WTD_vw/cb_W1_vw) - 1 as cb_vw_wow
, try(cb_MTD_vw/cb_M1_vw) - 1 as cb_vw_mom
, try(cb_WTD_vw/WTD_vw) as vw_pene_wtd
, try(cb_MTD_vw/MTD_vw) as vw_pene_mtd
, try(cb_WTD_vw/WTD_vw) - try(cb_W1_vw/W1_vw) as vw_pene_wow
, try(cb_MTD_vw/MTD_vw) - try(cb_M1_vw/M1_vw) as vw_pene_mom

, cb_WTD_imp
, cb_MTD_imp
, try(cb_WTD_imp/cb_W1_imp) - 1 as cb_imp_wow
, try(cb_MTD_imp/cb_M1_imp) - 1 as cb_imp_mom
, try(cb_WTD_imp/WTD_imp) as imp_pene_wtd
, try(cb_MTD_imp/MTD_imp) as imp_pene_mtd
, try(cb_WTD_imp/WTD_imp) - try(cb_W1_imp/W1_imp) as imp_pene_wow
, try(cb_MTD_imp/MTD_imp) - try(cb_M1_imp/M1_imp) as imp_pene_mom

, try(cb_WTD_clk/cb_WTD_imp) as cb_WTD_ctr
, try(cb_MTD_clk/cb_MTD_imp) as cb_MTD_ctr
, try(cb_WTD_clk/cb_WTD_imp) - try(cb_W1_clk/cb_W1_imp) as cb_ctr_wow
, try(cb_MTD_clk/cb_MTD_imp) - try(cb_M1_clk/cb_M1_imp) as cb_ctr_mom
, try((cb_WTD_clk/cb_WTD_imp)/(WTD_clk/WTD_imp)) as cb_WTD_ctr_pene
, try((cb_MTD_clk/cb_MTD_imp)/try(MTD_clk/MTD_imp)) as cb_MTD_ctr_pene
, try((cb_WTD_clk/cb_WTD_imp)/try(WTD_clk/WTD_imp) )- try((cb_W1_clk/cb_W1_imp)/try(W1_clk/W1_imp)) as cb_ctr_pene_wow
, try((cb_MTD_clk/cb_MTD_imp)/try(MTD_clk/MTD_imp)) - try((cb_M1_clk/cb_M1_imp)/try(M1_clk/M1_imp)) as cb_ctr_pene_mom

, try(cb_WTD_ADO/cb_WTD_vw) as cb_WTD_cr
, try(cb_MTD_ADO/cb_MTD_vw) as cb_MTD_cr
, try(cb_WTD_ADO/cb_WTD_vw) - try(cb_W1_ADO/cb_W1_vw ) as cb_cr_wow
, try(cb_MTD_ADO/cb_MTD_vw) - try(cb_M1_ADO/cb_M1_vw) as cb_cr_mom
, try((cb_WTD_ADO/cb_WTD_vw)/try(WTD_ADO/WTD_vw)) as cb_WTD_cr_pene
, try((cb_MTD_ADO/cb_MTD_vw)/try(MTD_ADO/MTD_vw)) as cb_MTD_cr_pene
, try((cb_WTD_ADO/cb_WTD_vw)/try(WTD_ADO/WTD_vw)) - try((cb_W1_ADO/cb_W1_vw)/try(W1_ADO/W1_vw)) as cb_cr_pene_wow
, try((cb_MTD_ADO/cb_MTD_vw)/try(MTD_ADO/MTD_vw)) - try((cb_M1_ADO/cb_M1_vw)/try(M1_ADO/M1_vw)) as cb_cr_pene_mom

, cb_WTD_ASP
, cb_MTD_ASP
, try(cb_WTD_ASP/cb_W1_ASP) - 1 as cb_ASP_wow
, try(cb_MTD_ASP/cb_M1_ASP) - 1 as cb_ASP_mom
, try(cb_WTD_ASP/WTD_ASP) as ASP_pene_wtd
, try(cb_MTD_ASP/MTD_ASP) as ASP_pene_mtd
, try(cb_WTD_ASP/WTD_ASP) - try(cb_W1_ASP/W1_ASP) as ASP_pene_wow
, try(cb_MTD_ASP/MTD_ASP) - try(cb_M1_ASP/M1_ASP) as ASP_pene_mom

, cb_WTD_ADO_ads
, cb_MTD_ADO_ads
, try(cb_WTD_ADO_ads/cb_W1_ADO_ads) - 1 as cb_ads_ads_ado_wow
, try(cb_MTD_ADO_ads/cb_M1_ADO_ads) - 1 as cb_ads_ads_ado_mom
, try(cb_WTD_ADO_ads/WTD_ADO_ads) as ads_ado_pene_wtd
, try(cb_MTD_ADO_ads/MTD_ADO_ads) as ads_ado_pene_mtd
, try(cb_WTD_ADO_ads/WTD_ADO_ads )- try(cb_W1_ADO_ads/W1_ADO_ads) as ads_ado_pene_wow
, try(cb_MTD_ADO_ads/MTD_ADO_ads) - try(cb_M1_ADO_ads/M1_ADO_ads) as ads_ado_pene_mom 

, cb_WTD_ADO_cfs
, cb_MTD_ADO_cfs
, try(cb_WTD_ADO_cfs/cb_W1_ADO_cfs) - 1 as cb_ado_cfswow
, try(cb_MTD_ADO_cfs/cb_M1_ADO_cfs) - 1 as cb_ado_cfsmom
, try(cb_WTD_ADO_cfs/WTD_ADO_cfs) as ado_cfspene_wtd
, try(cb_MTD_ADO_cfs/MTD_ADO_cfs) as ado_cfspene_mtd
, try(cb_WTD_ADO_cfs/WTD_ADO_cfs )- try(cb_W1_ADO_cfs/W1_ADO_cfs) as ado_cfspene_wow
, try(cb_MTD_ADO_cfs/MTD_ADO_cfs) - try(cb_M1_ADO_cfs/M1_ADO_cfs) as ado_cfspene_mom

, cb_WTD_slot
, cb_MTD_slot
, try(cb_WTD_slot/cb_W1_slot) - 1 as cb_ado_slot_wow
, try(cb_MTD_slot/cb_M1_slot) - 1 as cb_ado_slot_mom
, try(cb_WTD_slot/WTD_slot) as ado_slot_pene_wtd
, try(cb_MTD_slot/MTD_slot) as ado_slot_pene_mtd
, try(cb_WTD_slot/WTD_slot )- try(cb_W1_slot/W1_slot) as ado_slot_pene_wow
, try(cb_MTD_slot/MTD_slot) - try(cb_M1_slot/M1_slot) as ado_slot_pene_mom


FROM
cat
Left join map p on p.l1_cat=cat.main_category
where cluster is not null
)
UNION 
(SELECT
'CB' as cluster
, 'total' as main_category
, cb_WTD_ADO, cb_MTD_ADO
, try(cb_WTD_ADO/cb_W1_ADO) - 1 as cb_ado_wow
, try(cb_MTD_ADO/cb_M1_ADO) - 1 as cb_ado_mom
, try(cb_WTD_ADO/WTD_ADO) as ado_pene_wtd
, try(cb_MTD_ADO/MTD_ADO) as ado_pene_mtd
, try(cb_WTD_ADO/WTD_ADO )- try(cb_W1_ADO/W1_ADO) as ado_pene_wow
, try(cb_MTD_ADO/MTD_ADO) - try(cb_M1_ADO/M1_ADO) as ado_pene_mom

, cb_WTD_vw, cb_MTD_vw
, try(cb_WTD_vw/cb_W1_vw) - 1 as cb_vw_wow
, try(cb_MTD_vw/cb_M1_vw) - 1 as cb_vw_mom
, try(cb_WTD_vw/WTD_vw) as vw_pene_wtd
, try(cb_MTD_vw/MTD_vw) as vw_pene_mtd
, try(cb_WTD_vw/WTD_vw) - try(cb_W1_vw/W1_vw) as vw_pene_wow
, try(cb_MTD_vw/MTD_vw) - try(cb_M1_vw/M1_vw) as vw_pene_mom

, cb_WTD_imp, cb_MTD_imp
, try(cb_WTD_imp/cb_W1_imp) - 1 as cb_imp_wow
, try(cb_MTD_imp/cb_M1_imp) - 1 as cb_imp_mom
, try(cb_WTD_imp/WTD_imp) as imp_pene_wtd
, try(cb_MTD_imp/MTD_imp) as imp_pene_mtd
, try(cb_WTD_imp/WTD_imp) - try(cb_W1_imp/W1_imp) as imp_pene_wow
, try(cb_MTD_imp/MTD_imp) - try(cb_M1_imp/M1_imp) as imp_pene_mom

, try(cb_WTD_clk/cb_WTD_imp) as cb_WTD_ctr
, try(cb_MTD_clk/cb_MTD_imp) as cb_MTD_ctr
, try(cb_WTD_clk/cb_WTD_imp) - try(cb_W1_clk/cb_W1_imp) as cb_ctr_wow
, try(cb_MTD_clk/cb_MTD_imp) - try(cb_M1_clk/cb_M1_imp) as cb_ctr_mom
, try((cb_WTD_clk/cb_WTD_imp)/(WTD_clk/WTD_imp)) as cb_WTD_ctr_pene
, (cb_MTD_clk/cb