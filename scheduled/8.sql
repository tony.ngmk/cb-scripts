WITH mall
AS (SELECT grass_region,
Count(DISTINCT CASE
WHEN grass_date = Date_add('day', -1,
current_date)
THEN
shop_id
END) AS MTD_shop,
Count(DISTINCT CASE
WHEN grass_date = Date_trunc('month',
Date_add('day', -1,
current_date))
- interval
'1' day THEN shop_id
END) AS LM_shop
FROM (SELECT grass_region,
grass_date,
is_cb_shop,
shop_id
FROM mp_user.dim_shop__reg_s0_live
WHERE ( grass_date = Date_add('day', -1, current_date)
OR grass_date = Date_trunc('month',
Date_add('day', -1,
current_date)
)
- interval
'1' day )
--date_add('day',-1,current_date)
AND tz_type = 'local'
AND is_official_shop = 1
AND is_cb_shop = 1
AND status = 1)
GROUP BY 1),
o
AS (SELECT grass_region,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add
(
'day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_official_shop = 1
AND is_cb_shop = 1 THEN order_id
END) / Day(Date_add('day', -1, current_date))
AS
cb_mall_ado_mtd,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add
(
'day', -1,
current_date))
-
interval '1' month
AND
Date_trunc('month',
Date_add(
'day', -1,
current_date
)
)
- interval '1' day
AND is_official_shop = 1
AND is_cb_shop = 1 THEN order_id
END) / Day(Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' day)
AS
cb_mall_ado_lm,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add(
'day', -1,
current_date)
)
-
interval '7' day
AND
Date_trunc('week', Date_add(
'day', -1,
current_date)
)
- interval '1' day
AND is_official_shop = 1
AND is_cb_shop = 1 THEN order_id
END) / 7
AS
cb_mall_ado_w1,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add(
'day', -1,
current_date)
)
AND
Date_add('day', -1, current_date
)
AND is_official_shop = 1
AND is_cb_shop = 1 THEN order_id
END) / Day_of_week(Date_add('day', -1,
current_date))
AS
cb_mall_ado_wtd,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add(
'day', -1,
current_date)
)
-
interval '7' day
AND
Date_trunc('week', Date_add(
'day', -1,
current_date)
)
- interval '1' day
AND is_official_shop = 1 THEN order_id
END) / 7
AS
mall_ado_w1,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add(
'day', -1,
current_date)
)
AND
Date_add('day', -1, current_date
)
AND is_official_shop = 1 THEN order_id
END) / Day_of_week(Date_add('day', -1,
current_date))
AS
mall_ado_wtd,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add
(
'day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_cb_shop = 1 THEN order_id
END) / Day(Date_add('day', -1, current_date))
AS
cb_ado_mtd,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add
(
'day', -1,
current_date))
-
interval '1' month
AND
Date_trunc('month',
Date_add(
'day', -1,
current_date
)
)
- interval '1' day
AND is_cb_shop = 1 THEN order_id
END) / Day(Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' day)
AS
cb_ado_lm,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add(
'day', -1,
current_date)
)
-
interval '7' day
AND
Date_trunc('week', Date_add(
'day', -1,
current_date)
)
- interval '1' day
AND is_cb_shop = 1 THEN order_id
END) / 7
AS
cb_ado_w1,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add(
'day', -1,
current_date)
)
AND
Date_add('day', -1, current_date
)
AND is_cb_shop = 1 THEN order_id
END) / Day_of_week(Date_add('day', -1,
current_date))
AS
cb_ado_wtd,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add
(
'day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_official_shop = 1 THEN order_id
END) / Day(Date_add('day', -1, current_date))
AS
mall_ado_mtd,
Count(DISTINCT CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add
(
'day', -1,
current_date))
-
interval '1' month
AND
Date_trunc('month',
Date_add(
'day', -1,
current_date
)
)
- interval '1' day
AND is_official_shop = 1 THEN order_id
END) / Day(Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' day)
AS
mall_ado_lm,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add('day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_official_shop = 1
AND is_cb_shop = 1 THEN gmv_usd
END) / Day(Date_add('day', -1, current_date))
AS
cb_mall_gmv_mtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' month AND
Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' day
AND is_official_shop = 1
AND is_cb_shop = 1 THEN gmv_usd
END) / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval
'1' day)
AS
cb_mall_gmv_lm,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval '7' day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
- interval '1' day
AND is_official_shop = 1
AND is_cb_shop = 1 THEN gmv_usd
END) / 7
AS
cb_mall_gmv_w1,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_official_shop = 1
AND is_cb_shop = 1 THEN gmv_usd
END) / Day_of_week(Date_add('day', -1, current_date))
AS
cb_mall_gmv_wtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval '7' day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
- interval '1' day
AND is_official_shop = 1 THEN gmv_usd
END) / 7
AS
mall_gmv_w1,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_official_shop = 1 THEN gmv_usd
END) / Day_of_week(Date_add('day', -1, current_date))
AS
mall_gmv_wtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add('day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_cb_shop = 1 THEN gmv_usd
END) / Day(Date_add('day', -1, current_date))
AS
cb_gmv_mtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' month AND
Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' day
AND is_cb_shop = 1 THEN gmv_usd
END) / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval
'1' day)
AS
cb_gmv_lm,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
- interval '7' day AND
Date_trunc('week',
Date_add('day', -1,
current_date))
- interval '1' day
AND is_cb_shop = 1 THEN gmv_usd
END) / 7
AS
cb_gmv_w1,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('week',
Date_add('day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_cb_shop = 1 THEN gmv_usd
END) / Day_of_week(Date_add('day', -1, current_date))
AS
cb_gmv_wtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add('day', -1,
current_date))
AND
Date_add('day', -1, current_date
)
AND is_official_shop = 1 THEN gmv_usd
END) / Day(Date_add('day', -1, current_date))
AS
mall_gmv_mtd,
SUM(CASE
WHEN grass_date BETWEEN Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' month AND
Date_trunc('month',
Date_add('day', -1,
current_date))
- interval '1' day
AND is_official_shop = 1 THEN gmv_usd
END) / Day(Date_trunc('month', Date_add('day', -1,
current_date))
- interval
'1' day)
AS
mall_gmv_lm
FROM mp_order.dwd_order_place_pay_complete_di__reg_s0_live
WHERE grass_date BETWEEN Date_trunc('month', Date_add('day', -1,
current_date))
- interval '1' month AND
Date_add('day', -1, current_date)
AND is_placed = 1
AND tz_type = 'local'
GROUP BY 1),
total
AS (SELECT SUM(mtd_shop) AS MTD_shop,
SUM(lm_shop) AS LM_shop,
SUM(cb_mall_ado_mtd) AS cb_mall_ado_mtd,
SUM(cb_mall_ado_lm) AS cb_mall_ado_lm,
SUM(cb_mall_ado_w1) AS cb_mall_ado_w1,
SUM(cb_mall_ado_wtd) AS cb_mall_ado_wtd,
SUM(cb_ado_mtd) AS cb_ado_mtd,
SUM(cb_ado_lm) AS cb_ado_lm,
SUM(cb_ado_w1) AS cb_ado_w1,
SUM(cb_ado_wtd) AS cb_ado_wtd,
SUM(mall_ado_mtd) AS mall_ado_mtd,
SUM(mall_ado_lm) AS mall_ado_lm,
SUM(cb_mall_gmv_mtd) AS cb_mall_gmv_mtd,
SUM(cb_mall_gmv_lm) AS cb_mall_gmv_lm,
SUM(cb_mall_gmv_w1) AS cb_mall_gmv_w1,
SUM(cb_mall_gmv_wtd) AS cb_mall_gmv_wtd,
SUM(cb_gmv_mtd) AS cb_gmv_mtd,
SUM(cb_gmv_lm) AS cb_gmv_lm,
SUM(cb_gmv_w1) AS cb_gmv_w1,
SUM(cb_gmv_wtd) AS cb_gmv_wtd,
SUM(mall_gmv_mtd) AS mall_gmv_mtd,
SUM(mall_gmv_lm) AS mall_gmv_lm,
SUM(mall_ado_w1) AS mall_ado_w1,
SUM(mall_gmv_w1) AS mall_gmv_w1,
SUM(mall_ado_wtd) AS mall_ado_wtd,
SUM(mall_gmv_wtd) AS mall_gmv_wtd
FROM mall l
left join o
ON o.grass_region = l.grass_region)
(SELECT m.grass_region,
mtd_shop,
( mtd_shop - lm_shop ) AS mall_num_delta,
cb_mall_ado_mtd,
( cb_mall_ado_mtd * 1.0000 / cb_mall_ado_lm ) - 1 AS cb_mall_ado_mom,
( mall_ado_mtd * 1.0000 / mall_ado_lm ) - 1 AS
platform_mall_ado_mom,
cb_mall_ado_w1,
( cb_mall_ado_wtd * 1.0000 / cb_mall_ado_w1 ) - 1 AS cb_mall_ado_WOW,
cb_mall_ado_mtd * 1.0000 / cb_ado_mtd AS
cb_mall_ado_contri,
( cb_mall_ado_mtd * 1.0000 / cb_ado_mtd ) - (
cb_mall_ado_lm * 1.0000 / cb_ado_lm ) AS
cb_mall_ado_contri_mom,
cb_mall_ado_w1 * 1.0000 / cb_ado_w1 AS
cb_mall_ado_contri_w1,
( cb_mall_ado_wtd * 1.0000 / cb_ado_wtd ) - (
cb_mall_ado_w1 * 1.0000 / cb_ado_w1 ) AS
cb_mall_ado_contri_wow,
cb_mall_gmv_mtd,
( cb_mall_gmv_mtd * 1.0000 / cb_mall_gmv_lm ) - 1 AS cb_mall_gmv_mom,
( mall_gmv_mtd * 1.0000 / mall_gmv_lm ) - 1 AS
platform_mall_gmv_mom,
cb_mall_gmv_w1,
( cb_mall_gmv_wtd * 1.0000 / cb_mall_gmv_w1 ) - 1 AS cb_mall_gmv_WOW,
cb_mall_gmv_mtd * 1.0000 / cb_gmv_mtd AS
cb_mall_gmv_contri,
( cb_mall_gmv_mtd * 1.0000 / cb_gmv_mtd ) - (
cb_mall_gmv_lm * 1.0000 / cb_gmv_lm ) AS
cb_mall_gmv_contri_mom,
cb_mall_gmv_w1 * 1.0000 / cb_gmv_w1 AS
cb_mall_gmv_contri_w1,
( cb_mall_gmv_wtd * 1.0000 / cb_gmv_wtd ) - (
cb_mall_gmv_w1 * 1.0000 / cb_gmv_w1 ) AS
cb_mall_gmv_contri_wow,
cb_mall_gmv_mtd * 1.0000 / cb_mall_ado_mtd AS cb_mtd_abs,
( ( cb_mall_gmv_mtd * 1.0000 / cb_mall_ado_mtd ) * 1.0000 / (
cb_mall_gmv_lm * 1.0000 / cb_mall_ado_lm ) - 1 ) AS ABS_mom,
mall_ado_w1,
( mall_ado_wtd * 1.0000 / mall_ado_w1 ) - 1 AS mall_ado_WOW,
mall_gmv_w1,
( mall_gmv_wtd * 1.0000 / mall_gmv_w1 ) - 1 AS mall_gmv_WOW
FROM mall m
left join o
ON o.grass_region = m.grass_region)
UNION ALL
(SELECT 'total' AS grass_region,
mtd_shop,
( mtd_shop - lm_shop ) AS mall_num_delta,
cb_mall_ado_mtd,
( cb_mall_ado_mtd * 1.0000 / cb_mall_ado_lm ) - 1 AS cb_mall_ado_mom,
( mall_ado_mtd * 1.0000 / mall_ado_lm ) - 1 AS
platform_mall_ado_mom,
cb_mall_ado_w1,
( cb_mall_ado_wtd * 1.0000 / cb_mall_ado_w1 ) - 1 AS cb_mall_ado_WOW,
cb_mall_ado_mtd * 1.0000 / cb_ado_mtd AS
cb_mall_ado_contri,
( cb_mall_ado_mtd * 1.0000 / cb_ado_mtd ) - (
cb_mall_ado_lm * 1.0000 / cb_ado_lm ) AS
cb_mall_ado_contri_mom,
cb_mall_ado_w1 * 1.0000 / cb_ado_w1 AS
cb_mall_ado_contri_w1,
( cb_mall_ado_wtd * 1.0000 / cb_ado_wtd ) - (
cb_mall_ado_w1 * 1.0000 / cb_ado_w1 ) AS
cb_mall_ado_contri_wow,
cb_mall_gmv_mtd,
( cb_mall_gmv_mtd * 1.0000 / cb_mall_gmv_lm ) - 1 AS cb_mall_gmv_mom,
( mall_gmv_mtd * 1.0000 / mall_gmv_lm ) - 1 AS
platform_mall_gmv_mom,
cb_mall_gmv_w1,
( cb_mall_gmv_wtd * 1.0000 / cb_mall_gmv_w1 ) - 1 AS cb_mall_gmv_WOW,
cb_mall_gmv_mtd * 1.0000 / cb_gmv_mtd AS
cb_mall_gmv_contri,
( cb_mall_gmv_mtd * 1.0000 / cb_gmv_mtd ) - (
cb_mall_gmv_lm * 1.0000 / cb_gmv_lm ) AS
cb_mall_gmv_contri_mom,
cb_mall_gmv_w1 * 1.0000 / cb_gmv_w1 AS
cb_mall_gmv_contri_w1,
( cb_mall_gmv_wtd * 1.0000 / cb_gmv_wtd ) - (
cb_mall_gmv_w1 * 1.0000 / cb_gmv_w1 ) AS
cb_mall_gmv_contri_wow,
cb_mall_gmv_mtd * 1.0000 / cb_mall_ado_mtd AS cb_mtd_abs,
( ( cb_mall_gmv_mtd * 1.0000 / cb_mall_ado_mtd ) * 1.0000 / (
cb_mall_gmv_lm * 1.0000 / cb_mall_ado_lm ) - 1 ) AS ABS_mom,
mall_ado_w1,
( mall_ado_wtd * 1.0000 / mall_ado_w1 ) - 1 AS mall_ado_WOW,
mall_gmv_w1,
( mall_gmv_wtd * 1.0000 / mall_gmv_w1 ) - 1 AS mall_gmv_WOW
FROM total)
ORDER BY 1