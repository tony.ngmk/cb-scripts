WITH cb_ord AS
	(SELECT ov4.grass_region
		, create_date
	    , count(distinct ov4.orderid) AS gross_order
		, sum(gmv_usd) AS gmv_usd
	FROM
		(SELECT grass_region
			, order_id orderid
			, shop_id shopid
			, date(from_unixtime(create_timestamp))  as create_date
			, sum(gmv_usd) gmv_usd
		FROM mp_order.dwd_order_all_ent_df__reg_s0_live
        --#chng02 Changed shopee.shopee_order_v4_db__order_v4_tab to order_mart_dwd_order_all_event_final_status_df
        -- replaced total_price calculation x exchange rate to default gmv_usd
        -- replaced  cb_option to is_cb_shop
        -- added tz_type and is_placed_condition
        -- removed timezone conversion and stick with create_timestamp (SGT)
		WHERE is_cb_shop = 1
            AND grass_date >= (${nexus_data_date} - INTERVAL  '65' DAY)
            and tz_type='local'
			AND create_timestamp >= to_unixtime(${nexus_data_date}) - 86400 * 65
			and grass_region!=''
            group by 1,2,3,4
		) ov4
	GROUP BY 1, 2
	),


sku AS
	(SELECT iv4.grass_region
		, iv4.data_date
		, iv4.shopid
		, iv4.live_sku
		, iv4.new_live_sku
	FROM
		(

        SELECT grass_region
		, grass_date AS data_date
		, shop_id shopid
		, count(distinct item_id) AS live_sku
		, count(distinct IF(DATE(from_unixtime(create_timestamp)) = ${nexus_data_date}, item_id, NULL)) AS new_live_sku
		FROM mp_item.dim_item__reg_s0_live
        --#chng03 Changed shopee.shopee_item_v4_db__item_v4_tab to item_mart_dim_item
        -- replaced ctime with create_timestamp for new live sku
        -- replaced  cb_option to is_cb_shop
        -- added tz_type and is_placed_condition
        -- added item_mart_dws_shop_listing_td as the condition to join and limit the shopid with live sku
		WHERE is_cb_shop = 1
			AND stock > 0
			AND status = 1
			and grass_region<>'' 
			AND grass_date= ${nexus_data_date} 
            and tz_type='local'
		GROUP BY 1, 2, 3

		) iv4
	JOIN
	    (SELECT distinct grass_region, shop_id
	    FROM mp_user.dim_shop__reg_s0_live
	    WHERE grass_date = ${nexus_data_date}
	        AND is_holiday_mode = 0
	        AND status = 1
	        AND is_cb_shop = 1
	        AND tz_type = 'local'
	    ) s
	ON iv4.grass_region = s.grass_region
		AND iv4.shopid = s.shop_id
	JOIN
		(SELECT distinct grass_region, shop_id
        FROM mp_user.dws_user_login_td_account_info__reg_s0_live 
        WHERE tz_type = 'local'
        AND grass_date = ${nexus_data_date}
        AND DATE(from_unixtime(last_login_timestamp_td)) >= date_add('day', -6, grass_date)
		) u 
	ON iv4.grass_region = u.grass_region
		AND iv4.shopid = u.shop_id

    JOIN
		(SELECT distinct grass_region, shop_id
	    FROM mp_item.dws_shop_listing_td__reg_s0_live
	    WHERE grass_date = ${nexus_data_date}
	        AND active_item_with_stock_cnt > 0
		) sl 
	ON  iv4.grass_region  = sl.grass_region
		AND iv4.shopid =  sl.shop_id

	),

cb_sku AS
	(SELECT grass_region, data_date, sum(live_sku) AS live_sku, sum(new_live_sku) AS new_live_sku
	FROM sku
	GROUP BY 1, 2
	)

SELECT cb_ord.grass_region
	, cb_ord.create_date as data_date
	, cb_ord.gross_order AS cb_gross_order
	, cb_ord.gmv_usd AS cb_gmv_usd
	, cb_sku.live_sku AS cb_live_sku
FROM cb_ord
LEFT JOIN cb_sku
ON cb_sku.grass_region = cb_ord.grass_region
	AND cb_sku.data_date = cb_ord.create_date
ORDER BY 2 DESC, 1
