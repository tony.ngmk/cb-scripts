DELETE FROM regcbbi_others.ops_shop_violation_reason ;
INSERT INTO regcbbi_others.ops_shop_violation_reason 
SELECT * FROM (
WITH sip AS (
    SELECT DISTINCT
        affi_shopid,
        affi_userid
    FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
    WHERE
      grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
)

, t1 AS (
(
    SELECT DISTINCT 
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_vn_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime(CAST(action_time_msec AS bigint) / 1000)) action_time
        FROM
            marketplace.shopee_backend_log_vn_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime(CAST(action_time_msec AS bigint) / 1000)) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
        date(from_unixtime(CAST(a.action_time_msec AS bigint) / 1000)) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_sg_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_sg_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
        date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_my_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_my_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b 
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
        date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_th_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_th_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
        date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_id_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_id_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
        date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_ph_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_ph_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
      date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_tw_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_tw_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
      date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_br_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_br_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
      date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_mx_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_mx_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
      date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_co_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_co_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
      date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_cl_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_cl_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
      date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

UNION

(
    SELECT DISTINCT
        a.target_pk,
        a.username,
        a.changes,
        json_extract_scalar(changes, '$.reason') reason,
        from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
    FROM marketplace.shopee_backend_log_pl_db__shopee_audit_log_tab__reg_continuous_s0_live a
    INNER JOIN (
        SELECT
            target_pk,
            max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
        FROM
            marketplace.shopee_backend_log_pl_db__shopee_audit_log_tab__reg_continuous_s0_live
        WHERE
            ((username LIKE '%@%'
                AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
              or username = 'SYSTEM')
            AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
        GROUP BY 1
    ) b
    ON a.target_pk = b.target_pk
      AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
    WHERE
      date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
)

-- UNION

-- (
--     SELECT DISTINCT
--         a.target_pk,
--         a.username,
--         a.changes,
--         json_extract_scalar(changes, '$.reason') reason,
--         from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
--     FROM marketplace.shopee_backend_log_fr_db__shopee_audit_log_tab__reg_continuous_s0_live a
--     INNER JOIN (
--         SELECT
--             target_pk,
--             max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
--         FROM
--             marketplace.shopee_backend_log_fr_db__shopee_audit_log_tab__reg_continuous_s0_live
--         WHERE
--             ((username LIKE '%@%'
--                 AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
--               or username = 'SYSTEM')
--             AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
--         GROUP BY 1
--     ) b
--     ON a.target_pk = b.target_pk
--       AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
--     WHERE
--       date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
-- )
  
-- UNION 

-- (
--     SELECT DISTINCT
--         a.target_pk,
--         a.username,
--         a.changes,
--         json_extract_scalar(changes, '$.reason') reason,
--         from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) action_time
--     FROM marketplace.shopee_backend_log_es_db__shopee_audit_log_tab__reg_continuous_s0_live a
--     INNER JOIN (
--         SELECT
--             target_pk,
--             max(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) action_time
--         FROM
--             marketplace.shopee_backend_log_es_db__shopee_audit_log_tab__reg_continuous_s0_live
--         WHERE
--             ((username LIKE '%@%'
--                 AND json_extract_scalar(changes, '$.status[1]') IN ('frozen', 'banned'))
--               or username = 'SYSTEM')
--             AND date(from_unixtime((CAST(action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
--         GROUP BY 1
--     ) b 
--     ON a.target_pk = b.target_pk
--       AND from_unixtime((CAST(a.action_time_msec AS bigint) / 1000)) = b.action_time
--     WHERE
--       date(from_unixtime((CAST(a.action_time_msec AS bigint) / 1000))) >= date '2019-05-01'
--   )
)

SELECT DISTINCT
    sip.affi_shopid shopid,
    sip.affi_userid userid,
    reason,
    changes,
    t1.username,
    action_time
FROM (
    SELECT DISTINCT
        target_pk,
        changes,
        reason,
        username,
        date(action_time) action_time
    FROM t1
) t1
INNER JOIN sip
ON t1.target_pk = sip.affi_userid
UNION ALL
SELECT DISTINCT
    shopid,
    userid,
    reason,
    changes,
    t1.username,
    action_time
FROM (
    SELECT DISTINCT
        target_pk,
        changes,
        reason,
        username,
        date(action_time) action_time
    FROM t1
) t1
INNER JOIN(
    SELECT
        shopid,
        userid
    FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) a
ON t1.target_pk = a.userid
)
