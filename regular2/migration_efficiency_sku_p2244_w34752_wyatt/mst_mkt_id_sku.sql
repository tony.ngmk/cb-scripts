
insert into dev_regcbbi_general.bi__sip_migration_sku_aggregated_info__v2__df__reg__s0

WITH
  sku AS (
   SELECT DISTINCT grass_date
   , shop_id shopid
   , item_id itemid
   , status item_status
   , stock item_stock
   , is_cb_shop
   , grass_region 
   FROM
     mp_item.dim_item__reg_s0_live
WHERE grass_date  =  date(date_parse('${PRE_DAY}','%Y%m%d')) 
and tz_type = 'local' 
and seller_status=1 
and is_holiday_mode = 0 
and shop_status = 1 
and grass_region in 
('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')

) 
, sip AS (
   SELECT DISTINCT
     a.affi_shopid
   , affi_itemid
   , affi_country
   , mst_shopid
   , mst_itemid
   , b.country mst_country
   , b.cb_option
   FROM
     (marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
   INNER JOIN (select distinct shopid 
                     , country 
                     , cb_option
   from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
   where country ='ID'
   ) b ON (a.mst_shopid = b.shopid))
   join (select distinct affi_Shopid 
            from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live 
            where grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
            and sip_shop_status!=3) c on c.affi_shopid = a.affi_shopid 
   where  affi_country  in 
('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
) 
, affi AS (
   SELECT DISTINCT
     affi_country
   , affi_itemid
   , item_status
   , item_stock
   , mst_itemid
   , grass_date
   , cb_option
   FROM
     (sip
   INNER JOIN sku ON (sip.affi_itemid = sku.itemid))

  where sku.is_cb_shop=1 
) 
-- , mst AS (
--    SELECT DISTINCT
--      itemid mst_itemid
--    , item_status
--    , item_stock
--    , grass_region mst_country
--    FROM
--      sku 

-- ) 
SELECT DISTINCT sku.grass_region mst_country
, affi.affi_country
, sku.item_status mst_item_status
, (CASE WHEN (sku.item_stock >= 10) THEN 1 ELSE 0 END) mst_item_stock
, affi.item_status affi_item_status
, (CASE WHEN (affi.item_stock > 0) THEN 1 ELSE 0 END) affi_item_stock
, "count"(DISTINCT affi_itemid) sku_count
, affi.grass_date AS grass_date
, cb_option
, affi.grass_date AS data_date
FROM
  (affi
INNER JOIN sku ON (affi.mst_itemid = sku.itemid))
WHERE ((sku.item_status = 1) AND (sku.item_stock > 0))
GROUP BY 1, 2, 3, 4, 5, 6, 8, 9
