WITH
  raw AS (
   SELECT
     o.grass_region
   , o.item_id
   , (CASE WHEN (o.grass_date BETWEEN "date_trunc"('month', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN 1 ELSE 0 END) mtd
   , (CASE WHEN ((date_format(o.grass_date,'%y%m') =date_format( "date_add"('month',-1,"date"(current_date)),'%y%m'))) THEN 1 ELSE 0 END) m_1
   , (CASE WHEN (o.grass_date BETWEEN "date_trunc"('week', "date_add"('day', -1, current_date)) AND "date_add"('day', -1, current_date)) THEN 1 ELSE 0 END) wtd
   , (CASE WHEN (o.grass_date BETWEEN "date_add"('week', -1, "date_trunc"('week', current_date)) AND "date_add"('day', -1, "date_trunc"('week', current_date))) THEN 1 ELSE 0 END) w_1
   , (CASE WHEN (o.grass_date BETWEEN "date_add"('week', -2, "date_trunc"('week', current_date)) AND "date_add"('day', -8, "date_trunc"('week', current_date))) THEN 1 ELSE 0 END) w_2
   , order_fraction
   , gmv
   FROM
     ((mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
  )
   LEFT JOIN (
      SELECT DISTINCT b.affi_shopid
      FROM
        (marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
      LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b ON (b.mst_shopid = a.shopid))
      WHERE ((a.cb_option = 0) AND (a.country = 'TW') AND (b.grass_region IN ('MY','SG','ID')))
     
   )  sip ON (sip.affi_shopid = o.shop_id))
   WHERE (((sip.affi_shopid IS NOT NULL) AND (o.grass_date >= "date_add"('month', -1, "date_trunc"('month', "date_add"('day', -1, current_date)))))) AND (o.is_placed = 1) AND (o.tz_type = 'local')
) 
, sales AS (
   SELECT
     grass_region
   , item_id
   , "sum"((CASE WHEN (mtd = 1) THEN order_fraction ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double) mtd_orders
   , "sum"((CASE WHEN (mtd = 1) THEN gmv ELSE 0 END)) / CAST("day"("date_add"('day', -1, current_date)) AS double) mtd_gmv
   , "sum"((CASE WHEN (m_1 = 1) THEN order_fraction ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double) m_1_orders
   , "sum"((CASE WHEN (m_1 = 1) THEN gmv ELSE 0 END)) / CAST("day"("date_add"('day', -1, "date_trunc"('month', current_date))) AS double) m_1_gmv
   , "sum"((CASE WHEN (wtd = 1) THEN order_fraction ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double) wtd_orders
   , "sum"((CASE WHEN (wtd = 1) THEN gmv ELSE 0 END)) / CAST("day_of_week"("date_add"('day', -1, current_date)) AS double) wtd_gmv
   , "sum"((CASE WHEN (w_1 = 1) THEN order_fraction ELSE 0 END)) / DECIMAL '7.00' w_1_orders
   , "sum"((CASE WHEN (w_1 = 1) THEN gmv ELSE 0 END))  / DECIMAL '7.00' w_1_gmv
   , "sum"((CASE WHEN (w_2 = 1) THEN order_fraction ELSE 0 END))  / DECIMAL '7.00' w_2_orders
   , "sum"((CASE WHEN (w_2 = 1) THEN gmv ELSE 0 END))  / DECIMAL '7.00' w_2_gmv
   FROM
     raw
   GROUP BY 1, 2
) 
SELECT
  grass_region
, (CASE WHEN (r_w <= 10) THEN '[Top 10]' WHEN (r_w <= 50) THEN '[Top 11 - 50]' WHEN (r_w <= 100) THEN '[Top 51 - 100]' WHEN (r_w <= 1000) THEN '[Top 101 - 1000]' ELSE '[Others]' END) tier
, COUNT(DISTINCT item_id) AS items
, "sum"(mtd_orders) mtd_orders
, "sum"(mtd_gmv) mtd_gmv
, "sum"(m_1_orders) m_1_orders
, "sum"(m_1_gmv) m_1_gmv
, "sum"(wtd_orders) wtd_orders
, "sum"(wtd_gmv) wtd_gmv
, "sum"(w_1_orders) w_1_orders
, "sum"(w_1_gmv) w_1_gmv
, "sum"(w_2_orders) w_2_orders
, "sum"(w_2_gmv) w_2_gmv
FROM   (
   SELECT
     "rank"() OVER (PARTITION BY grass_region ORDER BY mtd_orders DESC) r_w
   , *
   FROM
     sales
) 
GROUP BY 1, 2
ORDER BY 1 ASC
