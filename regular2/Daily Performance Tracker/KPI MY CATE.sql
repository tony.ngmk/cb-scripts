WITH sip AS (
  SELECT DISTINCT
    b.affi_shopid shopid
  FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
  LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b
  ON b.mst_shopid = a.shopid
  WHERE
    a.cb_option = 0
    AND a.country = 'TW'
    AND b.grass_region IN ('ID', 'MY', 'MY')
)

SELECT DISTINCT
  z.main_cat
  , Ystday_Order
  , MTD_Order
  , WTD_Order
  , W_1_Order
  , W_2_Order
  , LM_Order
  , Ystday_CFS
  , MTD_CFS
  , WTD_CFS
  , W_1_CFS
  , W_2_CFS
  , LM_CFS
  , Ystday_Campaign
  , MTD_Campaign
  , WTD_Campaign
  , W_1_Campaign
  , W_2_Campaign
  , LM_Campaign
  , Ystday_Organic
  , MTD_Organic
  , WTD_Organic
  , W_1_Organic
  , W_2_Organic
  , LM_Organic
  , SUM(CASE WHEN MTD = 1 THEN views ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_views
  , SUM(CASE WHEN LM = 1 THEN views ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_views
  , SUM(CASE WHEN WTD = 1 THEN views ELSE 0 END) / day_of_week(date_add('day', -1, current_date)) AS WTD_views
  , SUM(CASE WHEN W_1 = 1 THEN views ELSE 0 END) / DECIMAL '7.0' AS W_1_views
  , SUM(CASE WHEN W_2 = 1 THEN views ELSE 0 END) / DECIMAL '7.0' AS W_2_views
  , MTD_gmv
  , LM_gmv
  , WTD_gmv
  , W_1_gmv
  , W_2_gmv
  , Ystday_GMV
  , D_2_GMV
  , MTD_pa_order  -- MONTH TO DAY PAID ADS ORDER
  , LM_pa_order
  , MTD_sv_order    --SHOP VOUCHER MTD
  , LM_sv_order
  , MTD_pv_order  -- PLATFORM VOUCHER MTD
  , LM_pv_order
  , WTD_pa_order
  , W_1_pa_order
  , WTD_sv_order
  , W_1_sv_order
  , MTD_lpp_order
  , LM_lpp_order
  , WTD_lpp_order
  , W_1_lpp_order
FROM
  (
    (SELECT DISTINCT
        orders.level1_category main_cat
        , *
      FROM (
          SELECT DISTINCT
            grass_region
            , level1_category
            , SUM(CASE WHEN YD = 1 THEN pp ELSE 0 END) AS Ystday_Order
            , SUM(CASE WHEN MTD = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_Order
            , SUM(CASE WHEN LM = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_Order
            , SUM(CASE WHEN WTD = 1 THEN pp ELSE 0 END) / day_of_week(date_add('day', -1, current_date)) AS WTD_Order
            , SUM(CASE WHEN W_1 = 1 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_1_Order
            , SUM(CASE WHEN W_2 = 1 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_2_Order
            , SUM(CASE WHEN YD = 1 AND flash_sale = 1 THEN pp ELSE 0 END) AS Ystday_CFS
            , SUM(CASE WHEN MTD = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_CFS
            , SUM(CASE WHEN LM = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_CFS
            , SUM(CASE WHEN WTD = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / day_of_week(date_add('day', -1, current_date)) AS WTD_CFS
            , SUM(CASE WHEN W_1 = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_1_CFS
            , SUM(CASE WHEN W_2 = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_2_CFS
            , SUM(CASE WHEN YD = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) AS Ystday_Campaign
            , SUM(CASE WHEN MTD = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_Campaign
            , SUM(CASE WHEN LM = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_Campaign
            , SUM(CASE WHEN WTD = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / day_of_week(date_add('day', -1, current_date)) AS WTD_Campaign
            , SUM(CASE WHEN W_1 = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_1_Campaign
            , SUM(CASE WHEN W_2 = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_2_Campaign
            , SUM(CASE WHEN YD = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) AS Ystday_Organic
            , SUM(CASE WHEN MTD = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_Organic
            , SUM(CASE WHEN LM = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_Organic
            , SUM(CASE WHEN WTD = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / day_of_week(date_add('day', -1, current_date)) AS WTD_Organic
            , SUM(CASE WHEN W_1 = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_1_Organic
            , SUM(CASE WHEN W_2 = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_2_Organic
            , SUM(CASE WHEN MTD = 1 THEN gmv ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_gmv
            , SUM(CASE WHEN LM = 1 THEN gmv ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_gmv
            , SUM(CASE WHEN WTD = 1 THEN gmv ELSE 0 END) / day_of_week(date_add('day', -1, current_date)) AS WTD_gmv
            , SUM(CASE WHEN W_1 = 1 THEN gmv ELSE 0 END) / DECIMAL '7.0' AS W_1_gmv
            , SUM(CASE WHEN W_2 = 1 THEN gmv ELSE 0 END) / DECIMAL '7.0' AS W_2_gmv
            , SUM(CASE WHEN YD = 1 THEN gmv ELSE 0 END) AS Ystday_GMV
            , SUM(CASE WHEN D_2 = 1 THEN gmv ELSE 0 END) AS D_2_GMV
            , SUM(CASE WHEN MTD = 1 AND is_sv_voucher_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_sv_order
            , SUM(CASE WHEN LM = 1 AND is_sv_voucher_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_sv_order
            , SUM(CASE WHEN WTD = 1 AND is_sv_voucher_order = 1 THEN pp ELSE 0 END) / day_of_week(date_add('day', -1, current_date)) AS WTD_sv_order
            , SUM(CASE WHEN W_1 = 1 AND is_sv_voucher_order = 1 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_1_sv_order
            , SUM(CASE WHEN MTD = 1 AND is_pv_voucher_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_pv_order
            , SUM(CASE WHEN LM = 1 AND is_pv_voucher_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_pv_order
            , SUM(CASE WHEN MTD = 1 AND lpp = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_lpp_order
            , SUM(CASE WHEN LM = 1 AND lpp = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_lpp_order
            , SUM(CASE WHEN WTD = 1 AND lpp = 1 THEN pp ELSE 0 END) / day_of_week(date_add('day', -1, current_date)) AS WTD_lpp_order
            , SUM(CASE WHEN W_1 = 1 AND lpp = 1 THEN pp ELSE 0 END) / DECIMAL '7.0' AS W_1_lpp_order
          FROM (
              SELECT DISTINCT
                a.grass_region
                , level1_global_be_category AS level1_category
                , a.order_id
                , a.shop_id
                , a.item_id
                , a.model_id
                , a.gmv_usd gmv
                , a.order_fraction pp
                -- , CASE WHEN b.item_id IS NOT NULL THEN 1 ELSE 0 END AS campaign
                , CASE WHEN a.item_promotion_source = 'shopee' THEN 1 ELSE 0 END AS campaign
                , CASE WHEN a.item_promotion_source = 'flash_sale' THEN 1 ELSE 0 END AS flash_sale
                , CASE WHEN a.order_price_pp BETWEEN 1 AND 9 THEN 1 ELSE 0 END AS lpp
                , CASE WHEN a.grass_date = date_add('day', -1, current_date) THEN 1 ELSE 0 END AS YD
                , CASE WHEN (sv_voucher_code IS not NULL and is_sv_seller_absorbed = 1) or is_pv_seller_absorbed = 1 THEN 1 ELSE 0 END AS is_sv_voucher_order
                , CASE WHEN (pv_voucher_code IS not NULL and is_pv_seller_absorbed = 0) or is_sv_seller_absorbed = 0 THEN 1 ELSE 0 END AS is_pv_voucher_order
                , CASE WHEN a.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END AS MTD
                , CASE WHEN date_format(a.grass_date, '%y%m') = date_format(date_add('month', -1, date_add('day', -1, current_date)), '%y%m') THEN 1 ELSE 0 END AS LM
                , CASE WHEN a.grass_date BETWEEN date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)))
                              AND date_add('day', -1, date_trunc('week', date_add('day', -1, current_date))) THEN 1 ELSE 0 END AS W_1
                , CASE WHEN a.grass_date BETWEEN date_add('week', -2, date_trunc('week', date_add('day', -1, current_date)))
                              AND date_add('day', -1, date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)))) THEN 1 ELSE 0 END AS W_2
                , CASE WHEN a.grass_date BETWEEN date_trunc('week', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END AS WTD
                , CASE WHEN a.grass_date = date_add('day', -2, current_date) THEN 1 ELSE 0 END AS D_2
              FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live a
              INNER JOIN sip s
              ON a.shop_id = s.shopid
              -- LEFT JOIN (
              --     SELECT DISTINCT
              --       item_id
              --       , from_unixtime(start_time) start_date
              --       , from_unixtime(end_time) end_date
              --     FROM regbida_userbehavior.shopee_bi_campaign_item
              --     WHERE grass_region = 'MY'
              -- ) b
              -- ON a.item_id = b.item_id
              --   AND a.grass_date >= b.start_date  
              --   AND a.grass_date <= b.end_date
              WHERE
                a.grass_date >= date_add('day', -64, current_date)
                AND a.grass_region = 'MY'
                AND a.is_cb_shop = 1
                AND a.is_placed = 1
                AND a.tz_type = 'local'
          )
          GROUP BY 1, 2
      ) orders
    ) z
    LEFT JOIN (
      SELECT DISTINCT
        i.level1_category
        , SUM(pv_cnt_1d) views
        , CASE WHEN ee.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END AS MTD
        , CASE WHEN date_format(ee.grass_date, '%y%m') = date_format(date_add('month', -1, date_add('day', -1, current_date)), '%y%m') THEN 1 ELSE 0 END AS LM
        , CASE WHEN ee.grass_date BETWEEN date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)))
                        AND date_add('day', -1, date_trunc('week', date_add('day', -1, current_date))) THEN 1 ELSE 0 END AS W_1
        , CASE WHEN ee.grass_date BETWEEN date_add('week', -2, date_trunc('week', date_add('day', -1, current_date)))
                        AND date_add('day', -1, date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)))) THEN 1 ELSE 0 END AS W_2
        , CASE WHEN ee.grass_date BETWEEN date_trunc('week', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END AS WTD
      FROM mp_shopflow.dws_item_civ_1d__reg_s0_live ee
      INNER JOIN sip
      ON ee.shop_id = sip.shopid
      LEFT JOIN (
          SELECT DISTINCT
            level1_global_be_category AS level1_category
            , item_id
          FROM mp_item.dim_item__reg_s0_live i
          INNER JOIN sip
          ON sip.shopid = i.shop_id
          WHERE
            grass_region = 'MY'
            AND i.tz_type = 'local'
            AND i.grass_date = current_date - interval '1' day
      ) i
      ON i.item_id = ee.item_id
      WHERE
        ee.grass_date >= date_add('day', -100, current_date)
        AND ee.grass_region = 'MY'
        AND ee.tz_type = 'local'
      GROUP BY 1,3,4,5,6,7
    ) e
    ON e.level1_category = z.main_cat
  )
  LEFT JOIN (
    SELECT DISTINCT
      i.level1_category
      , SUM(mtd_pa_order) MTD_pa_order
      , SUM(lm_pa_order) LM_pa_order
      , SUM(wtd_pa_order) WTD_pa_order
      , SUM(lw_pa_order) W_1_pa_order
    FROM (
        SELECT DISTINCT
          m.item_id
          , SUM(CASE WHEN m.grass_date BETWEEN date_trunc('month', current_date - interval '1' day) AND current_date - interval '1' day THEN m.order_cnt ELSE 0 END)
              / CAST(day(date_add('day', -1, current_date)) AS DOUBLE) AS mtd_pa_order
          , SUM(CASE WHEN m.grass_date BETWEEN date_trunc('month', date_add('day', -1, date_trunc('month', current_date - interval '1' day)))
                  AND date_add('day', -1, date_trunc('month', current_date - interval '1' day)) THEN m.order_cnt ELSE 0 END)
              / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS DOUBLE) AS lm_pa_order
          , SUM(CASE WHEN m.grass_date BETWEEN date_trunc('week', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN m.order_cnt ELSE 0 END)
              / CAST(day_of_week(date_add('day', -1, current_date)) AS DOUBLE) AS wtd_pa_order
          , SUM(CASE WHEN m.grass_date BETWEEN date_add('week', -1, date_trunc('week', date_add('day', -1, current_date)))
                  AND date_add('day', -1, date_trunc('week', date_add('day', -1, current_date))) THEN m.order_cnt ELSE 0 END)
              / DECIMAL '7.0' AS lw_pa_order
        FROM sip s
        LEFT JOIN (
            SELECT DISTINCT
              shop_id
              , item_id
              , grass_date
              , SUM(order_cnt) AS order_cnt
            FROM mp_paidads.ads_advertise_mkt_1d__reg_s0_live
            WHERE
              grass_date >= date_trunc('month', date_add('day', -1, date_trunc('month', current_date - interval '1' day)))
              AND tz_type = 'local'
              AND grass_region = 'MY'
            GROUP BY 1,2,3
        ) m
        ON s.shopid = m.shop_id
        GROUP BY 1
    ) pa
    LEFT JOIN (
        SELECT DISTINCT
          level1_global_be_category AS level1_category
          , item_id
        FROM mp_item.dim_item__reg_s0_live i
        INNER JOIN sip
        ON sip.shopid = i.shop_id
        WHERE
          grass_region = 'MY'
          AND i.tz_type = 'local'
          AND i.grass_date = current_date - interval '1' day
    ) i
    ON i.item_id = pa.item_id
    GROUP BY 1
  ) ads
  ON ads.level1_category = z.main_cat
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51
ORDER BY Ystday_Order DESC
