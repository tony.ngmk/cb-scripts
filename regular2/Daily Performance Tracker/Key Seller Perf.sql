WITH focus_seller AS (
    SELECT DISTINCT b.affi_shopid shopid
    FROM (
        SELECT DISTINCT TRY_CAST(mst_shopid AS bigint) shopid
        FROM regcbbi_others.shopee_regional_cb_team__twsip_focus_seller
        WHERE ingestion_timestamp = (
            SELECT max(ingestion_timestamp) col_1
            FROM regcbbi_others.shopee_regional_cb_team__twsip_focus_seller
            WHERE ingestion_timestamp <> '')
    ) a
    INNER JOIN (
        SELECT mst_shopid, affi_shopid
        FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
        WHERE grass_region IN ('SG', 'MY')
    ) b
    ON a.shopid = b.mst_shopid
) 

, shop_performance AS (
    SELECT
        shop_id
        , SUM(CASE WHEN MTD = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_Order
        , SUM(CASE WHEN LM = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) LM_Order
        , SUM(CASE WHEN MTD = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_CFS
        , SUM(CASE WHEN LM = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) LM_CFS
        , SUM(CASE WHEN MTD = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_Campaign
        , SUM(CASE WHEN LM = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) LM_Campaign
        , SUM(CASE WHEN MTD = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_Organic
        , SUM(CASE WHEN LM = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) LM_Organic
        , SUM(CASE WHEN MTD = 1 AND voucher_order = 1 AND seller_voucher = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_shopee_voucher
        , SUM(CASE WHEN LM = 1 AND voucher_order = 1 AND seller_voucher = 0 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) LM_shopee_voucher
        , SUM(CASE WHEN MTD = 1 AND voucher_order = 1 AND seller_voucher = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_seller_voucher
        , SUM(CASE WHEN LM = 1 AND voucher_order = 1 AND seller_voucher = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) LM_seller_voucher
        , SUM(CASE WHEN MTD = 1 THEN gmv ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_GMV
        , SUM(CASE WHEN LM = 1 THEN gmv ELSE 0 END) / day(date_add('day', -1, date_trunc('month',  date_add('day', -1, current_date)))) LM_GMV
        , (SUM((CASE WHEN ("WTD"=1) THEN pp ELSE 0 END)) /day_of_week(date_add('day', -1, current_date)))  "WTD_Order"
        , (SUM((CASE WHEN ("LW"=1) THEN pp ELSE 0 END) /7.00)) "LW_Order"
        -- , (SUM((CASE WHEN (("WTD"=1) AND (flash_sale=1)) THEN pp ELSE 0 END)) /day_of_week(date_add('day', -1, current_date))) "WTD_CFS"
        -- , (SUM((CASE WHEN (("LW"=1) AND (flash_sale=1)) THEN pp ELSE 0 END)) /7.00) "LW_CFS"
        -- , (SUM((CASE WHEN ((("WTD" = 1) AND (campaign = 1)) AND (flash_sale = 0)) THEN pp ELSE 0 END))/day_of_week(date_add('day', -1, current_date))) "WTD_Campaign"
        -- , (SUM((CASE WHEN ((("LW" = 1) AND (campaign = 1)) AND (flash_sale = 0)) THEN pp ELSE 0 END))/7.00) "LW_Campaign"
        -- , (SUM((CASE WHEN ((("WTD" = 1) AND (campaign = 0)) AND (flash_sale = 0)) THEN pp ELSE 0 END))/day_of_week(date_add('day', -1, current_date))) "WTD_Organic"
        -- , (SUM((CASE WHEN ((("LW" = 1) AND (campaign = 0)) AND (flash_sale = 0)) THEN pp ELSE 0 END))/7.00) "LW_Organic"
        -- , (SUM((CASE WHEN ((("WTD" = 1) AND (voucher_order = 1)) AND (seller_voucher = 0)) THEN pp ELSE 0 END))/day_of_week(date_add('day', -1, current_date))) "WTD_shopee_voucher"
        -- , (SUM((CASE WHEN ((("LW" = 1) AND (voucher_order = 1)) AND (seller_voucher = 0)) THEN pp ELSE 0 END))/7.00) "LW_shopee_voucher"
        -- , (SUM((CASE WHEN ((("WTD" = 1) AND (voucher_order = 1)) AND (seller_voucher = 1)) THEN pp ELSE 0 END))/day_of_week(date_add('day', -1, current_date))) "WTD_seller_voucher"
        -- , (SUM((CASE WHEN ((("LW" = 1) AND (voucher_order = 1)) AND (seller_voucher = 1)) THEN pp ELSE 0 END))/7.00) "LW_seller_voucher"
         , (SUM((CASE WHEN ("WTD" = 1) THEN gmv ELSE 0 END)) / day_of_week(date_add('day', -1, current_date))) "WTD_gmv"
         , (SUM((CASE WHEN ("LW" = 1) THEN gmv ELSE 0 END)) / DECIMAL '7.0') "W_1_gmv"
        -- , (SUM((CASE WHEN ("W_2" = 1) THEN gmv ELSE 0 END)) / DECIMAL '7.0') "W_2_gmv"   
        , SUM(CASE WHEN D_1 = 1 THEN pp ELSE 0 END) D_1_Order
        , SUM(CASE WHEN MTD = 1 AND is_follower_prize_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_follower_prize_order
        , SUM(CASE WHEN LM = 1 AND is_follower_prize_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) LM_follower_prize_order-- convert to AD value
        , SUM(CASE WHEN is_mtd_follower_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) mtd_follower_order  -- convert to AD value
        , SUM(CASE WHEN is_lm_follower_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) lm_follower_order   -- convert to AD value
        , SUM(CASE WHEN is_mtd_new_follower_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) mtd_new_follower_order-- convert to AD value
        , SUM(CASE WHEN is_lm_new_follower_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) lm_new_follower_order-- convert to AD value
        , SUM(CASE WHEN is_mtd_old_follower_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) mtd_old_follower_order-- convert to AD value
        , SUM(CASE WHEN is_lm_old_follower_order = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) lm_old_follower_order-- convert to AD value
        , SUM(CASE WHEN MTD = 1 THEN sv_rebate_by_seller_amt_usd ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_sv_cost_usd
        , SUM(CASE WHEN LM = 1 THEN sv_rebate_by_seller_amt_usd ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_sv_cost_usd
        , SUM(CASE WHEN MTD = 1 AND lpp = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, current_date)) AS MTD_lpp_ado
        , SUM(CASE WHEN LM = 1 AND lpp = 1 THEN pp ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS LM_lpp_ado
    FROM (
        SELECT DISTINCT
            a.grass_region
            , a.level1_category
            , a.order_id
            , a.shop_id
            , a.item_id
            , a.model_id
            , a.item_amount
            , a.gmv_usd gmv
            , a.order_fraction pp
            , sv_rebate_by_seller_amt_usd
            -- , (CASE WHEN (b.item_id IS NOT NULL) THEN 1 ELSE 0 END) campaign
            , CASE WHEN a.item_promotion_source = 'shopee' THEN 1 ELSE 0 END AS campaign
            , CASE WHEN a.item_promotion_source = 'flash_sale' THEN 1 ELSE 0 END flash_sale
            , CASE WHEN a.order_price_pp BETWEEN 1 AND 9 THEN 1 ELSE 0 END AS lpp
            , CASE WHEN a.sv_voucher_code IS NULL and a.pv_voucher_code IS NULL and a.fsv_voucher_code IS NULL THEN 0 ELSE 1 END voucher_order
            , CASE WHEN a.sv_voucher_code like 'SFP-%' OR a.pv_voucher_code like 'SFP-%' THEN 1 ELSE 0 END is_follower_prize_order
            , CASE WHEN (sv_voucher_code IS not NULL and is_sv_seller_absorbed = 1) or is_pv_seller_absorbed = 1 THEN 1 ELSE 0 END seller_voucher
            , CASE WHEN (a.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date)) and x.is_following=1 and  a.grass_date>=follow_date THEN 1 ELSE 0 END is_mtd_follower_order    -- current follower - MTD generated order after following day 
            , CASE WHEN (date_format(a.grass_date,'%y%m') =date_format(date_add('month',-1,date(date_add('day', -1, current_date))),'%y%m'))  and x.is_following=1 and  a.grass_date>=follow_date THEN 1 ELSE 0 END is_lm_follower_order  -- current follower - last month generated order after following day 
            , CASE WHEN (a.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date)) and x.is_mtd_follower=1 and  a.grass_date>=follow_date THEN 1 ELSE 0 END is_mtd_new_follower_order  -- mtd new follower - during mtd generated order after following day 
            , CASE WHEN (date_format(a.grass_date,'%y%m') =date_format(date_add('month',-1,date(date_add('day', -1, current_date))),'%y%m'))  and x.is_lm_follower=1 and  a.grass_date>=follow_date THEN 1 ELSE 0 END is_lm_new_follower_order-- last month follower - during last month generated order after following day 
            , CASE WHEN (a.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date)) and x.is_following=1 and x.is_mtd_follower=0 and  a.grass_date>=follow_date THEN 1 ELSE 0 END is_mtd_old_follower_order --current follower but no mtd new follower - during mtd generated order after following day 
            , CASE WHEN (date_format(a.grass_date,'%y%m') =date_format(date_add('month',-1,date(date_add('day', -1, current_date))),'%y%m')) and x.is_following=1  and x.is_lm_follower=0 and  a.grass_date>=follow_date THEN 1 ELSE 0 END is_lm_old_follower_order --current follower but not last month follower - during last month generated order after following day
            , CASE WHEN (a.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date)) THEN 1 ELSE 0 END MTD
            , CASE WHEN (date_format(a.grass_date,'%y%m') =date_format(date_add('month',-1,date(date_add('day', -1, current_date))),'%y%m')) THEN 1 ELSE 0 END LM
            , (CASE WHEN a.grass_date BETWEEN date_trunc('week', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END) "WTD"
            , (CASE WHEN a.grass_date BETWEEN date_add('day', -7, date_trunc('week', date_add('day', -1, current_date))) AND date_add('day', -1, date_trunc('week', date_add('day', -1, current_date))) THEN 1 ELSE 0 END) "LW"
            -- , (CASE WHEN (a.grass_date BETWEEN date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) AND date_add('day', -1, date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))))) THEN 1 ELSE 0 END) "W_2"      
            , CASE WHEN (a.grass_date =date_add('day',-1,current_date)) THEN 1 ELSE 0 END D_1        
            -- , (CASE WHEN (a.shop_id IN (SELECT DISTINCT TRY_CAST(child_shopid AS int) shopid FROM regbd_sf.cb__seller_index_tab WHERE gp_account_shipping_country = 'Taiwan' AND grass_region IN ('MY','ID','SG'))) THEN 1 ELSE 0 END) "TWC"
        FROM (
            SELECT * 
            FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live 
            WHERE grass_region IN ('MY','ID','SG') 
                AND is_placed = 1
                AND tz_type = 'local'
        ) a
        INNER JOIN focus_seller s
        ON a.shop_id = s.shopid
        -- LEFT JOIN (
        --     SELECT DISTINCT
        --         item_id
        --         , from_unixtime(start_time) start_date
        --         , from_unixtime(end_time) end_date
        --     FROM
        --       regbida_userbehavior.shopee_bi_campaign_item
        --     WHERE grass_region IN ('SG','MY','ID')
        -- ) b
        -- ON a.item_id = b.item_id 
        --   AND a.grass_date >= b.start_date
        --   AND a.grass_date <= b.end_date
        LEFT JOIN regcbbi_others.bi__sip_follower_aggregated_info_twsmt__v3__df__reg__s0 x 
        ON a.buyer_id=x.follower_user_id 
          AND a.shop_id=x.shopid
        WHERE a.grass_date >= date_add('month', -2, date_trunc('month', date_add('day', -1, current_date)))
          AND a.grass_region IN ('SG','MY','ID')
    ) 
    GROUP BY 1
) 

, shop_traffic AS (
    SELECT
      shopid
      , SUM(CASE WHEN MTD = 1 THEN pv_cnt_1d ELSE 0 END) / day(date_add('day', -1, current_date)) MTD_Unique_Views
      , SUM(CASE WHEN LM = 1 THEN pv_cnt_1d ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) LM_Unique_Views
      -- , (SUM((CASE WHEN (WTD=1) THEN pv_cnt_1d ELSE 0 END))/ day_of_week(date_add('day', -1, current_date))) WTD_Views
      -- , (SUM((CASE WHEN (LW=1) THEN pv_cnt_1d ELSE 0 END))/ 7.00) LW_views
      -- , (SUM((CASE WHEN (W_2 = 1) THEN pv_cnt_1d ELSE 0 END)) / DECIMAL '7.0') W_2_views 
    FROM (
        SELECT DISTINCT
            b.shop_id shopid
            , b.pv_cnt_1d
            , b.item_id itemid
            , grass_date
            , CASE WHEN b.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END MTD
            , CASE WHEN date_format(b.grass_date,'%y%m') = date_format(date_add('month',-1,date(date_add('day', -1, current_date))),'%y%m') THEN 1 ELSE 0 END LM
            -- , (CASE WHEN b.grass_date BETWEEN date_trunc('week', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END) WTD
            -- , (CASE WHEN b.grass_date BETWEEN date_add('day', -7, date_trunc('week', date_add('day', -1, current_date))) AND date_add('day', -1, date_trunc('week', date_add('day', -1, current_date))) THEN 1 ELSE 0 END) LW
            -- , (CASE WHEN (b.grass_date BETWEEN date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) AND date_add('day', -1, date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))))) THEN 1 ELSE 0 END) W_2
        FROM (
            SELECT * 
            FROM mp_shopflow.dws_item_civ_1d__reg_s0_live
            WHERE grass_region IN ('MY','ID','SG')
        ) b
        INNER JOIN focus_seller s
        ON b.shop_id = s.shopid
        WHERE grass_region IN ('SG','MY','ID')
            AND grass_date >= date_add('month', -2, date_trunc('month', date_add('day', -1, current_date)))
            AND tz_type = 'local'
    ) 
    GROUP BY 1
)

, sku AS (
    SELECT
        z.shop_id
        , SUM(CASE WHEN z.item_status=1 AND z.item_stock>0 THEN 1 ELSE 0 END) AS live_sku
        , COUNT(distinct item_id) as total_sku
    FROM (
        SELECT DISTINCT 
            a.shop_id
            , item_id
            , status AS item_status
            , stock AS item_stock
        FROM (
            SELECT shop_id
                , item_id
                , status
                , stock
            FROM mp_item.dim_item__reg_s0_live
            WHERE grass_region IN ('MY','ID','SG')
                AND grass_date = current_date - interval '1' day
                AND tz_type = 'local'
        ) a
        INNER JOIN focus_seller s
        ON s.shopid=a.shop_id
    ) z
    GROUP BY 1
)

, follower AS (
    SELECT DISTINCT
        s.shopid
        , t.follower_cnt_td AS total_follower_cnt_td
        -- , fa.wtd_new_followers
        , fa.mtd_new_followers
        -- , fa.lw_new_followers
        , fa.lm_new_followers
        -- , SUM(CASE WHEN p.prize_claim_date BETWEEN (DATE_TRUNC('week',  current_date -interval '1' day)) AND current_date - interval '1' day THEN 1 ELSE 0 END) AS wtd_new_sfp
        , SUM(CASE WHEN p.prize_claim_date BETWEEN DATE_TRUNC('month', current_date - interval '1' day) AND current_date - interval '1' day THEN 1 ELSE 0 END) AS mtd_new_sfp
        -- , SUM(CASE WHEN p.prize_claim_date BETWEEN (DATE_ADD('day', -7, DATE_TRUNC('week',  current_date -interval '1' day))) AND  DATE_ADD('day', -1 , DATE_TRUNC('week',  current_date -interval '1' day)) THEN 1 ELSE 0 END) AS lw_new_sfp
        , SUM(CASE WHEN p.prize_claim_date BETWEEN DATE_TRUNC('month', DATE_ADD('day',-1, DATE_TRUNC('month',  current_date - interval '1' day))) AND DATE_ADD('day',-1, DATE_TRUNC('month',  current_date -interval '1' day)) THEN 1 ELSE 0 END) AS lm_new_sfp
    FROM focus_seller s 
    LEFT JOIN (
        SELECT shopid,
            -- SUM(is_wtd_follower) AS wtd_new_followers,	
            SUM(is_mtd_follower) AS mtd_new_followers,
            -- SUM(is_lw_follower) AS lw_new_followers,
            SUM(is_lm_follower) AS lm_new_followers
        FROM regcbbi_others.bi__sip_follower_aggregated_info_twsmt__v3__df__reg__s0
        GROUP BY 1
    ) fa 
    ON s.shopid= fa.shopid
    LEFT JOIN (
        SELECT DISTINCT --to filter out those who followed long ago but only claimed recently (avoid neg value for organic follower)
            shop_id,
            MAX(DATE(FROM_UNIXTIME(create_time))) as prize_claim_date 
        FROM marketplace.shopee_follow_prize_follow_map_db__user_follow_shop_map_tab__reg_daily_s0_live 
        WHERE DATE(FROM_UNIXTIME(create_time)) >= (DATE_TRUNC('month', DATE_ADD('day',-1, DATE_TRUNC('month', current_date))))
        GROUP BY 1
    ) p
    ON p.shop_id = s.shopid
    LEFT JOIN (
        SELECT DISTINCT shop_id, 
            follower_cnt_td
        FROM mp_user.dws_shop_interaction_td__reg_s0_live
        WHERE grass_date = current_date - interval '1' day
            AND tz_type = 'local'
            AND grass_region IN ('SG', 'MY')
    ) t
    ON t.shop_id = s.shopid 
    GROUP BY 1,2,3,4
)

, organic_follower AS (
    SELECT DISTINCT
        shopid
        -- , SUM(wtd_new_followers-wtd_new_sfp) AS wtd_organic_new_follower 
        , SUM(mtd_new_followers-mtd_new_sfp) AS mtd_organic_new_follower 
        -- , SUM(lw_new_followers-lw_new_sfp) AS lw_organic_new_follower 
        , SUM(lm_new_followers-lm_new_sfp) AS lm_organic_new_follower 
    FROM follower
    GROUP BY 1
)

, pa_ord AS (
    SELECT DISTINCT
        s.shopid, 
        -- SUM(CASE WHEN m.grass_date BETWEEN (DATE_TRUNC('week', current_date)) AND current_date - interval '1' day THEN m.order_cnt ELSE 0 END) / CAST( day_of_week(date_add('day', -1, current_date)) as DOUBLE)    AS wtd_pa_order,	
        SUM(CASE WHEN m.grass_date BETWEEN DATE_TRUNC('month', current_date - interval '1' day) AND current_date - interval '1' day THEN m.order_cnt ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS DOUBLE)  AS mtd_pa_order,
        -- SUM(CASE WHEN m.grass_date BETWEEN (DATE_ADD('day', -7, DATE_TRUNC('week', current_date))) AND  DATE_ADD('day', -1 , DATE_TRUNC('week', current_date)) THEN m.order_cnt ELSE 0 END)  / 7.0    AS lw_pa_order,
        SUM(CASE WHEN m.grass_date BETWEEN DATE_TRUNC('month', DATE_ADD('day',-1, DATE_TRUNC('month', current_date - interval '1' day))) AND DATE_ADD('day',-1, DATE_TRUNC('month', current_date - interval '1' day)) THEN m.order_cnt ELSE 0 END) / day(date_add('day', -1, date_trunc('month', date_add('day', -1,current_date))))  AS lm_pa_order
    FROM focus_seller s 
    LEFT JOIN (
        SELECT DISTINCT
            shop_id, 
            grass_date,
            SUM(order_cnt) AS order_cnt
        FROM mp_paidads.ads_advertise_mkt_1d__reg_s0_live 
        WHERE grass_date >= DATE_TRUNC('month', DATE_ADD('day',-1, DATE_TRUNC('month', current_date - interval '1' day)))
            AND tz_type = 'local' 
            AND grass_region IN ('MY', 'SG')
        GROUP BY 1,2
    ) m
    ON s.shopid = m.shop_id
    GROUP BY 1 
)

, first_purchase AS (
    SELECT
        o.shop_id
        , buyer_id
        , first_order_time
        , CASE WHEN date(first_order_time) BETWEEN date_trunc('month', current_date - interval '1' day) AND current_date - interval '1' day THEN 'MTD' 
            WHEN date(first_order_time) BETWEEN date_add('month', -1, date_trunc('month', current_date - interval '1' day)) AND date_add('day', -1, date_trunc('month', current_date - interval '1' day)) THEN 'M1'
            ELSE 'earlier_M1' END AS first_buy
    FROM (
        SELECT 
            shop_id
            , buyer_id
            , MIN(from_unixtime(create_timestamp)) AS first_order_time
        FROM mp_order.dwd_order_all_ent_df__reg_s0_live
        WHERE grass_region IN ('SG', 'MY')
            AND tz_type = 'local'
            AND is_cb_shop = 1
        GROUP BY 1,2
    ) AS o
    JOIN focus_seller
    ON o.shop_id = focus_seller.shopid
)

, repeat_purchase AS (
    SELECT
        first_purchase.shop_id
        , first_purchase.buyer_id
        , first_purchase.first_buy
        , IF(date(MIN(o.create_time)) BETWEEN date_add('month', -1, date_trunc('month', current_date - interval '1' day)) AND date_add('day', -1, date_trunc('month', current_date - interval '1' day)), 1, 0) AS M1_repeat_buyer
        , IF(date(MAX(o.create_time)) BETWEEN date_trunc('month', current_date - interval '1' day) AND current_date - interval '1' day, 1, 0) AS MTD_repeat_buyer
    FROM first_purchase
    LEFT JOIN (
        SELECT
            shop_id
            , buyer_id
            , from_unixtime(create_timestamp) AS create_time
        FROM mp_order.dwd_order_all_ent_df__reg_s0_live
        WHERE grass_region IN ('SG', 'MY')
            AND tz_type = 'local'
            AND is_cb_shop = 1
            AND grass_date >= date_add('month', -1, date_trunc('month', current_date - interval '1' day)) - interval '1' day
            AND date(from_unixtime(create_timestamp)) >= date_add('month', -1, date_trunc('month', current_date - interval '1' day))
    ) AS o
    ON o.shop_id = first_purchase.shop_id
        AND o.buyer_id = first_purchase.buyer_id
        AND o.create_time <> first_purchase.first_order_time
    GROUP BY 1,2,3
)

, buyer_summary AS (
    SELECT
        o.shop_id
        , COUNT(distinct CASE WHEN first_buy = 'MTD' THEN o.buyer_id ELSE null END) AS MTD_first_time_buyer
        , COUNT(distinct CASE WHEN first_buy = 'M1' THEN o.buyer_id ELSE null END) AS M1_first_time_buyer
        , COUNT(distinct CASE WHEN MTD_repeat_buyer = 1 THEN o.buyer_id ELSE null END) AS MTD_repeat_buyer
        , COUNT(distinct CASE WHEN M1_repeat_buyer = 1 THEN o.buyer_id ELSE null END) AS M1_repeat_buyer
        , COUNT(distinct CASE WHEN create_date BETWEEN date_trunc('month', current_date - interval '1' day) AND current_date - interval '1' day THEN o.buyer_id ELSE null END) AS MTD_unique_buyer
        , COUNT(distinct CASE WHEN create_date BETWEEN date_add('month', -1, date_trunc('month', current_date - interval '1' day)) AND date_add('day', -1, date_trunc('month', current_date - interval '1' day)) THEN o.buyer_id ELSE null END) AS M1_unique_buyer
    FROM (
        SELECT
            shop_id
            , buyer_id
            , order_id
            , date(from_unixtime(create_timestamp)) AS create_date
        FROM mp_order.dwd_order_all_ent_df__reg_s0_live
        WHERE grass_region IN ('SG', 'MY')
            AND tz_type = 'local'
            AND is_cb_shop = 1
            AND grass_date >= date_add('month', -1, date_trunc('month', current_date - interval '1' day)) - interval '1' day
            AND date(from_unixtime(create_timestamp)) >= date_add('month', -1, date_trunc('month', current_date - interval '1' day))
    ) AS o
    JOIN repeat_purchase
    ON o.shop_id = repeat_purchase.shop_id
        AND o.buyer_id = repeat_purchase.buyer_id
    GROUP BY 1
)

SELECT DISTINCT
    u.shopid
    , ua.user_name
    , COALESCE(MTD_Order, 0) AS MTD_Order
    , COALESCE(LM_Order, 0) AS LM_Order
    , COALESCE(MTD_CFS, 0) AS MTD_CFS
    , COALESCE(LM_CFS, 0) AS LM_CFS
    , COALESCE(MTD_Campaign, 0) AS MTD_Campaign
    , COALESCE(LM_Campaign, 0) AS LM_Campaign 
    , COALESCE(MTD_Organic, 0) AS MTD_Organic
    , COALESCE(LM_Organic, 0) AS LM_Organic
    , COALESCE(MTD_seller_voucher, 0) AS MTD_seller_voucher
    , COALESCE(LM_seller_voucher, 0) AS LM_seller_voucher
    , COALESCE(MTD_shopee_voucher,0) AS MTD_shopee_voucher
    , COALESCE(LM_shopee_voucher,0) AS LM_shopee_voucher
    , COALESCE(MTD_GMV, 0) AS MTD_GMV
    , COALESCE(LM_GMV, 0) AS LM_GMV
    , COALESCE(MTD_Unique_Views,0) AS MTD_Unique_Views
    , COALESCE(LM_Unique_Views, 0) AS LM_Unique_Views
 
    -- , COALESCE(wtd_pa_order	, 0) AS WTD_PA_Order
    , COALESCE(mtd_pa_order, 0) AS MTD_PA_Order
    -- , COALESCE(lw_pa_order, 0) AS LW_PA_Order
    , COALESCE(lm_pa_order, 0) AS LM_PA_Order
    -- , COALESCE(WTD_CFS, 0) AS WTD_CFS
    -- , COALESCE(LW_CFS, 0) AS LW_CFS
    -- , COALESCE(WTD_Campaign, 0) AS WTD_Campaign
    -- , COALESCE(LW_Campaign,0) AS LW_Campaign
    -- , COALESCE(WTD_Organic,0) AS WTD_Organic
    -- , COALESCE(LW_Organic,0) AS LW_Organic
    -- , COALESCE(WTD_shopee_voucher,0) AS WTD_shopee_voucher
    -- , COALESCE(LW_shopee_voucher,0) AS LW_shopee_voucher
    -- , COALESCE(WTD_seller_voucher,0) AS WTD_seller_voucher
    -- , COALESCE(LW_seller_voucher,0) AS LW_seller_voucher
    , COALESCE(live_sku,0) AS live_sku
    , COALESCE(total_sku,0) AS total_sku

    -- , COALESCE(WTD_Views,0) AS WTD_Views
    -- , COALESCE(LW_views,0) AS LW_Views
    -- , COALESCE(W_2_gmv,0) AS W_2_GMV
    -- , COALESCE(W_2_views,0) AS W_2_Views
    , is_holiday_mode
    , rating_star
    , COALESCE(D_1_Order,0) AS D_1_Order
    , CASE WHEN is_official_shop = 1 THEN 'Mall Seller'
          WHEN is_official_shop = 0 AND is_preferred_shop = 1 THEN 'Preferred Seller'
          ELSE 'C2C' END AS seller_type
    , COALESCE(total_follower_cnt_td,0) AS total_follower_cnt_td    
    -- , COALESCE(wtd_new_followers,0) AS WTD_new_followers
    , COALESCE(mtd_new_followers,0) AS MTD_new_followers 
    -- , COALESCE(lw_new_followers,0) AS LW_new_followers 
    , COALESCE(lm_new_followers,0) AS LM_new_followers 
    -- , COALESCE(wtd_new_sfp,0) AS WTD_new_sfp 
    , COALESCE(mtd_new_sfp,0) AS MTD_new_sfp 
    -- , COALESCE(lw_new_sfp,0) AS LW_new_sfp 
    , COALESCE(lm_new_sfp,0) AS LM_new_sfp 
    -- , COALESCE(wtd_organic_new_follower,0) AS WTD_organic_new_follower 
    , COALESCE(mtd_organic_new_follower,0) AS MTD_organic_new_follower 
    -- , COALESCE(lw_organic_new_follower,0) AS LW_organic_new_follower 
    , COALESCE(lm_organic_new_follower,0) AS LM_organic_new_follower 
    , COALESCE(MTD_follower_prize_order,0) AS MTD_follower_prize_order
    , COALESCE(LM_follower_prize_order,0) AS LM_follower_prize_order
    , COALESCE(mtd_follower_order,0) AS mtd_follower_order 
    , COALESCE(lm_follower_order,0) AS lm_follower_order 
    , COALESCE(mtd_new_follower_order,0) AS mtd_new_follower_order 
    , COALESCE(lm_new_follower_order,0) AS lm_new_follower_order 
    , COALESCE(mtd_old_follower_order,0) AS mtd_old_follower_order
    , COALESCE(lm_old_follower_order,0) AS lm_old_follower_order
    , COALESCE(MTD_sv_cost_usd, 0) AS MTD_sv_cost_usd
    , COALESCE(LM_sv_cost_usd, 0) AS LM_sv_cost_usd
    , COALESCE(MTD_lpp_ado, 0) AS MTD_lpp_ado
    , COALESCE(LM_lpp_ado, 0) AS LM_lpp_ado
    , COALESCE(MTD_first_time_buyer, 0) AS MTD_first_time_buyer
    , COALESCE(M1_first_time_buyer, 0) AS M1_first_time_buyer
    , COALESCE(MTD_repeat_buyer, 0) AS MTD_repeat_buyer
    , COALESCE(M1_repeat_buyer, 0) AS M1_repeat_buyer
    , COALESCE(MTD_unique_buyer, 0) AS MTD_unique_buyer
    , COALESCE(M1_unique_buyer, 0) AS M1_unique_buyer
    , COALESCE(WTD_Order, 0) AS WTD_Order
     , COALESCE(LW_Order, 0) AS LW_Order
    , COALESCE(WTD_gmv,0) AS WTD_GMV
    , COALESCE(W_1_gmv,0) AS W_1_GMV
FROM focus_seller u
LEFT JOIN shop_performance o ON o.shop_id = u.shopid
LEFT JOIN shop_traffic v ON v.shopid = u.shopid
LEFT JOIN sku ON sku.shop_id=u.shopid
LEFT JOIN (
    SELECT * 
    FROM mp_user.dim_shop__reg_s0_live 
    WHERE grass_region IN ('MY','SG') 
        AND grass_date = current_date - interval '1' day 
        AND tz_type = 'local'
) ua ON ua.shop_id=u.shopid
LEFT JOIN follower ON follower.shopid = u.shopid
LEFT JOIN pa_ord ON pa_ord.shopid = u.shopid
LEFT JOIN organic_follower ON organic_follower.shopid = u.shopid
LEFT JOIN buyer_summary ON buyer_summary.shop_id = u.shopid
