WITH sip AS (
    SELECT DISTINCT b.affi_shopid shopid
    FROM
      marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
    LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b
    ON b.mst_shopid = a.shopid
    WHERE a.cb_option = 0 
      AND a.country = 'TW' 
      AND b.grass_region IN ('MY', 'ID', 'SG')
      AND b.sip_shop_status <> 3
)  
, sales_raw AS (
    SELECT DISTINCT
        o.grass_region
        , o.order_id
        , o.shop_id
        , o.item_id
        , o.model_id
        , CASE WHEN o.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END mtd
        , CASE WHEN date_format(o.grass_date, '%y%m') = date_format(date_add('month', -1, date_add('day', -1, current_date)), '%y%m') THEN 1 ELSE 0 END m_1
        , CASE WHEN o.grass_date BETWEEN date_trunc('week', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN 1 ELSE 0 END wtd
        , CASE WHEN o.grass_date BETWEEN date_add('week', -1, date_trunc('week', date_add('day', -1, current_date))) AND date_add('day', -1, date_trunc('week', date_add('day', -1, current_date))) THEN 1 ELSE 0 END w_1
        , CASE WHEN o.grass_date BETWEEN date_add('week', -2, date_trunc('week', date_add('day', -1, current_date))) AND date_add('day', -8, date_trunc('week', date_add('day', -1, current_date))) THEN 1 ELSE 0 END w_2
        , CASE WHEN o.item_promotion_source = 'shopee' THEN 1 ELSE 0 END campaign
        , CASE WHEN o.item_promotion_source = 'flash_sale' THEN 1 ELSE 0 END flash_sale
        , CASE WHEN is_sv_seller_absorbed = 1 OR is_pv_seller_absorbed = 1 THEN 1 ELSE 0 END seller_voucher
        , CASE WHEN is_sv_seller_absorbed = 0 OR is_pv_seller_absorbed = 0 THEN 1 ELSE 0 END shopee_voucher
        , order_fraction pp
        , gmv
    FROM
     mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o
    INNER JOIN sip s
    ON o.shop_id = s.shopid
  -- LEFT JOIN (
  --     SELECT DISTINCT
  --       campaignid
  --     , itemid
  --     , grass_date
  --     FROM
  --       regbida_userbehavior.shopee_bi_campaign_metrics
  --     WHERE ((grass_country IN ('SG', 'MY', 'ID')) AND (grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date)))))
  --  )  b ON ((o.item_id = b.itemid) AND (o.grass_date = b.grass_date))
    WHERE
      o.grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date)))
      AND o.grass_region IN ('SG', 'MY', 'ID')
      AND o.is_placed = 1
      AND o.tz_type = 'local'
) 
, sales AS (
    SELECT
        grass_region
        , item_id
        , SUM(CASE WHEN mtd = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_ADO
        , SUM(CASE WHEN mtd = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_Organic_Order
        , SUM(CASE WHEN mtd = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_Campaign_Order
        , SUM(CASE WHEN mtd = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_CFS_Order
        , SUM(CASE WHEN mtd = 1 AND seller_voucher = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_Seller_Voucher_Order
        , SUM(CASE WHEN mtd = 1 AND shopee_voucher = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_Shopee_Voucher_Order
        , SUM(CASE WHEN mtd = 1 THEN gmv ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_GMV
        , SUM(CASE WHEN m_1 = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_ADO
        , SUM(CASE WHEN m_1 = 1 AND campaign = 0 AND flash_sale = 0 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_Organic_Order
        , SUM(CASE WHEN m_1 = 1 AND campaign = 1 AND flash_sale = 0 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_Campaign_Order
        , SUM(CASE WHEN m_1 = 1 AND flash_sale = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_CFS_Order
        , SUM(CASE WHEN m_1 = 1 AND seller_voucher = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_Seller_Voucher_Order
        , SUM(CASE WHEN m_1 = 1 AND shopee_voucher = 1 THEN pp ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_Shopee_Voucher_Order
        , SUM(CASE WHEN m_1 = 1 THEN gmv ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_GMV
        , SUM(CASE WHEN wtd = 1 THEN pp ELSE 0 END) / CAST(day_of_week(date_add('day', -1, current_date)) AS double) WTD_ADO
        , SUM(CASE WHEN w_1 = 1 THEN pp ELSE 0 END) / DECIMAL '7.000' W_1_ADO
        , SUM(CASE WHEN w_2 = 1 THEN pp ELSE 0 END) / DECIMAL '7.000' W_2_ADO
    FROM
        sales_raw
    GROUP BY 1, 2
) 
, sales_rank AS (
    SELECT
        *
        , rank() OVER (PARTITION BY grass_region ORDER BY MTD_ADO DESC) TWCB_rank
    FROM sales
) 
, top_20 AS (
   SELECT *
   FROM
     sales_rank
   WHERE TWCB_rank <= 20
) 
, orders as (
    SELECT
        item_id
        , SUM(order_fraction) placed_order_fraction_td
    FROM mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o 
    JOIN sip 
    ON sip.shopid=o.shop_id
    WHERE o.grass_region IN ('ID','MY','SG') 
      AND is_cb_shop=1
      AND tz_type = 'local'
      AND is_placed = 1
    GROUP BY 1
)
, item_info AS (
    SELECT DISTINCT
        u.shop_id shopid
        , u.item_id itemid
        , uu.user_name username
        , name item_name
        , level1_global_be_category main_category
        , placed_order_fraction_td  item_gross_order_sold_all_time
        , rating_total_cnt item_rating_count
        , rating_score item_rating
        , TWCB_rank
        , u.grass_region
    FROM mp_item.dim_item__reg_s0_live u
    INNER JOIN top_20 t
    ON u.item_id = t.item_id
    JOIN mp_user.dim_shop__reg_s0_live uu 
    ON uu.shop_id = u.shop_id
    LEFT JOIN orders o 
    ON o.item_id = u.item_id
    WHERE u.grass_date = date_add('day', -1, current_date)
      AND u.grass_region in ('ID','SG','MY')
      AND u.tz_type='local'
      AND uu.grass_date = date_add('day', -1, current_date)
      AND uu.grass_region in ('ID','SG','MY')
      AND uu.tz_type='local'
) 
, traffic AS (
    SELECT
        exposure.item_id
        , SUM(CASE WHEN exposure.grass_date BETWEEN date_trunc('month', date_add('day', -1, current_date)) AND date_add('day', -1, current_date) THEN pv_cnt_1d ELSE 0 END) / CAST(day(date_add('day', -1, current_date)) AS double) MTD_view
        , SUM(CASE WHEN date_format(exposure.grass_date, '%y%m') = date_format(date_add('month', -1, date_add('day', -1, current_date)), '%y%m') THEN pv_cnt_1d ELSE 0 END) / CAST(day(date_add('day', -1, date_trunc('month', date_add('day', -1, current_date)))) AS double) M_1_view
    FROM
      mp_shopflow.dws_item_civ_1d__reg_s0_live exposure
    INNER JOIN top_20 t
    ON exposure.item_id = t.item_id
    WHERE tz_type = 'local'
      AND exposure.grass_region in ('ID','SG','MY')
      AND grass_date >= date_add('month', -1, date_trunc('month', date_add('day', -1, current_date)))
    GROUP BY 1
) 
SELECT
    u.grass_region
    , u.itemid
    , u.TWCB_rank
    , u.shopid
    , u.username
    , u.item_name
    , u.main_category
    , u.item_rating_count
    , u.item_rating
    , b.MTD_ADO
    , b.MTD_Organic_Order
    , b.MTD_CFS_Order
    , b.MTD_Campaign_Order
    , b.MTD_Seller_Voucher_Order
    , b.MTD_Shopee_Voucher_Order
    , b.MTD_GMV
    , d.MTD_view
    , b.M_1_ADO
    , b.M_1_Organic_Order
    , b.M_1_CFS_Order
    , b.M_1_Campaign_Order
    , b.M_1_Seller_Voucher_Order
    , b.M_1_Shopee_Voucher_Order
    , b.M_1_GMV
    , d.M_1_view
    , b.WTD_ADO
    , b.W_1_ADO
    , b.W_2_ADO
    , u.item_gross_order_sold_all_time
FROM
  item_info u
LEFT JOIN top_20 b
ON u.itemid = b.item_id
LEFT JOIN traffic d
ON u.itemid = d.item_id
ORDER BY 1 ASC, 3 ASC
