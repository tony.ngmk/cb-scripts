WITH
  sip AS (
   SELECT DISTINCT
     t2.affi_shopid
   , mst_shopid
   , t1.country mst_country
   , t2.country affi_country
   , t1.cb_option
--   , affi_itemid
--   , mst_itemid
   FROM
     (marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
   INNER JOIN (
      SELECT DISTINCT
        affi_shopid
      , affi_username
      , mst_shopid
      , country
      FROM
        marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
      WHERE (sip_shop_status <> 3)
     AND GRASS_REGION in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
   )  t2 ON (t1.shopid = t2.mst_shopid))
--   INNER JOIN (
--       SELECT DISTINCT
--         affi_itemid
--       , affi_shopid
--       , mst_itemid
--       FROM
--         SHOPEE.shopee_sip_v2_db__item_map_tab
--   )  t3 ON (t3.affi_shopid = t2.affi_shopid))
--   WHERE (T1.CB_OPTION = 0)
    
    
)

  ---  ('BR', 'MY', 'TW' ) 

, NON_PERFORM AS (
   SELECT DISTINCT
     a.itemid item_id 
   , shopid shop_id
   , country
  , cast(a.grass_date as date)  DATE
   FROM
     marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live a
     join (select
      itemid item_id
     , max(ctime) max_ctime
     from marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
   WHERE ((((((cast(grass_date as date)  >=  DATE '2022-01-01') 
   AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].Name')) = 'status'))
   AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].New')) IN ('4'))) 
   AND (TRY(try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description'))) IS NOT NULL)) 
   AND (country IN ('BR', 'MY', 'TW' )   )) 
   AND (try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description')) LIKE '%Non-Performing Listing%'))
     group by 1
     ) b on b.max_ctime=a.ctime and b.item_id = a.itemid
   WHERE ((((((cast(grass_date as date)  >=  DATE '2022-01-01') 
   AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].Name')) = 'status'))
   AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].New')) IN ('4'))) 
   AND (TRY(try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description'))) IS NOT NULL)) 
   AND (country IN ('BR', 'MY', 'TW' )    )) 
   AND (try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description')) LIKE '%Non-Performing Listing%'))
)



SELECT DISTINCT
DATE
, map.grass_region AFFI_COUNTRY
, MST_COUNTRY
, CB_OPTION
, COUNT(DISTINCT map.affi_itemid) asku
FROM
  ((NON_PERFORM np
INNER JOIN sip ON (sip.mst_shopid = np.SHOP_ID ))

INNER JOIN (
   SELECT DISTINCT item_id
   FROM
     mp_item.dim_item__reg_s0_live
   WHERE ((((grass_date = (current_date - INTERVAL  '1' DAY)) AND (status = 4)) AND (seller_status = 1)) AND (shop_status = 1)) and grass_region IN ('BR', 'MY', 'TW' )
   and is_cb_shop=1 AND is_holiday_mode=0
)  i ON (i.item_id = np.item_id))
join (select distinct primary_item_id mst_itemid 
            ,  item_id affi_itemid 
            , grass_region 
        from mp_item.ads_sip_affi_item_profile__reg_s0_live
        where grass_date = if(grass_region in ('SG','MY','PH','TH','ID','VN','TW'), current_date  -interval'1'day , current_date- interval'2'day  )
        and primary_shop_type=1 and primary_seller_region in ('BR','TW','MY') and status = 0 
        and seller_status=1 and shop_status=1 and is_holiday_mode=0 and primary_item_status=4 
        ) map on map.mst_itemid = i.item_id 
group by 1,2,3,4
