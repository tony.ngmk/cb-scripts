WITH
  sip AS (
   SELECT DISTINCT
     t2.affi_shopid
   , mst_shopid
   , t1.country mst_country
   , t2.country affi_country
   , t1.cb_option
--   , affi_itemid
--   , mst_itemid
   FROM
     (marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
   INNER JOIN (
      SELECT DISTINCT
        affi_shopid
      , affi_username
      , mst_shopid
      , country
      FROM
        marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
      WHERE (sip_shop_status <> 3) AND GRASS_REGION in ('BR', 'MY', 'TW', 'SG', 'TH', 'PH', 'VN','ID','MX')
   )  t2 ON (t1.shopid = t2.mst_shopid))

)
, NON_PERFORM AS (
   SELECT DISTINCT
     a.itemid item_id 
   , shopid shop_id
   , country
  , cast(grass_date as date) date 
   FROM
     marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live a
     join (select
     itemid item_id
     , max(ctime) max_ctime
     from marketplace.shopee_item_audit_log_db__item_audit_tab__reg_continuous_s0_live
   WHERE ((((((cast(grass_date as date)  >=  current_date-interval'1'day   ) 
   AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].Name')) = 'status'))
   AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].New')) IN ('4'))) 
   AND (TRY(try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description'))) IS NOT NULL)) 
   AND (country IN ('BR', 'MY', 'TW', 'SG', 'TH', 'PH', 'VN','ID','MX'))) 
   AND (try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description')) LIKE '%Non-Performing Listing%'))
     group by 1
     ) b on b.max_ctime=a.ctime and b.item_id = a.itemid
   WHERE ((((((cast(grass_date as date)  >=  current_date-interval'1'day   ) 
   AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].Name')) = 'status'))
   AND (TRY(json_extract_scalar(try(from_utf8(data)),'$.Fields[0].New')) IN ('4'))) 
   AND (TRY(try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description'))) IS NOT NULL)) 
   AND (country IN ('BR', 'MY', 'TW', 'SG', 'TH', 'PH', 'VN','ID','MX'))) 
   AND (try(json_extract_scalar(json_extract_scalar(try(from_utf8(data)),'$.ReportReason'),'$[0].description')) LIKE '%Non-Performing Listing%'))
)



SELECT DISTINCT
DATE
, AFFI_COUNTRY
, MST_COUNTRY
, CB_OPTION
, COUNT(DISTINCT NP.shop_id) ashop 
FROM
NON_PERFORM np


INNER JOIN (
select distinct shop_id 
            
from regcbbi_others.shop_profile
where grass_date =current_date-interval'2'day and is_cb_shop=1 
and grass_region in  ('BR', 'MY', 'TW', 'SG', 'TH', 'PH', 'VN','ID','MX')
and user_status=1 and status =1 and is_holiday_mode=0 and active_item_with_stock_cnt>0
) u ON (u.shop_id = np.shop_id )
join (select distinct shop_id 
            
from regcbbi_others.shop_profile
where grass_date =current_date-interval'1'day and is_cb_shop=1 
and grass_region in  ('BR', 'MY', 'TW', 'SG', 'TH', 'PH', 'VN','ID','MX')
and user_status=1 and status =1 and is_holiday_mode=0 and active_item_with_stock_cnt=0
) u2 on u2.shop_id = u.shop_id 
INNER JOIN sip ON (sip.AFFI_SHOPID = np.SHOP_ID )

group by 1,2,3,4

