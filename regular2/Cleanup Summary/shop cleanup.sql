WITH sip
AS (
	SELECT DISTINCT affi_shopid,
		mst_shopid,
		t1.country mst_country,
		t2.country affi_country,
		t1.cb_option
	FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
	INNER JOIN (
		SELECT DISTINCT affi_shopid,
			affi_username,
			mst_shopid,
			country
		FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
		WHERE (sip_shop_status <> 3)
			AND grass_region <> 'o'
		) t2 ON (t1.shopid = t2.mst_shopid)
	)
SELECT sip.*,
	u.*,
	l30d_ado
FROM sip
INNER JOIN (
	SELECT DISTINCT shop_id,
		STATUS,
		user_status,
		is_holiday_mode,
		active_item_cnt,
		active_item_with_stock_cnt,
		DATE (from_unixtime(shop_first_listing_timestamp)) shop_first_listing_timestamp
	FROM regcbbi_others.shop_profile
	WHERE (grass_date = (CURRENT_DATE - INTERVAL '1' DAY))
		AND (is_cb_shop = 1)
		AND (STATUS = 1)
		AND (user_status = 1)
		AND (is_holiday_mode = 0)
		AND (grass_region IN ('MY', 'TH', 'PH', 'VN'))
		AND (DATE (from_unixtime(shop_first_listing_timestamp)) < (CURRENT_DATE - INTERVAL '60' DAY))
	) u ON (u.shop_id = sip.affi_shopid)
INNER JOIN (
	SELECT DISTINCT shop_id,
		DATE (from_unixtime(min(create_timestamp))) shop_first_listing_timestamp
	FROM mp_item.dim_item__reg_s0_live
	WHERE (grass_date = (CURRENT_DATE - INTERVAL '1' DAY))
		AND (is_cb_shop = 1)
		AND (STATUS = 1)
		AND (seller_status = 1)
		AND (shop_status = 1)
		AND (is_holiday_mode = 0)
		AND grass_region IN ('MY', 'TH', 'PH', 'VN')
		AND tz_type = 'local'
	GROUP BY 1
	) ii ON (ii.shop_id = u.shop_id)

LEFT JOIN (
	SELECT DISTINCT shop_id
	FROM mp_order.dwd_order_all_ent_df__reg_s0_live
	WHERE (is_cb_shop = 1)
		AND DATE (split(create_datetime, ' ') [ 1 ]) BETWEEN (CURRENT_DATE - INTERVAL '360' DAY)
			AND (CURRENT_DATE - INTERVAL '1' DAY)
		AND (grass_region IN ('MY', 'TH', 'PH', 'VN'))
		AND grass_date >= CURRENT_DATE - INTERVAL '360' DAY
		AND grass_region = 'local'
	) o ON (o.shop_id = sip.affi_shopid)
LEFT JOIN (
	SELECT DISTINCT shop_id,
		(count(DISTINCT order_id) / DECIMAL '30.00') l30d_ado
	FROM mp_order.dwd_order_all_ent_df__reg_s0_live
	WHERE (tz_type = 'local')
    AND grass_date >= CURRENT_DATE - INTERVAL '30' DAY
		AND DATE (split(create_datetime, ' ') [ 1 ]) BETWEEN (CURRENT_DATE - INTERVAL '30' DAY)
			AND (CURRENT_DATE - INTERVAL '1' DAY)
	GROUP BY 1
	) oo ON (oo.shop_id = sip.mst_shopid)

LEFT JOIN (
	SELECT DISTINCT shop_id
	FROM mp_item.dim_item__reg_s0_live
	WHERE (grass_date = (CURRENT_DATE - INTERVAL '1' DAY))
		AND (is_cb_shop = 1)
		AND (STATUS = 1)
		AND (seller_status = 1)
		AND (shop_status = 1)
		AND (is_holiday_mode = 0)
		AND DATE (from_unixtime(create_timestamp)) BETWEEN (CURRENT_DATE - INTERVAL '30' DAY)
			AND (CURRENT_DATE - INTERVAL '1' DAY)
		AND (grass_region IN ('MY', 'TH', 'PH', 'VN'))
	) i ON (i.shop_id = sip.affi_shopid)
WHERE (o.shop_id IS NULL)
	AND (i.shop_id IS NULL)
	AND (ii.shop_first_listing_timestamp < (CURRENT_DATE - INTERVAL '60' DAY))
	AND (l30d_ado <= 100)

