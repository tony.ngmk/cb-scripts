/*drop_tn as (
    SELECT orderid
    , shopid
    , from_utf8(data) as remarks
    , regexp_extract(from_utf8(data),'(#)(.*)(\\t#)',2) as drop_lm_tracking_number
    -- , coalesce(regexp_extract(from_utf8(data),'(\\\\t)([A-Z]+\d+[A-Z]*)(\\\\t)',2), regexp_extract(from_utf8(data),'(\s)([A-Z]+\d+[A-Z]*)(\s)',2)) as drop_lm_tracking_number
    from marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live
    where grass_region = 'ID'
    and regexp_extract(from_utf8(data),'(\\tDropship)') is not null OR regexp_extract(from_utf8(data),'(\\tOthers)') is not null
    -- and (regexp_like(from_utf8(data),'(\\\\t)([A-Z]+\d+[A-Z]*)(\\\\t)')
    --     or regexp_like(from_utf8(data),'(\s)([A-Z]+\d+[A-Z]*)(\s)')
    --     )
    )*/
    

WITH drop_tn AS (
    SELECT DISTINCT
        orderid
        --, comment 
        , drop_lm_tracking_number
        , ''remarks 
    FROM (
        SELECT 
            orderid
            --, comment
            , regexp_extract(comment,'(?<=\#)(.*?)(?=\#)') drop_lm_tracking_number
        FROM marketplace.shopee_order_processing_backend_id_db__order_comment_tab__id_daily_s0_live
        WHERE regexp_extract(comment,'Dropship') is not null 
            AND date(from_unixtime(ctime))>= date_add('day', -60, current_date)
    )
)
, sip_shop AS (
    SELECT DISTINCT 
        b.affi_shopid
        , b.mst_shopid
        , a.country as p_country
        , b.country as a_country
        , admin_holiday_mode_on
    FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live a
    LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b 
    ON b.mst_shopid = a.shopid 
    WHERE a.cb_option = 0
        AND a.country in ('ID')
        AND b.grass_region != ''
)
, l30d AS (
  SELECT 
    shop_id
    , COUNT(DISTINCT order_sn) l30d_orders
  FROM mp_order.dwd_order_all_ent_df__reg_s0_live
  WHERE
    grass_region IN ('ID') 
    AND DATE(FROM_UNIXTIME(create_timestamp)) >= CURRENT_DATE - INTERVAL '30' DAY 
    AND tz_type = 'local' 
    AND grass_date >= current_date - interval '31' day
  GROUP BY 1
)

SELECT 
    *
    , num_order_caught/ (l30d_orders + 0.00) percentage 
FROM (
    SELECT DISTINCT 
        s2.mst_shopid
        , s2.affi_shopid
        , count(DISTINCT a_ordersn) num_order_caught
        , l30d_orders
    FROM (
        SELECT DISTINCT
            a_o.create_time a_purchase_date
            , a_o.grass_region a_grass_region
            , a_o.orderid a_orderid
            , a_o.ordersn a_ordersn
            , a_o.shop_id a_shopid
            , a_s.status a_shop_status
            , a_s.user_status a_user_status
            , a_s.is_holiday_mode a_is_holiday_mode
            , p_o_real.orderid p_orderid_real
            , p_o_real.ordersn p_ordersn_real
            , p_req_real.lm_tracking_number p_LMTN_real
            -- , p_o_drop.orderid p_orderid_drop
            , p_req_drop.ordersn p_ordersn_drop
            , drop_tn.drop_lm_tracking_number p_LMTN_drop
            , drop_tn.remarks
        FROM (
            SELECT 
                order_sn as ordersn 
                , order_id as orderid
                , buyer_id as userid
                , logistics_status_id as logistics_status
                , create_datetime as create_time
                , grass_region
                , grass_date
                , create_timestamp
            FROM mp_order.dwd_order_all_ent_df__reg_s0_live
            WHERE tz_type = 'local'
        ) p_o_real 
        JOIN drop_tn 
        ON drop_tn.orderid = p_o_real.orderid
        JOIN (
            SELECT
                oversea_orderid 
                , local_orderid 
            FROM marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live 
            WHERE local_orderid != 0
        ) map 
        ON map.local_orderid = p_o_real.orderid 
        LEFT JOIN (
            SELECT 
                order_sn AS ordersn 
                , order_id AS orderid
                , buyer_id AS userid
                , shop_id
                , logistics_status_id AS logistics_status
                , create_datetime AS create_time
                , grass_region
                , grass_date
                , create_timestamp
            FROM mp_order.dwd_order_all_ent_df__reg_s0_live
            WHERE tz_type = 'local'
        ) a_o 
        ON map.oversea_orderid = a_o.orderid 
        LEFT JOIN (
            SELECT DISTINCT 
                ordersn
                , lm_tracking_number
            FROM sls_mart.shopee_ssc_slsorder_id_db__logistic_request_tab__reg_daily_s0_live
        ) p_req_real 
        ON p_o_real.ordersn = p_req_real.ordersn
        LEFT JOIN (
            SELECT DISTINCT 
                ordersn
                , lm_tracking_number
            FROM sls_mart.shopee_ssc_slsorder_id_db__logistic_request_tab__reg_daily_s0_live
        ) p_req_drop 
        ON drop_tn.drop_lm_tracking_number = p_req_drop.lm_tracking_number
-- left join shopee.order_mart__order_profile p_o_drop on p_o_drop.ordersn = p_req_drop.ordersn
        LEFT JOIN (
            SELECT
                shop_id
                , status
                , user_status
                , is_holiday_mode 
            FROM regcbbi_others.shop_profile 
            WHERE grass_region IN ('SG','MY','TW','PH','TH','VN','BR','MX','CO','CL') 
                AND grass_date = current_date - interval '1' day 
                AND tz_type = 'local' 
                AND is_cb_shop = 1
        ) a_s 
        ON a_s.shop_id = a_o.shop_id
        WHERE p_o_real.grass_region in ('ID')
            AND a_o.grass_region != ''
            AND from_unixtime(p_o_real.create_timestamp) BETWEEN date_add('day', -30, current_date) AND date_add('day', -1, current_date)
            AND from_unixtime(a_o.create_timestamp) BETWEEN date_add('day', -30, current_date) AND date_add('day', -1, current_date)
            AND p_req_real.lm_tracking_number <> drop_tn.drop_lm_tracking_number
    ) a 
    INNER JOIN sip_shop s1 
    ON a.a_shopid = s1.affi_shopid
    LEFT JOIN sip_shop s2 
    ON s2.mst_shopid = s1.mst_shopid
    LEFT JOIN l30d 
    ON l30d.shop_id = s2.mst_shopid
    WHERE s2.admin_holiday_mode_on = 0
    GROUP BY 1,2,4
)
WHERE num_order_caught/ (l30d_orders + 0.00) >= 0.05
ORDER BY 5 DESC

