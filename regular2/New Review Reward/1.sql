insert into nexus.test_84c67cf2514b9ca3c4d1bf6c64e64538b72b2cc2ac2d60ce391fbbc09b2293ff_9e74feb21aa06f5507c41b793e486168 SELECT count(DISTINCT a.conversation_id) message,
a.grass_region,
grass_date
-- FROM shopee.webchat__seller a
FROM marketplace.webchat__seller_v3__reg_hourly_s0_live a
INNER JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live b 
ON b.affi_shopid = a.from_shop_id
WHERE a.grass_date = CURRENT_DATE - interval '1' day
AND a.grass_region IN ('SG', 'MY', 'PH', 'TH', 'ID', 'VN', 'TW', 'MX', 'BR', 'CO', 'CL', 'PL', 'ES', 'FR')
AND b.grass_region IN ('SG', 'MY', 'PH', 'TH', 'ID', 'VN', 'TW', 'MX', 'BR', 'CO', 'CL', 'PL', 'ES', 'FR')
AND (
content LIKE '%Hi! Thank you for purchasing our product, we hope you liked it. Your support means alot to us. Please consider giving a 5 star review and we look forward to serving you again, stay safe~%'
OR content LIKE '%Chào bạn, cảm ơn bạn đã mua hàng tại shop, shop hy vọng bạn hài lòng với sản phẩm và sự ủng hộ của bạn rất có ý nghĩa với shop. Bạn vui lòng cho shop xin đánh giá 5 sao, shop rất mong được tiếp tục phục vụ bạn ạ, xin chúc bạn nhiều sức khỏe và niềm vui.%'
OR content LIKE '%สวัสดีค่ะคุณลูกค้า ขอขอบคุณลูกค้าทุกท่านที่ซื้อสินค้าของเรา เราหวังว่าคุณจะชอบสินค้าทุกชิ้นที่เลือกซื้อนะคะ หากคุณชื่นชอบสินค้าของเราโปรดให้รีวิว 5 ดาว เราหวังว่าจะได้ให้บริการคุณอีกครั้ง ขอบคุณค่ะ ~%'
OR content LIKE '%Hai! Terima kasih telah membeli produk kami, kami harap kamu menyukainya. Dukungan kamu sangat berarti bagi kami. Mohon berikan kami bintang 5, kami berharap dapat melayani kamu lagi, tetap aman ya ~%'
OR content LIKE '%親愛的顧客您好，感謝您購買及支持我們店鋪的商品! 若您還滿意我們的商品，請給予我們五星好評價+留言，也希望未來能夠再為您服務喔!%'
OR content LIKE '%Oi! Obrigado por adquirir nosso produto, esperamos que tenha gostado. Seu apoio significa muito para nós. Por favor, considere dar uma avaliação de 5 estrelas, estamos ansiosos para atendê-lo novamente, fique seguro ~%'
) 
GROUP BY a.grass_region,
a.grass_date
ORDER BY a.grass_region