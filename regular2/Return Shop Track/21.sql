insert into nexus.test_0de3ab17cf217053452110ac58234df9bb45bfbd49d26b918b4cdd4ef33c6ccb_e7d04e10b6776e40a1e364b07d71c060 -- qn1 -- how many sku have been add discount price 
-- qn2 -- those no discount price add, what is the price change % for different seller tye 

--- shop --- live sku --- live model --- model edit y/n y--- price up/down --- avg 

with t as (select distinct cast(affi_shopid as BIGINT) affi_shopid 
, batch 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0
where batch='mass return batch1.5'
)
, 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, case when date(from_unixtime(offboard_time)) !=date'1970-01-01' then from_unixtime(offboard_time)
else 
date'2022-05-17' end 
offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
and date(from_unixtime(offboard_time)) != date'2022-01-11'
) t2 ON t1.shopid = t2.mst_shopid
where t1.cb_option= 1 
)
, cb as (SELECT DISTINCT (CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN (LOWER(child_account_owner_userrole_name) LIKE '%lt%' or LOWER(child_account_owner_userrole_name) LIKE '%css%') THEN 'Long Tail'
ELSE 'Others' END) seller_type 
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, grass_region
, child_account_name
, child_account_owner
, CAST(child_shopid AS BIGINT) child_shopid
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
--- where LOWER(child_account_owner_userrole_name) like '%mt%' or LOWER(child_account_owner_userrole_name) LIKE '%lt%'
)


SELECT DISTINCT
batch 
, seller_type 


, count(distinct m.model_id) live_model 
, count(distinct case when m.model_price < m.model_price_before_discount then m.model_id else null end ) add_dp_model 


, count(distinct case when m.model_price < m2.model_price then m.model_id else null end ) cheaper_model
, count(distinct case when m.model_price = m2.model_price then m.model_id else null end ) equal_model
, count(distinct case when m.model_price > m2.model_price then m.model_id else null end ) expensive_model


-- , count(distinct case when m.model_price < m2.model_price and m.model_price = m.model_price_before_discount then m.model_id else null end ) add_dp_cheaper_model
-- , count(distinct case when m.model_price = m2.model_price and m.model_price = m.model_price_before_discount then m.model_id else null end ) add_dp_equal_model
-- , count(distinct case when m.model_price > m2.model_price and m.model_price = m.model_price_before_discount then m.model_id else null end ) add_dp_expensive_model


-- , avg(m.model_price_usd) aft_model_price 
-- , avg(m2.model_price_usd) bef_model_price
FROM (select 
item_id 
, shop_id 
, model_id 
, model_price
, model_price_before_discount
, model_price_usd
from 
mp_item.dim_model__reg_s0_live m 
join t on t.affi_shopid = m.shop_id 
WHERE tz_type = 'local' 
and seller_status = 1 and m.shop_status = 1 and model_status = 1 and item_status = 1 and is_cb_shop = 1 and is_holiday_mode = 0 
and grass_date= current_date-interval'1'day 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
) m 
join (select 
item_id 
, shop_id 
, model_id 
, model_price
, model_price_usd
from 
mp_item.dim_model__reg_s0_live m 
join t on t.affi_shopid = m.shop_id 
WHERE tz_type = 'local' 
and seller_status = 1 and shop_status = 1 and model_status = 1 and item_status = 1 and is_cb_shop = 1 and is_holiday_mode = 0 
and grass_date= date'2022-06-29' - interval'2'day 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')) m2 on m2.model_id= m.model_id 
join t on t.affi_shopid = m.shop_id 
join sip on sip.affi_shopid = t.affi_shopid 
join cb on cb.child_shopid = sip.mst_shopid 
-- where m.model_price = m.model_price_before_discount 
-- limit 100 
where seller_type !='Others'
group by 1 , 2