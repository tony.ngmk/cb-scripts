insert into nexus.test_00515eb34b78a884711c58d03cee0cb8ddbc9d4ebfdf4834acb943feaca60ed6_3dc5421d69fc90b6919b38891839ed92 --- shop --- live sku --- live model --- model edit y/n y--- price up/down --- avg 


with t as (select distinct cast(affi_shopid as BIGINT) affi_shopid 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0
where batch='mass return batch1'
)


SELECT DISTINCT
--m.grass_date, 
--count(distinct m.model_id) ,
try(count(distinct CASE WHEN m.model_price < m.model_price_before_discount THEN m.model_id END )*decimal'1.000'/count(distinct m.model_id)) model_with_dp_perc 
, try(count(distinct CASE WHEN m.model_price < m.model_price_before_discount and p2.model_id is not null THEN m.model_id END )*decimal'1.000'/count(distinct m.model_id)) model_with_dp_perc_sip 
, try(count(distinct CASE WHEN m.model_price < m.model_price_before_discount and p2.model_id is null THEN m.model_id END )*decimal'1.000'/count(distinct m.model_id)) model_with_dp_perc_seller 
FROM (select distinct 
item_id 
, shop_id 
, model_id 
, model_price
, model_price_before_discount
, grass_date
from 
mp_item.dim_model__reg_s0_live m 
WHERE m.tz_type = 'local' 
and m.seller_status = 1 and m.shop_status = 1 and model_status = 1 and m.item_status = 1 and m.is_cb_shop = 1 and m.is_holiday_mode = 0 
and m.grass_date= current_date-interval'1'day 
and m.grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
) m 
join t on t.affi_shopid = m.shop_id 
left join (SELECT a.model_id
, b.start_datetime
FROM mp_promo.dim_seller_discount_sku__reg_live a 
JOIN (SELECT promotion_id 
, date(start_datetime) start_datetime
FROM mp_promo.dim_seller_discount__reg_live 
WHERE REGEXP_LIKE(promotion_name , 'SIP_SHOP_RETURN_MASS_BATCH1')
AND tz_type = 'local' AND grass_date = current_date - interval '1' day AND grass_region IN ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')) b 
ON a.promotion_id = b.promotion_id
where tz_type = 'local' AND grass_date = current_date - interval '1' day AND grass_region IN ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')


) p2 on p2.model_id = m.model_id and start_datetime <= grass_date