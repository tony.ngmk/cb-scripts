insert into nexus.test_4c33d6187093f2146a42c0d4b3d5206d3307fe05d8d67dea51349cd6ab49e7d1_82cb453aaa3c4ad89fae9da7c566d410 -- exclude batch 4 
-- split by seller type 
--- split by organic 
--- exclude dday 
with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, date'2022-06-13' offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and date(from_unixtime(offboard_time)) != date'2022-01-11'
) t2 ON t1.shopid = t2.mst_shopid
)
, cnsip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, date'2022-06-13' offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status!=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')


) t2 ON t1.shopid = t2.mst_shopid
where t1.cb_option=1 
)


, t as (select 
sip.affi_shopid 
, date(offboard_time) offboard_time
, batch 
, mst_shopid 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT)
where a.batch in ('mass return batch1')
)


-- select * 
-- from t 
, cb as (SELECT DISTINCT (CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN (LOWER(child_account_owner_userrole_name) LIKE '%lt%' or LOWER(child_account_owner_userrole_name) LIKE '%css%') THEN 'Long Tail'
ELSE 'Others' END) seller_type 
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, grass_region
, child_account_name
, child_account_owner
, CAST(child_shopid AS BIGINT) child_shopid
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
)


, orders as (
select shop_id 
, date(create_datetime) create_datetime
, order_sn 
, order_id 
, sum(gmv) gmv 
, sum(gmv_usd) gmv_usd
, sum(case when is_flash_sale = 0 and item_promotion_source!='shopee' then 0 else 1 end ) organic_order --organic orders only 
from mp_order.dwd_order_item_all_ent_df__reg_s0_live
where is_cb_shop= 1 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and grass_date>= date'2022-03-01' and date(create_datetime)>= date'2022-03-01' 
and date(create_datetime) not in (date'2022-01-01' , date'2022-03-03', date'2022-04-04', date'2022-05-05', date'2022-06-06',date'2022-07-07')
group by 1,2,3,4 
)
, day_diff as (
select count(distinct date_id) - 2 day_diff
from regbida_keyreports.dim_date 
where date_id not in ( date'2022-05-05',date'2022-06-06',date'2022-07-07')
and 
date_id between date'2022-06-13' and current_date-interval'1'day 
)






, batch as (
select case when o.create_datetime < offboard_time then 'before'
when o.create_datetime between offboard_time and offboard_time+ interval'1'day then 'return'
else 'after' end as time_range 
, o.create_datetime 
, case when o.create_datetime < offboard_time then concat('D-',cast(date_diff('day', o.create_datetime, offboard_time) as varchar ))
when o.create_datetime =offboard_time then 'D-0'
when o.create_datetime= offboard_time+ interval'1'day then 'D+0'
else concat('D+',cast(date_diff('day', offboard_time+interval'1'day , o.create_datetime) as varchar )) end as period 
, batch 
, seller_type 
-- , case when organic_order = 0 then 'Y' else 'N' end as organic_order
, o.shop_id 
, count(distinct o.order_sn) orders 
, count(distinct case when organic_order= 0 then o.order_sn else null end ) organic_order
, count(distinct case when organic_order= 1 then o.order_sn else null end ) inorganic_order 
-- , sum(gmv) gmv 
, sum(gmv_usd) gmv_usd
, sum(case when organic_order= 0 then gmv_usd else null end) organic_gmv_usd 
, sum(case when organic_order= 1 then gmv_usd else null end) inorganic_gmv_usd 
from orders o 
join t on t.affi_shopid = o.shop_id 
left join cb on cb.child_shopid = t.mst_shopid 
where o.create_datetime between offboard_time- interval'14'day and offboard_time+interval'31'day 
group by 1,2,3 , 4 , 5 , 6
-- , 7 
order by 2 
)




, cnsip_order as (
select o.create_datetime 
, case when organic_order = 0 then 'Y' else 'N' end as organic_order
, count(distinct o.order_sn) orders 
, sum(gmv) gmv 
, sum(gmv_usd) gmv_usd
from orders o 
join cnsip on cnsip.affi_shopid = o.shop_id 
where cnsip.cb_option=1 
group by 1,2
order by 2 
)
, cncb as (
select o.create_datetime 
, case when organic_order = 0 then 'Y' else 'N' end as organic_order
, count(distinct o.order_sn) orders 
, sum(gmv) gmv 
, sum(gmv_usd) gmv_usd
from orders o 

left join cnsip on cnsip.affi_shopid = o.shop_id 
where cnsip.affi_shopid is null 
group by 1,2
)


select 
batch 
, seller_type 
, sum(case when time_range = 'before' then orders else null end)/13.00 bef_ado 
, sum(case when time_range = 'after' then orders else null end)* decimal'1.000' /day_diff aft_ado 
, sum(case when time_range = 'before' then organic_order else null end)/13.00 bef_organic_ado 
, sum(case when time_range = 'after' then organic_order else null end)* decimal'1.000' /day_diff aft_organic_ado 
, sum(case when time_range = 'before' then inorganic_order else null end)/13.00 bef_inorganic_ado 
, sum(case when time_range = 'after' then inorganic_order else null end)* decimal'1.000' /day_diff aft_inorganic_ado 


, sum(case when time_range = 'before' then gmv_usd else null end)/13.00 bef_gmv_usd 
, sum(case when time_range = 'after' then gmv_usd else null end)* decimal'1.000' /day_diff aft_gmv_usd 
, sum(case when time_range = 'before' then organic_gmv_usd else null end)/13.00 bef_organic_gmv_usd 
, sum(case when time_range = 'after' then organic_gmv_usd else null end)* decimal'1.000' /day_diff aft_organic_gmv_usd 
, sum(case when time_range = 'before' then inorganic_gmv_usd else null end)/13.00 bef_inorganic_gmv_usd 
, sum(case when time_range = 'after' then inorganic_gmv_usd else null end)* decimal'1.000' /day_diff aft_inorganic_gmv_usd 
from batch b1 
join day_diff on 1=1 
group by 1 , 2 , day_diff