insert into nexus.test_1cbcf92eba8f8bb1de879efda187d07ba0b1eca1393b7f76ef001151a64f97bf_ed29f9b3faab8aff82aa0417a310dc41 with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, from_unixtime(mtime) offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')


) t2 ON t1.shopid = t2.mst_shopid
)


, t as (select sip.affi_shopid 
, date(offboard_time) offboard_time
, batch 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT)
where a.batch='batch5'
)
, orders as (
select shop_id 
, date(create_datetime) create_datetime
, order_sn 
, order_id 
from mp_order.dwd_order_all_ent_df__reg_s0_live
where is_cb_shop= 1 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and grass_date>= date'2022-03-01' and date(create_datetime)>= date'2022-03-01'
)


, batch5 as (
select case when o.create_datetime < offboard_time then 'before'
when o.create_datetime between offboard_time and offboard_time+ interval'1'day then 'return'
else 'after' end as time_range 
, o.create_datetime 
, case when o.create_datetime < offboard_time then concat('D-',cast(date_diff('day', o.create_datetime, offboard_time) as varchar ))
when o.create_datetime =offboard_time then 'D-0'
when o.create_datetime= offboard_time+ interval'1'day then 'D+0'
else concat('D+',cast(date_diff('day', offboard_time+interval'1'day , o.create_datetime) as varchar )) end as period 
, count(distinct o.order_sn) orders 
from orders o 
join t on t.affi_shopid = o.shop_id 
where t.batch='batch5' and o.create_datetime between offboard_time- interval'14'day and offboard_time+interval'31'day 
group by 1,2,3 
order by 2 
)


select 
-- time_range 
-- , create_datetime , 
b1.orders as batch5
from batch5 b1