insert into nexus.test_0310827b12ca4707cc697a8b890d33ee971554d2d57dc80ac41c91aed42d4557_6efa4c1f2bf7a3bed94d9f83676f8250 with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, from_unixtime(offboard_time) offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status!=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')


) t2 ON t1.shopid = t2.mst_shopid
where t1.cb_option=1 and t1.country !='SG'
)


, t as (select sip.affi_shopid 
, date(offboard_time) offboard_time
, batch 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT)
where a.batch='mass return batch1'
)
, orders as (
select shop_id 
, date(create_datetime) create_datetime
, order_sn 
, order_id 
from mp_order.dwd_order_all_ent_df__reg_s0_live
where is_cb_shop= 1 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and grass_date>= date'2022-03-01' and date(create_datetime)>= date'2022-03-01'
)




, mr_batch1 as (
select case when o.create_datetime < date'2022-06-13' then 'before'
when o.create_datetime between date'2022-06-13' and date'2022-06-10'+ interval'1'day then 'return'
else 'after' end as time_range 
, o.create_datetime 
, case when o.create_datetime < date'2022-06-13' then concat('D-',cast(date_diff('day', o.create_datetime, date'2022-06-13') as varchar ))
when o.create_datetime = date'2022-06-13' then 'D-0'
when o.create_datetime= date'2022-06-13'+ interval'1'day then 'D+0'
else concat('D+',cast(date_diff('day', date'2022-06-13'+interval'1'day , o.create_datetime) as varchar )) end as period 
, count(distinct o.order_sn) orders 
from orders o 
join sip on sip.affi_shopid = o.shop_id 
where o.create_datetime between date'2022-06-13'- interval'14'day and date'2022-06-13' +interval'31'day 
group by 1,2,3 
order by 2 
)


select 
-- time_range, 
-- create_datetime, 
b4.orders as ADO 
-- b2.* 
from mr_batch1 b4 
where b4.create_datetime= current_date-interval'1'day