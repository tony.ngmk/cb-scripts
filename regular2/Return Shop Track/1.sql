insert into nexus.test_849e46f5b3d8f8a320993cd28a03d7c4eafe74f967bb6f67c1811c2e59697d03_1592b7271debfaffcc34e14220d421d3 with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, from_unixtime(mtime) offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status = 3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) t2 
ON t1.shopid = t2.mst_shopid
-- WHERE DATE(offboard_time) >= current_date - INTERVAL '4' MONTH --only track recently returned shops 
)
, cb as (


SELECT DISTINCT (CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN (LOWER(child_account_owner_userrole_name) LIKE '%lt%' or LOWER(child_account_owner_userrole_name) LIKE '%css%') THEN 'Long Tail'
ELSE 'Others' END) seller_type 
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, grass_region
, child_account_name
, child_account_owner
, CAST(child_shopid AS BIGINT) child_shopid
, rm_tl
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
where grass_region <>'AR'
)


, t as (select sip.affi_shopid 
, date(offboard_time) offboard_date
, batch 
, mst_country
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT)
where a.batch in ('mass return batch1','mass return batch1.5')
)


, orders as (
select date(from_unixtime(create_timestamp)) create_date 
, grass_region
, shop_id 
, order_id
, gmv_usd
, SUM(CASE WHEN (is_flash_sale = 1 or item_promotion_source = 'shopee') THEN order_fraction ELSE 0 END) inorganic_orders
, SUM(CASE WHEN (is_flash_sale = 1 or item_promotion_source = 'shopee') THEN gmv_usd ELSE 0 END) inorganic_gmv_usd
from mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
where is_cb_shop= 1 
and tz_type = 'local'
and is_placed = 1
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and grass_date>= date'2022-05-01'
GROUP BY 1,2,3,4,5
)


, consol AS (SELECT 
o.grass_region
, create_date
, o.shop_id 
, gp_account_name
, ggp_account_name
, batch
, offboard_date 
, count(distinct o.order_id) orders 
, SUM(gmv_usd) gmv_usd
, SUM(inorganic_orders) inorganic_orders
, SUM(inorganic_gmv_usd) inorganic_gmv_usd
, case when sp.date is not null then 'yes' else 'no' end as spike_day
FROM orders o 
JOIN t 
ON t.affi_shopid = o.shop_id 
join cb on cb.child_shopid = t.affi_shopid 
left join (select 
cast(date as date) date 
from regcbbi_others.sip_spike_day_2022_reg_s0
) sp on sp.date= o.create_date
GROUP BY 1,2,3,4, 5 , 6 , 7 , 12 
)


SELECT grass_region 
, create_date 
, shop_id affi_shopid 
, gp_account_name
, ggp_account_name
, batch 
, offboard_date
, orders 
, gmv_usd 
, inorganic_orders 
, inorganic_gmv_usd
, (orders - inorganic_orders) organic_orders
, (gmv_usd - inorganic_gmv_usd) organic_gmv_usd
, spike_day
FROM consol