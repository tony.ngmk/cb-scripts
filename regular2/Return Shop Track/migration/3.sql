%%time

# Downstream

grass_region = 'SG'
grass_date = '2022-11-14'

sqlFile = f'''
WITH 
cb as (
SELECT DISTINCT (CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN (LOWER(child_account_owner_userrole_name) LIKE '%lt%' or LOWER(child_account_owner_userrole_name) LIKE '%css%') THEN 'Long Tail'
ELSE 'Others' END) seller_type 
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, grass_region
, child_account_name
, child_account_owner
, CAST(child_shopid AS BIGINT) child_shopid
, rm_tl
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
where grass_region <>'AR'
)
, orders AS (
    SELECT
          order_id
        , gmv_usd 
        , is_flash_sale
        , item_promotion_source
        , order_fraction
        , shop_id
        , DATE(from_unixtime(sip_offboard_timestamp)) offboard_date
        , DATE(from_unixtime(create_timestamp)) create_date
        , grass_region
        , is_flash_sale
        , item_promotion_source
    FROM
        staging_mp_cb.dwd_orderid_itemid_order_events_affiliate_df__reg_live
    WHERE
        1=1
        -- AND is_placed = 1 -- original is not filtered too
        AND grass_region = '{grass_region}'
        AND grass_date >= DATE '{grass_date}'
        AND DATE(from_unixtime(create_timestamp)) = '{grass_date}'
        -- AND sip_shop_status_id = 1 -- require to bring correct column
        -- AND affiliate_sip_shop_status_id = 3
)
, t as (
select 
  affi_shopid 
, batch
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
where batch not in ('batch1','batch2')
)
, intermediate AS (
SELECT
      grass_region
    , create_date
    , shop_id as affi_shopid
    , offboard_date
    , shop_id
    , count(distinct order_id) orders 
    , SUM(gmv_usd) gmv_usd
    , sum(case when is_flash_sale = 1 then order_fraction else 0 end) cfs_orders
    , sum(case when item_promotion_source = 'shopee' then order_fraction else 0 end) campaign_orders
    , sum(case when is_flash_sale = 1 then gmv_usd else 0 end) cfs_gmv_usd
    , sum(case when item_promotion_source = 'shopee' then gmv_usd else 0 end) campaign_gmv_usd
FROM orders
GROUP BY 1,2,3,4,5
)
, final AS (
    select
          o.grass_region
        , create_date
        , o.shop_id
        , gp_account_name
        , ggp_account_name
        , batch
        , offboard_date 
        , case when sp.date is not null then 'yes' else 'no' end as spike_day
        , SUM(orders) orders 
        , SUM(gmv_usd) gmv_usd
        , sum(cfs_orders)+sum(campaign_orders) inorganic_orders
        , sum(cfs_gmv_usd)+sum(campaign_gmv_usd) inorganic_gmv_usd
    from intermediate o
    join t on t.affi_shopid = o.shop_id
    join cb on cb.child_shopid = t.affi_shopid
    left join (select 
    cast(date as date) date 
    from regcbbi_others.sip_spike_day_2022_reg_s0
    ) sp on sp.date=o.create_date
    GROUP BY 1,2,3,4,5,6,7,8
)

SELECT
    *
FROM
    final
'''

sdf = spark.sql(sqlFile)
sdf.cache()
sdf.show(truncate=False)
# df = sdf.toPandas()
# df.to_csv("return_shop_track.csv", index=False)
# df