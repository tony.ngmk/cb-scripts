insert into nexus.test_48c3b49386b3fd56238ac05e38288dc6942991a63c237bfea2b059091511e3c5_fe65961a472f1092e55aa9072532bf26 with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, from_unixtime(mtime) offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')


) t2 ON t1.shopid = t2.mst_shopid
)




, t as (select sip.affi_shopid 
, date(offboard_time) offboard_time
, batch 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT)
where a.batch='batch5'
)
, web as (select distinct to_shop_id
, request_id
, grass_date
, chatbot_session
, conversation_id
, from_shop_id
from marketplace.webchat__seller_v3__reg_hourly_s0_live 
where grass_date between current_date - interval'2'day and current_date + interval'5'day and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and is_auto_reply=false 
)




, webchat as (
select w.grass_date
, sip.affi_shopid shop_id 
, count(distinct w.conversation_id) receive 
, count(distinct w2.conversation_id) reply
, try(count(distinct w2.conversation_id) *DECIMAL'1.000'/count(distinct w.conversation_id) ) reply_rate 
from web w 
join sip on sip.affi_shopid = cast(w.to_shop_id as BIGINT) 
left join web w2 on cast(w2.from_shop_id as BIGINT)= sip.affi_shopid and w.grass_date=w2.grass_date and w.conversation_id= w2.conversation_id
group by 1, 2 


)


, batch5 as (
select case when o.grass_date < offboard_time then 'before'
when o.grass_date between offboard_time and offboard_time+ interval'1'day then 'return'
else 'after' end as time_range 
, o.grass_date 
, case when o.grass_date < offboard_time then concat('D-',cast(date_diff('day', o.grass_date, offboard_time) as varchar ))
when o.grass_date =offboard_time then 'D-0'
when o.grass_date= offboard_time+ interval'1'day then 'D+0'
else concat('D+',cast(date_diff('day', offboard_time+interval'1'day , o.grass_date) as varchar )) end as period 
, try(sum(reply) *DECIMAL'1.000'/sum(receive) ) reply_rate 
from webchat o 
join t on t.affi_shopid = o.shop_id 
where t.batch='batch5' and o.grass_date between offboard_time- interval'14'day and offboard_time+interval'31'day 
group by 1,2,3 
order by 2 
)


select distinct 
-- time_range, 
-- grass_date, 
b4.reply_rate as batch5 
from batch5 b4
where b4.grass_date=current_date-interval'1'day