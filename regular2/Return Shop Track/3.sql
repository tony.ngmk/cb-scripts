insert into nexus.test_1d36eaf53b3656df10d63199d6644eb954d608d7e01f0c990ef5127516a45d14_99dd20dcaf7edc9dc2dd15cbcb8430ca with 
sip as (select affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
from marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
join (select affi_shopid
, affi_username
, mst_shopid 
, country 
, from_unixtime(mtime) offboard_time
from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status = 3 and grass_region in ('sg','my','ph','th','id','vn','tw','mx','br','co','cl','pl','es','fr')
) t2 
on t1.shopid = t2.mst_shopid
-- where date(offboard_time) >= current_date - interval '4' month --only track recently returned shops 
)
, cb as (


select distinct (case when lower(child_account_owner_userrole_name) like '%ust%' then 'ultra short tail'
when lower(child_account_owner_userrole_name) like '%st%' then 'short tail' 
when lower(child_account_owner_userrole_name) like '%mt%' then 'mid tail'
when (lower(child_account_owner_userrole_name) like '%lt%' or lower(child_account_owner_userrole_name) like '%css%') then 'long tail'
else 'others' end) seller_type 
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, grass_region
, child_account_name
, child_account_owner
, cast(child_shopid as bigint) child_shopid
, rm_tl
from cncbbi_general.shopee_cb_seller_profile_with_old_column
where grass_region <>'ar'
)


, t as (select sip.affi_shopid 
, date(offboard_time) offboard_date
, batch 
, mst_country
, gp_account_name
, ggp_account_name
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as bigint)
join cb on cb.child_shopid = cast(a.affi_shopid as bigint) 
where batch not in ('batch1','batch2')
)




, orders as (
select date(create_datetime) create_date 
, grass_region
, shop_id 
, order_id
, gmv_usd
, sum(case when is_flash_sale = 1 then order_fraction else 0 end) cfs_orders
, sum(case when item_promotion_source = 'shopee' then order_fraction else 0 end) campaign_orders
, sum(case when is_flash_sale = 1 then gmv_usd else 0 end) cfs_gmv_usd
, sum(case when item_promotion_source = 'shopee' then gmv_usd else 0 end) campaign_gmv_usd


from mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
where is_cb_shop= 1 
and tz_type = 'local'
and is_placed = 1
and grass_region in ('sg','my','ph','th','id','vn','tw','mx','br','co','cl','pl','es','fr')
and grass_date>= date'2022-03-12' and date(create_datetime) <= current_date-interval'1'day 
group by 1,2,3,4,5
)


, consol as (select 
o.grass_region
, create_date
, t.batch
, offboard_date 
, gp_account_name
, t.ggp_account_name
, count(distinct o.order_id) orders 
, sum(gmv_usd) gmv_usd
, sum(cfs_orders)+sum(campaign_orders) inorganic_orders
, sum(cfs_gmv_usd)+sum(campaign_gmv_usd) inorganic_gmv_usd
, case when sp.date is not null then 'yes' else 'no' end as spike_day
from t 


join orders o 
on t.affi_shopid = o.shop_id 
left join (select 
cast(date as date) date 
from regcbbi_others.sip_spike_day_2022_reg_s0
) sp on sp.date= o.create_date
group by 1,2,3,4, 5 , 6,11
)


select grass_region 
, create_date
, batch 
, offboard_date
, gp_account_name
, ggp_account_name
, orders 
, gmv_usd
, inorganic_orders 
, inorganic_gmv_usd
, (orders - inorganic_orders) organic_orders
, (gmv_usd - inorganic_gmv_usd) organic_gmv_usd
, spike_day 
from consol