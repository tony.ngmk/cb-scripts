insert into nexus.test_48450fefdb59aa4eb384e02c0c288bde08453c5d9fef353da83297a9d84eaffa_9d7a8e36fa7f7c0164c0c60f75fddab3 with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, from_unixtime(mtime) offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')


) t2 ON t1.shopid = t2.mst_shopid
)




, t as (select sip.affi_shopid 
, date(offboard_time) offboard_time
, batch 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT)
where a.batch='batch5'
)


, logistics AS (
SELECT orderid AS order_id
, FROM_UNIXTIME(MIN(IF(new_status = 1, ctime, NULL))) AS arrange_ship_time
, FROM_UNIXTIME(MIN(IF(new_status = 2, ctime, NULL))) AS pickup_done_time
FROM marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_continuous_s0_live
WHERE grass_region IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
AND date(from_unixtime(ctime)) >= DATE('2022-03-01') -- DATE_ADD('day', -30, current_date)
GROUP BY 1
)


, shipout as (
SELECT o.order_create_date grass_date
, o.grass_region
, o.shop_id 
, COUNT(DISTINCT o.order_sn) AS gross_order
, COUNT(DISTINCT CASE WHEN o.payment_method_id != 6 AND o.pay_timestamp IS NOT NULL THEN o.order_sn
WHEN o.payment_method_id = 6 AND o.shipping_confirm_timestamp IS NOT NULL THEN o.order_sn
ELSE NULL END) AS pay_order_count
, COUNT(DISTINCT CASE WHEN log.arrange_ship_time IS NOT NULL AND (o.cancel_user_id IS NULL OR o.cancel_user_id != o.buyer_id) THEN o.order_sn
ELSE NULL END) AS arrange_ship_order_count
, try(COUNT(DISTINCT CASE WHEN log.arrange_ship_time IS NOT NULL AND (o.cancel_user_id IS NULL OR o.cancel_user_id != o.buyer_id) THEN o.order_sn
ELSE NULL END)*decimal'1.000'/COUNT(DISTINCT CASE WHEN o.payment_method_id != 6 AND o.pay_timestamp IS NOT NULL THEN o.order_sn
WHEN o.payment_method_id = 6 AND o.shipping_confirm_timestamp IS NOT NULL THEN o.order_sn
ELSE NULL END)) ship_out_rate 
, COUNT(DISTINCT CASE WHEN log.pickup_done_time IS NOT NULL AND (o.cancel_user_id IS NULL OR o.cancel_user_id != o.buyer_id) THEN o.order_sn
ELSE NULL END) AS pickup_done_order_count
, COUNT(DISTINCT CASE WHEN o.cancel_datetime IS NOT NULL THEN o.order_sn
ELSE NULL END) AS cancel_order_count
FROM (
SELECT DISTINCT order_id 
, order_sn
, shop_id 
, payment_method_id
, grass_region
, pay_timestamp
, shipping_confirm_timestamp
, logistics_status 
, buyer_id 
, cancel_user_id 
, cancel_datetime
, date(split(create_datetime,' ')[1]) AS order_create_date
FROM mp_order.dwd_order_all_ent_df__reg_s0_live
WHERE tz_type = 'local' AND is_cb_shop = 1
AND grass_region IN ('BR', 'CL', 'CO', 'ES', 'FR', 'ID', 'MY', 'MX', 'PH', 'PL', 'SG', 'TH', 'TW', 'VN')
AND grass_date >= DATE('2022-03-01') -- DATE_ADD('day', -30, current_date)
AND DATE(split(create_datetime, ' ')[1]) BETWEEN DATE('2022-03-01') AND current_date-interval'1'day 
and date(ship_by_datetime)<= current_date-interval'1'day 
-- DATE_ADD('day', -30, current_date) AND DATE_ADD('day', -1, current_date)
) AS o
LEFT JOIN logistics AS log ON log.order_id = o.order_id
GROUP BY 1,2,3
)




, batch5 as (
select case when o.grass_date < offboard_time then 'before'
when o.grass_date between offboard_time and offboard_time+ interval'1'day then 'return'
else 'after' end as time_range 
, o.grass_date 
, case when o.grass_date < offboard_time then concat('D-',cast(date_diff('day', o.grass_date, offboard_time) as varchar ))
when o.grass_date =offboard_time then 'D-0'
when o.grass_date= offboard_time+ interval'1'day then 'D+0'
else concat('D+',cast(date_diff('day', offboard_time+interval'1'day , o.grass_date) as varchar )) end as period 
, try(sum(arrange_ship_order_count) *DECIMAL'1.000'/sum(pay_order_count) ) ship_out_rate 
from shipout o 
join t on t.affi_shopid = o.shop_id 
where t.batch='batch5' and o.grass_date between offboard_time- interval'14'day and offboard_time+interval'31'day 
group by 1,2,3 
order by 2 
)




select 


-- time_range,
-- grass_date,
b4.ship_out_rate as batch5 
from batch5 b4