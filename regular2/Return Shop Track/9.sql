insert into nexus.test_97837bba0cd6c76f37640c7d0a1365e06ba1cd6db3c9eef64d2389617a9ca4ff_fac74491005b5ea0871108b0d504c791 with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, from_unixtime(mtime) offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')


) t2 ON t1.shopid = t2.mst_shopid
)


, t as (select sip.affi_shopid 
, date(offboard_time) offboard_time
, batch 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT)
where a.batch='mass return batch1'
)
, orders as (
select shop_id 
, date(create_datetime) create_datetime
, order_sn 
, order_id 
from mp_order.dwd_order_all_ent_df__reg_s0_live
where is_cb_shop= 1 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
and grass_date>= date'2022-03-01' and date(create_datetime)>= date'2022-03-01'
)


, mrbatch1 as (
select case when o.create_datetime < offboard_time then 'before'
when o.create_datetime between offboard_time and offboard_time+ interval'1'day then 'return'
else 'after' end as time_range 
, o.create_datetime 
, case when o.create_datetime < offboard_time then concat('D-',cast(date_diff('day', o.create_datetime, offboard_time) as varchar ))
when o.create_datetime =offboard_time then 'D-0'
when o.create_datetime= offboard_time+ interval'1'day then 'D+0'
else concat('D+',cast(date_diff('day', offboard_time+interval'1'day , o.create_datetime) as varchar )) end as period 
, count(distinct o.order_sn) orders 
from orders o 
join t on t.affi_shopid = o.shop_id 
where t.batch='mass return batch1' and o.create_datetime between offboard_time- interval'14'day and offboard_time+interval'31'day 
group by 1,2,3 
order by 2 
)


select 
-- time_range 
-- , create_datetime , 
b1.orders as mr_batch1
from mrbatch1 b1