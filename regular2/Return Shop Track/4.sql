insert into nexus.test_7e9dc94719388aeca48903d5e34a3025e7e5c731f7f19e6fc4eea9023dd88e25_f0f65626b3ddfa19951562cc366dfbbe with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, date(from_unixtime(offboard_time)) offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) t2 ON t1.shopid = t2.mst_shopid
)
, cb as (


SELECT DISTINCT (CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN (LOWER(child_account_owner_userrole_name) LIKE '%lt%' or LOWER(child_account_owner_userrole_name) LIKE '%css%') THEN 'Long Tail'
ELSE 'Others' END) seller_type 
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, grass_region
, child_account_name
, child_account_owner
, CAST(child_shopid AS BIGINT) child_shopid
, rm_tl
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
where grass_region <>'AR'
) 

select 
sip.affi_shopid 
, batch 
, date(offboard_time) offboard_time
, affi_country 
, ggp_account_name
, gp_account_name 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0 a 
join sip on sip.affi_shopid = cast(a.affi_shopid as BIGINT) 
join cb on cb.child_shopid =sip.affi_shopid