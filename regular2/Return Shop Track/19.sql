insert into nexus.test_d0066094bf7c40311ea16f48180b62d45ea1a898168064cd21162b80ddc5aee4_4e27812b18a28db7ab14280ef8126e82 -- qn1 -- how many sku have been add discount price 
-- qn2 -- those no discount price add, what is the price change % for different seller tye 

--- shop --- live sku --- live model --- model edit y/n y--- price up/down --- avg 


with t as (select distinct cast(affi_shopid as BIGINT) affi_shopid 
, batch 
from regcbbi_others.sip_return_shop_v2_tmp_reg_s0
where batch='mass return batch1'
)
, sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, t1.cb_option
, offboard_time
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
, date(from_unixtime(offboard_time)) offboard_time
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES','FR')
) t2 ON t1.shopid = t2.mst_shopid
where t1.cb_option= 1 
)
, cb as (SELECT DISTINCT (CASE WHEN LOWER(child_account_owner_userrole_name) LIKE '%ust%' THEN 'Ultra Short Tail'
WHEN LOWER(child_account_owner_userrole_name) LIKE '%st%' THEN 'Short Tail' 
WHEN LOWER(child_account_owner_userrole_name) LIKE '%mt%' THEN 'Mid Tail'
WHEN (LOWER(child_account_owner_userrole_name) LIKE '%lt%' or LOWER(child_account_owner_userrole_name) LIKE '%css%') THEN 'Long Tail'
ELSE 'Others' END) seller_type 
, ggp_account_name
, gp_account_name
, gp_account_owner_userrole_name
, gp_account_seller_classification
, gp_account_owner
, grass_region
, child_account_name
, child_account_owner
, CAST(child_shopid AS BIGINT) child_shopid
FROM cncbbi_general.shopee_cb_seller_profile_with_old_column
-- where LOWER(child_account_owner_userrole_name) like '%mt%' or LOWER(child_account_owner_userrole_name) LIKE '%lt%'
)


SELECT DISTINCT
batch
, seller_type
, sum(coalesce(m2.normal_sku,0)) bef_normal_sku 
, sum(coalesce(m2.live_sku,0)) bef_live_sku
, sum(coalesce(m.normal_sku,0)) aft_normal_sku 
, sum(coalesce(m.live_sku,0)) aft_live_sku
FROM
t 
join sip on sip.affi_shopid = t.affi_shopid 
join cb on cb.child_shopid = sip.mst_shopid 
left join (select 
shop_id 
, active_item_cnt normal_sku 
, active_item_with_stock_cnt live_sku 


from dev_regcbbi_others.sip__shop_profile__reg__s0 m 
join t on t.affi_shopid = m.shop_id 
WHERE tz_type = 'local' 
and user_status = 1 and m.status = 1 and is_cb_shop = 1 and is_holiday_mode = 0 
and grass_date= current_date-interval'1'day 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')
) m on m.shop_id = t.affi_shopid 
left join (select 
shop_id 
, active_item_cnt normal_sku 
, active_item_with_stock_cnt live_sku 
from 
dev_regcbbi_others.sip__shop_profile__reg__s0 m 
join t on t.affi_shopid = m.shop_id 
WHERE tz_type = 'local' 
and user_status = 1 and m.status = 1 and is_cb_shop = 1 and is_holiday_mode = 0
and grass_date= date'2022-06-13' - interval'2'day 
and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')) m2 on m2.shop_id= t.affi_shopid 

group by 1,2