insert into nexus.test_b29288bab735495dd38afc14cc6f08f456709c9a4c5726cc8542f48ca449901a_91db2ffa27ff957560e51b9c7d2b9a81 with 
sip AS (SELECT affi_shopid
,mst_shopid
,t1.country as mst_country
,t2.country as affi_country 
, case when t1.cb_option = 1 then 'CB SIP' else 'Local SIP' end as shop_type 
, cb_option 
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live t1
JOIN (SELECT affi_shopid
, affi_username
, mst_shopid 
, country 
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
where sip_shop_status!=3 and grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL','ES')

) t2 ON t1.shopid = t2.mst_shopid
where cb_option=1 and t1.country in ('BR','MY','TW')
)


, o AS (
SELECT DISTINCT
"date"(create_datetime) grass_date
, grass_region
, shop_id
, is_cb_shop
, item_id 
, "sum"(order_fraction) orders
, "sum"(gmv_usd) gmv
, "sum"(IF((is_flash_sale = 1 or item_promotion_source = 'shopee') , order_fraction, null)) inorganic_orders
, "sum"(IF((is_flash_sale = 1 or item_promotion_source = 'shopee') , gmv_usd, null)) inorganic_gmv
, "sum"(IF((is_flash_sale != 1 and (item_promotion_source != 'shopee' or item_promotion_source is null) ) , order_fraction, null)) organic_orders
, "sum"(IF((is_flash_sale != 1 and (item_promotion_source != 'shopee' or item_promotion_source is null) ) , gmv_usd, null)) organic_gmv
FROM
mp_order.dwd_order_item_all_ent_df__reg_s0_live o 
join sip on sip.affi_shopid= o.shop_id 
WHERE ("date"("from_unixtime"(create_timestamp)) >= (date'2022-01-01')) AND grass_region <> 'FR'
and grass_date >= (date'2022-01-01')
GROUP BY 1, 2, 3, 4, 5 
) 
SELECT DISTINCT
o.grass_date
, case when o.grass_region in ('SG','MY','PH','TH','VN','TW','ID') then 'SEA' else 'Others' end as grass_region
, "sum"(orders) orders
, "sum"(gmv) gmv
, sum(inorganic_orders) inorganic_orders
, sum(inorganic_gmv) inorganic_gmv
, sum(organic_orders) organic_orders
, sum(organic_gmv) organic_gmv
, case when sp.date is not null then 'yes' else 'no' end as spike_day
FROM
o 
left join (select cast(date as date) date 
from regcbbi_others.sip_spike_day_2022_reg_s0
) sp on sp.date= o.grass_date 
GROUP BY 1, 2,9 
order by 1