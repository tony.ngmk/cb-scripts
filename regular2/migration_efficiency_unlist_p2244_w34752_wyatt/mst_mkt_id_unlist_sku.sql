insert into dev_regcbbi_general.bi__sip_migration_unlist_sku_aggregated_info__v2__df__reg__s0

 
WITH
  sku AS (
   SELECT DISTINCT grass_date
   , shop_id shopid
   , item_id itemid
   , status item_status
   , stock item_stock
   , GRASS_REGION 
   , seller_status
   , is_holiday_mode
   , shop_status
   
   FROM
     mp_item.dim_item__reg_s0_live
WHERE grass_date =  date(date_parse('${PRE_DAY}','%Y%m%d'))
  and grass_region in 
('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
and seller_status= 1 and shop_status=1 and is_holiday_mode=0
  )

, sip AS (
   SELECT DISTINCT
     a.affi_shopid
   , affi_itemid
   , affi_country
   , mst_shopid
   , mst_itemid
   , unlist_factor
   , b.country mst_country
   , b.cb_option
   FROM
     (marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
    join (select distinct affi_Shopid 
        from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live 
        where grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
        and sip_shop_status!=3) c on c.affi_shopid = a.affi_shopid
   INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON (a.mst_shopid = b.shopid))
   where b.country='ID'
) 
, affi AS (
   SELECT DISTINCT
     affi_country
   , affi_itemid
   , item_status
   , item_stock
   , mst_itemid
   , unlist_factor
   , cb_option
   
   FROM
     (sip
   INNER JOIN sku ON (sip.affi_itemid = sku.itemid))
     
   WHERE (item_status = 8) and seller_status= 1 and shop_status=1 and is_holiday_mode=0 
) 

SELECT DISTINCT
 GRASS_REGION mst_country
, affi.affi_country
, (CASE WHEN ("bitwise_and"(unlist_factor, 1) > 0) THEN 'MstItemUnlist' 
        WHEN ("bitwise_and"(unlist_factor, 2) > 0) THEN 'RecomCatScoreTooLow' 
        WHEN ("bitwise_and"(unlist_factor, 4) > 0) THEN 'MstPriceTooLow' 
        WHEN ("bitwise_and"(unlist_factor, 8) > 0) THEN 'VariationNameTooLong' 
        WHEN ("bitwise_and"(unlist_factor, 16) > 0) THEN 'VariationOptionTooLong' 
        WHEN ("bitwise_and"(unlist_factor, 32) > 0) THEN 'AffiPriceOutOfRange' 
        WHEN ("bitwise_and"(unlist_factor, 64) > 0) THEN 'AitemQcBlackListForbidden' 
        WHEN ("bitwise_and"(unlist_factor, 128) > 0) THEN 'SyncItemFail' 
        WHEN ("bitwise_and"(unlist_factor, 256) > 0) THEN 'SyncItemFail' 
        WHEN ("bitwise_and"(unlist_factor, 512) > 0) THEN 'NoHitChannelWhiteList' 
        WHEN ("bitwise_and"(unlist_factor, 1024) > 0) THEN 'PitemQcBlackListForbidden' 
        WHEN ("bitwise_and"(unlist_factor, 2048) > 0) THEN 'PitemForbiddenList' 
        WHEN ("bitwise_and"(unlist_factor, 4096) > 0) THEN 'DuplicateItem' 
        WHEN ("bitwise_and"(unlist_factor, 16384) > 0) THEN 'AffiitemForbiddenList' 
        WHEN ("bitwise_and"(unlist_factor, 32768) > 0) THEN 'Mst item under QC'
        WHEN ("bitwise_and"(unlist_factor, 65536) > 0) THEN 'Invalid DTS setting'
        WHEN ("bitwise_and"(unlist_factor, 131072) > 0) OR ((unlist_factor = 0) AND (a.unlist_region IS NOT NULL)) THEN 'Ops require to unlist'
        WHEN ("bitwise_and"(unlist_factor, 262144) > 0) THEN 'Not used code'
        WHEN ("bitwise_and"(unlist_factor, 524288) > 0) THEN 'Shop not sale'
        WHEN ("bitwise_and"(unlist_factor, 1048576) > 0) THEN 'PFF product'
        WHEN ("bitwise_and"(unlist_factor, 2097152) > 0) THEN 'Obtain category fail'
        WHEN ("bitwise_and"(unlist_factor, 4194304) > 0) THEN 'Forbidden attribute language'
        WHEN ("bitwise_and"(unlist_factor, 8388608) > 0) THEN 'Return affi shop'
        WHEN ("bitwise_and"(unlist_factor, 16777216) > 0) THEN 'Fail to build global tree' 
        WHEN ("bitwise_and"(unlist_factor, 33554432) > 0) THEN 'Hit Price restriction'
        WHEN ("bitwise_and"(unlist_factor, 67108864) > 0) THEN 'Unlist before Unlink mtsku'
        WHEN ("bitwise_and"(unlist_factor, 134217728) > 0) THEN 'Violate TW NCC'
        WHEN ("bitwise_and"(unlist_factor, 268435456) > 0) THEN 'Title Language Not Met'
        WHEN ("bitwise_and"(unlist_factor, 536870912) > 0) THEN 'Description Language Not Met'        
        WHEN ("bitwise_and"(unlist_factor, 1073741824) > 0) THEN 'Variation Language Not Met'
        WHEN ("bitwise_and"(unlist_factor, 2147483648) > 0) THEN 'Not Qualified SLS Channel'          
        WHEN ("bitwise_and"(unlist_factor, 4294967296) > 0) THEN 'Delisting second-hand goods'        
        ELSE 'Others' END) unlist_reason
, (CASE WHEN (affi.item_stock > 0) THEN 1 ELSE 0 END) affi_item_stock
, "count"(DISTINCT affi_itemid) sku_count
, grass_date
, cb_option
, grass_date as data_date
FROM
  ((affi
INNER JOIN SKU mst ON (affi.mst_itemid = mst.ITEMID))
LEFT JOIN (
   SELECT DISTINCT
     mst_itemid
   , unlist_region
   FROM
     marketplace.shopee_sip_db__item_unlist_config_tab__reg_daily_s0_live
)  a ON ((affi.mst_itemid = a.mst_itemid) AND (affi.affi_country = a.unlist_region)))
WHERE ((mst.item_status = 1) AND (mst.item_stock > 0))
GROUP BY 1, 2, 3, 4, 6, 7


