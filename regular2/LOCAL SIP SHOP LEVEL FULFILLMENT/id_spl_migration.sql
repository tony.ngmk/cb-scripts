WITH dim_affiliateshop AS (
	SELECT
		*
	FROM
		mp_cb.dim_affiliateshop__reg_live
	WHERE
		grass_region = 'ID'
		AND grass_date BETWEEN
			current_date - interval '150' day 
			AND current_date - interval '1' day
)

SELECT * FROM dim_affiliateshop
