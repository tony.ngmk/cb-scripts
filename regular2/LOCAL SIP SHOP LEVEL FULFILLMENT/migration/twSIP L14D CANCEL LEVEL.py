%%time

a_grass_regions = "('SG', 'MY', 'PH')"
a_grass_region = 'SG'
p_grass_region = 'TW'
grass_date = '2022-11-15'

spark.sql("REFRESH TABLE staging_mp_cb.dwd_orderid_itemid_order_events_affiliate_df__reg_live")
sqlFile = f'''
WITH
dwd_order AS (
    SELECT
          order_id
        , order_sn
        , shop_id
        , primary_shop_id
        , sync_primary_order_id
        , create_datetime
        , is_net_order
        , grass_region
        , primary_shop_region
        , cancel_timestamp
        , cancel_datetime
        , cancel_reason_id
        , cancel_reason
        , grass_region
        , cancel_user_id -- this column has likely some issues
  FROM
    staging_mp_cb.dwd_orderid_itemid_order_events_affiliate_df__reg_live
  WHERE
    date(split(cancel_datetime,' ')[0]) = DATE '{grass_date}'
    AND grass_date BETWEEN DATE '{grass_date}' - interval '30' day AND DATE '{grass_date}'
    AND grass_region IN {a_grass_regions}
    AND primary_shop_region = '{p_grass_region}'
    AND 1 = CASE 
        WHEN grass_region = 'TW' AND cancel_reason_id IN (1, 2, 3, 200, 201, 204, 302,303) THEN 1
        WHEN grass_region != 'TW' AND cancel_reason_id IN (1, 2, 3, 4, 204, 301, 302,303) THEN 1
        END
    AND primary_is_cb_option = 0
), intermediate AS (
SELECT
      a.shop_id as AFFI_SHOPID
    , a.primary_shop_id AS MST_SHOPID
    , a.grass_region as affi_country
    , a.primary_shop_region as mst_country
    , a.order_sn AS AFFI_ORDERSN
    , a.order_id AS oversea_orderid
    , a.sync_primary_order_id as local_orderid
    -- , p.order_sn AS MST_ORDERSN
    , date(split(a.create_datetime,' ')[0]) as AFFI_CREATE_TIME
    , date(split(a.cancel_datetime,' ')[0]) as AFFI_CANCEL_TIME
    -- , date(split(a.create_datetime,' ')[0]) as MST_CREATE_TIME
    -- , date(split(a.cancel_datetime,' ')[0]) as MST_CANCEL_TIME
    , a.cancel_reason AS AFFI_CANCEL_REASON
    -- , a.cancel_reason AS MST_CANCEL_REASON
    , (CASE WHEN (a.cancel_reason = 1) THEN 'a.CANCEL_REASON_OUT_OF_STOCK' WHEN (a.cancel_reason = 2) THEN 'a.CANCEL_REASON_CUSTOMER_REQUEST' WHEN (a.cancel_reason = 3) THEN 'a.CANCEL_REASON_UNDELIVERABLE_AREA ' WHEN (a.cancel_reason = 4) THEN 'a.CANCEL_REASON_CANNOT_SUPPORT_COD ' WHEN (a.cancel_reason = 5) THEN 'a.CANCEL_REASON_LOST_PARCEL ' WHEN (a.cancel_reason = 100) THEN 'a.CANCEL_REASON_SYSTEM_UNPAID ' WHEN (a.cancel_reason = 200) THEN 'a.CANCEL_REASON_LOGISTICS_REQUEST_CANCELED ' WHEN (a.cancel_reason = 201) THEN 'a.CANCEL_REASON_LOGISTICS_PICKUP_FAILED' WHEN (a.cancel_reason = 202) THEN 'a.CANCEL_REASON_LOGISTICS_DELIVERY_FAILED ' WHEN (a.cancel_reason = 203) THEN 'a.CANCEL_REASON_LOGISTICS_COD_REJECTED ' WHEN (a.cancel_reason = 204) THEN 'a.CANCEL_REASON_SELLER_NOT_ARRANGE_PICKUP' WHEN (a.cancel_reason = 300) THEN 'a.CANCEL_REASON_BACKEND_ESCROW_TERMINATED ' WHEN (a.cancel_reason = 301) THEN 'a.CANCEL_REASON_BACKEND_INACTIVE_SELLER' WHEN (a.cancel_reason in (302,303)) THEN 'a.CANCEL_REASON_BACKEND_SELLER_DID_NOT_SHIP' ELSE 'Other' END) AFFI_cancel_reason_ext
    -- , (CASE WHEN (a.cancel_reason = 1) THEN 'a.CANCEL_REASON_OUT_OF_STOCK' WHEN (a.cancel_reason = 2) THEN 'a.CANCEL_REASON_CUSTOMER_REQUEST' WHEN (a.cancel_reason = 3) THEN 'a.CANCEL_REASON_UNDELIVERABLE_AREA ' WHEN (a.cancel_reason = 4) THEN 'a.CANCEL_REASON_CANNOT_SUPPORT_COD ' WHEN (a.cancel_reason = 5) THEN 'a.CANCEL_REASON_LOST_PARCEL ' WHEN (a.cancel_reason = 100) THEN 'a.CANCEL_REASON_SYSTEM_UNPAID ' WHEN (a.cancel_reason = 200) THEN 'a.CANCEL_REASON_LOGISTICS_REQUEST_CANCELED ' WHEN (a.cancel_reason = 201) THEN 'a.CANCEL_REASON_LOGISTICS_PICKUP_FAILED' WHEN (a.cancel_reason = 202) THEN 'a.CANCEL_REASON_LOGISTICS_DELIVERY_FAILED ' WHEN (a.cancel_reason = 203) THEN 'a.CANCEL_REASON_LOGISTICS_COD_REJECTED ' WHEN (a.cancel_reason = 204) THEN 'a.CANCEL_REASON_SELLER_NOT_ARRANGE_PICKUP' WHEN (a.cancel_reason = 300) THEN 'a.CANCEL_REASON_BACKEND_ESCROW_TERMINATED ' WHEN (a.cancel_reason = 301) THEN 'a.CANCEL_REASON_BACKEND_INACTIVE_SELLER' WHEN (a.cancel_reason in (302,303)) THEN 'a.CANCEL_REASON_BACKEND_SELLER_DID_NOT_SHIP' ELSE 'Other' END) MST_cancel_reason_ext
FROM
    dwd_order a
-- LEFT JOIN dwd_order p ON a.sync_primary_order_id = p.order_id
)
, final AS (
SELECT DISTINCT
    AFFI_SHOPID
    , MST_SHOPID
    , affi_country
    , mst_country
    , AFFI_ORDERSN
    -- MST_ORDERSN KIV
    , oversea_orderid
    , local_orderid
    , AFFI_CREATE_TIME
    , AFFI_CANCEL_TIME
    , AFFI_CREATE_TIME AS MST_CREATE_TIME
    , AFFI_CANCEL_TIME AS MST_CANCEL_TIME
    , AFFI_CANCEL_REASON
    , AFFI_CANCEL_REASON AS MST_CANCEL_REASON
    , AFFI_cancel_reason_ext
    , AFFI_cancel_reason_ext AS MST_cancel_reason_ext
FROM 
    intermediate
)

SELECT * FROM final
'''

sdf = spark.sql(sqlFile)
sdf.cache()
# sdf.show(truncate=False)
df = sdf.toPandas()
df.to_csv("order_id.csv", index=False)
df