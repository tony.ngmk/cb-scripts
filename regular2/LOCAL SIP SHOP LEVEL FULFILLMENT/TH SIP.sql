WITH

buyer_account AS (
   SELECT *
   FROM
     (
 VALUES 
        ROW (32352918, 'TW')
      , ROW (174331327, 'MY')
      , ROW (216124110, 'ID')
      , row (413283389,'VN')
      , row (502225957,'TH')
   )  t (buyer_id, grass_region)
) 
, sip AS (
   SELECT DISTINCT
     affi_shopid
   , A.country AFFI_COUNTRY
   , B.COUNTRY MST_COUNTRY
   , mst_shopid
   FROM
     (marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live a
   INNER JOIN (
      SELECT DISTINCT
        shopid
      , COUNTRY
      FROM
        marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
      WHERE ((cb_option = 0) AND (country IN ( 'TH')))
   )  b ON (a.mst_shopid = b.shopid))
   WHERE ((a.country IN ('SG','PH','MY' )) AND (a.grass_region IN ('SG','PH','MY' )))
) 

, o as (
  SELECT DISTINCT
     shop_id shopid
   , create_datetime
   , order_id orderid 
   , order_sn 
   , cancel_reason_id cancel_reason 
   , cancel_timestamp cancel_time 
   , cancel_datetime
   , order_be_status_id 
   , grass_region
   , is_cb_shop 
   , cancel_user_id
--   , date(split(create_datetime,' ')[1]) as create_time
--   , "count"(DISTINCT order_id) gross_order
--   , "count"(DISTINCT (CASE WHEN (order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15)) THEN order_id ELSE null END)) net_order
   
   FROM
     mp_order.dwd_order_all_ent_df__reg_s0_live
   WHERE ( date(split(create_datetime,' ')[1]) >=  current_date-interval'150'day    ) and tz_type='local' 
   --and is_cb_shop=1
   and   grass_region in ('SG','MY','PH','TH') 
   and grass_date  >=  current_date-interval'150'day 
--   GROUP BY 1, 2
)

, raw AS (
   SELECT DISTINCT
     oversea_orderid
   , "max"(local_orderid) local_orderid
   FROM
     marketplace.shopee_order_processing_cbcollection_db__order_sync_tab__reg_daily_s0_live
     where oversea_country in ('sg','my','ph')
   GROUP BY 1
) 

, cancel AS (
   SELECT DISTINCT
     o1.shopid
    , O1.ORDERID 
    , O2.ORDERSN MST_ORDERSN
    , oversea_orderid
    , local_orderid
    , O1.ORDERSN AFFI_ORDERSN
    , O1.CANCEL_TIME  AFFI_CANCEL_TIME 
    , O1.CREATE_TIME  AFFI_CREATE_TIME 
    , O2.CANCEL_TIME  MST_CANCEL_TIME 
    , O2.CREATE_TIME  MST_CREATE_TIME
    ,O1.CANCEL_REASON AFFI_CANCEL_REASON 
    , O2.CANCEL_REASON MST_CANCEL_REASON 
   , (CASE WHEN (O1.cancel_reason = 1) THEN 'CANCEL_REASON_OUT_OF_STOCK' WHEN (O1.cancel_reason = 2) THEN 'CANCEL_REASON_CUSTOMER_REQUEST' WHEN (O1.cancel_reason = 3) THEN 'CANCEL_REASON_UNDELIVERABLE_AREA ' WHEN (O1.cancel_reason = 4) THEN 'CANCEL_REASON_CANNOT_SUPPORT_COD ' WHEN (O1.cancel_reason = 5) THEN 'CANCEL_REASON_LOST_PARCEL ' WHEN (O1.cancel_reason = 100) THEN 'CANCEL_REASON_SYSTEM_UNPAID ' WHEN (O1.cancel_reason = 200) THEN 'CANCEL_REASON_LOGISTICS_REQUEST_CANCELED ' WHEN (O1.cancel_reason = 201) THEN 'CANCEL_REASON_LOGISTICS_PICKUP_FAILED' WHEN (O1.cancel_reason = 202) THEN 'CANCEL_REASON_LOGISTICS_DELIVERY_FAILED ' WHEN (O1.cancel_reason = 203) THEN 'CANCEL_REASON_LOGISTICS_COD_REJECTED ' WHEN (O1.cancel_reason = 204) THEN 'CANCEL_REASON_SELLER_NOT_ARRANGE_PICKUP' WHEN (O1.cancel_reason = 300) THEN 'CANCEL_REASON_BACKEND_ESCROW_TERMINATED ' WHEN (O1.cancel_reason = 301) THEN 'CANCEL_REASON_BACKEND_INACTIVE_SELLER' WHEN (O1.cancel_reason in (302,303)   ) THEN 'CANCEL_REASON_BACKEND_SELLER_DID_NOT_SHIP' ELSE 'Other' END) AFFI_cancel_reason_ext
    
  , (CASE WHEN (O2.cancel_reason = 1) THEN 'CANCEL_REASON_OUT_OF_STOCK' WHEN (O2.cancel_reason = 2) THEN 'CANCEL_REASON_CUSTOMER_REQUEST' WHEN (o2.cancel_reason = 3) THEN 'CANCEL_REASON_UNDELIVERABLE_AREA ' WHEN (o2.cancel_reason = 4) THEN 'CANCEL_REASON_CANNOT_SUPPORT_COD ' WHEN (o2.cancel_reason = 5) THEN 'CANCEL_REASON_LOST_PARCEL ' WHEN (o2.cancel_reason = 100) THEN 'CANCEL_REASON_SYSTEM_UNPAID ' WHEN (o2.cancel_reason = 200) THEN 'CANCEL_REASON_LOGISTICS_REQUEST_CANCELED ' WHEN (O2.cancel_reason = 201) THEN 'CANCEL_REASON_LOGISTICS_PICKUP_FAILED' WHEN (o2.cancel_reason = 202) THEN 'CANCEL_REASON_LOGISTICS_DELIVERY_FAILED ' WHEN (o2.cancel_reason = 203) THEN 'CANCEL_REASON_LOGISTICS_COD_REJECTED ' WHEN (o2.cancel_reason = 204) THEN 'CANCEL_REASON_SELLER_NOT_ARRANGE_PICKUP' WHEN (o2.cancel_reason = 300) THEN 'CANCEL_REASON_BACKEND_ESCROW_TERMINATED ' WHEN (o2.cancel_reason = 301) THEN 'CANCEL_REASON_BACKEND_INACTIVE_SELLER' WHEN (o2.cancel_reason in (302,303)   ) THEN 'CANCEL_REASON_BACKEND_SELLER_DID_NOT_SHIP' ELSE 'Other' END) MST_cancel_reason_ext
   ,  (CASE WHEN ((o1.grass_region <> 'TW')  and o2.grass_region='TW' and (o2.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303 )) 
  AND (o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))    ) AND (local_orderid > 0)  THEN 'Y' 
   WHEN ((NOT (o1.grass_region IN ('TW')))  and o2.grass_region <>'TW' and (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302 ,303)) AND (local_orderid > 0)  
  AND (o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) THEN 'Y' ELSE 'N' END) cancel_order_v2
  
  
   ,  (CASE WHEN (((o1.grass_region = 'TW') AND (o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302 ,303))) AND (local_orderid > 0)) THEN 'Y' 
   WHEN (((NOT (o1.grass_region IN ('TW'))) AND (o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) AND (local_orderid > 0)) THEN 'Y' ELSE 'N' END) cancel_order_sync_suc
   ,  (CASE WHEN ((((o1.grass_region = 'TW') AND ((NOT (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302 ,303))) OR (o2.cancel_reason IS NULL))) AND (o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303))) AND (local_orderid > 0)) THEN 'Y' 
   WHEN (((((NOT (o1.grass_region IN ('TW'))) AND (o2.grass_region IN ('TW'))) AND ((NOT (o2.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303))) OR (o2.cancel_reason IS NULL))) AND (o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) AND (local_orderid > 0)) THEN 'Y'
   WHEN (((((NOT (o1.grass_region IN ('TW'))) AND (NOT (o2.grass_region IN ('TW')))) AND ((NOT (o2.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) OR (o2.cancel_reason IS NULL))) AND (o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) AND (local_orderid > 0)) THEN 'Y' ELSE 'N' END) sync_failed_acl1_acl2
   ,  (CASE WHEN ((o1.cancel_reason = 204) AND (l.ar_time IS NOT NULL)) THEN 'Y' WHEN ((o1.cancel_reason in (302,303)    ) AND (l.pu_time IS NOT NULL)) THEN 'Y' ELSE 'N' END) acl_order
   ,  (CASE WHEN ((o1.cancel_reason IN (204, 302,303)) AND (ba.buyer_id IS NOT NULL)) AND (local_orderid > 0)  
   THEN 'Y' ELSE 'N' END) seller_fault_v2
   FROM
     (((((raw
   LEFT JOIN (
      SELECT DISTINCT
         orderid
      , order_sn ORDERSN 
      , date(create_datetime) create_time
      , date(cancel_datetime) cancel_time
      , cancel_reason
      , shopid
      , grass_region
      FROM
        o 
     where is_cb_shop=1 
   )  o1 ON (o1.orderid = raw.oversea_orderid))
LEFT JOIN (
      SELECT DISTINCT
        orderid
      , order_sn ORDERSN
      , date(cancel_datetime) cancel_time
      , date(create_datetime) create_time
      , cancel_reason
      , grass_region
      FROM
        o 
      WHERE ((is_cb_shop = 0) AND (grass_region IN ( 'TH'))) and  date(create_datetime) >= current_date-interval'100'day 
   )  o2 ON (o2.orderid = raw.local_orderid))
   LEFT JOIN (
      SELECT DISTINCT
       orderid  order_id
      , order_sn
      , cancel_user_id
      FROM
        o 
   WHERE grass_region in ('TH')   and  date(create_datetime) >= current_date-interval'100'day  and is_cb_shop=0 
 ) o3 ON (o3.order_id = o2.orderid))
   LEFT JOIN buyer_account ba ON ((ba.buyer_id = cancel_user_id) AND (ba.grass_region = o2.grass_region)))
   LEFT JOIN (
      SELECT DISTINCT
        orderid
      , "min"((CASE WHEN (new_status = 1) THEN ctime ELSE null END)) ar_time
      , "min"((CASE WHEN (new_status = 2) THEN ctime ELSE null END)) pu_time
      FROM
        marketplace.shopee_logistics_audit_v3_db__logistics_audit_tab__reg_daily_s0_live
      WHERE (grass_region IN ('MY')) and date(from_unixtime(ctime)) >= current_date-interval'100'day 
      GROUP BY 1
   )  l ON (l.orderid = raw.local_orderid))
   WHERE ("date"(o1.cancel_time) between  current_date-interval'14'day  and  current_date-interval'1'day    ) and 
   (CASE WHEN ((o1.grass_region = 'TW') AND (o1.cancel_reason IN (1, 2, 3, 200, 201, 204, 302,303))) THEN True 
   WHEN ((NOT (o1.grass_region IN ('TW'))) AND (o1.cancel_reason IN (1, 2, 3, 4, 204, 301, 302,303))) THEN True ELSE False END)  
--   GROUP BY 1, 2
) 


SELECT DISTINCT 
AFFI_SHOPID 
,MST_SHOPID 
,affi_country 
, mst_country 
,oversea_orderid
, AFFI_ORDERSN 
,local_orderid
,MST_ORDERSN
,AFFI_CREATE_TIME
,AFFI_CANCEL_TIME
,MST_CREATE_TIME
,MST_CANCEL_TIME
,AFFI_CANCEL_REASON
,MST_CANCEL_REASON
,AFFI_cancel_reason_ext
,MST_cancel_reason_ext
, CASE WHEN cancel_order_sync_suc ='N' THEN 'A TO P SYNC FAILED'
  WHEN acl_order='Y' THEN 'AUTO PUSH & PICKUP DONE'
  WHEN  cancel_order_sync_suc ='Y' and (cancel_order_v2='Y' OR seller_fault_v2='Y') THEN 'SELLER FAULT'

  ELSE 'OTHER' END AS CANCEL_REASON
FROM CANCEL 
JOIN SIP ON SIP.AFFI_SHOPID = CANCEL.SHOPID 
WHERE MST_COUNTRY ='TH'
-- and affi_country ='CO'
