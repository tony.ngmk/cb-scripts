--DELETE FROM dev_regcbbi_general.bi__sip_migration_effi_failed_aggregated_info__v2__df__reg__s0 WHERE data_date = date(date_parse('${PRE_DAY}','%Y%m%d')) and mst_country='ID';

insert into dev_regcbbi_general.bi__sip_migration_effi_failed_aggregated_info__v2__df__reg__s0

WITH
  sku AS (
   SELECT DISTINCT grass_date
   , shop_id shopid
   , item_id itemid
   , status item_status
   , stock item_stock
   FROM
     mp_item.dim_item__reg_s0_live
WHERE grass_date = date(date_parse('${PRE_DAY}','%Y%m%d'))  AND status = 1 AND stock > 0
  and grass_region in 
('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
  and tz_type = 'local'
) 
, sip AS (
   SELECT DISTINCT
     a.affi_shopid
   , affi_itemid
   , affi_country
   , mst_shopid
   , mst_itemid
   , err_code
   , err_msg
   , b.country mst_country
   , cb_option
   FROM
     (marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
   join (select distinct affi_Shopid 
            from marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live 
            where grass_region in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
            and sip_shop_status!=3) c on c.affi_shopid = a.affi_shopid
   INNER JOIN marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live b ON (a.mst_shopid = b.shopid))
   WHERE (((a.affi_itemid = 0)) AND (affi_country IS NOT NULL)) and b.country='VN'
   --and mst_itemid = 7861315253
) 
, affi AS (
   SELECT DISTINCT
     affi_country
   , affi_shopid
   FROM
     (sip
   INNER JOIN (
      SELECT DISTINCT shop_id AS shopid 
      FROM
        regcbbi_others.shop_profile
      WHERE (((status = 1) AND (user_status = 1)) AND ((is_holiday_mode = 0) OR (is_holiday_mode IS NULL))) AND grass_date = date(date_parse('${PRE_DAY}','%Y%m%d'))  AND is_cb_shop = 1
     and grass_region in 
('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
   )  a ON (sip.affi_shopid = a.shopid))
) 
SELECT DISTINCT  mst_country
, affi.affi_country
, sku.grass_date
, CASE WHEN err_msg LIKE '%item limit%' or  err_msg like '%shop limit%'  THEN 'Shop limit'
       WHEN err_msg LIKE '%variation duplicate%' THEN 'Variation duplication'
       WHEN err_msg LIKE '%description length%' THEN 'Description length limit'
       WHEN err_msg LIKE '%name length%' THEN 'Title length limit'
       WHEN err_msg LIKE '%no mapping%' THEN 'No mapping category'
       WHEN err_msg LIKE '%max/min ratio%' or err_msg like '%greater than max limit%'  THEN 'Price ratio limit'
       WHEN err_msg LIKE '%in holiday mode%' THEN 'Ashop in holiday'
       WHEN err_msg LIKE '%build category and attributes fail%' THEN 'Build category fail'
       WHEN err_msg LIKE '%qc balck list filter fail%'         or err_msg like  '%qc black list filter fail%' THEN 'Hit prohibited keyword'
       WHEN err_msg like '%category contains forbidden category%'  or err_msg like'%cat hit forbidden list for psite%'

       THEN 'Hit prohibited category'
       WHEN err_msg like '%pshop not hit logistics channel whitelist%' or err_msg like'%channel not found%' THEN 'No supportive channel'
       WHEN err_msg like '%mst item abnormal on create%' or err_msg like'%local mst item in qc status on create%' THEN 'PSKU on QC'
       WHEN err_msg like '%invalid character%'  THEN 'Invalid Character'
       WHEN err_msg like '%forbid in ID%'  THEN 'Forbid in ID'       
       WHEN err_msg like '%add_item_with_mtsku failed%'  THEN 'Add item with mtsku failed'       
       WHEN err_msg like '%ERROR_MISSING_MANDATORY_ATTRIBUTE%'  THEN 'ERROR_MISSING_MANDATORY_ATTRIBUTE'     
       WHEN err_msg like '%ERROR_MTSKU_NO_GLOBAL_CATEGORY%'  THEN 'ERROR_MTSKU_NO_GLOBAL_CATEGORY'            
       WHEN err_msg like '%Can not find mtskuModelId%'  THEN 'Can not find mtskuModelId' 
  
       ELSE 'Others' END AS err_type
, "count"(DISTINCT mst_itemid) sku_count
, cb_option
, sku.grass_date as data_date

FROM
  ((affi
INNER JOIN sip ON (affi.affi_shopid = sip.affi_shopid))
INNER JOIN sku ON (sip.mst_itemid = sku.itemid))
where affi.affi_country in ('SG','MY','PH','TH','ID','VN','TW','MX','BR','CO','CL','PL')
GROUP BY 1, 2, 3, 4,6

--
--select * from dev_regcbbi_general.bi__sip_migration_effi_failed_aggregated_info__v2__df__reg__s0
