insert into nexus.test_2a36143ab4c442bd99772be1881ddb726b241952f292cdf0fa9c74f98f0ead98_46bfb2736e9bb35aeb2ea827242c4fb7 WITH sip
AS (
SELECT DISTINCT a.mst_shopid,
a.mst_itemid,
a.affi_shopid,
affi_itemid,
b.country mst_country,
b.cb_option
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT shopid,
country,
cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE country = 'VN'
AND cb_option = 0
) b ON (a.mst_shopid = b.shopid)
INNER JOIN (
SELECT DISTINCT affi_shopid
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status <> 3
AND grass_region <> ''
) c ON (c.affi_shopid = a.affi_shopid)
),
channel
AS (
SELECT DISTINCT shopid,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50021.enabled') AS BIGINT), 0) spx_vn,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50011.enabled') AS BIGINT), 0) ghn_vn,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50018.enabled') AS BIGINT), 0) jnt_vn,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50023.enabled') AS BIGINT), 0) njv_vn,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50024.enabled') AS BIGINT), 0) best_vn,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50012.enabled') AS BIGINT), 0) ghtk_vn,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50015.enabled') AS BIGINT), 0) vnpost_vn,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.50010.enabled') AS BIGINT), 0) viettel_vn,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.5001.enabled') AS BIGINT), 0) mask_channel_vn
FROM marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE (grass_region = 'VN')
)
SELECT DISTINCT mst_shopid,
mst_itemid,
affi_shopid
FROM sip
INNER JOIN channel ch ON (ch.shopid = sip.mst_shopid)
INNER JOIN (
SELECT DISTINCT item_id
FROM mp_item.dim_item__reg_s0_live
WHERE (grass_date = (CURRENT_DATE - INTERVAL '1' DAY))
AND (tz_type = 'local')
AND (grass_region IN ('SG', 'MY', 'TW', 'PH'))
AND (seller_status = 1)
AND (shop_status = 1)
AND (is_holiday_mode = 0)
AND (STATUS = 1)
) ai ON (ai.item_id = sip.affi_itemid)
WHERE sip.mst_country = 'VN'
AND (
mask_channel_vn = 0
OR (spx_vn + ghn_vn + jnt_vn + njv_vn + best_vn + ghtk_vn + vnpost_vn + viettel_vn = 0)
)