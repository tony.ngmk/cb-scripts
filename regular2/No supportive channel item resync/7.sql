insert into nexus.test_789f861cc27a123885c1040aa4dfa8b54c4967b383ff6168156b864e67d8a758_a5aba4e46cc1f2e5eb9d362d3113e108 WITH sip
AS (
SELECT DISTINCT a.mst_shopid,
a.mst_itemid,
a.affi_shopid,
affi_itemid,
b.country mst_country,
b.cb_option
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT shopid,
country,
cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE country = 'MY'
AND cb_option = 0
) b ON (a.mst_shopid = b.shopid)
INNER JOIN (
SELECT DISTINCT affi_shopid
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status <> 3
AND grass_region <> ''
) c ON (c.affi_shopid = a.affi_shopid)


),
channel
AS (
SELECT DISTINCT shopid,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.20088.enabled') AS BIGINT), 0) spxmarketplace_my,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.20007.enabled') AS BIGINT), 0) pos_my,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.20011.enabled') AS BIGINT), 0) jnt_my,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.20010.enabled') AS BIGINT), 0) edhl_my,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.20021.enabled') AS BIGINT), 0) njv_my,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.20023.enabled') AS BIGINT), 0) citylink_my,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.2000.enabled') AS BIGINT), 0) mask_channel_my
FROM marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE grass_region = 'MY'
)
SELECT DISTINCT mst_shopid,
mst_itemid,
affi_shopid
FROM sip
INNER JOIN channel ch ON (ch.shopid = sip.mst_shopid)
INNER JOIN (
SELECT DISTINCT item_id
FROM mp_item.dim_item__reg_s0_live
WHERE (grass_date = (CURRENT_DATE - INTERVAL '1' DAY))
AND (tz_type = 'local')
AND (grass_region IN ('SG', 'PH', 'TW'))
AND (seller_status = 1)
AND (shop_status = 1)
AND (is_holiday_mode = 0)
AND (STATUS = 1)
) ai ON (ai.item_id = sip.affi_itemid)
WHERE (sip.mst_country = 'MY')
AND (
mask_channel_my = 0
OR (spxmarketplace_my + pos_my + jnt_my + edhl_my + njv_my + citylink_my = 0)
)