insert into nexus.test_37a42560c10b2a254187a9bee635c7d7f1927d0ec1c2178b1f72096ef7a5f88c_ccca1736c41d0b8711dd67498de230b6 WITH
sip AS (
SELECT DISTINCT
a.mst_shopid
, a.mst_itemid
, a.affi_shopid
, affi_itemid
, b.country mst_country
, b.cb_option
FROM
((marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
, cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) b ON (a.mst_shopid = b.shopid))
INNER JOIN (
SELECT DISTINCT affi_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (sip_shop_status <> 3 AND grass_region IN ('SG','MY'))
) c ON (c.affi_shopid = a.affi_shopid))
WHERE ((b.country IN ('TW')) AND (b.cb_option = 0))
) 
, channel AS (
SELECT DISTINCT
shopid
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.30005.enabled') AS bigint), 0) seven_eleven_tw
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.30007.enabled') AS bigint), 0) hilife_tw
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.39305.enabled') AS bigint), 0) txwl_tw
FROM
marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE (grass_region IN ('TW'))
) 


SELECT DISTINCT
mst_shopid
, mst_itemid
, affi_shopid
FROM
((sip
INNER JOIN channel ch ON (ch.shopid = sip.mst_shopid))
INNER JOIN (
SELECT DISTINCT item_id
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((((grass_date = (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local')) AND (grass_region IN ('SG', 'MY'))) AND (seller_status = 1)) AND (shop_status = 1)) AND (is_holiday_mode = 0)) AND (status = 1))
) ai ON (ai.item_id = sip.affi_itemid))
WHERE ((sip.mst_country = 'TW') AND ((seven_eleven_tw + hilife_tw + txwl_tw = 0)))