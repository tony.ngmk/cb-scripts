insert into nexus.test_3c8ac8ec630c676963b88198329c7152f3a2ee41be5daa26d7e6049c49366226_17501d4f470f2589b091eb082218e1d4 WITH
sip AS (
SELECT DISTINCT
a.mst_shopid
, a.mst_itemid
, a.affi_shopid
, affi_itemid
, b.country mst_country
, b.cb_option
FROM
((marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT
shopid
, country
, cb_option
FROM
marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
) b ON (a.mst_shopid = b.shopid))
INNER JOIN (
SELECT DISTINCT affi_shopid
FROM
marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE (sip_shop_status <> 3 AND grass_region <> '')
) c ON (c.affi_shopid = a.affi_shopid))
WHERE ((b.country IN ('TH')) AND (b.cb_option = 0))
) 
, channel AS (
SELECT DISTINCT
shopid
, COALESCE(CAST("json_extract"("from_utf8"(extinfo.logistics_info), '$.channels.7000.enabled') AS bigint), 0) mask_channel_th
FROM
marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE (grass_region = 'TH')
) 
SELECT DISTINCT
mst_shopid
, mst_itemid
, affi_shopid
FROM
((sip
INNER JOIN channel ch ON (ch.shopid = sip.mst_shopid))
INNER JOIN (
SELECT DISTINCT item_id
FROM
mp_item.dim_item__reg_s0_live
WHERE (((((((grass_date = (current_date - INTERVAL '1' DAY)) AND (tz_type = 'local')) AND (grass_region IN ('SG', 'MY', 'PH', 'ID', 'VN', 'BR', 'MX','CO','CL'))) AND (seller_status = 1)) AND (shop_status = 1)) AND (is_holiday_mode = 0)) AND (status = 1))
) ai ON (ai.item_id = sip.affi_itemid))
WHERE ((sip.mst_country = 'TH') AND ((mask_channel_th = 0) ))--OR (((((((J_T_Express_id + JNE_Regular__Cashless_id) + SICEPAT_id) + IDE_id) + njv_id) + Anteraja_id) + SPX_ID) = 0)))