insert into nexus.test_6653ce3d31c41ee9573d85698fbef09069ab0e4e1553aeab602c4b90c9a777b4_60871c3e6a986d98a6f92e91613f2860 SELECT DISTINCT mst_shopid,
mst_itemid,
affi_shopid,
affi_itemid
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live map
INNER JOIN (
SELECT DISTINCT itemid
FROM (
SELECT DISTINCT itemid,
grass_region,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20007.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20007.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20007.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_pos,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20011.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20011.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20011.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_jnt,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20010.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20010.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20010.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_edhl,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20088.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20088.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20088.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_spxmarketplace,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20021.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20021.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20021.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_njv,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.30005.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.30005.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20007.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_711,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.39305.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.39305.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.39305.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_txwl,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.30007.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.30007.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.20011.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_hilife,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.2000.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.2000.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.2000.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_mask_channel_my,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80014.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80014.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80014.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_J_T,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80005.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80005.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80005.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_sicepat,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80015.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80015.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80015.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_JNE,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80023.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80023.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.80023.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_IDE,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50011.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50011.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50011.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_GHN,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50021.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50021.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50021.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_SPX,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.8003.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.8003.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.8003.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_mask_channel_id,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.5001.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.5001.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.5001.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_mask_channel_vn,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50018.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50018.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50018.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_JNT_vn,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50023.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50023.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50023.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_njv_vn,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50024.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50024.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50024.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_best_vn,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50012.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50012.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50012.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_ghtk_vn,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50015.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50015.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50015.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_vnpost_vn,
sum(DISTINCT (
CASE 
WHEN (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50010.enabled') AS VARCHAR) = 'true')
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50010.enabled') AS boolean) = true)
OR (CAST(json_extract(from_utf8(extinfo.logistics_info), '$.50010.enabled') AS INT) = 1)
THEN 1
ELSE 0
END
)) have_channel_viettel_vn
FROM marketplace.shopee_item_v4_db__item_v4_tab__reg_daily_s0_live
WHERE (cb_option = 0)
AND (STATUS = 1)
AND (stock > 0)
AND (grass_region = 'VN')
GROUP BY 1,
2
)
WHERE (grass_region = 'VN')
AND (((have_channel_GHN + have_channel_SPX) + have_channel_mask_channel_vn + have_channel_JNT_vn + have_channel_njv_vn + have_channel_best_vn + have_channel_ghtk_vn + have_channel_vnpost_vn + have_channel_viettel_vn) = 0)
) i4 ON (map.mst_itemid = i4.itemid)
INNER JOIN (
SELECT DISTINCT item_id
FROM mp_item.dim_item__reg_s0_live
WHERE (tz_type = 'local')
AND (grass_date = (CURRENT_DATE - INTERVAL '1' DAY))
AND (shop_status = 1)
AND (seller_status = 1)
AND (is_holiday_mode = 0)
AND (STATUS = 1)
AND (grass_region IN ('SG', 'MY', 'TW', 'PH'))
) ai ON (ai.item_id = map.affi_itemid)