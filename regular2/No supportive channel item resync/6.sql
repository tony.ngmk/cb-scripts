insert into nexus.test_19cc2a1e05d32f155afa3f3f40b298526760851fdd241abd6d36566176ca8bdf_6f7d19a5720ac556704c0c66aafd856a WITH sip
AS (
SELECT DISTINCT a.mst_shopid,
a.mst_itemid,
a.affi_shopid,
affi_itemid,
b.country mst_country,
b.cb_option
FROM marketplace.shopee_sip_v2_db__item_map_tab__reg_daily_s0_live a
INNER JOIN (
SELECT DISTINCT shopid,
country,
cb_option
FROM marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live
WHERE country IN ('ID')
AND cb_option = 0
) b ON (a.mst_shopid = b.shopid)
INNER JOIN (
SELECT DISTINCT affi_shopid
FROM marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live
WHERE sip_shop_status <> 3
AND grass_region <> ''
) c ON (c.affi_shopid = a.affi_shopid)
),
channel
AS (
SELECT DISTINCT shopid,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.80014.enabled') AS BIGINT), 0) J_T_Express_id,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.80005.enabled') AS BIGINT), 0) JNE_Regular__Cashless_id,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.80015.enabled') AS BIGINT), 0) SICEPAT_id,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.80023.enabled') AS BIGINT), 0) IDE_id,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.80020.enabled') AS BIGINT), 0) njv_id,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.80025.enabled') AS BIGINT), 0) Anteraja_id,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.80088.enabled') AS BIGINT), 0) SPX_ID,
COALESCE(CAST(json_extract(from_utf8(extinfo.logistics_info), '$.channels.8003.enabled') AS BIGINT), 0) mask_channel_id
FROM marketplace.shopee_shop_v2_db__shop_tab__reg_daily_s0_live
WHERE grass_region = 'ID'
)
SELECT DISTINCT mst_shopid,
mst_itemid,
affi_shopid
FROM sip
INNER JOIN channel ch ON ch.shopid = sip.mst_shopid
INNER JOIN (
SELECT DISTINCT item_id
FROM mp_item.dim_item__reg_s0_live
WHERE grass_date = (CURRENT_DATE - INTERVAL '1' DAY)
AND (tz_type = 'local')
AND (grass_region IN ('SG', 'MY', 'PH', 'TH', 'VN', 'TW', 'BR', 'MX', 'CO', 'CL'))
AND (seller_status = 1)
AND (shop_status = 1)
AND (is_holiday_mode = 0)
AND (STATUS = 1)
) ai ON (ai.item_id = sip.affi_itemid)
WHERE (sip.mst_country = 'ID')
AND (
(mask_channel_id = 0)
OR (((((((J_T_Express_id + JNE_Regular__Cashless_id) + SICEPAT_id) + IDE_id) + njv_id) + Anteraja_id) + SPX_ID) = 0)
)